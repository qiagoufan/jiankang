<?php

use lib\GenerationDTOUtil;

require '../index.php';


$util = new GenerationDTOUtil();
$modules = FUR_Config::get_mysql_config();
if ($modules == null) {
    echo 'no db config';
    die();
}
$gen = new GenerationDTOUtil();
if (count($argv) > 1) {
    $table = $argv[1];
    $module = isset($argv[2]) ? $argv[2] : "main";
    $gen->generateTable($table, $module);
} else {
    foreach ($modules as $module => $db_config) {
        $gen->generateDb($module);
    }

}


