<!DOCTYPE html>
<!--[if IE 8]>
<html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]>
<html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <?php require TPL_PATH . 'template_c/common/head.tpl.php'; ?>
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <link
            href="/static/bower_components/jquery.tagsinput/dist/jquery.tagsinput.min.css"
            rel="stylesheet"/>
    <link
            href="/static/bower_components/bootstrap-fileinput/css/fileinput.min.css"
            rel="stylesheet"/>
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->

</head>

<body>

<!-- start: HEADER -->
<?php require TPL_PATH . 'template_c/common/header.tpl.php'; ?>
<!-- end: HEADER -->
<!-- start: MAIN CONTAINER -->
<div class="main-container">
    <div class="navbar-content">
        <!-- start: SIDEBAR -->
        <?php require TPL_PATH . 'template_c/common/sidebar.tpl.php'; ?>
        <!-- end: SIDEBAR -->
    </div>

    <!-- start: PAGE -->
    <div class="main-content">
        <div class="container">
            <!-- start: PAGE HEADER -->
            <div class="row">
                <div class="col-sm-12">
                    <!-- start: STYLE SELECTOR BOX -->
                    <!-- end: STYLE SELECTOR BOX -->
                    <!-- start: PAGE TITLE & BREADCRUMB -->
                    <ol class="breadcrumb">
                        <li><i class="clip-user-5"></i> <a href="./"> 广告商 </a></li>
                        <li class="active">广告管理</li>
                    </ol>
                    <div class="page-header">
                        <h1>
                            广告填单
                            <small>overview &amp; stats</small>
                        </h1>
                    </div>
                    <!-- end: PAGE TITLE & BREADCRUMB -->
                </div>
            </div>
            <!-- end: PAGE HEADER -->
            <!-- start: PAGE CONTENT -->
            <div class="row">
                <div class="col-sm-12">
                    <!-- start: TEXT FIELDS PANEL -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-external-link-square"></i> 广告填单
                        </div>
                        <div class="panel-body">
                            <form role="form" class="form-horizontal"   action="/advertiser/advertisement_manage/save_advertisement" id="advertisement_form"  method="post" enctype="multipart/form-data">
                                <div class="errorHandler alert alert-danger no-display">
                                    <i class="fa fa-times-sign"></i> 出现了一些问题,请检查后再提交
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="advertisement_title">
                                        广告标题<span class="symbol required"></span>
                                    </label>
                                    <div class="col-sm-4">
                                        <input type="text" placeholder="" id="advertisement_title" name="advertisement_title" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="advertisement_content">
                                        广告文案<span class="symbol required"></span>
                                    </label>
                                    <div class="col-sm-8">
                                        <input type="text" placeholder="" id="advertisement_content" name="advertisement_content"   class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="advertisement_link">
                                        链接地址<span class="symbol required"></span>
                                    </label>
                                    <div class="col-sm-4">
                                        <input type="text" placeholder="" id="advertisement_link"  name="advertisement_link" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="advertisement_img_fileinput">广告图片 </label>
                                    <div class="form-group">
                                        <div class="col-sm-4">
                                            <label> 上传你的广告banner图 </label>
                                            <input id="advertisement_img_fileinput" type="file" class="file">
                                            <input type="hidden" name="advertisement_img" id="advertisement_img">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <span class="col-md-2 "></span>
                                    <div class="col-md-2">
                                        <button class="btn btn-yellow btn-block" type="submit" id="submit_btn">
                                            提交 <i class="fa fa-arrow-circle-right"></i>
                                        </button>
                                    </div>
                                    <div class="col-md-2">
                                        <button class="btn btn btn-light-grey btn-block" type="button"
                                                onclick="history.back();">
                                            返回 <i class="fa fa-close"></i>
                                        </button>
                                    </div>
                                </div>


                            </form>
                        </div>
                    </div>

                    <!-- end: TEXT FIELDS PANEL -->
                </div>
            </div>

            <!-- end: PAGE CONTENT-->
        </div>
    </div>
    <!-- end: PAGE -->
</div>
<!-- end: MAIN CONTAINER -->
<!-- start: FOOTER -->
<div class="footer clearfix">
    <div class="footer-inner">
        <script>
            document.write(new Date().getFullYear())
        </script>
        &copy; clip-one by cliptheme.
    </div>
    <div class="footer-items">
        <span class="go-top"><i class="clip-chevron-up"></i></span>
    </div>
</div>
<!-- end: FOOTER -->

<!-- start: RIGHT SIDEBAR -->
<!-- end: RIGHT SIDEBAR -->
<!-- start: MAIN JAVASCRIPTS -->
<!--[if lt IE 9]>
<script src="/static/bower_components/respond/dest/respond.min.js"></script>
<script src="/static/bower_components/Flot/excanvas.min.js"></script>
<script src="/static/bower_components/jquery-1.x/dist/jquery.min.js"></script>
<![endif]-->
<!--[if gte IE 9]><!-->
<script type="text/javascript"
        src="/static/bower_components/jquery/dist/jquery.min.js"></script>
<!--<![endif]-->
<script type="text/javascript"
        src="/static/bower_components/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript"
        src="/static/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript"
        src="/static/bower_components/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
<script type="text/javascript"
        src="/static/bower_components/blockUI/jquery.blockUI.js"></script>
<script type="text/javascript"
        src="/static/bower_components/iCheck/icheck.min.js"></script>
<script type="text/javascript"
        src="/static/bower_components/perfect-scrollbar/js/min/perfect-scrollbar.jquery.min.js"></script>
<script type="text/javascript"
        src="/static/bower_components/jquery.cookie/jquery.cookie.js"></script>
<script type="text/javascript"
        src="/static/bower_components/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript" src="/static/assets/js/min/main.min.js"></script>
<!-- end: MAIN JAVASCRIPTS -->
<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script src="/static/bower_components/moment/min/moment.min.js"></script>
<script
        src="/static/bower_components/bootstrap-maxlength/src/bootstrap-maxlength.js"></script>
<script src="/static/bower_components/autosize/dist/autosize.min.js"></script>
<script src="/static/bower_components/select2/dist/js/select2.min.js"></script>
<script
        src="/static/bower_components/jquery.maskedinput/dist/jquery.maskedinput.min.js"></script>
<script
        src="/static/bower_components/jquery-maskmoney/dist/jquery.maskMoney.min.js"></script>
<script
        src="/static/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script
        src="/static/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script
        src="/static/bower_components/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script
        src="/static/bower_components/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<script
        src="/static/bower_components/jquery.tagsinput/src/jquery.tagsinput.js"></script>
<script
        src="/static/bower_components/summernote/dist/summernote.min.js"></script>
<script
        src="/static/bower_components/bootstrap-fileinput/js/plugins/canvas-to-blob.min.js"></script>
<script
        src="/static/bower_components/bootstrap-fileinput/js/fileinput.js"></script>
<script src="/static/assets/js/form-elements.js"></script>

<script src="/static/bower_components/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="/static/bower_components/summernote/dist/summernote.min.js"></script>
<script src="/static/bower_components/jquery-ajaxform/jquery.form.min.js"></script>
<script src="/static/bower_components/jquery-validation/src/localization/messages_zh.js"></script>

<!--     表单验证 -->
<script src="/static/user/advertisement-form-validation.js"></script>
<script src="/static/user/ajax-submit.js"></script>
<script src="/static/bower_components/bootstrap-fileinput/js/fileinput_locale_zh.js"></script>
<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->

<script>
    $("#advertisement_img_fileinput").fileinput({
        uploadUrl: '/common/upload/upload_img',
        dropZoneEnabled: false,
        uploadAsync: true,
        allowedFileExtensions: ['gif', 'jpg', 'png'],
        language: 'zh',
        maxFileSize: 2000,//单位为kb，如果为0表示不限制文件大小

    });

    $("#advertisement_img_fileinput").on("fileuploaded", function (event, data) {
		if(data.response.result==false){
			alert(data.response.message);
			return false;
		}
    		$("#advertisement_img").val(data.response.file_path);
    });
    jQuery(document).ready(function () {

        Main.init();
        ajaxSubmit.init();
        AdvertisementFormValidator.init();

    });
</script>

</body>

</html>