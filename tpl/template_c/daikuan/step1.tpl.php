<!DOCTYPE html>
<html>
     
    <head>
        <meta charset="utf-8">
        <title>个人贷款</title>
        <meta http-equiv="keywords" content="无抵押个人贷款,个人快速贷款,快速贷款,小额贷款公司,急需小额贷款,小额贷款 当天放款" />
        <meta http-equiv="description" content="个人贷款怎么找？无抵押个人贷款哪里快,小额贷款最快当天放款,门槛低，免抵押免担保，多家贷款公司提供贷款服务，更加匹配您的需求" />
        <link href="/static/css/basic.css" rel="stylesheet" type="text/css" />
        <link href="/static/css/daikuan_wap.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="/static/js/jquery-1.9.1.min.js"></script>
        <script defer="defer" type="text/javascript" src="/static/js/daikuan_step_1.js"></script>
        <script defer="defer" type="text/javascript" src="/static/js/area.js"></script>
        <script type="text/javascript">
            var homeurl = "/";
        </script>
    </head>
     
    <body>
        <meta name="viewport" content="width=device-width,user-scalable=no" />
        <div class="wrapper">
            <!--头部-->
            <div class="loan_banner">
                <img src="/static/img/2341_banner.jpg" alt="壹财道" />
            </div>
            <script type="text/javascript">
                var level = 'B';
                var bx_act = '1';
                var min_act = "false"; //是否小额
 
                var LID = '';
                var url_act = 'wap';
                var act = 'wap';
                var id = '';
                var phone = '';
                var name = '';
                var LoanAmount = '';
 
                
            </script>
            <!--第一步-->
            <section class="loan_data" style="display:;" id="step_1">
                <header>
                        <h2>贷款人资料</h2>
 
                </header>
                <div tt-data-click tt-data-convertid="50374588212" tt-data-eventtype="form">
                    <form action="" method="post">
                        <ul class="main">
                            <li>
                                <label>贷款金额:</label>
                                <input type="number" name="amount" id="amount" value="" placeholder="请输入贷款金额" class="input" /> <span>万元</span>
                            </li>
                            <li>
                                <label>真实姓名:</label>
                                <input type="text" name="name" id="name" value="" placeholder="请输入真实姓名" class="input" maxlength="10" />
                            </li>
                            <li>
                                <label>手机号码:</label>
                                <input type="tel" name="phone" id="phone" value="" placeholder="请输入手机号" class="input" />
                            </li>
                        </ul>
                    </form>
                    <div class="ppp">
                        <p>
                            <input name="privacy" type="checkbox" value="1" id="privacy" />已阅读<a href="javascript:void(0);" class="on_privacy" id="on_privacy">《安全条款》</a>隐私信息将严格保密</p>
                    </div>
                    <div class="loan_button" data="step_1" id="next_btn">下一步</div>
                    <p class="note" style="text-align: left">注：您所填写的资料务必真实，有助于您更快获得贷款！后续将会有平安普惠等公司客户经理为您提供服务！</p>
                    <style type="text/css">
                        .ppa {
                            margin-top: -20px;
                        }
                        .ppa p {
                            line-height: 25px;
                        }
                    </style>
                    <div class="ppp ppa">
                        <p>
                            <input name="privacy_more_loan" type="checkbox" value="1" id="privacy_more_loan" />本人同意壹财道系统推荐多家贷款供应商</p>
                        <p>
                            <input name="privacy_ywx_a" type="checkbox" value="1" id="privacy_ywx_a" />本人同意领取免费险 <a href="javascript:void(0);" class="on_privacy_pingan" id="on_privacy_pingan">（信息安全条款）</a>
 
                        </p>
                        <p>
                            <input name="privacy_ywx_b" type="checkbox" value="1" id="privacy_ywx_b" />本人同意中国平安后续致电联系确认保险产品相关事宜</p>
                    </div>
                </div>
                <script type="text/javascript">
                    $("#privacy_ywx_a,#privacy_ywx_b").click(function() {
                        if ($(this).prop('checked') == true) {
                            $("#privacy_ywx_a,#privacy_ywx_b").prop("checked", "checked");
                        } else {
                            $("#privacy_ywx_a,#privacy_ywx_b").removeAttr("checked");
                        }
 
                    })
                </script>
                <div class="pingan-xuzhi" style="margin: 0 20px; line-height: 25px; color: #999999;">
                    <p>   <a href="/upload/baoxian.html" target="_blank">《平安交通工具意外伤害保险条款》</a>、    <a href="/upload/caichananquan.html">《平安旅行附加行李物品和旅行证件损失保险条款》</a>
 
                    </p>
                    <p>在保险期间内，承担被保险人在乘坐商业运营的飞机、火车、地铁、轻轨、轮船、汽车、出租车期间因遭遇意外伤害事故所导致的身故、残疾责任。</p>
                    <p>保障项目：</p>
                    <p>飞机意外--100万（保额）</p>
                    <p>火车/地铁/轻轨意外--50万（保额）</p>
                    <p>轮船意外--50万（保额）</p>
                    <p>汽车意外--10万（保额）</p>
                    <p>交通意外伤害医疗责任（保额为2万元，免赔100元后按90%赔付）</p>
                    <p>承担被保险人在旅行时的行李物品和旅行证件损失责任限额500元</p>
                    <p>保险名称：畅行天下升级版</p>
                    <p>保障期限：180天</p>
                    <p>投保人群：本保险的被保险人为25-50周岁身体健康身体健康、能正常生活的人员</p>
                    <p>注：本保险仅限赠送，不得转卖或变相销售。本勾选项仅表现了赠送意向不确保每个人都可以领取成功。</p>
                    <p>【投保须知】</p>
                    <p>1）投保规定：本保险身故受益人为法定受益人。本保险的保险对象为25-50周岁身体健康、能正常工作或正常生活的自然人。</p>
                    <p>2）保险限制：每一客户受赠保险以1份为限；本赠险产品仅限被保人本人领取。</p>
                    <p>3）保险期限：本保险的保障期限为（6）个月，以保单载明的保险起止日期为准。对保险起至日期之外所发生的保险事故本公司不负给付保险金责任。</p>
                    <p>4）告知义务：依据我国《保险法》的规定，投保人、被保险人应如实告知，否则保险人有权依法解除保险合同，并对于保险合同解除前发生的保险事故不负任何责任。投保人、被保险人在投保时，应对投保书内各项内容如实详细地说明或填写清楚。否则，保险人有权依法解除保险合同。</p>
                    <p>5）保险凭证：本产品仅提供电子保单。仅限赠送。保单经审核生效后客户会自动收到短信通知，请将短信保存并将短信上的电子保单号记录在适当的位置，以方便查询及理赔。您也可以通过拨打电话：95511或登陆http://www.pingan.com/property_insurance/pa18AutoInquiry/ 查询您的保单信息。</p>
                    <p>6）本保险不接受撤保、退保、加保及被保险人更换。</p>
                    <p>7）保险金申请：发生保险事故后，请被保险人或受益人及时凭电子保险单号及身份信息向保险公司报案，并提供相关证明和资料，保险公司将尽快按照有关条款履行给付责任。</p>
                </div>
            </section>
            <!--第二步-->
            <!--第三步-->
            <!--验证手机号-->
            <!--保险-->
            <!--信用卡-->
            <!--申请成功-->
            <!--底部-->
            <input name="LMAgentID" id="LMAgentID" type="hidden" value="" />
            <input name="LMMemberID" id="LMMemberID" type="hidden" value="" />
            <input name="c" id="c" value="" type="hidden">
            <input name="s" id="s" value="" type="hidden">
            <input name="p" id="p" value="" type="hidden">
            <input name="a" id="a" value="" type="hidden">
            <input name="v" id="v" value="" type="hidden">
            <input name="proid" id="proid" value="" type="hidden">
            <input name="wcid" id="wcid" value="" type="hidden">
            <input name="wxnid" id="wxnid" value="" type="hidden">
            <input name="AMID" id="AMID" value="" type="hidden">
            <input name="ANID" id="ANID" value="" type="hidden">
            <input name="ADID" id="ADID" value="" type="hidden">
            <input name="isSendsms" class='isSendsms' value="0" type="hidden" />
            <div class="loading" style="display: none;">loading...</div>
            <div class="risk_tips">
                    <h2>本平台郑重承诺：</h2>
 
                <p style="text-indent: 2em;">所有贷款申请在未成功放款前不收取任何费用，且不承诺一定可放贷成功，谨防电信诈骗，请警惕需要您付款的来电。</p>
            </div>
            <footer>
                <div class="foot_box">
                        <h3>放贷流程</h3>
 
                    <div class="process">
                        <div class="icon">
                            <img src="http://img.yicaidao.cn/static/images/loan/apply.png" alt="在线申请" />
                        </div>
                        <div class="icon">
                            <img src="http://img.yicaidao.cn/static/images/loan/customer.png" alt="客服协助" />
                        </div>
                        <div class="icon">
                            <img src="http://img.yicaidao.cn/static/images/loan/sub.png" alt="提交资料" />
                        </div>
                        <div class="icon">
                            <img src="http://img.yicaidao.cn/static/images/loan/bank.png" alt="银行放贷" />
                        </div>
                    </div>
                </div>
            </footer>
            <div class="copy">深圳市壹财道信息咨询有限公司版权所有
                <br />粤ICP备13088114号</div>
            <!--错误提示对话框-->
            <div class="zhezhao"></div>
            <div class="layer" id="layer"></div>
            <div class="show_city_tips">
                    <h2>
                请确认您的常驻城市？<span id="close_city">x</span>
            </h2>
 
                <p id="check_city">深圳</p>
                <p id="phone_city">内蒙古</p>
                <p id="other_city">其它城市</p>
            </div>
            <!--城市选择框-->
            <section class="city_box" style="display: none;">
                    <h3>当前</h3>
 
                <div class="myposition">  <b style="margin-bottom: 0;" id="dw_city"></b>
 
                </div>
                    <h3>热门城市</h3>
 
                <div class="rm_city"></div>
                <!--字母排序开始-->
                <div class="letter"></div>
            </section>
            <script type="text/javascript">
                var LMAgentID = "";
                if (LMAgentID != "") {
                    $("input[type='checkbox']").prop("checked", "checked");
                }
            </script>
        </div>
        <!--隐私条款-->
        <section class="privacy_boxs" id="privacy_boxs" style="display: none;">
            <div class="pingan_privacy_box" style="display: none;">
                <header>
                        <h2>隐私策略-安全保障</h2>
 
                </header>
                <div class="privacy_box">
                        <h3>【信息安全条款】</h3>
 
                    <p>本人授权平安集团，除法律另有规定之外，将本人提供给平安集团的信息、享受平安集团服务产生的信息（包括本单证签署之前提供和产生的信息）以及平安集团根据本条约定查询、收集的信息，用于平安集团及其因服务必要委托的合作伙伴为本人提供服务、推荐产品、开展市场调查与信息数据分析。</p>
                    <p>本人授权平安集团，除法律另有规定之外，基于为本人提供更优质服务和产品的目的，向平安集团因服务必要开展合作的伙伴提供、查询、收集本人的信息。</p>
                    <p>本人同意授权贵公司向征信机构查询本人被征信机构合法采集、整理或加工产生的其他信息提供者提供的个人信息用于核保审查相关事宜；如您不同意本授权条款，可【致电客服热线95511】取消授权。</p>
                    <p>为确保本人信息的安全，平安集团及其合作伙伴对上述信息负有保密义务，并采取各种措施保证信息安全。</p>
                    <p>本条款自本单证签署时生效，具有独立法律效力，不受合同成立与否及效力状态变化的影响。</p>
                    <p>本条所称“平安集团”是指中国平安保险（集团）股份有限公司及其直接或间接控股的公司，以及中国平安保险（集团）股份有限公司直接或间接作为其单一最大股东的公司。</p>
                    <p>如您不同意上述授权条款的部分或全部，可致电客服热线（95511）取消或变更授权。</p>
                </div>
            </div>
            <div class="default-box">
                <header>
                        <h2>隐私策略-安全保障</h2>
 
                </header>
                <div class="privacy_box">
                        <h3>用户个人信息及隐私保护政策</h3>
 
                    <p>重要须知：深圳市壹财道信息咨询有限责任公司（下称“壹财道”）一贯重视用户的个人信息及隐私的保护，在您使用壹财道的服务的时候，壹财道有可能会收集和使用您的个人信息及隐私。为此，壹财道通过本《壹财道用户个人信息及隐私保护政策》（以下简称“本《隐私条款》”）向您说明您在使用壹财道的服务时，壹财道是如何收集、存储、使用和分享这些信息的，以及壹财道向您提供的访问、更新、控制和保护这些信息的方式。</p>
                    <p>本《隐私条款》与您使用壹财道的服务、在壹财道购物息息相关，请您务必仔细阅读、充分理解（未成年人应当在其监护人的陪同下阅读），包括但不限于免除或者限制壹财道责任的条款。</p>
                    <p>您如果使用或者继续使用壹财道的服务，即视为您充分理解并完全接受本《隐私条款》；您如果对本《隐私条款》有任何疑问、异议或者不能完全接受本《隐私条款》，请联系壹财道客服，客户服务电话：0755-26917587</p>
                    <p>第一条 本《隐私条款》所述的个人信息，是指个人姓名、住址、出生日期、身份证号码、银行账号、移动电话号码等单独或与其他信息对照可以设别特定的个人的信息，包括但不限于您在注册壹财道用户账号时填写并提供给壹财道的姓名、性别、生日、移动电话号码、身份证号码、地址等。</p>
                    <p>本《隐私条款》所述的个人信息，特指您在首次使用壹财道的服务之前就已经形成并确定的个人信息。</p>
                    <p>第二条 您承诺并保证：您主动填写或者提供给壹财道的个人信息是真实的、准确的、完整的。而且，填写或者提供给壹财道后，如果发生了变更的，您会在第一时间内，通过原有的渠道或者壹财道提供的新的渠道进行更新，以确保壹财道所获得的您的这些个人信息是最新的、真实的、准确的和完整的；否则，壹财道无须承担由此给您造成的任何损失。</p>
                    <p>第三条 您应当重视您的个人信息的保护，您如果发现您的个人信息已经被泄露或者存在被泄露的可能，且有可能会危及您注册获得的壹财道账户安全，或者给您造成其他的损失的，您务必在第一时间通知壹财道，以便壹财道采取相应的措施确保您的壹财道账户安全，防止损失的发生或者进一步扩大；否则，壹财道无须承担由此给您造成的任何损失（及扩大的损失）。</p>
                    <p>第四条 您充分理解并完全接受：您在使用壹财道的服务时，壹财道有可能会收集您的如下信息（以下统称“用户信息”）：</p>
                    <p>（一） 第一条所述的您的个人信息；</p>
                    <p>（二） 您提供给第三方或者向第三方披露的个人信息及隐私；</p>
                    <p>（三） 您登录和使用壹财道网站、App的时间、系统日志信息以及行为数据（包括但不限于收入、年龄、财产状态等数据）；</p>
                    <p>（四） 您所使用的台式计算机、移动设备的品牌、型号、IP地址以及软件版本信息</p>
                    <p>（五）为了实现前述目的，通过cookie或者其他方式自动采集到的您的其他个人信息或者隐私。</p>
                    <p>您通过具有定位功能的移动设备登录、使用壹财道的App时，壹财道有可能会通过GPS或者Wifi收集您的地理位置信息；您如果不同意收集，您在您的移动设备上关闭此项功能。</p>
                    <p>第五条 您充分理解并完全接受：保护用户信息是壹财道一贯的政策，壹财道将会使用各种安全技术和程序存储、保护用户信息，防止其被未经授权的访问、使用、复制和泄露。壹财道不向任何第三方透漏用户信息，但存在下列任何一项情形或者为第七条所述的目的而披露给第三方的除外：</p>
                    <p>（一） 基于国家法律法规的规定而对外披露；</p>
                    <p>（二） 应国家司法机关及其他有法律权限的政府机关基于法定程序的要求而披露；</p>
                    <p>（三） 为保护壹财道或您的合法权益而披露；</p>
                    <p>（四） 在紧急情况下，为保护其他用户或者第三方人身安全而披露；</p>
                    <p>（五） 用户本人或其监护人授权披露；</p>
                    <p>（六） 应用户的监护人的合法要求而向其披露。</p>
                    <p>（七） 第三方是壹财道的合作方签订广告投放或数据共享业务合同，并承诺对客户信息保密的。</p>
                    <p>壹财道即便是按照前款约定将用户信息披露给第三方，亦会要求接收上述用户信息的第三方严格按照国家法律法规使用和保护用户信息。</p>
                    <p>第六条 您充分理解并完全接受：即便是壹财道采取各种安全技术和程序存储、保护用户信息，防止其被未经授权的访问、使用、复制和泄露，但用户信息仍然有可能发生被黑客攻击、窃取，因不可抗力或者其他非壹财道的自身原因而被泄露的情形。对此，只要是壹财道采取了必要的措施防止上述情形之发生，并在上述情形发生之后采取必要的措施防止其损失进一步扩大，壹财道则无须赔偿由此给您造成的任何损失。</p>
                    <p>第七条 您充分理解并完全接受：壹财道有可能将用户信息用于下列某一个或者某几个目的：</p>
                    <p>在您登录壹财道网站或者App时，为了满足您的业务申请需求我们会跟第三方合作为您提供服务，以及为提升您的用户体验我们会进行一定程度的回访，包括但不仅限于给客户赠送礼品、保险以及将跟客户需求匹配的其他产品推荐给客户做选择。</p>
                    <p>第八条 您充分理解并完全接受：壹财道或者为实现第七条所述目的而接收了壹财道提供的用户信息的第三方企业或者公司发生合并或者分立，那么合并或者分立后企业或者公司仍然有权按照本《隐私条款》及其他相关约定收集、存储、保护、使用和分享用户信息，而无须另行征得您的同意。</p>
                    <p>第九条 您充分理解并完全接受：壹财道为了实现第七条所述目的，有权将用户信息提供给其壹财道集团公司旗下的关联公司“深圳市壹财道信息咨询有限公司”以及业务合作方使用，包括不仅限于“北京心向优势信息技术有限公司”、“ 恒诚科技发展（北京）有限公司” 、“深圳市融信信息咨询有限公司”、“海南圆点立方信息技术有限公司”、“平安普惠”等，如客户有保险测算意向并于流程留下相关信息，视为本人授权将本人相关信息转给“招商信诺人寿保险有限公司”、“友邦保险有限公司”、“平安人寿”、“太平人寿”、“阳光保险”、“中英人寿”、“泰康人寿”、“中美大都会”等保险公司（以上保险公司为简写）以提供后续保险服务。</p>
                    <p>壹财道承诺并保证：上述壹财道关联公司以及跟合作企业签订可使用用户信息时，亦将遵照本《隐私条款》履行相应的用户个人信息及隐私保护义务。</p>
                    <p>第十条 您充分理解并完全接受：如果壹财道因为违反本《隐私条款》或者本《隐私条款》部分条款无效，导致壹财道不得不向您进行赔偿的，那么赔偿的最高金额亦仅以您使用壹财道的服务时所支付的对价金额为限，上述对价不包括您从壹财道购买商品或者服务所支付的价款。</p>
                    <p>第十一条 您充分理解并完全接受：本《隐私条款》已经向您充分说明了壹财道是如何收集、存储、使用、保护以及分享您的用户信息，除了您对本《隐私条款》存在疑问或者异议，可以询问壹财道客户服务部外，壹财道没有义务另行以其他书面的方式再向您说明其用户个人信息及隐私保护政策。但如果壹财道有另行书面说明，且与本《隐私条款》相矛盾或者不一致的，以另行书面说明的为准；未尽事宜，仍以本《隐私条款》为准。</p>
                    <p>第十二条 本《隐私条款》条款是可分的，所约定的任何条款如果部分或者全部无效，不影响该条款其他部分及本协议其他条款的法律效力。</p>
                    <p>第十三条 壹财道基于本《隐私条款》及其补充约定的有效弃权必须是书面的，并且该弃权不能产生连带的相同或者类似的弃权。</p>
                    <p>第十四条 壹财道不会向客户收取业务办理费用，请您拒绝任何需要收费的来电。</p>
                    <p>第十五条 壹财道有可能会适时变更本《隐私条款》，一经变更，壹财道将会按照此前的渠道或者可以用户知晓的其他渠道对外进行公布，必要的时候，壹财道将会在壹财道网站、App显著的位置或者以发送电子邮件的方式提醒您。修订后的《隐私条款》即取代修订前的《隐私条款》而产生相应的法律效力。您务必关注并了解本《隐私条款》的变更，且一经变更，如果您继续使用壹财道的服务，即视为您充分理解并完全接受变更后的本《隐私条款》。</p>
                </div>
            </div>
            <div class="privacy_tips pp_dh">点击页面，返回表单</div>
        </section>
    </body>
 
</html>