<!DOCTYPE html>
<html>
     
    <head>
        <meta charset="utf-8">
        <title>个人贷款</title>
        <meta http-equiv="keywords" content="无抵押个人贷款,个人快速贷款,快速贷款,小额贷款公司,急需小额贷款,小额贷款 当天放款" />
        <meta http-equiv="description" content="个人贷款怎么找？无抵押个人贷款哪里快,小额贷款最快当天放款,门槛低，免抵押免担保，多家贷款公司提供贷款服务，更加匹配您的需求" />
        <link href="/static/css/basic.css" rel="stylesheet" type="text/css" />
        <link href="/static/css/daikuan_wap.css" rel="stylesheet" type="text/css" />
        <link href="/static/css/mobiscroll.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="/static/js/jquery-1.9.1.min.js"></script>
        <script defer="defer" type="text/javascript" src="/static/js/mobiscroll.js?rand=20170719"></script>
        <script defer="defer" type="text/javascript" src="/static/js/daikuan_wap.js?rand=20170719"></script>
        <script type="text/javascript">
            var homeurl = "/";
        </script>
        <script type="text/javascript" src="/static/js/area.js"></script>
    </head>
     
    <body>
        <meta name="viewport" content="width=device-width,user-scalable=no" />
        <div class="wrapper">
            <!--头部-->
            <div class="loan_banner">
                <img src="http://img.yicaidao.cn/static/images/loan/banner/2341_banner.jpg" alt="壹财道" />
            </div>
            <script type="text/javascript">
                var level = 'B';
                var bx_act = '1';
                var min_act = "false"; //是否小额
 
                var LID = '';
                var url_act = 'step_2';
                var act = 'wap';
                var id = '<?php echo $id?>';
                var phone = '<?php echo $phone?>';
                var name = '<?php echo $name?>';
                var LoanAmount = '<?php echo $amount?>';
            </script>
            <!--第一步-->
            <!--第二步-->
            <section class="loan_data" id="step_2">
                <header>
                        <h2>贷款人资料</h2>
 
                </header>
                <div tt-data-click tt-data-convertid="50374588212" tt-data-eventtype="form">
                    <form action="" method="post">
                        <ul class="main">
                            <li>
                                <label>房贷情况:</label>
                                <div class="select">有房贷</div>
                                <div class="select">红本在手</div>
                                <div class="select">无房</div>
                                <input type="hidden" name="mortgage" id="mortgage" value="" />
                            </li>
                            <li>
                                <label>信用卡:</label>
                                <div class="select">有</div>
                                <div class="select">无</div>
                                <input type="hidden" name="card" id="card" value="" />
                            </li>
                            <li>
                                <label>车贷情况:</label>
                                <div class="select">有车贷</div>
                                <div class="select">有车无贷</div>
                                <div class="select">无车</div>
                                <input type="hidden" name="car" id="car" value="" />
                            </li>
                            <li>
                                <label>月收入:</label>
                                <div class="select">5千以下</div>
                                <div class="select">5千-1万</div>
                                <div class="select">1万-2万</div>
                                <div class="select">2万以上</div>
                                <input type="hidden" name="wage" id="wage" value="" />
                            </li>
                            <li>
                                <label>人寿保险:</label>
                                <div class="select">无</div>
                                <div class="select">年缴2400内</div>
                                <div class="select">年缴超2400</div>
                                <input type="hidden" name="baoxian" id="baoxian" value="" />
                            </li>
                            <li style="border-bottom: none;">
                                <label>常驻城市:</label>
                                <input type="text" name="city" id="city" value="" class="input" readonly="readonly" /> <span>></span>
                            </li>
                            <li>
                                <label>居住时间:</label>
                                <div class="select">半年内</div>
                                <div class="select">超半年</div>
                                <input type="hidden" name="livetime" id="livetime" value="" />
                            </li>
                        </ul>
                        <div class="ppp ppa">
                            <p>
                                <input name="privacy_ywx_b" type="checkbox" value="1" id="privacy_ywx_b" />同意领取公交意外赠险并授权将本人相关信息转给（招商信诺人寿保险有限公司）以提供后续保险服务</p>
                        </div>
                    </form>
                    <div class="loan_button" data="step_2">下一步</div>
                </div>
            </section>
            <!--第三步-->
            <!--下一步-->
            <section class="loan_data" style="display: none;" id="step_3">
                <header>
                        <h2>贷款人资料</h2>
 
                </header>
                <form action="" method="post">
                    <ul class="main">
                        <li>
                            <label>出生年月:</label>
                            <input type="text" name="birthday" id="birthday" value="1980-10-10" class="input" readonly="readonly" />  <span>></span>
                        </li>
                        <li>
                            <label>性 别:</label>
                            <div class="sex">男</div>
                            <div style="width: 15px;"></div>
                            <div class="sex">女</div>
                            <input type="hidden" name="sex" id="sex" value="" />
                        </li>
                        <li>
                            <label>雇佣情况:</label>
                            <div class="select">工薪族</div>
                            <div class="select">公务员</div>
                            <div class="select">个体户</div>
                            <div class="select">企业主</div>
                            <input type="hidden" name="hire" id="hire" value="" />
                        </li>
                        <li class="hide_qiye">
                            <label>营业执照:</label>
                            <div class="select">有</div>
                            <div class="select">无</div>
                            <input type="hidden" name="zhizhao" id="zhizhao" value="" />
                        </li>
                        <li class="hide_qiye">
                            <label>经营时间:</label>
                            <div class="select">半年内</div>
                            <div class="select">6-12月</div>
                            <div class="select">1年以上</div>
                            <input type="hidden" name="opentime" id="opentime" value="" />
                        </li>
                        <li class="hide_qiye">
                            <label>年营业额:</label>
                            <div class="select">10万内</div>
                            <div class="select">10-20万</div>
                            <div class="select">20万以上</div>
                            <input type="hidden" name="turnover" id="turnover" value="" />
                        </li>
                        <li>
                            <label>发薪方式:</label>
                            <div class="select">公账发</div>
                            <div class="select">私帐转</div>
                            <div class="select">发现金</div>
                            <input type="hidden" name="payway" id="payway" value="" />
                        </li>
                        <li>
                            <label>公积金:</label>
                            <div class="select">无公积金</div>
                            <div class="select">半年内</div>
                            <div class="select">超半年</div>
                            <input type="hidden" name="EPFTime" id="EPFTime" value="" />
                        </li>
                        <li>
                            <label>社保年限:</label>
                            <div class="select">无社保</div>
                            <div class="select">半年内</div>
                            <div class="select">超半年</div>
                            <input type="hidden" name="shebao" id="shebao" value="" />
                        </li>
                        <li>
                            <label>贷款服务:</label>
                            <div class="select" style="position: relative;">同意系统推荐 <i style="font-size: 11px; color: #ccc;">（更易放款）</i> 
                                <img id="pro_con" src="/static/img/loan_pro.png" />
                            </div>
                            <input type="hidden" name="fuwu" id="fuwu" value="" />
                        </li>
                    </ul>
                </form>
                <div class="loan_button" data="step_3">贷款申请</div>
            </section>
            <style type="text/css">
                #pro_con {
                    position: absolute;
                    cursor: help;
                    top: 25%;
                    right: 15px;
                    width: 20px !important;
                    height: 20px;
                }
                .pro_info_box {
                    display: none;
                    position: fixed;
                    top: 20%;
                    width: 280px;
                    background: #fff;
                    z-index: 999999;
                    border-radius: 5px;
                    padding-bottom: 20px;
                }
                .pro_info_box p {
                    margin: 10px 20px;
                    line-height: 25px;
                    font-size: 14px;
                    color: #666666;
                }
                .pro_info_box h2 {
                    text-align: center;
                    font-size: 18px;
                    margin: 20px;
                    color: #FFAA00;
                }
                .pro_info_box span {
                    display: block;
                    cursor: pointer;
                    margin: auto;
                    width: 80%;
                    color: #666666;
                    height: 35px;
                    line-height: 35px;
                    border: 1px solid #CCCCCC;
                    border-radius: 5px;
                    text-align: center;
                    font-size: 16px;
                    color: #666666;
                    text-align: center;
                    margin-top: 15px;
                }
            </style>
            <div class="pro_info_box">
                    <h2>目前可做贷款公司如下</h2>
 
                <p>平安普惠、宜信、中腾信、恒昌、宜人贷、拍拍贷、助贷网，以及其它壹财道签署了合作协议的产品及产品授权方。</p>
                <p>确认本选项则意味着您同意壹财道为您分配信贷服务机构，而不仅限于某一家。</p>  <span id="close_pro">关闭</span>
 
            </div>
            <script type="text/javascript">
                show_size_pro()
                 $(window).resize(function() {
                    show_size_pro()
                });
 
                function show_size_pro() {
                    //获取遮罩层宽度
                    var zzw = document.body.clientWidth;
                    var ww = $(document).width();
                    var c_w = (ww - 280) / 2;
                    $(".pro_info_box").css("left", c_w);
                }
                $("#close_pro").click(function() {
                    $(".zhezhao,.pro_info_box").hide()
                })
 
                 $("#pro_con").click(function() {
                    $(".zhezhao,.pro_info_box").show()
                })
            </script>
            <!--验证手机号-->
            <section class="loan_data" id="step_4" style="display: none;">
                <div class="check_phone" style="margin-bottom: 15px;">
                    <header>
                        <div class="phone_tips">
                            <p>为提升贷款成功率</p>
                            <p>请补充身份证并验证您的手机号</p>
                        </div>
                    </header>
                    <div>
                        <ul class="main">
                            <li>
                                <label>身份证：</label>
                                <input type="text" name="id_card" id="id_card" value="" placeholder="请输入身份证号码" class="input" maxlength="18" />
                            </li>
                            <li class='sn_code_li'>
                                <label>手机号：</label>
                                <input type="text" name="re_phone" id="re_phone" value="18563953502" placeholder="请输入正确的手机号码" class="input" maxlength="11" /> <span>(可修改)</span>
 
                            </li>
                            <li class='sn_code_li'>
                                <label>验证码:</label>
                                <input type="text" name="sn_code" id="sn_code" class="input" value="" placeholder="请输入手机验证码" class="input" maxlength="4" />
                                <div class='get_sn_code'>
                                    <button id="get_password" class="btn-green">获取验证码</button>
                                </div>
                            </li>
                        </ul>
                        <div class="new_btn loan_button" data="step_4" act="true">确认提交</div>
                        <div class="new_btn loan_button" data="step_4" act="false" style="background: #C22103; display: none;">手机无误，跳过验证码提交</div>
                    </div>
                </div>
                <style type="text/css">
                    .zx_box {
                        margin: 0 20px;
                        color: #666;
                        font-size: 13px;
                        line-height: 25px;
                    }
                    #remove_btn {
                        color: coral;
                    }
                    .zx_box p:nth-of-type(1) {
                        text-indent: 2em;
                    }
                    .open_txt {
                        height: 20px;
                        display: block;
                        margin: 10px auto;
                        width: 20px;
                        cursor: pointer;
                    }
                    .tf {
                        transform: rotate(180deg);
                    }
                    .txt_box {
                        display: none;
                    }
                </style>
                <div class="zx_box">
                    <p>恭喜您：根据您的条件，系统已经给您推荐了一份最高百万保障额度，将会由保险公司工作人员联系您！</p>
                    <p>如您不同意请点击 ：<a href="javascript:void(0);" id="remove_btn">取消赠险</a>
 
                    </p>
                    <p>【投保须知】</p>
                    <p>1）投保规定：本保险身故受益人为法定受益人。本保险的保险对象为25-50周岁身体健康、能正常工作或正常生活的自然人。</p>
                    <p>2）保险限制：每一客户受赠保险以1份为限；本赠险产品仅限被保人本人领取。</p>
                    <p>3）保险期限：本保险的保障期限为（6）个月，以保单载明的保险起止日期为准。对保险起至日期之外所发生的保险事故本公司不负给付保险金责任。</p>
                    <p>4）告知义务：依据我国《保险法》的规定，投保人、被保险人应如实告知，否则保险人有权依法解除保险合同，并对于保险合同解除前发生的保险事故不负任何责任。投保人、被保险人在投保时，应对投保书内各项内容如实详细地说明或填写清楚。否则，保险人有权依法解除保险合同。</p>
                    <p>5）保险凭证：本产品仅提供电子保单。仅限赠送。保单经审核生效后客户会自动收到短信通知，请将短信保存并将短信上的电子保单号记录在适当的位置，以方便查询及理赔。您也可以通过拨打电话：95511或登陆http://www.pingan.com/property_insurance/pa18AutoInquiry/ 查询您的保单信息。</p>
                    <p>6）本保险不接受撤保、退保、加保及被保险人更换。</p>
                    <p>7）保险金申请：发生保险事故后，请被保险人或受益人及时凭电子保险单号及身份信息向保险公司报案，并提供相关证明和资料，保险公司将尽快按照有关条款履行给付责任。</p>
                    <p>其它保险（如您未能符合平安保险需求或者已经获得过平安保险赠险将不会为您重复赠送，我们可为您推荐其他保险公司的保险产品：赠送的保障将由招商信诺、泰康人寿、中英人寿、太平人寿、中美大都会、华夏保险等保险公司送出。保障产品及说明如下，我司会根据所保障产品的情况自行调整，不另做说明）</p>   <span class="open_txt"><img src="/static/images/loan/open.png" /></span>
 
                    <div class="txt_box">
                        <p>1.投保对象：本保险身故受益人为法定受益人。本保险的保险对象为22-50周岁身体健康、能正常工作或正常生活的自然人</p>
                        <p>2.保险期限：以被保险人收到短信通知上的保险起止日期为准。对保险日期之外所发生的保障事故保险公司不负责给付保险责任</p>
                        <p>3.使用条款：详情内容请登录保险公司官网网站查询</p>
                        <p>4.告知义务：依我国《保险法》的规定，投保人、被保险人应如实告知，否则保险人有权依法解除保险合同，并对于保险合同解除前发生的保险事故不负任何责任。 投保人、被保险人在投保时，应对投保书内各项内容如实详细的说明或填写清楚。否则，保险人有权依法解除保险合同。</p>
                        <p>5.保险判定流程：根据用户所填写的资料后台自动匹配最适合用户的保险为期进行投保；</p>
                        <p>6.保险提供商：</p>
                        <p>相关保险由泰康人寿（保险名称：铁定保；保障内容：火车；保障时长：1年；最高保额：50万元；适用年龄：25-50周岁）、中国平安（保险名称：畅行神州保障；保障内容：飞机/轨道交通/商业运营汽车；飞机意外身故、残疾保额为100万元/人，火车意外身故、残疾保额为50万元/人，汽车意外身故残疾保额为10万元/人；适用年龄：25-50周岁）、</p>
                        <p>太平人寿（保险名称：太平无忧航空意外伤害险；保障内容：飞机；保障时长：90天；最高保额：8万元；适用年龄：22-48周岁）;</p>
                        <p>中英人寿（保险名称：交通意外伤害保险C款；保障内容：飞机/轨道交通/汽车/非营运汽车；保障时长：90天；最高保额：20万元；适用年龄：25-48周岁）、阳光人寿（保险名称：阳光出行无忧；保障内容：飞机意外、轮船意外/轨道车辆意外/汽车意外；保障时长：30天；最高保额：10万元；适用年龄：25-50周岁）;</p>
                        <p>大都会保险（保险名称：大都会交通综合保障；保障内容：飞机意外身故/残疾、水陆公交意外身故/残疾、自驾（驾驶＋乘坐）意外身故/残疾；保障时长：30天；最高保额：100万元；适用年龄：25-50周岁）;</p>
                        <p>华夏保险（保险名称：华夏自由行公共交通意外伤害保险；保障内容：飞机意外、公共汽车/出租车意外、地铁/火车/轻轨/轮船；保障时长：90天；最高保额：5万元；适用年龄：25-50周岁）提供，</p>
                        <p>招商信诺公共交通意外保障（北京市、上海市、重庆市、湖北省、四川省、江苏省、浙江省、山东省、广东省、辽宁省、陕西省、湖南省、河南省、江西省客户可获得B款赠险）保障内容：保额3万，覆盖境内外海陆空公共交通出行。保险生效领取次日零时起生效，保障期限90天。 招商信诺：保险条款链接：http://m.cignacmb.com/campaign/mc/free/pdf/xlb.pdf以上内容仅体现产品核心内容供销售参考并非完整条款，完整条款以及限制请咨询官方客服。7.保险凭证：险公司对保险仅提供电子保单；赠送保险每人只能投保一份，重复或投保多分无效。8.如对本活动有疑问及建议请拨打4008230011转0号线进行咨询（咨询时间:工作日9:30至18:00）；
                            如对赠险内容及理赔有疑问请拨打相关保险公司客服电话〔致电客服热线泰康人寿【95522】、中国平安【95511】、太平人寿【95589】、中英人寿【950951】〕、阳光人寿【95510】、华夏人寿【95300】、中美大都会【95308】、招商信诺【95362】进行咨询。</p>
                    </div>
                </div>
                <script type="text/javascript">
                    $(".open_txt").click(function() {
                        if ($(this).hasClass("tf")) {
                            $(this).removeClass("tf");
                            $(".txt_box").fadeOut();
                        } else {
                            $(this).addClass("tf");
                            $(".txt_box").fadeIn();
                        }
                    })
                </script>
            </section>
            <script type="text/javascript">
                var time = 60,
                    htm = "",
                    i = "";
                 //动态密码倒数效果
 
                function timer() {
 
                    setTimeout(function() {
                        i = time - 1;
                        time = i;
                        htm = i + "秒后可重发";
                        if (i == -1) {
                            clearTimeout(timer);
                            $("#get_password").removeClass("btn-disable");
                            $("#get_password").html("获取动态密码");
                            time = 60;
                            $(".new_btn").eq(1).show();
                            return;
                        } else {
                            $("#get_password").html(htm);
                            timer();
                        }
                    }, 1000);
                }
              //获取动态密码
        		$("#get_password").bind("click",function(){
        			
        			if($(this).hasClass("btn-disable")){
        				return false;
        			}
        			var UserTelVal = $.trim($("#re_phone").val());
        				reData = {"mobile":UserTelVal,"Types":"loanCode","Name":$("#name").val(),"loanID":id};
        				if(!UserTelVal){
        					box("请输入手机号码");
        					return false;
        				}
        				var myreg = /^(1[3|4|5|7|8][0-9])\d{8}$/; //手机号
        				if( !myreg.test(UserTelVal) ) { box('请输入有效手机号码'); return false; } 

        				//$(this).attr("data-opt","0");
        				$(this).addClass("btn-disable");
        				$.ajax({
        					type:"POST",
        					url:"/api/sms/creditCard",
        					dataType:"json",
        					data: reData,
        					success:function(data){
        							box(data.msg);
        					}
        				});
        				timer();
        				return false;
        		});
        		
        		//取消保险
        		$("#remove_btn").click(function(){
        		    if(confirm("是否确定取消保险？"))
        		    {
        				$.ajax({  
        				    	url:'/daikuan/up_bx?rand='+Math.random(),
        			 			type:'post', 
        						dataType:'json', 
        			 			success:function(data) {  
        		 					box(data.msg);
        			 			}
        			 		})
        		     } 
        		})
                        
            </script>
            <!--保险-->
            <!--信用卡-->
            <!--申请成功-->
            <!--底部-->
            <input name="LMAgentID" id="LMAgentID" type="hidden" value="" />
            <input name="LMMemberID" id="LMMemberID" type="hidden" value="" />
            <input name="c" id="c" value="" type="hidden">
            <input name="s" id="s" value="" type="hidden">
            <input name="p" id="p" value="" type="hidden">
            <input name="a" id="a" value="" type="hidden">
            <input name="v" id="v" value="" type="hidden">
            <input name="proid" id="proid" value="" type="hidden">
            <input name="wcid" id="wcid" value="" type="hidden">
            <input name="wxnid" id="wxnid" value="" type="hidden">
            <input name="AMID" id="AMID" value="" type="hidden">
            <input name="ANID" id="ANID" value="" type="hidden">
            <input name="ADID" id="ADID" value="" type="hidden">
            <input name="isSendsms" class='isSendsms' value="0" type="hidden" />
            <div class="loading" style="display: none;">loading...</div>
            <div class="risk_tips">
                    <h2>本平台郑重承诺：</h2>
 
                <p style="text-indent: 2em;">所有贷款申请在未成功放款前不收取任何费用，且不承诺一定可放贷成功，谨防电信诈骗，请警惕需要您付款的来电。</p>
            </div>
            <footer>
                <div class="foot_box">
                        <h3>放贷流程</h3>
 
                    <div class="process">
                        <div class="icon">
                            <img src="http://img.yicaidao.cn/static/images/loan/apply.png" alt="在线申请" />
                        </div>
                        <div class="icon">
                            <img src="http://img.yicaidao.cn/static/images/loan/customer.png" alt="客服协助" />
                        </div>
                        <div class="icon">
                            <img src="http://img.yicaidao.cn/static/images/loan/sub.png" alt="提交资料" />
                        </div>
                        <div class="icon">
                            <img src="http://img.yicaidao.cn/static/images/loan/bank.png" alt="银行放贷" />
                        </div>
                    </div>
                </div>
            </footer>
            <div class="copy">深圳市壹财道信息咨询有限公司版权所有
                <br />粤ICP备13088114号</div>
            <!--错误提示对话框-->
            <div class="zhezhao"></div>
            <div class="layer" id="layer"></div>
            <div class="show_city_tips">
                    <h2>
                请确认您的常驻城市？<span id="close_city">x</span>
            </h2>
 
                <p id="check_city">深圳</p>
                <p id="phone_city">内蒙古</p>
                <p id="other_city">其它城市</p>
            </div>
            <!--城市选择框-->
            <section class="city_box" style="display: none;">
                    <h3>当前</h3>
 
                <div class="myposition">  <b style="margin-bottom: 0;" id="dw_city"></b>
 
                </div>
                    <h3>热门城市</h3>
 
                <div class="rm_city"></div>
                <!--字母排序开始-->
                <div class="letter"></div>
            </section>
            <script type="text/javascript">
                var LMAgentID = "";
                if (LMAgentID != "") {
                    $("input[type='checkbox']").prop("checked", "checked");
                }
            </script>
        </div>
        <!--隐私条款-->
    </body>
 
</html>