<!DOCTYPE html>
<!--[if IE 8]>
<html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]>
<html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html class="no-js">
<!--<![endif]-->

<?php require  TPL_PATH.'template_c/common/head.tpl.php';?>

<body>

<!-- start: HEADER -->
<?php require  TPL_PATH.'template_c/common/header.tpl.php';?>
<!-- end: HEADER -->
<!-- start: MAIN CONTAINER -->
<div class="main-container">
    <div class="navbar-content">
        <!-- start: SIDEBAR -->
      	<?php require  TPL_PATH.'template_c/common/sidebar.tpl.php';?>
        <!-- end: SIDEBAR -->
    </div>

    <!-- start: PAGE -->
    <div class="main-content">
        <!-- start: PANEL CONFIGURATION MODAL FORM -->

        <!-- /.modal -->
        <!-- end: SPANEL CONFIGURATION MODAL FORM -->
        <div class="container">
            <!-- start: PAGE HEADER -->
            <div class="row">
                <div class="col-sm-12">
                    <!-- start: STYLE SELECTOR BOX -->
                    <!-- end: STYLE SELECTOR BOX -->
                    <!-- start: PAGE TITLE & BREADCRUMB -->
                    <ol class="breadcrumb">
                        <li>
                            <i class="clip-user-5"></i>
                            <a href="./">
                                渠道商
                            </a>
                        </li>
                        <li class="active">
                            渠道资源管理
                        </li>
                    </ol>
                    <div class="page-header">
                        <h1> 渠道资源管理
                            <small>overview &amp; stats</small>
                        </h1>
                    </div>
                    <!-- end: PAGE TITLE & BREADCRUMB -->
                </div>
            </div>
            <!-- end: PAGE HEADER -->
            <!-- start: PAGE CONTENT -->
            <div class="row">
                <div class="col-md-12 space20">
                    <a href="/channel/channel_resource_manage/add_channel_resource">
                        <button class="btn btn-green add-row">
                            添加渠道 <i class="fa fa-plus"></i>
                        </button>
                    </a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <!-- start: BASIC TABLE PANEL -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-external-link-square"></i>资源列表
                            <div class="panel-tools">
                            </div>
                        </div>
                        <div class="panel-body">
                            <table class="table table-hover" id="sample-table-1">
                                <thead>
                                    <tr>
                                        <th class="center">#</th>
                                        <th>资源标题</th>
                                        <th>资源类型</th>
                                        <th>计费价格</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php if ($data_list!=null){
                                    foreach ($data_list as $data) {?>
                                    <tr>
                                    <td class="center"><?php echo $data['resource_id']?></td>
                                    <td ><?php echo $data['resource_name']?></td>
                                    <td class="hidden-xs">
                                    <?php echo $data['resource_type'] ?>
                                    </td>
                                    <td>
                                    <?php 
                                        if ($data['support_cpm']==1) {
                                            $price =number_format($data['cpm_price'],2);
                                            echo "cpm - ".$price." 元/千次展示<br />";
                                        }
                                        if ($data['support_cpc']==1) {
                                            $price =number_format($data['cpc_price'],2);
                                            echo "cpc - ".$price." 元/点击<br />";
                                        }
                                        if ($data['support_cpa']==1) {
                                            $price =number_format($data['cpa_price'],2);
                                            echo "cpa - ".$price." 元/用户<br />";
                                        }
                                        if ($data['support_cpt']==1) {
                                            $price =number_format($data['cpt_price'],2);
                                            echo "cpt - ".$price." 元/小时<br />";
                                        }
                                    ?>
                                    </td>
                                    
                                    <td class="center">
                                        <div class="visible-md visible-lg hidden-sm hidden-xs">
                                            <a href="/channel/channel_resource_manage/query_channel_resource?resource_id=<?php echo $data['resource_id'];?>" class="btn btn-xs btn-primary tooltips" data-placement="top"
                                               data-original-title="查看"><i class="fa clip-note"></i></a>
                                            <a href="/channel/channel_resource_manage/edit_channel_resource?resource_id=<?php echo $data['resource_id'];?>" class="btn btn-xs btn-teal tooltips" data-placement="top"
                                               data-original-title="编辑"><i class="fa fa-edit"></i></a>
                                            <a href="/channel/channel_resource_manage/delete_channel_resource?resource_id=<?php echo $data['resource_id'];?>" class="btn btn-xs btn-bricky tooltips" data-placement="top"
                                               data-original-title="删除"><i class="fa fa-times fa fa-white"></i></a>
                                        </div>
                                    </td>
                                </tr>
                                <?php 
                                    }
                                }
                                ?>
                                
                                
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- end: BASIC TABLE PANEL -->
                </div>
            </div>
				<div>
					<ul class="pagination ">
					<?php echo $page_link?>
					</ul>
				</div>
				<!-- end: PAGE CONTENT-->
        </div>
    </div>
    <!-- end: PAGE -->
</div>
<!-- end: MAIN CONTAINER -->
<!-- start: FOOTER -->
<?php require  TPL_PATH.'template_c/common/footer.tpl.php';?>
<!-- end: FOOTER -->
<!-- start: RIGHT SIDEBAR -->

<!-- end: RIGHT SIDEBAR -->
<!-- start: MAIN JAVASCRIPTS -->
<!--[if lt IE 9]>
<script src="/static/bower_components/respond/dest/respond.min.js"></script>
<script src="/static/bower_components/Flot/excanvas.min.js"></script>
<script src="/static/bower_components/jquery-1.x/dist/jquery.min.js"></script>
<![endif]-->
<!--[if gte IE 9]><!-->
<script type="text/javascript" src="/static/bower_components/jquery/dist/jquery.min.js"></script>
<!--<![endif]-->
<script type="text/javascript" src="/static/bower_components/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="/static/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript"
        src="/static/bower_components/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
<script type="text/javascript" src="/static/bower_components/blockUI/jquery.blockUI.js"></script>
<script type="text/javascript" src="/static/bower_components/iCheck/icheck.min.js"></script>
<script type="text/javascript"
        src="/static/bower_components/perfect-scrollbar/js/min/perfect-scrollbar.jquery.min.js"></script>
<script type="text/javascript" src="/static/bower_components/jquery.cookie/jquery.cookie.js"></script>
<script type="text/javascript" src="/static/bower_components/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript" src="/static/assets/js/min/main.min.js"></script>
<!-- end: MAIN JAVASCRIPTS -->

<script>
    jQuery(document).ready(function () {
        Main.init();
    });
</script>

</body>

</html>