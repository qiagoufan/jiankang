<!DOCTYPE html>
<!--[if IE 8]>
<html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]>
<html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <?php require TPL_PATH . 'template_c/common/head.tpl.php'; ?>
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    <link
            href="/static/bower_components/jquery.tagsinput/dist/jquery.tagsinput.min.css"
            rel="stylesheet"/>
    <link
            href="/static/bower_components/bootstrap-fileinput/css/fileinput.min.css"
            rel="stylesheet"/>
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->

</head>

<body>

<!-- start: HEADER -->
<?php require TPL_PATH . 'template_c/common/header.tpl.php'; ?>
<!-- end: HEADER -->
<!-- start: MAIN CONTAINER -->
<div class="main-container">
    <div class="navbar-content">
        <!-- start: SIDEBAR -->
        <?php require TPL_PATH . 'template_c/common/sidebar.tpl.php'; ?>
        <!-- end: SIDEBAR -->
    </div>

    <!-- start: PAGE -->
    <div class="main-content">
        <div class="container">
            <!-- start: PAGE HEADER -->
            <div class="row">
                <div class="col-sm-12">
                    <!-- start: STYLE SELECTOR BOX -->
                    <!-- end: STYLE SELECTOR BOX -->
                    <!-- start: PAGE TITLE & BREADCRUMB -->
                    <ol class="breadcrumb">
                        <li><i class="clip-user-5"></i> <a href="./"> 渠道商 </a></li>
                        <li class="active">渠道资源管理</li>
                    </ol>
                    <div class="page-header">
                        <h1>
                            编辑渠道资源
                            <small>overview &amp; stats</small>
                        </h1>
                    </div>
                    <!-- end: PAGE TITLE & BREADCRUMB -->
                </div>
            </div>
            <!-- end: PAGE HEADER -->
            <!-- start: PAGE CONTENT -->
            <div class="row">
                <div class="col-sm-12">
                    <!-- start: TEXT FIELDS PANEL -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-external-link-square"></i> 编辑渠道资源 『 <?php echo $info['resource_name']; ?> 』
                        </div>
                        <div class="panel-body">
                            <form role="form" class="form-horizontal"
                                  action="/channel/channel_resource_manage/update_channel_resource?resource_id=<?php echo $info['resource_id'] ?>"
                                  id="resource_form" method="post" enctype="multipart/form-data">
                                <div class="errorHandler alert alert-danger no-display">
                                    <i class="fa fa-times-sign"></i> 出现了一些问题,请检查后再提交
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="resource_name">
                                        资源名称<span class="symbol required"></span>
                                    </label>
                                    <div class="col-sm-4">
                                        <input type="text" placeholder="" id="resource_name" name="resource_name"
                                               class="form-control" value="<?php echo $info['resource_name']; ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="resource_type">
                                        资源类型<span class="symbol required"></span>
                                    </label>
                                    <div class="col-sm-4">
                                        <input type="text" placeholder="" id="resource_type" name="resource_type"
                                               class="form-control" value="<?php echo $info['resource_type']; ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="resource_desc">
                                        资源位简介
                                    </label>
                                    <div class="col-sm-8">
                                        <input type="text" placeholder="" id="resource_desc" name="resource_desc"
                                               class="form-control" value="<?php echo $info['resource_desc']; ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="resource_desc">
                                        计费方式<span class="symbol required"></span>
                                    </label>
                                    <div class="col-sm-8">
                                        <div class="col-sm-2">
                                            <label class="checkbox-inline">
                                                <input type="checkbox" class="red billing_type" name="billing_type[]"
                                                       id='billing_type_cpm'
                                                       value="cpm" <?php echo $info['support_cpm'] == 1 ? 'checked="checked"' : ""; ?>
                                                >
                                                支持CPM
                                            </label>
                                        </div>
                                        <div class="col-sm-2">
                                            <label class="checkbox-inline">
                                                <input type="checkbox" class="green billing_type" name="billing_type[]"
                                                       id='billing_type_cpc'
                                                       value="cpc" <?php echo $info['support_cpc'] == 1 ? 'checked="checked"' : ""; ?>>
                                                支持CPC
                                            </label>
                                        </div>
                                        <div class="col-sm-2">
                                            <label class="checkbox-inline">
                                                <input type="checkbox" class="purple billing_type" name="billing_type[]"
                                                       id='billing_type_cpa'
                                                       value="cpa" <?php echo $info['support_cpa'] == 1 ? 'checked="checked"' : ""; ?>>
                                                支持CPA
                                            </label>
                                        </div>
                                        <div class="col-sm-2">
                                            <label class="checkbox-inline">
                                                <input type="checkbox" class="orange billing_type" name="billing_type[]"
                                                       id='billing_type_cpt'
                                                       value="cpt" <?php echo $info['support_cpt'] == 1 ? 'checked="checked"' : ""; ?>>
                                                支持CPT
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="resource_desc">
                                        计费价格<span class="symbol required"></span>
                                    </label>
                                    <div class="col-sm-2">
                                        <label for="form-field-mask-5"> CPM价格 (元/千次展示)
                                        </label>
                                        <div>
                                            <input type="text" id="cpm_price" name="cpm_price"
                                                   class="form-control currency"
                                                   value="<?php echo number_format($info['cpm_price'], 2) ?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <label for="form-field-mask-5"> CPC价格 (元/点击)
                                        </label>
                                        <div>
                                            <input type="text" id="cpc_price" name="cpc_price"
                                                   class="form-control currency"
                                                   value="<?php echo number_format($info['cpc_price'], 2) ?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <label for="form-field-mask-5"> CPA价格 (元/用户)
                                        </label>
                                        <div>
                                            <input type="text" id="cpa_price" name="cpa_price"
                                                   class="form-control currency"
                                                   value="<?php echo number_format($info['cpa_price'], 2) ?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-2">
                                        <label for="form-field-mask-5"> CPT价格 (元/小时)
                                        </label>
                                        <div>
                                            <input type="text" id="cpt_price" name="cpt_price"
                                                   class="form-control currency"
                                                   value="<?php echo number_format($info['cpt_price'], 2) ?>">
                                        </div>
                                    </div>
                                </div>
                                <?php if ($info['resource_position_img_url'] != "") { ?>
                                    <div class="form-group" id="old_img">
                                        <label class="col-sm-2 control-label"
                                               for="advertisement_img_fileinput">当前图片 </label>
                                        <div class="form-group">
                                            <div class="col-sm-4">
                                                <br>
                                                <img src="<?php echo $info['resource_position_img_url']; ?>"
                                                     style="max-height: 200px;max-width: 300px;">
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"
                                           for="resource_position_img_fileinput">资源位示意图 </label>
                                    <div class="form-group">
                                        <div class="col-sm-4">
                                            <input id="resource_position_img_fileinput" type="file" class="file">
                                            <input type="hidden" name="resource_position_img"  value="<?php echo $info ['resource_position_img'];?>"  id="resource_position_img">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <span class="col-md-2 "></span>
                                    <div class="col-md-2">
                                        <button class="btn btn-yellow btn-block" type="submit" id="submit_btn">
                                            提交 <i class="fa fa-arrow-circle-right"></i>
                                        </button>
                                    </div>
                                    <div class="col-md-2">
                                        <button class="btn btn btn-light-grey btn-block" type="button"
                                                onclick="history.back();">
                                            返回 <i class="fa fa-close"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- end: TEXT FIELDS PANEL -->
                </div>
            </div>

            <!-- end: PAGE CONTENT-->
        </div>
    </div>
    <!-- end: PAGE -->
</div>
<!-- end: MAIN CONTAINER -->
<!-- start: FOOTER -->
<div class="footer clearfix">
    <div class="footer-inner">
        <script>
            document.write(new Date().getFullYear())
        </script>
        &copy; clip-one by cliptheme.
    </div>
    <div class="footer-items">
        <span class="go-top"><i class="clip-chevron-up"></i></span>
    </div>
</div>
<!-- end: FOOTER -->

<!-- start: RIGHT SIDEBAR -->
<!-- end: RIGHT SIDEBAR -->
<!-- start: MAIN JAVASCRIPTS -->
<!--[if lt IE 9]>
<script src="/static/bower_components/respond/dest/respond.min.js"></script>
<script src="/static/bower_components/Flot/excanvas.min.js"></script>
<script src="/static/bower_components/jquery-1.x/dist/jquery.min.js"></script>
<![endif]-->
<!--[if gte IE 9]><!-->
<script type="text/javascript"
        src="/static/bower_components/jquery/dist/jquery.min.js"></script>
<!--<![endif]-->
<script type="text/javascript"
        src="/static/bower_components/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript"
        src="/static/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript"
        src="/static/bower_components/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
<script type="text/javascript"
        src="/static/bower_components/blockUI/jquery.blockUI.js"></script>
<script type="text/javascript"
        src="/static/bower_components/iCheck/icheck.min.js"></script>
<script type="text/javascript"
        src="/static/bower_components/perfect-scrollbar/js/min/perfect-scrollbar.jquery.min.js"></script>
<script type="text/javascript"
        src="/static/bower_components/jquery.cookie/jquery.cookie.js"></script>
<script type="text/javascript"
        src="/static/bower_components/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript" src="/static/assets/js/min/main.min.js"></script>
<!-- end: MAIN JAVASCRIPTS -->
<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script src="/static/bower_components/moment/min/moment.min.js"></script>
<script
        src="/static/bower_components/bootstrap-maxlength/src/bootstrap-maxlength.js"></script>
<script src="/static/bower_components/autosize/dist/autosize.min.js"></script>
<script src="/static/bower_components/select2/dist/js/select2.min.js"></script>
<script
        src="/static/bower_components/jquery.maskedinput/dist/jquery.maskedinput.min.js"></script>
<script
        src="/static/bower_components/jquery-maskmoney/dist/jquery.maskMoney.min.js"></script>
<script
        src="/static/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script
        src="/static/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script
        src="/static/bower_components/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script
        src="/static/bower_components/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<script
        src="/static/bower_components/jquery.tagsinput/src/jquery.tagsinput.js"></script>
<script
        src="/static/bower_components/summernote/dist/summernote.min.js"></script>
<script
        src="/static/bower_components/bootstrap-fileinput/js/plugins/canvas-to-blob.min.js"></script>
<script
        src="/static/bower_components/bootstrap-fileinput/js/fileinput.js"></script>
<script src="/static/assets/js/form-elements.js"></script>

<script src="/static/bower_components/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="/static/bower_components/summernote/dist/summernote.min.js"></script>
<script src="/static/bower_components/jquery-ajaxform/jquery.form.min.js"></script>
<script src="/static/bower_components/jquery-validation/src/localization/messages_zh.js"></script>

<!--     表单验证 -->
<script src="/static/user/resource-form-validation.js"></script>
<script src="/static/user/ajax-submit.js"></script>
<script src="/static/bower_components/bootstrap-fileinput/js/fileinput_locale_zh.js"></script>
<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->

<script>
    $("#resource_position_img_fileinput").fileinput({
        uploadUrl: '/common/upload/upload_img',
        dropZoneEnabled: false,
        uploadAsync: true,
        allowedFileExtensions: ['gif', 'jpg', 'png'],
        language: 'zh',
        maxFileSize: 2000,//单位为kb，如果为0表示不限制文件大小

    });

    $("#resource_position_img_fileinput").on("fileuploaded", function (event, data) {
        if (data.response.result == false) {
            alert(data.response.message);
            return false;
        }
        $("#resource_position_img").val(data.response.file_path);
		$("#old_img").hide();
    });
    jQuery(document).ready(function () {

        Main.init();
        ajaxSubmit.init();
        ResourceFormValidator.init();
        $(".currency").maskMoney();
    });
</script>

</body>

</html>