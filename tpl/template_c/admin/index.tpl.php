<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="http://yacht-server.oss-cn-zhangjiakou.aliyuncs.com/<?php echo $env?>/yacht-admin/<?php echo $admin_static_version?>/umi.css" />
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,minimum-scale=1,user-scalable=no" />
    <link rel="shortcut icon" type="image/x-icon" href="https://gw.alicdn.com/tfs/TB1k1_6qipE_u4jSZKbXXbCUVXa-50-50.svg">
    <title>鲁班大师-后台管理系统</title>
    <script>
        window.routerBase = "/admin/main/index";
        window.publicPath = 'http://yacht-server.oss-cn-zhangjiakou.aliyuncs.com/<?php echo $env?>/yacht-admin/<?php echo $admin_static_version?>/'
    </script>
</head>
<body>
<div id="root"></div>
<script src="http://yacht-server.oss-cn-zhangjiakou.aliyuncs.com/<?php echo $env?>/yacht-admin/<?php echo $admin_static_version?>/umi.js"></script>
</body>
</html>