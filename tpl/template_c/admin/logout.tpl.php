
<!DOCTYPE html>
<!--[if IE 8]><html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html class="no-js">
<!--<![endif]-->
<?php require  TPL_PATH.'template_c/common/head.tpl.php';?>

<body class="error-full-page rain-page">


    <!-- start: PAGE -->
    <div class="container">
        <div class="row">
            <!-- start: 404 -->
            <div class="col-sm-12 page-error">
                <div class="error-details col-sm-6 col-sm-offset-3">
                    <h3>退出成功</h3>
                    <p>
                        <a href="/main/index/main" class="btn btn-teal btn-return">
                            返回主页
                        </a>
                    </p>
                </div>
            </div>
            <!-- end: 404 -->
        </div>
    </div>
    <!-- end: PAGE -->
    <!-- start: MAIN JAVASCRIPTS -->
    <!--[if lt IE 9]>
            <script src="../../bower_components/respond/dest/respond.min.js"></script>
            <script src="../../bower_components/Flot/excanvas.min.js"></script>
            <script src="../../bower_components/jquery-1.x/dist/jquery.min.js"></script>
            <![endif]-->

    <!--[if gte IE 9]><!-->
    <script type="text/javascript" src="../../bower_components/jquery/dist/jquery.min.js"></script>
    <!--<![endif]-->
    <script type="text/javascript" src="../../bower_components/jquery-ui/jquery-ui.min.js"></script>
    <script type="text/javascript" src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../../bower_components/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
    <script type="text/javascript" src="../../bower_components/blockUI/jquery.blockUI.js"></script>
    <script type="text/javascript" src="../../bower_components/iCheck/icheck.min.js"></script>
    <script type="text/javascript" src="../../bower_components/perfect-scrollbar/js/min/perfect-scrollbar.jquery.min.js"></script>
    <script type="text/javascript" src="../../bower_components/jquery.cookie/jquery.cookie.js"></script>
    <script type="text/javascript" src="../../bower_components/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript" src="assets/js/min/main.min.js"></script>
    <!-- end: MAIN JAVASCRIPTS -->
    <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
    <script src="../../bower_components/rainyday.js/dist/rainyday.min.js"></script>
    <script src="assets/js/utility-error404.js"></script>
    <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->

    <script>
        jQuery(document).ready(function() {
            Main.init();
            Error404.init();
        });
    </script>

</body>

</html>