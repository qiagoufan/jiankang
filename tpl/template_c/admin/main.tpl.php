<!DOCTYPE html>
<!--[if IE 8]>
<html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]>
<html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html class="no-js">
<!--<![endif]-->
<head>
<?php require  TPL_PATH.'template_c/common/head.tpl.php';?>
</head>
<body>

<!-- start: HEADER -->
<?php require  TPL_PATH.'template_c/common/header.tpl.php';?>
<!-- end: HEADER -->
<!-- start: MAIN CONTAINER -->
<div class="main-container">
    <div class="navbar-content">
        <!-- start: SIDEBAR -->
		<?php require  TPL_PATH.'template_c/common/sidebar.tpl.php';?>
        <!-- end: SIDEBAR -->
    </div>

    <!-- start: PAGE -->
    <div class="main-content">
        <!-- start: PANEL CONFIGURATION MODAL FORM -->
        <div class="modal fade" id="panel-config" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                            &times;
                        </button>
                        <h4 class="modal-title">Panel Configuration</h4>
                    </div>
                    <div class="modal-body">
                        Here will be a configuration form
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">
                            Close
                        </button>
                        <button type="button" class="btn btn-primary">
                            Save changes
                        </button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- end: SPANEL CONFIGURATION MODAL FORM -->
        <div class="container">
            <!-- start: PAGE HEADER -->
            <div class="row">
                <div class="col-sm-12">
                    <!-- start: STYLE SELECTOR BOX -->

                    <!-- end: STYLE SELECTOR BOX -->
                    <!-- start: PAGE TITLE & BREADCRUMB -->
                    <ol class="breadcrumb">
                        <li>
                            <i class="clip-home-3"></i>
                            <a href="#">
                                Home1
                            </a>
                        </li>
                        <li class="active">
                            Dashboard
                        </li>
                        <li class="search-box">
                            <form class="sidebar-search">
                                <div class="form-group">
                                    <input type="text" placeholder="Start Searching...">
                                    <button class="submit">
                                        <i class="clip-search-3"></i>
                                    </button>
                                </div>
                            </form>
                        </li>
                    </ol>
                    <div class="page-header">
                        <h1>Dashboard
                            <small>overview &amp; stats</small>
                        </h1>
                    </div>
                    <!-- end: PAGE TITLE & BREADCRUMB -->
                </div>
            </div>
            <!-- end: PAGE HEADER -->
            <!-- start: PAGE CONTENT -->

            <!-- end: PAGE CONTENT-->
        </div>
    </div>
    <!-- end: PAGE -->
</div>
<!-- end: MAIN CONTAINER -->
<!-- start: FOOTER -->
<?php require  TPL_PATH.'template_c/common/footer.tpl.php';?>
<!-- end: FOOTER -->
<!-- start: RIGHT SIDEBAR -->

<!-- end: RIGHT SIDEBAR -->
<!-- start: MAIN JAVASCRIPTS -->
<!--[if lt IE 9]>
<script src="/static/bower_components/respond/dest/respond.min.js"></script>
<script src="/static/bower_components/Flot/excanvas.min.js"></script>
<script src="/static/bower_components/jquery-1.x/dist/jquery.min.js"></script>
<![endif]-->
<!--[if gte IE 9]><!-->
<script type="text/javascript" src="/static/bower_components/jquery/dist/jquery.min.js"></script>
<!--<![endif]-->

<script type="text/javascript" src="/static/bower_components/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript" src="/static/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript"
        src="/static/bower_components/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
<script type="text/javascript" src="/static/bower_components/blockUI/jquery.blockUI.js"></script>
<script type="text/javascript" src="/static/bower_components/iCheck/icheck.min.js"></script>
<script type="text/javascript"
        src="/static/bower_components/perfect-scrollbar/js/min/perfect-scrollbar.jquery.min.js"></script>
<script type="text/javascript" src="/static/bower_components/jquery.cookie/jquery.cookie.js"></script>
<script type="text/javascript" src="/static/bower_components/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript" src="/static/assets/js/min/main.min.js"></script>
<!-- end: MAIN JAVASCRIPTS -->
<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script src="/static/bower_components/Flot/jquery.flot.js"></script>
<script src="/static/bower_components/Flot/jquery.flot.pie.js"></script>
<script src="/static/bower_components/Flot/jquery.flot.resize.js"></script>
<script src="/static/assets/plugin/jquery.sparkline.min.js"></script>
<script src="/static/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js"></script>
<script src="/static/bower_components/jqueryui-touch-punch/jquery.ui.touch-punch.min.js"></script>
<script src="/static/bower_components/moment/min/moment.min.js"></script>
<script src="/static/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
<script src="/static/assets/js/min/index.min.js"></script>
<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->

<script>
    jQuery(document).ready(function () {
        Main.init();
        Index.init();
    });
</script>

</body>

</html>