<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>维修报告</title>
</head>
<body>
<br/><br/>
<div><br/><br/><br/><br/><br/><br/><br/><br/>
    <h1 align="center"><?php echo $order_info->customer_info->company_name ?></h1>
    <h3 align="center">Air Conditioning Maintenance Report</h3>
    <br/>
    <br/>
    <h1 align="center"><?php echo $order_info->base_info->order_category_name ?></h1>
    <br/><br/><br/><br/><br/>
    <h2 align="center"><?php echo $order_info->customer_info->store_address ?></h2>
    <h3 align="center">
        Maintenance Date:<?php echo date("Y-m-d", $order_info->base_info->created_timestamp / 1000) ?></h3>
</div>
<pagebreak></pagebreak>
<div>
    <br/><br/><br/>
    <h1 style="color: red">报修情况：</h1>
    <br/>
    <h3 style="margin-left: 100px;"><?php echo $order_info->order_detail->customer_remark != "" ? $order_info->order_detail->customer_remark : '暂无描述'; ?></h3>
    <br/>
    <h1 style="color: red">维修过程：</h1>
    <br/>
    <h3 style="margin-left: 100px;"><?php echo $order_info->extra_info->repair_instructions != "" ? $order_info->extra_info->repair_instructions : '暂无说明'; ?></h3>
    <br/>
</div>
<pagebreak></pagebreak>
<div>
    <br/><br/>
    <?php foreach ($order_info->order_detail->images as $image) { ?>
        <img src="<?php echo $image ?>" style="max-height: 300px;max-width: 300px;margin: 10px;">
    <?php } ?>
    <br/>
</div>
<div style="position: absolute; bottom: 20px;text-align: center;width: 90%; ">
    <h2>现场照片</h2>
</div>
<pagebreak></pagebreak>

<br/><br/><br/>
<?php if (!empty($order_info->extra_info->repair_photos)) { ?>
    <div>
        <?php foreach ($order_info->extra_info->repair_photos as $image) { ?>
            <img src="<?php echo $image ?>" style="max-height: 260px;max-width: 300px;margin: 10px;"/>
        <?php } ?>

    </div>
    <div style="position: absolute; bottom: 20px;text-align: center;width: 90%; ">
        <h2>维修照片</h2>
    </div>
    <br/>
<?php } else { ?>
    <h1 align="center">暂无维修照片</h1>
    <br/>
<?php } ?>
<pagebreak></pagebreak>
<div>
    <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
    <h1 align="center">Thanks</h1>
    <h3 align="center">谢谢</h3>
</div>
</body>

</html>