<!DOCTYPE html>
<!--[if IE 8]>
<html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]>
<html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html class="no-js">
<!--<![endif]-->

<head>
    <?php require TPL_PATH . 'template_c/common/head.tpl.php'; ?>
    
    <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
    
    <link href="/static/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />
    <link
            href="/static/bower_components/jquery.tagsinput/dist/jquery.tagsinput.min.css"
            rel="stylesheet"/>
            
    <link href="/static/bower_components/select2/dist/css/select2.min.css" rel="stylesheet" />
    <link
            href="/static/bower_components/bootstrap-fileinput/css/fileinput.min.css"
            rel="stylesheet"/>
    <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->

</head>

<body>

<!-- start: HEADER -->
<?php require TPL_PATH . 'template_c/common/header.tpl.php'; ?>
<!-- end: HEADER -->
<!-- start: MAIN CONTAINER -->
<div class="main-container">
    <div class="navbar-content">
        <!-- start: SIDEBAR -->
        <?php require TPL_PATH . 'template_c/common/sidebar.tpl.php'; ?>
        <!-- end: SIDEBAR -->
    </div>

    <!-- start: PAGE -->
    <div class="main-content">
        <div class="container">
            <!-- start: PAGE HEADER -->
            <div class="row">
                <div class="col-sm-12">
                    <!-- start: STYLE SELECTOR BOX -->
                    <!-- end: STYLE SELECTOR BOX -->
                    <!-- start: PAGE TITLE & BREADCRUMB -->
                    <ol class="breadcrumb">
                        <li><i class="clip-user-5"></i> <a href="./"> 代理商 </a></li>
                        <li class="active">结算广告订单</li>
                    </ol>
                    <div class="page-header">
                        <h1>
                            广告订单
                            <small>overview &amp; stats</small>
                        </h1>
                    </div>
                    <!-- end: PAGE TITLE & BREADCRUMB -->
                </div>
            </div>
            <!-- end: PAGE HEADER -->
            <!-- start: PAGE CONTENT -->
            <div class="row">
                <div class="col-sm-12">
                    <!-- start: TEXT FIELDS PANEL -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-external-link-square"></i> 结算『 <?php echo $info['advertisement_title']; ?> 』  的广告订单
                        </div>
                        
                        <div class="panel-body">
                            <form role="form" class="form-horizontal"
                                  action="/agent/agent_order_manage/save_advertisement_order_offer?order_id=<?php echo $info['order_id'] ?>"
                                  id="advertisement_order_form" method="post" enctype="multipart/form-data">
                                <div class="errorHandler alert alert-danger no-display">
                                    <i class="fa fa-times-sign"></i> 出现了一些问题,请检查后再提交
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="resource_type">
                                        投放时间段
                                    </label>
                                    <div class="col-sm-6">
                                        <div class="input-group">
                                            <span class="input-group-addon"> <i class="fa fa-calendar"></i> </span>
                                            <input type="text" name="datetime_range"  id="datetime_range" class="form-control date-time-range" 
                                            value="<?php echo date("Y-m-d H:i", $info['start_time']) ." - ".date("Y-m-d H:i",$info['end_time'])?>" disabled="disabled">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="resource_type">
                                        期望资源类型
                                    </label>
                                    <div class="col-sm-4">
                                        <input type="text" placeholder="" id="resource_type" name="resource_type"
                                               class="form-control"
                                               value="<?php echo $info['resource_type']?>"  disabled="disabled">
                                    </div>
                                </div>

                                  <div class="form-group">
                                    <label class="col-sm-2 control-label" for="agent_offer_price">
                                         广告商结算价格
                                    </label>
                                    <div class="col-sm-4">
                                       <input type="text" id="agent_offer_price" name="agent_offer_price"  class="form-control currency"     >
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <span class="col-md-2 "></span>
                                    <div class="col-md-2">
                                        <button class="btn btn-yellow btn-block" type="submit" id="submit_btn">
                                            提交 <i class="fa fa-arrow-circle-right"></i>
                                        </button>
                                    </div>
                                    <div class="col-md-2">
                                        <button class="btn btn btn-light-grey btn-block" type="button"
                                                onclick="history.back();">
                                            返回 <i class="fa fa-close"></i>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                    <!-- end: TEXT FIELDS PANEL -->
                </div>
            </div>

            <!-- end: PAGE CONTENT-->
        </div>
    </div>
    <!-- end: PAGE -->
</div>
<!-- end: MAIN CONTAINER -->
<!-- start: FOOTER -->
<div class="footer clearfix">
    <div class="footer-inner">
        <script>
            document.write(new Date().getFullYear())
        </script>
        &copy; clip-one by cliptheme.
    </div>
    <div class="footer-items">
        <span class="go-top"><i class="clip-chevron-up"></i></span>
    </div>
</div>
<!-- end: FOOTER -->

<!-- start: RIGHT SIDEBAR -->
<!-- end: RIGHT SIDEBAR -->
<!-- start: MAIN JAVASCRIPTS -->
<!--[if lt IE 9]>
<script src="/static/bower_components/respond/dest/respond.min.js"></script>
<script src="/static/bower_components/Flot/excanvas.min.js"></script>
<script src="/static/bower_components/jquery-1.x/dist/jquery.min.js"></script>
<![endif]-->
<!--[if gte IE 9]><!-->
<script type="text/javascript"
        src="/static/bower_components/jquery/dist/jquery.min.js"></script>
<!--<![endif]-->
<script type="text/javascript"
        src="/static/bower_components/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript"
        src="/static/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript"
        src="/static/bower_components/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
<script type="text/javascript"
        src="/static/bower_components/blockUI/jquery.blockUI.js"></script>
<script type="text/javascript"
        src="/static/bower_components/iCheck/icheck.min.js"></script>
<script type="text/javascript"
        src="/static/bower_components/perfect-scrollbar/js/min/perfect-scrollbar.jquery.min.js"></script>
<script type="text/javascript"
        src="/static/bower_components/jquery.cookie/jquery.cookie.js"></script>
<script type="text/javascript"
        src="/static/bower_components/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript" src="/static/assets/js/min/main.min.js"></script>
<!-- end: MAIN JAVASCRIPTS -->
<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
<script src="/static/bower_components/moment/min/moment.min.js"></script>
<script
        src="/static/bower_components/bootstrap-maxlength/src/bootstrap-maxlength.js"></script>
<script src="/static/bower_components/autosize/dist/autosize.min.js"></script>
<script src="/static/bower_components/select2/dist/js/select2.min.js"></script>
<script
        src="/static/bower_components/jquery.maskedinput/dist/jquery.maskedinput.min.js"></script>
<script
        src="/static/bower_components/jquery-maskmoney/dist/jquery.maskMoney.min.js"></script>
<script
        src="/static/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script
        src="/static/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script
        src="/static/bower_components/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
<script
        src="/static/bower_components/mjolnic-bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<script
        src="/static/bower_components/jquery.tagsinput/src/jquery.tagsinput.js"></script>
<script
        src="/static/bower_components/summernote/dist/summernote.min.js"></script>
<script
        src="/static/bower_components/bootstrap-fileinput/js/plugins/canvas-to-blob.min.js"></script>
<script
        src="/static/bower_components/bootstrap-fileinput/js/fileinput.js"></script>
<script src="/static/assets/js/form-elements.js"></script>

<script src="/static/bower_components/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="/static/bower_components/summernote/dist/summernote.min.js"></script>
<script src="/static/bower_components/jquery-ajaxform/jquery.form.min.js"></script>
<script src="/static/bower_components/jquery-validation/src/localization/messages_zh.js"></script>

<!--     表单验证 -->
<script src="/static/user/advertisement_order-form-validation.js"></script>
<script src="/static/user/ajax-submit.js"></script>
<script src="/static/bower_components/bootstrap-fileinput/js/fileinput_locale_zh.js"></script>
<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->

<script>
    jQuery(document).ready(function () {
        Main.init();
        
        ajaxSubmit.init();
        AdvertisementOrderFormValidator.init();
        $('.date-time-range').daterangepicker({
            timePicker: true,
            timePickerIncrement: 30,
            locale: {
                format: 'YYYY-MM-DD HH:mm'
            }
        });
        $(".search-select").select2({
            placeholder: "选择渠道",
            allowClear: true
        });
        $(".currency").maskMoney();
    });
</script>

</body>

</html>