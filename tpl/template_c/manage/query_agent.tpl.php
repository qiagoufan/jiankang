<!DOCTYPE html>
<!--[if IE 8]>
<html class="ie8 no-js" lang="en"><![endif]-->
<!--[if IE 9]>
<html class="ie9 no-js" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html class="no-js">
<!--<![endif]-->
<head>
<?php require TPL_PATH . 'template_c/common/head.tpl.php'; ?>
</head>
<body>

<!-- start: HEADER -->
<?php require TPL_PATH . 'template_c/common/header.tpl.php'; ?>
<!-- end: HEADER -->
<!-- start: MAIN CONTAINER -->
<div class="main-container">
    <div class="navbar-content">
        <!-- start: SIDEBAR -->
        <?php require TPL_PATH . 'template_c/common/sidebar.tpl.php'; ?>
        <!-- end: SIDEBAR -->
    </div>

    <!-- start: PAGE -->
    <div class="main-content">
        <!-- start: PANEL CONFIGURATION MODAL FORM -->

        <!-- /.modal -->
        <!-- end: SPANEL CONFIGURATION MODAL FORM -->
        <div class="container">
            <!-- start: PAGE HEADER -->
            <div class="row">
                <div class="col-sm-12">
                    <!-- start: STYLE SELECTOR BOX -->
                    <!-- end: STYLE SELECTOR BOX -->
                    <!-- start: PAGE TITLE & BREADCRUMB -->
                    <ol class="breadcrumb">
                        <li><i class="clip-user-5"></i> <a href="./"> 系统管理 </a></li>
                        <li class="active">代理商详情</li>
                    </ol>
                    <div class="page-header">
                        <h1>
                            代理商详情
                            <small>overview &amp; stats</small>
                        </h1>
                    </div>


                    <!-- end: PAGE TITLE & BREADCRUMB -->
                </div>
            </div>
            <!-- end: PAGE HEADER -->
            <!-- start: PAGE CONTENT -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="tabbable">
                        <div class="tab-content">
                            <div id="panel_overview" class="tab-pane in active">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12">
                                        <div class="user-left">
                                            <table class="table table-condensed table-hover">
                                                <thead>
                                                <tr>
                                                    <th colspan="3">代理商 『 <?php echo $info['agent_name']; ?> 』详情</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>代理商企业全称</td>
                                                    <td>
                                                        <?php echo $info['agent_name']; ?>
                                                    </td>
                                                    <td><a href="/manage/agent_manage/edit_agent?agent_id=<?php echo $info['agent_id'] ?>"  >
                                                    <i class="fa fa-pencil edit-user-info"></i></a></td>
                                                </tr>
                                                <tr>
                                                    <td>代理商企业地址</td>
                                                    <td>
                                                        <?php echo $info['agent_address']; ?>
                                                    </td>
                                                    <td><a href="/manage/agent_manage/edit_agent?agent_id=<?php echo $info['agent_id'] ?>"  >
                                                    <i class="fa fa-pencil edit-user-info"></i></a></td>
                                                </tr>
                                                <tr>
                                                    <td>法人姓名</td>
                                                    <td>
                                                        <?php echo $info['legal_person']; ?>
                                                    </td>
                                                    <td><a href="/manage/agent_manage/edit_agent?agent_id=<?php echo $info['agent_id'] ?>"  >
                                                    <i class="fa fa-pencil edit-user-info"></i></a></td>
                                                </tr>
                                                <tr>
                                                    <td>法人身份证</td>
                                                    <td>
                                                        <?php echo $info['legal_person_id']; ?>
                                                    </td>
                                                    <td><a href="/manage/agent_manage/edit_agent?agent_id=<?php echo $info['agent_id'] ?>"  >
                                                    <i class="fa fa-pencil edit-user-info"></i></a></td>
                                                </tr>
                                                <tr>
                                                    <td>开户银行</td>
                                                    <td>
                                                        <?php echo $info['agent_bank_name']; ?>
                                                    </td>
                                                    <td><a href="/manage/agent_manage/edit_agent?agent_id=<?php echo $info['agent_id'] ?>"  >
                                                    <i class="fa fa-pencil edit-user-info"></i></a></td>
                                                </tr>
                                                <tr>
                                                    <td>开户银行账号</td>
                                                    <td>
                                                        <?php echo $info['agent_bank_account']; ?>
                                                    </td>
                                                    <td><a href="/manage/agent_manage/edit_agent?agent_id=<?php echo $info['agent_id'] ?>"  >
                                                    <i class="fa fa-pencil edit-user-info"></i></a></td>
                                                </tr>
                                                <tr>
                                                    <td>银行卡号</td>
                                                    <td>
                                                        <?php echo $info['agent_bank_card_num']; ?>
                                                    </td>
                                                    <td><a href="/manage/agent_manage/edit_agent?agent_id=<?php echo $info['agent_id'] ?>"  >
                                                    <i class="fa fa-pencil edit-user-info"></i></a></td>
                                                </tr>
                                                <tr>
                                                    <td>联系人</td>
                                                    <td>
                                                        <?php echo $info['agent_contact']; ?>
                                                    </td>
                                                    <td><a href="/manage/agent_manage/edit_agent?agent_id=<?php echo $info['agent_id'] ?>"  >
                                                    <i class="fa fa-pencil edit-user-info"></i></a></td>
                                                </tr>
                                                <tr>
                                                    <td>联系人邮箱</td>
                                                    <td>
                                                        <?php echo $info['agent_contact_email']; ?>
                                                    </td>
                                                    <td><a href="/manage/agent_manage/edit_agent?agent_id=<?php echo $info['agent_id'] ?>"  >
                                                    <i class="fa fa-pencil edit-user-info"></i></a></td>
                                                </tr>
                                                <tr>
                                                    <td>联系人手机号</td>
                                                    <td>
                                                        <?php echo $info['agent_contact_phone_num']; ?>
                                                    </td>
                                                    <td><a href="/manage/agent_manage/edit_agent?agent_id=<?php echo $info['agent_id'] ?>"  >
                                                    <i class="fa fa-pencil edit-user-info"></i></a></td>
                                                </tr>
                                                <tr>
                                                    <td>组织机构代码证</td>
                                                    <td>
                                                        <?php echo $info['organization_code_certificate']; ?>
                                                    </td>
                                                    <td><a href="/manage/agent_manage/edit_agent?agent_id=<?php echo $info['agent_id'] ?>"  >
                                                    <i class="fa fa-pencil edit-user-info"></i></a></td>
                                                </tr>
                                                <tr>
                                                    <td>工商注册号</td>
                                                    <td>
                                                        <?php echo $info['business_registration_no']; ?>
                                                    </td>
                                                    <td><a href="/manage/agent_manage/edit_agent?agent_id=<?php echo $info['agent_id'] ?>"  >
                                                    <i class="fa fa-pencil edit-user-info"></i></a></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <a href='#' onclick="history.back()">返回</a>
            <!-- end: PAGE CONTENT-->
        </div>
    </div>
    <!-- end: PAGE -->
</div>
<!-- end: MAIN CONTAINER -->
<!-- start: FOOTER -->
<?php require TPL_PATH . 'template_c/common/footer.tpl.php'; ?>
<!-- end: FOOTER -->
<!-- start: RIGHT SIDEBAR -->

<!-- end: RIGHT SIDEBAR -->
<!-- start: MAIN JAVASCRIPTS -->
<!--[if lt IE 9]>
<script src="/static/bower_components/respond/dest/respond.min.js"></script>
<script src="/static/bower_components/Flot/excanvas.min.js"></script>
<script src="/static/bower_components/jquery-1.x/dist/jquery.min.js"></script>
<![endif]-->
<!--[if gte IE 9]><!-->
<script type="text/javascript"
        src="/static/bower_components/jquery/dist/jquery.min.js"></script>
<!--<![endif]-->
<script type="text/javascript"
        src="/static/bower_components/jquery-ui/jquery-ui.min.js"></script>
<script type="text/javascript"
        src="/static/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript"
        src="/static/bower_components/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js"></script>
<script type="text/javascript"
        src="/static/bower_components/blockUI/jquery.blockUI.js"></script>
<script type="text/javascript"
        src="/static/bower_components/iCheck/icheck.min.js"></script>
<script type="text/javascript"
        src="/static/bower_components/perfect-scrollbar/js/min/perfect-scrollbar.jquery.min.js"></script>
<script type="text/javascript"
        src="/static/bower_components/jquery.cookie/jquery.cookie.js"></script>
<script type="text/javascript"
        src="/static/bower_components/sweetalert/dist/sweetalert.min.js"></script>
<script type="text/javascript" src="/static/assets/js/min/main.min.js"></script>
<!-- end: MAIN JAVASCRIPTS -->

<script>
    jQuery(document).ready(function () {
        Main.init();
    });
</script>

</body>

</html>