<?php

use business\feed\FeedInfoRespService;
use facade\request\admin\GetAllFeedListParams;
use facade\request\feed\GetFeedListParams;
use facade\request\feed\heat\TriggerEventParams;
use facade\request\interactive\CreateLikeParams;
use facade\request\user\FollowParams;
use facade\response\Result;
use lib\AppConstant;
use model\dto\FeedInfoDTO;
use service\admin\impl\FeedAdminServiceImpl;
use service\feed\impl\FeedHeatServiceImpl;
use service\feed\impl\FeedServiceImpl;
use service\interactive\impl\InteractiveServiceImpl;
use service\user\impl\FollowServiceImpl;
use service\user\impl\UserServiceImpl;

class FeedSchedule extends FUR_Controller
{


    const DEFAULT_PAGE_SIZE = 10;


    public function featureHeatSchedule()
    {


        $getFeedListParams = new  GetFeedListParams();
        $getFeedListParams->page_size = 100;
        $feedIdListRet = FeedServiceImpl::getInstance()->getIndexFeedIdList($getFeedListParams);
        if (Result::isSuccess($feedIdListRet)) {
            $feedIdList = $feedIdListRet->data;
            foreach ($feedIdList as $feedId) {
                $triggerEventParams = new TriggerEventParams();
                $triggerEventParams->feed_id = $feedId;
                $triggerEventParams->event_type = FeedHeatServiceImpl::EVENT_SCHEDULE;
                $triggerEventParams->point = rand(50, 100);
                FeedHeatServiceImpl::getInstance()->triggerEvent($triggerEventParams);
            }
        }

        $result = new Result();
        $result->setSuccessWithResult(true);
        $this->render($result);
    }


    public function freshHeatSchedule()
    {
        $getFeedListParams = new  GetFeedListParams();
        $getFeedListParams->page_size = 100;
        $feedIdListRet = FeedServiceImpl::getInstance()->getIndexFeedIdList($getFeedListParams);
        if (Result::isSuccess($feedIdListRet)) {
            $feedIdList = $feedIdListRet->data;
            foreach ($feedIdList as $feedId) {
                $triggerEventParams = new TriggerEventParams();
                $triggerEventParams->feed_id = $feedId;
                $triggerEventParams->event_type = FeedHeatServiceImpl::EVENT_SCHEDULE;
                $triggerEventParams->point = rand(50, 100);
                FeedHeatServiceImpl::getInstance()->triggerEvent($triggerEventParams);
            }


        }

        $getAllFeedListParams = new GetAllFeedListParams();
        $getAllFeedListParams->feed_status = FeedInfoDTO::FEED_STATUS_NORMAL;
        $getAllFeedListParams->page_size = 50;
        $latestFeedListRet = FeedAdminServiceImpl::getInstance()->getAllFeedList($getAllFeedListParams);
        if (Result::isSuccess($latestFeedListRet)) {
            $feedInfoDTOList = $latestFeedListRet->data;
            foreach ($feedInfoDTOList as $feedInfoDTO) {
                $triggerEventParams = new TriggerEventParams();
                $triggerEventParams->feed_id = $feedInfoDTO->id;
                $triggerEventParams->event_type = FeedHeatServiceImpl::EVENT_SCHEDULE;
                $triggerEventParams->point = rand(1, 50);
                FeedHeatServiceImpl::getInstance()->triggerEvent($triggerEventParams);
            }

            $getAllFeedListParams = new GetAllFeedListParams();
            $getAllFeedListParams->feed_status = FeedInfoDTO::FEED_STATUS_NORMAL;
            $getAllFeedListParams->page_size = 50;
        }
        $result = new Result();
        $result->setSuccessWithResult(true);
        $this->render($result);
    }


    public function likeSchedule()
    {
        $getFeedListParams = new  GetFeedListParams();
        $getFeedListParams->page_size = 60;
        $feedIdListRet = FeedServiceImpl::getInstance()->getIndexFeedIdList($getFeedListParams);
        $userServiceImpl = UserServiceImpl::getInstance();
        $userCountRet = $userServiceImpl->getRegisterUserCount();
        $userCount = $userCountRet->data;
        $divNum = intval($userCount / 5000) + 7;

        if (Result::isSuccess($feedIdListRet)) {
            $feedIdList = $feedIdListRet->data;
            foreach ($feedIdList as $feedId) {
                $rand = $feedId % $divNum + 1;
                for ($i = 0; $i < $rand; $i++) {
                    $createLikeParams = new CreateLikeParams();

                    $rankUserIdRet = $userServiceImpl->getMajiaUserId();
                    if (Result::isSuccess($rankUserIdRet)) {
                        $createLikeParams->user_id = $rankUserIdRet->data;
                    }

                    $createLikeParams->biz_id = $feedId;
                    InteractiveServiceImpl::getInstance()->addLike($createLikeParams);
                }
            }
        }

//        $getAllFeedListParams = new GetAllFeedListParams();
//        $getAllFeedListParams->feed_status = FeedInfoDTO::FEED_STATUS_NORMAL;
//        $getAllFeedListParams->page_size = 50;
//        $latestFeedListRet = FeedAdminServiceImpl::getInstance()->getAllFeedList($getAllFeedListParams);
//        if (Result::isSuccess($latestFeedListRet)) {
//            $feedInfoDTOList = $latestFeedListRet->data;
//            foreach ($feedInfoDTOList as $feedInfoDTO) {
//                $rand = rand(1, 5);
//                for ($i = 0; $i < $rand; $i++) {
//                    $createLikeParams = new CreateLikeParams();
//                    $userServiceImpl = UserServiceImpl::getInstance();
//                    $rankUserIdRet = $userServiceImpl->getMajiaUserId();
//                    if (Result::isSuccess($rankUserIdRet)) {
//                        $createLikeParams->user_id = $rankUserIdRet->data;
//                    }
//                    $createLikeParams->biz_id = $feedInfoDTO->id;
//                    InteractiveServiceImpl::getInstance()->addLike($createLikeParams);
//                }
//
//            }
//
//        }

        $result = new Result();
        $result->setSuccessWithResult(true);
        $this->render($result);
    }

    public function followSchedule()
    {

        $result = new Result();

        $operation_config = FUR_Config::get_operation_config('index_page');

        if ($operation_config == null || !isset($operation_config['index_star_list'])) {
            $result->setSuccessWithResult(false);
            $this->render($result);
        }
        $index_star_full_list = $operation_config['index_star_list'];

        foreach ($index_star_full_list as $index_star_list) {

            if (!$index_star_list) {
                break;
            }

            foreach ($index_star_list as $starId) {
                $followNum = $starId % 7 + rand(1, 3);
                for ($i = 0; $i < $followNum; $i++) {
                    $followParams = new FollowParams();
                    $followParams->user_id = UserServiceImpl::getInstance()->getMajiaUserId()->data;
                    $followParams->target_type = AppConstant::BIZ_TYPE_STAR;
                    $followParams->target_id = $starId;
                    FollowServiceImpl::getInstance()->follow($followParams);
                }
            }
        }


        $result = new Result();
        $result->setSuccessWithResult(true);
        $this->render($result);
    }
}