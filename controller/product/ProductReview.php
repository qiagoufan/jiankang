<?php

use business\product\ProductReviewConversionUtil;
use facade\request\product\GetProductReviewListParams;
use facade\request\product\SendProductReviewParams;
use facade\response\base\PageInfoResp;
use facade\response\product\ProductReviewListResp;
use facade\response\Result;
use model\product\dto\ProductReviewDTO;
use service\aliyun\impl\AcsServiceImpl;
use service\product\impl\ProductReviewServiceImpl;

class ProductReview extends FUR_Controller
{
    public function __construct()
    {

    }

    public function sendReview()
    {
        /**@var  SendProductReviewParams $sendProductReviewParams */
        $sendProductReviewParams = $this->requestObject(SendProductReviewParams::class);

        $sendProductReviewParams->user_id = $this->getRequestUserId();
        $productReviewService = ProductReviewServiceImpl::getInstance();
        $result = new Result();
        //看评论内容是否命中敏感词
        $acsContentCheckRet = AcsServiceImpl::getInstance()->checkContent($sendProductReviewParams->content);
        if (Result::isSuccess($acsContentCheckRet) && $acsContentCheckRet->data == true) {
            $result->setError(\Config_Error::ERR_REVIEW_CONTENT_IS_BLOCK);
            $this->render($result);
        }

        $result = $productReviewService->sendReview($sendProductReviewParams);
        $this->render($result);
    }

    public function getReviewInfo()
    {
        $result = new Result();
        $reviewId = $this->request('review_id');

        $productReviewService = ProductReviewServiceImpl::getInstance();
        $productReviewInfoRet = $productReviewService->getReviewInfo($reviewId);

        if (!Result::isSuccess($productReviewInfoRet)) {
            $this->render($productReviewInfoRet);
        }
        $productReviewResp = ProductReviewConversionUtil::conversionReview($productReviewInfoRet->data);
        $result->setSuccessWithResult($productReviewResp);
        $this->render($result);
    }

    public function getProductReviewList()
    {
        $result = new Result();
        /** @var  GetProductReviewListParams $getProductReviewListParams */
        $getProductReviewListParams = $this->requestObject(GetProductReviewListParams::class);
        if ($getProductReviewListParams->page_size == 0) {
            $getProductReviewListParams->page_size = 10;
        }

        $productReviewService = ProductReviewServiceImpl::getInstance();
        $reviewInfoRet = $productReviewService->getProductReviewList($getProductReviewListParams);
        if (!Result::isSuccess($reviewInfoRet)) {
            $this->render($reviewInfoRet);
        }
        $reviewListResp = new ProductReviewListResp();
        $productReviewRespList = [];
        $itemId = (int)$getProductReviewListParams->item_id;
        if (!empty($reviewInfoRet->data)) {
            /** @var ProductReviewDTO $productReviewDTO */
            foreach ($reviewInfoRet->data as $productReviewDTO) {
                if ($itemId == 0) {
                    $itemId = $productReviewDTO->item_id;
                }
                $productReviewResp = ProductReviewConversionUtil::conversionReview($productReviewDTO);
                if ($productReviewResp != null) {
                    array_push($productReviewRespList, $productReviewResp);
                }
            }
        }
        $reviewListResp->product_review_list = $productReviewRespList;
        $index = $getProductReviewListParams->index;
        $pageSize = $getProductReviewListParams->page_size;
        $currentSize = count($reviewInfoRet->data);

        $reviewListResp->page_info = PageInfoResp::buildPageInfoRespBaseLine($index, $pageSize, $currentSize);

        $reviewCountRet = $productReviewService->getReviewCount($itemId);
        if (Result::isSuccess($reviewCountRet)) {
            $reviewListResp->total = $reviewCountRet->data;
        }
        $result->setSuccessWithResult($reviewListResp);
        $this->render($result);
    }
}