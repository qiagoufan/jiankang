<?php

use business\feed\FeedTagConversionUtil;
use business\product\ProductDetailConversionUtil;
use business\product\ProductReviewConversionUtil;
use facade\request\feed\ConversionFeedParams;
use facade\request\product\CountProductSkuTotalPriceParams;
use facade\request\product\GetProductDetailParams;
use facade\request\product\GetProductItemListParams;
use facade\request\product\GetProductReviewListParams;
use facade\request\product\GetProductShowListParams;
use facade\request\product\SelectProductSpecificParams;
use facade\request\tag\QueryTagEntityListParams;
use facade\request\user\UserCollectParams;
use facade\response\base\PageInfoResp;
use facade\response\product\CountProductSkuTotalPriceResp;
use facade\response\product\ProductBaseInfoResp;
use facade\response\product\ProductDecorationResp;
use facade\response\product\ProductDetailInfoResp;
use facade\response\product\ProductExtraInfoResp;
use facade\response\product\ProductGalleyResp;
use facade\response\product\ProductItemForSkuResp;
use facade\response\product\ProductItemListResp;
use facade\response\product\ProductReviewListResp;
use facade\response\product\ProductSellerInfoResp;
use facade\response\product\ProductShowInfoResp;
use facade\response\product\ProductShowListInfoResp;
use facade\response\product\ProductSkuBrandInfoResp;
use facade\response\product\ProductSkuDetailResp;
use facade\response\product\ProductSkuPriceResp;
use facade\response\product\ProductSpecificResp;
use facade\response\Result;
use lib\ClientRouteHelper;
use lib\PriceHelper;
use model\product\dto\ProductItemDTO;
use model\product\dto\ProductSkuDTO;
use service\feed\impl\FeedInfoRespServiceImpl;
use service\feed\impl\FeedServiceImpl;
use service\product\impl\ProductReviewServiceImpl;
use service\product\impl\ProductServiceImpl;
use service\tag\impl\TagServiceImpl;
use service\user\impl\CollectServiceImpl;
use service\user\impl\StarServiceImpl;

class Product extends FUR_Controller
{
    /**
     * 查看商品详情
     */
    public function getProductSkuDetail()
    {

        $userId = $this->getRequestUserId(false);
        $result = new Result();
        $productSkuDetailParams = $this->requestObject(GetProductDetailParams::class);
        $productSkuServiceImpl = ProductServiceImpl::getInstance();
        $productSkuDetailResult = $productSkuServiceImpl->getProductDetail($productSkuDetailParams);

        if (!Result::isSuccess($productSkuDetailResult)) {
            $this->render($productSkuDetailResult);
        }

        $productSkuDetailDTO = $productSkuDetailResult->data;
        $productSkuDetailResp = $this->conversionProductSkuDetail($productSkuDetailDTO, $userId);

        $result->setSuccessWithResult($productSkuDetailResp);
        $this->render($result);

    }

    public function getProductList()
    {
        $productListRep = new ProductItemListResp();
        $result = new Result();
        $getProductListParams = $this->requestObject(GetProductItemListParams::class);
        $productServiceImpl = ProductServiceImpl::getInstance();
        $productListRet = $productServiceImpl->getProductItemList($getProductListParams);
        if (!Result::isSuccess($productListRet)) {
            $result->setSuccessWithResult($productListRep);
            $this->render($result);
        }
        $productListRep->list = $productListRet->data;
        $result->setSuccessWithResult($productListRep);
        $this->render($result);

    }

    public function getProductDetail()
    {
        $result = new Result();
        $getProductDetailParams = $this->requestObject(GetProductDetailParams::class);
        $productServiceImpl = ProductServiceImpl::getInstance();
        $productDetailRet = $productServiceImpl->getProductDetail($getProductDetailParams);
        if (!Result::isSuccess($productDetailRet)) {
            $result->setSuccessWithResult($productDetailRet);
            $this->render($result);
        }

        $result->setSuccessWithResult($productDetailRet);
        $this->render($result);

    }


    /**
     *获取商品规格选择器数据
     */
    public function selectProductSpecific()
    {
        $result = new Result();
        $productService = ProductServiceImpl::getInstance();
        $selectProductSpecificParams = $this->requestObject(SelectProductSpecificParams::class);
        $productSkuDetailDTO = $productService->selectProductSpecific($selectProductSpecificParams);
        if (!Result::isSuccess($productSkuDetailDTO)) {
            $this->render($productSkuDetailDTO);
        }
        $productSkuDetailResp = $this->conversionProductSkuDetail($productSkuDetailDTO->data);
        $result->setSuccessWithResult($productSkuDetailResp);
        $this->render($result);

    }


    /**
     * @param ProductSkuDTO $productSkuDTO
     * @param int $userId
     * @return ProductSkuDetailResp|null
     * 查询商品下单总价
     */
    public function countProductTotalPrice()
    {
        $result = new Result();
        $productService = ProductServiceImpl::getInstance();
        $countProductSkuTotalPriceParams = $this->requestObject(CountProductSkuTotalPriceParams::class);

        if ($countProductSkuTotalPriceParams->sku_id == null || $countProductSkuTotalPriceParams->count == null) {
            $result->setError(\Config_Error::ERR_COUNT_TOTAL_PRICE_PARAMS_IS_NULL);
            $this->render($result);
        }
        $productSkuDTO = new ProductSkuDTO();
        $productSkuDTO->id = $countProductSkuTotalPriceParams->sku_id;
        $productPriceRet = $productService->getProductSkuPrice($productSkuDTO);
        if (!Result::isSuccess($productPriceRet)) {
            $result->setError(\Config_Error::ERR_PRODUCT_PRICE_IS_NOT_EXIST);
            $this->render($result);
        }
        $currentPrice = $productPriceRet->data->current_price;
        $totalPrice = PriceHelper::conversionDisplayPrice($currentPrice * $countProductSkuTotalPriceParams->count, true);
        $payPrice = PriceHelper::conversionDisplayPrice($currentPrice * $countProductSkuTotalPriceParams->count + $productPriceRet->data->shipping_price, true);;
        $countProductSkuTotalPriceResp = new CountProductSkuTotalPriceResp();
        $countProductSkuTotalPriceResp->total_price = $totalPrice;
        $countProductSkuTotalPriceResp->pay_price = $payPrice;
        $shipping_price = PriceHelper::conversionDisplayPrice($productPriceRet->data->shipping_price);
        if ($shipping_price == 0) {
            $shipping_price = '免运费';
        } else {
            $shipping_price = PriceHelper::conversionDisplayPrice($productPriceRet->data->shipping_price, true);
        }
        $countProductSkuTotalPriceResp->shipping_price = $shipping_price;
        $result->setSuccessWithResult($countProductSkuTotalPriceResp);
        $this->render($result);

    }


    private function conversionProductSkuDetail(ProductSkuDTO &$productSkuDTO, int $userId = 0): ?ProductSkuDetailResp
    {

        $productSkuDetailResp = new ProductSkuDetailResp();
        /**
         * 转换商品画廊
         */
        $productGalleryResp = $this->conversionProductGallery($productSkuDTO);
        if (empty($productGalleryResp)) {
            return null;
        }
        $productSkuDetailResp->product_gallery = $productGalleryResp;
        /**
         * 获取产品基本信息
         */
        $productBaseInfoResp = $this->getProductBaseInfo($productSkuDTO);
        if (empty($productBaseInfoResp)) {
            return null;
        }
        $productSkuDetailResp->product_base_info = $productBaseInfoResp;


        /**
         * 转换产品晒货区
         */
        $productSkuShowResp = $this->getProductShowInfoResp($productSkuDTO, $userId);
        $productSkuDetailResp->product_show_info = $productSkuShowResp;

        /**
         * 转换产品额外信息
         */
        $productExtraResp = $this->getProductExtraInfo($productSkuDTO, $userId);
        $productSkuDetailResp->product_extra_info = $productExtraResp;


        /**
         * 转换产品详情
         */
        $productDetailResp = $this->getProductDetailInfo($productSkuDTO);
        if (empty($productDetailResp)) {
            return null;
        }
        $productSkuDetailResp->product_detail_info = $productDetailResp;
        /**
         * 转换产品规格
         */
        $productSpecificationResp = $this->getProductSpecification($productSkuDTO);
        if (empty($productSpecificationResp)) {
            return null;
        }
        $productSkuDetailResp->product_specification = $productSpecificationResp;

        /**
         * 转换产品评论
         */
        $productSkuReviewResp = $this->getProductReviewListResp($productSkuDTO->item_id);
        $productSkuDetailResp->product_review_info = $productSkuReviewResp;


        /**
         * 转换产品item
         */
        $productItemResp = $this->getProductItem($productSkuDTO->item_id);
        if (empty($productItemResp)) {
            return null;
        }
        $productSkuDetailResp->product_item = $productItemResp;

        /**
         * 转换产品附件信息
         */
        $productDecorationResp = $this->getProductDecoration($productSkuDTO);
        $productSkuDetailResp->product_decoration_info = $productDecorationResp;

        /**
         * 转换超级商品IP信息
         */
        $productSkuIpResp = $this->getProductBrandInfo($productItemResp);
        $productSkuDetailResp->product_brand_info = $productSkuIpResp;


        /**
         * 转换商品标签信息
         */
        $productTagResp = $this->getProductTagInfo($productSkuDTO);
        $productSkuDetailResp->product_tag_info = $productTagResp;


        return $productSkuDetailResp;


    }


    /**
     * 获取产品主图
     * @param ProductSkuDTO $productSkuDTO
     * @return ProductGalleyResp|null
     */
    private function conversionProductGallery(ProductSkuDTO &$productSkuDTO): ?ProductGalleyResp
    {
        $productSkuGalleryResp = new ProductGalleyResp();
        $productSkuGalleryResp->product_gallery_list = json_decode($productSkuDTO->gallery_image);
        $productSkuGalleryResp->main_image = $productSkuDTO->main_image;
        if ($productSkuGalleryResp->product_gallery_list == null) {
            $productSkuGalleryResp->product_gallery_list = [$productSkuDTO->main_image];
        }

        return $productSkuGalleryResp;
    }


    /**
     * 转换产品基本信息
     * @param ProductSkuDTO $productSkuDTO
     * @return ProductBaseInfoResp|null
     */
    private function getProductBaseInfo(ProductSkuDTO &$productSkuDTO): ?ProductBaseInfoResp
    {
        $productBaseInfoResp = new ProductBaseInfoResp();
        $productBaseInfoResp->item_id = $productSkuDTO->item_id;
        $productBaseInfoResp->title = $productSkuDTO->product_name;
        $productBaseInfoResp->stock = $productSkuDTO->stock;
        $productBaseInfoResp->sale = $productSkuDTO->sale;
        $productBaseInfoResp->seller_id = $productSkuDTO->seller_id;
        $productBaseInfoResp->sku_id = $productSkuDTO->id;

        /**
         * 转换seller信息
         */
        $productSellerInfoResp = new ProductSellerInfoResp();
        $productSellerInfoResp->name = UserDTO::OFFICIAL_ACCOUNT_NAME;
        $productSellerInfoResp->avatar = UserDTO::OFFICIAL_ACCOUNT_AVATAR;
        $productBaseInfoResp->seller = $productSellerInfoResp;

        $productSkuPriceResp = $this->getProductSkuPrice($productSkuDTO);
        if ($productSkuPriceResp == null) {
            return null;
        }
        $productBaseInfoResp->price = $productSkuPriceResp;
        $productBaseInfoResp->purchase_info = ProductDetailConversionUtil::buildPurchaseInfo($productSkuDTO);

        return $productBaseInfoResp;

    }


    /**
     * 转换产品额外信息
     * @param ProductSkuDTO $productSkuDTO
     * @return ProductExtraInfoResp|null
     */
    private function getProductExtraInfo(ProductSkuDTO &$productSkuDTO, int $userId): ?ProductExtraInfoResp
    {
        $productExtraInfoResp = new ProductExtraInfoResp();
        $productExtraInfoResp->explanation = FUR_Config::get_product_config('default_explanation');
        if ($userId != 0) {
            /**
             * 收藏状态
             */
            $collectParams = new UserCollectParams();
            $collectParams->user_id = $userId;
            $collectParams->collect_biz_id = $productSkuDTO->id;
            $collectParams->collect_biz_type = ProductSkuDTO::PRODUCT_SKU_BIZ_TYPE;
            $collectRet = CollectServiceImpl::getInstance()->isCollect($collectParams);
            if (Result::isSuccess($collectRet)) {
                $productExtraInfoResp->collect_status = intval($collectRet->data);
            }
        }
        return $productExtraInfoResp;

    }


    /**
     * 转换产品详情（文字或者图片）
     * @param ProductSkuDTO $productSkuDTO
     * @return ProductDetailInfoResp|null
     */
    private function getProductDetailInfo(ProductSkuDTO $productSkuDTO): ?ProductDetailInfoResp
    {
        $productDetailInfoResp = new ProductDetailInfoResp();
        $productDetailInfoResp->pic_list = json_decode($productSkuDTO->detail_image);
        return $productDetailInfoResp;

    }


    /**
     * 获取产品价格
     * @param ProductSkuDTO $productSkuDTO
     * @return ProductSkuPriceResp|null
     */

    private function getProductSkuPrice(ProductSkuDTO $productSkuDTO): ?ProductSkuPriceResp
    {
        $productSkuService = ProductServiceImpl::getInstance();
        $productSkuPriceResult = $productSkuService->getProductSkuPrice($productSkuDTO);
        if (!Result::isSuccess($productSkuPriceResult)) {
            return null;
        }

        $productSkuPriceRangeResult = $productSkuService->getProductSkuPriceRange($productSkuDTO);
        if (!Result::isSuccess($productSkuPriceRangeResult)) {
            return null;
        }

        $productSkuPriceRange = $productSkuPriceRangeResult->data;
        $productSkuPriceDTO = $productSkuPriceResult->data;
        $productSkuPriceResp = ProductDetailConversionUtil::conversionProductPrice($productSkuPriceDTO);
        $productSkuPriceResp->low_price = intval(PriceHelper::conversionDisplayPrice($productSkuPriceRange->low_price));
        $productSkuPriceResp->high_price = intval(PriceHelper::conversionDisplayPrice($productSkuPriceRange->high_price));
        $productSkuPriceResp->low_price_formatted = PriceHelper::conversionDisplayPrice($productSkuPriceRange->low_price, true);
        $productSkuPriceResp->high_price_formatted = PriceHelper::conversionDisplayPrice($productSkuPriceRange->high_price, true);

        return $productSkuPriceResp;
    }

    /**
     * 获取产品评论
     * @param int $itemId
     * @return ProductReviewListResp|null
     */

    private function getProductReviewListResp(int $itemId): ?ProductReviewListResp
    {

        $reviewListResp = new ProductReviewListResp();
        $productReviewService = ProductReviewServiceImpl::getInstance();
        $getProductReviewListParams = new GetProductReviewListParams();
        $getProductReviewListParams->page_size = 3;
        $getProductReviewListParams->index = 0;
        $getProductReviewListParams->item_id = $itemId;

        $reviewInfoRet = $productReviewService->getProductReviewList($getProductReviewListParams);
        if (!Result::isSuccess($reviewInfoRet)) {
            $this->render($reviewInfoRet);
        }
        $productReviewRespList = [];
        if (!empty($reviewInfoRet->data)) {
            foreach ($reviewInfoRet->data as $productReviewDTO) {
                $productReviewResp = ProductReviewConversionUtil::conversionReview($productReviewDTO);
                if ($productReviewResp != null) {
                    array_push($productReviewRespList, $productReviewResp);
                }
            }
        }
        $reviewListResp->product_review_list = $productReviewRespList;
        $index = $getProductReviewListParams->index;
        $pageSize = $getProductReviewListParams->page_size;
        $currentSize = count($reviewInfoRet->data);
        $reviewListResp->page_info = PageInfoResp::buildPageInfoRespBaseLine($index, $pageSize, $currentSize);

        $reviewCountRet = $productReviewService->getReviewCount($itemId);
        if (Result::isSuccess($reviewCountRet)) {
            $reviewListResp->total = $reviewCountRet->data;
        }

        return $reviewListResp;

    }

    /**
     * 获取产品规格
     * @param ProductSkuDTO $productSkuDTO
     * @return array|null
     */

    private function getProductSpecification(ProductSkuDTO $productSkuDTO): ?ProductSpecificResp
    {
        $productSpecificationResp = new ProductSpecificResp();
        $productSpecificationResp->index = $productSkuDTO->indexes;
        $productSpecificationResp->specification = json_decode($productSkuDTO->specifications);
        return $productSpecificationResp;
    }

    /**
     * 获取产品item详情
     * @param int $itemId
     * @return ProductItemForSkuResp|null
     */
    private function getProductItem(int $itemId): ?ProductItemForSkuResp
    {
        $productItemResp = new ProductItemForSkuResp();
        $productSkuService = ProductServiceImpl::getInstance();
        $productItemResult = $productSkuService->getProductItem($itemId);
        if (!Result::isSuccess($productItemResult)) {
            $this->render($productItemResult);
        }
        /** @var ProductItemDTO $productItemDTO */
        $productItemDTO = $productItemResult->data;
        $productItemResp->item_id = $productItemDTO->id;
        $productItemResp->default_sku_id = $productItemDTO->default_sku_id;
        $productItemResp->product_specification_list = json_decode($productItemDTO->specifications);

        /**已售罄、已下架状态*/
        if ($productItemDTO->publish_status == ProductItemDTO::publish_status_ONLINE) {

            $productItemResp->purchase_status = ProductItemForSkuResp::PURCHASE_STATUS_NORMAL;
            if ($productItemDTO->stock == 0) {
                $productItemResp->purchase_status = ProductItemForSkuResp::PURCHASE_STATUS_SELL_OUT;
            }
        } else {

            $productItemResp->purchase_status = ProductItemForSkuResp::PURCHASE_STATUS_OFFLINE;
        }

        return $productItemResp;
    }


    /**
     * 获取产品decoration信息
     * @param ProductSkuDTO $productSkuDTO
     * @return ProductDecorationResp|null
     */

    private function getProductDecoration(ProductSkuDTO $productSkuDTO): ?ProductDecorationResp
    {

        $productDecorationResp = new ProductDecorationResp();
        $productDecorationResp->banner_pic = "https://img.alicdn.com/imgextra/i2/1684634320/O1CN01jCHc2e1hmbas8Ejag_!!1684634320.jpg";

        return $productDecorationResp;

    }

    /**
     * @param ProductSkuDTO $productSkuDTO
     * @return ProductSkuBrandInfoResp|null
     * 获取产品品牌信息
     */
    private function getProductBrandInfo(ProductItemForSkuResp $productItemForSkuResp): ?ProductSkuBrandInfoResp
    {

        $productSkuIpInfoResp = new ProductSkuBrandInfoResp();


        /**
         * 获取sku绑定的明星id
         */

        $queryTagEntityListParams = new QueryTagEntityListParams();
        $queryTagEntityListParams->tag_biz_type = TagEntityDTO::BIZ_TYPE_PRODUCT_SKU;
        $queryTagEntityListParams->tag_biz_id = $productItemForSkuResp->default_sku_id;
        $queryTagEntityListParams->entity_biz_type = TagEntityDTO::BIZ_TYPE_STAR;
        $queryTagEntityListParams->index = 0;
        $queryTagEntityListParams->page_size = 1;
        $tagService = TagServiceImpl::getInstance();
        $entityListRet = $tagService->queryTagEntityList($queryTagEntityListParams);


        foreach ($entityListRet->data as $star_id) {
            $starService = StarServiceImpl::getInstance();
            $startInfoRet = $starService->getStarInfo($star_id);

            if (Result::isSuccess($startInfoRet) && ($startInfoRet->data != null)) {
                /**商品品牌IP信息,不做限制*/
//                if ($startInfoRet->data->ip_type == UserStarDTO::IP_TYPE_FAMOUS) {
                $productSkuIpInfoResp->brand_link = ClientRouteHelper::buildStarLink($star_id, UserStarDTO::IP_TYPE_FAMOUS);
                $productSkuIpInfoResp->brand_name = $startInfoRet->data->star_name;
//                }

                return $productSkuIpInfoResp;
            }
        }


        return $productSkuIpInfoResp;
    }


    /**
     * @param ProductSkuDTO $productSkuDTO
     * @return array|null
     * 获取商品晒货区数据
     */
    private function getProductShowInfoResp(ProductSkuDTO $productSkuDTO, ?int $userId): ?array
    {

        $productShowInfoArray = [];


        $index = 0;
        $productFeedList = $this->getProductFeedList($productSkuDTO->id, $index, $userId, 4);

        if ($productFeedList == null) {
            return $productShowInfoArray;
        }
        foreach ($productFeedList as $feedInfo) {
            /**返回带图片的feed，取第一张图片*/
            if ($feedInfo->feed_detail->feed_content_list == null) {
                continue;
            }
            $feedContentList = $feedInfo->feed_detail->feed_content_list;

            $productShowInfo = new ProductShowInfoResp();
            $productShowInfo->feed_id = $feedInfo->feed_base_info->feed_id;
            $productShowInfo->product_show_image = $feedContentList[0]->image;

            array_push($productShowInfoArray, $productShowInfo);

        }
        return $productShowInfoArray;
    }


    /**
     * @param ProductSkuDTO $productSkuDTO
     * @return array|null
     * 获取商品标签信息
     */
    private
    function getProductTagInfo(ProductSkuDTO $productSkuDTO): ?array
    {
        $productTagInfoArray = [];
        $ProductFeedTagResp = FeedTagConversionUtil::buildProductFeedTagInfo($productSkuDTO);
        array_push($productTagInfoArray, $ProductFeedTagResp);

        return $productTagInfoArray;
    }

    /**
     * 查看商品晒货区
     */
    public
    function getProductShowInfo()
    {

        $result = new Result();

        /** @var GetProductShowListParams $getProductShowListParams */
        $getProductShowListParams = $this->requestObject(GetProductShowListParams::class);

        $skuId = $getProductShowListParams->sku_id;
        $index = $getProductShowListParams->index;
        $productFeedList = $this->getProductFeedList($skuId, $index, $this->getRequestUserId(false));


        $ProductShowListResp = new ProductShowListInfoResp();
        $page_info = PageInfoResp::buildPageInfoRespBaseLine($index, $getProductShowListParams->page_size, count($productFeedList));

        $ProductShowListResp->page_info = $page_info;
        $ProductShowListResp->feed_info_list = $productFeedList;


        $result->setSuccessWithResult($ProductShowListResp);
        $this->render($result);


    }


    /**
     * @param int $skuId
     * @return |null
     * 获取商品晒货区feed list
     */
    private function getProductFeedList(int $skuId, int $index, ?int $userId = 0, ?int $pageSize = 10)
    {

        $productFeedList = [];
        $tagService = TagServiceImpl::getInstance();
        /**
         * 查询打上商品标的feed列表
         */


        $queryTagEntityListParams = new QueryTagEntityListParams ();
        $queryTagEntityListParams->tag_biz_type = TagEntityDTO::BIZ_TYPE_PRODUCT_SKU_SHOWCASE;
        $queryTagEntityListParams->tag_biz_id = $skuId;
        $queryTagEntityListParams->entity_biz_type = TagEntityDTO::BIZ_TYPE_FEED;
        $queryTagEntityListParams->index = $index;
        $queryTagEntityListParams->page_size = $pageSize;
        $entityListRet = $tagService->queryTagEntityList($queryTagEntityListParams);

        if (!Result::isSuccess($entityListRet)) {
            return null;
        }

        $feedInfoRespService = FeedInfoRespServiceImpl::getInstance();
        $feedIdList = $entityListRet->data;
        $feedIdList = array_unique($feedIdList);

        $feedService = FeedServiceImpl::getInstance();


        $conversionFeedParams = new ConversionFeedParams();
        $conversionFeedParams->scene = FeedInfoRespServiceImpl::SCENE_SUPER_PRODUCT_CARD;
        $conversionFeedParams->user_id = $userId;


        foreach ($feedIdList as $feedId) {
            $feedDetailRet = $feedService->getFeedDetail($feedId);
            if (!Result::isSuccess($feedDetailRet)) {
                continue;
            }

            $feedInfo = $feedDetailRet->data;
            $feedInfoResp = $feedInfoRespService->conversionFeedInfoResp($feedInfo, $conversionFeedParams);
            if ($feedInfoResp == null) {
                continue;
            }

            array_push($productFeedList, $feedInfoResp);

        }

        return $productFeedList;

    }
}

