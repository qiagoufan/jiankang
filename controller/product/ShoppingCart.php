<?php


use business\order\CalculateOrderPriceUtil;
use facade\request\order\CalculateOrderPriceParams;
use facade\response\Result;
use facade\response\shoppingCart\ShoppingCartResp;

class ShoppingCart extends FUR_Controller
{
    private $userId;

    public function __construct()
    {
        $userId = $this->getRequestUserId();
        $this->userId = $userId;
    }


    public function calculateOrderPrice()
    {
        $result = new Result();
        /** @var CalculateOrderPriceParams $calculateOrderPriceParams */
        $calculateOrderPriceParams = $this->requestObject(CalculateOrderPriceParams::class);
        if ($calculateOrderPriceParams->product_list == null) {
            $shoppingCartResp = new ShoppingCartResp();
            $result->setSuccessWithResult($shoppingCartResp);
            $this->render($result);
        }
        $calculatePriceRet = CalculateOrderPriceUtil::getInstance()->calculatePrice($calculateOrderPriceParams);
        if (!Result::isSuccess($calculatePriceRet)) {
            $this->render($calculatePriceRet);
        }
        $shoppingCartResp = new ShoppingCartResp();
        FUR_Core::copyProperties($calculatePriceRet->data, $shoppingCartResp);

        $result->setSuccessWithResult($shoppingCartResp);
        $this->render($result);
    }

//    private function getShoppingCartRemind(ShoppingCartResp &$shoppingCartResp, ?bool $fromshoppingCart = false): ?ShoppingCartRemindResp
//    {
//        $shoppingCartRemindResp = new ShoppingCartRemindResp();
//        $freeShippingPrice = PriceHelper::conversionDisplayPrice(CalculateOrderPriceUtil::FREE_SHIPPING_PRICE);
//        if ($fromshoppingCart) {
//            $shoppingCartRemindResp->button_name = '再看看';
//            $shoppingCartRemindResp->link = ClientRouteHelper::buildVoucherProductLink();
//            $shoppingCartRemindResp->content = "满" . $freeShippingPrice . "元包邮,需再购买" . $freeShippingPrice . "元";
//        } elseif ($shoppingCartResp->shipping_price == 0 && $shoppingCartResp->product_total_price != 0) {
//            $shoppingCartRemindResp->content = "已满足" . $freeShippingPrice . "元包邮免运费条件";
//        } else {
//            $shoppingCartRemindResp->button_name = '再看看';
//            $shoppingCartRemindResp->link = ClientRouteHelper::buildVoucherProductLink();
//
//            $agioPrice = PriceHelper::conversionDisplayPrice(CalculateOrderPriceUtil::FREE_SHIPPING_PRICE - $shoppingCartResp->product_total_price);
//            $shoppingCartRemindResp->content = "满" . $freeShippingPrice . "元包邮,需再购买" . $agioPrice . "元";
//        }
//
//
//        return $shoppingCartRemindResp;
//    }

}