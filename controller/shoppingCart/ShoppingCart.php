<?php


use facade\request\shoppingCart\UpdateCartParams;
use facade\request\shoppingCart\RemoveFromCartParams;
use facade\response\Result;
use facade\response\shoppingCart\ShoppingCartProductResp;
use facade\response\shoppingCart\ShoppingCartResp;
use lib\PriceHelper;
use model\product\dto\ProductItemDTO;
use model\shoppingCart\dto\ShoppingCartDTO;
use service\product\impl\ProductServiceImpl;
use service\shoppingCart\impl\ShoppingCartServiceImpl;

class ShoppingCart extends FUR_Controller
{
    private $userId;

    public function __construct()
    {
        $userId = $this->getRequestUserId(false);
        $this->userId = $userId;
    }


    /**
     * 查看用户购物车详情
     */
    public function getUserShoppingCartDetail()
    {
        $result = new Result();

        $shoppingCartResp = new ShoppingCartResp();
        $shoppingCartResp->total_price = 0;
        $userCartRet = ShoppingCartServiceImpl::getInstance()->getUserCart($this->userId);
        if (Result::isSuccess($userCartRet) == false) {
            $result->setSuccessWithResult($shoppingCartResp);
            $this->render($result);
        }
        $shoppingCartDTOList = $userCartRet->data;
        $shoppingCartResp->shopping_cart_product_list = [];
        /** @var ShoppingCartDTO $shoppingCartDTO */
        foreach ($shoppingCartDTOList as $shoppingCartDTO) {
            $shoppingCartProductResp = new  ShoppingCartProductResp();
            $shoppingCartProductResp->item_id = $shoppingCartDTO->item_id;
            $shoppingCartProductResp->count = $shoppingCartDTO->count;
            $productInfoRet = ProductServiceImpl::getInstance()->getProductItem($shoppingCartDTO->item_id);
            if (!Result::isSuccess($productInfoRet)) {
                $shoppingCartProductResp->product_name = '无效商品';
                array_push($shoppingCartResp->shopping_cart_product_list, $shoppingCartProductResp);
                continue;
            }
            /** @var ProductItemDTO $productItemDTO */
            $productItemDTO = $productInfoRet->data;
            $shoppingCartProductResp->product_name = $productItemDTO->product_name;
            $shoppingCartProductResp->main_image = $productItemDTO->main_image;
            $shoppingCartProductResp->product_price = $productItemDTO->current_price;
            $shoppingCartProductResp->format_product_price = PriceHelper::conversionDisplayPrice($shoppingCartProductResp->product_price);
            $shoppingCartResp->total_price += $shoppingCartDTO->count * $productItemDTO->current_price;
            $shoppingCartResp->format_total_price = PriceHelper::conversionDisplayPrice($shoppingCartResp->total_price);
            array_push($shoppingCartResp->shopping_cart_product_list, $shoppingCartProductResp);
        }

        $result->setSuccessWithResult($shoppingCartResp);
        $this->render($result);
    }

    public function updateCart()
    {

        $result = new Result();
        /** @var UpdateCartParams $params */
        $params = $this->requestObject(UpdateCartParams::class);
        if ($params->count == 0) {
            $result->setError(Config_Error::ERR_SHOPPING_CART_COUNT_ERROR);
            $this->render($result);
        }
        if ($params->item_id == 0 && $params->sku_id == 0) {
            $result->setError(Config_Error::ERR_SHOPPING_CART_NULL_PRODUCT);
            $this->render($result);
        }
        $params->user_id = $this->userId;
        $addRet = ShoppingCartServiceImpl::getInstance()->updateCart($params);
        $this->render($addRet);
    }

    public function removeFromCart()
    {
        /** @var RemoveFromCartParams $removeFromCartParams */
        $removeFromCartParams = $this->requestObject(RemoveFromCartParams::class);
        $userId = $this->userId;
        $itemIdList = ($removeFromCartParams->item_id_list);
        if (!is_array($itemIdList)) {
            $result = new Result();
            $result->setSuccessWithResult(true);
            $this->render($result);
        }
        $result = ShoppingCartServiceImpl::getInstance()->removeFromCart($userId, $itemIdList);
        $this->render($result);
    }
}