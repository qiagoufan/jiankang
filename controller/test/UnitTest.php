<?php


use db\ElasticSearchClient;
use facade\request\aliyun\oss\UploadFileParams;
use facade\request\feed\search\EsDataSynParams;
use facade\request\user\FollowParams;
use lib\PregHelper;
use lib\AppConstant;
use model\jk\dto\JkBodyOptionDTO;
use model\jk\dto\JkBodyPossibleDiseaseCopy1DTO;
use model\jk\dto\JkBodyPossibleDiseaseDTO;
use model\jk\dto\JkBodyQuestionDTO;
use model\jk\dto\JkBodyQuestionOptionDTO;
use model\jk\dto\JkBodySymptomDTO;
use model\jk\dto\JkBodySymptomListDTO;
use model\jk\JkBodyPossibleDiseaseModel;
use model\jk\JkBodySymptomQuestionModel;
use model\search\dto\SearchResultDTO;
use service\aliyun\impl\AliyunOssServiceImpl;
use service\aliyun\impl\VodServiceImpl;
use service\media\impl\ImageServiceImpl;
use service\qiniu\impl\QiniuOssServiceImpl;
use service\search\impl\BizEsDataSynServiceImpl;
use service\search\impl\SearchServiceImpl;

class UnitTest extends FUR_Controller
{
    public function qiuniuosstest()
    {

        $uploadOssFileParams = new UploadFileParams();
        $uploadOssFileParams->object_name = 'jkjc' . DIRECTORY_SEPARATOR . "dc0357847db099717fe4ef98b6b5c091.jpg";
        $uploadOssFileParams->file_path = APP_PATH . 'file' . DIRECTORY_SEPARATOR . "dc0357847db099717fe4ef98b6b5c091.jpg";
        $data = QiniuOssServiceImpl::getInstance()->uploadOssFile($uploadOssFileParams);
        var_dump($data);
    }


}