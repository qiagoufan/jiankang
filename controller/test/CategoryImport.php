<?php

use facade\request\category\EditCategoryParams;
use facade\response\Result;
use service\category\impl\CategoryServiceImpl;

class CategoryImport extends FUR_Controller
{

    const DEFAULT_PAGE_SIZE = 10;


    public function start()
    {

        set_time_limit(1000);
        $file = @fopen(APP_PATH . "static/data.text", 'r');
        $content = array();
        if (!$file) {
            return 'file open fail';
        }
        $categoryRepairService = CategoryServiceImpl::getInstance();
        while (!feof($file)) {
            $content = mb_convert_encoding(fgets($file), "UTF-8");
            $cate_level_1_id = 0;
            $cate_level_2_id = 0;
            $cate_level_3_id = 0;

            if ($content == "") {
                break;
            }
            list($cate_level_1_name, $cate_level_2_name, $cate_level_3_name) = explode(" ", $content);
            $cate_1_record_ret = $categoryRepairService->queryCategoryByNameAndLevel($cate_level_1_name, 1);
            if (Result::isSuccess($cate_1_record_ret) && $cate_1_record_ret->data != 0) {
                $cate_level_1_id = $cate_1_record_ret->data->id;
            } else {
                $editCategoryParams = new EditCategoryParams();
                $editCategoryParams->category_level = 1;
                $editCategoryParams->cate_level1_id = $cate_level_1_id;
                $editCategoryParams->cate_level1_name = $cate_level_1_name;
                $editCategoryParams->parent_id = 0;
                $editCategoryParams->category_name = $cate_level_1_name;
                $createRet = $categoryRepairService->createCategory($editCategoryParams);
                $cate_level_1_id = $createRet->data;
            }
            if ($cate_level_2_name == null) {
                continue;
            }
            $cate_2_record_ret = $categoryRepairService->queryCategoryByNameAndLevel($cate_level_2_name, 2);
            if (Result::isSuccess($cate_2_record_ret) && $cate_2_record_ret->data != 0) {
                $cate_level_2_id = $cate_2_record_ret->data->id;
            } else {
                $editCategoryParams = new EditCategoryParams();
                $editCategoryParams->category_level = 2;
                $editCategoryParams->cate_level1_name = $cate_level_1_name;
                $editCategoryParams->cate_level1_id = $cate_level_1_id;
                $editCategoryParams->cate_level2_id = $cate_level_1_id;
                $editCategoryParams->cate_level2_name = $cate_level_2_name;
                $editCategoryParams->parent_id = $cate_level_1_id;
                $editCategoryParams->category_name = $cate_level_2_name;
                $createRet = $categoryRepairService->createCategory($editCategoryParams);
                if (Result::isSuccess($createRet)) {
                    $cate_level_2_id = $createRet->data;
                }
            }

//            $cate_3_record_ret = $categoryRepairService->queryProductCateByName($cate_level_3_name, 3);
//            if (Result::isSuccess($cate_3_record_ret) && $cate_3_record_ret->data != 0) {
//                $cate_level_3_id = $cate_3_record_ret->data->id;
//            } else {
//                $createRet = $categoryRepairService->createProductCate($cate_level_3_name, 3, $cate_level_2_id, $cate_level_1_name, $cate_level_1_id, $cate_level_2_name, $cate_level_2_id, $cate_level_3_name, 0);
//                if (Result::isSuccess($createRet)) {
//                    $cate_level_3_id = $createRet->data;
//                }
//            }
        }
        fclose($file);


    }


}