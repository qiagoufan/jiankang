<?php


use db\ElasticSearchClient;
use facade\request\feed\search\EsDataSynParams;
use facade\request\user\FollowParams;
use lib\PregHelper;
use lib\AppConstant;
use model\search\dto\SearchResultDTO;
use Mpdf\Mpdf;
use service\aliyun\impl\AliyunOssServiceImpl;
use service\aliyun\impl\VodServiceImpl;
use service\media\impl\ImageServiceImpl;
use service\search\impl\BizEsDataSynServiceImpl;
use service\search\impl\SearchServiceImpl;

class PdfTest extends FUR_Controller
{
    public function pdf()
    {

        $mpdf = new Mpdf();
        try {
            $mpdf->allow_charset_conversion = true;
            $mpdf->autoScriptToLang = true;
            $mpdf->autoLangToFont = true;
            $mpdf->charset_in = 'utf-8';
            $mpdf->SetHTMLHeader($this->getHtmlHeader());
            $mpdf->WriteHTML('<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
<br/><br/>
<div ><br/><br/><br/><br/><br/><br/><br/><br/>
    <h1 align="center">公司名称</h1>
    <h3 align="center">Air Conditioning Maintenance Report</h3>
    <br/>
    <h2 align="center">维修类型:空调维修</h2>
    <br/><br/><br/>
    <h2 align="center">维修地址</h2>
    <h3 align="center">Maintenance Date:2020.08.04</h3>

</div>

<pagebreak></pagebreak>
<div ><br/><br/><br/><br/><br/><br/><br/><br/>
    <h1 align="center">公司名称</h1>
    <h3 align="center">Air Conditioning Maintenance Report</h3>
    <br/>
    <h2 align="center">维修类型:空调维修</h2>
    <br/><br/><br/>
    <h2 align="center">维修地址</h2>
    <h3 align="center">Maintenance Date:2020.08.04</h3>

</div>
<div style="position: absolute; bottom: 5px;">

This is text in a fixed position block element.
</div>
</body>

</html>');
        } catch (\Mpdf\MpdfException $e) {
        }
        try {
            $mpdf->Output();
        } catch (\Mpdf\MpdfException $e) {
        }
    }

    private function getHtmlHeader()
    {
        $headerPath = APP_PATH . 'static' . DIRECTORY_SEPARATOR . "lubmaster.png";
        return "<img src='$headerPath' width='50px' height='50px'><br /><hr/>";
    }


}