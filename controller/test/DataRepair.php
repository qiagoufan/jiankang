<?php


use facade\request\aliyun\media\CreateUploadVideoParams;
use facade\request\media\MediaVideoParams;
use facade\request\socialite\SocialiteParams;
use facade\request\user\FollowParams;
use facade\request\user\QueryUserCollectListParams;
use facade\response\Result;
use service\aliyun\impl\VodServiceImpl;
use service\media\impl\VideoServiceImpl;
use service\socialite\impl\SocialiteServiceImpl;
use service\user\impl\CollectServiceImpl;

class DataRepair extends FUR_Controller
{
    public function userCollectData()
    {

        $user_id = $this->get('user_id');

        $collectService = CollectServiceImpl::getInstance();

        $params = new QueryUserCollectListParams();
        $params->user_id = $user_id;
        $params->page_size = 1000;
        $params->index = 0;
        $collectRet = $collectService->queryUserCollectList($params);
        if (!Result::isSuccess($collectRet)) {
            $this->render($collectRet);
        }
        $collectList = $collectRet->data;
        /** @var \model\dto\UserCollectDTO $item */
        foreach ($collectList as $item) {
            $item->id = null;
            \model\UserCollectModel::getInstance()->saveRecordCache($item);
        }
    }

    public function follow()
    {

        $user_id = $this->get('user_id');
        $star_id = $this->get('star_id');
        $followParams = new FollowParams();
        $followParams->user_id = $user_id;
        $followParams->target_id = $star_id;
        $followParams->target_type = \model\dto\UserStarDTO::STAR_BIZ_TYPE;
        \service\user\impl\FollowServiceImpl::getInstance()->follow($followParams);

    }

    public function vodtest1()
    {

        $video_id = $this->get('video_id');

        $createUploadVideoParams = new CreateUploadVideoParams();
        $createUploadVideoParams->cover_url = null;
        $createUploadVideoParams->file_size = 1231;
        $createUploadVideoParams->file_name = 'aaa.mp4';
        $result = VodServiceImpl::getInstance()->createUploadVideo($createUploadVideoParams);

        $videoService = VideoServiceImpl::getInstance();

        $mediaVideoParams = new MediaVideoParams();
        $mediaVideoParams->video_id = $video_id;
        $mediaVideoParams->user_id = 134;
        $result = $videoService->getVideoInfo($mediaVideoParams);
        var_dump($result);
//        $this->render($result);
    }
}