<?php

use facade\request\message\NotifySwitchParams;
use facade\response\message\NotifySwitchConfigResp;
use facade\response\Result;
use service\message\impl\NotifyServiceImpl;

class Notify extends FUR_Controller
{

    public function __construct()
    {

    }


    public function getNotifySwitchConfig()
    {
        $notifyService = NotifyServiceImpl::getInstance();
        $result = $notifyService->getNotifyConfig($this->request('device_id'));
        $notifySwitchConfigResp = new NotifySwitchConfigResp();
        if ($result != null && $result->errorCode == 0) {
            $notifySwitchConfigResp->push_switch = $result->data->push_switch;
        }
        $result = new Result();
        $result->setSuccessWithResult($notifySwitchConfigResp);

        $this->render($result);
    }

    public function updateNotifySwitchConfig()
    {
        $result = new Result();
        $notifySwitchParams = $this->requestObject(NotifySwitchParams::class);
        $notifyService = NotifyServiceImpl::getInstance();
        $result = $notifyService->updateNotifySwitch($notifySwitchParams);
        $this->render($result);
    }
}