<?php

use facade\request\PageParams;
use facade\response\base\PageInfoResp;
use facade\response\message\MessageListResp;
use facade\response\message\MessageResp;
use facade\response\message\UnreadRemindResp;
use facade\response\Result;
use lib\TimeHelper;
use model\dto\UserMessageDTO;
use service\message\impl\MessageServiceImpl;
use \facade\request\message\GetInteractiveMessageListParams;
use \facade\response\message\MessageInteractiveListResp;
use \facade\response\message\MessageInteractiveLikeResp;
use \service\user\impl\UserServiceImpl;

class Message extends FUR_Controller
{
    private $userId;

    public function __construct()
    {
    }

    public function getUnreadRemind()
    {
        $result = new Result();
        $userId = $this->getRequestUserId(false);
        $unreadRemindResp = new UnreadRemindResp();
        $messageService = MessageServiceImpl::getInstance();
        $unReadRet = $messageService->getUserUnreadMessage($userId);
        if (!Result::isSuccess($unReadRet)) {
            $result->setSuccessWithResult($unreadRemindResp);
            $this->render($result);
        }
        $red_point = $unReadRet->data > 0;
        $unreadRemindResp->badge_num = $unReadRet->data;
        $unreadRemindResp->red_point = $red_point;
        $result->setSuccessWithResult($unreadRemindResp);
        $this->render($result);
    }

    public function getMessageList()
    {
        $result = new Result();
        $userId = $this->getRequestUserId(false);
        $MessageListResp = new  MessageListResp();
        /** @var PageParams $pageParams */
        $pageParams = $this->requestObject(PageParams::class);
        $messageService = MessageServiceImpl::getInstance();
        $messageList = $messageService->getAllMessageList($pageParams, $userId);
        $messageArray = [];
        if ($messageList->errorCode == 0 && $messageList->data != null) {


            /** @var UserMessageDTO $userMessageDTO */
            foreach ($messageList->data as $userMessageDTO) {
                $messageResp = new MessageResp();
                FUR_Core::copyProperties($userMessageDTO, $messageResp);
                if ($messageResp->publish_timestamp == 0) {
                    $messageResp->publish_timestamp = $messageResp->created_timestamp;
                }
                $messageResp->publish_timestamp_str = TimeHelper::getPersonalTimeString($messageResp->publish_timestamp);
                array_push($messageArray, $messageResp);
            }


        }
        $MessageListResp->message_list = $messageArray;
        $pageInfo = PageInfoResp::buildPageInfoRespBaseLine($pageParams->index, $pageParams->page_size, count($messageArray), $pageParams->page_size);
        $MessageListResp->page_info = $pageInfo;
        $result->setSuccessWithResult($MessageListResp);
        $this->render($result);
    }


    /**
     * 互动消息通知列表
     */

    public function getMessageInteractiveList()
    {
        $result = new Result();
        $getInteractiveMessageListParams = $this->requestObject(GetInteractiveMessageListParams::class);
        $userId = $this->getRequestUserId(true);
        $interactiveMessageType = $getInteractiveMessageListParams->type;
        $index = $getInteractiveMessageListParams->index;
        $pageSize = $getInteractiveMessageListParams->page_size;

        $MessageInteractiveListResp = new MessageInteractiveListResp();
        $messageServiceImpl = MessageServiceImpl::getInstance();
        $InteractiveMessageListRet = $messageServiceImpl->getUserInteractiveMessageList($interactiveMessageType, $userId, $index, $pageSize);


        if (Result::isSuccess($InteractiveMessageListRet)) {

            foreach ($InteractiveMessageListRet->data as $messageInteractiveDTO) {

                $sendUserId = $messageInteractiveDTO->send_user_id;
                $userServiceImpl = UserServiceImpl::getInstance();
                $userInfoRet = $userServiceImpl->getValidUserInfo($sendUserId);

                if (Result::isSuccess($userInfoRet)) {
                    $userInfoDTO = $userInfoRet->data;
                }

                //todo:优化，移除假数据
                if ($interactiveMessageType == GetInteractiveMessageListParams::MESSAGE_LIKE_TYPE) {
                    $messageInteractiveLike = new MessageInteractiveLikeResp();
                    $messageInteractiveLike->like_user_id = $userInfoDTO->id;
                    $messageInteractiveLike->like_user_avatar = $userInfoDTO->avatar;
                    $messageInteractiveLike->like_user_name = $userInfoDTO->nick_name;
                    $messageInteractiveLike->created_timestamp = TimeHelper::getPersonalTimeString($messageInteractiveDTO->created_timestamp);
                    $messageInteractiveLike->feed_image = $messageInteractiveDTO->image;
                    $messageInteractiveLike->feed_content = '测试内容哈哈哈哈';
                    array_push($MessageInteractiveListResp->interactive_message_list, $messageInteractiveLike);
                }

            }
        }


        $messageLikeCountRet = $messageServiceImpl->getUserInteractiveMessageCount($userId);
        if (Result::isSuccess($messageLikeCountRet)) {
            $MessageInteractiveListResp->interactive_message_like_count = $messageLikeCountRet->data;
        }

        $MessageInteractiveListResp->like_people_count = 1;
        $pageInfo = PageInfoResp::buildPageInfoRespBaseLine($getInteractiveMessageListParams->index, $getInteractiveMessageListParams->page_size, count($InteractiveMessageListRet->data), $getInteractiveMessageListParams->page_size);
        $MessageInteractiveListResp->page_info = $pageInfo;

        $result->setSuccessWithResult($MessageInteractiveListResp);
        $this->render($result);

    }


}