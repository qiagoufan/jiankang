<?php

use facade\request\feed\heat\TriggerEventParams;
use facade\request\interactive\CreateCommentParams;
use facade\request\interactive\CreateReplyParams;
use facade\request\interactive\QueryInteractiveCommentListParams;
use facade\request\interactive\QueryInteractiveReplyListParams;
use facade\request\interactive\ShareParams;
use facade\response\base\PageInfoResp;
use facade\response\interactive\InteractiveCommentBaseListResp;
use facade\response\interactive\InteractiveCommentListResp;
use facade\response\interactive\InteractiveReplyBaseListResp;
use facade\response\interactive\InteractiveReplyListResp;
use facade\response\Result;
use lib\TimeHelper;
use lib\AppConstant;
use model\interactive\dto\InteractiveCommentDTO;
use model\interactive\dto\InteractiveReplyDTO;
use model\tag\dto\TagEntityDTO;
use service\security\impl\SecurityCheckServiceImpl;
use \service\tag\impl\TagServiceImpl;
use service\aliyun\impl\AcsServiceImpl;
use service\feed\impl\FeedHeatServiceImpl;
use service\feed\impl\FeedServiceImpl;
use service\interactive\impl\InteractiveServiceImpl;
use service\user\impl\UserServiceImpl;
use \facade\request\interactive\CreateLikeParams;
use \facade\request\interactive\CancelLikeParams;
use \facade\request\tag\QueryTagEntityListParams;
use \service\user\impl\StarServiceImpl;

class Interactive extends FUR_Controller
{

    /**
     * 发送评论
     */
    public function sendComment()
    {
        $result = new Result();
        /** @var CreateCommentParams $createCommentParams */
        $createCommentParams = $this->requestObject(CreateCommentParams::class);
        $interactiveServiceImpl = InteractiveServiceImpl::getInstance();
        $userId = $this->getRequestUserId();
        $checkRet = SecurityCheckServiceImpl::getInstance()->checkContent($createCommentParams->content_text);
        if (!Result::isSuccess($checkRet)) {
            $this->render($checkRet);
        }

//        $userCheckRet = UserServiceImpl::getInstance()->bindPhoneCheck($userId);
//        if (!Result::isSuccess($userCheckRet)) {
//            $this->render($userCheckRet);
//        }

        $userInfoRet = UserServiceImpl::getInstance()->getValidUserInfo($userId);
        if (!Result::isSuccess($userInfoRet)) {
            $result->setError(\Config_Error::ERR_USER_NOT_EXIST);
            $this->render($result);
        }

        /**
         * 全站黑名单
         */
//        $blackUserRet = UserServiceImpl::getInstance()->queryBlackUserById($userId);
//        if (Result::isSuccess($blackUserRet)) {
//            $result->setError(\Config_Error::ERR_BLACK_USER_SEND_COMMENT_IS_NOT_ALLOWED);
//            $this->render($result);
//        }

        if ($createCommentParams->content_text == "") {
            $result->setError(\Config_Error::ERR_INTERACTIVE_COMMENT_IS_NULL);
            $this->render($result);
        }
//        //检查是否拉黑
//        if ($this->isBlockByAuthor($createCommentParams->biz_type, $createCommentParams->biz_id, $userId)) {
//            $result->setError(\Config_Error::ERR_INTERACTIVE_IS_BLOCK);
//            $this->render($result);
//        }


        //看评论内容是否命中敏感词
//        $acsContentCheckRet = AcsServiceImpl::getInstance()->checkContent($createCommentParams->content_text);
//        if (Result::isSuccess($acsContentCheckRet) && $acsContentCheckRet->data == true) {
//            $result->setError(\Config_Error::ERR_INTERACTIVE_COMMENT_IS_BLOCK);
//            $this->render($result);
//        }


        if (!$createCommentParams->biz_id || !$createCommentParams->biz_type) {
            $result->setError(\Config_Error::ERR_INTERACTIVE_COMMENT_IS_INVALID);
            $this->render($result);
        }

        $sendCommentRet = $interactiveServiceImpl->createComment($userId, $createCommentParams);


        $result->setSuccessWithResult($sendCommentRet->data);
        $this->render($result);
    }


    /**
     * 删除评论
     */
    public function deleteComment()
    {
        $result = new Result();
        $userId = $this->getRequestUserId();

        $commentId = $this->request('comment_id');
        $interactiveServiceImpl = InteractiveServiceImpl::getInstance();
        //将评论下面的回复删除
        if ($commentId == null || $userId == null) {
            $result->setError(Config_Error::ERR_DELETE_COMMENT_PARAMS_INFO_NO_RIGHT);
            $this->render($result);
        }
        $commentRet = $interactiveServiceImpl->queryCommentById($commentId);
        /** @var InteractiveCommentDTO $interactiveCommentDTO */
        $interactiveCommentDTO = $commentRet->data;
        if (Result::isSuccess($commentRet) == false) {
            $result->setSuccessWithResult(true);
            $this->render($result);
        }


        $delCommentRet = $interactiveServiceImpl->delComment($userId, $commentId, $interactiveCommentDTO->biz_id);
        if (!Result::isSuccess($delCommentRet)) {
            $result->setError(Config_Error::ERR_DELETE_FEED_NO_RIGHT);
            $this->render($result);
        }

        /** @var InteractiveCommentDTO $interactiveCommentDTO */


        $interactiveServiceImpl->deleteReplyByComment($interactiveCommentDTO->biz_id, $interactiveCommentDTO->biz_type, $commentId);
        //更新overview
        if ($interactiveCommentDTO != null) {
            $interactiveServiceImpl->updateOverviewCount($interactiveCommentDTO->biz_id, $interactiveCommentDTO->biz_type, 'comment', -1);
        }


        $result->setSuccessWithResult($delCommentRet->data);
        $this->render($result);

    }


    public function sendReply()
    {
        $result = new Result();
        /** @var CreateReplyParams $createReplyParams */
        $createReplyParams = $this->requestObject(CreateReplyParams::class);
        $interactiveServiceImpl = InteractiveServiceImpl::getInstance();
        $userId = $this->getRequestUserId();


        $userInfoRet = UserServiceImpl::getInstance()->getValidUserInfo($userId);
        if (!Result::isSuccess($userInfoRet)) {
            $result->setError(\Config_Error::ERR_USER_NOT_EXIST);
            $this->render($result);
        }

        /**
         * 全站黑名单
         */
        $blackUserRet = UserServiceImpl::getInstance()->queryBlackUserById($userId);
        if (Result::isSuccess($blackUserRet)) {
            $result->setError(\Config_Error::ERR_BLACK_USER_SEND_COMMENT_IS_NOT_ALLOWED);
            $this->render($result);
        }

        //看评论内容是否命中敏感词
        $acsContentCheckRet = AcsServiceImpl::getInstance()->checkContent($createReplyParams->reply_content);
        if (Result::isSuccess($acsContentCheckRet) && $acsContentCheckRet->data == true) {
            $result->setError(\Config_Error::ERR_INTERACTIVE_REPLY_IS_BLOCK);
            $this->render($result);
        }

        if (!$createReplyParams->biz_id || !$createReplyParams->biz_type || !$createReplyParams->comment_id || !$userId) {
            $result->setError(\Config_Error::ERR_INTERACTIVE_REPLY_IS_INVALID);
            $this->render($result);
        }

        //检查是否拉黑
        if ($this->isBlockByAuthor($createReplyParams->biz_type, $createReplyParams->biz_id, $userId, $createReplyParams->comment_id, $createReplyParams->reply_to_id)) {
            $result->setError(\Config_Error::ERR_INTERACTIVE_IS_BLOCK);
            $this->render($result);
        }

        $createReplyRet = $interactiveServiceImpl->createReply($userId, $createReplyParams);
        if (!Result::isSuccess($createReplyRet)) {
            $this->render($createReplyRet);
        }

        /**
         * 增加热度
         */
        if ($createReplyParams->biz_type == AppConstant::BIZ_TYPE_FEED) {
            $triggerEventParams = new  TriggerEventParams();
            $triggerEventParams->feed_id = $createReplyParams->biz_id;
            $triggerEventParams->event_type = FeedHeatServiceImpl::EVENT_COMMENT;
            FeedHeatServiceImpl::getInstance()->triggerEvent($triggerEventParams);
        }

        $result->setSuccessWithResult($createReplyRet->data);
        $this->render($result);
    }


    /**
     * 获取评论列表
     */
    public function getInteractiveCommentList()
    {
        $result = new Result();
        $queryInteractiveCommentListParams = $this->requestObject(QueryInteractiveCommentListParams::class);
        $interactiveServiceImpl = InteractiveServiceImpl::getInstance();
        $interactiveCommentListRet = $interactiveServiceImpl->queryInteractiveCommentList($queryInteractiveCommentListParams);

        if (!Result::isSuccess($interactiveCommentListRet)) {
            $this->render($interactiveCommentListRet);
        }

        $interactiveCommentListResp = new InteractiveCommentListResp();
        foreach ($interactiveCommentListRet->data as $key => $interactiveCommentDTO) {
            $interactiveCommentBaseListResp = $this->conversionInteractiveCommentListResp($interactiveCommentDTO, $queryInteractiveCommentListParams);

            if ($interactiveCommentBaseListResp != null) {
                array_push($interactiveCommentListResp->comment_base_list, $interactiveCommentBaseListResp);
            }
        }
        $queryCommentOverviewRet = $interactiveServiceImpl->queryCommentCountByBizId($queryInteractiveCommentListParams->biz_type, $queryInteractiveCommentListParams->biz_id);
        $commentCount = 0;
        if (Result::isSuccess($queryCommentOverviewRet)) {
            $commentCount = $queryCommentOverviewRet->data;
        }

        $pageInfo = PageInfoResp::buildPageInfoResp($queryInteractiveCommentListParams->index, $pageSize = 10, count($interactiveCommentListRet->data), $commentCount);
        $pageInfo->count = $commentCount;
        $interactiveCommentListResp->page_info = $pageInfo;
        $result->setSuccessWithResult($interactiveCommentListResp);
        $this->render($result);

    }

    public function getCommentDetailById()
    {
        $result = new Result();
        $commentId = $this->request('comment_id');
        $interactiveServiceImpl = InteractiveServiceImpl::getInstance();
        $queryCommentDetailRet = $interactiveServiceImpl->queryCommentDetailById($commentId);
        if (!Result::isSuccess($queryCommentDetailRet)) {
            $result->setError(\Config_Error::ERR_INTERACTIVE_COMMENT_INFO_IS_NOT_EXIST);
            $this->render($result);
        }
        $interactiveCommentBaseListResp = $this->conversionInteractiveCommentListResp($queryCommentDetailRet->data);
        $result->setSuccessWithResult($interactiveCommentBaseListResp);
        $this->render($result);

    }

    /**
     * 删除回复
     */
    public function deleteReply()
    {
        $result = new Result();
        $replyId = $this->request('reply_id');
        //查询详情
        $interactiveServiceImpl = InteractiveServiceImpl::getInstance();
        $replyRet = $interactiveServiceImpl->queryReplyById($replyId);
        if (Result::isSuccess($replyRet) == false) {
            $result->setSuccessWithResult(true);
            $this->render($result);
        }

        /**
         * 删除回复
         */
        $userId = $this->getRequestUserId();
        $result = $interactiveServiceImpl->delReply($userId, $replyId);
        if (!Result::isSuccess($result)) {
            $this->render($result);
        }

        /** @var InteractiveReplyDTO $replyDTO */
        $replyDTO = $replyRet->data;

        $interactiveServiceImpl->updateOverviewCount($replyDTO->biz_id, $replyDTO->biz_type, 'reply', -1);

        /**
         * 增加热度
         */
        if ($replyRet->data->biz_type == AppConstant::BIZ_TYPE_FEED) {
            $triggerEventParams = new  TriggerEventParams();
            $triggerEventParams->feed_id = $replyRet->data->biz_id;
            $triggerEventParams->event_type = FeedHeatServiceImpl::EVENT_COMMENT;
            $triggerEventParams->action_type = FeedHeatServiceImpl::ACTION_TYPE_DECR;
            FeedHeatServiceImpl::getInstance()->triggerEvent($triggerEventParams);
        }

        $result->setSuccessWithResult(true);
        $this->render($result);

    }

    /**
     * @param InteractiveCommentDTO $interactiveCommentDTO
     * @return InteractiveCommentBaseListResp|null
     * 转换评论列表
     */
    private function conversionInteractiveCommentListResp(InteractiveCommentDTO &$interactiveCommentDTO, ?QueryInteractiveCommentListParams $queryInteractiveCommentListParams = null): ?InteractiveCommentBaseListResp
    {
        $interactiveCommentBaseListResp = new InteractiveCommentBaseListResp();
        $interactiveCommentBaseListResp->comment_content = json_decode($interactiveCommentDTO->comment_content);
        $interactiveCommentBaseListResp->comment_id = $interactiveCommentDTO->id;
        $interactiveCommentBaseListResp->user_id = $interactiveCommentDTO->comment_user_id;
        $interactiveCommentBaseListResp->created_timestamp = $interactiveCommentDTO->created_timestamp;
        $interactiveCommentBaseListResp->publish_time_str = TimeHelper::getPersonalTimeString($interactiveCommentDTO->created_timestamp);


        $userId = 0;
        if ($queryInteractiveCommentListParams != null) {
            $userId = $queryInteractiveCommentListParams->user_id;
        }

        //屏蔽黑名单
        $blockRet = UserServiceImpl::getInstance()->isBlock($userId, $interactiveCommentDTO->comment_user_id);
        if (Result::isSuccess($blockRet) && $blockRet->data) {
            return null;
        }


        $userInfoRet = UserServiceImpl::getInstance()->getValidUserInfo($interactiveCommentDTO->comment_user_id);
        if (!Result::isSuccess($userInfoRet)) {
            return null;
        }
        $userInfo = $userInfoRet->data;
        $interactiveCommentBaseListResp->user_avatar = $userInfo->avatar;
        $interactiveCommentBaseListResp->user_name = $userInfo->nick_name;

        $queryInteractiveReplyListParams = new QueryInteractiveReplyListParams();
        $queryInteractiveReplyListParams->comment_id = $interactiveCommentDTO->id;
        $queryInteractiveReplyListParams->user_id = $userId;
        $interactiveCommentBaseListResp->reply_list = $this->getInteractiveReplyList($interactiveCommentDTO->id, $queryInteractiveReplyListParams, $interactiveCommentDTO->biz_id, $interactiveCommentDTO->biz_type);
        return $interactiveCommentBaseListResp;

    }


    /**
     * @param $commentId
     * @param $queryInteractiveReplyListParams
     * @param $bizId
     * @param $bizType
     * @return InteractiveReplyListResp|null
     * 获取回复列表
     */
    private function getInteractiveReplyList($commentId, $queryInteractiveReplyListParams, $bizId, $bizType): ?InteractiveReplyListResp
    {

        $interactiveServiceImpl = InteractiveServiceImpl::getInstance();
        $queryInteractiveReplyListRet = $interactiveServiceImpl->queryInteractiveReplyList($queryInteractiveReplyListParams);

        if (!Result::isSuccess($queryInteractiveReplyListRet)) {
            return null;
        }


        $userId = 0;
        if ($queryInteractiveReplyListParams != null) {
            $userId = $queryInteractiveReplyListParams->user_id;
        }


        $interactiveReplyListResp = new InteractiveReplyListResp();
        /** @var InteractiveReplyDTO $interactiveReplyDTO */
        foreach ($queryInteractiveReplyListRet->data as $interactiveReplyDTO) {
            $interactiveReplyBaseListResp = $this->conversionInteractiveReplyListResp($interactiveReplyDTO);
            if ($interactiveReplyBaseListResp != null) {
                array_push($interactiveReplyListResp->reply_base_list, $interactiveReplyBaseListResp);
            }
            //屏蔽黑名单
            $blockRet = UserServiceImpl::getInstance()->isBlock($userId, $interactiveReplyDTO->reply_user_id);
            if (Result::isSuccess($blockRet) && $blockRet->data) {
                return null;
            }
        }

        $queryCommentOverViewRet = $interactiveServiceImpl->queryInteractiveOverview($bizType, $bizId);
        if (!Result::isSuccess($queryCommentOverViewRet)) {
            return null;
        }


        if ($this->request('index')) {
            $queryInteractiveReplyListParams->index = $this->request('index');
        }
        $replyCount = 0;
        $replyCountRet = $interactiveServiceImpl->queryReplyCountByComment($commentId, $bizId, $bizType);
        if (Result::isSuccess($replyCountRet)) {
            $replyCount = $replyCountRet->data;
        }
        $pageInfo = PageInfoResp::buildPageInfoResp($queryInteractiveReplyListParams->index, $pageSize = 10, count($queryInteractiveReplyListRet->data), $replyCount);

        $interactiveReplyListResp->page_info = $pageInfo;

        return $interactiveReplyListResp;


    }


    private function isBlockByAuthor(string $bizType, int $bizId, int $userId, ?int $commentId = 0, ?int $replyId = 0): bool
    {
        if ($bizType != FeedInfoDTO::FEED_BIZ_TYPE) {
            return false;
        }

        //检查feed的作者有没有拉黑
        $feedInfoRet = FeedServiceImpl::getInstance()->getFeedDetail($bizId);
        if (!Result::isSuccess($feedInfoRet)) {
            return false;
        }
        $blockRet = UserServiceImpl::getInstance()->isBlock($feedInfoRet->data->author_id, $userId);
        if (Result::isSuccess($blockRet) && $blockRet->data) {
            return true;
        }

        //检查评论的人有没有把拉黑
        if ($commentId == 0) {
            return false;
        }
        $commentInfoRet = InteractiveServiceImpl::getInstance()->queryCommentById($commentId);
        if (!Result::isSuccess($commentInfoRet)) {
            return false;
        }

        $blockRet = UserServiceImpl::getInstance()->isBlock($commentInfoRet->data->comment_user_id, $userId);
        if (Result::isSuccess($blockRet) && $blockRet->data) {
            return true;
        }

        //检查你要回复的人有没有把拉黑
        if ($replyId == 0) {
            return false;
        }

        $replyRet = InteractiveServiceImpl::getInstance()->queryReplyById($replyId);
        if (!Result::isSuccess($replyRet)) {
            return false;
        }
        $blockRet = UserServiceImpl::getInstance()->isBlock($replyRet->data->reply_user_id, $userId);
        if (Result::isSuccess($blockRet) && $blockRet->data) {
            return true;
        }

        return false;
    }

    /**
     * @param InteractiveReplyDTO $interactiveReplyDTO
     * @return InteractiveReplyBaseListResp|null
     * 转换评论列表
     */

    private
    function conversionInteractiveReplyListResp(InteractiveReplyDTO &$interactiveReplyDTO): ?InteractiveReplyBaseListResp
    {

        $interactiveReplyBaseListResp = new InteractiveReplyBaseListResp();
        $interactiveReplyBaseListResp->created_timestamp = $interactiveReplyDTO->created_timestamp;
        $interactiveReplyBaseListResp->reply_content = json_decode($interactiveReplyDTO->reply_content);
        $interactiveReplyBaseListResp->reply_id = $interactiveReplyDTO->id;
        $interactiveReplyBaseListResp->user_id = $interactiveReplyDTO->reply_user_id;

        $userInfoRet = UserServiceImpl::getInstance()->getValidUserInfo($interactiveReplyDTO->reply_user_id);
        if (!Result::isSuccess($userInfoRet)) {
            return null;
        }

        $interactiveReplyBaseListResp->reply_status = $interactiveReplyDTO->reply_status;
        $interactiveReplyBaseListResp->user_avatar = $userInfoRet->data->avatar;
        $interactiveReplyBaseListResp->user_name = $userInfoRet->data->nick_name;
        $interactiveReplyBaseListResp->reply_to_id = $interactiveReplyDTO->reply_to_id;
        $interactiveReplyBaseListResp->reply_type = InteractiveReplyBaseListResp::REPLY_TYPE_TO_COMMENT;
        $interactiveReplyBaseListResp->publish_time_str = TimeHelper::getPersonalTimeString($interactiveReplyBaseListResp->created_timestamp);

        //如果@了人
        if (($interactiveReplyDTO->reply_to_id != null) && ($interactiveReplyDTO->reply_to_user_id != null)) {
            $replyInfoRet = InteractiveServiceImpl::getInstance()->queryReplyById($interactiveReplyDTO->reply_to_id);
            if (!Result::isSuccess($replyInfoRet)) {
                return $interactiveReplyBaseListResp;
            }
            $replyInfo = $replyInfoRet->data;
            $replyUserInfo = UserServiceImpl::getInstance()->getValidUserInfo($interactiveReplyDTO->reply_to_user_id);
            if (!Result::isSuccess($replyUserInfo)) {
                return $interactiveReplyBaseListResp;
            }
            $interactiveReplyBaseListResp->reply_to_user_id = $replyInfo->reply_user_id;
            $interactiveReplyBaseListResp->reply_to_user_name = $replyUserInfo->data->nick_name;
            $interactiveReplyBaseListResp->reply_to_user_content = json_decode($replyInfo->reply_content);
            $interactiveReplyBaseListResp->reply_to_parent_status = $replyInfo->reply_status;
            $interactiveReplyBaseListResp->reply_type = InteractiveReplyBaseListResp::REPLY_TYPE_REPLY;
        }

        return $interactiveReplyBaseListResp;
    }


    /**
     * 点赞
     */
    public function addLike()
    {
        $result = new Result();
        /** @var CreateLikeParams $createLikeParams */
        $createLikeParams = $this->requestObject(CreateLikeParams::class);
        $userId = $this->getRequestUserId();

        /**
         * 参数校验
         */
        if ($createLikeParams->biz_id == null) {
            $result->setError(\Config_Error::ERR_LIKE_BIZ_ID_NOT_EXIST);
            $this->render($result);
        }


        $userInfoRet = UserServiceImpl::getInstance()->getValidUserInfo($userId);
        if (!Result::isSuccess($userInfoRet)) {
            $result->setError(\Config_Error::ERR_USER_NOT_EXIST);
            $this->render($result);
        }
        $createLikeParams->user_id = $userId;

        $result = InteractiveServiceImpl::getInstance()->addLike($createLikeParams);


        $this->render($result);

    }

    public function addShare()
    {
        $result = new Result();
        /** @var ShareParams $shareParams */
        $shareParams = $this->requestObject(ShareParams::class);
        $result = InteractiveServiceImpl::getInstance()->addShare($shareParams);
        $this->render($result);

    }


    /**
     * 取消点赞
     */
    public function cancelLike()
    {
        $result = new Result();
        /** @var CancelLikeParams $cancelLikeParams */
        $cancelLikeParams = $this->requestObject(CancelLikeParams::class);
        $userId = $this->getRequestUserId();


        /**
         * 参数校验
         */
        if ($cancelLikeParams->biz_id == null) {
            $result->setError(\Config_Error::ERR_LIKE_BIZ_ID_NOT_EXIST);
            $this->render($result);
        }


        $userInfoRet = UserServiceImpl::getInstance()->getValidUserInfo($userId);
        if (!Result::isSuccess($userInfoRet)) {
            $result->setError(\Config_Error::ERR_USER_NOT_EXIST);
            $this->render($result);
        }


        $result = InteractiveServiceImpl::getInstance()->cancelLike($userId, $cancelLikeParams->biz_id, $cancelLikeParams->biz_type);
        $this->render($result);
    }


}

