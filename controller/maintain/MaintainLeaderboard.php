<?php

use business\star\StarConversionUtil;
use business\user\UserInfoConversionUtil;
use facade\request\user\FollowParams;
use facade\response\base\PageInfoResp;
use facade\response\maintain\contribution\StarContributionInfoResp;
use facade\response\maintain\contribution\UserContributionResp;
use facade\response\maintain\MaintainLeaderboardInfoResp;
use facade\response\maintain\MaintainLeaderboardTaskResp;
use facade\response\maintain\MaintainrContributionLeaderboardResp;
use facade\response\Result;
use lib\ClientRouteHelper;
use model\dto\MaintainLeaderboardRecordDTO;
use model\dto\UserStarDTO;
use service\maintain\impl\MaintainLeaderboardServiceImpl;
use service\user\impl\FollowServiceImpl;
use service\user\impl\StarServiceImpl;
use service\user\impl\UserServiceImpl;

class MaintainLeaderboard extends FUR_Controller
{

    const FEED_LEADERBOARD_LIST = [
        MaintainLeaderboardRecordDTO::RECORD_TYPE_POST_FEED,
        MaintainLeaderboardRecordDTO::RECORD_TYPE_DELETE_FEED,
        MaintainLeaderboardRecordDTO::RECORD_TYPE_DENY_FEED,
        MaintainLeaderboardRecordDTO::RECORD_TYPE_APPROVE_FEED];

    public function getMaintainLeaderboardInfo()
    {
        $result = new Result();
        $userId = $this->getRequestUserId(false);
        $starId = $this->request('star_id', 0);

        $starInfoRet = StarServiceImpl::getInstance()->getStarInfo($starId);
        if (!Result::isSuccess($starInfoRet)) {
            $result->setError(Config_Error::ERR_STAR_IS_NOT_EXIST);
            $this->render($result);
        }
        /** @var UserStarDTO $starInfoDTO */
        $starInfoDTO = $starInfoRet->data;

        $isFollow = 0;
        $followParams = new  FollowParams();
        $followParams->user_id = $userId;
        $followParams->target_id = $starId;
        $followParams->target_type = UserStarDTO::STAR_BIZ_TYPE;
        $followStatusRet = FollowServiceImpl::getInstance()->getFollowStatus($followParams);
        if (Result::isSuccess($followStatusRet)) {
            $isFollow = intval($followStatusRet->data);
        }


        $maintainLeaderboardInfo = new MaintainLeaderboardInfoResp();
        $result->setSuccessWithResult($maintainLeaderboardInfo);
        /**
         * 获取人气排名
         */
        $leaderboard_rank = -1;
        $starRankRet = MaintainLeaderboardServiceImpl::getInstance()->getStarPopularityRank($starId);
        if (Result::isSuccess($starRankRet)) {
            $leaderboard_rank = $starRankRet->data;
        }


        /**
         * 获取人气值
         */
        $leaderboard_value = 0;
        $leaderboardValueRet = MaintainLeaderboardServiceImpl::getInstance()->getStarPopularityValue($starId);
        if (Result::isSuccess($starRankRet)) {
            $leaderboard_value = $leaderboardValueRet->data == null ? 0 : $leaderboardValueRet->data;
        }

        $maintainLeaderboardInfo->leaderboard_title = '打榜任务';
        $maintainLeaderboardInfo->leaderboard_icon = 'https://ipxcdn.jfshare.com/ipxmall/a23342c035e12974b5195a6d405cc991';
        $maintainLeaderboardInfo->leaderboard_summary = StarConversionUtil::buildLeaderboardSummary($leaderboard_value, $leaderboard_rank);

        $maintainLeaderboardInfo->task_list = [];
        //关注打卡
        $task = new MaintainLeaderboardTaskResp();
        $task->title = '关注' . $starInfoDTO->star_name;
        $task->summary = '人气值+1；完成' . $isFollow . '/1';
        if ($isFollow) {
            $task->button_name = '已关注';
            $task->button_status = 0;
        } else {
            $task->button_name = '未关注';
            $task->button_status = 1;
        }

        array_push($maintainLeaderboardInfo->task_list, $task);

        $recordTypeList = self::FEED_LEADERBOARD_LIST;
        //发内容打卡
        $UerStarPopularityValueByTypeRet = MaintainLeaderboardServiceImpl::getInstance()->getUserStarPopularityValueByType($starId, $userId, $recordTypeList);
        $myPostFeedScore = 0;
        if (Result::isSuccess($UerStarPopularityValueByTypeRet)) {
            $myPostFeedScore = $UerStarPopularityValueByTypeRet->data;
        }
        $task = new MaintainLeaderboardTaskResp();
        $task->title = '贡献' . $starInfoDTO->star_name . '相关内容';
        $task->summary = '每条视频/图片，+2人气值；已贡献：' . $myPostFeedScore;
        $task->button_name = '去贡献';
        $task->button_status = 1;
        $task->button_link = ClientRouteHelper::buildUploadLink();
        array_push($maintainLeaderboardInfo->task_list, $task);
        $this->render($result);
    }


    public function getMaintainContributionLeaderboard()
    {
        $result = new Result();
        $starId = $this->request('star_id', 0);
        $userId = $this->getRequestUserId(false);
//        $userId = 900041;
        $index = $this->request('index', 0);
        $pageSize = 20;
        //明星信息
        $starInfoRet = StarServiceImpl::getInstance()->getStarInfo($starId);
        if (!Result::isSuccess($starInfoRet)) {
            $result->setError(Config_Error::ERR_STAR_IS_NOT_EXIST);
            $this->render($result);
        }
        $leaderboardResp = new MaintainrContributionLeaderboardResp();
        $leaderboardResp->list_name = '实时热榜';
        $result->setSuccessWithResult($leaderboardResp);

        $maintainLeaderboardService = MaintainLeaderboardServiceImpl::getInstance();
        $leaderboardResp->user_contribution = UserInfoConversionUtil::getInstance()->buildContributionResp($userId, $starId);

        //明星总贡献值
        $starContributionTotalValueRet = $maintainLeaderboardService->getStarContributionTotalValue($starId);
        $starContribution = new StarContributionInfoResp();
        $starContribution->sum_score = $starContributionTotalValueRet->data;
        $starContribution->background = $starInfoRet->data->official_background;
        $starContribution->leaderboard_name = $starInfoRet->data->star_name . '粉丝贡献榜';
        $leaderboardResp->starInfo = $starContribution;

        //贡献值排行榜
        $leaderboardRet = $maintainLeaderboardService->getStarContributionLeaderboard($starId, $index, $pageSize);
        if (Result::isSuccess($leaderboardRet)) {
            $userIdList = $leaderboardRet->data;
            $userContributionList = [];
            foreach ($userIdList as $userId) {
                array_push($userContributionList, UserInfoConversionUtil::getInstance()->buildContributionResp($userId, $starId));
            }
            $leaderboardResp->user_list = $userContributionList;
        }

        $leaderboardResp->page_info = PageInfoResp::buildPageInfoRespBaseLine($index, $pageSize, count($leaderboardResp->user_list));
        $this->render($result);

    }


}