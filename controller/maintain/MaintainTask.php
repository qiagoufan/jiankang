<?php

use facade\request\maintain\CreateMaintainUserTaskRecordParams;
use  service\maintain\impl\MaintainTaskServiceImpl;
use facade\response\Result;
use business\star\StarConversionUtil;
use facade\request\maintain\GetMaintainTaskRankParams;
use facade\response\base\PageInfoResp;
use facade\response\user\StarListResp;
use facade\request\maintain\GetUserTaskHistoryForStarParams;
use service\user\impl\UserServiceImpl;
use  facade\response\maintain\MaintainTaskHistoryListResp;
use business\maintain\MaintainStarTaskConverisonUtil;
use facade\request\user\FollowParams;
use service\user\impl\FollowServiceImpl;
use model\dto\UserStarDTO;

class MaintainTask extends FUR_Controller
{
    const DEFAULT_PAGE_SIZE = 10;

    /**
     * 参加明星打卡任务
     */
    public function createUserTaskRecord()
    {
        $result = new Result();

        /**
         * 参数校验
         */
        $CreateUserTaskRecordParams = $this->requestObject(CreateMaintainUserTaskRecordParams::class);
        if ($CreateUserTaskRecordParams->star_id == null || $CreateUserTaskRecordParams->task_id == null) {
            $result->setError(\Config_Error::ERR_CREATE_TASK_PARAMS_IS_NOT_FULL);
            $this->render($result);
        }

        $userId = $this->getRequestUserId();
        if ($userId == 0) {
            $result->setError(\Config_Error::ERR_USER_NOT_EXIST);
            $this->render($result);
        }

        /**
         * 先检查用户是否关注star，关注才能打卡
         */
        $followParams = new  FollowParams();
        $followParams->user_id = $userId;
        $followParams->target_id = $CreateUserTaskRecordParams->star_id;
        $followParams->target_type = UserStarDTO::STAR_BIZ_TYPE;
        $followStatusRet = FollowServiceImpl::getInstance()->getFollowStatus($followParams);

        if ($followStatusRet->data == null) {
            $result->setError(\Config_Error::ERR_NEED_FOLLOW_IF_CREATE_TASK_RECORD);
            $this->render($result);
        }


        $taskInfoRet = MaintainTaskServiceImpl::getInstance()->queryTaskInfo($CreateUserTaskRecordParams->task_id);

        if (!Result::isSuccess($taskInfoRet)) {
            $result->setError(\Config_Error::ERR_CREATE_TASK_INFO_IS_NOT_EXIST);
            $this->render($result);
        }
        $CreateUserTaskRecordParams->value = $taskInfoRet->data->value;
        $CreateUserTaskRecordParams->user_id = $userId;
        $insertRet = MaintainTaskServiceImpl::getInstance()->insertUserTaskRecord($CreateUserTaskRecordParams);
        if (Result::isSuccess($insertRet)) {
            $result->setSuccessWithResult($insertRet->data);
        }
        $this->render($result);
    }


    /**
     * 1.星能量榜单
     */
    public function getTaskRank()
    {
        $result = new Result();
        $userId = $this->getRequestUserId(false);

        /** @var GetMaintainTaskRankParams $getMaintainRankParams */

        $getMaintainRankParams = $this->requestObject(GetMaintainTaskRankParams::class);
        if ($getMaintainRankParams->rank_type == null) {
            $result->setError(\Config_Error::ERR_STAR_TASK_RANK_TYPE_IS_NOT_EXIST);
            $this->render($result);
        }

        $starListResp = new StarListResp();
        $index = $getMaintainRankParams->index;
        $getMaintainRankParams->page_size = self::DEFAULT_PAGE_SIZE;
        $getMaintainRankParams->index = $getMaintainRankParams->index;
        $maintainTaskServiceImpl = MaintainTaskServiceImpl::getInstance();
        $rankRet = $maintainTaskServiceImpl->getMaintainTaskRankList($getMaintainRankParams);
        if (Result::isSuccess($rankRet) == false) {
            $result->setSuccessWithResult($starListResp);
            $this->render($result);
        }
        $starList = $rankRet->data;


        $starInfoRespList = [];
        foreach ($starList as $starId) {
            $starInfoResp = StarConversionUtil::getInstance()->buildStarInfoResp($starId, $userId);
            array_push($starInfoRespList, $starInfoResp);
        }
        $starListResp->star_list = $starInfoRespList;
        $totalCount = $maintainTaskServiceImpl->getMaintainTaskRankCount($getMaintainRankParams->rank_type)->data;
        if ($index == 0) {
            $starListResp->list_name = '本期最强：'.$starInfoRespList[0]->star_name;
        }
        $pageInfoResp = PageInfoResp::buildPageInfoResp($index, $getMaintainRankParams->page_size, count($starInfoRespList), $totalCount);

        $starListResp->page_info = $pageInfoResp;
        $result->setSuccessWithResult($starListResp);
        $this->render($result);
    }


    /**
     * 获取star下面用户星能量的贡献记录
     *
     */
    public function getRecordHistoryByStarId()
    {

        $result = new Result();
        $getUserTaskHistoryForStarParams = $this->requestObject(GetUserTaskHistoryForStarParams::class);
        $index = $getUserTaskHistoryForStarParams->index;
        /**
         * 参数校验
         */
        if ($getUserTaskHistoryForStarParams->star_id == 0) {
            $result->setError(\Config_Error::ERR_QUERY_TASK_HISTORY_STAR_ID_IS_NULL);
            $this->render($result);
        }

        $userId = $this->getRequestUserId();

        if ($userId == 0) {
            $result->setError(\Config_Error::ERR_GET_TASK_RECORD_USER_ID_IS_NULL);
            $this->render($result);
        }


        if ($userId != $getUserTaskHistoryForStarParams->visit_user_id) {
            $result->setError(\Config_Error::ERR_VISIT_RECORD_IS_NOT_ALLOWED);
            $this->render($result);
        }

        $userInfoRet = UserServiceImpl::getInstance()->getValidUserInfo($userId);
        if (!Result::isSuccess($userInfoRet)) {
            $result->setError(\Config_Error::ERR_USER_NOT_EXIST);
            $this->render($result);
        }

        /** 获取针对IP的详细打卡记录*/
        $maintainTaskServiceImpl = MaintainTaskServiceImpl::getInstance();
        $getUserTaskHistoryForStarParams->user_id = $userId;
        $taskList = $maintainTaskServiceImpl->getUserTaskHistoryForStar($getUserTaskHistoryForStarParams);


        $MaintainTaskHistoryListResp = new MaintainTaskHistoryListResp();
        $taskHistoryArray = [];
        if (Result::isSuccess($taskList)) {
            foreach ($taskList->data as $taskInfo) {
                $starTaskConversionUtil = MaintainStarTaskConverisonUtil::getInstance();
                $MaintainTaskHistoryResp = $starTaskConversionUtil->buildUserTaskHistory($taskInfo);
                array_push($taskHistoryArray, $MaintainTaskHistoryResp);

            }
        }
        $page_info = PageInfoResp::buildPageInfoResp($index, self::DEFAULT_PAGE_SIZE, count($taskList->data));
        $MaintainTaskHistoryListResp->page_info = $page_info;
        $MaintainTaskHistoryListResp->task_list = $taskHistoryArray;

        /**
         * TODO:完善信息
         */

        $userStarInfo = [];
        $userStarInfo['task_total_value'] = $maintainTaskServiceImpl->queryTotalValueForStar($getUserTaskHistoryForStarParams)->data;
        $userStarInfo['constant_days'] = 7;
        $userStarInfo['total_days'] = 9;
        $MaintainTaskHistoryListResp->task_user_star_info = $userStarInfo;


        $result->setSuccessWithResult($MaintainTaskHistoryListResp);
        $this->render($result);
    }


}