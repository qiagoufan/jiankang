<?php


use facade\request\feed\heat\TriggerEventParams;
use facade\response\Result;
use facade\response\share\GetShareInfoResp;
use service\feed\impl\FeedHeatServiceImpl;
use \service\share\impl\ShareServiceImpl;
use \facade\request\share\GetShareInfoParams;

class Share extends FUR_Controller
{

    /**
     * 分享信息给第三方
     */
    public function getShareInfoToThird()
    {
        $result = new Result();
        $getShareInfoParams = $this->requestObject(GetShareInfoParams::class);
        $shareServiceImpl = ShareServiceImpl::getInstance();
        if (!$getShareInfoParams->share_type || !$getShareInfoParams->biz_id) {
            $result->setError(\Config_Error::ERR_SHARE_PARAMS_IS_NULL);
            $this->render($result);
        }
        $getShareInfoRet = $shareServiceImpl->getShareInfoToThird($getShareInfoParams);
        if (!Result::isSuccess($getShareInfoRet)) {
            $this->render($getShareInfoRet);
        }
        $getShareInfoResp = new GetShareInfoResp();

        if ($getShareInfoParams->share_type == GetShareInfoParams::SHARE_TYPE_PRODUCT || $getShareInfoParams->share_type == GetShareInfoParams::SHARE_TYPE_SUPER_PRODUCT) {
            /**分享普通商品以及超级商品*/
            $productDetailDTO = $getShareInfoRet->data;
            //普通商品
            if ($getShareInfoParams->share_type == GetShareInfoParams::SHARE_TYPE_PRODUCT) {
                $shareLink = FUR_Config::get_share_config('share_product') . $getShareInfoParams->biz_id;
            }

            //超级商品
            if ($getShareInfoParams->share_type == GetShareInfoParams::SHARE_TYPE_SUPER_PRODUCT) {
                $shareLink = FUR_Config::get_share_config('share_super_product') . $getShareInfoParams->biz_id;
            }
            $share_pic = $productDetailDTO->main_pic . '?x-oss-process=image/resize,w_200,h_200/quality,q_80';
            $getShareInfoResp = $this->conversionShareInfo(null, $share_pic, $productDetailDTO->summary, $shareLink);

        } elseif ($getShareInfoParams->share_type == GetShareInfoParams::SHARE_TYPE_STAR_FEED) {
            /**分享明星卡片*/
            $startInfoDTO = $getShareInfoRet->data;
            $share_title = $startInfoDTO->star_name;
            $shareLink = FUR_Config::get_share_config('share_star') . $getShareInfoParams->biz_id;
            $share_pic = $startInfoDTO->official_background . '?x-oss-process=image/resize,w_200,h_200/quality,q_80';
            if ($startInfoDTO != null) {
                $getShareInfoResp = $this->conversionShareInfo($startInfoDTO->official_description, $share_pic, $share_title, $shareLink);
            }

        } elseif ($getShareInfoParams->share_type == GetShareInfoParams::SHARE_TYPE_MATERIALS_FEED) {
            /**分享feed物料*/
            $feedInfoResp = $getShareInfoRet->data;
            $feedDetail = $feedInfoResp->feed_detail;
            $share_title = null;
            $share_pic = null;
            //默认纯文本
            $shareLink = FUR_Config::get_share_config('share_feed_text') . $getShareInfoParams->biz_id;
            if ($feedDetail) {
                $feed_entity_list = $feedDetail->feed_content_list;
                $share_title = $feedDetail->description;
                if ($feed_entity_list) {
                    //图片
                    if ($feed_entity_list[0]->element_type == 'image') {
                        $share_pic = str_replace("m_lfit,h_1000,w_1000/format,jpg", "m_lfit,h_200,w_200/format,jpg,q_80", $feed_entity_list[0]->image);
                        $shareLink = FUR_Config::get_share_config('share_feed_image') . $getShareInfoParams->biz_id;
                    }
                    //视频
                    if ($feed_entity_list[0]->element_type == 'video') {
                        $share_pic = str_replace("m_lfit,h_1000,w_1000/format,jpg", "m_lfit,h_200,w_200/format,jpg,q_80", $feed_entity_list[0]->cover);
                        $shareLink = FUR_Config::get_share_config('share_feed_video') . $getShareInfoParams->biz_id;
                    }
                }
            }
            $getShareInfoResp = $this->conversionShareInfo(null, $share_pic, $share_title, $shareLink);

            /**
             * 增加热度
             */
            $triggerEventParams = new  TriggerEventParams();
            $triggerEventParams->feed_id = $getShareInfoParams->biz_id;
            $triggerEventParams->event_type = FeedHeatServiceImpl::EVENT_SHARE;
            FeedHeatServiceImpl::getInstance()->triggerEvent($triggerEventParams);


        }
        $result->setSuccessWithResult($getShareInfoResp);
        $this->render($result);
    }


    /**
     * @param $share_desc
     * @param $share_pic
     * @param $share_title
     * @param $shareLink
     * @return GetShareInfoResp|null
     * 转换分享信息
     */
    private function conversionShareInfo($share_desc, $share_pic, $share_title, $shareLink): ?GetShareInfoResp
    {
        $getShareInfoResp = new GetShareInfoResp();
        $getShareInfoResp->share_desc = $share_desc;
        $getShareInfoResp->share_pic = $share_pic;
        $getShareInfoResp->share_title = $share_title;
        $getShareInfoResp->share_link = $shareLink;
        return $getShareInfoResp;
    }

}