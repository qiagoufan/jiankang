<?php

use business\product\JkProductConversionUtil;
use facade\request\jk\riskCheck\SubmitRiskCheckAnswerParams;
use facade\request\PageParams;
use facade\request\product\GetProductItemListParams;
use facade\request\tag\QueryTagEntityListParams;
use facade\response\base\DataListResp;
use facade\response\jiankang\riskCheck\RiskCheckQuestionResp;
use facade\response\jiankang\riskCheck\RiskCheckResultDetailResp;
use facade\response\jiankang\riskCheck\RiskCheckResultResp;
use facade\response\Result;
use model\jk\dto\JkRiskCheckCategoryDTO;
use model\jk\dto\JkRiskCheckQuestionDTO;
use model\jk\dto\JkRiskCheckResultDTO;
use model\jk\dto\JkUserCheckHistoryDTO;
use model\product\dto\ProductItemDTO;
use service\jk\impl\JkRiskCheckServiceImpl;
use service\jk\impl\JkUserHistoryServiceImpl;
use service\product\impl\ProductItemServiceImpl;
use service\product\impl\ProductServiceImpl;
use service\tag\impl\TagServiceImpl;

class JkRiskCheck extends FUR_Controller
{
    private $userId;

    const DEFAULT_PAGE_SIZE = 10;

    public function __construct()
    {
        $this->userId = $this->getRequestUserId(false);
    }

    public function getRiskCheckCategoryList()
    {
        $result = JkRiskCheckServiceImpl::getInstance()->getJkRiskCheckCategoryList();
        $this->render($result);
    }

    public function getRiskCheckCategoryListV1()
    {
        $result = new Result();

        $listRet = JkRiskCheckServiceImpl::getInstance()->getJkRiskCheckCategoryList();
        $listResp = new DataListResp();
        /** @var JkRiskCheckCategoryDTO $category */
        foreach ($listRet->data as $category) {
            if ($category->parent_id == 0) {
                $category->category_list = [];
                array_push($listResp->list, $category);
            }
        }
        foreach ($listRet->data as $category) {
            if ($category->parent_id != 0) {
                foreach ($listResp->list as $level1Category) {
                    if ($level1Category->id == $category->parent_id) {
                        array_push($level1Category->category_list, $category);
                        break;
                    }
                }
            }
        }
        $result->setSuccessWithResult($listResp);
        $this->render($result);
    }


    public function getRiskCheckQuestion()
    {
        $result = new Result();
        $categoryId = $this->request('category_id');
        $questionListRet = JkRiskCheckServiceImpl::getInstance()->getJkRiskCheckQuestionList($categoryId);
        if (!Result::isSuccess($questionListRet)) {
            $this->render($questionListRet);
        }

        $listResp = new DataListResp();
        $questionList = $questionListRet->data;

        /** @var JkRiskCheckQuestionDTO $question */
        foreach ($questionList as $question) {
            $questionResp = new  RiskCheckQuestionResp();
            $questionResp->id = $question->id;
            $questionResp->category_id = $question->category_id;
            $questionResp->question_title = $question->question_title;
            $questionResp->question_sort_num = $question->question_sort_num;
            $optionListRet = JkRiskCheckServiceImpl::getInstance()->queryOptionList($question->id);
            $questionResp->question_option_list = $optionListRet->data;
            array_push($listResp->list, $questionResp);
        }
        $result->setSuccessWithResult($listResp);
        $this->render($result);
    }

    public function submitRiskCheckQuestion()
    {
        $result = new Result();
        $userId = $this->getRequestUserId();
        $checkResultResp = new RiskCheckResultResp();
        /** @var SubmitRiskCheckAnswerParams $submitQuestionParams */
        $submitQuestionParams = $this->requestObject(SubmitRiskCheckAnswerParams::class);
        $diseaseRet = JkRiskCheckServiceImpl::getInstance()->submitRiskCheckQuestion($submitQuestionParams);

        /** @var JkRiskCheckResultDTO $checkResultDTO */
        $checkResultDTO = $diseaseRet->data;

        $checkResultResp->check_result = $checkResultDTO;
        $userHistoryDTO = new JkUserCheckHistoryDTO();
        $userHistoryDTO->user_id = $userId;
        $userHistoryDTO->check_result = json_encode($checkResultDTO);
        $userHistoryDTO->check_type = \Config_Const::CHECK_TYPE_RISK;

        JkUserHistoryServiceImpl::getInstance()->saveUserHistory($userHistoryDTO);

        //先获取标签商品
        $queryTagEntityListParams = new QueryTagEntityListParams();
        $queryTagEntityListParams->tag_biz_type = Config_Const::BIZ_TYPE_RISK_CHECK_RESULT;
        $queryTagEntityListParams->tag_biz_id = $checkResultDTO->id;
        $queryTagEntityListParams->entity_biz_type = Config_Const::BIZ_TYPE_PRODUCT;
        $productIdListRet = TagServiceImpl::getInstance()->queryTagEntityList($queryTagEntityListParams);
        if (Result::isSuccess($productIdListRet)) {
            $productIdList = $productIdListRet->data;
            $productListRet = ProductItemServiceImpl::getInstance()->queryProductByIdList($productIdList);
            if (Result::isSuccess($productListRet)) {
                foreach ($productListRet->data as $productItem) {
                    $productResp = JkProductConversionUtil::getInstance()->buildProductResp($productItem);
                    if ($productResp == null) {
                        continue;
                    }
                    array_push($checkResultResp->product_list, $productResp);
                }
            }

        }


        //如果是空的再去拿默认商品
        if (empty($checkResultResp->product_list)) {
            $getProductListParams = new  GetProductItemListParams();
            $productListRet = ProductItemServiceImpl::getInstance()->getProductList($getProductListParams);

            if (Result::isSuccess($productListRet)) {
                /** @var ProductItemDTO $productItem */
                foreach ($productListRet->data as $productItem) {
                    $productResp = JkProductConversionUtil::getInstance()->buildProductResp($productItem);
                    if ($productResp == null) {
                        continue;
                    }
                    array_push($checkResultResp->product_list, $productResp);
                }
            }
        }

        $result->setSuccessWithResult($checkResultResp);
        $this->render($result);
    }

}