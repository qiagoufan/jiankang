<?php

use business\user\UserInfoConversionUtil;
use facade\request\PageParams;
use facade\response\base\DataListResp;
use facade\response\base\PageInfoResp;
use facade\response\Result;
use facade\response\user\invite\InviteRecordResp;
use lib\TimeHelper;
use model\user\dto\UserInviteRecordDTO;
use service\user\impl\UserInviteServiceImpl;
use service\user\impl\UserServiceImpl;

class JkUserInvite extends FUR_Controller
{

    public function getUserInviteCode()
    {
        $result = new Result();
        $userId = $this->getRequestUserId();
        if ($userId == 0) {
            $result->setError(Config_Error::ERR_USER_NOT_EXIST);
            $this->render($result);
        }
        $userService = UserServiceImpl::getInstance();
        $userInfoResult = $userService->getValidUserInfo($userId);
        /** @var \model\user\dto\UserDTO $userInfo */
        $userInfo = $userInfoResult->data;
        $data['invite_code'] = $userInfo->invite_code;
        $result->setSuccessWithResult($data);
        $this->render($result);
    }


    public function getUserInviteRecord()
    {
        $result = new Result();
        $userId = $this->getRequestUserId();
        if ($userId == 0) {
            $result->setError(Config_Error::ERR_USER_NOT_EXIST);
            $this->render($result);
        }
        /** @var PageParams $params */
        $params = $this->requestObject(PageParams::class);
        $params->user_id = $userId;
        $recordResult = UserInviteServiceImpl::getInstance()->queryUserInviteRecord($params);

        if (!Result::isSuccess($recordResult)) {
            $this->render($recordResult);
        }

        $recordList = $recordResult->data;
        $listResp = new DataListResp();

        /** @var UserInviteRecordDTO $userInviteRecord */
        foreach ($recordList as $userInviteRecord) {
            $userInfo = UserInfoConversionUtil::getInstance()->getSimpleUserInfoResp($userInviteRecord->invite_user_id);
            if ($userInfo == null) {
                continue;
            }
            $inviteRecordResp = new InviteRecordResp();
            $inviteRecordResp->real_name = $userInfo->real_name;
            $inviteRecordResp->nick_name = $userInfo->nick_name;
            $inviteRecordResp->avatar = $userInfo->avatar;
            $inviteRecordResp->invite_timestamp = $userInviteRecord->created_timestamp;
            $inviteRecordResp->display_invite_time = TimeHelper::getPersonalTimeString($userInviteRecord->created_timestamp);

            array_push($listResp->list, $inviteRecordResp);
        }

        $listResp->page_info = PageInfoResp::buildPageInfoRespBaseLine($params->index, $params->page_size, count($listResp->list));
        $result->setSuccessWithResult($listResp);

        $this->render($result);
    }

}