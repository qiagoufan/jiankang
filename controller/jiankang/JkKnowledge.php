<?php

use business\jiankang\JkKnowledgeConversionUtil;
use facade\request\jk\knowledge\QueryKnowledgeListParams;
use facade\response\Result;
use service\jk\impl\JkKnowledgeServiceImpl;

class JkKnowledge extends FUR_Controller
{
    private $userId;

    const DEFAULT_PAGE_SIZE = 10;

    public function __construct()
    {
        $this->userId = $this->getRequestUserId(false);
    }


    public function getKnowledgeList()
    {
        $result = new Result();
        /** @var QueryKnowledgeListParams $params */
        $params = $this->requestObject(QueryKnowledgeListParams::class);
        $params->is_publish = 1;
        $listRet = JkKnowledgeServiceImpl::getInstance()->getJkKnowledgeList($params);
        $list = $listRet->data;
        $respList = [];
        foreach ($list as $item) {
            $data = JkKnowledgeConversionUtil::conversionKnowledge($item);
            if ($data == null) {
                continue;
            }
            array_push($respList, $data);
        }
        $result->setSuccessWithResult($respList);
        $this->render($result);
    }

    public function getKnowledgeCategoryList()
    {
        $result = JkKnowledgeServiceImpl::getInstance()->getJkKnowledgeCategoryList();
        $this->render($result);
    }


    public function getKnowledgeDetail()
    {
        $id = $this->request('knowledge_id');
        $dataRet = JkKnowledgeServiceImpl::getInstance()->getJkKnowledge($id);
        $result = new Result();
        $result->setSuccessWithResult(JkKnowledgeConversionUtil::conversionKnowledge($dataRet->data));
        $this->render($result);
    }


}