<?php

use business\product\JkProductConversionUtil;
use facade\request\product\GetProductDetailParams;
use facade\request\product\GetProductItemListParams;
use facade\response\base\DataListResp;
use facade\response\base\PageInfoResp;
use facade\response\product\category\CategoryResp;
use facade\response\Result;
use model\product\dto\ProductCategoryDTO;
use model\product\dto\ProductItemDTO;
use service\category\impl\CategoryServiceImpl;
use service\product\impl\ProductItemServiceImpl;
use service\product\impl\ProductServiceImpl;

class JkProduct extends FUR_Controller
{
    private $userId;

    const DEFAULT_PAGE_SIZE = 10;

    public function __construct()
    {
        $this->userId = $this->getRequestUserId(false);
    }


    public function getProductCategoryList()
    {
        $result = new Result();
        $categoryDTOListRet = CategoryServiceImpl::getInstance()->queryCategoryList();
        if (!Result::isSuccess($categoryDTOListRet)) {
            $this->render($categoryDTOListRet);
        }
        $listResp = new DataListResp();
        $categoryList = [];
        /** @var ProductCategoryDTO $category */
        foreach ($categoryDTOListRet->data as $category) {
            if ($category->category_level == 1) {
                $categoryResp = new CategoryResp();
                FUR_Core::copyProperties($category, $categoryResp);
                $categoryResp->category_list = [];
                array_push($categoryList, $categoryResp);
                continue;
            }
        }
        foreach ($categoryDTOListRet->data as $category) {
            if ($category->category_level == 2) {
                $categoryResp = new CategoryResp();
                FUR_Core::copyProperties($category, $categoryResp);

                foreach ($categoryList as $level1Category) {
                    if ($level1Category->id == $categoryResp->cate_level1_id) {
                        array_push($level1Category->category_list, $categoryResp);
                        break;
                    }
                }
                continue;
            }
        }
        $listResp->list = $categoryList;
        $result->setSuccessWithResult($listResp);
        $this->render($result);
    }

    public function getProductList()
    {
        $result = new Result();
        /** @var GetProductItemListParams $getProductListParams */
        $getProductListParams = $this->requestObject(GetProductItemListParams::class);
        $productListRet = ProductItemServiceImpl::getInstance()->getProductList($getProductListParams);
        if (!Result::isSuccess($productListRet)) {
            $result->setSuccessWithResult($productListRet);
            $this->render($result);
        }

        $productListResp = new DataListResp();
        /** @var ProductItemDTO $productItem */
        foreach ($productListRet->data as $productItem) {
            $productResp = JkProductConversionUtil::getInstance()->buildProductResp($productItem);
            if ($productResp == null) {
                continue;
            }
            array_push($productListResp->list, $productResp);
        }
        $productListResp->page_info = PageInfoResp::buildPageInfoRespBaseLine($getProductListParams->index, $getProductListParams->page_size, count($productListResp->list));
        $result->setSuccessWithResult($productListResp);
        $this->render($result);

    }

    public function getProductDetail()
    {
        $result = new Result();
        /** @var GetProductDetailParams $getProductDetailParams */
        $getProductDetailParams = $this->requestObject(GetProductDetailParams::class);
        $productServiceImpl = ProductServiceImpl::getInstance();
        $productDetailRet = $productServiceImpl->getProductItem($getProductDetailParams->item_id);
        if (!Result::isSuccess($productDetailRet)) {
            $this->render($productDetailRet);
        }
        $productItem = $productDetailRet->data;
        $productResp = JkProductConversionUtil::getInstance()->buildProductResp($productItem);
        $result->setSuccessWithResult($productResp);
        $this->render($result);

    }

}