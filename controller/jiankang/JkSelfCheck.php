<?php

use business\product\JkProductConversionUtil;
use facade\request\jk\selfCheck\SubmitSelfCheckAnswerParams;
use facade\request\product\GetProductItemListParams;
use facade\request\tag\QueryTagEntityListParams;
use facade\response\base\DataListResp;
use facade\response\jiankang\riskCheck\RiskCheckQuestionResp;
use facade\response\jiankang\selfCheck\SelfCheckResultResp;
use facade\response\Result;
use model\jk\dto\JkSelfCheckQuestionDTO;
use model\jk\dto\JkSelfCheckResultDTO;
use model\jk\dto\JkUserCheckHistoryDTO;
use model\product\dto\ProductItemDTO;
use service\jk\impl\JkSelfCheckServiceImpl;
use service\jk\impl\JkUserHistoryServiceImpl;
use service\product\impl\ProductItemServiceImpl;
use service\tag\impl\TagServiceImpl;

class JkSelfCheck extends FUR_Controller
{
    private $userId;

    const DEFAULT_PAGE_SIZE = 10;

    public function __construct()
    {
        $this->userId = $this->getRequestUserId(false);
    }

    public function getSelfCheckCategoryList()
    {
        $result = JkSelfCheckServiceImpl::getInstance()->getJkSelfCheckCategoryList();
        $this->render($result);
    }


    public function getSelfCheckQuestion()
    {
        $result = new Result();
        $categoryId = $this->request('category_id');
        $questionListRet = JkSelfCheckServiceImpl::getInstance()->getJkSelfCheckQuestionList($categoryId);
        if (!Result::isSuccess($questionListRet)) {
            $this->render($questionListRet);
        }

        $listResp = new DataListResp();
        $questionList = $questionListRet->data;

        /** @var JkSelfCheckQuestionDTO $question */
        foreach ($questionList as $question) {
            $questionResp = new  RiskCheckQuestionResp();
            $questionResp->category_id = $question->category_id;
            $questionResp->question_title = $question->question_title;
            $questionResp->question_sort_num = $question->question_sort_num;
            $questionResp->question_option_list = json_decode($question->question_option);
            array_push($listResp->list, $questionResp);
        }
        $result->setSuccessWithResult($listResp);
        $this->render($result);
    }

    public function submitSelfCheckQuestion()
    {
        $result = new Result();
        $checkResultResp = new SelfCheckResultResp();
        $userId = $this->getRequestUserId();
        /** @var SubmitSelfCheckAnswerParams $submitQuestionParams */
        $submitQuestionParams = $this->requestObject(SubmitSelfCheckAnswerParams::class);
        $diseaseRet = JkSelfCheckServiceImpl::getInstance()->submitSelfCheckQuestion($submitQuestionParams);
        if (!Result::isSuccess($diseaseRet)) {
            $checkResultDTO = new  JkSelfCheckResultDTO;
            $checkResultDTO->id = 1;
            $checkResultDTO->disease = '没有任何异常';
            $checkResultDTO->suggest = '还没有任何相关的建议';
            $checkResultResp->check_result = $checkResultDTO;

        } else {
            /** @var JkSelfCheckResultDTO $checkResultDTO */
            $checkResultDTO = $diseaseRet->data;
            $checkResultResp->check_result = $checkResultDTO;
        }

//        $userHistoryDTO = new JkUserCheckHistoryDTO();
//        $userHistoryDTO->user_id = $userId;
//        $userHistoryDTO->check_result = json_encode($checkResultDTO);
//        $userHistoryDTO->check_type = Config_Const::CHECK_TYPE_SELF;
//
//        JkUserHistoryServiceImpl::getInstance()->saveUserHistory($userHistoryDTO);

        //先获取标签商品
        $queryTagEntityListParams = new QueryTagEntityListParams();
        $queryTagEntityListParams->tag_biz_type = Config_Const::BIZ_TYPE_SELF_CHECK_RESULT;
        $queryTagEntityListParams->tag_biz_id = $checkResultDTO->id;
        $queryTagEntityListParams->entity_biz_type = Config_Const::BIZ_TYPE_PRODUCT;
        $productIdListRet = TagServiceImpl::getInstance()->queryTagEntityList($queryTagEntityListParams);
        if (Result::isSuccess($productIdListRet)) {
            $productIdList = $productIdListRet->data;
            $productListRet = ProductItemServiceImpl::getInstance()->queryProductByIdList($productIdList);
            if (Result::isSuccess($productListRet)) {
                foreach ($productListRet->data as $productItem) {
                    $productResp = JkProductConversionUtil::getInstance()->buildProductResp($productItem);
                    if ($productResp == null) {
                        continue;
                    }
                    array_push($checkResultResp->product_list, $productResp);
                }
            }

        }


        //如果是空的再去拿默认商品
        if (empty($checkResultResp->product_list)) {
            $getProductListParams = new  GetProductItemListParams();
            $productListRet = ProductItemServiceImpl::getInstance()->getProductList($getProductListParams);

            if (Result::isSuccess($productListRet)) {
                /** @var ProductItemDTO $productItem */
                foreach ($productListRet->data as $productItem) {
                    $productResp = JkProductConversionUtil::getInstance()->buildProductResp($productItem);
                    if ($productResp == null) {
                        continue;
                    }
                    array_push($checkResultResp->product_list, $productResp);
                }
            }
        }
        $result->setSuccessWithResult($checkResultResp);
        $this->render($result);
    }

}