<?php

use business\product\JkProductConversionUtil;
use facade\request\PageParams;
use facade\request\product\GetProductItemListParams;
use facade\response\base\DataListResp;
use facade\response\base\PageInfoResp;
use facade\response\jiankang\history\JkCheckHistoryResp;
use facade\response\Result;
use model\jk\dto\JkUserCheckHistoryDTO;
use model\product\dto\ProductItemDTO;
use service\jk\impl\JkUserHistoryServiceImpl;
use service\product\impl\ProductItemServiceImpl;

class UserCheckHistory extends FUR_Controller
{
    private $userId;


    public function __construct()
    {
        $this->userId = $this->getRequestUserId();
    }


    public function queryCheckHistoryList()
    {
        $result = new Result();
        /** @var PageParams $pageParams */
        $pageParams = $this->requestObject(PageParams::class);
        $pageParams->user_id = $this->userId;
        $recordListRet = JkUserHistoryServiceImpl::getInstance()->getUserHistory($pageParams);

        $listResp = new DataListResp();
        if (Result::isSuccess($recordListRet)) {
            $recordList = $recordListRet->data;

            /** @var JkUserCheckHistoryDTO $item */
            foreach ($recordList as $item) {
                $checkHistoryResp = $this->buildJkCheckHistoryResp($item);
                if ($checkHistoryResp == null) {
                    continue;
                }
                array_push($listResp->list, $checkHistoryResp);
            }
            $listResp->page_info = PageInfoResp::buildPageInfoRespBaseLine($pageParams->index, $pageParams->page_size, count($listResp->list));

        }

        $result->setSuccessWithResult($listResp);
        $this->render($result);
    }

    public function queryCheckHistoryDetail()
    {
        $result = new Result();
        $id = $this->request('id');
        $recordRet = JkUserHistoryServiceImpl::getInstance()->getDetail($id);
        if (Result::isSuccess($recordRet) == false) {
            $this->render($recordRet);
        }
        $record = $recordRet->data;
        if ($record->user_id != $this->userId) {
            $result->setError(Config_Error::ERR_USER_NO_RESULT);
            $this->render($result);
        }
        $resp = $this->buildJkCheckHistoryResp($record);
        $result->setSuccessWithResult($resp);
        $this->render($result);
    }


    private function buildJkCheckHistoryResp(?JkUserCheckHistoryDTO $jkUserCheckHistoryDTO): ?JkCheckHistoryResp
    {
        if ($jkUserCheckHistoryDTO == null) {
            return null;
        }

        $jkCheckHistoryResp = new JkCheckHistoryResp();

        $checkResult = json_decode($jkUserCheckHistoryDTO->check_result);
        $jkCheckHistoryResp->created_timestamp = $jkUserCheckHistoryDTO->created_timestamp;
        $jkCheckHistoryResp->display_created_time = date('Y-m-d H:i', $jkUserCheckHistoryDTO->created_timestamp);
        $jkCheckHistoryResp->disease = $checkResult->disease;
        $jkCheckHistoryResp->suggest = $checkResult->suggest;
        $jkCheckHistoryResp->check_type = $jkUserCheckHistoryDTO->check_type;
        $jkCheckHistoryResp->id = $jkUserCheckHistoryDTO->id;
        if (isset($checkResult->option_list)){
            $jkCheckHistoryResp->option_list = $checkResult->option_list;
        }


        $getProductListParams = new  GetProductItemListParams();
        $productListRet = ProductItemServiceImpl::getInstance()->getProductList($getProductListParams);

        if (Result::isSuccess($productListRet)) {
            /** @var ProductItemDTO $productItem */
            foreach ($productListRet->data as $productItem) {
                $productResp = JkProductConversionUtil::getInstance()->buildProductResp($productItem);
                if ($productResp == null) {
                    continue;
                }
                array_push($jkCheckHistoryResp->product_list, $productResp);
            }
        }
        return $jkCheckHistoryResp;

    }
}