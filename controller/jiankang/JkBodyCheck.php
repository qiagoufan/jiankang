<?php

use business\product\JkProductConversionUtil;
use facade\request\jk\bodyCheck\QueryPossibleDiseaseListParams;
use facade\request\jk\bodyCheck\QuerySymptomListParams;
use facade\request\product\GetProductItemListParams;
use facade\request\tag\QueryTagEntityListParams;
use facade\response\base\DataListResp;
use facade\response\jiankang\bodyCheck\BodyDiseaseResultResp;
use facade\response\jiankang\bodyCheck\BodyOptionResp;
use facade\response\jiankang\bodyCheck\BodyQuestionResp;
use facade\response\Result;
use model\jk\dto\JkBodyOptionDTO;
use model\jk\dto\JkBodyQuestionDTO;
use model\product\dto\ProductItemDTO;
use service\jk\impl\JkBodyServiceImpl;
use service\product\impl\ProductItemServiceImpl;
use service\tag\impl\TagServiceImpl;

class JkBodyCheck extends FUR_Controller
{
    private $userId;

    const DEFAULT_PAGE_SIZE = 10;

    public function __construct()
    {
        $this->userId = $this->getRequestUserId(false);
    }

    public function getBodyImage()
    {
        $result = new Result();
        $data = [];
        $data['m'] = ['front' => 'https://cdn.ipxmall.com/system/admin/ef8a4a086f8f8d34ac475b7b9b5e7db9.png', 'back' => 'https://cdn.ipxmall.com/system/admin/d9987270d0cbc2ad9d63627549157b92.png'];
        $data['f'] = ['front' => 'https://cdn.ipxmall.com/system/admin/6f06406b535582269e51e8ea82014565.png', 'back' => 'https://cdn.ipxmall.com/system/admin/a730bc29822379e7af7dee193526b1e1.png'];

        $result->setSuccessWithResult($data);
        $this->render($result);
    }

    public function getBodyList()
    {
        $result = JkBodyServiceImpl::getInstance()->queryBodyList();
        $this->render($result);
    }


    public function querySymptomList()
    {
        $result = new Result();
        /** @var QuerySymptomListParams $params */
        $params = $this->requestObject(QuerySymptomListParams::class);
        $params->age = intval($params->age / 10) * 10 + 5;
        $symptomListRet = JkBodyServiceImpl::getInstance()->querySymptomList($params);
        $this->render($symptomListRet);
    }

    public function querySymptomQuestionOptionList()
    {
        $result = new Result();
        $symptomId = $this->request('symptom_id');
        $questionListRet = JkBodyServiceImpl::getInstance()->queryQuestionList($symptomId);

        if (!Result::isSuccess($questionListRet)) {
            $this->render($questionListRet);
        }
        $listResp = new DataListResp();

        $questionList = $questionListRet->data;
        /** @var JkBodyQuestionDTO $question */
        foreach ($questionList as $question) {
            $questionResp = new BodyQuestionResp();
            $questionResp->question_id = $question->id;
            $questionResp->title = $question->title;
            $questionResp->seq = $question->seq;

            $optionListRet = JkBodyServiceImpl::getInstance()->queryOptionList($question->id);
            if (Result::isSuccess($optionListRet)) {
                $optionList = $optionListRet->data;
                /** @var JkBodyOptionDTO $option */
                foreach ($optionList as $option) {
                    $optionResp = new BodyOptionResp();
                    $optionResp->option_id = $option->id;
                    $optionResp->seq = $option->seq;
                    $optionResp->content = $option->content;
                    array_push($questionResp->option_list, $optionResp);
                }
                array_push($listResp->list, $questionResp);
            }

        }
        $result->setSuccessWithResult($listResp);

        $this->render($result);
    }

    public function queryPossibleDiseaseList()
    {
        $result = new Result();
        /** @var QueryPossibleDiseaseListParams $params */
        $params = $this->requestObject(QueryPossibleDiseaseListParams::class);
        $params->age = intval($params->age / 10) * 10 + 5;
        $diseaseListRet = JkBodyServiceImpl::getInstance()->queryPossibleDiseaseList($params);
        $this->render($diseaseListRet);
    }

    public function getDiseaseDetail()
    {
        $result = new Result();
        $diseaseId = $this->request('disease_id');
        $diseaseDetailRet = JkBodyServiceImpl::getInstance()->getDiseaseDetail($diseaseId);
        $diseaseResultResp = new BodyDiseaseResultResp();
        $diseaseResultResp->disease = $diseaseDetailRet->data;


        //先获取标签商品
        $queryTagEntityListParams = new QueryTagEntityListParams();
        $queryTagEntityListParams->tag_biz_type = Config_Const::BIZ_TYPE_DISEASE;
        $queryTagEntityListParams->tag_biz_id = $diseaseId;
        $queryTagEntityListParams->entity_biz_type = Config_Const::BIZ_TYPE_PRODUCT;
        $productIdListRet = TagServiceImpl::getInstance()->queryTagEntityList($queryTagEntityListParams);
        FUR_Log::info('productIdList', json_encode($productIdListRet->data));
        if (Result::isSuccess($productIdListRet)) {
            $productIdList = $productIdListRet->data;
            $productListRet = ProductItemServiceImpl::getInstance()->queryProductByIdList($productIdList);
            if (Result::isSuccess($productListRet)) {
                foreach ($productListRet->data as $productItem) {
                    $productResp = JkProductConversionUtil::getInstance()->buildProductResp($productItem);
                    if ($productResp == null) {
                        continue;
                    }
                    array_push($diseaseResultResp->product_list, $productResp);
                }
            }

        }
        FUR_Log::info('product_list', json_encode($diseaseResultResp->product_list));
        //如果是空的再去拿默认商品
        if (empty($diseaseResultResp->product_list)) {
            $getProductListParams = new  GetProductItemListParams();
            $productListRet = ProductItemServiceImpl::getInstance()->getProductList($getProductListParams);

            if (Result::isSuccess($productListRet)) {
                /** @var ProductItemDTO $productItem */
                foreach ($productListRet->data as $productItem) {
                    $productResp = JkProductConversionUtil::getInstance()->buildProductResp($productItem);
                    if ($productResp == null) {
                        continue;
                    }
                    array_push($diseaseResultResp->product_list, $productResp);
                }
            }
        }


        $result->setSuccessWithResult($diseaseResultResp);
        $this->render($result);
    }

}