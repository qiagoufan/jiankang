<?php

use business\sign\SignTaskConversionUtil;
use facade\request\PageParams;
use facade\request\sign\SignUserTaskParams;
use facade\response\base\DataListResp;
use facade\response\base\PageInfoResp;
use facade\response\Result;
use lib\TimeHelper;
use model\sign\dto\SignInTaskDTO;
use model\sign\dto\SignInTaskRecordDTO;
use model\sign\dto\SignInUserFinishRecordDTO;
use service\sign\impl\SignServiceImpl;

class JkSignIn extends FUR_Controller
{
    private $userId;

    const DEFAULT_PAGE_SIZE = 10;

    public function __construct()
    {
        $this->userId = $this->getRequestUserId(false);
    }


    public function querySignTypeList()
    {
        $result = new Result();
        /** @var PageParams $pageParams */
        $pageParams = $this->requestObject(PageParams::class);
        $signTypeListRet = SignServiceImpl::getInstance()->querySignTypeList($pageParams);
        $this->render($signTypeListRet);
    }

    public function queryUserSignTaskList()
    {

        $result = new Result();
        $signTypeId = $this->request('sign_type_id');

        /** @var PageParams $pageParams */
        $pageParams = $this->requestObject(PageParams::class);
        $signTaskListRet = SignServiceImpl::getInstance()->querySignTaskList($signTypeId, $pageParams);
        if (!Result::isSuccess($signTaskListRet)) {
            $this->render($signTaskListRet);
        }
        /** @var array $signTaskList */
        $signTaskList = $signTaskListRet->data;


        $listResp = new DataListResp();

        /** @var SignInTaskDTO $signTask */
        foreach ($signTaskList as $signTask) {
            $signUserTaskResp = SignTaskConversionUtil::getInstance()->buildSignTaskUserResp($signTask, $this->userId);
            if ($signUserTaskResp == null) {
                continue;
            }
            array_push($listResp->list, $signUserTaskResp);
        }

        $listResp->page_info = PageInfoResp::buildPageInfoRespBaseLine(0, 10, count($listResp->list));
        $result->setSuccessWithResult($listResp);
        $this->render($result);
    }


    public function getUserSignHistory()
    {
        $result = new Result();
        $yearMonth = $this->request('year_month');
        if ($yearMonth == null) {
            $yearMonth = date('Ym');
        }

        $userId = $this->userId;
        $startDate = $yearMonth . '01';
        $endDate = TimeHelper::getLastOfMonth(strtotime($startDate));
        $signTypeId = $this->request('sign_type_id');
        $userSignFinishRecordList = [];

        $userSignHistoryResp = [];
        if ($userId) {
            $userSignFinishRecordListRet = SignServiceImpl::getInstance()->queryUserSignFinishRecordList($signTypeId, $this->userId, $startDate, $endDate);
            if (!Result::isSuccess($userSignFinishRecordListRet)) {
                $this->render($userSignFinishRecordListRet);
            }
            $userSignFinishRecordList = $userSignFinishRecordListRet->data;
            //获取tips
            $date = date('Ymd');
            $finishRet = SignServiceImpl::getInstance()->queryUserSignFinishRecord($signTypeId, $userId, $date);
            FUR_Log::info('finishRet', json_encode($finishRet));
            if (Result::isSuccess($finishRet)) {
                $tipsRet = SignServiceImpl::getInstance()->getLatestTips($signTypeId, $date);
                $userSignHistoryResp['tips'] = $tipsRet->data;
            }

        }

        $userSignHistoryResp['list'] = [];
        for ($i = $startDate; $i <= $endDate; $i++) {
            $signInfo = ['finish_status' => 0];
            /** @var SignInUserFinishRecordDTO $userSign */
            foreach ($userSignFinishRecordList as $userSign) {
                if ($userSign->sign_in_date == $i) {
                    $signInfo['finish_status'] = 1;
                }
            }
            $userSignHistoryResp['list'][$i] = $signInfo;
        }


        $result->setSuccessWithResult($userSignHistoryResp);
        $this->render($result);
    }

    public function queryUserSignHistoryRecord()
    {

        $result = new Result();

        $userId = $this->userId;
        $startDate = $this->request('start_date');
        $endDate = $this->request('end_date');
        $signTypeId = $this->request('sign_type_id');
        if ($startDate == null) {
            $startDate = date('Ym') . "01";
        }
        if ($endDate == null) {
            $endDate = TimeHelper::getLastOfMonth(strtotime($startDate));
        }

        $signInTypeRet = SignServiceImpl::getInstance()->getSignTypeDetail($signTypeId);
        if (!Result::isSuccess($signInTypeRet)) {
            $this->render($signInTypeRet);
        }

        /** @var \model\sign\dto\SignInTypeDTO $signInTypeInfo */
        $signInTypeInfo = $signInTypeRet->data;
        /** @var PageParams $pageParams */
        $pageParams = $this->requestObject(PageParams::class);
        $signTaskListRet = SignServiceImpl::getInstance()->querySignTaskList($signTypeId, $pageParams);

        if (!Result::isSuccess($signTaskListRet)) {
            $this->render($signTaskListRet);
        }
        /** @var array $signTaskList */
        $signTaskList = $signTaskListRet->data;

        $userSignHistoryResp = [];
        $userSignHistoryResp['task_list'] = [];
        $userSignHistoryResp['title'] = $signInTypeInfo->sign_type_name;
        $userSignHistoryResp['start_date'] = $startDate;
        $userSignHistoryResp['end_date'] = $endDate;

        /** @var SignInTaskDTO $signTask */
        foreach ($signTaskList as $signTask) {
            $signTaskId = $signTask->id;

            $signTaskRecord = new stdClass();
            $signTaskRecord->task_name = $signTask->sign_in_task_name;
            $signTaskRecord->task_value_unit = $signTask->task_value_unit;
            $signTaskRecord->task_value = $signTask->task_value;
            if ($userId) {
                $userSignListRet = SignServiceImpl::getInstance()->queryUserSignTaskRecordList($signTaskId, $userId, $startDate, $endDate);
                if (!Result::isSuccess($userSignListRet)) {
                    continue;
                }

                $signTaskRecord->user_sign_list = $userSignListRet->data;
            }
            array_push($userSignHistoryResp['task_list'], $signTaskRecord);


        }
        $result->setSuccessWithResult($userSignHistoryResp);
        $this->render($result);
    }


    public
    function sign()
    {
        /** @var SignUserTaskParams $params */
        $params = $this->requestObject(SignUserTaskParams::class);
        $params->user_id = $this->getRequestUserId();
        $result = SignServiceImpl::getInstance()->userSign($params);
        $this->render($result);
    }

    public
    function getSignInFinishTips()
    {

        $result = new Result();
        /** @var PageParams $params */
        $params = $this->requestObject(PageParams::class);
        $signTypeId = $this->request('sign_type_id');
        $userId = $this->userId;
        $date = date('Ymd');
        $finishRet = SignServiceImpl::getInstance()->queryUserSignFinishRecord($signTypeId, $userId, $date);
        if (!Result::isSuccess($finishRet)) {
            $result->setSuccessWithResult(null);
            $this->render($result);
        }
        $tipsRet = SignServiceImpl::getInstance()->getLatestTips($signTypeId, $date);
        $this->render($tipsRet);

    }


}