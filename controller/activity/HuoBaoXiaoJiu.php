<?php

use facade\request\activity\hbxj\FillDeliveryAddressParams;
use facade\response\Result;
use lib\wxpay\WxPayTransfers;
use model\activity\dto\XhActivityHuobaoxiaojiuUserAwardDTO;
use model\activity\XhActivityHuobaoxiaojiuModel;
use service\activity\impl\HbxjServiceImpl;
use service\socialite\impl\SocialiteServiceImpl;

class HuoBaoXiaoJiu extends FUR_Controller
{
    public $userId = 0;

    public function __construct()
    {
    }

    /**
     * 获取碎片
     */
    public function receiveFragment()
    {
        $result = new Result();
        $userId = $this->getRequestUserId();
        $code = $this->request('code');
        /**
         * 查看二维码
         */
        if ($code == null) {
            $result->setError(Config_Error::ERR_HBXJ_CODE_IS_NULL);
            $this->render($result);
        }


        $hbxjService = HbxjServiceImpl::getInstance();
        /**
         * 锁用户
         */
        $lockResult = $hbxjService->lockUser($userId);
        if (Result::isSuccess($lockResult) && $lockResult->data == false) {
            $result->setError(Config_Error::ERR_HBXJ_FREQUENT_OPERATION);
            $this->render($result);
        }

        $receiveFragmentRet = $hbxjService->receiveFragment($code, $userId);

        /**
         * 解锁
         */
        $hbxjService->unlockUser($userId);
        $this->render($receiveFragmentRet);

    }


    /**
     * 获得我的奖励列表
     */
    public function getMyAwardList()
    {
        $userId = $this->getRequestUserId();
        $hbxjService = HbxjServiceImpl::getInstance();
        $result = $hbxjService->getUserAwardList($userId);
        $this->render($result);
    }


    /**
     * 查看中奖碎片详情
     */
    public function getUserFragmentCount()
    {
        $userId = $this->getRequestUserId();
        $hbxjService = HbxjServiceImpl::getInstance();
        $result = $hbxjService->getUserFragmentCount($userId);
        $this->render($result);
    }


    /**
     * 微信提现
     */
    public function wechatWithdraw()
    {
        $result = new Result();
        $userId = $this->getRequestUserId();
        $userAwardId = $this->request('user_award_id', 0);

        $socialiteService = SocialiteServiceImpl::getInstance();
        $queryOpenidRet = $socialiteService->queryUserOpenid(SocialiteServiceImpl::PROVIDER_WEIXIN_MINI, $userId);
        if (!Result::isSuccess($queryOpenidRet)) {
            $result->setError(Config_Error::ERR_HBXJ_ERROR_WITHDRAW_NOT_BIND_WXMINI);
        }
        $openId = $queryOpenidRet->data;
        $hbxjService = HbxjServiceImpl::getInstance();
        /**
         * 锁用户
         */
        $lockResult = $hbxjService->lockUser($userId);
        if (Result::isSuccess($lockResult) && $lockResult->data == false) {
            $result->setError(Config_Error::ERR_HBXJ_FREQUENT_OPERATION);
            $this->render($result);
        }
        $userAwardDetailRet = $hbxjService->getUserAwardDetail($userAwardId);
        if (!Result::isSuccess($userAwardDetailRet)) {
            //解除锁定
            $hbxjService->unlockUser($userId);
            $result->setError(Config_Error::ERR_HBXJ_SERVICE_ERROR);
            $this->render($result);
        }
        /** @var XhActivityHuobaoxiaojiuUserAwardDTO $userAwardDetail */
        $userAwardDetail = $userAwardDetailRet->data;
        //提现前校验用户
        $checkResult = $this->checkWechatWithdraw($userId, $userAwardDetail);
        if (!Result::isSuccess($checkResult)) {
            //解除锁定
            $hbxjService->unlockUser($userId);
            $this->render($checkResult);
        }


        //开始提现
        $paymentNum = $this->sendWeixinTransfers($openId, $userAwardDetail->withdraw_order_num, 10);
        //提现失败
        if (!$paymentNum) {
            //解除锁定
            $hbxjService->unlockUser($userId);
            $result->setError(Config_Error::ERR_HBXJ_ERROR_WITHDRAW_FAIL_BYWECHAT);
            $this->render($result);
        }
        //更新状态
        $checkResult = $hbxjService->updateUserWithdraw($userId, $userAwardId, $userAwardDetail->withdraw_order_num, $paymentNum);
        if (!Result::isSuccess($checkResult)) {
            $this->render($checkResult);
        }
        //解除锁定
        $hbxjService->unlockUser($userId);
        $result->setSuccessWithResult(true);
        $this->render($result);

    }

    /**
     * 填写收货地址
     */
    public function fillDeliveryAddress()
    {
        $userId = $this->getRequestUserId();
        $params = $this->requestObject(FillDeliveryAddressParams::class);
        $hbxjService = HbxjServiceImpl::getInstance();
        $result = $hbxjService->fillDeliveryAddress($userId, $params);
        $this->render($result);
    }


    /**
     * 填写收货地址
     */
    public function getAwardTemplateList()
    {
        $hbxjService = HbxjServiceImpl::getInstance();
        $result = $hbxjService->getAwardTemplateList();
        $this->render($result);
    }


    private function sendWeixinTransfers($open_id, $trade_no, $amount, $real_name = '')
    {
        $WxPayTransfers = new  WxPayTransfers(
            FUR_Config::get_wechat_pay_config('miniAppId'),
            FUR_Config::get_wechat_pay_config('miniAppSecret'),
            FUR_Config::get_wechat_pay_config('mchid'),
            FUR_Config::get_wechat_pay_config('apiKey'),
            FUR_Config::get_wechat_pay_config('wxpay_cert_pem_path'),
            FUR_Config::get_wechat_pay_config('wxpay_key_pem_path'));


        return $WxPayTransfers->createJsBizPackage($open_id, $amount, $trade_no, $real_name, '[享花星Mall]火爆小酒现金红包');
    }


    public function checkWechatWithdraw(int $userId, XhActivityHuobaoxiaojiuUserAwardDTO $userAwardDetail): ?Result
    {
        $result = new Result();
        if ($userAwardDetail == null) {
            $result->setError(\Config_Error::ERR_HBXJ_USER_AWARD_NOT_EXIST);
            return $result;
        }
        if ($userAwardDetail->user_id != $userId) {
            $result->setError(\Config_Error::ERR_HBXJ_USER_AWARD_UNAUTHORIZED);
            return $result;
        }
        if ($userAwardDetail->receive_status != 0) {
            $result->setError(\Config_Error::ERR_HBXJ_USER_AWARD_HAS_RECEIVED);
            return $result;
        }
        if ($userAwardDetail->receive_status != 0) {
            $result->setError(\Config_Error::ERR_HBXJ_USER_AWARD_HAS_RECEIVED);
            return $result;
        }
        //3是现金提现,懒得查了,直接写死
        if ($userAwardDetail->award_id != 3) {
            $result->setError(\Config_Error::ERR_HBXJ_ERROR_RECEIVE_TYPE_ERROR);
            return $result;
        }
        $result->setSuccessWithResult(true);
        return $result;

    }

}