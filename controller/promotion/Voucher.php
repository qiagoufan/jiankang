<?php


use business\promotion\VoucherConversionUtil;
use facade\request\promotion\ProductCountParams;
use facade\request\promotion\QueryVoucherListForProductParams;
use facade\response\base\DataListResp;
use facade\response\promotion\VoucherListResp;
use facade\response\Result;
use lib\PriceHelper;
use model\promotion\dto\PromotionVoucherDTO;
use model\promotion\VoucherEntityModel;
use service\product\impl\ProductServiceImpl;
use service\promotion\impl\PromotionVoucherServiceImpl;

class Voucher extends FUR_Controller
{
    private $userId;

    public function __construct()
    {
        $userId = $this->getRequestUserId();
        $this->userId = $userId;
    }


    public function getAvailableVoucherList()
    {
        $enableVoucherListRet = PromotionVoucherServiceImpl::getInstance()->queryEnableVoucherList();
        $enableVoucherList = $enableVoucherListRet->data;
        $userAvailableVoucherList = [];
        $now = time();
        /** @var PromotionVoucherDTO $voucherInfo */
        foreach ($enableVoucherList as $voucherInfo) {
            $voucherEntityRecord = VoucherEntityModel::getInstance()->queryUserVoucherEntityByVoucherId($this->userId, $voucherInfo->id);
            if ($voucherEntityRecord && $voucherEntityRecord->expire_timestamp > $now) {
                continue;
            }
            $voucherResp = VoucherConversionUtil::conversionVoucherInfo($voucherInfo);
            if ($voucherResp == null) {
                continue;
            }
            array_push($userAvailableVoucherList, $voucherResp);

        }

        $listResp = new DataListResp();
        $listResp->list = $userAvailableVoucherList;
        $result = new Result();
        $result->setSuccessWithResult($listResp);
        $this->render($result);
    }

    public function dispatchVoucher()
    {
        $voucherId = $this->request('voucher_id');
        $result = PromotionVoucherServiceImpl::getInstance()->distributeVoucher($this->userId, $voucherId);
        $this->render($result);

    }

    /**
     * 个人优惠券页
     */
    public function getUserVoucherList()
    {

        $result = new Result();
        $userId = $this->userId;
        $voucher_status_type = $this->request('voucher_status_type');

        $voucherEntityListRet = PromotionVoucherServiceImpl::getInstance()->queryUserVoucherList($userId);
        if (!Result::isSuccess($voucherEntityListRet)) {
            $this->render($voucherEntityListRet);
        }

        $voucherListResp = new VoucherListResp();
        $enable_voucher_list = [];
        $unable_voucher_list = [];
        $userVoucherList = $voucherEntityListRet->data;

        foreach ($userVoucherList as $voucherEntity) {
            $voucherDetail = VoucherConversionUtil::conversionUserVoucher($voucherEntity);
            if ($voucherDetail == null) {
                continue;
            }
            if ($voucherDetail->enable_status == 1) {
                array_push($enable_voucher_list, $voucherDetail);
            } else {
                array_push($unable_voucher_list, $voucherDetail);
            }
        }
        if ($voucher_status_type == '1') {
            $voucherListResp->voucher_list = $enable_voucher_list;
        } elseif ($voucher_status_type == '-1') {
            $voucherListResp->voucher_list = $unable_voucher_list;
        } else {
            $voucherListResp->voucher_list = array_merge($enable_voucher_list, $unable_voucher_list);
        }

        $result->setSuccessWithResult($voucherListResp);
        $this->render($result);

    }

    /**
     * 结算页查询可用券
     */
    public function querySettlementVoucherList()
    {
        $result = new Result();
        /** @var QueryVoucherListForProductParams $queryParams */
        $queryParams = $this->requestObject(QueryVoucherListForProductParams::class);

        if ($queryParams->product_list == null) {
            $result->setError(Config_Error::ERR_VOUCHER_PRODUCT_IS_NULL);
            $this->render($result);
        }
        $userId = $this->userId;
        $voucherEntityListRet = PromotionVoucherServiceImpl::getInstance()->queryUserVoucherList($userId);
        if (!Result::isSuccess($voucherEntityListRet)) {
            $this->render($voucherEntityListRet);
        }

        $voucherListResp = new VoucherListResp();
        $enable_voucher_list = [];
        $unable_voucher_list = [];
        $userVoucherList = $voucherEntityListRet->data;


        $totalProductPrice = 0;
        /** @var ProductCountParams $orderProductInfo */
        foreach ($queryParams->product_list as $orderProductInfo) {
            $skuPriceRet = ProductServiceImpl::getInstance()->getProductSkuDetail($orderProductInfo->sku_id);
            $productPrice = $skuPriceRet->data->current_price;
            $totalProductPrice = $productPrice * $orderProductInfo->count + $totalProductPrice;
        }


        foreach ($userVoucherList as $voucherEntity) {
            $voucherDetailResp = VoucherConversionUtil::conversionUserVoucher($voucherEntity);

            if ($voucherDetailResp->use_status == 1) {
                continue;
            }
            if ($voucherDetailResp->enable_status != Config_Const::VOUCHER_ENABLE_STATUS_VALID) {
//                array_push($unable_voucher_list, $voucherDetailResp);
                continue;
            }

            $voucherDetail = $voucherDetailResp->voucher_detail;
            if ($voucherDetail->price_limit > $totalProductPrice) {
                $voucherDetailResp->enable_status = Config_Const::VOUCHER_ENABLE_STATUS_INCOMPATIBLE;
                $agioPrice = $voucherDetail->price_limit - $totalProductPrice;
                $voucherDetailResp->unavailable_reason = '还差' . PriceHelper::conversionDisplayPrice($agioPrice) . "元可用";
                array_push($unable_voucher_list, $voucherDetailResp);
                continue;
            }
            array_push($enable_voucher_list, $voucherDetailResp);
        }
        $voucherListResp->voucher_list = array_merge($enable_voucher_list, $unable_voucher_list);
        $result->setSuccessWithResult($voucherListResp);
        $this->render($result);

    }

//    private function autoDistributeVoucher($userId)
//    {
//        $enableVoucherListRet = PromotionVoucherServiceImpl::getInstance()->queryEnableVoucherList();
//        if (!Result::isSuccess($enableVoucherListRet)) {
//            return;
//        }
//        $enableVoucherList = $enableVoucherListRet->data;
//        $now = time();
//        /** @var \model\promotion\dto\PromotionVoucherDTO $voucherInfo */
//        foreach ($enableVoucherList as $voucherInfo) {
//            $voucherEntityRecord = VoucherEntityModel::getInstance()->queryUserVoucherEntityByVoucherId($userId, $voucherInfo->id);
//            if ($voucherEntityRecord && $voucherEntityRecord->expire_timestamp > $now) {
//                return;
//            }
//            PromotionVoucherServiceImpl::getInstance()->distributeVoucher($userId, $voucherInfo->id);
//        }
//    }

}