<?php

use facade\response\Result;
use model\order\dto\OrderInfoDTO;
use service\logistics\impl\LogisticsServiceImpl;
use service\order\impl\OrderServiceImpl;

class Logistics extends FUR_Controller
{
    private $userId;

    public function __construct()
    {
        $this->userId = $this->getRequestUserId();
    }


    /**
     * 创建订单信息
     */
    public function getOrderLogisticsInfo()
    {
        $result = new Result();
        $orderService = OrderServiceImpl::getInstance();
        $orderId = (int)$this->request('order_id', 0);
        $orderInfoRet = $orderService->getOrderInfo($orderId);
        if (!Result::isSuccess($orderInfoRet)) {
            $result->setError(Config_Error::ERR_LOGISTICS_ORDER_NOT_EXIST);
            $this->render($result);
        }
        /** @var OrderInfoDTO $orderInfoDTO */
        $orderInfoDTO = $orderInfoRet->data;


        $logisticsInfoRet = LogisticsServiceImpl::getInstance()->getOrderLogisticsDetail($orderInfoDTO);
        $this->render($logisticsInfoRet);
    }


}