<?php
require_once(PUBLIC_PATH . '/lib/alipay/AopSdk.php');


use business\order\CalculateOrderPriceUtil;
use business\order\OrderInfoConversionUtil;
use facade\request\maintain\CreateLeaderboardRecordParams;
use facade\request\order\CalculateOrderPriceParams;
use facade\request\order\CreateOrderParams;
use facade\request\order\CreateOrderPayInfoParams;
use facade\request\order\QueryOrderListParams;
use facade\request\order\QueryOrderPayInfoParams;
use facade\response\base\PageInfoResp;
use facade\response\base\RemindResp;
use facade\response\order\CalculatePriceResp;
use facade\response\order\CreateOrderPayInfoResp;
use facade\response\order\CreateOrderProductsResp;
use facade\response\order\OrderRemindResp;
use facade\response\order\product\OrderProductResp;
use facade\response\order\QueryOrderPayInfoResp;
use facade\response\order\QueryPayDetailInfoResp;
use facade\response\order\UserOrderListResp;
use facade\response\Result;
use facade\response\shoppingCart\ShoppingCartResp;
use lib\PriceHelper;
use model\order\dto\OrderInfoDTO;
use model\order\dto\OrderProductDTO;
use model\product\dto\ProductSkuDTO;
use service\admin\impl\ProductAdminServiceImpl;
use service\alipay\impl\AlipayServiceImpl;
use service\maintain\impl\MaintainLeaderboardServiceImpl;
use service\order\impl\OrderServiceImpl;
use service\product\impl\ProductServiceImpl;
use service\promotion\impl\PromotionVoucherServiceImpl;
use service\shoppingCart\impl\ShoppingCartServiceImpl;
use service\user\impl\StarServiceImpl;
use service\user\impl\UserServiceImpl;
use service\wechat\impl\WechatPayServiceImpl;

class Order extends FUR_Controller
{

    public function calculateOrderPrice()
    {
        $result = new Result();
        /** @var CalculateOrderPriceParams $calculateOrderPriceParams */
        $calculateOrderPriceParams = $this->requestObject(CalculateOrderPriceParams::class);
        if ($calculateOrderPriceParams->product_list == null) {
            $shoppingCartResp = new ShoppingCartResp();
            $result->setSuccessWithResult($shoppingCartResp);
            $this->render($result);
        }
        $calculatePriceRet = CalculateOrderPriceUtil::getInstance()->calculatePrice($calculateOrderPriceParams);
        $this->render($calculatePriceRet);
    }


    public function createOrder()
    {
        $result = new Result();
        $orderService = OrderServiceImpl::getInstance();
        /** @var  CreateOrderParams $createOrderParams */
        $createOrderParams = $this->requestObject(CreateOrderParams::class);
        $createOrderParams->need_chose_voucher = 0;
        $createOrderParams->user_id = $this->getRequestUserId();
        /**
         * 校验参数
         */
        $userId = $this->getRequestUserId();


        if ($createOrderParams->product_list == null || empty($createOrderParams->product_list)) {
            $result->setError(\Config_Error::ERR_CREATE_ORDER_PRODUCT_STOCK_NOT_ENOUGH);
            $this->render($result);
        }

        if ($createOrderParams->shipping_id == null) {
            $result->setError(\Config_Error::ERR_CREATE_ORDER_SHIPPING_ID_IS_NULL);
            $this->render($result);
        }

        $calculatePriceRet = CalculateOrderPriceUtil::getInstance()->calculatePrice($createOrderParams);
        if (!Result::isSuccess($calculatePriceRet)) {
            $this->render($calculatePriceRet);
        }


        /** @var CalculatePriceResp $calculatePriceResp */
        $calculatePriceResp = $calculatePriceRet->data;
        $buildOrderInfoRet = OrderInfoConversionUtil::buildOrderInfoDTO($calculatePriceResp, $createOrderParams);
        if (!Result::isSuccess($buildOrderInfoRet)) {
            $this->render($buildOrderInfoRet);
        }

        //检查库存
        /** @var OrderProductResp $orderProduct */
        foreach ($calculatePriceResp->order_product_list as $orderProduct) {
            if ($orderProduct->in_stock == 0) {
                $result->setError(\Config_Error::ERR_ORDER_STOCK_IS_NOT_ENOUGH);
                $this->render($result);
            }
            if ($orderProduct->publish_status != Config_Const::PUBLISHED_STATUS_ONLINE) {
                $result->setError(\Config_Error::ERR_ORDER_PRODUCT_IS_INVALID);
                $this->render($result);
            }
        }
        $orderInfoDTO = $buildOrderInfoRet->data;
        //创建订单信息
        $createOrderInfoRet = $orderService->createOrderInfo($orderInfoDTO);
        if (!Result::isSuccess($createOrderInfoRet)) {
            $this->render($createOrderInfoRet);
        }
        $orderId = $createOrderInfoRet->data;
        $orderInfoDTO->id = $orderId;

        $skuIdList = [];
        /** @var OrderProductResp $orderProduct */
        foreach ($calculatePriceResp->order_product_list as $orderProduct) {
            //更新库存销量
            $updateRet = $orderService->updateProductStock($orderProduct->item_id, $orderProduct->sku_id, $orderProduct->count);
            if (!Result::isSuccess($updateRet)) {
                $orderService->deleteOrder($orderId, $userId);
                $this->render($updateRet);
            }
            //写商品库
            $orderProductInfoDTO = OrderInfoConversionUtil::conversionOrderProductDTO($orderProduct, $orderId);
            $orderProductRet = $orderService->createOrderProductInfo($orderProductInfoDTO);
            if (!Result::isSuccess($orderProductRet)) {
                $orderService->deleteOrder($orderId, $userId);
                $this->render($orderProductRet);
            }
            array_push($skuIdList, $orderProductInfoDTO->sku_id);

        }
        //移出购物车
//            ShoppingCartServiceImpl::getInstance()->removeFromCart($userId, $skuIdList);
        //更新优惠券
        if ($createOrderParams->voucher_sn) {
            $useVoucherRet = PromotionVoucherServiceImpl::getInstance()->useVoucher($createOrderParams->voucher_sn, $orderId);
            if (!Result::isSuccess($useVoucherRet)) {
                $orderService->deleteOrder($orderId, $userId);
                $this->render($useVoucherRet);
            }
        }

        if ($createOrderParams->voucher_sn_list) {
            foreach ($createOrderParams->voucher_sn_list as $voucherSn) {
                $useVoucherRet = PromotionVoucherServiceImpl::getInstance()->useVoucher($voucherSn, $orderId);
                if (!Result::isSuccess($useVoucherRet)) {
                    $orderService->deleteOrder($orderId, $userId);
                    $this->render($useVoucherRet);
                }
            }

        }

        //订单信息创建完成
        $createOrderProductsResp = new CreateOrderProductsResp();
        $orderDisplayInfoResp = OrderInfoConversionUtil::getInstance()->conversionOrderPriceInfo($orderInfoDTO);
        $createOrderProductsResp->order_info = $orderDisplayInfoResp;

        $result->setSuccessWithResult($createOrderProductsResp);
        $this->render($result);


    }


    public function getOrderList()
    {
        $result = new Result();
        /** @var QueryOrderListParams $queryOrderListParams */
        $queryOrderListParams = $this->requestObject(QueryOrderListParams::class);
        if ($queryOrderListParams->order_type == null) {
            $result->setError(\Config_Error::ERR_ORDER_PARAMS_IS_NOT_FULL);
            $this->render($result);
        }

        $userId = $this->getRequestUserId();
        $index = $queryOrderListParams->index;
        $orderList = [];
        $orderService = OrderServiceImpl::getInstance();
        $orderListRet = $orderService->getOrderList($userId, $queryOrderListParams->order_type);
        $orderListResp = new UserOrderListResp();
        foreach ($orderListRet->data as $orderInfoDTO) {
            $orderDetailResp = OrderInfoConversionUtil::conversionOrderDetailResp($orderInfoDTO);
            if ($orderDetailResp == null) {
                continue;
            }
            array_push($orderList, $orderDetailResp);
        }

        $pageInfo = PageInfoResp::buildPageInfoResp($index, $queryOrderListParams->page_size, count($orderListRet->data));

        $orderListResp->order_list = $orderList;
        $orderListResp->page_info = $pageInfo;
        $result->setSuccessWithResult($orderListResp);
        $this->render($result);
    }


    public function queryOrderDetail()
    {
        $result = new Result();
        $orderSn = $this->request('order_sn');
        $userId = $this->getRequestUserId();
        $orderService = OrderServiceImpl::getInstance();
        $orderInfoRet = $orderService->getOrderInfoByOrderSn($orderSn);
        if (!Result::isSuccess($orderInfoRet)) {
            $result->setSuccessWithResult(null);
            $this->render($result);
        }
        if ($userId != $orderInfoRet->data->user_id) {
            $result->setSuccessWithResult(null);
            $this->render($result);
        }
        $orderDetailResp = OrderInfoConversionUtil::conversionOrderDetailResp($orderInfoRet->data);
        $result->setSuccessWithResult($orderDetailResp);
        $this->render($result);

    }


    /**
     *
     * 获取订单提醒
     */

    public function getOrderRemind()
    {
        $result = new Result();
        $orderRemindResp = new OrderRemindResp();
        $userId = $this->getRequestUserId();
        if ($userId == 0) {
            $result->setError(Config_Error::ERR_USER_NOT_EXIST);
            $this->render($result);
        }

        $orderService = OrderServiceImpl::getInstance();
        $remindNumRet = $orderService->getOrderRemindNum($userId);
        if (!Result::isSuccess($remindNumRet)) {
            $this->render($remindNumRet);
        }

        $waitPayRemindResp = new RemindResp();
        $waitPayRemindResp->badge_num = $remindNumRet->data->wait_pay;
        $waitPayRemindResp->red_point = $waitPayRemindResp->badge_num > 0;
        $orderRemindResp->wait_pay = $waitPayRemindResp;//付款

        $waitReceiveRemindResp = new RemindResp();
        $waitReceiveRemindResp->badge_num = $remindNumRet->data->wait_receive;
        $waitReceiveRemindResp->red_point = $waitReceiveRemindResp->badge_num > 0;
        $orderRemindResp->wait_receive = $waitReceiveRemindResp;//收货

        $waitReviewRemindResp = new RemindResp();
        $waitReviewRemindResp->badge_num = $remindNumRet->data->wait_review;
        $waitReviewRemindResp->red_point = $waitReviewRemindResp->badge_num > 0;
        $orderRemindResp->wait_review = $waitReviewRemindResp;//评价

        $waitShipRemindResp = new RemindResp();
        $waitShipRemindResp->badge_num = $remindNumRet->data->wait_ship;
        $waitShipRemindResp->red_point = $waitShipRemindResp->badge_num > 0;
        $orderRemindResp->wait_ship = $waitShipRemindResp;//发货


        $result->setSuccessWithResult($orderRemindResp);
        $this->render($result);
    }


    public function getOrderRemindV2()
    {
        $result = new Result();
        $orderRemindResp = new OrderRemindResp();
        $userId = $this->getRequestUserId();
        if ($userId == 0) {
            $result->setError(Config_Error::ERR_USER_NOT_EXIST);
            $this->render($result);
        }

        $orderService = OrderServiceImpl::getInstance();
        $remindNumRet = $orderService->getOrderRemindNum($userId);
        if (!Result::isSuccess($remindNumRet)) {
            $this->render($remindNumRet);
        }

        $waitPayRemindResp = new RemindResp();
        $waitPayRemindResp->badge_num = $remindNumRet->data->wait_pay;
        $waitPayRemindResp->red_point = $waitPayRemindResp->badge_num > 0;
        $orderRemindResp->wait_pay = $waitPayRemindResp;//付款

        $waitReceiveRemindResp = new RemindResp();
        $waitReceiveRemindResp->badge_num = $remindNumRet->data->wait_receive;
        $waitReceiveRemindResp->red_point = $waitReceiveRemindResp->badge_num > 0;
        $orderRemindResp->wait_receive = $waitReceiveRemindResp;//收货

        $waitReviewRemindResp = new RemindResp();
        $waitReviewRemindResp->badge_num = $remindNumRet->data->wait_review;
        $waitReviewRemindResp->red_point = $waitReviewRemindResp->badge_num > 0;
        $orderRemindResp->wait_review = $waitReviewRemindResp;//评价

        $waitShipRemindResp = new RemindResp();
        $waitShipRemindResp->badge_num = $remindNumRet->data->wait_ship;
        $waitShipRemindResp->red_point = $waitShipRemindResp->badge_num > 0;
        $orderRemindResp->wait_ship = $waitShipRemindResp;//发货


        $productCountRet = ShoppingCartServiceImpl::getInstance()->queryUserCartProductCount($userId);
        $orderRemindResp->shopping_cart_count = $productCountRet->data;
        $result->setSuccessWithResult($orderRemindResp);

        $this->render($result);
    }

    /**
     * 取消订单,交易关闭
     */
    public function cancelOrder()
    {

        $result = new Result();
        $orderSn = $this->request('order_sn');
        $userId = $this->getRequestUserId();
        if ($userId == 0 || $orderSn == null) {
            $result->setError(Config_Error::ERR_CANCEL_ORDER_PARAMS_IS_NOT_FULL);
            $this->render($result);
        }
        $orderServiceImpl = OrderServiceImpl::getInstance();
        $orderInfoRet = $orderServiceImpl->getOrderInfoByOrderSn($orderSn);
        if (!Result::isSuccess($orderInfoRet, true)) {
            $result->setError(Config_Error::ERR_ORDER_INFO_IS_NOT_EXIST);
            $this->render($result);
        }
        /** @var OrderInfoDTO $orderInfo */
        $orderInfo = $orderInfoRet->data;
        $order_id = $orderInfo->id;
        if ($userId != $orderInfo->user_id) {
            $result->setError(Config_Error::ERR_ORDER_INFO_IS_NOT_EXIST);
            $this->render($result);
        }

        $cancelOrderRet = $orderServiceImpl->cancelOrder($orderInfo->id);
        if (!Result::isSuccess($cancelOrderRet)) {
            $result->setError(Config_Error::ERR_CANCEL_ORDER_IS_FAILED);
            $this->render($result);
        }


        /**
         * 加库存减销量
         */
        $orderProductListRet = $orderServiceImpl->queryOrderProductList($order_id);
        if (!Result::isSuccess($orderProductListRet)) {
            $result->setSuccessWithResult($cancelOrderRet->data);
            $this->render($result);
        }
        $orderProductList = $orderProductListRet->data;

        /** @var OrderProductDTO $orderProductDTO */
        foreach ($orderProductList as $orderProductDTO) {
            $productSkuService = ProductServiceImpl::getInstance();
            $productSkuRet = $productSkuService->getProductSkuDetail($orderProductDTO->sku_id);
            if (!Result::isSuccess($productSkuRet)) {
                continue;
            }
            /** @var ProductSkuDTO $productSku */
            $productSku = $productSkuRet->data;


            $productItemRet = $productSkuService->getProductItem($orderProductDTO->item_id);
            if (!Result::isSuccess($productItemRet)) {
                continue;
            }

            /** @var \model\product\dto\ProductItemDTO $productItem */
            $productItem = $productItemRet->data;
            $orderServiceImpl->updateProductStock($productItem->id, $productSku->id, 0 - $orderProductDTO->count);

        }

        //返还优惠券
        if ($orderInfoRet->data->voucher_sn != null) {
            PromotionVoucherServiceImpl::getInstance()->returnVoucher($orderInfoRet->data->voucher_sn, $order_id);
        }


        $result->setSuccessWithResult($cancelOrderRet->data);
        $this->render($result);

    }


    /**
     * 删除订单
     */

    public function deleteOrder()
    {

        $result = new Result();
        $userId = $this->getRequestUserId();
        $orderSn = $this->request('order_sn');
        if ($userId == 0 || $orderSn == null) {
            $result->setError(Config_Error::ERR_DELIVER_ORDER_IS_NOT_FULL);
            $this->render($result);
        }
        $orderServiceImpl = OrderServiceImpl::getInstance();
        $deleteOrderRet = $orderServiceImpl->deleteOrder($orderSn);
        $result->setSuccessWithResult($deleteOrderRet->data);
        $this->render($result);

    }


    /**
     * 确认收货
     */
    public function confirmReceipt()
    {

        $result = new Result();
        $order_sn = $this->request('order_sn');

        $userId = $this->getRequestUserId(true);


        $orderServiceImpl = OrderServiceImpl::getInstance();
        $orderInfoRet = OrderServiceImpl::getInstance()->getOrderInfoByOrderSn($order_sn);

        /** @var OrderInfoDTO $orderInfo */
        $orderInfo = $orderInfoRet->data;

        $confirmReceiptRet = $orderServiceImpl->confirmReceipt($orderInfo->id, $userId);
        if (!Result::isSuccess($confirmReceiptRet)) {
            $result->setError(Config_Error::ERR_CONFIRM_RECEIPT_ORDER_IS_FAILED);
            $this->render($result);
        }


        $result->setSuccessWithResult($confirmReceiptRet->data);
        $this->render($result);

    }


    /**
     * @param int $order_sn
     * @param $pay_type
     * @param $trade_no
     * @param $gmt_payment
     * @param $buyer_pay_amount
     * @return null |null
     * 更新订单信息
     */
    private function updateOrderInfoToPay($order_sn, $pay_type, $trade_no, $gmt_payment, $buyer_pay_amount)
    {

        \FUR_Log::info("更新订单信息：" . "order_sn:" . $order_sn . " || pay_type: " . $pay_type . " || trade_no: " . $trade_no . " || gmt_payment:" . $gmt_payment . " || buyer_pay_amount:" . $buyer_pay_amount);
        $updateOrderInfoDTO = OrderServiceImpl::getInstance()->getOrderInfoByOrderSn($order_sn)->data;
        $updateOrderInfoDTO->order_sn = $order_sn;
        $updateOrderInfoDTO->payment_trade_no = $trade_no;
        $updateOrderInfoDTO->pay_status = Config_Const::PAY_STATUS_PAYED;//已完成
        $updateOrderInfoDTO->pay_timestamp = time();
        $updateOrderInfoDTO->delivery_timestamp = strtotime('+1 day');
        $updateOrderInfoDTO->pay_type = $pay_type;
        $updateOrderInfoDTO->order_status = Config_Const::ORDER_STATUS_HAS_PAY;//0.未付款 10.已付款 20.待发货30.已发货40.交易完成50交易关闭60.已评价70.待评价
        $updateOrderInfoDTO->total_price = $buyer_pay_amount;
        $updateOrderInfoDTO->pay_price = $buyer_pay_amount;
        \FUR_Log::info("订单待更新信息:", json_encode($updateOrderInfoDTO));


        //更新订单状态
        $orderService = OrderServiceImpl::getInstance();
        $orderService->updateOrder($updateOrderInfoDTO);
        \FUR_Log::info("更新订单支付信息成功");


        //更新订单交易记录
        $orderInfoRet = $orderService->getOrderInfoByOrderSn($updateOrderInfoDTO->order_sn);
        /** @var OrderInfoDTO $orderInfoDTO */
        $orderInfoDTO = $orderInfoRet->data;
        $updateRet = $orderService->updateOrderTransaction($orderInfoDTO);
        if (!Result::isSuccess($updateRet)) {
            return null;
        }


        \FUR_Log::info("更新订单结束");
        return null;
    }


    /**
     * 查询支付是否成功
     */
    public function queryOrderPayInfo()
    {
        $result = new Result();
        $userId = $this->getRequestUserId();
        /** @var CreateOrderPayInfoParams $createOrderPayInfoParams */
        $createOrderPayInfoParams = $this->requestObject(CreateOrderPayInfoParams::class);
        \FUR_Log::info("createOrderPayInfoParams---start" . json_encode($createOrderPayInfoParams));
        if (!$createOrderPayInfoParams->order_sn) {
            $result->setError(Config_Error::ERR_ORDER_ID_CAN_NOT_BE_NULL);
            $this->render($result);
        }
        $orderService = OrderServiceImpl::getInstance();
        $orderDataRet = $orderService->getOrderInfoByOrderSn($createOrderPayInfoParams->order_sn);
        if (!Result::isSuccess($orderDataRet)) {
            $this->render($orderDataRet);
        }
        /** @var OrderInfoDTO $orderData */
        $orderData = $orderDataRet->data;

        $product_name = 'product';
        $total_price = $orderData->pay_price;
        $order_sn = $orderData->order_sn;
        $wechatPayService = WechatPayServiceImpl::getInstance();

        //微信公众号支付
        \FUR_Log::info("---------createOrderPayInfoParams----------" . json_encode($createOrderPayInfoParams));
        $wechatPayService->setTotalFee($total_price);
        $wechatPayService->setOutTradeNo($order_sn);
        $wechatPayService->setOrderName($product_name);
        \FUR_Log::info("---------createOrderPayInfoParams----------" . json_encode($createOrderPayInfoParams));

        $openIdRet = UserServiceImpl::getInstance()->queryLoginAccountByUserId($userId, Config_Const::USER_LOGIN_TYPE_WX_MINI);
        $openId = $openIdRet->data;
        $payData = $wechatPayService->createMiniBizPackage($total_price, $order_sn, $product_name, time(), $openId, $createOrderPayInfoParams->code);

        $result->setSuccessWithResult($payData);
        $this->render($result);
    }


    /**
     * 支付宝回调
     */
    public function aliCallBack()
    {

        \FUR_Log::info("支付宝回调传参：", json_encode($_POST));
        $aop = new \AopClient;
        $aop->alipayrsaPublicKey = \FUR_Config::get_alipay_config('ALIPAY_PUBLIC_KEY');
        $flag = $aop->rsaCheckV1($_POST, \FUR_Config::get_alipay_config('ALIPAY_PUBLIC_KEY'), "RSA2");


        /**验签成功*/
        if ($flag) {

            $trade_status = $this->request('trade_status');
            $order_sn = $this->request('out_trade_no');
            $total_amount = $this->request('total_amount');
            $orderService = OrderServiceImpl::getInstance();
            $orderInfoDTO = $orderService->getOrderInfoByOrderSn($order_sn)->data;
            $pay_price = PriceHelper::conversionDisplayPrice($orderInfoDTO->pay_price);
            \FUR_Log::info("订单信息:", json_encode($orderInfoDTO));


            /**验签通过后核实如下参数trade_status、out_trade_no、total_amount*/
//            if ($trade_status == 'TRADE_SUCCESS' && $pay_price == $total_amount && $orderInfoDTO->order_sn == $order_sn) {
            if ($trade_status == 'TRADE_SUCCESS' && $orderInfoDTO->order_sn == $order_sn) {
                $trade_no = $this->request('trade_no');
                $gmt_payment = time();
                $buyer_pay_amount = $this->request('buyer_pay_amount') * 100;

                try {
                    $this->updateOrderInfoToPay($order_sn, OrderInfoDTO::ALI_APP_PAY_TYPE, $trade_no, $gmt_payment, intval($buyer_pay_amount));
                    echo 'success';

                } catch (Exception $e) {

                    \FUR_Log::info("支付异常:", json_encode($e));
                    \FUR_Log::info("支付异常:", $e->getMessage());
                    \FUR_Log::info("支付异常:", $e->getLine());
                    \FUR_Log::info("支付异常:", $e->getTraceAsString());

                }
            }
        } else {

            \FUR_Log::info("支付宝回调验签失败:", json_encode($flag));
            return;

        }

    }

    /**
     * 微信支付回调
     */
    public function wechatCallBack()
    {

        $config = array(
            'mch_id' => \FUR_Config::get_wechat_pay_config('mchid'),
            'appid' => \FUR_Config::get_wechat_pay_config('appid'),
            'key' => \FUR_Config::get_wechat_pay_config('mch_key'),
        );


        $postStr = isset($GLOBALS['HTTP_RAW_POST_DATA']) ? $GLOBALS['HTTP_RAW_POST_DATA'] : file_get_contents("php://input");
        $postObj = simplexml_load_string($postStr, 'SimpleXMLElement', LIBXML_NOCDATA);

        \FUR_Log::info("微信回调post：", json_encode($postObj));
        if ($postObj === false) {
            die('parse xml error');
        }
        if ($postObj->return_code != 'SUCCESS') {
            die($postObj->return_msg);
        }
        if ($postObj->result_code != 'SUCCESS') {
            die($postObj->err_code);
        }

        $arr = (array)$postObj;
        unset($arr['sign']);
        $order_sn = $postObj->out_trade_no;

        $weChatPayServiceImpl = WechatPayServiceImpl::getInstance();

        if ($weChatPayServiceImpl::getSign($arr, $config['key']) == $postObj->sign) {

            \FUR_Log::info("微信验签成功:", json_encode($postObj));
            $transaction_id = $postObj->transaction_id;
            $time_end = $postObj->time_end;
            $total_fee = $postObj->total_fee;

            \FUR_Log::info("微信支付更新订单参数：order_sn:$order_sn. ||:transaction_id: " . $transaction_id . " ||time_end :" . $time_end . " ||total_fee: " . $total_fee);
            try {

                $this->updateOrderInfoToPay($order_sn, Config_Const::WECHAT_PAY_TYPE, $transaction_id, intval($time_end), intval($total_fee));
                echo '<xml><return_code><![CDATA[SUCCESS]]></return_code><return_msg><![CDATA[OK]]></return_msg></xml>';

            } catch (Exception $e) {

                \FUR_Log::info("支付异常:", json_encode($e));
                \FUR_Log::info("支付异常:", $e->getMessage());
                \FUR_Log::info("支付异常:", $e->getLine());
                \FUR_Log::info("支付异常:", $e->getTraceAsString());

            }
        } else {

            echo '<xml> <return_code><![CDATA[FAIL]]></return_code></xml>';
        }

        return $arr;
    }


    public function pretreatmentOrder()
    {

        $result = new Result();
        $order_id = $this->request('order_id');
        if ($this->userId == 0 || $order_id == null) {
            $result->setError(Config_Error::ERR_CONFIRM_RECEIPT_ORDER_INFO_IS_NOT_FULL);
            $this->render($result);
        }
        $orderServiceImpl = OrderServiceImpl::getInstance();
        $confirmReceiptRet = $orderServiceImpl->confirmReceipt($order_id, $this->userId);
        $result->setSuccessWithResult($confirmReceiptRet->data);
        $this->render($result);

    }

}