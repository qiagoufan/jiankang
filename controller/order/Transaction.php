<?php

use facade\response\order\OrderPayCallbackResp;
use facade\response\Result;
use model\dto\OrderInfoDTO;
use service\order\impl\OrderServiceImpl;

require_once(PUBLIC_PATH . '/lib/alipay/AopSdk.php');

class Transaction
{

    public function alipayCallBack()
    {

        //验签
        $aop = new \AopClient;
        $aop->alipayrsaPublicKey = \FUR_Config::get_alipay_config('ALIPAY_PUBLIC_KEY');
        $flag = $aop->rsaCheckV1($_POST, \FUR_Config::get_alipay_config('ALIPAY_PUBLIC_KEY'), "RSA2");
        \FUR_Log::info("alipay data:", json_encode($_POST));
        \FUR_Log::info("alipay flag:", json_encode($flag));

        //验签通过后核实如下参数trade_status、out_trade_no、total_amount
        if (!$flag) {
            //TODO log the error
            return;
        }
        $trade_status = $this->request('trade_status');
        $order_sn = $this->request('out_trade_no');
        $total_amount = $this->request('total_amount');
        $orderService = OrderServiceImpl::getInstance();
        $orderInfoDTO = $orderService->getOrderInfoByOrderSn($order_sn)->data;
        /**
         * TODO :remember check the total_amount in production env
         */
//       if ($trade_status != 'TRADE_SUCCESS' || $orderInfoDTO->order_sn != $order_sn || $orderInfoDTO->total_price / 100 != $total_amount) {
        if ($trade_status != 'TRADE_SUCCESS' || $orderInfoDTO->order_sn != $order_sn) {
            //TODO log the error
            \FUR_Log::info("trade failed---------alipay data:", json_encode($_POST));
            return;
        }
        try {
            $orderService = OrderServiceImpl::getInstance();
            $orderInfoDTO = new OrderInfoDTO();
            $orderInfoDTO->order_sn = $order_sn;
            $orderInfoDTO->payment_trade_no = $this->request('trade_no');
            $orderInfoDTO->pay_status = OrderInfoDTO::PAY_STATUS_FINISHED;//已完成
            $orderInfoDTO->pay_timestamp = strtotime($this->request('gmt_payment'));
            $orderInfoDTO->delivery_timestamp = strtotime('+1 day');
            $orderInfoDTO->pay_type = OrderInfoDTO::ALI_APP_PAY_TYPE;
            $orderInfoDTO->order_status = OrderInfoDTO::ORDER_STATUS_HAS_PAY;//0.未付款 10.已付款 20.待发货30.已发货40.交易完成50交易关闭60.已评价70.待评价
            $orderInfoDTO->total_price = $this->request('buyer_pay_amount') * 100;
            \FUR_Log::info("orderInfoDTO:", json_encode($orderInfoDTO));
            //更新订单状态
            $orderService->updateOrder($orderInfoDTO);
            //更新订单交易记录
            $orderInfoRet = $orderService->getOrderInfoByOrderSn($orderInfoDTO->order_sn);
            if (!Result::isSuccess($orderInfoRet)) {
                return null;
            }
            $orderRecord = $orderInfoRet->data;
            $orderService->updateOrderTransaction($orderRecord);
            echo 'success';
        } catch (Exception $e) {
            echo 'failure';
            \FUR_Log::info("支付异常:", json_encode($e));
            \FUR_Log::info("支付异常:", $e->getMessage());
            \FUR_Log::info("支付异常:", $e->getLine());
            \FUR_Log::info("支付异常:", $e->getTraceAsString());
        }

    }


}