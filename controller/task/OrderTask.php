<?php

use \service\order\impl\PromotionVoucherServiceImpl;
use facade\response\Result;
use model\dto\OrderInfoDTO;
use \service\product\impl\ProductServiceImpl;
use \facade\request\product\GetProductDetailParams;
use \model\dto\ProductSkuDTO;
use \business\order\OrderUtil;

class OrderTask extends FUR_Controller
{

    /**
     * @return |null
     * 关闭订单
     */
    public function closeOrder()
    {
        $result = new Result();
        $orderServiceImpl = PromotionVoucherServiceImpl::getInstance();
        $getUnPaidOrdersRet = $orderServiceImpl->getOverTimeUnPaidOrders();
        if (Result::isSuccess($getUnPaidOrdersRet)) {
            foreach ($getUnPaidOrdersRet->data as $orderInfoDTO) {
                //更新库存
                $orderServiceImpl->closeOrder($orderInfoDTO->id);
                $updateRet = OrderUtil::updateProductStock($orderInfoDTO->id, OrderInfoDTO::ADD_STOCK_TYPE);
                if ($updateRet == false) {
                    $result->setError(Config_Error::ERR_UPDATE_ORDER_STOCK_IS_FAILED);
                    $this->render($result);
                }
            }
        }
        $result->setSuccessWithResult(true);
        $this->render($result);

    }


    /**
     * 更新库存
     * @param $order_id
     */
    private function updateProductStock($order_id, $type)
    {
        $orderServiceImpl = PromotionVoucherServiceImpl::getInstance();
        $orderProductInfoRet = $orderServiceImpl->getOrderProductsInfo($order_id);
        $skuId = $orderProductInfoRet->data->sku_id;
        $productSkuDetailParams = new GetProductDetailParams();
        $productSkuDetailParams->sku_id = $skuId;
        $productServiceImpl = ProductServiceImpl::getInstance();
        $ProductSkuDetailRet = $productServiceImpl->getProductDetail($productSkuDetailParams);
        if (!Result::isSuccess($ProductSkuDetailRet)) {
            return null;
        }
        $productSkuDTO = $ProductSkuDetailRet->data;
        $stock = $productSkuDTO->stock;
        $sale = $productSkuDTO->sale;
        $count = $productSkuDTO->count;


        $productSkuService = ProductServiceImpl::getInstance();
        $productItemResult = $productSkuService->getProductItem($productSkuDTO->item_id);
        if (!Result::isSuccess($productItemResult)) {
            $this->render($productItemResult);
        }
        $productItemDTO = $productItemResult->data;
        $itemStock = $productItemDTO->stock;
        $itemSale = $productItemDTO->sale;
        $itemId = $productItemDTO->id;

        if ($type == OrderInfoDTO::ADD_STOCK_TYPE) {
            //加库存,减销量
            if ($sale >= 1) {
                $productSkuDTO = new ProductSkuDTO();
                $productSkuDTO->id = $skuId;
                $productSkuDTO->stock = $stock + $count;
                $productSkuDTO->sale = $sale - 1;
                $orderServiceImpl->addProductStock($productSkuDTO);


                //更新item库存
                $productItemDTO = new \model\dto\ProductItemDTO();
                $productItemDTO->id = $itemId;
                $productItemDTO->stock = $itemStock + $count;
                $productItemDTO->sale = $itemSale - 1;
                $orderServiceImpl->updateProductItemStockSale($productItemDTO);


            }
        }

        $productServiceImpl->clearProductDetailCache($skuId);
        $productAdminService = \service\admin\impl\ProductAdminServiceImpl::getInstance();
        $productAdminService->deleteProductItemCache($itemId);
    }
}