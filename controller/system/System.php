<?php


use facade\response\Result;
use facade\response\system\OTAResp;
use service\system\impl\SystemConfigServiceImpl;

class System extends FUR_Controller
{
    public function updateSysConfig()
    {
        $key = $this->request('key');
        $value = $this->request('value');
        $result = SystemConfigServiceImpl::getInstance()->setConfig($key, $value);
        $this->render($result);
    }
}
