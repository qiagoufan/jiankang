<?php


use facade\request\aliyun\media\CreateUploadImageParams;
use facade\request\aliyun\media\CreateUploadVideoParams;
use facade\request\aliyun\media\RefreshUploadVideoParams;
use facade\request\aliyun\oss\UploadFileParams;
use facade\response\Result;
use service\aliyun\impl\AliyunOssServiceImpl;
use service\aliyun\impl\VodServiceImpl;
use service\media\impl\LocalStorageServiceImpl;
use service\qiniu\impl\QiniuOssServiceImpl;

class Media extends FUR_Controller
{
    public $accessKeyId;
    public $accessKeySecret;
    public $regionId;


    public function createUploadVideo()
    {
        $params = $this->requestObject(CreateUploadVideoParams::class);
        $result = VodServiceImpl::getInstance()->createUploadVideo($params);
        $this->render($result);
    }

    public function refreshUploadVideo()
    {
        $params = $this->requestObject(RefreshUploadVideoParams::class);
        $result = VodServiceImpl::getInstance()->refreshUploadVideo($params);
        $this->render($result);
    }

    public function createUploadImage()
    {
        $params = $this->requestObject(CreateUploadImageParams::class);
        $result = VodServiceImpl::getInstance()->createUploadImage($params);
        $this->render($result);
    }

    public function uploadBase64Image()
    {

        $base64Data = $this->request('file_base_64');
        $fileName = $this->request('file_name');
        if (substr($base64Data, 0, 4) == 'data') {
            $base64Data = explode("base64,", $base64Data)[1];
        }
        $result = QiniuOssServiceImpl::getInstance()->uploadBase64Image($base64Data,$fileName);
        $this->render($result);
    }

    public function uploadImage()
    {
        $result = new Result();
        if (!isset($_FILES['file'])) {
            $result->setError(Config_Error::ERR_MEDIA_CREATE_IMAGE_FILE_NOT_EXISTS);
            $this->render($result);
        }
        $uploadFileParams = new UploadFileParams();
        $fileExtension = strrchr($_FILES['file']['name'], '.');
        $uploadFileParams->file_path = $_FILES['file']['tmp_name'];
        $uploadFileParams->object_name = 'jkjc/' . md5_file($uploadFileParams->file_path) . $fileExtension;
        $uploadRet = QiniuOssServiceImpl::getInstance()->uploadOssFile($uploadFileParams);
//        $uploadRet = LocalStorageServiceImpl::getInstance()->localStorage($uploadFileParams);
        $this->render($uploadRet);
    }


}