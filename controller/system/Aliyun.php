<?php


use AlibabaCloud\Client\AlibabaCloud;
use AlibabaCloud\Sts\Sts;
use facade\response\Result;
use facade\response\system\OSSResp;
use service\aliyun\impl\AliyunOssServiceImpl;

class Aliyun extends FUR_Controller
{
    public $accessKeyId;
    public $accessKeySecret;
    public $regionId;


    public function __construct()
    {
        $accessKeyId = FUR_Config::get_aliyun_config('accessKeyId');
        $accessKeySecret = FUR_Config::get_aliyun_config('accessKeySecret');
        $regionId = FUR_Config::get_aliyun_config('regionId');
        AlibabaCloud::accessKeyClient($accessKeyId, $accessKeySecret)->regionId($regionId)->asDefaultClient();
    }

    public function getOssSign()
    {

        $this->getRequestUserId();
        $stsResult = AliyunOssServiceImpl::getInstance()->getOssResp();
        $this->render($stsResult);
    }


}
