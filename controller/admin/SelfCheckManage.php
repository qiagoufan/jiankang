<?php

use business\product\JkProductConversionUtil;
use facade\request\PageParams;
use facade\request\tag\CreateTagEntityParams;
use facade\request\tag\QueryTagEntityListParams;
use facade\request\tag\TagEntityParams;
use facade\response\base\DataListResp;
use facade\response\base\PageInfoResp;
use facade\response\jiankang\selfCheck\SelfCheckQuestionResp;
use facade\response\Result;
use model\jk\dto\JkSelfCheckCategoryDTO;
use model\jk\dto\JkSelfCheckQuestionDTO;
use model\jk\dto\JkSelfCheckResultDTO;
use service\jk\impl\JkSelfCheckServiceImpl;
use service\product\impl\ProductServiceImpl;
use service\tag\impl\TagServiceImpl;

class SelfCheckManage extends FUR_Controller
{


    public function getSelfCheckCategoryList()
    {
        $result = new Result();

        /** @var PageParams $params */
        $params = $this->requestObject(PageParams::class);
        $listRet = JkSelfCheckServiceImpl::getInstance()->queryJkSelfCheckCategoryList($params);
        $countRet = JkSelfCheckServiceImpl::getInstance()->queryJkSelfCheckCategoryCount();
        $listResp = new DataListResp();
        foreach ($listRet->data as $item) {
            array_push($listResp->list, $item);
        }
        $listResp->page_info = PageInfoResp::buildPageInfoResp($params->index, $params->page_size, count($listResp->list), $countRet->data);
        $result->setSuccessWithResult($listResp);
        $this->render($result);
    }


    public function getSelfCheckCategoryDetail()
    {

        $id = $this->request('id');
        $detailRet = JkSelfCheckServiceImpl::getInstance()->getJkSelfCheckCategory($id);
        $this->render($detailRet);
    }


    public function createSelfCheckCategory()
    {

        $selfCheckCategory = new JkSelfCheckCategoryDTO();
        $selfCheckCategory->category_name = $this->request('category_name');
        $selfCheckCategory->category_cover = $this->request('category_cover');
        $selfCheckCategory->seq = intval($this->request('seq'));
        $selfCheckCategory->calculation_type = intval($this->request('calculation_type'));
        $result = JkSelfCheckServiceImpl::getInstance()->insertJkSelfCheckCategory($selfCheckCategory);
        $this->render($result);

    }


    public function deleteSelfCheckCategory()
    {
        $result = new Result();
        $id = $this->request('id');

        JkSelfCheckServiceImpl::getInstance()->deleteJkSelfCheckCategory($id);

        $result->setSuccessWithResult(true);
        $this->render($result);
    }


    public function updateSelfCheckCategory()
    {
        $result = new Result();

        $selfCheckCategory = new JkSelfCheckCategoryDTO();
        $selfCheckCategory->category_name = $this->request('category_name');

        $selfCheckCategory->calculation_type = intval($this->request('calculation_type'));
        $category_cover = $this->request('category_cover');
        if ($category_cover) {
            $selfCheckCategory->category_cover = $category_cover;
        }
        $selfCheckCategory->id = $this->request('id');
        $selfCheckCategory->seq = intval($this->request('seq'));
        JkSelfCheckServiceImpl::getInstance()->updateJkSelfCheckCategory($selfCheckCategory);
        $result->setSuccessWithResult(true);
        $this->render($result);
    }


    public function getSelfCheckDetail()
    {
        $result = new Result();
        /** @var PageParams $params */
        $id = $this->request('id');
        $detailRet = JkSelfCheckServiceImpl::getInstance()->getJkSelfCheckCategory($id);
        /** @var JkSelfCheckCategoryDTO $category */
        $category = $detailRet->data;

        $questionListRet = JkSelfCheckServiceImpl::getInstance()->getJkSelfCheckQuestionList($category->id);
        $questionList = $questionListRet->data;
        $category->question_list = [];
        /** @var JkSelfCheckQuestionDTO $question */
        foreach ($questionList as $question) {

            $questionResp = new  SelfCheckQuestionResp();
            $questionResp->id = $question->id;
            $questionResp->category_id = $question->category_id;
            $questionResp->question_title = $question->question_title;
            $questionResp->question_sort_num = $question->question_sort_num;
            $questionResp->question_option_list = json_decode($question->question_option);
            array_push($category->question_list, $questionResp);
        }


        $result->setSuccessWithResult($category);
        $this->render($result);
    }

    public
    function getSelfCheckList()
    {
        $result = new Result();
        /** @var PageParams $params */
        $params = $this->requestObject(PageParams::class);


        $listRet = JkSelfCheckServiceImpl::getInstance()->queryJkSelfCheckCategoryList($params);
        $countRet = JkSelfCheckServiceImpl::getInstance()->queryJkSelfCheckCategoryCount();
        $listResp = new DataListResp();
        /** @var JkSelfCheckCategoryDTO $category */
        foreach ($listRet->data as $category) {

            $questionListRet = JkSelfCheckServiceImpl::getInstance()->getJkSelfCheckQuestionList($category->id);
            $questionList = $questionListRet->data;
            $category->question_list = [];
            $category->title = '';
            /** @var JkSelfCheckQuestionDTO $question */
            foreach ($questionList as $question) {
                if ($category->title == null) {
                    $category->title = $question->question_title;
                }
                $questionResp = new  SelfCheckQuestionResp();
                $questionResp->id = $question->id;
                $questionResp->category_id = $question->category_id;
                $questionResp->question_title = $question->question_title;
                $questionResp->question_sort_num = $question->question_sort_num;
                $questionResp->question_option_list = json_decode($question->question_option);
                array_push($category->question_list, $questionResp);
            }
            if ($category->title == null) {
                $category->title = '暂无问题';
            }
            array_push($listResp->list, $category);

        }

        $listResp->page_info = PageInfoResp::buildPageInfoResp($params->index, $params->page_size, count($listResp->list), $countRet->data);
        $result->setSuccessWithResult($listResp);
        $this->render($result);
    }


    public function createSelfCheckQuestion()
    {
        $result = new Result();
        $category_id = $this->request('category_id');
        $question_list = json_decode($this->request('question_list', [], false));
        FUR_Log::info('question_list', json_encode($question_list));
        foreach ($question_list as $questionInfo) {
            $questionDTO = new JkSelfCheckQuestionDTO();
            $questionDTO->category_id = $category_id;
            $decodeQuestionInfo = ($questionInfo);
            $questionDTO->question_sort_num = $decodeQuestionInfo->sort_num;
            $questionDTO->question_title = $decodeQuestionInfo->question_title;
            $questionDTO->question_option = json_encode($decodeQuestionInfo->question_option);
            JkSelfCheckServiceImpl::getInstance()->insertJkSelfCheckQuestionDTO($questionDTO);
        }
        $result->setSuccessWithResult(true);
        $this->render($result);

    }


    public function updateSelfCheckQuestion()
    {
        $result = new Result();
        $category_id = $this->request('category_id');
        $question_list = json_decode($this->request('question_list', [], false));
        FUR_Log::info('question_list', json_encode($question_list));
        JkSelfCheckServiceImpl::getInstance()->deleteJkSelfCheckQuestionById($category_id);
        foreach ($question_list as $questionInfo) {
            $questionDTO = new JkSelfCheckQuestionDTO();
            $questionDTO->category_id = $category_id;
            $decodeQuestionInfo = ($questionInfo);
            $questionDTO->question_sort_num = $decodeQuestionInfo->sort_num;
            $questionDTO->question_title = $decodeQuestionInfo->question_title;
            $questionDTO->question_option = json_encode($decodeQuestionInfo->question_option);
            JkSelfCheckServiceImpl::getInstance()->insertJkSelfCheckQuestionDTO($questionDTO);
        }
        $result->setSuccessWithResult(true);
        $this->render($result);
    }


    public function deleteSelfCheck()
    {
        $result = new Result();
        $category_id = $this->request('id');
        JkSelfCheckServiceImpl::getInstance()->deleteJkSelfCheckQuestionById($category_id);
        JkSelfCheckServiceImpl::getInstance()->deleteJkSelfCheckResultById($category_id);
        $result->setSuccessWithResult(true);
        $this->render($result);
    }

    public function updateSelfCheckResult()
    {
        $result = new Result();
        $category_id = $this->request('id');
        JkSelfCheckServiceImpl::getInstance()->deleteJkSelfCheckResultById($category_id);


        $result_list = json_decode($this->request('result_list', [], false));

        foreach ($result_list as $scoreInfo) {
            $resultDTO = new JkSelfCheckResultDTO();
            $resultDTO->category_id = $category_id;
            $resultDTO->start_score = $scoreInfo->start_score;
            $resultDTO->end_score = $scoreInfo->end_score;
            $resultDTO->suggest = $scoreInfo->suggest;
            $resultDTO->disease = $scoreInfo->disease;
            $insertRet = JkSelfCheckServiceImpl::getInstance()->insertJkSelfCheckResultDTO($resultDTO);
            $id = $insertRet->data;

            $cleanParams = new  TagEntityParams();
            $cleanParams->tag_biz_id = $id;
            $cleanParams->tag_biz_type = Config_Const::BIZ_TYPE_SELF_CHECK_RESULT;
            $cleanParams->entity_biz_type = Config_Const::BIZ_TYPE_PRODUCT;
            TagServiceImpl::getInstance()->cleanTagEntity($cleanParams);
            foreach ($scoreInfo->product_list as $itemId) {
                $createTagEntityParams = new CreateTagEntityParams();
                $createTagEntityParams->tag_biz_id = $id;
                $createTagEntityParams->tag_biz_type = Config_Const::BIZ_TYPE_SELF_CHECK_RESULT;
                $createTagEntityParams->entity_biz_id = $itemId;
                $createTagEntityParams->entity_biz_type = Config_Const::BIZ_TYPE_PRODUCT;
                TagServiceImpl::getInstance()->createTagEntity($createTagEntityParams);
            }

        }

        $result->setSuccessWithResult(true);
        $this->render($result);
    }

    public function getSelfCheckResult()
    {
        $result = new Result();
        $category_id = $this->request('id');
        $checkListRet = JkSelfCheckServiceImpl::getInstance()->getJkSelfCheckResultList($category_id);
        $checkList = $checkListRet->data;
        $listResp = new DataListResp();
        /** @var JkSelfCheckResultDTO $checkDTO */
        foreach ($checkList as $checkDTO) {
            $checkDTO->product_list = [];
            $queryTagEntityListParams = new QueryTagEntityListParams();
            $queryTagEntityListParams->tag_biz_type = Config_Const::BIZ_TYPE_SELF_CHECK_RESULT;
            $queryTagEntityListParams->tag_biz_id = $checkDTO->id;
            $queryTagEntityListParams->entity_biz_type = Config_Const::BIZ_TYPE_PRODUCT;
            $productListRet = TagServiceImpl::getInstance()->queryTagEntityList($queryTagEntityListParams);
            $productIdList = $productListRet->data;

            foreach ($productIdList as $productId) {
                $productDetailRet = ProductServiceImpl::getInstance()->getProductItem($productId);
                if (!Result::isSuccess($productDetailRet)) {
                    continue;
                }

                $productItem = $productDetailRet->data;
                $productResp = JkProductConversionUtil::getInstance()->buildProductResp($productItem);
                array_push($checkDTO->product_list, $productResp);
            }
            array_push($listResp->list, $checkDTO);
        }
        $result->setSuccessWithResult($listResp);
        $this->render($result);
    }

}