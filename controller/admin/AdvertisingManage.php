<?php


use facade\request\admin\banner\CreateBannerParams;
use facade\request\admin\banner\UpdateBannerParams;
use facade\request\PageParams;
use facade\response\base\DataListResp;
use facade\response\base\PageInfoResp;
use facade\response\operation\AdvertisingResp;
use facade\response\Result;
use model\operation\dto\OperationAdvertisingDTO;
use service\aliyun\AliyunBase;
use service\operation\impl\AdvertisingServiceImpl;

class AdvertisingManage extends FUR_Controller
{


    public function getAdvertisingList()
    {
        $result = new Result();
        /** @var PageParams $params */
        $params = $this->requestObject(PageParams::class);
        $adService = AdvertisingServiceImpl::getInstance();
        $adListRet = $adService->getAdvertisingList($params);
        $listResp = new DataListResp();
        foreach ($adListRet->data as $advertisingDTO) {
            $advertisingResp = new AdvertisingResp();
            $advertisingResp->id = $advertisingDTO->id;
            $advertisingResp->link = $advertisingDTO->link;
            $advertisingResp->link_type = $advertisingDTO->link_type;
            $advertisingResp->type = $advertisingDTO->type;
            $advertisingResp->sort = $advertisingDTO->sort;
            $advertisingResp->title = $advertisingDTO->title;
            $advertisingResp->space_id = $advertisingDTO->space_id;
            if ($advertisingDTO->type == Config_Const::AD_TYPE_IMAGE) {
                $jsonDecodeData = json_decode($advertisingDTO->data);
                $advertisingResp->image = $jsonDecodeData->image;
            }
            array_push($listResp->list, $advertisingResp);
        }

        $listResp->page_info = PageInfoResp::buildPageInfoRespBaseLine($params->index, $params->page_size, count($listResp->list));
        $result->setSuccessWithResult($listResp);
        $this->render($result);
    }


    /**
     * 获取banner详情
     */
    public function getAdvertisingDetail()
    {

        $result = new Result();
        $id = $this->request('id');
        /**
         * 校验参数
         */
        if ($id == null) {
            $result->setError(Config_Error::ERR_MEDIA_QUERY_IMAGE_FILE_NOT_FOUND);
            $this->render($result);
        }

        $detailRet = AdvertisingServiceImpl::getInstance()->getAdvertisingDetail($id);
        /** @var OperationAdvertisingDTO $advertisingDTO */
        $advertisingDTO = $detailRet->data;
        $advertisingResp = new AdvertisingResp();
        $advertisingResp->id = $advertisingDTO->id;
        $advertisingResp->link = $advertisingDTO->link;
        $advertisingResp->link_type = $advertisingDTO->link_type;
        $advertisingResp->type = $advertisingDTO->type;
        $advertisingResp->sort = $advertisingDTO->sort;
        $advertisingResp->title = $advertisingDTO->title;
        $advertisingResp->space_id = $advertisingDTO->space_id;
        if ($advertisingDTO->type == Config_Const::AD_TYPE_IMAGE) {
            $jsonDecodeData = json_decode($advertisingDTO->data);
            $advertisingResp->image = $jsonDecodeData->image;
        }
        $result->setSuccessWithResult($advertisingResp);
        $this->render($result);
    }


    /**
     * 创建新的banner
     */
    public function createAdvertising()
    {

        $result = new Result();
        /** @var CreateBannerParams $createBannerParams */
        $createBannerParams = $this->requestObject(CreateBannerParams::class);


        $data['image'] = $createBannerParams->image;
        $advertisingDTO = new  OperationAdvertisingDTO();
        $advertisingDTO->type = 'image';
        $advertisingDTO->status = 1;
        $advertisingDTO->title = $createBannerParams->title;
        $advertisingDTO->created_timestamp = time();
        $advertisingDTO->sort = $createBannerParams->sort;
        $advertisingDTO->link = $createBannerParams->link;
        $advertisingDTO->link_type = $createBannerParams->link_type;
        $advertisingDTO->space_id = $createBannerParams->space_id;
        $advertisingDTO->data = json_encode($data);
        $insertRet = AdvertisingServiceImpl::getInstance()->createAdvertising($advertisingDTO);
        $result->setSuccessWithResult($insertRet->data);
        $this->render($result);

    }

    /**
     * 删除banner
     */
    public function deleteAdvertising()
    {
        $result = new Result();
        $bannerId = $this->request('id');

        AdvertisingServiceImpl::getInstance()->deleteAdvertising($bannerId);

        $result->setSuccessWithResult(true);
        $this->render($result);
    }


    /**
     * 编辑banner
     */
    public function updateAdvertising()
    {
        $result = new Result();
        /** @var UpdateBannerParams $updateBannerParams */
        $updateBannerParams = $this->requestObject(UpdateBannerParams::class);

        $advertisingDTO = new  OperationAdvertisingDTO();
        if ($updateBannerParams->image) {

            $data['image'] = ($updateBannerParams->image);
            $advertisingDTO->data = json_encode($data);
        }
        $advertisingDTO->updated_timestamp = time();
        $advertisingDTO->sort = $updateBannerParams->sort;

        if ($updateBannerParams->link == null) {
            $updateBannerParams->link = "";
        }
        if ($updateBannerParams->link_type == null) {
            $updateBannerParams->link_type = "";
        }
        $advertisingDTO->link = $updateBannerParams->link;
        $advertisingDTO->link_type = $updateBannerParams->link_type;
        $advertisingDTO->title = $updateBannerParams->title;
        $advertisingDTO->id = $updateBannerParams->id;
        AdvertisingServiceImpl::getInstance()->updateAdvertising($advertisingDTO);

        $result->setSuccessWithResult(true);
        $this->render($result);


    }

}