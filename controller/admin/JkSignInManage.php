<?php

use facade\request\PageParams;
use facade\request\sign\EditSignTaskParams;
use facade\request\sign\EditSignTipsParams;
use facade\request\sign\EditSignTypeParams;
use facade\response\base\DataListResp;
use facade\response\base\PageInfoResp;
use facade\response\Result;
use model\sign\dto\SignInTaskDTO;
use model\sign\dto\SignInTipsDTO;
use service\sign\impl\SignServiceImpl;

class JkSignInManage extends FUR_Controller
{
    private $userId;

    const DEFAULT_PAGE_SIZE = 10;

    public function __construct()
    {
        $this->userId = $this->getRequestUserId(false);
    }


    public function createSignType()
    {
        $result = new Result();
        /** @var EditSignTypeParams $params */
        $params = $this->requestObject(EditSignTypeParams::class);
        if ($params->sign_type_name == null) {
            $result->setError(Config_Error::ERR_JK_SIGN_TYPE_NO_NAME);
            $this->render($result);
        }

        $result = SignServiceImpl::getInstance()->createSignType($params);
        $this->render($result);
    }

    public function getSignTypeList()
    {
        $result = new Result();
        /** @var PageParams $pageParams */
        $pageParams = $this->requestObject(PageParams::class);
        $signTypeListRet = SignServiceImpl::getInstance()->querySignTypeList($pageParams);
        $listResp = new DataListResp();
        $listResp->list = $signTypeListRet->data;
        $countRet = SignServiceImpl::getInstance()->querySignTypeCount();
        $listResp->page_info = PageInfoResp::buildPageInfoResp($pageParams->index, $pageParams->page_size, count($listResp->list), $countRet->data);
        $result->setSuccessWithResult($listResp);
        $this->render($result);
    }


    public function getSignTypeDetail()
    {
        $id = $this->request('id');
        $result = SignServiceImpl::getInstance()->getSignTypeDetail($id);
        $this->render($result);
    }


    public function updateSignType()
    {
        $result = new Result();
        /** @var EditSignTypeParams $params */
        $params = $this->requestObject(EditSignTypeParams::class);
        if ($params->sign_type_name == null) {
            $result->setError(Config_Error::ERR_JK_SIGN_TYPE_NO_NAME);
            $this->render($result);
        }

        $result = SignServiceImpl::getInstance()->updateSignType($params);
        $this->render($result);
    }

    public function deleteSignType()
    {

        $signTypeId = $this->request('id');
        $result = SignServiceImpl::getInstance()->deleteSignType($signTypeId);
        $this->render($result);
    }


    public function createSignTask()
    {
        $result = new Result();
        /** @var EditSignTaskParams $params */
        $params = $this->requestObject(EditSignTaskParams::class);
        if ($params->sign_in_task_name == null) {
            $result->setError(Config_Error::ERR_JK_SIGN_TASK_NO_NAME);
            $this->render($result);
        }
        if ($params->task_value < 0) {
            $result->setError(Config_Error::ERR_JK_SIGN_TASK_VALUE_FAIL);
            $this->render($result);
        }
        if ($params->sign_in_task_detail == null) {
            $result->setError(Config_Error::ERR_JK_SIGN_TASK_NO_DETAIL);
            $this->render($result);
        }
        if ($params->sign_in_type_id == null) {
            $result->setError(Config_Error::ERR_JK_SIGN_TASK_TYPE_NOT_CHOOSE);
            $this->render($result);
        }
        $result = SignServiceImpl::getInstance()->createSignTask($params);
        $this->render($result);
    }


    public function querySignTask()
    {
        $signTaskId = $this->request('sign_task_id');
        $result = SignServiceImpl::getInstance()->getSignTaskDetail($signTaskId);
        $this->render($result);
    }

    public function updateSignTask()
    {
        $result = new Result();
        /** @var EditSignTaskParams $params */
        $params = $this->requestObject(EditSignTaskParams::class);
        if ($params->sign_in_task_name == null) {
            $result->setError(Config_Error::ERR_JK_SIGN_TASK_NO_NAME);
            $this->render($result);
        }
        if ($params->task_value < 0) {
            $result->setError(Config_Error::ERR_JK_SIGN_TASK_VALUE_FAIL);
            $this->render($result);
        }

        $signTaskInfoRet = SignServiceImpl::getInstance()->getSignTaskDetail($params->sign_task_id);
        /** @var SignInTaskDTO $signTaskInfo */
        $signTaskInfo = $signTaskInfoRet->data;
        FUR_Core::copyProperties($params, $signTaskInfo);

        $result = SignServiceImpl::getInstance()->updateSignTask($signTaskInfo);
        $this->render($result);
    }

    public function deleteSignTask()
    {
        $signTaskId = $this->request('sign_task_id');
        $result = SignServiceImpl::getInstance()->deleteSignTask($signTaskId);
        $this->render($result);
    }

    public function querySignTaskList()
    {
        $result = new Result();
        /** @var PageParams $params */
        $params = $this->requestObject(PageParams::class);
        $signTypeId = $this->request('sign_type_id');
        $taskListRet = SignServiceImpl::getInstance()->querySignTaskList($signTypeId, $params);
        $listResp = new DataListResp();
        $listResp->list = $taskListRet->data;
        $listResp->page_info = PageInfoResp::buildPageInfoRespBaseLine($params->index, $params->page_size, count($listResp->list));
        $result->setSuccessWithResult($listResp);
        $this->render($result);
    }


    public function createSignTips()
    {
        $result = new Result();
        /** @var EditSignTipsParams $params */
        $params = $this->requestObject(EditSignTipsParams::class);
        if ($params->content == null || $params->image == null) {
            $result->setError(Config_Error::ERR_JK_SIGN_TASK_TIP_CONTENT_CANT_NULL);
            $this->render($result);
        }

        $result = SignServiceImpl::getInstance()->createSignTips($params);
        $this->render($result);
    }


    public function querySignTips()
    {
        $signTipsId = $this->request('sign_tips_id');
        $result = SignServiceImpl::getInstance()->getSignTipsDetail($signTipsId);
        $this->render($result);
    }

    public function updateSignTips()
    {
        $result = new Result();
        /** @var EditSignTipsParams $params */
        $params = $this->requestObject(EditSignTipsParams::class);
        $signTipsInfoRet = SignServiceImpl::getInstance()->getSignTipsDetail($params->sign_tips_id);
        /** @var SignInTipsDTO $signTipsInfo */
        $signTipsInfo = $signTipsInfoRet->data;
        FUR_Core::copyProperties($params, $signTipsInfo);

        $result = SignServiceImpl::getInstance()->updateSignTips($signTipsInfo);
        $this->render($result);
    }

    public function deleteSignTips()
    {
        $signTipsId = $this->request('sign_tips_id');
        $result = SignServiceImpl::getInstance()->deleteSignTips($signTipsId);
        $this->render($result);
    }

    public function querySignTipsList()
    {
        $result = new Result();
        /** @var PageParams $params */
        $params = $this->requestObject(PageParams::class);
        $signTypeId = $this->request('sign_type_id');
        $tipsListRet = SignServiceImpl::getInstance()->getSignTipsList($signTypeId, $params);
        $listResp = new DataListResp();
        $listResp->list = $tipsListRet->data;
        $tipsCountRet = SignServiceImpl::getInstance()->getSignTipsCount($signTypeId);
        $count = $tipsCountRet->data;
        $listResp->page_info = PageInfoResp::buildPageInfoResp($params->index, $params->page_size, count($listResp->list), $count);
        $result->setSuccessWithResult($listResp);
        $this->render($result);
    }

}