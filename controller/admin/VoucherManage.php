<?php


use business\order\OrderInfoConversionUtil;
use business\promotion\VoucherConversionUtil;
use facade\request\PageParams;
use facade\request\promotion\EditVoucherParams;
use facade\response\base\DataListResp;
use facade\response\base\PageInfoResp;
use facade\response\Result;
use model\promotion\dto\PromotionVoucherDTO;
use service\admin\impl\OrderAdminServiceImpl;
use service\order\impl\OrderServiceImpl;
use service\promotion\impl\PromotionVoucherServiceImpl;

class VoucherManage extends FUR_Controller
{


    public function queryVoucherList()
    {
        $result = new Result();

        $listResp = new DataListResp();
        /** @var PageParams $params */
        $params = $this->requestObject(PageParams::class);
        $voucherListRet = PromotionVoucherServiceImpl::getInstance()->queryVoucherList($params);
        if (!Result::isSuccess($voucherListRet)) {
            $result->setSuccessWithResult($listResp);
            $this->render($result);
        }
        /** @var PromotionVoucherDTO $voucherDTO */
        foreach ($voucherListRet->data as $voucherDTO) {
            $voucherResp = VoucherConversionUtil::conversionManageVoucher($voucherDTO);
            array_push($listResp->list, $voucherResp);
        }
        $totalCount = PromotionVoucherServiceImpl::getInstance()->queryVoucherCount($params)->data;
        $pageInfo = PageInfoResp::buildPageInfoResp($params->index, $pageSize = 10, count($voucherListRet->data), $totalCount);
        $listResp->page_info = $pageInfo;
        $result->setSuccessWithResult($listResp);
        $this->render($result);

    }


    public function queryVoucherDetail()
    {
        $result = new Result();
        $voucherId = $this->request('id');
        $voucherInfoRet = PromotionVoucherServiceImpl::getInstance()->getVoucherInfo($voucherId);
        if ($voucherInfoRet->data == null && $voucherId < 0) {
            PromotionVoucherServiceImpl::getInstance()->createsShareVoucher($voucherId);
            $voucherInfoRet = PromotionVoucherServiceImpl::getInstance()->getVoucherInfo($voucherId);
        }

        $orderDetailResp = VoucherConversionUtil::conversionManageVoucher($voucherInfoRet->data);
        $result->setSuccessWithResult($orderDetailResp);
        $this->render($result);


    }

    public function createVoucher()
    {
        /** @var EditVoucherParams $params */
        $params = $this->requestObject(EditVoucherParams::class);

        $result = PromotionVoucherServiceImpl::getInstance()->createVoucher($params);
        $this->render($result);
    }

    public function updateVoucher()
    {
        $result = new Result();
        /** @var EditVoucherParams $params */
        $params = $this->requestObject(EditVoucherParams::class);
        $result = PromotionVoucherServiceImpl::getInstance()->updateVoucher($params);
        $this->render($result);
    }

    public function deleteVoucher()
    {
        $voucherId = $this->request('id');
        $result = PromotionVoucherServiceImpl::getInstance()->deleteVoucher($voucherId);
        $this->render($result);
    }

    public function updateVoucherStatus()
    {
        $voucherId = $this->request('id');
        $status = $this->request('voucher_status');
        $result = PromotionVoucherServiceImpl::getInstance()->updateVoucherStatus($voucherId, $status);
        $this->render($result);
    }

}