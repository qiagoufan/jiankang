<?php

use business\invoice\InvoiceConversionUtil;
use facade\request\invoice\QueryInvoiceListParams;
use facade\response\base\DataListResp;
use facade\response\base\PageInfoResp;
use facade\response\Result;
use model\invoice\dto\InvoiceInfoDTO;
use service\invoice\impl\InvoiceServiceImpl;

class InvoiceManage extends FUR_Controller
{


    public function __construct()
    {

        $this->session_start();
        $admin_account_id = $_SESSION['admin_account_id'];
        if ($admin_account_id == null) {
            $result = new Result();
            $result->setError(Config_Error::ERR_ADMIN_ACCOUNT_NEED_LOGIN);
//            $this->render($result);
        }
    }


    /**
     * 完成开票
     */

    public function finishInvoicing()
    {
        $result = new Result();

        $invoiceId = $this->request('invoice_id');

        $finishRet = InvoiceServiceImpl::getInstance()->finishInvoice($invoiceId);
        if (!Result::isSuccess($finishRet)) {
            $this->render($finishRet);
        }
        $result->setSuccessWithResult(true);
        $this->render($result);
    }


    public function queryInvoiceList()
    {
        $result = new Result();


        /** @var  QueryInvoiceListParams $params */
        $params = $this->requestObject(QueryInvoiceListParams::class);
        $invoiceListRet = InvoiceServiceImpl::getInstance()->queryInvoiceList($params);
        if (!Result::isSuccess($invoiceListRet)) {
            $this->render($invoiceListRet);
        }
        $invoiceCountRet = InvoiceServiceImpl::getInstance()->queryInvoiceCount($params);
        if (!Result::isSuccess($invoiceCountRet)) {
            $this->render($invoiceCountRet);
        }

        $listResp = new DataListResp();

        /** @var InvoiceInfoDTO $invoiceInfo */
        foreach ($invoiceListRet->data as $invoiceInfo) {
            $invoiceResp = InvoiceConversionUtil::getInstance()->buildInvoiceResp($invoiceInfo);
            if ($invoiceResp == null) {
                continue;
            }
            array_push($listResp->list, $invoiceResp);
        }


        $listResp->page_info = PageInfoResp::buildPageInfoResp($params->index, $params->page_size, count($invoiceListRet->data), $invoiceCountRet->data);
        $result->setSuccessWithResult($listResp);
        $this->render($result);

    }

}