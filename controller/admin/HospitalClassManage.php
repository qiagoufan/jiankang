<?php


use facade\response\base\DataListResp;
use facade\response\base\PageInfoResp;
use facade\response\Result;
use model\jk\dto\JkHospitalClassDTO;
use service\jk\impl\JkDiseaseServiceImpl;
use service\jk\impl\JkHospitalClassServiceImpl;

class HospitalClassManage extends FUR_Controller
{


    public function getHospitalClassList()
    {
        $result = new Result();

        $listRet = JkHospitalClassServiceImpl::getInstance()->getHospitalClassList();
        $listResp = new DataListResp();
        foreach ($listRet->data as $item) {
            array_push($listResp->list, $item);
        }
        $listResp->page_info = PageInfoResp::buildPageInfoRespBaseLine(0, 1000, count($listResp->list));
        $result->setSuccessWithResult($listResp);
        $this->render($result);
    }


    public function getHospitalClassDetail()
    {

        $id = $this->request('id');
        $detailRet = JkHospitalClassServiceImpl::getInstance()->getHospitalClass($id);
        $this->render($detailRet);
    }


    public function createHospitalClass()
    {

        $knowledgeCategory = new JkHospitalClassDTO();
        $knowledgeCategory->class_name = $this->request('class_name');
        $result = JkHospitalClassServiceImpl::getInstance()->insertHospitalClass($knowledgeCategory);
        $this->render($result);

    }


    public function deleteHospitalClass()
    {
        $result = new Result();
        $id = $this->request('id');

        $diseaseCountRet = JkDiseaseServiceImpl::getInstance()->getDiseaseCountByClassId($id);
        if (Result::isSuccess($diseaseCountRet) && $diseaseCountRet->data != 0) {
            $result->setError(Config_Error::ERR_CLASS_HAVE_DISEASE);
            $this->render($result);
        }
        JkHospitalClassServiceImpl::getInstance()->deleteHospitalClass($id);

        $result->setSuccessWithResult(true);
        $this->render($result);
    }


    public function updateHospitalClass()
    {
        $result = new Result();
        $knowledgeCategory = new JkHospitalClassDTO();
        $knowledgeCategory->class_name = $this->request('class_name');
        $knowledgeCategory->id = $this->request('id');
        JkHospitalClassServiceImpl::getInstance()->updateHospitalClass($knowledgeCategory);
        $result->setSuccessWithResult(true);
        $this->render($result);
    }


}