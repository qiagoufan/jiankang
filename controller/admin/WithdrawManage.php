<?php


use business\withdraw\WithdrawInfoConversionUtil;
use facade\request\withdraw\QueryWithdrawListParams;
use facade\response\base\DataListResp;
use facade\response\base\PageInfoResp;
use facade\response\Result;
use model\repair\dto\RepairOrderInfoDTO;
use model\withdraw\dto\WithdrawRecordDTO;
use service\repair\impl\RepairOrderServiceImpl;
use service\user\impl\UserEngineerServiceImpl;
use service\withdraw\impl\WithdrawServiceImpl;

class WithdrawManage extends FUR_Controller
{
    public function __construct()
    {
        $this->session_start();
        $admin_account_id = $_SESSION['admin_account_id'];
        if ($admin_account_id == null) {
            $result = new Result();
            $result->setError(Config_Error::ERR_ADMIN_ACCOUNT_NEED_LOGIN);
//            $this->render($result);
        }
    }

    public function queryWithdrawList()
    {
        $result = new Result();
        /** @var QueryWithdrawListParams $queryWithdrawListParams */
        $queryWithdrawListParams = $this->requestObject(QueryWithdrawListParams::class);
        if ($queryWithdrawListParams->engineer_phone_num) {
            $queryWithdrawListParams->engineer_id = -1;
            $engineerInfoRet = UserEngineerServiceImpl::getInstance()->queryEngineerInfoByPhone($queryWithdrawListParams->engineer_phone_num);
            if (Result::isSuccess($engineerInfoRet)) {
                $queryWithdrawListParams->engineer_id = $engineerInfoRet->data->phone_num;
            }
        }
        $withdrawListRet = WithdrawServiceImpl::getInstance()->queryWithdrawRecordList($queryWithdrawListParams);
        $withdrawList = $withdrawListRet->data;

        $listResp = new DataListResp();
        /** @var WithdrawRecordDTO $withdrawInfo */
        foreach ($withdrawList as $withdrawInfo) {
            $withdrawResp = WithdrawInfoConversionUtil::getInstance()->conversionWithdrawInfo($withdrawInfo, true);
            array_push($listResp->list, $withdrawResp);
        }
        $withdrawListCountRet = WithdrawServiceImpl::getInstance()->queryWithdrawRecordCount($queryWithdrawListParams);
        $withdrawListCount = $withdrawListCountRet->data;
        $listResp->page_info = PageInfoResp::buildPageInfoResp($queryWithdrawListParams->index, $queryWithdrawListParams->page_size, count($withdrawList), $withdrawListCount);

        $result->setSuccessWithResult($listResp);
        $this->render($result);
    }


    public function completeWithdraw()
    {

        $result = new Result();
        $withdraw_id = $this->request('withdraw_id', 0);
        $withdrawInfoRet = WithdrawServiceImpl::getInstance()->queryRecordById($withdraw_id);
        if (!Result::isSuccess($withdrawInfoRet)) {
            $this->render($withdrawInfoRet);
        }
        /** @var WithdrawRecordDTO $withdrawInfo */
        $withdrawInfo = $withdrawInfoRet->data;
        if ($withdrawInfo == null || $withdrawInfo->cash_out_status != Config_Const::CASH_OUT_STATUS_WAIT_FOR_PAY) {
            $result->setError(Config_Error::ERR_REPAIR_ORDER_HAS_PAY);
            $this->render($result);
        }

        $repairOrderIdList = json_decode($withdrawInfo->repair_order_id_list);
        /** @var int $repairOrderId */
        foreach ($repairOrderIdList as $repairOrderId) {
            $repairOrderInfoRet = RepairOrderServiceImpl::getInstance()->getRepairOrder($repairOrderId);
            /** @var RepairOrderInfoDTO $repairOrderInfo */
            $repairOrderInfo = $repairOrderInfoRet->data;
            $repairOrderInfo->cash_out_status = Config_Const::CASH_OUT_STATUS_PAID;
            $repairOrderInfo->order_status = Config_Const::REPAIR_ORDER_COMPLETE;
            RepairOrderServiceImpl::getInstance()->updateRepairOrder($repairOrderInfo);
        }

        $withdrawInfo->cash_out_status = Config_Const::CASH_OUT_STATUS_PAID;
        $updateRet = WithdrawServiceImpl::getInstance()->updateWithDrawRecord($withdrawInfo);
        if (Result::isSuccess($updateRet) == false) {
            $this->render($updateRet);
        }

        $result->setSuccessWithResult(true);
        $this->render($result);

    }

}