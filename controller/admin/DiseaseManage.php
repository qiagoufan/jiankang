<?php


use business\product\JkProductConversionUtil;
use facade\request\jk\bodyCheck\manage\EditDiseaseParams;
use facade\request\jk\bodyCheck\manage\QueryDiseaseParams;
use facade\request\PageParams;
use facade\request\tag\CreateTagEntityParams;
use facade\request\tag\QueryTagEntityListParams;
use facade\request\tag\TagEntityParams;
use facade\response\base\DataListResp;
use facade\response\base\PageInfoResp;
use facade\response\Result;
use service\jk\impl\JkDiseaseServiceImpl;
use service\product\impl\ProductServiceImpl;
use service\tag\impl\TagServiceImpl;

class DiseaseManage extends FUR_Controller
{


    public function getDiseaseList()
    {
        $result = new Result();

        /** @var QueryDiseaseParams $params */
        $params = $this->requestObject(QueryDiseaseParams::class);
        $listRet = JkDiseaseServiceImpl::getInstance()->getDiseaseList($params);
        $listResp = new DataListResp();
        foreach ($listRet->data as $item) {
            array_push($listResp->list, $item);
        }
        $countRet = JkDiseaseServiceImpl::getInstance()->getDiseaseCount($params);
        $listResp->page_info = PageInfoResp::buildPageInfoResp($params->index, $params->page_size, count($listResp->list), $countRet->data);
        $result->setSuccessWithResult($listResp);
        $this->render($result);
    }


    public function getDiseaseDetail()
    {
        $id = $this->request('id');
        $detailRet = JkDiseaseServiceImpl::getInstance()->getDisease($id);
        $this->render($detailRet);
    }


    public function createDisease()
    {

        /** @var EditDiseaseParams $editDiseaseParams */
        $editDiseaseParams = $this->requestObject(EditDiseaseParams::class);
        $result = JkDiseaseServiceImpl::getInstance()->insertDisease($editDiseaseParams);
        $this->render($result);

    }


    public function deleteDisease()
    {
        $result = new Result();
        $id = $this->request('id');

        JkDiseaseServiceImpl::getInstance()->deleteDisease($id);

        $result->setSuccessWithResult(true);
        $this->render($result);
    }


    public function updateDisease()
    {
        /** @var EditDiseaseParams $editDiseaseParams */
        $editDiseaseParams = $this->requestObject(EditDiseaseParams::class);
        $result = JkDiseaseServiceImpl::getInstance()->updateDisease($editDiseaseParams);
        $this->render($result);
    }


    public function getDiseaseRelatedProductList()
    {
        $id = $this->request('id');

        $detailRet = JkDiseaseServiceImpl::getInstance()->getDisease($id);
        $result = new  Result();
        $resp = new stdClass();
        $resp->product_list = [];
        $resp->disease_detail = $detailRet->data;
        $queryTagEntityListParams = new QueryTagEntityListParams();
        $queryTagEntityListParams->tag_biz_type = Config_Const::BIZ_TYPE_DISEASE;
        $queryTagEntityListParams->tag_biz_id = $id;
        $queryTagEntityListParams->entity_biz_type = Config_Const::BIZ_TYPE_PRODUCT;
        $productListRet = TagServiceImpl::getInstance()->queryTagEntityList($queryTagEntityListParams);
        $productIdList = $productListRet->data;
        foreach ($productIdList as $productId) {
            $productDetailRet = ProductServiceImpl::getInstance()->getProductItem($productId);
            if (!Result::isSuccess($productDetailRet)) {
                continue;
            }

            $productItem = $productDetailRet->data;
            $productResp = JkProductConversionUtil::getInstance()->buildProductResp($productItem);
            array_push($resp->product_list, $productResp);
        }
        $result->setSuccessWithResult($resp);
        $this->render($result);

    }

    public function updateDiseaseRelatedProductList()
    {
        $result = new  Result();
        $result->setSuccessWithResult(true);
        $diseaseId = $this->request('disease_id');

        $productIdList = $this->request('product_id');

        $cleanParams = new  TagEntityParams();
        $cleanParams->tag_biz_id = $diseaseId;
        $cleanParams->tag_biz_type = Config_Const::BIZ_TYPE_DISEASE;
        $cleanParams->entity_biz_type = Config_Const::BIZ_TYPE_PRODUCT;
        TagServiceImpl::getInstance()->cleanTagEntity($cleanParams);
        if ($productIdList == null) {
            $this->render($result);
        }
        foreach ($productIdList as $itemId) {
            $createTagEntityParams = new CreateTagEntityParams();
            $createTagEntityParams->tag_biz_id = $diseaseId;
            $createTagEntityParams->tag_biz_type = Config_Const::BIZ_TYPE_DISEASE;
            $createTagEntityParams->entity_biz_id = $itemId;
            $createTagEntityParams->entity_biz_type = Config_Const::BIZ_TYPE_PRODUCT;
            TagServiceImpl::getInstance()->createTagEntity($createTagEntityParams);
        }

        $this->render($result);

    }

}