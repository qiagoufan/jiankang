<?php


use facade\request\user\VerificationCodeParams;
use facade\response\Result;
use model\admin\dto\AdminAccountDTO;
use service\admin\impl\AdminAccountServiceImpl;
use service\user\impl\VerificationCodeServiceImpl;

class AdminAccount extends FUR_Controller
{


    public function adminAccountLogin()
    {
        $result = new Result();
        $account = $this->request('account');
        $password = $this->request('password');

        $loginRet = AdminAccountServiceImpl::getInstance()->adminAccountLogin($account, $password);
        if (!Result::isSuccess($loginRet)) {
            $this->render($loginRet);
        }
        $adminAccountId = $loginRet->data;
        $adminAccountInfoRet = AdminAccountServiceImpl::getInstance()->queryAdminById($adminAccountId);
        /** @var AdminAccountDTO $adminAccountInfo */
        $adminAccountInfo = $adminAccountInfoRet->data;

        $this->session_start();
        $_SESSION['user_name'] = $adminAccountInfo->user_name;
        $_SESSION['admin_account_id'] = $adminAccountInfo->id;
        $result->setSuccessWithResult(true);
        $this->render($result);
    }


    public function getUserInfo()
    {
        $result = new Result();
        $this->session_start();
        $admin_account_id = $_SESSION['admin_account_id'];
        if ($admin_account_id == null) {
            $result->setError(Config_Error::ERR_ADMIN_ACCOUNT_NEED_LOGIN);
            $this->render($result);
        }
        $adminAccountInfoRet = AdminAccountServiceImpl::getInstance()->queryAdminById($admin_account_id);
        /** @var AdminAccountDTO $adminAccountInfo */
        $adminAccountInfo = $adminAccountInfoRet->data;
        $data = ['user_name' => $adminAccountInfo->user_name, 'account_name' => $adminAccountInfo->account_name, 'admin_account_id' => $adminAccountInfo->id];
        $result->setSuccessWithResult($data);
        $this->render($result);
    }

    public function logout()
    {
        $result = new Result();
        $this->session_start();
        unset($_SESSION);
        session_destroy();
        $result->setSuccessWithResult(true);
        $this->render($result);
    }

    public function sendLoginCode()
    {
        /** @var VerificationCodeParams $verificationCodeParams */
        $verificationCodeParams = $this->requestObject(VerificationCodeParams::class);
        $verificationCodeParams->action = Config_Const::ACTION_ADMIN_LOGIN;
        $verificationCodeService = VerificationCodeServiceImpl::getInstance();
        $result = $verificationCodeService->sendCode($verificationCodeParams);
        $this->render($result);
    }


    public function verifyLoginCode()
    {
        $result = new Result();
        /** @var VerificationCodeParams $verificationCodeParams */
        $verificationCodeParams = $this->requestObject(VerificationCodeParams::class);
        $verificationCodeParams->action = Config_Const::ACTION_ADMIN_LOGIN;
        $verificationCodeService = VerificationCodeServiceImpl::getInstance();
        $verificationCodeRet = $verificationCodeService->verifyCode($verificationCodeParams);
        if (!Result::isSuccess($verificationCodeRet)) {
            $this->render($verificationCodeRet);
        }
        $phone = $verificationCodeParams->phone;
        $adminAccountInfoRet = AdminAccountServiceImpl::getInstance()->queryAdminByPhone($phone);
        if (!Result::isSuccess($adminAccountInfoRet)) {
            $result->setError(Config_Error::ERR_ADMIN_USER_IS_NOT_EXIST);
            $this->render($result);
        }

        /** @var AdminAccountDTO $adminAccountInfo */
        $adminAccountInfo = $adminAccountInfoRet->data;
        if ($adminAccountInfo->status != Config_Const::USER_STATUS_VALID) {
            $result->setError(Config_Error::ERR_ADMIN_USER_IS_INVALID);
            $this->render($result);
        }

        $this->session_start();
        $_SESSION['user_name'] = $adminAccountInfo->user_name;
        $_SESSION['admin_account_id'] = $adminAccountInfo->id;
        $result->setSuccessWithResult(true);
        $this->render($result);

    }

}