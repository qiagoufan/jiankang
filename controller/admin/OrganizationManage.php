<?php

use facade\request\organization\EditOrganizationCompanyParams;
use facade\request\organization\EditOrganizationStoreParams;
use facade\request\organization\QueryOrganizationCompanyParams;
use facade\request\organization\QueryOrganizationStoreParams;
use facade\response\base\PageInfoResp;
use facade\response\organization\OrganizationCompanyDetailResp;
use facade\response\organization\OrganizationCompanyListResp;
use facade\response\organization\OrganizationStoreDetailResp;
use facade\response\organization\OrganizationStoreListResp;
use facade\response\Result;
use model\organization\dto\OrganizationCompanyDTO;
use model\organization\dto\OrganizationStoreDTO;
use service\organization\impl\OrganizationServiceImpl;

class OrganizationManage extends FUR_Controller
{

    const PAGE_SIZE = 20;

    public function __construct()
    {

    }


    public function getCompanyList()
    {
        $result = new Result();
        $index = intval($this->request('index', 0));
        $params = $this->requestObject(QueryOrganizationCompanyParams::class);
        $companyListRet = OrganizationServiceImpl::getInstance()->getCompanyList($params);
        $companyCountRet = OrganizationServiceImpl::getInstance()->getCompanyCount($params);

        $companyListResp = new OrganizationCompanyListResp();
        $companyListResp->list = [];
        /** @var OrganizationCompanyDTO $companyDTO */
        foreach ($companyListRet->data as $companyDTO) {
            $companyDetailResp = new OrganizationCompanyDetailResp();
            FUR_Core::copyProperties($companyDTO, $companyDetailResp);
            array_push($companyListResp->list, $companyDetailResp);
        }

        $pageInfoResp = PageInfoResp::buildPageInfoResp($index, self::PAGE_SIZE, count($companyListResp->list), $companyCountRet->data);
        $companyListResp->page_info = $pageInfoResp;
        $result->setSuccessWithResult($companyListResp);
        $this->render($result);

    }

    public function createCompany()
    {
        /** @var EditOrganizationCompanyParams $editParams */
        $editParams = $this->requestObject(EditOrganizationCompanyParams::class);
        $editParams->id == null;
        $checkParamsRet = $this->checkEditCompanyParams($editParams);
        if (!Result::isSuccess($checkParamsRet)) {
            $this->render($checkParamsRet);
        }
        $createRet = OrganizationServiceImpl::getInstance()->createCompany($editParams);
        $this->render($createRet);

    }

    public function updateCompany()
    {
        $result = new Result();
        /** @var EditOrganizationCompanyParams $editParams */
        $editParams = $this->requestObject(EditOrganizationCompanyParams::class);
        if ($editParams->id == null) {
            $result->setError(Config_Error::ERR_COMPANY_ID_IS_NULL);
            $this->render($result);
        }
        $checkParamsRet = $this->checkEditCompanyParams($editParams);
        if (!Result::isSuccess($checkParamsRet)) {
            $this->render($checkParamsRet);
        }
        $createRet = OrganizationServiceImpl::getInstance()->updateCompanyInfo($editParams);
        $this->render($createRet);
    }

    public function getCompanyDetail()
    {
        $result = new Result();
        $companyId = $this->request('company_id');
        $detailRet = OrganizationServiceImpl::getInstance()->getCompanyInfo($companyId);
        if (!Result::isSuccess($detailRet)) {
            $this->render($detailRet);
        }
        $companyDetailResp = new OrganizationCompanyDetailResp();
        FUR_Core::copyProperties($detailRet->data, $companyDetailResp);
        $result->setSuccessWithResult($companyDetailResp);
        $this->render($result);
    }

    public function deleteCompany()
    {
        $companyId = $this->request('company_id');
        $deleteRet = OrganizationServiceImpl::getInstance()->deleteCompany($companyId);
        $this->render($deleteRet);
    }

    public function reactivateCompany()
    {
        $companyId = $this->request('company_id');
        $ret = OrganizationServiceImpl::getInstance()->reactivateCompany($companyId);
        $this->render($ret);
    }

    private function checkEditCompanyParams(EditOrganizationCompanyParams $params): Result
    {
        $result = new Result();
        if ($params->id != null) {
            $result->setSuccessWithResult(true);
            return $result;
        }
        if ($params->company_name == null) {
            $result->setError(Config_Error::ERR_COMPANY_NAME_IS_NULL);
            return $result;
        }
        if ($params->company_license == null) {
            $result->setError(Config_Error::ERR_COMPANY_LICENSE_IS_NULL);
            return $result;
        }
        if ($params->company_license_pic == null) {
            $result->setError(Config_Error::ERR_COMPANY_LICENSE_IMAGE_IS_NULL);
            return $result;
        }
        if ($params->id == null) {
            if ($params->account_name == null || $params->account_password == null) {
                $result->setError(Config_Error::ERR_COMPANY_ACCOUNT_INFO_IS_NULL);
                return $result;
            }

        }

        $result->setSuccessWithResult(true);
        return $result;
    }


}