<?php

use business\product\JkProductConversionUtil;
use facade\request\PageParams;
use facade\request\tag\CreateTagEntityParams;
use facade\request\tag\QueryTagEntityListParams;
use facade\request\tag\TagEntityParams;
use facade\response\base\DataListResp;
use facade\response\base\PageInfoResp;
use facade\response\jiankang\riskCheck\RiskCheckQuestionResp;
use facade\response\Result;
use model\jk\dto\JkRiskCheckCategoryDTO;
use model\jk\dto\JkRiskCheckOptionDTO;
use model\jk\dto\JkRiskCheckQuestionDTO;
use model\jk\dto\JkRiskCheckResultDTO;
use service\jk\impl\JkRiskCheckServiceImpl;
use service\product\impl\ProductServiceImpl;
use service\tag\impl\TagServiceImpl;

class RiskCheckManage extends FUR_Controller
{


    public function getRiskCheckCategoryList()
    {
        $result = new Result();

        /** @var PageParams $params */
        $params = $this->requestObject(PageParams::class);

        $listRet = JkRiskCheckServiceImpl::getInstance()->getJkRiskCheckCategoryList();
        $listResp = new DataListResp();
        /** @var JkRiskCheckCategoryDTO $category */
        foreach ($listRet->data as $category) {
            if ($category->parent_id == 0) {
                $category->category_list = [];
                array_push($listResp->list, $category);
            }
        }
        foreach ($listRet->data as $category) {
            if ($category->parent_id != 0) {
                foreach ($listResp->list as $level1Category) {
                    if ($level1Category->id == $category->parent_id) {
                        array_push($level1Category->category_list, $category);
                        break;
                    }
                }
            }
        }
        $listResp->page_info = PageInfoResp::buildPageInfoResp($params->index, count($listResp->list), count($listResp->list), count($listResp->list));
        $result->setSuccessWithResult($listResp);
        $this->render($result);
    }

    public function getRiskCheckCategoryList1()
    {
        $result = new Result();

        /** @var PageParams $params */
        $params = $this->requestObject(PageParams::class);
        $listRet = JkRiskCheckServiceImpl::getInstance()->getJkRiskCheckCategoryList1();

        $listResp = new DataListResp();
        foreach ($listRet->data as $item) {
            array_push($listResp->list, $item);
        }
        $listResp->page_info = PageInfoResp::buildPageInfoResp($params->index, $params->page_size, count($listResp->list), count($listResp->list));
        $result->setSuccessWithResult($listResp);
        $this->render($result);
    }


    public function getRiskCheckCategoryDetail()
    {

        $id = $this->request('id');
        $detailRet = JkRiskCheckServiceImpl::getInstance()->getJkRiskCheckCategory($id);
        $this->render($detailRet);
    }


    public function createRiskCheckCategory()
    {

        $riskCheckCategory = new JkRiskCheckCategoryDTO();
        $riskCheckCategory->category_name = $this->request('category_name');
        $riskCheckCategory->parent_id = $this->request('parent_id');
        $riskCheckCategory->category_cover = $this->request('category_cover');
        $riskCheckCategory->seq = intval($this->request('seq'));
        $riskCheckCategory->calculation_type = intval($this->request('calculation_type'));
        $result = JkRiskCheckServiceImpl::getInstance()->insertJkRiskCheckCategory($riskCheckCategory);
        $this->render($result);

    }


    public function deleteRiskCheckCategory()
    {
        $result = new Result();
        $id = $this->request('id');
        $detailRet = JkRiskCheckServiceImpl::getInstance()->getJkRiskCheckCategory($id);

        /** @var JkRiskCheckCategoryDTO $categoryDetail */
        $categoryDetail = $detailRet->data;
        if ($categoryDetail->parent_id == 0) {
            $totalCountRet = JkRiskCheckServiceImpl::getInstance()->queryJkRiskCheckCategoryCountByParentId($id);
            if (Result::isSuccess($totalCountRet) && $totalCountRet->data != 0) {
                $result->setError(Config_Error::ERR_RISK_CATEGORY_HAVE_CHILDREN);
                $this->render($result);
            }
        }

        JkRiskCheckServiceImpl::getInstance()->deleteJkRiskCheckCategory($id);

        $result->setSuccessWithResult(true);
        $this->render($result);
    }


    public function updateRiskCheckCategory()
    {
        $result = new Result();

        $riskCheckCategory = new JkRiskCheckCategoryDTO();
        $riskCheckCategory->category_name = $this->request('category_name');
        $riskCheckCategory->parent_id = $this->request('parent_id');
        $riskCheckCategory->calculation_type = intval($this->request('calculation_type'));
        $category_cover = $this->request('category_cover');
        if ($category_cover) {
            $riskCheckCategory->category_cover = $category_cover;
        }
        $riskCheckCategory->id = $this->request('id');
        $riskCheckCategory->seq = intval($this->request('seq'));
        JkRiskCheckServiceImpl::getInstance()->updateJkRiskCheckCategory($riskCheckCategory);
        $result->setSuccessWithResult(true);
        $this->render($result);
    }


    public function getRiskCheckDetail()
    {
        $result = new Result();
        /** @var PageParams $params */
        $id = $this->request('id');
        $detailRet = JkRiskCheckServiceImpl::getInstance()->getJkRiskCheckCategory($id);
        /** @var JkRiskCheckCategoryDTO $category */
        $category = $detailRet->data;
        $questionListRet = JkRiskCheckServiceImpl::getInstance()->getJkRiskCheckQuestionList($category->id);
        $questionList = $questionListRet->data;
        $category->question_list = [];
        /** @var JkRiskCheckQuestionDTO $question */
        foreach ($questionList as $question) {

            $questionResp = new  RiskCheckQuestionResp();
            $questionResp->id = $question->id;
            $questionResp->category_id = $question->category_id;
            $questionResp->question_title = $question->question_title;
            $questionResp->question_sort_num = $question->question_sort_num;
            $optionListRet = JkRiskCheckServiceImpl::getInstance()->queryOptionList($question->id);
            $questionResp->question_option_list = $optionListRet->data;
            array_push($category->question_list, $questionResp);
        }


        $result->setSuccessWithResult($category);
        $this->render($result);
    }

    public
    function getRiskCheckList()
    {
        $result = new Result();
        /** @var PageParams $params */
        $params = $this->requestObject(PageParams::class);


        $listRet = JkRiskCheckServiceImpl::getInstance()->queryJkRiskCheckLevel2CategoryList($params);
        $countRet = JkRiskCheckServiceImpl::getInstance()->queryJkRiskCheckLevel2CategoryCount();
        $listResp = new DataListResp();
        /** @var JkRiskCheckCategoryDTO $category */
        foreach ($listRet->data as $category) {
            if ($category->parent_id == 0) {
                continue;
            }

            $questionListRet = JkRiskCheckServiceImpl::getInstance()->getJkRiskCheckQuestionList($category->id);
            $questionList = $questionListRet->data;
            $category->question_list = [];
            $category->title_list = [];
            /** @var JkRiskCheckQuestionDTO $question */
            foreach ($questionList as $question) {
                array_push($category->title_list, $question->question_title);
                $questionResp = new  RiskCheckQuestionResp();
                $questionResp->id = $question->id;
                $questionResp->category_id = $question->category_id;
                $questionResp->question_title = $question->question_title;
                $questionResp->question_sort_num = $question->question_sort_num;
                $questionResp->question_option_list = json_decode($question->question_option);
                array_push($category->question_list, $questionResp);
            }
            array_push($listResp->list, $category);

        }

        $listResp->page_info = PageInfoResp::buildPageInfoResp($params->index, $params->page_size, count($listResp->list), $countRet->data);
        $result->setSuccessWithResult($listResp);
        $this->render($result);
    }


    public function createRiskCheckQuestion()
    {
        $result = new Result();
        $category_id = $this->request('category_id');
        $question_list = json_decode($this->request('question_list', [], false));
        FUR_Log::info('question_list', json_encode($question_list));
        foreach ($question_list as $questionInfo) {
            $questionDTO = new JkRiskCheckQuestionDTO();
            $questionDTO->category_id = $category_id;
            $decodeQuestionInfo = ($questionInfo);
            $questionDTO->question_sort_num = $decodeQuestionInfo->sort_num;
            $questionDTO->question_title = $decodeQuestionInfo->question_title;
            $questionDTO->question_option = json_encode($decodeQuestionInfo->question_option);
            $idRet = JkRiskCheckServiceImpl::getInstance()->insertJkRiskCheckQuestionDTO($questionDTO);

            $questionId = $idRet->data;
            foreach ($decodeQuestionInfo->question_option as $questionOption) {
                $rickOptionDTO = new  JkRiskCheckOptionDTO();
                $rickOptionDTO->question_id = $questionId;
                $rickOptionDTO->symptom = $questionOption->symptom;
                $rickOptionDTO->option_desc = $questionOption->option_desc;
                $rickOptionDTO->symptom = $questionOption->symptom;
                $rickOptionDTO->score = $questionOption->score;
                $rickOptionDTO->option_type = $questionOption->option_type;
                JkRiskCheckServiceImpl::getInstance()->insertJkRiskCheckOptionDTO($rickOptionDTO);
            }
        }
        $result->setSuccessWithResult(true);
        $this->render($result);

    }


    public
    function updateRiskCheckQuestion()
    {
        $result = new Result();
        $category_id = $this->request('category_id');
        $question_list = json_decode($this->request('question_list', [], false));
        FUR_Log::info('question_list', json_encode($question_list));
        JkRiskCheckServiceImpl::getInstance()->deleteJkRiskCheckQuestionById($category_id);
        foreach ($question_list as $questionInfo) {
            $questionDTO = new JkRiskCheckQuestionDTO();
            $questionDTO->category_id = $category_id;
            $decodeQuestionInfo = ($questionInfo);
            $questionDTO->question_sort_num = $decodeQuestionInfo->sort_num;
            $questionDTO->question_title = $decodeQuestionInfo->question_title;
            $questionDTO->question_option = json_encode($decodeQuestionInfo->question_option);
            $idRet = JkRiskCheckServiceImpl::getInstance()->insertJkRiskCheckQuestionDTO($questionDTO);

            $questionId = $idRet->data;
            foreach ($decodeQuestionInfo->question_option as $questionOption) {
                $rickOptionDTO = new  JkRiskCheckOptionDTO();
                $rickOptionDTO->question_id = $questionId;
                $rickOptionDTO->symptom = $questionOption->symptom;
                $rickOptionDTO->option_desc = $questionOption->option_desc;
                $rickOptionDTO->symptom = $questionOption->symptom;
                $rickOptionDTO->score = $questionOption->score;
                $rickOptionDTO->option_type = $questionOption->option_type;
                JkRiskCheckServiceImpl::getInstance()->insertJkRiskCheckOptionDTO($rickOptionDTO);
            }
        }
        $result->setSuccessWithResult(true);
        $this->render($result);
    }


    public
    function deleteRiskCheck()
    {
        $result = new Result();
        $category_id = $this->request('id');
        JkRiskCheckServiceImpl::getInstance()->deleteJkRiskCheckQuestionById($category_id);
        JkRiskCheckServiceImpl::getInstance()->deleteJkRiskCheckResultById($category_id);
        $result->setSuccessWithResult(true);
        $this->render($result);
    }

    public
    function updateRiskCheckResult()
    {
        $result = new Result();
        $category_id = $this->request('id');
        JkRiskCheckServiceImpl::getInstance()->deleteJkRiskCheckResultById($category_id);


        $result_list = json_decode($this->request('result_list', [], false));
        FUR_Log::info('result_list', json_encode($result_list));
        foreach ($result_list as $scoreInfo) {
            $resultDTO = new JkRiskCheckResultDTO();
            $resultDTO->category_id = $category_id;
            $resultDTO->start_score = $scoreInfo->start_score;
            $resultDTO->end_score = $scoreInfo->end_score;
            $resultDTO->suggest = $scoreInfo->suggest;
            $resultDTO->disease = $scoreInfo->disease;
            $insertRet = JkRiskCheckServiceImpl::getInstance()->insertJkRiskCheckResultDTO($resultDTO);
            $id = $insertRet->data;

            $cleanParams = new  TagEntityParams();
            $cleanParams->tag_biz_id = $id;
            $cleanParams->tag_biz_type = Config_Const::BIZ_TYPE_RISK_CHECK_RESULT;
            $cleanParams->entity_biz_type = Config_Const::BIZ_TYPE_PRODUCT;
            TagServiceImpl::getInstance()->cleanTagEntity($cleanParams);

            foreach ($scoreInfo->product_list as $itemId) {
                $createTagEntityParams = new CreateTagEntityParams();
                $createTagEntityParams->tag_biz_id = $id;
                $createTagEntityParams->tag_biz_type = Config_Const::BIZ_TYPE_RISK_CHECK_RESULT;
                $createTagEntityParams->entity_biz_id = $itemId;
                $createTagEntityParams->entity_biz_type = Config_Const::BIZ_TYPE_PRODUCT;
                TagServiceImpl::getInstance()->createTagEntity($createTagEntityParams);
            }

        }

        $result->setSuccessWithResult(true);
        $this->render($result);
    }

    public
    function getRiskCheckResult()
    {
        $result = new Result();
        $category_id = $this->request('id');
        $checkListRet = JkRiskCheckServiceImpl::getInstance()->getJkRiskCheckResultList($category_id);
        $checkList = $checkListRet->data;
        $listResp = new DataListResp();


        /** @var JkRiskCheckResultDTO $checkDTO */
        foreach ($checkList as $checkDTO) {
            $checkDTO->product_list = [];
            $queryTagEntityListParams = new QueryTagEntityListParams();
            $queryTagEntityListParams->tag_biz_type = Config_Const::BIZ_TYPE_RISK_CHECK_RESULT;
            $queryTagEntityListParams->tag_biz_id = $checkDTO->id;
            $queryTagEntityListParams->entity_biz_type = Config_Const::BIZ_TYPE_PRODUCT;
            $productListRet = TagServiceImpl::getInstance()->queryTagEntityList($queryTagEntityListParams);
            $productIdList = $productListRet->data;
            foreach ($productIdList as $productId) {
                $productDetailRet = ProductServiceImpl::getInstance()->getProductItem($productId);
                if (!Result::isSuccess($productDetailRet)) {
                    continue;
                }

                $productItem = $productDetailRet->data;
                $productResp = JkProductConversionUtil::getInstance()->buildProductResp($productItem);
                array_push($checkDTO->product_list, $productResp);
            }
            array_push($listResp->list, $checkDTO);
        }
        $result->setSuccessWithResult($listResp);
        $this->render($result);


    }
}