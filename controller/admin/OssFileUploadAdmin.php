<?php

use facade\request\aliyun\oss\UploadFileParams;
use facade\response\Result;
use service\aliyun\impl\AliyunOssServiceImpl;
use service\aliyun\AliyunBase;
use \service\admin\impl\UserAdminServiceImpl;

class OssFileUploadAdmin extends FUR_Controller
{
    private $userId;

    public function __construct()
    {
//        $result = new Result();
//        $userId = $this->getRequestUserId();
//        $this->userId = $userId;
//        $UserAdminServiceImpl = UserAdminServiceImpl::getInstance();
//        $adminRet = $UserAdminServiceImpl->getValidAdminInfo($this->userId);
//        if ($this->userId == 0 || $adminRet->data == false) {
//            $result->setError(Config_Error::ERR_ADMIN_USER_IS_NOT_LOGIN);
//            $this->render($result);
//        }
    }

    public function uploadOssFile()
    {
        $result = new Result();
        $imageUrlArray = [];
        $uploadOssFileParams = $this->requestObject(UploadFileParams::class);
        $ossService = AliyunOssServiceImpl::getInstance();

        if (isset($_FILES['image'])) {

            $fileExtension = strrchr($_FILES['image']['name'], '.');
            $uploadOssFileParams->file_path = $_FILES['image']['tmp_name'];
        }


        if (isset($_FILES['video'])) {

            $fileExtension = strrchr($_FILES['video']['name'], '.');
            $uploadOssFileParams->file_path = $_FILES['video']['tmp_name'];
        }

        $uploadOssFileParams->object_name = 'system/admin/' . md5_file($uploadOssFileParams->file_path) . $fileExtension;
        if ($_FILES['image'] || $_FILES['video']) {
            $uploadResult = $ossService->uploadOssFile($uploadOssFileParams);
            if (Result::isSuccess($uploadResult)) {
                if ($uploadOssFileParams->is_cdn == true) {
                    $imageUrl = AliyunBase::replaceCdnUrl($uploadResult->data);
                } else {
                    $imageUrl = $uploadResult->data;
                }
                array_push($imageUrlArray, $imageUrl);
            }
        }


        $result->setSuccessWithResult($imageUrlArray);
        $this->render($result);

    }


    //图床工具
    public function imageHostingService()
    {
        $result = new Result();
        $uploadOssFileParams = new  UploadFileParams();
        $ossService = AliyunOssServiceImpl::getInstance();
        $imageUrlArray = [];
        for ($i = 0; $i < 100; $i++) {
            if (!isset($_FILES['image' . $i])) {
                break;
            }
            $fileExtension = strrchr($_FILES['image' . $i]['name'], '.');
            $uploadOssFileParams->file_path = $_FILES['image' . $i]['tmp_name'];
            $uploadOssFileParams->object_name = 'system/admin/' . md5_file($uploadOssFileParams->file_path) . $fileExtension;
            $uploadResult = $ossService->uploadOssFile($uploadOssFileParams);
            if (!Result::isSuccess($uploadResult)) {
                $this->render($uploadResult);
            }
            $imageUrl = AliyunBase::replaceCdnUrl($uploadResult->data);
            array_push($imageUrlArray, $imageUrl);
        }

        $result->setSuccessWithResult($imageUrlArray);
        $this->render($result);
    }


}



