<?php

use facade\request\category\EditCategoryParams;
use facade\request\product\GetProductItemListParams;
use facade\response\base\DataListResp;
use facade\response\base\PageInfoResp;
use facade\response\product\category\CategoryResp;
use facade\response\Result;
use model\product\dto\ProductCategoryDTO;
use service\category\impl\CategoryServiceImpl;
use service\product\impl\ProductServiceImpl;

class CategoryManage extends FUR_Controller
{


    /**
     *创建商品item
     */
    public function getProductCategoryList()
    {
        $result = new Result();
        $categoryDTOListRet = CategoryServiceImpl::getInstance()->queryCategoryList();
        if (!Result::isSuccess($categoryDTOListRet)) {
            $this->render($categoryDTOListRet);
        }
        $listResp = new DataListResp();
        $categoryList = [];
        /** @var ProductCategoryDTO $category */
        foreach ($categoryDTOListRet->data as $category) {
            if ($category->category_level == 1) {
                $categoryResp = new CategoryResp();
                FUR_Core::copyProperties($category, $categoryResp);
                $categoryResp->category_list = [];
                array_push($categoryList, $categoryResp);
                continue;
            }
        }
        foreach ($categoryDTOListRet->data as $category) {
            if ($category->category_level == 2) {
                $categoryResp = new CategoryResp();
                FUR_Core::copyProperties($category, $categoryResp);

                foreach ($categoryList as $level1Category) {
                    if ($level1Category->id == $categoryResp->cate_level1_id) {
                        array_push($level1Category->category_list, $categoryResp);
                        break;
                    }
                }
                continue;
            }
        }
        $listResp->list = $categoryList;
        $listResp->page_info = PageInfoResp::buildPageInfoRespBaseLine(0, 1000, count($listResp->list));
        $result->setSuccessWithResult($listResp);
        $this->render($result);

    }

    public function addProductCategory()
    {
        /** @var EditCategoryParams $params */
        $params = $this->requestObject(EditCategoryParams::class);

        if ($params->parent_id != 0) {
            $categoryInfoRet = CategoryServiceImpl::getInstance()->queryCategoryById($params->parent_id);
            /** @var ProductCategoryDTO $parentCategoryInfo */
            $parentCategoryInfo = $categoryInfoRet->data;
            $params->cate_level1_id = $parentCategoryInfo->id;
            $params->cate_level1_name = $parentCategoryInfo->category_name;
            $params->category_level = 2;
            $params->cate_level2_name = $params->category_name;
        } else {
            $params->category_level = 1;
            $params->cate_level1_name = $params->category_name;
        }

        $createRet = CategoryServiceImpl::getInstance()->createCategory($params);
        $this->render($createRet);
    }

    public function updateProductCategory()
    {
        /** @var EditCategoryParams $params */
        $params = $this->requestObject(EditCategoryParams::class);
        $result = CategoryServiceImpl::getInstance()->updateCategory($params);
        $this->render($result);
    }

    public function deleteProductCategory()
    {
        $result = new Result();
        $category_id = $this->request('category_id');
        $categoryDTORet = CategoryServiceImpl::getInstance()->queryCategoryById($category_id);

        /** @var ProductCategoryDTO $categoryDTO */
        $categoryDTO = $categoryDTORet->data;
        if ($categoryDTO->parent_id == 0) {
            $categoryDTOListRet = CategoryServiceImpl::getInstance()->queryCategoryListByParentId($category_id);
            if (Result::isSuccess($categoryDTOListRet) && !empty($categoryDTOListRet->data)) {
                $result->setError(Config_Error::ERR_CATEGORY_HAVE_CHILDREN);
                $this->render($result);
            }
        } else {
            $getProductItemListParams = new GetProductItemListParams();
            $getProductItemListParams->product_category_id = $category_id;
            $productListRet = ProductServiceImpl::getInstance()->getProductItemList($getProductItemListParams);
            if (Result::isSuccess($productListRet) && !empty($productListRet->data)) {
                $result->setError(Config_Error::ERR_CATEGORY_HAVE_PRODUCT);
                $this->render($result);
            }
        }

        $result = CategoryServiceImpl::getInstance()->deleteCategoryById($category_id);
        $this->render($result);
    }

    public function queryProductCategory()
    {
        $category_id = $this->request('category_id');
        $categoryDTOLRet = CategoryServiceImpl::getInstance()->queryCategoryById($category_id);
        $this->render($categoryDTOLRet);
    }
}