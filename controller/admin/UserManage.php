<?php

use business\user\UserInfoConversionUtil;
use facade\request\user\QueryUserListParams;
use facade\request\user\UpdateUserInfoParams;
use facade\response\base\DataListResp;
use facade\response\base\PageInfoResp;
use facade\response\Result;
use model\user\dto\UserDTO;
use service\admin\impl\UserAdminServiceImpl;
use service\user\impl\UserServiceImpl;

class UserManage extends FUR_Controller
{


    public function queryUserList()
    {
        $result = new Result();

        /** @var  QueryUserListParams $params */
        $params = $this->requestObject(QueryUserListParams::class);


        $userListRet = UserAdminServiceImpl::getInstance()->getUserList($params);
        $userCountRet = UserAdminServiceImpl::getInstance()->getUserTotalCount($params);

        $userList = $userListRet->data;
        $totalCount = $userCountRet->data;
        $listResp = new DataListResp();
        /** @var UserDTO $userDTO */
        foreach ($userList as $userDTO) {
            $userInfo = UserInfoConversionUtil::buildSimpleUserInfo($userDTO, true);
            if ($userInfo == null) {
                continue;
            }
            array_push($listResp->list, $userInfo);
        }
        $listResp->page_info = PageInfoResp::buildPageInfoResp($params->index, $params->page_size, count($userListRet->data), $totalCount);
        $result->setSuccessWithResult($listResp);
        $this->render($result);
    }

    public function deleteUser()
    {
        $userId = $this->request('id');
        $result = UserAdminServiceImpl::getInstance()->deleteUser($userId);
        $this->render($result);
    }

    public function queryUserDetail()
    {
        $result = new Result();
        $userId = $this->request('id');
        $userInfoRet = UserAdminServiceImpl::getInstance()->queryUserInfo($userId);
        $userDTO = $userInfoRet->data;
        $userInfo = UserInfoConversionUtil::buildSimpleUserInfo($userDTO, true);
        $result->setSuccessWithResult($userInfo);
        $this->render($result);
    }

    public function updateUser()
    {
        $result = new Result();
        $userId = $this->request('id');
        /** @var UpdateUserInfoParams $params */
        $params = $this->requestObject(UpdateUserInfoParams::class);
        $params->user_id = $userId;
        $updateRet = UserServiceImpl::getInstance()->updateUserInfo($params);
        $this->render($updateRet);
    }
}