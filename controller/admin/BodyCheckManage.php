<?php

use facade\request\jk\bodyCheck\manage\EditSymptomParams;
use facade\request\jk\bodyCheck\manage\QuerySymptomParams;
use facade\request\PageParams;
use facade\response\base\DataListResp;
use facade\response\base\PageInfoResp;
use facade\response\jiankang\bodyCheck\BodyOptionResp;
use facade\response\jiankang\bodyCheck\BodyQuestionResp;
use facade\response\Result;
use model\jk\dto\JkBodyAgeSymptomDTO;
use model\jk\dto\JkBodyOptionDTO;
use model\jk\dto\JkBodyPossibleDiseaseDTO;
use model\jk\dto\JkBodyQuestionDTO;
use model\jk\JkBodySymptomModel;
use service\jk\impl\JkBodyServiceImpl;
use service\jk\impl\JkDiseaseServiceImpl;

class BodyCheckManage extends FUR_Controller
{


    public function getBodyList()
    {
        $result = new Result();
        /** @var PageParams $params */
        $params = $this->requestObject(PageParams::class);
        $listRet = JkBodyServiceImpl::getInstance()->queryBodyList();
        $listResp = new DataListResp();
        foreach ($listRet->data as $item) {
            array_push($listResp->list, $item);
        }
        $listResp->page_info = PageInfoResp::buildPageInfoRespBaseLine($params->index, 1000, count($listResp->list));
        $result->setSuccessWithResult($listResp);
        $this->render($result);
    }

    public function querySymptomList()
    {
        /** @var QuerySymptomParams $params */
        $params = $this->requestObject(QuerySymptomParams::class);
        $symptomListRet = JkBodyServiceImpl::getInstance()->queryManageSymptomList($params);
        $symptomListCountRet = JkBodyServiceImpl::getInstance()->queryManageSymptomCount($params);

        $listResp = new DataListResp();
        $listResp->list = $symptomListRet->data;
        $listResp->page_info = PageInfoResp::buildPageInfoResp($params->index, $params->page_size, count($listResp->list), $symptomListCountRet->data);
        $result = new Result();
        $result->setSuccessWithResult($listResp);
        $this->render($result);
    }

    public function querySymptomDetail()
    {
        $symptomId = $this->request('symptom_id');
        $symptomRet = JkBodyServiceImpl::getInstance()->querySymptomDetail($symptomId);
        $symptomAgeListRet = JkBodyServiceImpl::getInstance()->querySymptomAgeList($symptomId);
        $result = new Result();
        $symptomInfo = new stdClass();
        $symptomInfo->symptom_detail = $symptomRet->data;
        $symptomInfo->age = [];
        $symptomInfo->sex = [];
        /** @var JkBodyAgeSymptomDTO $ageSymptomDTO */
        foreach ($symptomAgeListRet->data as $ageSymptomDTO) {
            if (!in_array($ageSymptomDTO->age, $symptomInfo->age)) {
                array_push($symptomInfo->age, $ageSymptomDTO->age);
            }
            if (!in_array($ageSymptomDTO->sex, $symptomInfo->sex)) {
                array_push($symptomInfo->sex, $ageSymptomDTO->sex);
            }
        }

        $result->setSuccessWithResult($symptomInfo);
        $this->render($result);
    }

    public function createSymptom()
    {
        /** @var EditSymptomParams $params */
        $params = $this->requestObject(EditSymptomParams::class);
        $result = JkBodyServiceImpl::getInstance()->createSymptom($params);
        $this->render($result);
    }

    public function updateSymptom()
    {
        /** @var EditSymptomParams $params */
        $params = $this->requestObject(EditSymptomParams::class);
        $result = JkBodyServiceImpl::getInstance()->updateSymptom($params);
        $this->render($result);
    }

    public function deleteSymptom()
    {
        $symptomId = $this->request('symptom_id');
        $result = JkBodyServiceImpl::getInstance()->deleteSymptom($symptomId);
        $this->render($result);
    }


    public function updateBodyCheckQuestion()
    {
        $result = new Result();
        $symptom_id = $this->request('symptom_id');
        $question_list = json_decode($this->request('question_list', [], false));
        FUR_Log::info('question_list', json_encode($question_list));
        if ($question_list != null && !empty($question_list)) {
            JkBodySymptomModel::getInstance()->updateJkBodySymptomHaveQuestion($symptom_id, 1);
        } else {
            JkBodySymptomModel::getInstance()->updateJkBodySymptomHaveQuestion($symptom_id, 0);
        }
        $questionSeq = 0;
        JkBodyServiceImpl::getInstance()->cleanJkBodyQuestion($symptom_id);
        foreach ($question_list as $questionInfo) {
            $questionDTO = new JkBodyQuestionDTO();
            $questionSeq = $questionSeq + 1;
            $questionDTO->symptom_id = $symptom_id;
            $questionDTO->title = $questionInfo->question_title;
            $questionDTO->seq = $questionSeq;
            $idRet = JkBodyServiceImpl::getInstance()->insertJkBodyQuestion($questionDTO);
            $questionId = $idRet->data;
            $optionSeq = 0;
            foreach ($questionInfo->question_option as $option) {
                $optionSeq = $optionSeq + 1;
                $optionDTO = new JkBodyOptionDTO();
                $optionDTO->question_id = $questionId;
                $optionDTO->content = $option->content;
                $optionDTO->seq = $optionSeq;
                $optionDTO->score = $option->score;
                JkBodyServiceImpl::getInstance()->insertJkBodyOption($optionDTO);
            }
        }
        $result->setSuccessWithResult(true);
        $this->render($result);
    }

    public function querySymptomQuestionOptionList()
    {
        $result = new Result();
        $symptomId = $this->request('symptom_id');
        $questionListRet = JkBodyServiceImpl::getInstance()->queryQuestionList($symptomId);

        if (!Result::isSuccess($questionListRet)) {
            $this->render($questionListRet);
        }
        $listResp = new DataListResp();

        $questionList = $questionListRet->data;
        /** @var JkBodyQuestionDTO $question */
        foreach ($questionList as $question) {
            $questionResp = new BodyQuestionResp();
            $questionResp->question_id = $question->id;
            $questionResp->title = $question->title;
            $questionResp->seq = $question->seq;

            $optionListRet = JkBodyServiceImpl::getInstance()->queryOptionList($question->id);
            if (Result::isSuccess($optionListRet)) {
                $optionList = $optionListRet->data;
                /** @var JkBodyOptionDTO $option */
                foreach ($optionList as $option) {
                    $optionResp = new BodyOptionResp();
                    $optionResp->option_id = $option->id;
                    $optionResp->seq = $option->seq;
                    $optionResp->content = $option->content;
                    $optionResp->score = $option->score;
                    array_push($questionResp->option_list, $optionResp);
                }
                array_push($listResp->list, $questionResp);
            }

        }
        $result->setSuccessWithResult($listResp);

        $this->render($result);
    }

    public function updatePossibleDisease()
    {
        $result = new Result();

        $symptomId = $this->request('symptom_id');
        JkBodyServiceImpl::getInstance()->cleanPossibleDisease($symptomId);


        $result_list = json_decode($this->request('result_list', [], false));
        FUR_Log::info('result_list', json_encode($result_list));
        $symptomAgeListRet = JkBodyServiceImpl::getInstance()->querySymptomAgeList($symptomId);
        $symptomAgeList = $symptomAgeListRet->data;
        foreach ($result_list as $scoreInfo) {
            $diseaseInfoRet = JkDiseaseServiceImpl::getInstance()->getDisease($scoreInfo->disease_id);
            if (Result::isSuccess($diseaseInfoRet) == false) {
                continue;
            }
            /** @var JkBodyPossibleDiseaseDTO $diseaseInfo */
            $diseaseInfo = $diseaseInfoRet->data;
            /** @var JkBodyAgeSymptomDTO $symptomAge */
            foreach ($symptomAgeList as $symptomAge) {
                $jkBodyPossibleDiseaseDTO = new JkBodyPossibleDiseaseDTO();
                $jkBodyPossibleDiseaseDTO->start_score = $scoreInfo->start_score;
                $jkBodyPossibleDiseaseDTO->symptom_id = $symptomId;
                $jkBodyPossibleDiseaseDTO->age = $symptomAge->age;
                $jkBodyPossibleDiseaseDTO->sex = $symptomAge->sex;
                $jkBodyPossibleDiseaseDTO->class_id = $diseaseInfo->class_id;
                $jkBodyPossibleDiseaseDTO->disease_id = $scoreInfo->disease_id;
                $jkBodyPossibleDiseaseDTO->end_score = $scoreInfo->end_score;
                JkBodyServiceImpl::getInstance()->insertPossibleDisease($jkBodyPossibleDiseaseDTO);
            }
        }
        $result->setSuccessWithResult(true);
        $this->render($result);
    }

    public function queryPossibleDiseaseList()
    {
        $result = new Result();
        $symptomId = $this->request('symptom_id');
        $diseaseListRet = JkBodyServiceImpl::getInstance()->queryPossibleDiseaseListBySymptomId($symptomId);
        $listResp = new DataListResp();
        foreach ($diseaseListRet->data as $item) {
            array_push($listResp->list, $item);
        }
        $result->setSuccessWithResult($listResp);
        $this->render($result);
    }
}