<?php


use facade\request\user\VerificationCodeParams;
use facade\response\Result;
use model\admin\dto\AdminAccountDTO;
use service\admin\impl\AdminAccountServiceImpl;
use service\system\impl\SystemConfigServiceImpl;
use service\user\impl\VerificationCodeServiceImpl;

class Main extends FUR_Controller
{


    public function index()
    {
        $configValueRet = SystemConfigServiceImpl::getInstance()->getConfigValue('admin_static_version');
        $data['admin_static_version'] = $configValueRet->data;
        $data['env'] = (SERVER_TYPE == 'development' ? "dev" : "g");

        $this->display('admin/index', $data);
    }


}