<?php


use facade\request\jk\knowledge\QueryKnowledgeListParams;
use facade\response\base\DataListResp;
use facade\response\base\PageInfoResp;
use facade\response\Result;
use model\jk\dto\JkKnowledgeCategoryDTO;
use model\jk\dto\JkKnowledgeDTO;
use service\jk\impl\JkKnowledgeServiceImpl;

class KnowledgeManage extends FUR_Controller
{


    public function getKnowledgeCategoryList()
    {
        $result = new Result();

        $listRet = JkKnowledgeServiceImpl::getInstance()->getJkKnowledgeCategoryList();
        $listResp = new DataListResp();
        foreach ($listRet->data as $item) {
            array_push($listResp->list, $item);
        }
        $listResp->page_info = PageInfoResp::buildPageInfoRespBaseLine(0, 1000, count($listResp->list));
        $result->setSuccessWithResult($listResp);
        $this->render($result);
    }


    public function getKnowledgeCategoryDetail()
    {

        $id = $this->request('id');
        $detailRet = JkKnowledgeServiceImpl::getInstance()->getJkKnowledgeCategory($id);
        $this->render($detailRet);
    }


    public function createKnowledgeCategory()
    {

        $knowledgeCategory = new JkKnowledgeCategoryDTO();
        $knowledgeCategory->category_name = $this->request('category_name');
        $result = JkKnowledgeServiceImpl::getInstance()->insertJkKnowledgeCategory($knowledgeCategory);
        $this->render($result);

    }


    public function deleteKnowledgeCategory()
    {
        $result = new Result();
        $id = $this->request('id');
        $params = new QueryKnowledgeListParams();
        $params->category_id = $id;
        $knowledgeListRet = JkKnowledgeServiceImpl::getInstance()->getManageJkKnowledgeList($params);

        if (Result::isSuccess($knowledgeListRet) && !empty($knowledgeListRet->data)) {
            $result->setError(Config_Error::ERR_HAS_KNOWLEDGE);
            $this->render($result);
        }
        JkKnowledgeServiceImpl::getInstance()->deleteJkKnowledgeCategory($id);

        $result->setSuccessWithResult(true);
        $this->render($result);
    }


    public function updateKnowledgeCategory()
    {
        $result = new Result();

        $knowledgeCategory = new JkKnowledgeCategoryDTO();
        $knowledgeCategory->category_name = $this->request('category_name');
        $knowledgeCategory->id = $this->request('id');
        JkKnowledgeServiceImpl::getInstance()->updateJkKnowledgeCategory($knowledgeCategory);
        $result->setSuccessWithResult(true);
        $this->render($result);
    }


    public function getKnowledgeList()
    {
        $result = new Result();
        /** @var QueryKnowledgeListParams $params */
        $params = $this->requestObject(QueryKnowledgeListParams::class);

        $listRet = JkKnowledgeServiceImpl::getInstance()->getManageJkKnowledgeList($params);
        $listResp = new DataListResp();
        foreach ($listRet->data as $item) {
            array_push($listResp->list, $item);
        }


        $totalRet = JkKnowledgeServiceImpl::getInstance()->getJkKnowledgeCount($params);
        $listResp->page_info = PageInfoResp::buildPageInfoResp($params->index, $params->page_size, count($listResp->list), $totalRet->data);
        $result->setSuccessWithResult($listResp);
        $this->render($result);
    }


    public function getKnowledgeDetail()
    {

        $id = $this->request('id');
        $detailRet = JkKnowledgeServiceImpl::getInstance()->getJkKnowledge($id);
        $this->render($detailRet);
    }


    public function createKnowledge()
    {

        $knowledge = new JkKnowledgeDTO();
        $knowledge->category_id = $this->request('category_id');
        $knowledge->knowledge_title = $this->request('knowledge_title');

        $cover = $this->request('knowledge_cover');

        if ($cover != null) {
            $knowledge->knowledge_cover = $cover;
        }
        $knowledge->knowledge_content = $this->request('knowledge_content', "", false);

        $result = JkKnowledgeServiceImpl::getInstance()->insertJkKnowledge($knowledge);
        $this->render($result);

    }


    public function deleteKnowledge()
    {
        $result = new Result();
        $id = $this->request('id');

        JkKnowledgeServiceImpl::getInstance()->deleteJkKnowledge($id);

        $result->setSuccessWithResult(true);
        $this->render($result);
    }


    public function updateKnowledge()
    {
        $result = new Result();

        $knowledge = new JkKnowledgeDTO();
        $knowledge->category_id = $this->request('category_id');
        $knowledge->knowledge_title = $this->request('knowledge_title');
        $knowledge->knowledge_content = $this->request('knowledge_content', "", false);
        $cover = $this->request('knowledge_cover');

        if ($cover != null) {
            $knowledge->knowledge_cover = $cover;
        }
        $knowledge->id = $this->request('id');
        JkKnowledgeServiceImpl::getInstance()->updateJkKnowledge($knowledge);
        $result->setSuccessWithResult(true);
        $this->render($result);
    }

    public function updateKnowledgePublishStatus()
    {
        $result = new Result();

        $id = $this->request('id');
        $publishStatus = intval($this->request('publish_status'));
        $knowledgeInfoRet = JkKnowledgeServiceImpl::getInstance()->getJkKnowledge($id);
        /** @var JkKnowledgeDTO $knowledgeInfo */
        $knowledgeInfo = $knowledgeInfoRet->data;
        $knowledgeInfo->publish_status = $publishStatus;
        JkKnowledgeServiceImpl::getInstance()->updateJkKnowledge($knowledgeInfo);
        $result->setSuccessWithResult(true);
        $this->render($result);
    }

}