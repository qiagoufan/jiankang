<?php


use facade\request\admin\banner\CreateBannerParams;
use facade\request\admin\banner\UpdateBannerParams;
use facade\request\PageParams;
use facade\response\base\DataListResp;
use facade\response\base\PageInfoResp;
use facade\response\operation\AdvertisingResp;
use facade\response\Result;
use model\operation\dto\OperationAdvertisingDTO;
use model\security\dto\SecurityWordDTO;
use service\aliyun\AliyunBase;
use service\operation\impl\AdvertisingServiceImpl;
use service\security\impl\SecurityCheckServiceImpl;

class SecurityWordManage extends FUR_Controller
{


    public function getSecurityWordList()
    {
        $result = new Result();
        /** @var PageParams $params */
        $params = $this->requestObject(PageParams::class);
        $wordListRet = SecurityCheckServiceImpl::getInstance()->queryWordList($params);
        $wordCountRet = SecurityCheckServiceImpl::getInstance()->queryWordCount($params);
        $listResp = new DataListResp();

        $listResp->list = $wordListRet->data;

        $listResp->page_info = PageInfoResp::buildPageInfoResp($params->index, $params->page_size, count($listResp->list), $wordCountRet->data);
        $result->setSuccessWithResult($listResp);
        $this->render($result);
    }


    /**
     */
    public function getSecurityWordDetail()
    {

        $id = $this->request('id');
        $wordRet = SecurityCheckServiceImpl::getInstance()->getDetail($id);
        $this->render($wordRet);
    }


    /**
     */
    public function createSecurityWord()
    {

        $result = new Result();
        $word = $this->request('security_word');
        $insertRet = SecurityCheckServiceImpl::getInstance()->addWord($word);
        $result->setSuccessWithResult($insertRet->data);
        $this->render($result);

    }

    /**
     */
    public function deleteSecurityWord()
    {
        $result = new Result();
        $bannerId = $this->request('id');

        SecurityCheckServiceImpl::getInstance()->deleteWord($bannerId);

        $result->setSuccessWithResult(true);
        $this->render($result);
    }


    /**
     * 编辑banner
     */
    public function updateSecurityWord()
    {

        $id = $this->request('id');
        $word = $this->request('security_word');
        $wordRet = SecurityCheckServiceImpl::getInstance()->getDetail($id);
        /** @var SecurityWordDTO $securityWordDTO */
        $securityWordDTO = $wordRet->data;

        $securityWordDTO->word = $word;
        $result = SecurityCheckServiceImpl::getInstance()->updateWord($securityWordDTO);

        $this->render($result);


    }

}