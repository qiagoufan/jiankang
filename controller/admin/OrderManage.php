<?php


use business\order\OrderInfoConversionUtil;
use facade\request\admin\QueryAllOrderListParams;
use facade\response\base\DataListResp;
use facade\response\base\PageInfoResp;
use facade\response\Result;
use model\order\dto\OrderInfoDTO;
use service\admin\impl\OrderAdminServiceImpl;
use service\order\impl\OrderServiceImpl;

class OrderManage extends FUR_Controller
{


    /**
     * 获取订单列表
     */

    public function queryOrderList()
    {
        $result = new Result();
        /** @var QueryAllOrderListParams $params */
        $params = $this->requestObject(QueryAllOrderListParams::class);
        $orderAdminService = OrderAdminServiceImpl::getInstance();
        $orderListRet = $orderAdminService->getOrderList($params);
        if (!Result::isSuccess($orderListRet)) {
            $this->render($orderListRet);
        }
        $orderArrayList = [];
        /** @var OrderInfoDTO $orderInfoDTO */
        foreach ($orderListRet->data as $orderInfoDTO) {
            $orderInfoResp = OrderInfoConversionUtil::conversionOrderDetailResp($orderInfoDTO);
            if (!$orderInfoResp) {
                FUR_Log::warn("getOrderBuildInfo fail", json_encode($orderInfoDTO));
                continue;
            }
            array_push($orderArrayList, $orderInfoResp);
        }
        $totalCount = $orderAdminService->queryOrderListCount($params)->data;
        $pageInfo = PageInfoResp::buildPageInfoResp($params->index, $pageSize = 10, count($orderListRet->data), $totalCount);
        $OrderListResp = new DataListResp();
        $OrderListResp->list = $orderArrayList;
        $OrderListResp->page_info = $pageInfo;
        $result->setSuccessWithResult($OrderListResp);
        $this->render($result);

    }


    public function queryOrderDetail()
    {
        $result = new Result();
        $orderSn = $this->request('order_sn');
        $orderService = OrderServiceImpl::getInstance();
        $orderInfoRet = $orderService->getOrderInfoByOrderSn($orderSn);
        if (!Result::isSuccess($orderInfoRet)) {
            $result->setSuccessWithResult(null);
            $this->render($result);
        }
        $orderDetailResp = OrderInfoConversionUtil::conversionOrderDetailResp($orderInfoRet->data);
        $result->setSuccessWithResult($orderDetailResp);
        $this->render($result);


    }

    /**
     *操作发货
     */
    public function deliverOrder()
    {
        $result = new Result();
        $orderSn = $this->request('order_sn');
        $expressCompany = $this->request('express_company');
        $expressNo = $this->request('express_no');
        /**
         * 校验参数
         */
        $orderAdminService = OrderAdminServiceImpl::getInstance();
        $deliverRet = $orderAdminService->deliveryOrder($orderSn, $expressCompany, $expressNo);
        $this->render($deliverRet);
    }

    /**
     *获取物流公司
     */
    public
    function getExpressList()
    {
        $result = new Result();
        $expressCompanyList = FUR_Config::get_express_config('express_company_list');
        $result->setSuccessWithResult($expressCompanyList);
        $this->render($result);
    }


    /**
     * 取消订单,交易关闭
     */
    public
    function cancelOrder()
    {
        $result = new Result();
        $order_id = $this->request('order_id');
        $orderServiceImpl = OrderServiceImpl::getInstance();
        /**
         * 校验参数
         */
        if ($order_id == null) {
            $result->setError(Config_Error::ERR_CANCEL_ORDER_INFO_IS_NOT_FULL);
            $this->render($result);
        }
        $cancelOrderRet = $orderServiceImpl->cancelOrder($order_id);
        $this->render($cancelOrderRet);

    }


}