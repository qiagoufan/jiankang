<?php

use business\product\JkProductConversionUtil;
use facade\request\admin\EditProductItemParams;
use facade\request\product\GetProductItemListParams;
use facade\request\product\QueryAdminProductItemListParams;
use facade\response\base\DataListResp;
use facade\response\base\PageInfoResp;
use facade\response\Result;
use service\admin\impl\ProductAdminServiceImpl;

class ProductManage extends FUR_Controller
{


    /**
     *创建商品item
     */
    public function createProductItem()
    {
        /** @var EditProductItemParams $params */
        $params = $this->requestObject(EditProductItemParams::class);
        $checkParamsRet = $this->checkParams($params);
        if (!Result::isSuccess($checkParamsRet)) {
            $this->render($checkParamsRet);
        }
        $insertRet = ProductAdminServiceImpl::getInstance()->createProductItem($params);
        $this->render($insertRet);
    }


    /**
     * 获取商品列表
     */
    public function getProductItemList()
    {
        $result = new Result();
        /** @var QueryAdminProductItemListParams $params */
        $params = $this->requestObject(QueryAdminProductItemListParams::class);
        $productAdminService = ProductAdminServiceImpl::getInstance();
        $itemListRet = $productAdminService->queryProductItemList($params);
        if (!Result::isSuccess($itemListRet)) {
            $this->render($itemListRet);
        }
        $listResp = new DataListResp();
        foreach ($itemListRet->data as $itemInfo) {

            $productItemResp = JkProductConversionUtil::getInstance()->buildProductResp($itemInfo);
            array_push($listResp->list, $productItemResp);
        }

        $count = $productAdminService->queryItemCount($params)->data;
        $pageInfo = PageInfoResp::buildPageInfoResp($params->index, $params->page_size, count($itemListRet->data), $count);
        $listResp->page_info = $pageInfo;
        $result->setSuccessWithResult($listResp);
        $this->render($result);
    }


    /**
     *更新商品
     */
    public function updateProductItem()
    {
        /** @var EditProductItemParams $params */
        $params = $this->requestObject(EditProductItemParams::class);
        $checkParamsRet = $this->checkParams($params);
        if (!Result::isSuccess($checkParamsRet)) {
            $this->render($checkParamsRet);
        }
        $updateRet = ProductAdminServiceImpl::getInstance()->updateProductInfo($params);
        $this->render($updateRet);
    }


    /**
     * @return Result|null
     */
    public function getProductItemDetail()
    {
        $result = new Result();
        $itemId = $this->request('item_id');
        if (!$itemId) {
            $result->setError(\Config_Error::ERR_PRODUCT_ITEM_ID_NOT_EXISTS);
            $this->render($result);
        }
        $productAdminService = ProductAdminServiceImpl::getInstance();
        $productItemRet = $productAdminService->getProductItemDetailById($itemId);
        $productItemDTO = $productItemRet->data;
        if (!Result::isSuccess($productItemRet)) {
            $this->render($productItemRet);
        }

        $productItemResp = JkProductConversionUtil::getInstance()->buildProductResp($productItemDTO);
        $result->setSuccessWithResult($productItemResp);
        $this->render($result);

    }


    /**
     * 更新商品上下架状态
     */
    public function updateProductPublishStatus()
    {

        $itemId = $this->request('item_id');

        $publishStatus = $this->request('publish_status');
        $result = ProductAdminServiceImpl::getInstance()->updateProductItemPublishStatus($itemId, $publishStatus);
        $this->render($result);
    }

    /**
     * 删除商品
     */
    public function deleteProduct()
    {

        $itemId = $this->request('item_id');
        $result = ProductAdminServiceImpl::getInstance()->deleteProduct($itemId);
        $this->render($result);
    }


    private function checkParams(EditProductItemParams $editParams): Result
    {
        $result = new Result();
        if ($editParams->product_name == null) {
            $result->setSuccessWithResult(Config_Error::ERR_PRODUCT_ITEM_NAME_IS_NULL);
            return $result;
        }
        if ($editParams->main_image == null) {
            $result->setSuccessWithResult(Config_Error::ERR_PRODUCT_ITEM_MAIN_IMAGE_IS_NULL);
            return $result;
        }
        if ($editParams->current_price == 0) {
            $result->setSuccessWithResult(Config_Error::ERR_PRODUCT_ITEM_PRICE_IS_NULL);
            return $result;
        }
        if ($editParams->summary == null) {
            $result->setSuccessWithResult(Config_Error::ERR_PRODUCT_ITEM_SUMMARY_IS_NULL);
            return $result;
        }
        if ($editParams->product_desc == null) {
            $result->setSuccessWithResult(Config_Error::ERR_PRODUCT_ITEM_DESCRIPTION_IS_NULL);
            return $result;
        }
        $result->setSuccessWithResult(true);
        return $result;

    }

}