<?php


use facade\request\feed\ConversionFeedParams;
use facade\request\feed\QueryManageFeedListParams;
use facade\response\base\DataListResp;
use facade\response\base\PageInfoResp;
use facade\response\Result;
use model\feed\dto\FeedCategoryDTO;
use model\feed\dto\FeedInfoDTO;
use service\feed\impl\FeedInfoRespServiceImpl;
use service\feed\impl\FeedServiceImpl;

class FeedManage extends FUR_Controller
{


    public function getFeedCategoryList()
    {
        $result = new Result();

        $listRet = FeedServiceImpl::getInstance()->getFeedCategoryList();
        $listResp = new DataListResp();
        foreach ($listRet->data as $item) {
            array_push($listResp->list, $item);
        }
        $listResp->page_info = PageInfoResp::buildPageInfoRespBaseLine(0, 1000, count($listResp->list));
        $result->setSuccessWithResult($listResp);
        $this->render($result);
    }


    public function getFeedCategoryDetail()
    {

        $id = $this->request('id');
        $detailRet = FeedServiceImpl::getInstance()->getFeedCategory($id);
        $this->render($detailRet);
    }


    public function createFeedCategory()
    {

        $feedCategory = new FeedCategoryDTO();
        $feedCategory->category_name = $this->request('category_name');
        $result = FeedServiceImpl::getInstance()->insertFeedCategory($feedCategory);
        $this->render($result);

    }


    public function deleteFeedCategory()
    {
        $result = new Result();
        $id = $this->request('id');

        FeedServiceImpl::getInstance()->deleteFeedCategory($id);

        $result->setSuccessWithResult(true);
        $this->render($result);
    }


    public function updateFeedCategory()
    {
        $result = new Result();

        $FeedCategory = new FeedCategoryDTO();
        $FeedCategory->category_name = $this->request('category_name');
        $FeedCategory->id = $this->request('id');
        FeedServiceImpl::getInstance()->updateFeedCategory($FeedCategory);
        $result->setSuccessWithResult(true);
        $this->render($result);
    }


    public function queryManageFeedList()
    {
        $result = new Result();
        /** @var QueryManageFeedListParams $params */
        $params = $this->requestObject(QueryManageFeedListParams::class);


        $conversionFeedParams = new ConversionFeedParams();
        FUR_Core::copyProperties($params, $conversionFeedParams);

        $listRet = FeedServiceImpl::getInstance()->queryManageFeedList($params);
        $listResp = new DataListResp();
        foreach ($listRet->data as $item) {
            $data = FeedInfoRespServiceImpl::getInstance()->conversionFeedInfoResp($item, $conversionFeedParams);
            if ($data == null) {
                continue;
            }
            array_push($listResp->list, $data);
        }

        $totalRet = FeedServiceImpl::getInstance()->queryManageFeedCount($params);
        $listResp->page_info = PageInfoResp::buildPageInfoResp($params->index, $params->page_size, count($listRet->data), $totalRet->data);
        $result->setSuccessWithResult($listResp);
        $this->render($result);
    }


    public function getFeedDetail()
    {

        $id = $this->request('id');
        $result = new Result();
        $detailRet = FeedServiceImpl::getInstance()->getFeedDetail($id);
        $data = FeedInfoRespServiceImpl::getInstance()->conversionFeedInfoResp($detailRet->data, $conversionFeedParams);

        $result->setSuccessWithResult($data);
        $this->render($result);
    }


    public function deleteFeed()
    {
        $result = new Result();
        $id = $this->request('id');

        FeedServiceImpl::getInstance()->deleteFeed($id);

        $result->setSuccessWithResult(true);
        $this->render($result);
    }


    public function updateFeedStatus()
    {
        $result = new Result();

        $feedInfoDTO = new FeedInfoDTO();

        $feedInfoDTO->id = $this->request('id');
        $feedInfoDTO->status = $this->request('check_status');
        FeedServiceImpl::getInstance()->updateFeedDTO($feedInfoDTO);
        $result->setSuccessWithResult(true);
        $this->render($result);
    }

}