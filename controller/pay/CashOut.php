<?php

use business\withdraw\WithdrawInfoConversionUtil;
use facade\request\repair\QueryRepairOrderListParams;
use facade\response\base\DataListResp;
use facade\response\base\PageInfoResp;
use facade\response\Result;
use lib\PriceHelper;
use model\repair\dto\RepairOrderInfoDTO;
use model\user\dto\UserEngineerDTO;
use model\withdraw\dto\WithdrawRecordDTO;
use service\repair\impl\RepairOrderServiceImpl;
use service\user\impl\UserEngineerServiceImpl;
use service\withdraw\impl\WithdrawServiceImpl;

class CashOut extends FUR_Controller
{

    public function __construct()
    {

    }

    public function queryAmount()
    {
        $result = new Result();
        $jwtPayloadInfo = $this->getRequestUserPayloadInfo(true);

        $engineerId = 0;
        if ($jwtPayloadInfo->login_type != Config_Const::LOGIN_TYPE_ENGINEER) {
            $result->setSuccessWithResult(Config_Error::ERR_LOGIN_ENGINEER_NOT_EXIST);
            return $result;
        }
        if ($jwtPayloadInfo->login_type == Config_Const::LOGIN_TYPE_ENGINEER) {
            $engineerInfoRet = UserEngineerServiceImpl::getInstance()->queryEngineerInfoByPhone($jwtPayloadInfo->unique_id);
            if (!Result::isSuccess($engineerInfoRet)) {
                $result->setSuccessWithResult(Config_Error::ERR_LOGIN_ENGINEER_NOT_EXIST);
                return $result;
            }
            /** @var UserEngineerDTO $engineerInfo */
            $engineerInfo = $engineerInfoRet->data;
            $engineerId = $engineerInfo->id;
        }
        $amountRet = RepairOrderServiceImpl::getInstance()->queryWithdrawAbleAmount($engineerId);
        $amount = $amountRet->data;
        $result->setSuccessWithResult(PriceHelper::conversionDisplayPrice($amount));

        $this->render($result);
    }


    public function queryEngineerWithdrawCardInfo()
    {
        $result = new Result();

        $jwtPayloadInfo = $this->getRequestUserPayloadInfo(true);
        if ($jwtPayloadInfo->login_type != Config_Const::LOGIN_TYPE_ENGINEER) {
            $result->setSuccessWithResult(Config_Error::ERR_LOGIN_ENGINEER_NOT_EXIST);
            return $result;
        }
        $engineerInfoRet = UserEngineerServiceImpl::getInstance()->queryEngineerInfoByPhone($jwtPayloadInfo->unique_id);
        if (!Result::isSuccess($engineerInfoRet)) {
            $result->setSuccessWithResult(Config_Error::ERR_LOGIN_ENGINEER_NOT_EXIST);
            return $result;
        }
        /** @var UserEngineerDTO $engineerInfo */
        $engineerInfo = $engineerInfoRet->data;

        $data['withdraw_card_num'] = $engineerInfo->withdraw_card_num;
        $data['withdraw_card_real_name'] = $engineerInfo->withdraw_card_real_name;
        $result->setSuccessWithResult($data);
        $this->render($result);
    }

    public function applyForWithdraw()
    {
        $result = new Result();

        $withdraw_card_num = $this->request('withdraw_card_num');
        $withdraw_card_real_name = $this->request('withdraw_card_real_name');
        $withdraw_card_bank = $this->request('withdraw_card_bank');

        $jwtPayloadInfo = $this->getRequestUserPayloadInfo(true);
        if ($jwtPayloadInfo->login_type != Config_Const::LOGIN_TYPE_ENGINEER) {
            $result->setSuccessWithResult(Config_Error::ERR_LOGIN_ENGINEER_NOT_EXIST);
            return $result;
        }
        $engineerInfoRet = UserEngineerServiceImpl::getInstance()->queryEngineerInfoByPhone($jwtPayloadInfo->unique_id);
        if (!Result::isSuccess($engineerInfoRet)) {
            $result->setSuccessWithResult(Config_Error::ERR_LOGIN_ENGINEER_NOT_EXIST);
            return $result;
        }
        /** @var UserEngineerDTO $engineerInfo */
        $engineerInfo = $engineerInfoRet->data;
        $engineerId = $engineerInfo->id;
        if ($withdraw_card_real_name == null || $withdraw_card_num == null || $withdraw_card_bank == null) {
            $result->setError(Config_Error::ERR_REPAIR_ORDER_WITHDRAW_CARD_INFO_ERROR);
            $this->render($result);
        }

        $queryOrderListParams = new QueryRepairOrderListParams();
        $queryOrderListParams->page_size = 1000;
        $queryOrderListParams->order_status = Config_Const::REPAIR_ORDER_CONFIRM_PRICE;
        $queryOrderListParams->engineer_id = $engineerId;
        $queryOrderListParams->cash_out_status = Config_Const::CASH_OUT_STATUS_NEW;
        $repairOrderListRet = RepairOrderServiceImpl::getInstance()->queryRepairOrderList($queryOrderListParams);
        if (!Result::isSuccess($repairOrderListRet)) {
            $this->render($repairOrderListRet);
        }
        $repairOrderList = $repairOrderListRet->data;

        $withdrawRepairOrderIdList = [];
        $withdraw_total_amount = 0;
        /** @var RepairOrderInfoDTO $repairOrderInfo */
        foreach ($repairOrderList as $repairOrderInfo) {
            if ($repairOrderInfo->cash_out_status != Config_Const::CASH_OUT_STATUS_NEW) {
                continue;
            }
            if ($repairOrderInfo->engineer_reward <= 0) {
                continue;
            }
            array_push($withdrawRepairOrderIdList, $repairOrderInfo->id);
            $withdraw_total_amount = $withdraw_total_amount + $repairOrderInfo->engineer_reward;

            //变更订单提现状态
            $repairOrderInfo->cash_out_status = Config_Const::CASH_OUT_STATUS_WAIT_FOR_PAY;
            RepairOrderServiceImpl::getInstance()->updateRepairOrder($repairOrderInfo);
        }


        //插入数据
        $withdrawDTO = new WithdrawRecordDTO();
        $withdrawDTO->repair_order_id_list = json_encode($withdrawRepairOrderIdList);
        $withdrawDTO->engineer_id = $engineerId;
        $withdrawDTO->withdraw_total_amount = $withdraw_total_amount;
        $withdrawDTO->withdraw_card_bank = $withdraw_card_bank;
        $withdrawDTO->withdraw_card_num = $withdraw_card_num;
        $withdrawDTO->withdraw_card_real_name = $withdraw_card_real_name;
        $withdrawDTO->cash_out_status = Config_Const::CASH_OUT_STATUS_WAIT_FOR_PAY;
        $insertRet = WithdrawServiceImpl::getInstance()->createWithDrawRecord($withdrawDTO);


        //记录用户卡号
        $engineerInfo->withdraw_card_num = $withdraw_card_num;
        $engineerInfo->withdraw_card_real_name = $withdraw_card_real_name;
        $engineerInfo->withdraw_card_bank = $withdraw_card_bank;
        UserEngineerServiceImpl::getInstance()->updateEngineerInfo($engineerInfo);

        $this->render($insertRet);
    }

    public function queryEngineerWithdrawRecordList()
    {
        $index = $this->request('index', 0);
        $pageSize = 10;
        $result = new Result();
        $jwtPayloadInfo = $this->getRequestUserPayloadInfo(true);
        if ($jwtPayloadInfo->login_type != Config_Const::LOGIN_TYPE_ENGINEER) {
            $result->setSuccessWithResult(Config_Error::ERR_LOGIN_ENGINEER_NOT_EXIST);
            return $result;
        }
        $engineerInfoRet = UserEngineerServiceImpl::getInstance()->queryEngineerInfoByPhone($jwtPayloadInfo->unique_id);
        if (!Result::isSuccess($engineerInfoRet)) {
            $result->setSuccessWithResult(Config_Error::ERR_LOGIN_ENGINEER_NOT_EXIST);
            return $result;
        }
        /** @var UserEngineerDTO $engineerInfo */
        $engineerInfo = $engineerInfoRet->data;
        $engineerId = $engineerInfo->id;


        $withdrawRecordListRet = WithdrawServiceImpl::getInstance()->queryEngineerWithdrawRecordList($engineerId, $index, $pageSize);
        if (!Result::isSuccess($withdrawRecordListRet)) {
            $this->render($withdrawRecordListRet);
        }

        $listResp = new DataListResp();
        $withdrawRecordList = $withdrawRecordListRet->data;
        /** @var WithdrawRecordDTO $withdrawRecord */
        foreach ($withdrawRecordList as $withdrawRecord) {
            $withdrawResp = WithdrawInfoConversionUtil::getInstance()->conversionWithdrawInfo($withdrawRecord);
            if ($withdrawResp == null) {
                continue;
            }
            array_push($listResp->list, $withdrawResp);
        }
        $listResp->page_info = PageInfoResp::buildPageInfoRespBaseLine($index, $pageSize, count($listResp->list));

        $result->setSuccessWithResult($listResp);
        $this->render($result);
    }

}