<?php

use facade\response\operation\AdvertisingListResp;
use facade\response\operation\AdvertisingResp;
use facade\response\Result;
use model\operation\dto\OperationAdvertisingDTO;
use service\operation\impl\AdvertisingServiceImpl;

class Advertising extends FUR_Controller
{

    public function getAdvertisingList()
    {
        $result = new Result();
        $space_id = (int)$this->request('space_id');
        $advertisingRet = AdvertisingServiceImpl::getInstance()->getAdvertisingListBySpace($space_id);
        if (Result::isSuccess($advertisingRet)) {
            $advertisingListResp = new AdvertisingListResp();
            $advertisingListResp->advertising_list = [];
            /** @var OperationAdvertisingDTO $advertisingDTO */
            foreach ($advertisingRet->data as $advertisingDTO) {
                $advertisingResp = new AdvertisingResp();
                $advertisingResp->id = $advertisingDTO->id;
                $advertisingResp->link = $advertisingDTO->link_type . $advertisingDTO->link;
                $advertisingResp->type = $advertisingDTO->type;
                $advertisingResp->title = $advertisingDTO->title;
                if ($advertisingDTO->type == Config_Const::AD_TYPE_IMAGE) {
                    $jsonDecodeData = json_decode($advertisingDTO->data);
                    $advertisingResp->image = $jsonDecodeData->image;
                }

                array_push($advertisingListResp->advertising_list, $advertisingResp);
            }
            $result->setSuccessWithResult($advertisingListResp);
        }
        $this->render($result);
    }


}