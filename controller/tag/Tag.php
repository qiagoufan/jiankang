<?php

use facade\request\tag\CreateTagEntityParams;
use facade\request\tag\CreateTagParams;
use facade\request\tag\DeleteTagEntityParams;
use facade\request\tag\QueryTagEntityListParams;
use facade\request\tag\TagParams;
use facade\response\Result;
use service\tag\impl\TagServiceImpl;

class Tag extends FUR_Controller
{


    public function createTag()
    {
        $tagService = TagServiceImpl::getInstance();
        $createTagParams = $this->requestObject(CreateTagParams::class);
        $result = $tagService->createTagIfNotExists($createTagParams);
        $this->render($result);
    }

    public function queryTagId()
    {
        $tagService = TagServiceImpl::getInstance();
        $tagParams = $this->requestObject(TagParams::class);
        $result = $tagService->queryTagId($tagParams);
        $this->render($result);
    }

    public function createTagEntity()
    {
        $tagService = TagServiceImpl::getInstance();
        $tagParams = $this->requestObject(CreateTagEntityParams::class);
        $result = $tagService->createTagEntity($tagParams);
        $this->render($result);
    }

    public function queryTagEntityList()
    {
        $tagService = TagServiceImpl::getInstance();
        $tagParams = $this->requestObject(QueryTagEntityListParams::class);
        $result = $tagService->queryTagEntityList($tagParams);
        $this->render($result);
    }

    public function deleteTagEntity()
    {
        $tagService = TagServiceImpl::getInstance();
        $tagParams = $this->requestObject(DeleteTagEntityParams::class);
        $result = $tagService->deleteTagEntity($tagParams);
        $this->render($result);
    }
}