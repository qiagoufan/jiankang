<?php

use business\star\StarConversionUtil;
use facade\request\user\FollowParams;
use facade\response\base\PageInfoResp;
use facade\response\Result;
use facade\response\user\StarInfoResp;
use facade\response\user\StarListResp;
use model\dto\UserStarDTO;
use service\maintain\impl\MaintainLeaderboardServiceImpl;
use service\user\impl\FollowServiceImpl;
use service\user\impl\StarServiceImpl;
use facade\request\user\GetStarRankListParams;

class Star extends FUR_Controller
{
    const DEFAULT_PAGE_SIZE = 10;


    public function getStarInfo()
    {
        $result = new Result();
        $userId = $this->getRequestUserId(false);
        $starId = $this->request('star_id');
        $starInfoResp = StarConversionUtil::getInstance()->buildStarInfoResp($starId, $userId);
        $result->setSuccessWithResult($starInfoResp);
        $this->render($result);
    }


    public function getStarList()
    {
        $result = new Result();
        $starListResp = new StarListResp();

        $userId = $this->getRequestUserId(false);
        $index = (int)$this->request('index', 0);
        $keyword = $this->request('keyword');
        /** 如果有关键词就走搜索,之前没定义好先放一个方法里了 */
        if ($keyword != null) {
            $this->searchStar();
        }

        if ($userId == 0) {
            $result->setSuccessWithResult($starListResp);
            $this->render($result);
        }
        $starInfoRespList = [];

        $followService = FollowServiceImpl::getInstance();
        $followParams = new FollowParams();
        $followParams->target_type = UserStarDTO::STAR_BIZ_TYPE;
        $followParams->user_id = $userId;

        $focusStarListRet = $followService->getUserFocusList($followParams);
        if (!Result::isSuccess($focusStarListRet)) {
            $result->setSuccessWithResult($starListResp);
            $this->render($result);
        }
        $starIdList = $focusStarListRet->data;
        foreach ($starIdList as $starId) {
            /** @var StarInfoResp $starInfoResp */
            $starInfoResp = StarConversionUtil::getInstance()->buildStarInfoResp($starId, $userId, true);
            if ($starInfoResp == null) {
                continue;
            }
            array_push($starInfoRespList, $starInfoResp);
        }

        $starListResp->star_list = $starInfoRespList;
        $pageInfoResp = PageInfoResp::buildPageInfoRespBaseLine($index, self::DEFAULT_PAGE_SIZE, count($starInfoRespList));
        $pageInfoResp->has_more = false;
        $starListResp->page_info = $pageInfoResp;


        $result->setSuccessWithResult($starListResp);
        $this->render($result);
    }

    public function getUserFocusStarList()
    {
        $result = new Result();
        $starListResp = new StarListResp();

        $userId = $this->request('visit_user_id', 0);
        $index = (int)$this->request('index', 0);


        if ($userId == 0) {
            $result->setSuccessWithResult($starListResp);
            $this->render($result);
        }

        $starInfoRespList = [];

        $followService = FollowServiceImpl::getInstance();
        $followParams = new FollowParams();
        $followParams->target_type = UserStarDTO::STAR_BIZ_TYPE;
        $followParams->user_id = $userId;
        $followParams->index = $index;
        $focusStarListRet = $followService->getUserFocusList($followParams);
        if (!Result::isSuccess($focusStarListRet)) {
            $result->setSuccessWithResult($starListResp);
            $this->render($result);
        }
        $starIdList = $focusStarListRet->data;
        foreach ($starIdList as $starId) {
            /** @var StarInfoResp $starInfoResp */
            $starInfoResp = StarConversionUtil::getInstance()->buildStarInfoResp($starId, $userId);
            if ($starInfoResp == null) {
                continue;
            }
            array_push($starInfoRespList, $starInfoResp);
        }

        $starListResp->star_list = $starInfoRespList;
        $pageInfoResp = PageInfoResp::buildPageInfoRespBaseLine($index, self::DEFAULT_PAGE_SIZE, count($starInfoRespList));
        $starListResp->page_info = $pageInfoResp;
        $result->setSuccessWithResult($starListResp);
        $this->render($result);
    }


    public function searchStar()
    {
        $result = new Result();
        $userId = $this->getRequestUserId(false);
        $keyword = $this->request('keyword');
        $index = $this->request('index', 0);
        $starService = StarServiceImpl::getInstance();

        $starListResp = new StarListResp();

        $starListRet = $starService->searchStar($keyword, $index, self::DEFAULT_PAGE_SIZE);
        if (Result::isSuccess($starListRet) == false) {
            $result->setSuccessWithResult($starListResp);
            $this->render($result);
        }
        $starDTOList = $starListRet->data;
        $starIdList = [];
        /** @var UserStarDTO $starDTO */
        foreach ($starDTOList as $starDTO) {
            array_push($starIdList, $starDTO->id);
        }
        $starInfoRespList = [];
        foreach ($starIdList as $starId) {
            $starInfoResp = StarConversionUtil::getInstance()->buildStarInfoResp($starId, $userId);
            array_push($starInfoRespList, $starInfoResp);
        }
        $starListResp->star_list = $starInfoRespList;
        $pageInfoResp = PageInfoResp::buildPageInfoRespBaseLine($index, self::DEFAULT_PAGE_SIZE, count($starInfoRespList));
        $starListResp->page_info = $pageInfoResp;
        $result->setSuccessWithResult($starListResp);
        $this->render($result);
    }


    public function starRank()
    {
        $result = new Result();
        $getStarRankListParams = $this->requestObject(GetStarRankListParams::class);
        $userId = $this->getRequestUserId(false);

        $starListResp = new StarListResp();
        $index = $getStarRankListParams->index;

        $starInfoRespList = [];
        if ($getStarRankListParams->type == GetStarRankListParams::RANK_TYPE_STAR) {
            $leaderboardRet = MaintainLeaderboardServiceImpl::getInstance()->getStarPopularityLeaderboard($index, self::DEFAULT_PAGE_SIZE);
            if (Result::isSuccess($leaderboardRet) == false) {
                $result->setSuccessWithResult($starListResp);
                $this->render($result);
            }
            $starList = $leaderboardRet->data;
            foreach ($starList as $starId) {
                $starInfoResp = StarConversionUtil::getInstance()->buildStarInfoResp($starId, $userId);
                $starInfoResp->leaderboard_rank = substr($starInfoResp->leaderboard_rank, 3);
                array_push($starInfoRespList, $starInfoResp);
            }

        } elseif ($getStarRankListParams->type == GetStarRankListParams::RANK_TYPE_BRAND) {


            $starServiceImpl = StarServiceImpl::getInstance();
            $brandListRet = $starServiceImpl->getStarDiscussionList(UserStarDTO::STAR_BIZ_TYPE, self::DEFAULT_PAGE_SIZE, $index);
            if (Result::isSuccess($brandListRet)) {
                foreach ($brandListRet->data as $starId) {
                    $starInfoResp = StarConversionUtil::getInstance()->buildStarInfoResp($starId, $userId);
                    $starInfoResp->brand_leader_board_rank = substr($starInfoResp->brand_leader_board_rank, 3);
                    array_push($starInfoRespList, $starInfoResp);
                }
            }
        }


        $starListResp->star_list = $starInfoRespList;
        $pageInfoResp = PageInfoResp::buildPageInfoRespBaseLine($index, self::DEFAULT_PAGE_SIZE, count($starInfoRespList));
        if ($index == 0) {
            $starListResp->list_name = '当前霸榜：' . $starInfoRespList[0]->star_name;
        }
        $starListResp->page_info = $pageInfoResp;


        $result->setSuccessWithResult($starListResp);
        $this->render($result);
    }
}