<?php

use facade\request\socialite\WxMiniLoginParams;
use facade\request\user\VerificationCodeParams;
use facade\request\user\WxMiniLoginBindPhoneParams;
use facade\response\Result;
use facade\response\user\JwtTokenResp;
use Firebase\JWT\JWT;
use lib\wxMini\WXBizDataCrypt;
use service\organization\impl\OrganizationCompanyAccountServiceImpl;
use service\organization\impl\OrganizationServiceImpl;
use service\socialite\impl\SocialiteServiceImpl;
use service\user\impl\UserEngineerServiceImpl;
use service\user\impl\VerificationCodeServiceImpl;
use service\user\info\JwtPayloadInfo;

class UserLogin extends FUR_Controller
{


    public function sendUserLoginCode()
    {

        /** @var VerificationCodeParams $verificationCodeParams */
        $verificationCodeParams = $this->requestObject(VerificationCodeParams::class);
        $verificationCodeParams->action = Config_Const::ACTION_ACCOUNT_LOGIN;
        $checkAccountInfoRet = $this->checkAccountInfo($verificationCodeParams);
        if (Result::isSuccess($checkAccountInfoRet) == false) {
            $this->render($checkAccountInfoRet);
        }
        $result = VerificationCodeServiceImpl::getInstance()->sendCode($verificationCodeParams);
        $this->render($result);

    }

    public function verifyUserLoginCode()
    {

        $result = new Result();
        /** @var VerificationCodeParams $verificationCodeParams */
        $verificationCodeParams = $this->requestObject(VerificationCodeParams::class);
        $verificationCodeParams->action = Config_Const::ACTION_ACCOUNT_LOGIN;
        //校验身份
        $checkAccountInfoRet = $this->checkAccountInfo($verificationCodeParams);
        if (Result::isSuccess($checkAccountInfoRet) == false) {
            $this->render($checkAccountInfoRet);
        }
        //校验
        $verificationCodeService = VerificationCodeServiceImpl::getInstance();
        $verificationCodeRet = $verificationCodeService->verifyCode($verificationCodeParams);
        if (!Result::isSuccess($verificationCodeRet)) {
            $this->render($verificationCodeRet);
        }
        $phoneNum = $verificationCodeParams->phone;
        $loginType = $verificationCodeParams->login_type;
        //获取用户信息
        if ($loginType == Config_Const::LOGIN_TYPE_STORE_MANAGER) {

        } elseif ($loginType == Config_Const::LOGIN_TYPE_ENGINEER) {
            $engineerId = $this->getEngineerId($phoneNum);
        }
        $jwtPayload = JwtPayloadInfo::buildJwtPayloadInfoV2($phoneNum, $loginType);
        $accessToken = JWT::encode($jwtPayload, \FUR_Config::get_common('jwt_key'));

        $jwtTokenResp = JwtTokenResp::buildToken($accessToken, $phoneNum);
        $result->setSuccessWithResult($jwtTokenResp);
        $this->render($result);

    }


    private function checkAccountInfo(VerificationCodeParams &$verificationCodeParams): Result
    {
        $result = new Result();
        if ($verificationCodeParams->login_type == Config_Const::LOGIN_TYPE_COMPANY) {
            $companyInfoRet = OrganizationCompanyAccountServiceImpl::getInstance()->queryCompanyByAccountName($verificationCodeParams->phone);
            if (!Result::isSuccess($companyInfoRet)) {
                $result->setSuccessWithResult(Config_Error::ERR_LOGIN_COMPANY_NOT_EXIST);
                return $result;
            }
        } elseif ($verificationCodeParams->login_type == Config_Const::LOGIN_TYPE_STORE_MANAGER) {
            $storeManagerInfoRet = OrganizationServiceImpl::getInstance()->queryStoreManagerByPhone($verificationCodeParams->phone);
            if (!Result::isSuccess($storeManagerInfoRet)) {
                $result->setSuccessWithResult(Config_Error::ERR_LOGIN_STORE_MANAGER_NOT_EXIST);
                return $result;
            }
        }
        $result->setSuccessWithResult(true);
        return $result;
    }


    private function getEngineerId($phoneNum)
    {
        $engineerInfoRet = UserEngineerServiceImpl::getInstance()->queryEngineerInfoByPhone($phoneNum);
        if (Result::isSuccess($engineerInfoRet)) {
            return $engineerInfoRet->data->id;
        }
        $registerRet = UserEngineerServiceImpl::getInstance()->registerEngineer($phoneNum);
        return $registerRet->data;

    }

    public function wxMiniLogin()
    {

        $result = new Result();
        /** @var WxMiniLoginParams $wxMiniParams */
        $wxMiniParams = $this->requestObject(WxMiniLoginParams::class);
        $encryptedData = $wxMiniParams->encryptedData;
        $iv = $wxMiniParams->iv;
        FUR_Log::debug('wxMiniLogin:socialiteParams:', json_encode($wxMiniParams));
        /**
         *openid,如果能获取到证明access_token有效
         */
        $socialiteService = SocialiteServiceImpl::getInstance();
        $sessionDataRet = $socialiteService->getWxMiniSessionData($wxMiniParams->code);
        if (!Result::isSuccess($sessionDataRet)) {
            $this->render($sessionDataRet);
        }

        $sessionData = json_decode($sessionDataRet->data);
        $sessionKey = $sessionData->session_key;

        $miniAppId = \FUR_Config::get_wx_mini_config('mini_app_id');
        $phoneData = '';
        $wXBizDataCrypt = new WXBizDataCrypt($miniAppId, $sessionKey);
        $ret = $wXBizDataCrypt->decryptData($encryptedData, $iv, $phoneData);

        FUR_Log::debug('wxMiniLogin:ret :', $ret);
        if ($ret != 0) {
            $result->setError(Config_Error::ERR_USER_WXMINI_DECODE_PHONE_FAIL);
            $this->render($result);
        }
        FUR_Log::debug('wxMiniLogin:phoneData :', ($phoneData));
        $phoneNum = json_decode($phoneData)->purePhoneNumber;
        $loginType = $wxMiniParams->login_type;
        //获取用户信息
        if ($loginType == Config_Const::LOGIN_TYPE_STORE_MANAGER) {

        } elseif ($loginType == Config_Const::LOGIN_TYPE_ENGINEER) {
            $engineerId = $this->getEngineerId($phoneNum);
        }
        $jwtPayload = JwtPayloadInfo::buildJwtPayloadInfoV2($phoneNum, $loginType);
        $accessToken = JWT::encode($jwtPayload, \FUR_Config::get_common('jwt_key'));

        $jwtTokenResp = JwtTokenResp::buildToken($accessToken, $phoneNum);
        $result->setSuccessWithResult($jwtTokenResp);
        $this->render($result);
    }

}