<?php

use business\user\UserInfoConversionUtil;
use facade\request\socialite\SocialiteParams;
use facade\request\socialite\WxMiniLoginParams;
use facade\request\user\LoginByPhoneParams;
use facade\request\user\LoginBySocialiteParams;
use facade\request\user\OneClickLoginParams;
use facade\request\user\RegisterParams;
use facade\request\user\UpdateUserInfoParams;
use facade\request\user\UpdateUserPushTokenParams;
use facade\request\user\WxMiniLoginBindPhoneParams;
use facade\response\Result;
use facade\response\user\UserSocialiteBindStatusResp;
use lib\wxMini\WXBizDataCrypt;
use model\user\dto\UserLoginInfoDTO;
use service\aliyun\AliyunBase;
use service\aliyun\impl\OneClickLoginServiceImpl;
use service\group\impl\GroupServiceImpl;
use service\order\impl\PromotionVoucherServiceImpl;
use service\socialite\drivers\WeChatSocialiteDriver;
use service\socialite\impl\SocialiteServiceImpl;
use service\user\impl\UserPushServiceImpl;
use service\user\impl\UserServiceImpl;

class UserBak extends FUR_Controller
{

    public function getUserInfo()
    {
        $result = new Result();
        $userId = $this->request('user_id');
        if ($userId == 0) {
            $result->setError(Config_Error::ERR_USER_NOT_EXIST);
            $this->render($result);
        }
        $userService = UserServiceImpl::getInstance();
        $userInfoResult = $userService->getValidUserInfo($userId);

        if ($userInfoResult == null || $userInfoResult->data == null) {
            $result->setError(Config_Error::ERR_USER_NOT_EXIST);
            $this->render($result);
        }
        $userDTO = $userInfoResult->data;
        $userInfoResp = UserInfoConversionUtil::buildSimpleUserInfo($userDTO);
        $result->setSuccessWithResult($userInfoResp);
        $this->render($result);
    }

    public function updateUserInfo()
    {
        $result = new Result();
        $userId = $this->getRequestUserId();
        if ($userId == 0) {
            $result->setError(Config_Error::ERR_USER_NOT_EXIST);
            $this->render($result);
        }
        /** @var  UpdateUserInfoParams $updateUserInfoParams */
        $updateUserInfoParams = $this->requestObject(UpdateUserInfoParams::class);
        $updateUserInfoParams->user_id = $userId;
        $updateUserInfoParams = AliyunBase::replaceCdnUrl($updateUserInfoParams);
        $userService = UserServiceImpl::getInstance();
        $result = $userService->updateUserInfo($updateUserInfoParams);
        $this->render($result);
    }

    public function updateUserPushToken()
    {
        $result = new Result();

        /** @var  UpdateUserPushTokenParams $updateUserPushTokenParams */
        $updateUserPushTokenParams = $this->requestObject(UpdateUserPushTokenParams::class);
        $userPushService = UserPushServiceImpl::getInstance();
        $result = $userPushService->updateUserPushToken($updateUserPushTokenParams);
        $this->render($result);
    }

    public function refreshToken()
    {

        $token = $this->request("refresh_token");
        $deviceId = $this->request("device_id");

        FUR_Log::info("refresh_token:" . $token);
        FUR_Log::info("device_id:" . $deviceId);
        $userService = UserServiceImpl::getInstance();
        $result = $userService->refreshToken($token, $deviceId);
        $this->render($result);
    }

    public function logout()
    {
        $userId = $this->getRequestUserId(false);
        if ($userId == 0) {
            $result = new Result();
            $result->setSuccessWithResult();
            $this->render($result);
        }
        $userService = UserServiceImpl::getInstance();
        $result = $userService->logout($userId);
        $this->render($result);
    }

    public function oneClickLogin()
    {
        /** @var OneClickLoginParams $oneClickLoginParams */
        $oneClickLoginParams = $this->requestObject(OneClickLoginParams::class);

        $accessToken = $oneClickLoginParams->access_token;
        $oneClickLoginService = OneClickLoginServiceImpl::getInstance();
        $result = $oneClickLoginService->getPhone($accessToken);
        if (!Result::isSuccess($result)) {
            $this->render($result);
        }

        $phone = $result->data;
        $userService = UserServiceImpl::getInstance();
        $queryUserIdRet = $userService->queryUserIdByPhone($phone);
        if (!Result::isSuccess($queryUserIdRet) || $queryUserIdRet->data == 0) {
            $registerParams = RegisterParams::buildUserDTOByPhone($oneClickLoginParams, $phone);
            $registerRet = $userService->register($registerParams);
            if (!Result::isSuccess($registerRet)) {
                $this->render($registerRet);
            }
        }


        $loginParams = new LoginByPhoneParams();
        $loginParams->phone = $phone;
        FUR_Core::copyProperties($oneClickLoginParams, $loginParams);
        $result = $userService->loginByPhone($loginParams);

        $this->render($result);
    }

    /**
     * 社交网站登录
     */
    public function socialiteLogin()
    {
        /** @var SocialiteParams $socialiteParams */
        $socialiteParams = $this->requestObject(SocialiteParams::class);

        /**
         *openid,如果能获取到证明access_token有效
         */
        $socialiteService = SocialiteServiceImpl::getInstance();
        $openIdRet = $socialiteService->getOpenId($socialiteParams);
        if (!Result::isSuccess($openIdRet)) {
            $this->render($openIdRet);
        }
        $openId = $openIdRet->data;
        $userService = UserServiceImpl::getInstance();
        $loginBySocialiteParams = new LoginBySocialiteParams();
        FUR_Core::copyProperties($socialiteParams, $loginBySocialiteParams);
        $loginBySocialiteParams->open_id = $openId;
        $result = $userService->loginBySocialite($loginBySocialiteParams);

        $this->render($result);
    }

    /**
     * 社交网站登录
     */
    public function socialiteRegister()
    {
        /** @var SocialiteParams $socialiteParams */
        $socialiteParams = $this->requestObject(SocialiteParams::class);

        /**
         *openid,如果能获取到证明access_token有效
         */
        $socialiteService = SocialiteServiceImpl::getInstance();
        $openIdRet = $socialiteService->getOpenId($socialiteParams);
        if (!Result::isSuccess($openIdRet)) {
            $this->render($openIdRet);
        }
        $openId = $openIdRet->data;
        $userService = UserServiceImpl::getInstance();
        $loginBySocialiteParams = new LoginBySocialiteParams();
        FUR_Core::copyProperties($socialiteParams, $loginBySocialiteParams);
        $loginBySocialiteParams->open_id = $openId;
        $result = $userService->registerBySocialite($loginBySocialiteParams);

        $this->render($result);
    }


    public function wxMiniLogin()
    {

        $result = new Result();
        /** @var WxMiniLoginParams $wxMiniParams */
        $wxMiniParams = $this->requestObject(WxMiniLoginParams::class);
        $encryptedData = $wxMiniParams->encryptedData;
        $iv = $wxMiniParams->iv;
        FUR_Log::debug('wxMiniLogin:socialiteParams:', json_encode($wxMiniParams));
        /**
         *openid,如果能获取到证明access_token有效
         */
        $socialiteService = SocialiteServiceImpl::getInstance();
        $sessionDataRet = $socialiteService->getWxMiniSessionData($wxMiniParams->code);
        if (!Result::isSuccess($sessionDataRet)) {
            $this->render($sessionDataRet);
        }

        $sessionData = json_decode($sessionDataRet->data);

        $openId = $sessionData->openid;
        $sessionKey = $sessionData->session_key;
        $userService = UserServiceImpl::getInstance();
        $loginBySocialiteParams = new LoginBySocialiteParams();
        FUR_Core::copyProperties($wxMiniParams, $loginBySocialiteParams);
        $loginBySocialiteParams->open_id = $openId;

        $miniAppId = \FUR_Config::get_wechat_pay_config('miniAppId');
        $phoneData = '';
        $wXBizDataCrypt = new WXBizDataCrypt($miniAppId, $sessionKey);
        $ret = $wXBizDataCrypt->decryptData($encryptedData, $iv, $phoneData);

        FUR_Log::debug('wxMiniLogin:ret :', $ret);
        if ($ret != 0) {
            $result->setSuccessWithResult(Config_Error::ERR_USER_WXMINI_DECODE_PHONE_FAIL);
            $this->render($result);
        }
        FUR_Log::debug('wxMiniLogin:phoneData :', ($phoneData));
        $phone = json_decode($phoneData)->purePhoneNumber;


        $queryUserIdRet = $userService->queryUserIdByPhone($phone);
        //新用户解密用户信息,用手机号注册一个,再绑定微信小程序
        if (!Result::isSuccess($queryUserIdRet) || $queryUserIdRet->data == 0) {

            $registerParams = new RegisterParams();
            FUR_Core::copyProperties($wxMiniParams, $registerParams);
            $registerParams->login_account = $phone;
            $registerParams->login_type = Config_Const::USER_LOGIN_TYPE_PHONE;
//            $registerParams->nick_name = $wxMiniParams->nick_name;
//            $registerParams->avatar = $wxMiniParams->avatar;
//            $registerParams->sex = $wxMiniParams->sex;
            $registerRet = $userService->register($registerParams);
            if (!Result::isSuccess($registerRet)) {
                $this->render($registerRet);
            }
            $userId = $registerRet->data;

            $userService->bindUserSocialite($userId, $openId, $wxMiniParams->provider);


        } else {
            $userId = $queryUserIdRet->data;
        }

        $getTokenResult = $userService->getUserLoginToken($userId, $loginBySocialiteParams);
        $this->render($getTokenResult);
    }


//    public function wxMiniLogin()
//    {
//        /** @var SocialiteParams $socialiteParams */
//        $socialiteParams = $this->requestObject(SocialiteParams::class);
//
//        FUR_Log::debug('wxMiniLogin:socialiteParams:', json_encode($socialiteParams));
//        /**
//         *openid,如果能获取到证明access_token有效
//         */
//        $socialiteService = SocialiteServiceImpl::getInstance();
//        $openIdRet = $socialiteService->getOpenId($socialiteParams);
//        if (!Result::isSuccess($openIdRet)) {
//            $this->render($openIdRet);
//        }
//        $openId = $openIdRet->data;
//        $userService = UserServiceImpl::getInstance();
//        $loginBySocialiteParams = new LoginBySocialiteParams();
//        FUR_Core::copyProperties($socialiteParams, $loginBySocialiteParams);
//        $loginBySocialiteParams->open_id = $openId;
//
//        $queryUserIdRet = $userService->queryUserIdByOpenId($socialiteParams->open_id, \Config_Const::USER_LOGIN_TYPE_WX_MINI);
//        if (!Result::isSuccess($queryUserIdRet) || $queryUserIdRet->data == 0) {
//            $registerRet = $userService->registerBySocialite($socialiteParams);
//            if (!Result::isSuccess($registerRet)) {
//                $this->render($registerRet);
//            }
//            $userId = $registerRet->data;
//        } else {
//            $userId = $queryUserIdRet->data;
//        }
//
//        $getTokenResult = $userService->getUserLoginToken($userId, $loginBySocialiteParams);
//        $this->render($getTokenResult);
//    }

    public function wxMiniLoginBindPhone()
    {
        $result = new Result();
        /** @var WxMiniLoginBindPhoneParams $requestParams */
        $requestParams = $this->requestObject(WxMiniLoginBindPhoneParams::class);

        FUR_Log::debug('wxMiniLoginBindPhone:WxMiniLoginBindPhoneParams :', json_encode($requestParams));
        $openId = $requestParams->openid;
        $encryptedData = $requestParams->encryptedData;
        $iv = $requestParams->iv;

        $sessionKey = "";
        $sessionKeyRet = SocialiteServiceImpl::getInstance()->getWxMiniSessionKey($openId);
        if (Result::isSuccess($sessionKeyRet)) {
            $sessionKey = $sessionKeyRet->data;
        }
        FUR_Log::debug('wxMiniLoginBindPhone:sessionKey :', json_encode($sessionKey));

        $miniAppId = \FUR_Config::get_wechat_pay_config('miniAppId');

        $phoneData = '';
        $wXBizDataCrypt = new WXBizDataCrypt($miniAppId, $sessionKey);
        $ret = $wXBizDataCrypt->decryptData($encryptedData, $iv, $phoneData);
        if ($ret != 0) {
            $result->setSuccessWithResult(Config_Error::ERR_USER_WXMINI_DECODE_PHONE_FAIL);
            return $result;
        }

        FUR_Log::debug('wxMiniLoginBindPhone:phoneData :', ($phoneData));
        $phone = json_decode($phoneData)->purePhoneNumber;
        /**
         * 检查手机号是否注册
         */
        $userService = UserServiceImpl::getInstance();

        $queryUserIdRet = $userService->queryUserIdByPhone($phone);

        /**
         * 没注册的话就先去注册
         */
        $userId = 0;
        if (Result::isSuccess($queryUserIdRet) && $queryUserIdRet->data != 0) {
            $userId = $queryUserIdRet->data;
        } else {
            $userService = UserServiceImpl::getInstance();
            $registerParams = RegisterParams::buildUserDTOByPhone($requestParams, $phone);
            $registerRet = $userService->register($registerParams);
            if (!Result::isSuccess($registerRet)) {
                $this->render($registerRet);
            }
            $userId = $registerRet->data;
        }
        if ($userId == 0) {
            $result->setError(Config_Error::ERR_USER_SOCIALITE_REGISTER_FAIL);
            $this->render($result);
        }

        /**
         * login
         */
        $getTokenResult = $userService->getUserLoginToken($userId, $requestParams);
        $this->render($getTokenResult);
    }


    public function socialiteBind()
    {
        $result = new Result();
        /** @var SocialiteParams $socialiteParams */
        $socialiteParams = $this->requestObject(SocialiteParams::class);

        $userId = $this->getRequestUserId();

        /** 查看这个社交账号是否绑定过其他账号 */
        $userService = UserServiceImpl::getInstance();
        $userIdRet = $userService->queryUserIdBySocialite($socialiteParams->open_id, $socialiteParams->provider);
        if (Result::isSuccess($userIdRet) && $userIdRet->data != 0) {
            $result->setError(Config_Error::ERR_USER_SOCIALITE_HAS_BIND);
            $this->render($result);
        }

        $provider = $socialiteParams->provider;

        $socialiteService = SocialiteServiceImpl::getInstance();
        $openIdRet = $socialiteService->getOpenId($socialiteParams);
        if (!Result::isSuccess($openIdRet)) {
            $this->render($openIdRet);
        }
        $openId = $openIdRet->data;
        $userService = UserServiceImpl::getInstance();
        $bindRet = $userService->bindUserSocialite($userId, $openId, $provider);

        $this->render($bindRet);
    }


    public function getWechatOpenId()
    {
        $code = $this->request('code');
        $driver = new WeChatSocialiteDriver();
        $platform = $this->request('platform');
        $result = $driver->getOpenIdByCode($code, $platform);

        $this->render($result);
    }
}