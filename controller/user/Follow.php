<?php

use facade\request\maintain\CreateLeaderboardRecordParams;
use facade\request\user\FollowParams;
use facade\response\Result;
use lib\AppConstant;
use service\group\impl\GroupServiceImpl;
use service\user\impl\FollowServiceImpl;
use \service\maintain\impl\MaintainLeaderboardServiceImpl;

class Follow extends FUR_Controller
{
    private int $userId = 0;

    public function __construct()
    {
        $userId = $this->getRequestUserId(true);
        $this->userId = $userId;
    }

    public function follow()
    {
        /** @var FollowParams $followParams */
        $followParams = $this->requestObject(FollowParams::class);
        $followParams->user_id = $this->userId;
        $followService = FollowServiceImpl::getInstance();
        $result = $followService->follow($followParams);
        $this->render($result);
    }

    public function unFollow()
    {
        $result = new Result();
        /** @var FollowParams $followParams */
        $followParams = $this->requestObject(FollowParams::class);
        $followParams->user_id = $this->userId;
        $followService = FollowServiceImpl::getInstance();

        /**
         * 检查是不是圈子管理员
         */
        if ($followParams->target_type == AppConstant::BIZ_TYPE_STAR) {
            $groupManagerTypeRet = GroupServiceImpl::getInstance()->queryUserManagerType($followParams->user_id, $followParams->target_id);
            if (Result::isSuccess($groupManagerTypeRet) && $groupManagerTypeRet->data != 0) {
                $result->setError(Config_Error::ERR_FOLLOW_GROUP_OWNER_CANT_UNFOLLOW);
                $this->render($result);
            }

        }


        $result = $followService->unFollow($followParams);
        $this->render($result);
    }

    public function getUserFocusList()
    {
        $followService = FollowServiceImpl::getInstance();
        $followParams = new FollowParams();
        $followParams->user_id = $this->userId;
        $result = $followService->getUserFocusList($this->getRequestUserId());
        $this->render($result);
    }
}