<?php

use facade\request\user\FeedbackParams;
use service\user\impl\FeedbackServiceImpl;

class Feedback extends FUR_Controller
{
    public function __construct()
    {

    }

    public function sendFeedback()
    {
        $feedbackParams = $this->requestObject(FeedbackParams::class);
        $feedbackService = FeedbackServiceImpl::getInstance();
        $result = $feedbackService->sendFeedback($feedbackParams);
        $this->render($result);
    }


}