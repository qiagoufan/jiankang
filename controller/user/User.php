<?php

use business\user\UserInfoConversionUtil;
use facade\request\socialite\SocialiteParams;
use facade\request\user\LoginBySocialiteParams;
use facade\request\user\RegisterParams;
use facade\request\user\UpdateUserInfoParams;
use facade\request\user\WxMiniLoginBindPhoneParams;
use facade\response\Result;
use lib\wxMini\WXBizDataCrypt;
use model\user\dto\UserInviteRecordDTO;
use service\promotion\impl\PromotionVoucherServiceImpl;
use service\socialite\impl\SocialiteServiceImpl;
use service\user\impl\UserInviteServiceImpl;
use service\user\impl\UserServiceImpl;

class User extends FUR_Controller
{

    public function getUserInfo()
    {
        $result = new Result();
        $userId = $this->getRequestUserId();
        if ($userId == 0) {
            $result->setError(Config_Error::ERR_USER_NOT_EXIST);
            $this->render($result);
        }
        $userService = UserServiceImpl::getInstance();
        $userInfoResult = $userService->getValidUserInfo($userId);

        if ($userInfoResult == null || $userInfoResult->data == null) {
            $result->setError(Config_Error::ERR_USER_NOT_EXIST);
            $this->render($result);
        }
        $userDTO = $userInfoResult->data;
        $userInfoResp = UserInfoConversionUtil::getInstance()->conversionUserInfo($userDTO);
        $result->setSuccessWithResult($userInfoResp);
        $this->render($result);
    }

    public function getProfessionCategory()
    {
        $result = new Result();
        $data = ['党政机关工作人员', '企/事业单位负责人', '军人', '个体工商户', '农民', '学生', '技术人员', '生产、运输设备操作人员', '无业', '其他'];
        $result->setSuccessWithResult($data);
        $this->render($result);
    }

    public function getEducationCategory()
    {
        $result = new Result();
        $data = ['博士', '硕士', '本科', '专科', '中专/高中', '初中', '小学'];
        $result->setSuccessWithResult($data);
        $this->render($result);
    }

    public function getInsuranceTypeList()
    {
        $result = new Result();
        $data = ['一类医保', '二类医保', '农村医保', '无'];
        $result->setSuccessWithResult($data);
        $this->render($result);
    }

    public function updateUserInfo()
    {
        $result = new Result();
        $userId = $this->getRequestUserId();
        if ($userId == 0) {
            $result->setError(Config_Error::ERR_USER_NOT_EXIST);
            $this->render($result);
        }
        /** @var  UpdateUserInfoParams $updateUserInfoParams */
        $updateUserInfoParams = $this->requestObject(UpdateUserInfoParams::class);
        $updateUserInfoParams->user_id = $userId;
        $userService = UserServiceImpl::getInstance();
        $result = $userService->updateUserInfo($updateUserInfoParams);
        $this->render($result);
    }


    public function wxMiniLogin()
    {
        /** @var SocialiteParams $socialiteParams */
        $socialiteParams = $this->requestObject(SocialiteParams::class);

        FUR_Log::debug('wxMiniLogin:socialiteParams:', json_encode($socialiteParams));
        /**
         *openid,如果能获取到证明access_token有效
         */
        $socialiteService = SocialiteServiceImpl::getInstance();
        $openIdRet = $socialiteService->getOpenId($socialiteParams);
        if (!Result::isSuccess($openIdRet)) {
            $this->render($openIdRet);
        }
        $openId = $openIdRet->data;
        $userService = UserServiceImpl::getInstance();
        $socialiteParams->open_id = $openId;

        $queryUserIdRet = $userService->queryUserIdByOpenId($openId, \Config_Const::USER_LOGIN_TYPE_WX_MINI);
        if (!Result::isSuccess($queryUserIdRet) || $queryUserIdRet->data == 0) {
            $registerRet = $userService->registerBySocialite($socialiteParams);
            if (!Result::isSuccess($registerRet)) {
                $this->render($registerRet);
            }
            $userId = $registerRet->data;

            $this->inviteUser($socialiteParams->invite_code, $userId);
        } else {
            $userId = $queryUserIdRet->data;
        }

        $getTokenResult = $userService->getUserLoginToken($userId, $socialiteParams);
        $this->render($getTokenResult);
    }

    private function inviteUser($invite_code, $userId)
    {
        if (!$invite_code) {
            return;
        }
        $inviteUserIdRet = UserServiceImpl::getInstance()->queryUserIdByInviteCode($invite_code);

        $inviteUserId = $inviteUserIdRet->data;
        $userInviteRecordDTO = new    UserInviteRecordDTO();
        $userInviteRecordDTO->user_id = $inviteUserId;
        $userInviteRecordDTO->invite_user_id = $userId;
        $userInviteRecordDTO->created_timestamp = time();
        UserInviteServiceImpl::getInstance()->insertUserInviteRecord($userInviteRecordDTO);
        //发券
        $result = PromotionVoucherServiceImpl::getInstance()->distributeVoucher($inviteUserId, -1);

    }

    public function wxMiniLoginBindPhone()
    {
        $result = new Result();
        /** @var WxMiniLoginBindPhoneParams $requestParams */
        $requestParams = $this->requestObject(WxMiniLoginBindPhoneParams::class);

        FUR_Log::debug('wxMiniLoginBindPhone:WxMiniLoginBindPhoneParams :', json_encode($requestParams));

        $socialiteService = SocialiteServiceImpl::getInstance();
        $sessionDataRet = $socialiteService->getWxMiniSessionData($requestParams->code);
        if (!Result::isSuccess($sessionDataRet)) {
            $this->render($sessionDataRet);
        }

        $sessionData = json_decode($sessionDataRet->data);

        $openId = $sessionData->openid;
        $sessionKey = $sessionData->session_key;
        $encryptedData = $requestParams->encryptedData;
        $iv = $requestParams->iv;

        FUR_Log::debug('wxMiniLoginBindPhone:sessionKey :', json_encode($sessionKey));

        $miniAppId = \FUR_Config::get_wechat_pay_config('miniAppId');

        $phoneData = '';
        $wXBizDataCrypt = new WXBizDataCrypt($miniAppId, $sessionKey);
        $ret = $wXBizDataCrypt->decryptData($encryptedData, $iv, $phoneData);
        if ($ret != 0) {
            $result->setError(Config_Error::ERR_USER_WXMINI_DECODE_PHONE_FAIL);
            $this->render($result);
        }

        FUR_Log::debug('wxMiniLoginBindPhone:phoneData :', ($phoneData));
        $phone = json_decode($phoneData)->purePhoneNumber;
        /**
         * 检查手机号是否注册
         */
        $userService = UserServiceImpl::getInstance();

        $queryUserIdRet = $userService->queryUserIdByPhone($phone);

        /**
         * 注册过的话返回错误
         */
        if (Result::isSuccess($queryUserIdRet) && $queryUserIdRet->data != 0) {
            $result->setError(Config_Error::ERR_USER_PHONE_HAS_BIND);
            $this->render($result);
        }
        $userService = UserServiceImpl::getInstance();
        $registerParams = new RegisterParams();
        $registerParams->login_account = $phone;
        $registerParams->login_type = Config_Const::USER_LOGIN_TYPE_PHONE;
        $registerParams->user_id = $this->getRequestUserId();
        $registerParams->created_timestamp = time();
        $userService->bindUserNewLoginInfo($registerParams);

        /**
         * 返回绑定结果
         */
        $result->setSuccessWithResult(true);
        $this->render($result);
    }


}