<?php

use business\user\UserInfoConversionUtil;
use facade\request\feed\ConversionFeedParams;
use facade\request\user\GetUserHomePageListParams;
use facade\request\user\QueryUserLikeListParams;
use facade\response\base\PageInfoResp;
use facade\response\Result;
use facade\response\user\UserHostHomeInfoResp;
use model\dto\FeedInfoDTO;
use model\dto\UserDTO;
use service\feed\impl\FeedInfoRespServiceImpl;
use service\feed\impl\FeedServiceImpl;
use service\user\impl\UserServiceImpl;
use facade\request\user\FollowParams;
use service\user\impl\FollowServiceImpl;
use \service\interactive\impl\InteractiveServiceImpl;
use \model\dto\InteractiveLikeDTO;

class UserHomePage extends FUR_Controller
{
    const DEFAULT_PAGE_SIZE = 10;
    private int $userId = 0;

    public function __construct()
    {
        $userId = $this->getRequestUserId(false);
        $this->userId = $userId;
    }


    public function getHostHomeInfo()
    {
        $result = new Result();
        /** @var GetUserHomePageListParams $getUserHomePageListParams */
        $getUserHomePageListParams = $this->requestObject(GetUserHomePageListParams::class);
        $visitUserId = $getUserHomePageListParams->visit_user_id;
        $index = $getUserHomePageListParams->index;
        $userId = $this->userId;
        $visitType = $getUserHomePageListParams->visit_type;

        if ($visitUserId == 0) {
            $result->setError(Config_Error::ERR_VISIT_USER_IS_NOT_EXIST);
            $this->render($result);
        }

        if ($visitType == null) {
            $result->setError(Config_Error::ERR_VISIT_USER_LIST_TYPE_IS_NOT_EXIST);
            $this->render($result);
        }

        /**
         * 获取用户信息
         */
        $userInfoRet = UserServiceImpl::getInstance()->getValidUserInfo($visitUserId);
        if (!Result::isSuccess($userInfoRet)) {
            $result->setError(Config_Error::ERR_VISIT_USER_IS_NOT_EXIST);
            $this->render($result);
        }
        $userDTO = $userInfoRet->data;
        $userInfoResp = UserInfoConversionUtil::getInstance()->conversionUserInfo($userDTO);


        $homeInfoList = $this->getUserFeedList($getUserHomePageListParams);

        $pageInfo = PageInfoResp::buildPageInfoRespBaseLine($index, self::DEFAULT_PAGE_SIZE, count($homeInfoList));
        $userHostHomeInfoResp = new UserHostHomeInfoResp();
        $userHostHomeInfoResp->page_info = $pageInfo;
        $userHostHomeInfoResp->home_feed_info_list = $homeInfoList;
        $userHostHomeInfoResp->visit_user_info = $userInfoResp;

        $userHostHomeInfoResp->block_status = 0;
        if ($visitUserId != $userId) {
            $blockStatusRet = UserServiceImpl::getInstance()->isBlock($userId, $visitUserId);
            if (Result::isSuccess($blockStatusRet)) {
                $userHostHomeInfoResp->block_status = intval($blockStatusRet->data);
            }
        }

        $result->setSuccessWithResult($userHostHomeInfoResp);
        $this->render($result);
    }


    private function getUserFeedList(GetUserHomePageListParams $getUserHomePageListParams): ?array
    {
        /**
         * 场景参数
         */
        $personalForumFeedList = [];
        /** @var ConversionFeedParams $conversionFeedParams */
        $conversionFeedParams = $this->requestObject(ConversionFeedParams::class);
        $conversionFeedParams->scene = FeedInfoRespServiceImpl::SCENE_USER_HOME_PAGE;
        if ($getUserHomePageListParams->visit_type == GetUserHomePageListParams::POST_FEED) {

            $personalForumFeedList = $this->getUserPostFeedList($getUserHomePageListParams, $conversionFeedParams);

        } elseif ($getUserHomePageListParams->visit_type == GetUserHomePageListParams::LIKE_FEED) {

            $personalForumFeedList = $this->getUserLikeFeedList($getUserHomePageListParams, $conversionFeedParams);
        }


        return $personalForumFeedList;
    }


    private function getUserLikeFeedList(GetUserHomePageListParams $getUserHomePageListParams, &$conversionFeedParams): ?array
    {
        $personalForumFeedList = [];
        $interactiveService = InteractiveServiceImpl::getInstance();
        $feedInfoRespService = FeedInfoRespServiceImpl::getInstance();
        $feedService = FeedServiceImpl::getInstance();
        $queryUserLikeListParams = new QueryUserLikeListParams();
        \FUR_Core::copyProperties($getUserHomePageListParams, $queryUserLikeListParams);
        $queryUserLikeListParams->user_id = $getUserHomePageListParams->visit_user_id;
        $userLikeListRet = $interactiveService->queryUserLikeList($queryUserLikeListParams);

        if (!Result::isSuccess($userLikeListRet)) {
            return $personalForumFeedList;
        } else {
            /** @var InteractiveLikeDTO $interactiveLikeDTO */
            foreach ($userLikeListRet->data as $interactiveLikeDTO) {

                if ($interactiveLikeDTO->like_biz_type == FeedInfoDTO::FEED_BIZ_TYPE) {
                    $feedId = $interactiveLikeDTO->like_biz_id;
                    $feedInfoRet = $feedService->getFeedDetail($feedId);
                    if (!Result::isSuccess($feedInfoRet)) {
                        continue;
                    } else {
                        $feedInfoDTO = $feedInfoRet->data;
                        /**过滤掉删除状态*/
                        if ($feedInfoDTO->status == FeedInfoDTO::FEED_STATUS_DELETE) {
                            continue;
                        }
                        /**客态不可以看到未过审信息*/
                        if ($getUserHomePageListParams->user_id != $getUserHomePageListParams->visit_user_id && $feedInfoDTO->status != FeedInfoDTO::FEED_STATUS_NORMAL) {
                            continue;
                        } else {

                            $feedInfoResp = $feedInfoRespService->conversionFeedInfoResp($feedInfoDTO, $conversionFeedParams);
                            if ($feedInfoResp == null) {
                                continue;
                            }
                            array_push($personalForumFeedList, $feedInfoResp);

                        }
                    }
                }
            }
        }

        return $personalForumFeedList;
    }


    private function getUserPostFeedList(GetUserHomePageListParams $getUserHomePageListParams, &$conversionFeedParams): ?array
    {
        $personalForumFeedList = [];
        $feedService = FeedServiceImpl::getInstance();
        $feedInfoRespService = FeedInfoRespServiceImpl::getInstance();
        $feedListRet = $feedService->getUserPostFeedList($getUserHomePageListParams->visit_user_id, $getUserHomePageListParams->index, self::DEFAULT_PAGE_SIZE);
        foreach ($feedListRet->data as $value) {
            /**客态不可以看到未过审信息*/
            if ($getUserHomePageListParams->user_id != $getUserHomePageListParams->visit_user_id && $value->status != FeedInfoDTO::FEED_STATUS_NORMAL) {
                continue;
            } else {
                $feedInfoResp = $feedInfoRespService->conversionFeedInfoResp($value, $conversionFeedParams);
                if ($feedInfoResp == null) {
                    continue;
                }
                array_push($personalForumFeedList, $feedInfoResp);
            }
        }

        return $personalForumFeedList;
    }
}