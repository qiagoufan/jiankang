<?php

use business\user\UserInfoConversionUtil;
use facade\request\feed\ConversionFeedParams;
use facade\request\user\GetUserHomePageListParams;
use facade\response\base\PageInfoResp;
use facade\response\Result;
use facade\response\user\UserHostHomeInfoResp;
use model\dto\FeedInfoDTO;
use model\dto\UserDTO;
use service\feed\impl\FeedInfoRespServiceImpl;
use service\feed\impl\FeedServiceImpl;
use service\user\impl\UserServiceImpl;
use facade\request\user\FollowParams;
use service\user\impl\FollowServiceImpl;

class UserBlock extends FUR_Controller
{
    const DEFAULT_PAGE_SIZE = 10;
    private int $userId = 0;

    public function __construct()
    {
        $userId = $this->getRequestUserId(false);
        $this->userId = $userId;
    }


    public function blockUser()
    {
        $blockUserId = $this->request('block_user_id', 0);
        $userId = $this->getRequestUserId();
        $result = UserServiceImpl::getInstance()->blockUser($userId, $blockUserId);
        $this->render($result);
    }

    public function unblockUser()
    {
        $blockUserId = $this->request('block_user_id', 0);
        $userId = $this->getRequestUserId();
        $result = UserServiceImpl::getInstance()->unblockUser($userId, $blockUserId);
        $this->render($result);
    }

    public function getBlockUserList()
    {
        $result = new Result();
        $userId = $this->getRequestUserId();
        $blockUserIdListRet = UserServiceImpl::getInstance()->getBlockUserIdList($userId);
        $userList = [];
        if (Result::isSuccess($blockUserIdListRet)) {
            foreach ($blockUserIdListRet->data as $userId) {
                $userInfoRet = UserServiceImpl::getInstance()->getValidUserInfo($userId);
                if (Result::isSuccess($userInfoRet)) {
                    array_push($userList, $userInfoRet->data);
                } else {
                    $userDTO = UserDTO::buildInvalidUserDTO($userId);
                    array_push($userList, $userDTO);
                }
            }
        }
        $result->setSuccessWithResult($userList);
        $this->render($result);
    }



}