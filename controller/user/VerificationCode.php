<?php

use facade\request\socialite\SocialiteParams;
use facade\request\user\LoginByPhoneParams;
use facade\request\user\RegisterParams;
use  \facade\request\user\VerificationCodeParams;
use facade\request\user\VerificationSocialiteCodeParams;
use facade\response\Result;
use lib\AppCheckUtil;
use lib\PhoneHelper;
use model\dto\UserDTO;
use service\socialite\impl\SocialiteServiceImpl;
use service\user\impl\UserServiceImpl;
use service\user\UserService;
use service\user\impl\VerificationCodeServiceImpl;

class VerificationCode extends FUR_Controller
{

    public function sendCode()
    {
        /** @var VerificationCodeParams $verificationCodeParams */
        $verificationCodeParams = $this->requestObject(VerificationCodeParams::class);
        $verificationCodeService = VerificationCodeServiceImpl::getInstance();
        $result = $verificationCodeService->sendCode($verificationCodeParams);
        $this->render($result);
    }

    /**
     * 直接用手机登录
     */
    public function verifyLoginCode()
    {
        /** @var VerificationCodeParams $verificationCodeParams */
        $verificationCodeParams = $this->requestObject(VerificationCodeParams::class);

        $verificationCodeService = VerificationCodeServiceImpl::getInstance();
        $result = $verificationCodeService->verifyCode($verificationCodeParams);
        if ($result->errorCode != 0) {
            $this->render($result);
        }
        $userService = UserServiceImpl::getInstance();
        /**
         * 查看手机号是否注册过用户,如果没有注册过先注册
         */
        $userId = 0;
        $queryUserIdRet = $userService->queryUserIdByPhone($verificationCodeParams->phone);
        if (!Result::isSuccess($queryUserIdRet) || $queryUserIdRet->data == 0) {
            $registerRet = $this->registerByPhone($verificationCodeParams);
            if (!Result::isSuccess($registerRet)) {
                $this->render($registerRet);
            }
            $userId = $registerRet->data;
        } else {
            $userId = $queryUserIdRet->data;
        }
        $loginParams = new LoginByPhoneParams();
        FUR_Core::copyProperties($verificationCodeParams, $loginParams);
        $verificationCodeService->deleteCode($verificationCodeParams->phone, $verificationCodeParams->code  );

        $getTokenResult = $userService->getUserLoginToken($userId, $loginParams);

        $this->render($getTokenResult);

    }

    /**
     * 通过第三方账号绑定登录
     */
    public function verifySocialiteLoginCode()
    {
        /**
         * 判断验证码
         */

        /** @var VerificationSocialiteCodeParams $verificationCodeParams */
        $verificationCodeParams = $this->requestObject(VerificationSocialiteCodeParams::class);
        $verificationCodeService = VerificationCodeServiceImpl::getInstance();
        $result = $verificationCodeService->verifyCode($verificationCodeParams);
        if ($result->errorCode != 0) {
            $this->render($result);
        }

        /**
         *获取 open_id,如果能获取到证明access_token有效
         */
        $socialiteService = SocialiteServiceImpl::getInstance();

        $socialiteParams = new SocialiteParams();
        FUR_Core::copyProperties($verificationCodeParams, $socialiteParams);
        $openIdRet = $socialiteService->getOpenId($socialiteParams);
        if (!Result::isSuccess($openIdRet)) {
            $this->render($openIdRet);
        }

        $openId = $openIdRet->data;
        $provider = $verificationCodeParams->provider;
        /**
         * 检查手机号是否注册
         */
        $userService = UserServiceImpl::getInstance();

        $queryUserIdRet = $userService->queryUserIdByPhone($verificationCodeParams->phone);

        /**
         * 没注册的话就先去注册
         */
        $userId = 0;
        if (Result::isSuccess($queryUserIdRet) && $queryUserIdRet->data != 0) {
            $userId = $queryUserIdRet->data;
        } else {
            $registerRet = $this->registerBySocialite($verificationCodeParams);
            if (!Result::isSuccess($registerRet)) {
                $this->render($registerRet);
            }
            $userId = $registerRet->data;
        }
        if ($userId == 0) {
            $result->setError(Config_Error::ERR_USER_SOCIALITE_REGISTER_FAIL);
            $this->render($result);
        }
        /** 查看用户是否已经绑定过社交账号 */
        $bindStatusRet = $userService->querySocialiteBindStatus($userId, $provider);
        if (Result::isSuccess($bindStatusRet) && $bindStatusRet->data == true) {
            $result->setError(Config_Error::ERR_USER_HAS_BIND_SOCIALITE_ACCOUNT);
            $this->render($result);
        }

        /** 查看这个社交账号是否绑定过其他账号 */
        $userIdRet = $userService->queryUserIdBySocialite($openId, $provider);
        if (Result::isSuccess($userIdRet) && $userIdRet->data != 0) {
            $result->setError(Config_Error::ERR_USER_SOCIALITE_HAS_BIND);
            $this->render($result);
        }
        /**
         * 绑定
         */
        $bindRet = $userService->bindUserSocialite($userId, $openId, $provider);
        if (!Result::isSuccess($bindRet)) {
            $this->render($bindRet);
        }

        /**
         * login
         */

        $loginByPhoneParams = new LoginByPhoneParams();
        FUR_Core::copyProperties($verificationCodeParams, $loginByPhoneParams);
        $loginByPhoneParams->source = $provider;

        $result = $userService->loginByPhone($loginByPhoneParams);

        $this->render($result);

    }

    /**
     * 已有账号绑定手机
     */
    public function verifyLoginCodeBindPhone()
    {
        $userId = $this->getRequestUserId();
        /** @var VerificationCodeParams $verificationCodeParams */
        $verificationCodeParams = $this->requestObject(VerificationCodeParams::class);

        $verificationCodeService = VerificationCodeServiceImpl::getInstance();
        $result = $verificationCodeService->verifyCode($verificationCodeParams);
        if ($result->errorCode != 0) {
            $this->render($result);
        }
        $userService = UserServiceImpl::getInstance();
        $result = $userService->bindPhone($userId, $verificationCodeParams->phone);

        $this->render($result);

    }

    private function registerBySocialite(VerificationSocialiteCodeParams $socialiteCodeParams)
    {
        $userService = UserServiceImpl::getInstance();

        $registerParams = new RegisterParams();
        $registerParams->phone = $socialiteCodeParams->phone;
        $registerParams->sex = intval($socialiteCodeParams->sex);
        $registerParams->created_timestamp = time();

        $registerParams->nick_name = trim($socialiteCodeParams->nick_name);
        $getVaildNickNameRet = UserServiceImpl::getInstance()->formatValidSocialiteNickName($registerParams->nick_name);
        if (Result::isSuccess($getVaildNickNameRet)) {
            $registerParams->nick_name = $getVaildNickNameRet->data;
        }


        $registerParams->phone_verified_timestamp = time();
        $registerParams->user_type = UserDTO::USER_TYPE_NORMAL;
        $registerParams->status = UserDTO::USER_STATUS_VALID;
        $registerParams->avatar = $socialiteCodeParams->avatar;
        $registerParams->platform = $socialiteCodeParams->platform;
        $registerParams->device_id = $socialiteCodeParams->device_id;
        return $userService->register($registerParams);
    }

    private function registerByPhone(VerificationCodeParams $phoneCodeParams)
    {
        $userService = UserServiceImpl::getInstance();

        $registerParams = RegisterParams::buildUserDTOByPhone($phoneCodeParams, $phoneCodeParams->phone);
        return $userService->register($registerParams);
    }

}