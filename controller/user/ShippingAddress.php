<?php


use facade\request\user\ShippingAddressParams;
use facade\response\base\DataListResp;
use facade\response\base\PageInfoResp;
use facade\response\Result;
use facade\response\user\shippingAddress\ShippingAddressListResp;
use facade\response\user\shippingAddress\ShippingAddressResp;
use model\user\dto\UserShippingAddressDTO;
use service\user\impl\ShippingAddressServiceImpl;

class ShippingAddress extends FUR_Controller
{
    public $userId;

    public function __construct()
    {
        $userId = $this->getRequestUserId();
        $this->userId = $userId;
    }

    public function addShippingAddress()
    {
        /** @var ShippingAddressParams $shippingAddressParams */
        $shippingAddressParams = $this->requestObject(ShippingAddressParams::class);

        $shippingAddressParams->id = null;
        $shippingAddressParams->user_id = $this->userId;
        $shippingAddressService = ShippingAddressServiceImpl::getInstance();
        $result = $shippingAddressService->createShippingAddress($shippingAddressParams);
        $this->render($result);
    }

    public function updateShippingAddress()
    {
        /** @var ShippingAddressParams $shippingAddressParams */
        $shippingAddressParams = $this->requestObject(ShippingAddressParams::class);

        $shippingAddressParams->user_id = $this->userId;
        $shippingAddressService = ShippingAddressServiceImpl::getInstance();
        $result = $shippingAddressService->updateShippingAddress($shippingAddressParams);
        $this->render($result);
    }

    public function getShippingAddressList()
    {

        $result = new Result();
        $result->setSuccessWithResult();
        $shippingAddressService = ShippingAddressServiceImpl::getInstance();
        $addressListRet = $shippingAddressService->getUserShippingAddressList($this->userId);
        $shippingAddressListResp = new  DataListResp();
        if (Result::isSuccess($addressListRet)) {
            /** @var UserShippingAddressDTO $shippingAddressDTO */


            $shippingAddressList = [];
            foreach ($addressListRet->data as $shippingAddressDTO) {
                $shippingAddressResp = new ShippingAddressResp();
                FUR_Core::copyProperties($shippingAddressDTO, $shippingAddressResp);
                array_push($shippingAddressList, $shippingAddressResp);
            }

            $shippingAddressListResp->list = $shippingAddressList;

            $pageInfo = new PageInfoResp();
            $pageInfo->page_size = count($shippingAddressList);
            $pageInfo->has_more = false;
            $pageInfo->index = 1;
            $shippingAddressListResp->page_info = $pageInfo;


        }
        $result->setSuccessWithResult($shippingAddressListResp);
        $this->render($result);

    }

    public function getShippingAddressDetail()
    {
        $result = new Result();
        $shippingAddressService = ShippingAddressServiceImpl::getInstance();
        $shippingId = intval($this->request('shipping_id'));
        $shippingAddressDetailRet = $shippingAddressService->getShippingAddressDetail($this->userId, $shippingId);

        if ($shippingAddressDetailRet->errorCode == 0 && $shippingAddressDetailRet->data != null) {
            /** @var UserShippingAddressDTO $shippingAddressDTO */
            $shippingAddressResp = new  ShippingAddressResp();
            FUR_Core::copyProperties($shippingAddressDetailRet->data, $shippingAddressResp);
            $result->setSuccessWithResult($shippingAddressResp);
        }

        $this->render($result);
    }

    public function deleteShippingAddress()
    {
        $shippingId = intval($this->request('shipping_id'));
        $shippingAddressService = ShippingAddressServiceImpl::getInstance();
        $result = $shippingAddressService->deleteShippingAddress($this->userId, $shippingId);
        $this->render($result);
    }

}