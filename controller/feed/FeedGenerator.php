<?php


use facade\request\feed\CreateFeedParams;
use facade\request\feed\creation\CreateJkFeedParams;
use facade\request\feed\creation\FeedContent;
use facade\response\Result;
use model\feed\dto\FeedInfoDTO;
use service\feed\impl\FeedServiceImpl;
use service\security\impl\SecurityCheckServiceImpl;
use service\user\impl\UserServiceImpl;

class FeedGenerator extends FUR_Controller
{

    private $userId;
    const DEFAULT_PAGE_SIZE = 10;

    public function __construct()
    {
        $this->userId = $this->getRequestUserId(false);
    }


    public function createFeed()
    {
        $result = new Result();
        $userService = UserServiceImpl::getInstance();
//        $userInfoRet = $userService->getValidUserInfo($this->userId);
//        if (!Result::isSuccess($userInfoRet)) {
//            $result->setError(\Config_Error::ERR_USER_NOT_EXIST);
//            $this->render($result);
//        }


        /** @var CreateJkFeedParams $createFeedParams */
        $createFeedParams = $this->requestObject(CreateJkFeedParams::class);
        $feedId = $this->insertFeed($createFeedParams);
        $result->setSuccessWithResult($feedId);
        $this->render($result);

    }

    public function deleteUserFeed()
    {
        $result = new Result();
        $feedId = $this->request('feed_id', 0);

        if ($this->userId == 0) {
            $result->setError(Config_Error::ERR_DELETE_FEED_USER_NOT_LOGIN);
            $this->render($result);
        }
        $feedService = FeedServiceImpl::getInstance();
        //查询feed详情,然后比对author
        $feedInfoRet = $feedService->getFeedDetail($feedId);
        if (!Result::isSuccess($feedInfoRet)) {
            $result->setError(Config_Error::ERR_DELETE_FEED_NOT_EXIST);
            $this->render($result);
        }
        /** @var FeedInfoDTO $feedInfo */
        $feedInfo = $feedInfoRet->data;
        if ($this->userId != $feedInfo->author_id) {
            $userServiceImpl = UserServiceImpl::getInstance();
            $userWhiteRet = $userServiceImpl->queryWhiteUserById($this->userId);
            if (!Result::isSuccess($userWhiteRet)) {
                $result->setError(Config_Error::ERR_DELETE_FEED_NO_RIGHT);
                $this->render($result);
            }

        }
        $deleteRet = $feedService->deleteFeed($feedId);
        $this->render($deleteRet);

    }


    /**
     * 获取发送Feed标签列表
     */
    public function getFeedCategoryList()
    {
        $result = FeedServiceImpl::getInstance()->getFeedCategoryList();
        $this->render($result);
    }


    private function insertFeed(CreateJkFeedParams $createJkFeedParams)
    {
        /**
         * 数据校验
         */
        $result = new Result();

//        $userCheckRet = UserServiceImpl::getInstance()->bindPhoneCheck($this->userId);
//        if (!Result::isSuccess($userCheckRet)) {
//            $this->render($userCheckRet);
//        }
        if ($createJkFeedParams == null) {
            $result->setError(Config_Error::ERR_CREATE_FEED_PARAMS_IS_NULL);
            $this->render($result);
        }
        if ($createJkFeedParams->feed_content == null) {
            $result->setError(Config_Error::ERR_CREATE_FEED_CONTENT_IS_NULL);
            $this->render($result);
        }

        if ($createJkFeedParams->feed_title == null) {
            $result->setError(Config_Error::ERR_CREATE_FEED_TITLE_IS_NULL);
            $this->render($result);
        }

        if (mb_strlen($createJkFeedParams->feed_title) > 20) {
            $result->setError(Config_Error::ERR_CREATE_FEED_TITLE_IS_TOO_LONG);
            $this->render($result);
        }

        $checkRet = SecurityCheckServiceImpl::getInstance()->checkContent($createJkFeedParams->feed_title);
        if (!Result::isSuccess($checkRet)) {
            $this->render($checkRet);
        }
        $checkRet = SecurityCheckServiceImpl::getInstance()->checkContent($createJkFeedParams->feed_content);
        if (!Result::isSuccess($checkRet)) {
            $this->render($checkRet);
        }

        /**
         *组装元素
         */
        $imageList = $createJkFeedParams->image_list;
        if (is_string($imageList)) {
            $imageList = json_decode($imageList);
        }
        $template_type = Config_Const::FEED_TYPE_NORMAL;


        /**
         * 构建feed content
         */
        $feedContent = new FeedContent();
        $feedContent->image_list = $imageList;
        $feedContent->content_desc = $createJkFeedParams->feed_content;
        $feedContent->video_link = $createJkFeedParams->video_link;
        $feedContent->content_title = $createJkFeedParams->feed_title;
        $feedContent->content_type = 'feed';


        $createFeedParams = new  CreateFeedParams();
        FUR_Core::copyProperties($createJkFeedParams, $createFeedParams);
        if (!empty($imageList)) {
            $createFeedParams->cover_url = $imageList[0];
        }
        $createFeedParams->feed_type = Config_Const::FEED_TYPE_NORMAL;
        $createFeedParams->feed_title = $createJkFeedParams->feed_title;
        $createFeedParams->template_type = $template_type;
        $createFeedParams->content = json_encode($feedContent);
        $createFeedParams->author_id = $this->userId;

        $feedService = FeedServiceImpl::getInstance();
        $createResult = $feedService->createFeed($createFeedParams);
        if (!Result::isSuccess($createResult)) {
            $this->render($createResult);
        }
        return $createResult->data;

    }


}