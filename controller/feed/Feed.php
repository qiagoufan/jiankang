<?php

use business\feed\FeedTagConversionUtil;
use business\feed\FeedVirtualFeedBuildUtil;
use facade\request\feed\ConversionFeedParams;
use facade\request\feed\creation\entity\FeedBaseEntity;
use facade\request\feed\creation\FeedContent;
use facade\request\feed\GetFeedDetailParams;
use facade\request\feed\GetFeedListParams;
use facade\request\feed\SearchParams;
use facade\request\product\GetProductDetailParams;
use facade\response\base\PageInfoResp;
use facade\response\feed\FeedHashTagListResp;
use facade\response\feed\FeedListResp;
use facade\response\Result;
use lib\ArrayUtil;
use model\feed\dto\FeedInfoDTO;
use service\feed\impl\FeedInfoRespServiceImpl;
use service\feed\impl\FeedServiceImpl;
use service\product\impl\ProductServiceImpl;
use service\search\impl\SearchServiceImpl;
use service\user\impl\StarServiceImpl;

class Feed extends FUR_Controller
{
    private $userId;

    const DEFAULT_PAGE_SIZE = 10;

    public function __construct()
    {
        $this->userId = $this->getRequestUserId(false);
    }


    public function getFeedDetail()
    {

        $result = new Result();
        $feedInfoResp = null;

        /** @var GetFeedDetailParams $getFeedDetailParams */
        $getFeedDetailParams = $this->requestObject(GetFeedDetailParams::class);
        $feedId = $getFeedDetailParams->feed_id;
        $feedService = FeedServiceImpl::getInstance();
        $getFeedDetailResult = $feedService->getFeedDetail($feedId);

        if (!Result::isSuccess($getFeedDetailResult)) {
            $this->render($getFeedDetailResult);
        }
        $feedDetailDTO = $getFeedDetailResult->data;

        $conversionFeedParams = new ConversionFeedParams();
        FUR_Core::copyProperties($getFeedDetailParams, $conversionFeedParams);
        if ($conversionFeedParams->scene == null) {
            $conversionFeedParams->scene = FeedInfoRespServiceImpl::SCENE_DETAIL;
        }
        $conversionFeedParams->user_id = $this->userId;
        $feedInfoResp = FeedInfoRespServiceImpl::getInstance()->conversionFeedInfoResp($feedDetailDTO, $conversionFeedParams);

        $result->setSuccessWithResult($feedInfoResp);
        $this->render($result);
    }


    public function getFeedList()
    {
        $result = new Result();
        /** @var GetFeedListParams $getFeedListParams */
        $getFeedListParams = $this->requestObject(GetFeedListParams::class);
        $scene = $getFeedListParams->scene;
        $conversionFeedParams = new ConversionFeedParams();
        FUR_Core::copyProperties($getFeedListParams, $conversionFeedParams);
        $conversionFeedParams->user_id = $this->userId;

        $index = $getFeedListParams->index;
        $feedListResp = new FeedListResp();

        $pageSize = $getFeedListParams->page_size;
        $feedListArray = [];
        $feedService = FeedServiceImpl::getInstance();
        $feedInfoDTOListRet = $feedService->getIndexFeedList($getFeedListParams);
        $feedInfoDTOList = $feedInfoDTOListRet->data;
        /**
         * 格式化数据
         */
        if (!empty($feedInfoDTOList)) {
            $feedInfoRespService = FeedInfoRespServiceImpl::getInstance();
            /** @var FeedInfoDTO $feedInfo */
            foreach ($feedInfoDTOList as $feedInfo) {
                if ($feedInfo == null) {
                    continue;
                }
                $feedInfoResp = $feedInfoRespService->conversionFeedInfoResp($feedInfo, $conversionFeedParams);
                if ($feedInfoResp == null) {
                    continue;
                }
                array_push($feedListArray, $feedInfoResp);
            }
        }
        $pageInfo = PageInfoResp::buildPageInfoRespBaseLine($index, $pageSize, count($feedListArray));
        $feedListResp->page_info = $pageInfo;


        $feedListResp->feed_info_list = $feedListArray;
        $result->setSuccessWithResult($feedListResp);

        $this->render($result);
    }

    public function getSearchRecommendWord()
    {
        /**
         * @var SearchParams $searchParams
         */
        $searchParams = $this->requestObject(SearchParams::class);
        $searchService = SearchServiceImpl::getInstance();
        $result = $searchService->getRecommendWordList($searchParams);
        $this->render($result);
    }


    public function searchFeed()
    {
        $result = new Result();
        /** @var SearchParams $searchParams */
        $searchParams = $this->requestObject(SearchParams::class);

        /**
         * 配置一下默认返回数据
         */
        $feedListResp = new FeedListResp();
        $pageInfo = new PageInfoResp();
        $feedListResp->page_info = $pageInfo;

        $feedInfoRespArray = [];
        $searchService = SearchServiceImpl::getInstance();

        $getSearchDTOListRet = $searchService->getSearchDTOList($searchParams);
        if (!Result::isSuccess($getSearchDTOListRet)) {
            $result->setSuccessWithResult($feedListResp);
            $this->render($result);
        }
        $feedInfoList = FeedVirtualFeedBuildUtil::buildSearchFeed($getSearchDTOListRet->data);
        if ($feedInfoList == null || empty($feedInfoList)) {
            $result->setSuccessWithResult($feedListResp);
            $this->render($result);
        }

        $conversionFeedParams = new ConversionFeedParams();
        FUR_Core::copyProperties($searchParams, $conversionFeedParams);
        $conversionFeedParams->scene = FeedInfoRespServiceImpl::SCENE_SEARCH;
        foreach ($feedInfoList as $feedInfo) {
            $feedInfoResp = FeedInfoRespServiceImpl::getInstance()->conversionFeedInfoResp($feedInfo, $conversionFeedParams);
            if ($feedInfoResp == null) {
                continue;
            }
            array_push($feedInfoRespArray, $feedInfoResp);
        }

        $pageInfo = PageInfoResp::buildPageInfoRespBaseLine($searchParams->index, $searchParams->page_size, count($feedInfoRespArray));
        $feedListResp->feed_info_list = $feedInfoRespArray;
        $feedListResp->page_info = $pageInfo;
        $result->setSuccessWithResult($feedListResp);

        $this->render($result);
    }


}