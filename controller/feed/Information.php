<?php

use business\feed\FeedDetailConversionUtil;
use business\feed\FeedInfoRespService;
use facade\request\feed\ConversionFeedParams;
use facade\request\feed\creation\entity\FeedBaseEntity;
use facade\request\feed\creation\entity\FeedProductEntity;
use facade\request\feed\news\GetFeedInformationDetailParams;
use facade\request\PageParams;
use facade\request\tag\QueryTagEntityListParams;
use facade\response\base\PageInfoResp;
use facade\response\feed\detail\element\FeedProductElementResp;
use facade\response\feed\info\FeedInformationListResp;
use facade\response\feed\info\FeedInformationResp;
use facade\response\Result;
use lib\TimeHelper;
use model\dto\tag\TagEntityDTO;
use model\feed\dto\FeedInformationDTO;
use service\feed\impl\FeedInformationServiceImpl;
use service\tag\impl\TagServiceImpl;
use service\user\impl\UserServiceImpl;


class Information extends FUR_Controller
{
    private int $userId;

    const DEFAULT_PAGE_SIZE = 10;

    public function __construct()
    {
        $this->userId = $this->getRequestUserId(false);
    }


    public function getInformationDetail()
    {

        $result = new Result();

        /** @var GetFeedInformationDetailParams $getFeedInformationDetailParams */
        $getFeedInformationDetailParams = $this->requestObject(GetFeedInformationDetailParams::class);
        $infoId = $getFeedInformationDetailParams->info_id;
        $infoDetailRet = FeedInformationServiceImpl::getInstance()->getFeedInformationDetail($infoId);


        $feedInfoResp = new  FeedInformationResp();
        if (!Result::isSuccess($infoDetailRet)) {
            $result->setError(Config_Error::ERR_INFO_NOT_EXIST);
            $this->render($result);
        }
        /** @var FeedInformationDTO $infoDTO */
        $infoDTO = $infoDetailRet->data;
        FUR_Core::copyProperties($infoDTO, $feedInfoResp);

        if ($feedInfoResp->is_original) {
            $userInfoRet = UserServiceImpl::getInstance()->getValidUserInfo($feedInfoResp->author_id);
            if (Result::isSuccess($userInfoRet)) {
                $feedInfoResp->author_name = $userInfoRet->data->nick_name;
            }
        }

        $feedInfoResp->created_time_str = TimeHelper::getPersonalTimeString($feedInfoResp->created_timestamp);

        $feedInfoResp->bind_product = $this->getBindingProduct($infoId);
        $result->setSuccessWithResult($feedInfoResp);
        $this->render($result);
    }


    public function getInformationList()
    {

        $result = new Result();
        $feedInformationListResp = new FeedInformationListResp();
        $feedInformationListResp->info_list = [];
        /** @var PageParams $pageParams */
        $pageParams = $this->requestObject(PageParams::class);

        $infoListRet = FeedInformationServiceImpl::getInstance()->getFeedInformationList($pageParams);

        if (!Result::isSuccess($infoListRet)) {
            $result->setSuccessWithResult($feedInformationListResp);
            $this->render($result);
        }
        /** @var FeedInformationDTO $infoDTO */
        $infoListData = $infoListRet->data;
        foreach ($infoListData as $infoDTO) {
            $feedInfoResp = new  FeedInformationResp();
            FUR_Core::copyProperties($infoDTO, $feedInfoResp);

            if ($feedInfoResp->is_original) {
                $userInfoRet = UserServiceImpl::getInstance()->getValidUserInfo($feedInfoResp->author_id);
                if (Result::isSuccess($userInfoRet)) {
                    $feedInfoResp->author_name = $userInfoRet->data->nick_name;
                }

            }
            if ($feedInfoResp->author_name == null) {
                continue;
            }
            $feedInfoResp->created_time_str = TimeHelper::getPersonalTimeString($feedInfoResp->created_timestamp);
            $feedInfoResp->bind_product = $this->getBindingProduct($infoDTO->id);
            array_push($feedInformationListResp->info_list, $feedInfoResp);
        }

        $pageInfo = PageInfoResp::buildPageInfoRespBaseLine($pageParams->index, $pageSize = 10, count($infoListData));
        $feedInformationListResp->page_info = $pageInfo;
        $result->setSuccessWithResult($feedInformationListResp);
        $this->render($result);
    }


    private function getBindingProduct(int $infoId): ?FeedProductElementResp
    {
        /**
         * 查看绑定的商品
         */
        $queryTagEntityListParams = new  QueryTagEntityListParams ();
        $queryTagEntityListParams->tag_biz_id = $infoId;
        $queryTagEntityListParams->tag_biz_type = TagEntityDTO::BIZ_TYPE_INFORMATION;
        $queryTagEntityListParams->page_size = 1;
        $queryTagEntityListParams->entity_biz_type = TagEntityDTO::BIZ_TYPE_PRODUCT_SKU;

        $tagTagEntityListRet = TagServiceImpl::getInstance()->queryTagEntityList($queryTagEntityListParams);
        if (!Result::isSuccess($tagTagEntityListRet)) {
            return null;
        }

        $tagTagEntityIdList = $tagTagEntityListRet->data;
        if (empty($tagTagEntityIdList)) {
            return null;
        }
        $skuId = $tagTagEntityIdList[0];

        $feedProductEntity = new FeedProductEntity();
        $feedProductEntity->entity_id = $skuId;
        $feedProductEntity->entity_type = FeedBaseEntity::ENTITY_TYPE_PRODUCT;

        /** @var ConversionFeedParams $conversionFeedParams */
        $conversionFeedParams = $this->requestObject(ConversionFeedParams::class);
        $feedProductElementResp = FeedDetailConversionUtil::getInstance()->conversionProductElement($feedProductEntity, $conversionFeedParams);
        return $feedProductElementResp;
    }


}