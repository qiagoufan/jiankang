<?php


use business\theatre\TheatreConversionUtil;
use facade\request\theatre\GetShowListParams;
use facade\request\theatre\GetTheatreIndexFeedListParams;
use facade\response\base\PageInfoResp;
use facade\response\Result;
use facade\response\theatre\detail\ShowBaseInfoResp;
use facade\response\theatre\ShowDetailResp;
use facade\response\theatre\TheatreIndexFeedListResp;
use facade\response\theatre\TheatreIndexListResp;
use model\dto\FeedInfoDTO;
use model\theatre\dto\theatreDailyShowDTO;
use service\feed\impl\FeedServiceImpl;
use service\theatre\impl\TheatreServiceImpl;

class TheatreFeed extends FUR_Controller
{


    public $user_id;

    public function __construct()
    {
        $this->user_id = $this->getRequestUserId(false);
    }

    public function getTheatreIndexFeedList()
    {
        $result = new Result();

        $theatreIndexFeedListResp = new  TheatreIndexFeedListResp();
        /** @var GetTheatreIndexFeedListParams $params */
        $params = $this->requestObject(GetTheatreIndexFeedListParams::class);
        $index = $params->index;


        FeedServiceImpl::getInstance()->getFeatureFeedIdListByTemplateType($index, FeedInfoDTO::TEMPLATE_TYPE_STANDARD_VIDEO_3);


        $pageInfo = new PageInfoResp();
        $pageInfo->page_size = 10;
        $pageInfo->index = 1;
        $pageInfo->count = $theatreIndexListResp->history_show_list != null ? count($theatreIndexListResp->history_show_list) : 0;
        $pageInfo->has_more = $pageInfo->count > 0 ? true : false;
        $theatreIndexListResp->page_info = $pageInfo;


        $result->setSuccessWithResult($theatreIndexListResp);
        $this->render($result);

    }


    private function getHistoryTheatreList(int $index): array
    {
        $theatreList = [];
        $startDate = $this->getBeforeDateByIndex($index);
        if ($startDate == null) {
            return $theatreList;
        }
        for ($i = 0; $i < 3; $i++) {
            $theatreService = TheatreServiceImpl::getInstance();
            $showListRet = $theatreService->getShowListByDate($startDate);
            if (Result::isSuccess($showListRet) == false) {
                continue;
            }

            $startDate = date('Y-m-d', strtotime('-1day', strtotime($startDate)));
            $theatreList = array_merge($showListRet->data, $theatreList);

        }
        return $theatreList;
    }

    private function getTodayTheatreList(): array
    {
        $theatreList = [];
        $startDate = date('Y-m-d');
        $theatreService = TheatreServiceImpl::getInstance();
        $showListRet = $theatreService->getShowListByDate($startDate);
        if (!Result::isSuccess($showListRet)) {
            return $theatreList;
        }
        $theatreList = $showListRet->data;


        return $theatreList;
    }

    private function getUpcomingTheatre()
    {
        $upcomingTheatreDailyShowDTO = new theatreDailyShowDTO();
        $upcomingTheatreDailyShowDTO->id = 0;
        $upcomingTheatreDailyShowDTO->show_type = 0;
        $upcomingTheatreDailyShowDTO->show_date = date('Y-m-d', strtotime('+1day'));
        return $this->conversionData($upcomingTheatreDailyShowDTO, $this->user_id);
    }

    private function getBeforeDateByIndex(int $index): ?string
    {
        $startDate = date('Y-m-d', strtotime("-1day"));
        if ($index != 0) {
            $theatreService = TheatreServiceImpl::getInstance();
            $showInfoRet = $theatreService->getShowInfoById($index);
            if (!Result::isSuccess($showInfoRet)) {
                return null;
            }
            $time = strtotime($showInfoRet->data->show_date);
            $startDate = date('Y-m-d', strtotime("-1day", $time));
        }
        return $startDate;
    }

    private function conversionData(TheatreDailyShowDTO &$theatreDailyShowDTO, int $userId): ?ShowDetailResp
    {
        $showDetailResp = new  ShowDetailResp();

        $showBaseInfoResp = new ShowBaseInfoResp();
        $showBaseInfoResp->date = date("M.d", strtotime($theatreDailyShowDTO->show_date));
        $showBaseInfoResp->show_id = $theatreDailyShowDTO->id;
        $showBaseInfoResp->show_type = $theatreDailyShowDTO->show_type;
        $showDetailResp->show_base_info = $showBaseInfoResp;


        $theatreConversionUtil = TheatreConversionUtil::getInstance();
        if ($theatreDailyShowDTO->show_type == theatreDailyShowDTO::SHOW_TYPE_DAILY_PICTORIAL) {
            $dailyPictorialShowResp = $theatreConversionUtil->buildDailyPictorialShowResp($theatreDailyShowDTO, $userId);
            $showDetailResp->daily_pictorial = $dailyPictorialShowResp;
        } elseif ($theatreDailyShowDTO->show_type == theatreDailyShowDTO::SHOW_TYPE_NEW_PRODUCT) {
            $newArrivalProductShowResp = $theatreConversionUtil->buildNewArrivalProductShowResp($theatreDailyShowDTO, $userId);
            $showDetailResp->new_arrival_product = $newArrivalProductShowResp;
        } elseif ($theatreDailyShowDTO->show_type == theatreDailyShowDTO::SHOW_TYPE_ACTIVITY_NOTICE) {
            $showDetailResp->activity_notice = $theatreConversionUtil->buildActivityNoticeShowResp($theatreDailyShowDTO, $userId);
        } elseif ($theatreDailyShowDTO->show_type == theatreDailyShowDTO::SHOW_TYPE_SUPER_PRODUCT) {
            $superProductShowResp = $theatreConversionUtil->buildSuperProductShowResp($theatreDailyShowDTO, $userId);
            $showDetailResp->super_product = $superProductShowResp;
        } elseif ($theatreDailyShowDTO->show_type == theatreDailyShowDTO::SHOW_TYPE_UPCOMING_TIPS) {
            $upcomingTipsShowResp = $theatreConversionUtil->buildUpcomingTipsShowResp($theatreDailyShowDTO, $userId);
            $showDetailResp->upcoming_tips = $upcomingTipsShowResp;
        }

        return $showDetailResp;

    }
}