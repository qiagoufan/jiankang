<?php

use business\feed\FeedInfoRespService;
use business\feed\FeedVirtualFeedBuildUtil;
use facade\request\feed\ConversionFeedParams;
use facade\request\feed\CreateFeedParams;
use facade\request\feed\creation\entity\FeedBaseEntity;
use facade\request\feed\creation\entity\FeedInformationEntity;
use facade\request\feed\creation\FeedContent;
use facade\request\feed\GetFeedDetailParams;
use facade\request\feed\GetFeedListParams;
use facade\request\feed\GetHashTagListParams;
use facade\request\feed\SearchParams;
use facade\request\product\GetRelatedReviewParams;
use facade\request\product\GetProductDetailParams;
use facade\request\tag\QueryTagEntityListParams;
use facade\request\user\QueryUserCollectListParams;
use facade\response\base\PageInfoResp;
use facade\response\feed\FeedHashTagCategoryResp;
use facade\response\feed\FeedListResp;
use facade\response\Result;
use lib\ArrayUtil;
use model\dto\FeedInfoDTO;
use model\dto\ProductItemDTO;
use model\dto\ProductSkuDTO;
use model\dto\tag\TagEntityDTO;
use model\dto\UserCollectDTO;
use model\dto\UserStarDTO;
use service\feed\impl\FeedInfoRespServiceImpl;
use service\feed\impl\FeedServiceImpl;
use service\feed\impl\FeedTagServiceImpl;
use service\product\impl\ProductReviewServiceImpl;
use service\product\impl\ProductServiceImpl;
use service\search\impl\SearchServiceImpl;
use service\tag\impl\TagServiceImpl;
use service\user\impl\CollectServiceImpl;
use \business\feed\FeedTagConversionUtil;
use service\user\impl\StarServiceImpl;
use  \facade\response\feed\FeedHashTagListResp;

class Search extends FUR_Controller
{


    const DEFAULT_PAGE_SIZE = 10;

    /**
     * 获取feed标签目录列表
     */
    public function getSearchHashTagCategoryList()
    {
        $result = FeedTagServiceImpl::getInstance()->getHashTagCategoryList(FeedTagServiceImpl::SCENE_INDEX_SEARCH);
        $this->render($result);

    }

    /**
     * 获取发送Feed标签列表
     */
    public function getHashTagList()
    {
        /** @var GetHashTagListParams $getFeedTagListParams */
        $getFeedTagListParams = $this->requestObject(GetHashTagListParams::class);
        $result = FeedTagServiceImpl::getInstance()->getHashTagList($getFeedTagListParams);
        $this->render($result);
    }


    /**
     *获取搜索结果
     */
    public function searchFeed()
    {
        $result = new Result();
        /** @var SearchParams $searchParams */
        $searchParams = $this->requestObject(SearchParams::class);

        /**
         * 配置一下默认返回数据
         */
        $feedListResp = new FeedListResp();
        $pageInfo = new PageInfoResp();
        $feedListResp->page_info = $pageInfo;

        $feedInfoRespArray = [];
        $searchService = SearchServiceImpl::getInstance();

        $getSearchDTOListRet = $searchService->getSearchDTOList($searchParams);
        if (!Result::isSuccess($getSearchDTOListRet)) {
            $result->setSuccessWithResult($feedListResp);
            $this->render($result);
        }
        $feedInfoList = FeedVirtualFeedBuildUtil::buildSearchFeed($getSearchDTOListRet->data);
        if ($feedInfoList == null || empty($feedInfoList)) {
            $result->setSuccessWithResult($feedListResp);
            $this->render($result);
        }

        $conversionFeedParams = new ConversionFeedParams();
        FUR_Core::copyProperties($searchParams, $conversionFeedParams);
        $conversionFeedParams->scene = FeedInfoRespServiceImpl::SCENE_SEARCH;
        foreach ($feedInfoList as $feedInfo) {
            $feedInfoResp = FeedInfoRespServiceImpl::getInstance()->conversionFeedInfoResp($feedInfo, $conversionFeedParams);
            if ($feedInfoResp == null) {
                continue;
            }
            array_push($feedInfoRespArray, $feedInfoResp);
        }

        $pageInfo = PageInfoResp::buildPageInfoRespBaseLine($searchParams->index, $searchParams->page_size, count($feedInfoRespArray));

        $feedListResp->feed_info_list = $feedInfoRespArray;
        $feedListResp->page_info = $pageInfo;
        $result->setSuccessWithResult($feedListResp);

        $this->render($result);
    }


}