<?php

use business\feed\FeedInfoRespService;
use business\feed\FeedVirtualFeedBuildUtil;
use facade\request\feed\ConversionFeedParams;
use facade\request\feed\CreateFeedParams;
use facade\request\feed\creation\entity\FeedBaseEntity;
use facade\request\feed\creation\entity\FeedInformationEntity;
use facade\request\feed\creation\FeedContent;
use facade\request\feed\GetFeedDetailParams;
use facade\request\feed\GetFeedListParams;
use facade\request\feed\SearchParams;
use facade\request\product\GetRelatedReviewParams;
use facade\request\tag\QueryTagEntityListParams;
use facade\request\user\QueryUserCollectListParams;
use facade\response\base\PageInfoResp;
use facade\response\feed\FeedListResp;
use facade\response\Result;
use model\dto\FeedInfoDTO;
use model\dto\ProductSkuDTO;
use model\dto\tag\TagEntityDTO;
use model\dto\UserCollectDTO;
use service\feed\impl\FeedInfoRespServiceImpl;
use service\feed\impl\FeedServiceImpl;
use service\product\impl\ProductReviewServiceImpl;
use service\product\impl\ProductServiceImpl;
use service\search\impl\SearchServiceImpl;
use service\tag\impl\TagServiceImpl;
use service\user\impl\CollectServiceImpl;


class FeedReport extends FUR_Controller
{
    private int $userId;

    public function __construct()
    {
        $this->userId = $this->getRequestUserId(false);
    }


    public function getFeedReportCategory()
    {

        $result = new Result();
        $categoryList = [];
        $categoryList[1] = '涉黄信息';
        $categoryList[2] = '人身攻击';
        $categoryList[3] = '有害信息';
        $categoryList[4] = '违法信息';
        $categoryList[5] = '诈骗信息';
        $categoryList[6] = '恶意营销';

        $result->setSuccessWithResult($categoryList);
        $this->render($result);
    }


    public function reportFeed()
    {
        $result = new Result();
        $feedId = $this->request('feed_id');
        $reportCategoryId = $this->request('report_category_id');
        $result->setSuccessWithResult(true);
        $this->render($result);
    }

}