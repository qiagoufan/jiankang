<?php


use facade\request\feed\heat\TriggerEventParams;
use facade\request\user\UserCollectParams;
use facade\response\Result;
use service\feed\impl\FeedHeatServiceImpl;
use service\user\impl\CollectServiceImpl;

class Collect extends \FUR_Controller
{
    private int $userId;

    public function __construct()
    {
        $this->userId = $this->getRequestUserId();
    }


    public function collect()
    {
        $result = new Result();
        /** @var UserCollectParams $collectParams */
        $collectParams = $this->requestObject(UserCollectParams::class);
        $collectParams->user_id = $this->userId;

        $collectService = CollectServiceImpl::getInstance();
        $result = $collectService->saveUserCollect($collectParams);

        /**
         * 增加热度
         */
        if ($collectParams->collect_biz_type == \lib\AppConstant::BIZ_TYPE_FEED) {
            $triggerEventParams = new  TriggerEventParams();
            $triggerEventParams->feed_id = $collectParams->collect_biz_id;
            $triggerEventParams->event_type = FeedHeatServiceImpl::EVENT_COLLECT;
            FeedHeatServiceImpl::getInstance()->triggerEvent($triggerEventParams);
        }


        $this->render($result);

    }

    public function cancelCollect()
    {
        $result = new Result();
        /** @var UserCollectParams $collectParams */
        $collectParams = $this->requestObject(UserCollectParams::class);
        $collectParams->user_id = $this->userId;
        $collectService = CollectServiceImpl::getInstance();
        $result = $collectService->deleteUserCollect($collectParams);
        /**
         * 增加热度
         */
        if ($collectParams->collect_biz_type == \lib\AppConstant::BIZ_TYPE_FEED) {
            $triggerEventParams = new  TriggerEventParams();
            $triggerEventParams->feed_id = $collectParams->collect_biz_id;
            $triggerEventParams->event_type = FeedHeatServiceImpl::EVENT_COLLECT;
            $triggerEventParams->action_type = FeedHeatServiceImpl::ACTION_TYPE_DECR;
            FeedHeatServiceImpl::getInstance()->triggerEvent($triggerEventParams);
        }

        $this->render($result);
    }


}