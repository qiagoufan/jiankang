<?php

use business\feed\FeedGroupManagementUtil;
use business\maintain\MaintainStarTaskConverisonUtil;
use business\star\StarConversionUtil;
use business\user\UserInfoConversionUtil;
use facade\request\feed\ConversionFeedParams;
use facade\request\feed\creation\entity\FeedBaseEntity;
use facade\request\feed\creation\FeedContent;
use facade\request\feed\GetStarCardFeedListParams;
use facade\request\tag\QueryTagEntityListParams;
use facade\request\tag\TagEntityParams;
use facade\response\base\PageInfoResp;
use facade\response\feed\FeedInfoResp;
use facade\response\feed\ForumOverviewResp;
use facade\response\feed\group\UserGroupRightResp;
use facade\response\feed\StarCardFeedListResp;
use facade\response\Result;
use facade\response\user\group\StarGroupManagementResp;
use model\dto\FeedInfoDTO;
use model\dto\ProductSkuDTO;
use model\dto\tag\TagEntityDTO;
use model\dto\UserStarDTO;
use model\group\dto\GroupManagerDTO;
use service\feed\impl\FeedInfoRespServiceImpl;
use service\feed\impl\FeedServiceImpl;
use service\group\impl\GroupServiceImpl;
use service\tag\impl\TagServiceImpl;
use \business\feed\FeedTagConversionUtil;
use service\user\impl\StarServiceImpl;

class StarFeed extends FUR_Controller
{
    private int $userId;

    const DEFAULT_PAGE_SIZE = 10;

    public function __construct()
    {
        $this->userId = $this->getRequestUserId(false);
    }


    /**
     * 获取明星卡的feed列表
     */
    public function getStarCardFeedList()
    {
        $result = new Result();
        /** @var GetStarCardFeedListParams $starParams */
        $starParams = $this->requestObject(GetStarCardFeedListParams::class);

        $starId = $starParams->star_id;
        $userId = $this->userId;
        $index = $starParams->index;
        $starCardFeedListResp = new  StarCardFeedListResp();

        if ($index == 0) {
            /**
             * 明星信息
             */
            $starInfoResp = StarConversionUtil::getInstance()->buildStarInfoResp($starId, $userId);
            $starCardFeedListResp->star_info_resp = $starInfoResp;
            /**
             * 相关商品和活动
             */
            $related_feed_list = $this->getStarRelatedFeedList($starId);
            $starCardFeedListResp->related_feed_list = $related_feed_list;
        }

        $starFeedList = $this->getStarForumFeedList($starId, $index);
        $starCardFeedListResp->forum_feed_list = $starFeedList;
        $starCardFeedListResp->forum_overview = $this->buildStarForumOverview($starId, $starFeedList);
        /**
         * 展示相关star feed task列表
         */
        $starTaskConversionUtil = MaintainStarTaskConverisonUtil::getInstance();
        $starCardFeedListResp->star_task_list = $starTaskConversionUtil->conversionStarTaskList($starParams);


        /**
         * 相关活动
         */
//        $starCardFeedListResp->star_related_activity = $starTaskConversionUtil->buildStarRelatedAcitivity();
        $starCardFeedListResp->star_related_activity = null;

        /**
         * 标签信息
         */
        $starCardFeedListResp->star_tag_info = $this->getStarTagInfo($starId);


        $page_info = PageInfoResp::buildPageInfoRespBaseLine($index, self::DEFAULT_PAGE_SIZE, count($starFeedList));
        $starCardFeedListResp->page_info = $page_info;


        $starCardFeedListResp->star_group_management = $this->buildStarGroupInfoResp($starId, $userId);


        $result->setSuccessWithResult($starCardFeedListResp);

        $this->render($result);
    }

    private function getStarForumFeedList(int $starId, int $index): ?array
    {
        $starForumFeedList = [];

        $tagService = TagServiceImpl::getInstance();
        /**
         * 查询打上明星标的feed列表
         */
        $queryTagEntityListParams = new QueryTagEntityListParams ();
        $queryTagEntityListParams->tag_biz_type = TagEntityDTO::BIZ_TYPE_STAR;
        $queryTagEntityListParams->tag_biz_id = $starId;
        $queryTagEntityListParams->entity_biz_type = TagEntityDTO::BIZ_TYPE_FEED;
        $queryTagEntityListParams->index = $index;
        $entityListRet = $tagService->queryTagEntityList($queryTagEntityListParams);

        if (!Result::isSuccess($entityListRet)) {
            return $starForumFeedList;
        }
        $feedIdList = $entityListRet->data;
        if (empty($feedIdList)) {
            return $feedIdList;
        }
        /**
         * 场景参数
         */

        /** @var ConversionFeedParams $conversionFeedParams */
        $conversionFeedParams = $this->requestObject(ConversionFeedParams::class);
        $conversionFeedParams->scene = FeedInfoRespServiceImpl::SCENE_STAR_CARD;
        $conversionFeedParams->star_id = $starId;

        $feedService = FeedServiceImpl::getInstance();
        $feedInfoRespService = FeedInfoRespServiceImpl::getInstance();
        foreach ($feedIdList as $feedId) {
            $feedDetailRet = $feedService->getFeedDetail($feedId);
            if (!Result::isSuccess($feedDetailRet)) {
                continue;
            }
            $feedInfo = $feedDetailRet->data;

            $feedInfoResp = $feedInfoRespService->conversionFeedInfoResp($feedInfo, $conversionFeedParams);
            if ($feedInfoResp == null) {
                continue;
            }
            array_push($starForumFeedList, $feedInfoResp);
        }
        return $starForumFeedList;
    }

    private function getStarRelatedFeedList(int $starId): ?array
    {
        $related_feed_list = [];
        /**
         * 查询明星商品
         */
        $tagService = TagServiceImpl::getInstance();

        $queryTagEntityListParams = new QueryTagEntityListParams ();
        $queryTagEntityListParams->tag_biz_type = UserStarDTO::STAR_BIZ_TYPE;
        $queryTagEntityListParams->tag_biz_id = $starId;
        $queryTagEntityListParams->entity_biz_type = ProductSkuDTO::PRODUCT_SKU_BIZ_TYPE;
        $relatedEntityListRet = $tagService->queryTagEntityList($queryTagEntityListParams);


        $feedInfoRespService = FeedInfoRespServiceImpl::getInstance();


        if (Result::isSuccess($relatedEntityListRet)) {
            /** @var ConversionFeedParams $conversionFeedParams */
            $conversionFeedParams = $this->requestObject(ConversionFeedParams::class);
            $conversionFeedParams->scene = FeedInfoRespServiceImpl::SCENE_SUPER_PRODUCT_CARD;

            /** @var FeedInfoDTO $productFeedInfo */
            $productFeedInfo = $this->buildStarVirtualProductFeed($relatedEntityListRet->data);
            $productFeedInfoResp = $feedInfoRespService->conversionFeedInfoResp($productFeedInfo, $conversionFeedParams);
            if ($productFeedInfoResp != null) {
                array_push($related_feed_list, $productFeedInfoResp);
            }
        }

        /**
         * 查询明星活动
         */
        $queryTagEntityListParams = new QueryTagEntityListParams ();
        $queryTagEntityListParams->entity_biz_type = 'activity';
        $queryTagEntityListParams->tag_biz_type = UserStarDTO::STAR_BIZ_TYPE;
        $queryTagEntityListParams->tag_biz_id = $starId;

        $relatedEntityListRet = $tagService->queryTagEntityList($queryTagEntityListParams);


        $conversionFeedParams = new ConversionFeedParams();
        $conversionFeedParams->scene = FeedInfoRespServiceImpl::SCENE_SUPER_PRODUCT_CARD;
        $conversionFeedParams->user_id = $this->userId;

//        $activityFeedInfo = $this->buildActivityVirtualFeed($relatedEntityListRet->data);
//        $activityFeedInfoResp = $feedInfoRespService->conversionFeedInfoResp($activityFeedInfo, $conversionFeedParams);
//        if ($activityFeedInfoResp != null) {
//            array_push($related_feed_list, $activityFeedInfoResp);
//        }

//        $activityFeedInfo = $this->buildActivityVirtualFeed2($relatedEntityListRet->data);
//        $activityFeedInfoResp = $feedInfoRespService->conversionFeedInfoResp($activityFeedInfo);
//        if ($activityFeedInfoResp != null) {
//            array_push($related_feed_list, $activityFeedInfoResp);
//        }


        return $related_feed_list;
    }


    private function getStarTagInfo(int $starId): ?array
    {
        $starTagInfoArray = [];
        $starService = StarServiceImpl::getInstance();
        $startInfoRet = $starService->getStarInfo($starId);
        if (!Result::isSuccess($startInfoRet)) {
            return null;
        }

        /** @var UserStarDTO $starDTO */
        $starDTO = $startInfoRet->data;

        $ProductFeedTagResp = FeedTagConversionUtil::buildStarFeedTagInfo($starDTO);
        array_push($starTagInfoArray, $ProductFeedTagResp);

        return $starTagInfoArray;
    }

    private function buildStarForumOverview(int $starId, ?array $feedArray)
    {
        $forumOverview = new ForumOverviewResp();
        $tagEntityParams = new TagEntityParams ();
        $tagEntityParams->tag_biz_type = TagEntityDTO::BIZ_TYPE_STAR;
        $tagEntityParams->tag_biz_id = $starId;
        $tagEntityParams->entity_biz_type = TagEntityDTO::BIZ_TYPE_FEED;
        $tagEntityCountRet = TagServiceImpl::getInstance()->getTagEntityCount($tagEntityParams);
        if (Result::isSuccess($tagEntityCountRet)) {
            $forumOverview->total_user_num = $tagEntityCountRet->data;
        }

        if ($feedArray != null && !empty($feedArray)) {
            $userList = [];
            $userIdList = [];
            /** @var FeedInfoResp $feedInfoResp */
            foreach ($feedArray as $feedInfoResp) {
                if (in_array($feedInfoResp->author_info->user_id, $userIdList)) {
                    continue;
                }
                $userInfo['user_id'] = $feedInfoResp->author_info->user_id;
                $userInfo['avatar'] = $feedInfoResp->author_info->avatar;
                array_push($userList, $userInfo);
                array_push($userIdList, $feedInfoResp->author_info->user_id);
            }
            $forumOverview->total_user_list = $userList;
        }
        return $forumOverview;
    }

    private function buildStarVirtualProductFeed(array $skuIdList): ?FeedInfoDTO
    {
        $feedInfoDTO = new FeedInfoDTO();
        $feedInfoDTO->id = 0;
        $feedInfoDTO->template_type = FeedInfoDTO::TEMPLATE_TYPE_PRODUCT_LIST_8;
        $feedInfoDTO->feed_type = FeedInfoDTO::FEED_TYPE_PRODUCT;
        $feedInfoDTO->author_id = 0;
        $feedInfoDTO->published_timestamp = time();
        $feedInfoDTO->virtual = FeedInfoDTO::VIRTUAL_FEED;
        $feedContent = new FeedContent();

        if (empty($skuIdList)) {
            return $feedInfoDTO;
        }
        $feedEntityList = [];
        $index = 0;
        foreach ($skuIdList as $skuId) {
            $feedEntityProduct = new FeedBaseEntity();
            $feedEntityProduct->entity_id = $skuId;
            $feedEntityProduct->index = $index;
            $feedEntityProduct->entity_type = FeedBaseEntity::ENTITY_TYPE_PRODUCT;
            array_push($feedEntityList, $feedEntityProduct);
            $index++;
        }
        $feedContent->feed_entity_list = $feedEntityList;
        $feedInfoDTO->content = json_encode($feedContent);
        return $feedInfoDTO;
    }

    private function buildActivityVirtualFeed(array $activityIdList): ?FeedInfoDTO
    {
        $feedInfoDTO = new FeedInfoDTO();
        $feedInfoDTO->id = 0;
        $feedInfoDTO->template_type = FeedInfoDTO::TEMPLATE_TYPE_STANDARD_SINGLE_IMAGE_6;
        $feedInfoDTO->feed_type = FeedInfoDTO::FEED_TYPE_ACTIVITY;
        $feedInfoDTO->virtual = FeedInfoDTO::VIRTUAL_FEED;
        $feedInfoDTO->author_id = 0;
        $feedInfoDTO->published_timestamp = time();
        $feedContent = new FeedContent();
        $feedEntityList = [];

        $feedEntityImage = new FeedBaseEntity();
        $feedEntityImage->entity_id = 1;
        $feedEntityImage->index = 0;
        $feedEntityImage->content = '';
        $feedEntityImage->image = 'https://cdn.ipxmall.com/system/star/75bc9bfee9402264ab905b4149b1f283.jpg';
        $feedEntityImage->entity_type = FeedBaseEntity::ENTITY_TYPE_IMAGE;
        $feedEntityImage->icon = '';
        array_push($feedEntityList, $feedEntityImage);

        $feedContent->feed_entity_list = $feedEntityList;

        $feedInfoDTO->content = json_encode($feedContent);

        return $feedInfoDTO;
    }


    private function buildBrandForumFeedList(int $brandId, int $index): ?array
    {

        $brandForumFeedList = [];
        $tagService = TagServiceImpl::getInstance();

        /**
         * 查询打上品牌标的feed列表
         */
        $queryTagEntityListParams = new QueryTagEntityListParams ();
        $queryTagEntityListParams->tag_biz_type = TagEntityDTO::BIZ_TYPE_BRAND;
        $queryTagEntityListParams->tag_biz_id = $brandId;
        $queryTagEntityListParams->entity_biz_type = TagEntityDTO::BIZ_TYPE_FEED;
        $queryTagEntityListParams->index = $index;
        $entityListRet = $tagService->queryTagEntityList($queryTagEntityListParams);
        if (!Result::isSuccess($entityListRet)) {
            return $brandForumFeedList;
        }

        $feedIdList = $entityListRet->data;
        if (empty($feedIdList)) {
            return $feedIdList;
        }

        /**
         * 场景参数
         */

        /** @var ConversionFeedParams $conversionFeedParams */
        $conversionFeedParams = $this->requestObject(ConversionFeedParams::class);
        $conversionFeedParams->scene = FeedInfoRespServiceImpl::SCENE_BRAND_CARD;

        $feedService = FeedServiceImpl::getInstance();
        $feedInfoRespService = FeedInfoRespServiceImpl::getInstance();
        foreach ($feedIdList as $feedId) {
            $feedDetailRet = $feedService->getFeedDetail($feedId);
            if (!Result::isSuccess($feedDetailRet)) {
                continue;
            }
            $feedInfo = $feedDetailRet->data;

            $feedInfoResp = $feedInfoRespService->conversionFeedInfoResp($feedInfo, $conversionFeedParams);
            if ($feedInfoResp == null) {
                continue;
            }
            array_push($brandForumFeedList, $feedInfoResp);
        }
        return $brandForumFeedList;

    }


    private function buildStarGroupInfoResp(int $starId, int $userId): ?StarGroupManagementResp
    {


        $starGroupManagementResp = new StarGroupManagementResp();
        $ownerId = 0;
        $managerIdList = [];
        $groupService = GroupServiceImpl::getInstance();
        $groupManagerListRet = $groupService->queryManagerList($starId);
        if (Result::isSuccess($groupManagerListRet)) {
            $managerDTOList = $groupManagerListRet->data;
            /** @var GroupManagerDTO $managerDTO */
            foreach ($managerDTOList as $managerDTO) {
                if ($managerDTO->manager_type == GroupManagerDTO::MANAGER_TYPE_OWNER) {
                    $ownerId = $managerDTO->manager_user_id;
                } else {
                    array_push($managerIdList, $managerDTO->manager_user_id);
                }
            }
        }
        $starGroupManagementResp->enable_apply_group_owner = 1;
        if ($ownerId != 0) {
            $starGroupManagementResp->group_owner = UserInfoConversionUtil::getInstance()->getSimpleUserInfoResp($ownerId);
            $starGroupManagementResp->enable_apply_group_owner = 0;
        }
        $starGroupManagementResp->group_manager_list = [];
        if ($managerIdList != null) {
            foreach ($managerIdList as $managerUserId) {
                $userInfo = UserInfoConversionUtil::getInstance()->getSimpleUserInfoResp($managerUserId);
                if ($userInfo == null) {
                    continue;
                }
                array_push($starGroupManagementResp->group_manager_list, $userInfo);
            }
        }
        $starGroupManagementResp->enable_apply_group_manager = 1;
        if (count($starGroupManagementResp->group_manager_list) >= 5) {
            $starGroupManagementResp->enable_apply_group_manager = 0;
        }

        /**
         * 判断用户在圈子里的权限
         */
        $userManagerType = 0;
        if ($userId && $ownerId == $userId) {
            $userManagerType = GroupManagerDTO::MANAGER_TYPE_OWNER;
            $starGroupManagementResp->is_owner = 1;
        } else if ($userId && in_array($userId, $managerIdList)) {
            $userManagerType = GroupManagerDTO::MANAGER_TYPE_MANAGER;
            $starGroupManagementResp->is_manager = 1;
        }


        $userGroupRight = FeedGroupManagementUtil::getInstance()->buildUserGroupRightResp($userManagerType);
        $starGroupManagementResp->user_group_right = $userGroupRight;
        return $starGroupManagementResp;
    }

}