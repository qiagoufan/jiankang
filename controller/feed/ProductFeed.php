<?php

use business\feed\FeedVirtualFeedBuildUtil;
use business\product\ProductDetailConversionUtil;
use facade\request\feed\ConversionFeedParams;
use facade\request\feed\creation\entity\FeedBaseEntity;
use facade\request\feed\creation\entity\FeedImageEntity;
use facade\request\feed\creation\FeedContent;
use facade\request\feed\GetProductCardFeedListParams;
use facade\request\product\GetProductDetailParams;
use facade\request\tag\QueryTagEntityListParams;
use facade\response\base\PageInfoResp;
use facade\response\feed\ProductCardFeedListResp;
use facade\response\Result;
use model\dto\FeedInfoDTO;
use model\dto\tag\TagEntityDTO;
use model\feed\dto\FeedInformationDTO;
use service\feed\impl\FeedInfoRespServiceImpl;
use service\feed\impl\FeedInformationServiceImpl;
use service\feed\impl\FeedServiceImpl;
use service\tag\impl\TagServiceImpl;
use facade\response\feed\ForumOverviewResp;
use facade\request\tag\TagEntityParams;
use \service\product\impl\ProductServiceImpl;
use  \business\feed\FeedTagConversionUtil;

class ProductFeed extends FUR_Controller
{
    private int $userId;

    const DEFAULT_PAGE_SIZE = 10;

    public function __construct()
    {
        $this->userId = $this->getRequestUserId(false);
    }


    /**
     * 获取超级商品的页卡列表
     */
    public function getProductCardFeedList()
    {
        $result = new Result();
        /** @var GetProductCardFeedListParams $productParams */
        $productParams = $this->requestObject(GetProductCardFeedListParams::class);
        $skuId = $productParams->sku_id;
        $feedId = $productParams->feed_id;
        $userId = $this->userId;
        $index = $productParams->index;
        $productCardFeedListResp = new  ProductCardFeedListResp();

        $feedList = [];
        if ($index == 0) {
            $productSuperDetailResp = ProductDetailConversionUtil::getInstance()->buildProductSuperDetail($skuId, $userId);
            $productCardFeedListResp->product_info_resp = $productSuperDetailResp;

        }

        //精彩内容文章
        if ($index == 0) {
            //资讯相关
            $feedInfoRespService = FeedInfoRespServiceImpl::getInstance();
            $informationFeedList = $this->getProductInformationList($skuId);
            if ($informationFeedList != null && !empty($informationFeedList)) {
                foreach ($informationFeedList as $feedInfoDTO) {
                    $feedInfoResp = $feedInfoRespService->conversionFeedInfoResp($feedInfoDTO, $conversionFeedParams);
                    if ($feedInfoResp == null) {
                        continue;
                    }
                    array_push($productCardFeedListResp->forum_news_list, $feedInfoResp);
                }
            }

        }


        //标签相关
        $productCardFeedListResp->product_tag_info = $this->getSkuTagInfo($skuId);


        //讨论区feed
        $skuForumFeedList = $this->getSkuForumFeedList($skuId, $feedId, $index);
        if ($skuForumFeedList != null && !empty($skuForumFeedList)) {
            $feedList = array_merge($feedList, $skuForumFeedList);
        }
        $productCardFeedListResp->forum_feed_list = $feedList;
        $productCardFeedListResp->forum_overview = $this->buildProductForumOverview($skuId, $feedList);

        $page_info = PageInfoResp::buildPageInfoRespBaseLine($index, self::DEFAULT_PAGE_SIZE, count($feedList));
        $productCardFeedListResp->page_info = $page_info;
        $result->setSuccessWithResult($productCardFeedListResp);
        $this->render($result);
    }


    private function getSkuTagInfo(int $skuId): ?array
    {
        $product_tag_info = [];
        $productSkuDetailParams = new GetProductDetailParams();
        $productSkuDetailParams->sku_id = $skuId;
        $productSkuServiceImpl = ProductServiceImpl::getInstance();
        $productSkuDetailResult = $productSkuServiceImpl->getProductDetail($productSkuDetailParams);
        if (!Result::isSuccess($productSkuDetailResult)) {
            return [];
        }

        $productSkuDTO = $productSkuDetailResult->data;
        $ProductFeedTagResp = FeedTagConversionUtil::buildProductFeedTagInfo($productSkuDTO);
        array_push($product_tag_info, $ProductFeedTagResp);

        return $product_tag_info;

    }

    private function buildProductForumOverview(int $skuId, ?array $feedArray)
    {
        $forumOverview = new ForumOverviewResp();
        $tagEntityParams = new TagEntityParams ();
        $tagEntityParams->tag_biz_type = TagEntityDTO::BIZ_TYPE_PRODUCT_SKU;
        $tagEntityParams->tag_biz_id = $skuId;
        $tagEntityParams->entity_biz_type = TagEntityDTO::BIZ_TYPE_FEED;
        $tagEntityCountRet = TagServiceImpl::getInstance()->getTagEntityCount($tagEntityParams);
        if (Result::isSuccess($tagEntityCountRet)) {
            $forumOverview->total_user_num = $tagEntityCountRet->data;
        }

        if ($feedArray != null && !empty($feedArray)) {
            $userList = [];
            $userIdList = [];
            /** @var FeedInfoResp $feedInfoResp */
            foreach ($feedArray as $feedInfoResp) {
                if (in_array($feedInfoResp->author_info->user_id, $userIdList)) {
                    continue;
                }
                $userInfo['user_id'] = $feedInfoResp->author_info->user_id;
                $userInfo['avatar'] = $feedInfoResp->author_info->avatar;
                array_push($userList, $userInfo);
                array_push($userIdList, $feedInfoResp->author_info->user_id);
            }
            $forumOverview->total_user_list = $userList;
        }
        return $forumOverview;
    }

    private function getSkuForumFeedList(int $skuId, int $feedId, int $index): ?array
    {
        $forumFeedList = [];


        $tagService = TagServiceImpl::getInstance();
        /**
         * 查询打上商品标的feed列表
         */
        $queryTagEntityListParams = new QueryTagEntityListParams ();
        $queryTagEntityListParams->tag_biz_type = TagEntityDTO::BIZ_TYPE_PRODUCT_SKU;
        $queryTagEntityListParams->tag_biz_id = $skuId;
        $queryTagEntityListParams->entity_biz_type = TagEntityDTO::BIZ_TYPE_FEED;
        $queryTagEntityListParams->index = $index;
        $entityListRet = $tagService->queryTagEntityList($queryTagEntityListParams);

        if (!Result::isSuccess($entityListRet)) {
            return $forumFeedList;
        }
        $feedIdList = $entityListRet->data;
        if ($feedId != 0) {
            array_unshift($feedIdList, $feedId);
        }
        if (empty($feedIdList)) {
            return $forumFeedList;
        }
        $feedIdList = array_unique($feedIdList);
        /**
         * 场景参数
         */

        /** @var ConversionFeedParams $conversionFeedParams */
        $conversionFeedParams = $this->requestObject(ConversionFeedParams::class);
        $conversionFeedParams->scene = FeedInfoRespServiceImpl::SCENE_SUPER_PRODUCT_CARD;

        $feedService = FeedServiceImpl::getInstance();
        $feedInfoRespService = FeedInfoRespServiceImpl::getInstance();


//        $feedInfoMock = $this->mockTypea1();
//        $feedInfoRespMock = $feedInfoRespService->conversionFeedInfoResp($feedInfoMock, $conversionFeedParams);
//        array_push($forumFeedList, $feedInfoRespMock);
//        $feedInfoMock = $this->mockTypea2();
//        $feedInfoRespMock = $feedInfoRespService->conversionFeedInfoResp($feedInfoMock, $conversionFeedParams);
//        array_push($forumFeedList, $feedInfoRespMock);
//
//        if ($index == 0) {
//            //资讯相关
//            $informationFeedList = $this->getProductInformationList($skuId);
//            if ($informationFeedList != null && !empty($informationFeedList)) {
//                foreach ($informationFeedList as $feedInfoDTO) {
//                    $feedInfoResp = $feedInfoRespService->conversionFeedInfoResp($feedInfoDTO, $conversionFeedParams);
//                    if ($feedInfoResp == null) {
//                        continue;
//                    }
//                    array_push($forumFeedList, $feedInfoResp);
//                }
//            }
//
//        }


        foreach ($feedIdList as $feedId) {
            $feedDetailRet = $feedService->getFeedDetail($feedId);
            if (!Result::isSuccess($feedDetailRet)) {
                continue;
            }
            $feedInfo = $feedDetailRet->data;
            $feedInfoResp = $feedInfoRespService->conversionFeedInfoResp($feedInfo, $conversionFeedParams);
            if ($feedInfoResp == null) {
                continue;
            }
            array_push($forumFeedList, $feedInfoResp);
        }
        return $forumFeedList;
    }

    /**
     * 获取商品资讯
     * @param int $skuId
     * @return array|null
     */
    private function getProductInformationList(int $skuId): ?array
    {
        $information_feed_list = [];
        //查询相关资讯

        $tagService = TagServiceImpl::getInstance();
        $queryTagEntityListParams = new QueryTagEntityListParams ();
        $queryTagEntityListParams->tag_biz_type = TagEntityDTO::BIZ_TYPE_PRODUCT_SKU;
        $queryTagEntityListParams->tag_biz_id = $skuId;
        $queryTagEntityListParams->entity_biz_type = TagEntityDTO::BIZ_TYPE_INFORMATION;
        $queryTagEntityListParams->page_size = 3;
        $infoEntityListRet = $tagService->queryTagEntityList($queryTagEntityListParams);
        if (!Result::isSuccess($infoEntityListRet)) {
            return null;
        }
        /** @var array $infoIdList */
        $infoIdList = $infoEntityListRet->data;

        foreach ($infoIdList as $infoId) {
            $feedInfoDTO = FeedVirtualFeedBuildUtil::buildInformationFeed($infoId);
            if ($feedInfoDTO == null) {
                continue;
            }
            array_push($information_feed_list, $feedInfoDTO);
        }


        return $information_feed_list;
    }


    public function mockTypea1()
    {
        $feedInfoDTO = new  FeedInfoDTO();
        $feedInfoDTO->id = 1001;
        $feedInfoDTO->template_type = FeedInfoDTO::TEMPLATE_TYPE_NEWS_11;
        $feedInfoDTO->feed_type = FeedInfoDTO::FEED_TYPE_PRODUCT_OPINION;
        $feedInfoDTO->author_id = 134;
        $feedInfoDTO->published_timestamp = time() - 1231111;
        $feedContent = new FeedContent();
        $feedEntityList = [];

        $feedEntityImage = new FeedImageEntity();
        $feedEntityImage->entity_id = 1;
        $feedEntityImage->index = 0;
        $feedEntityImage->image = 'https://consumer.huawei.com/content/dam/huawei-cbg-site/greate-china/cn/mkt/pdp/phones/p40-pro/images/design/design-intro-e-cn@2x.webp';
        $feedEntityImage->entity_type = FeedBaseEntity::ENTITY_TYPE_IMAGE;
        array_push($feedEntityList, $feedEntityImage);

        $feedContent->feed_entity_list = $feedEntityList;
        $feedContent->content_title = '早晚相伴 游刃有余 元气满满 最新最优质的智能手机，尽在华为官网';
//        $feedContent->content_desc = '早晚相伴 游刃有余 元气满满';

        $feedInfoDTO->content = json_encode($feedContent);

        return $feedInfoDTO;

    }

    public function mockTypea2()
    {
        $feedInfoDTO = new  FeedInfoDTO();
        $feedInfoDTO->id = 1000;
        $feedInfoDTO->template_type = FeedInfoDTO::TEMPLATE_TYPE_ARTICLE_12;
        $feedInfoDTO->feed_type = FeedInfoDTO::FEED_TYPE_PRODUCT_OPINION;
        $feedInfoDTO->author_id = 134;
        $feedInfoDTO->published_timestamp = time() - 1231111;
        $feedContent = new FeedContent();
        $feedEntityList = [];

        $feedEntity = new FeedImageEntity();
        $feedEntity->entity_id = 1;
        $feedEntity->index = 0;
        $feedEntity->image = 'https://consumer.huawei.com/content/dam/huawei-cbg-site/greate-china/cn/mkt/pdp/phones/p40-pro/images/power/power-intro-e@2x.webp';
        $feedEntity->entity_type = FeedBaseEntity::ENTITY_TYPE_IMAGE;
        array_push($feedEntityList, $feedEntity);

        $feedContent->feed_entity_list = $feedEntityList;
        $feedContent->content_desc = '作为国内高端手机的代表机型，华为mate系列在最近几年发展迅速，一跃成为了国内最高端手机代表，深得高端用户的喜爱';
        $feedContent->content_title = '差价500，买华为Mate30还是Mate20P？答案很简单';

        $feedInfoDTO->content = json_encode($feedContent);

        return $feedInfoDTO;
    }

}