<?php

use business\feed\FeedInfoRespService;
use business\feed\FeedVirtualFeedBuildUtil;
use business\user\UserInfoConversionUtil;
use facade\request\feed\ConversionFeedParams;
use facade\request\feed\CreateFeedParams;
use facade\request\feed\creation\entity\FeedBaseEntity;
use facade\request\feed\creation\entity\FeedInformationEntity;
use facade\request\feed\creation\FeedContent;
use facade\request\feed\GetFeedDetailParams;
use facade\request\feed\GetFeedListParams;
use facade\request\feed\group\ApplyManagerParams;
use facade\request\feed\group\RemoveFeedParams;
use facade\request\feed\group\RemoveManagerParams;
use facade\request\feed\group\UpdateGroupStarInfoParams;
use facade\request\feed\heat\TriggerEventParams;
use facade\request\feed\SearchParams;
use facade\request\product\GetRelatedReviewParams;
use facade\request\product\GetProductDetailParams;
use facade\request\tag\QueryTagEntityListParams;
use facade\request\tag\TagEntityParams;
use facade\request\user\FollowParams;
use facade\request\user\QueryUserCollectListParams;
use facade\request\user\UpdateStarParams;
use facade\response\base\PageInfoResp;
use facade\response\feed\FeedListResp;
use facade\response\feed\group\ApplyForm4UserRecordResp;
use facade\response\feed\group\FeedGroupInfoResp;
use facade\response\feed\group\GroupManagerApplyFormResp;
use facade\response\Result;
use facade\response\user\group\GroupUserInfoResp;
use lib\ArrayUtil;
use lib\TimeHelper;
use model\dto\FeedInfoDTO;
use model\dto\InteractiveCommentDTO;
use model\dto\MaintainLeaderboardRecordDTO;
use model\dto\ProductSkuDTO;
use model\dto\tag\TagEntityDTO;
use model\dto\UserCollectDTO;
use model\dto\UserStarDTO;
use model\group\dto\GroupManagerApplyFormDTO;
use model\group\dto\GroupManagerDTO;
use model\group\dto\GroupManageViolationRecordDTO;
use service\feed\impl\FeedHeatServiceImpl;
use service\feed\impl\FeedInfoRespServiceImpl;
use service\feed\impl\FeedServiceImpl;
use service\group\impl\GroupServiceImpl;
use service\interactive\impl\InteractiveServiceImpl;
use service\maintain\impl\MaintainLeaderboardServiceImpl;
use service\product\impl\ProductReviewServiceImpl;
use service\product\impl\ProductServiceImpl;
use service\search\impl\SearchServiceImpl;
use service\tag\impl\TagServiceImpl;
use service\user\impl\CollectServiceImpl;
use \business\feed\FeedTagConversionUtil;
use service\user\impl\FollowServiceImpl;
use service\user\impl\StarServiceImpl;
use  \facade\response\feed\FeedHashTagListResp;
use service\user\impl\UserServiceImpl;

class FeedGroup extends FUR_Controller
{
    private int $userId;


    const DEFAULT_PAGE_SIZE = 10;

    public function __construct()
    {
        $this->userId = $this->getRequestUserId(false);
    }


    /**
     * 查看圈子管理信息
     */
    public function queryGroupManagementDetail()
    {
        $result = new Result();
        $feedGroupInfoResp = new FeedGroupInfoResp();
        $starId = $this->request('star_id');
        if ($starId == 0) {
            $result->setSuccessWithResult($feedGroupInfoResp);
            $this->render($result);
        }
        $starInfoRet = StarServiceImpl::getInstance()->getStarInfo($starId);
        if (Result::isSuccess($starInfoRet) == false) {
            $result->setSuccessWithResult($feedGroupInfoResp);
            $this->render($result);
        }
        $feedGroupInfoResp->group_name = $starInfoRet->data->star_name . "的圈子";
        $feedGroupInfoResp->star_name = $starInfoRet->data->star_name;


        $ownerId = 0;
        $managerIdList = [];
        $groupService = GroupServiceImpl::getInstance();
        $groupManagerListRet = $groupService->queryManagerList($starId);
        if (Result::isSuccess($groupManagerListRet)) {
            $managerDTOList = $groupManagerListRet->data;
            /** @var GroupManagerDTO $managerDTO */
            foreach ($managerDTOList as $managerDTO) {
                if ($managerDTO->manager_type == GroupManagerDTO::MANAGER_TYPE_OWNER) {
                    $ownerId = $managerDTO->manager_user_id;
                } else {
                    array_push($managerIdList, $managerDTO->manager_user_id);
                }
            }
        }
        if ($ownerId != 0) {
            $feedGroupInfoResp->group_owner = UserInfoConversionUtil::getInstance()->buildGroupUserInfoResp($ownerId, $starId, GroupManagerDTO::MANAGER_TYPE_OWNER);
        }
        if ($managerIdList != null) {
            $feedGroupInfoResp->group_manager_list = [];
            foreach ($managerIdList as $userId) {
                $groupUserInfo = UserInfoConversionUtil::getInstance()->buildGroupUserInfoResp($userId, $starId, GroupManagerDTO::MANAGER_TYPE_MANAGER);
                if ($groupUserInfo == null) {
                    continue;
                }
                array_push($feedGroupInfoResp->group_manager_list, $groupUserInfo);
            }
        }


        $maintainLeaderboardService = MaintainLeaderboardServiceImpl::getInstance();
        //贡献值排行榜
        $leaderboardRet = $maintainLeaderboardService->getStarContributionLeaderboard($starId, 0, 3);
        if (Result::isSuccess($leaderboardRet)) {
            $userIdList = $leaderboardRet->data;
            $topUserList = [];
            foreach ($userIdList as $userId) {
                array_push($topUserList, UserInfoConversionUtil::getInstance()->buildGroupUserInfoResp($userId, $starId));
            }
            $feedGroupInfoResp->top_fans_list = $topUserList;
        }

        //管理员申请列表
        if ($ownerId == $this->userId) {
            $feedGroupInfoResp->apply_form_list = $this->getApplyFormList($starId);
        }

        $result->setSuccessWithResult($feedGroupInfoResp);
        $this->render($result);
    }

    /**
     * 查看管理员申请详情
     */
    public function queryGroupManagerRequisitionDetail()
    {
        $applyFormId = $this->request('form_id');
        $userId = $this->userId;


    }

    /**
     * 通过管理员申请
     */
    public function approveGroupManagerRequisition()
    {
        $result = new Result();
        $applyFormId = $this->request('form_id');
        $userId = $this->userId;
        $applyFormRet = GroupServiceImpl::getInstance()->queryApplyForm($applyFormId);
        if (!Result::isSuccess($applyFormRet)) {
            $result->setError(Config_Error::ERR_GROUP_APPLY_FORM_NOT_FOUND);
            $this->render($result);
        }

        /** @var GroupManagerApplyFormDTO $applyForm */
        $applyForm = $applyFormRet->data;
        if ($applyForm->form_status != GroupManagerApplyFormDTO::FORM_STATUS_NEW) {
            $result->setError(Config_Error::ERR_GROUP_APPLY_FORM_INVALID);
            $this->render($result);
        }


        /**
         * 查看管理员人数是否满了
         */
        $groupManagerNum = 0;
        $groupManagerNumRet = GroupServiceImpl::getInstance()->queryManagerNum($applyForm->star_id, GroupManagerDTO::MANAGER_TYPE_MANAGER);
        if (Result::isSuccess($groupManagerNumRet)) {
            $groupManagerNum = $groupManagerNumRet->data;
            if ($groupManagerNum >= 5) {
                $result->setError(Config_Error::ERR_GROUP_APPLY_FORM_MANAGER_NUM);
                $this->render($result);
            }
        }

        /**
         * 查看用户是否已经是管理员了
         */
        $userRoleRet = GroupServiceImpl::getInstance()->queryUserManagerType($applyForm->user_id, $applyForm->star_id);
        if (Result::isSuccess($userRoleRet) && $userRoleRet->data != 0) {
            $result->setError(Config_Error::ERR_GROUP_APPLY_USER_IS_MANAGER);
            $this->render($result);

        }
        /**
         * 更新申请单状态
         */
        GroupServiceImpl::getInstance()->updateApplyFormStatus($applyFormId, GroupManagerApplyFormDTO::FORM_STATUS_PASS);

        /**
         *添加管理员
         */
        $followParams = new FollowParams();
        $followParams->user_id = $applyForm->user_id;
        $followParams->target_id = $applyForm->star_id;
        $followParams->target_type = UserStarDTO::STAR_BIZ_TYPE;
        FollowServiceImpl::getInstance()->follow($followParams);
        $createRet = GroupServiceImpl::getInstance()->createGroupManager($applyForm->user_id, $applyForm->star_id, GroupManagerDTO::MANAGER_TYPE_MANAGER);

        /**
         * 把未审核的全部拒绝
         */
        if ($groupManagerNum + 1 >= 5) {
            $managerApplyFormListRet = GroupServiceImpl::getInstance()->queryManagerApplyFormList($applyForm->star_id);
            if (Result::isSuccess($managerApplyFormListRet)) {
                $managerApplyFormList = $managerApplyFormListRet->data;
                /** @var GroupManagerApplyFormDTO $managerApplyFormDTO */
                foreach ($managerApplyFormList as $managerApplyFormDTO) {
                    GroupServiceImpl::getInstance()->updateApplyFormStatus($managerApplyFormDTO->id, GroupManagerApplyFormDTO::FORM_STATUS_REJECT);
                }
            }
        }

        $this->render($createRet);

    }

    /**
     *检查圈主状态
     */
    public function checkGroupOwner()
    {
        $result = new Result();
        $starName = trim($this->request('star_name'));
        $starInfoRet = StarServiceImpl::getInstance()->getStarInfoByName($starName);

        if (!Result::isSuccess($starInfoRet)) {
            $result->setError(Config_Error::ERR_GROUP_APPLY_IP_NOT_EXIST);
            $this->render($result);
        }
        $starId = $starInfoRet->data->id;
        $ownerNumRet = GroupServiceImpl::getInstance()->queryManagerNum($starId, GroupManagerDTO::MANAGER_TYPE_OWNER);
        if (Result::isSuccess($ownerNumRet) && $ownerNumRet->data != 0) {
            $result->setError(Config_Error::ERR_GROUP_APPLY_OWNER_IS_EXIST);
            $result->setData(['star_id' => $starId]);
            $this->render($result);
        }
        $result->setSuccessWithResult(true);
        $this->render($result);

    }


    /**
     *拒绝管理员申请
     */
    public function rejectGroupManagerRequisition()
    {
        $result = new Result();
        $applyFormId = $this->request('form_id');
        $userId = $this->userId;
        $applyFormRet = GroupServiceImpl::getInstance()->queryApplyForm($applyFormId);
        if (!Result::isSuccess($applyFormRet)) {
            $result->setError(Config_Error::ERR_GROUP_APPLY_FORM_NOT_FOUND);
            $this->render($result);
        }


        /** @var GroupManagerApplyFormDTO $applyForm */
        $applyForm = $applyFormRet->data;

        if ($applyForm->form_status != GroupManagerApplyFormDTO::FORM_STATUS_NEW) {
            $result->setError(Config_Error::ERR_GROUP_APPLY_FORM_INVALID);
            $this->render($result);
        }

        /**
         * 更新申请单状态
         */
        GroupServiceImpl::getInstance()->updateApplyFormStatus($applyFormId, GroupManagerApplyFormDTO::FORM_STATUS_REJECT);
        $result->setSuccessWithResult(true);
        $this->render($result);

    }


    /**
     *移除管理员
     */
    public function removeGroupManager()
    {

        /** @var RemoveManagerParams $removeManagerParams */
        $removeManagerParams = $this->requestObject(RemoveManagerParams::class);


        GroupServiceImpl::getInstance()->deleteGroupManager($removeManagerParams->mananger_user_id, $removeManagerParams->star_id);
        //记录操作历史

        $groupManageViolationRecordDTO = new GroupManageViolationRecordDTO();
        $groupManageViolationRecordDTO->star_id = $removeManagerParams->star_id;
        $groupManageViolationRecordDTO->created_timestamp = time();
        $groupManageViolationRecordDTO->biz_type = GroupManageViolationRecordDTO::BIZ_TYPE_REMOVE_MANAGER;
        $groupManageViolationRecordDTO->operator_id = $removeManagerParams->user_id;
        $groupManageViolationRecordDTO->biz_id = $removeManagerParams->mananger_user_id;
        $groupManageViolationRecordDTO->reason = GroupManageViolationRecordDTO::REMOVE_MANAGER_REASON_MAP[$removeManagerParams->reason_id];
        $groupManageViolationRecordDTO->additional_desc = $removeManagerParams->additional_desc;

        $createRet = GroupServiceImpl::getInstance()->createViolationRecord($groupManageViolationRecordDTO);
        $this->render($createRet);
    }

    /**
     *移除内容
     */
    public function removeFeed()
    {
        /** @var RemoveFeedParams $removeFeedParams */
        $removeFeedParams = $this->requestObject(RemoveFeedParams::class);
        $starId = $this->request('star_id');
        $result = new Result();
        $userId = $this->getRequestUserId();

        //如果带了starid说明是圈子管理,只需要去标就可以了
        if ($starId) {
            $userRoleRet = GroupServiceImpl::getInstance()->queryUserManagerType($userId, $starId);
            if (!Result::isSuccess($userRoleRet) || $userRoleRet->data == 0) {
                $result->setError(Config_Error::ERR_GROUP_USER_IS_NOT_MANAGER);
                $this->render($result);
            }
        } else {
            $queryRet = UserServiceImpl::getInstance()->queryWhiteUserById($userId);
            if (!Result::isSuccess($queryRet)) {
                $result->setError(Config_Error::ERR_GROUP_USER_IS_NOT_MANAGER);
                $this->render($result);
            }

        }
        /**
         * 先删标
         */
        $tagService = TagServiceImpl::getInstance();


        //如果是白名单用户操作需要硬删除
        if (!$starId) {
            $feedService = FeedServiceImpl::getInstance();
            //查询feed详情,然后比对author
            $feedInfoRet = $feedService->getFeedDetail($removeFeedParams->feed_id);
            if (!Result::isSuccess($feedInfoRet)) {
                $result->setError(Config_Error::ERR_DELETE_FEED_NOT_EXIST);
                $this->render($result);
            }
            /** @var FeedInfoDTO $feedInfo */
            $feedInfo = $feedInfoRet->data;

            $tagService = TagServiceImpl::getInstance();
            $queryTagEntityListParams = new QueryTagEntityListParams ();
            $queryTagEntityListParams->tag_biz_type = TagEntityDTO::BIZ_TYPE_FEED;
            $queryTagEntityListParams->tag_biz_id = $removeFeedParams->feed_id;
            $queryTagEntityListParams->entity_biz_type = TagEntityDTO::BIZ_TYPE_STAR;
            $tagEntityListRet = $tagService->queryTagEntityList($queryTagEntityListParams);

            if (Result::isSuccess($tagEntityListRet)) {
                $feedService->updateIdolMaintainLeaderboardScore($removeFeedParams->feed_id, $tagEntityListRet->data, MaintainLeaderboardRecordDTO::RECORD_TYPE_DELETE_FEED);
                $feedService->deleteStarFeedTag($removeFeedParams->feed_id, $tagEntityListRet->data);
            }
            $feedService->deleteFeed($removeFeedParams->feed_id, $feedInfo->author_id);

        } else {
            //从圈子里面删feed
            $tagEntityParams = new TagEntityParams();
            $tagEntityParams->tag_biz_id = $removeFeedParams->star_id;
            $tagEntityParams->tag_biz_type = TagEntityDTO::BIZ_TYPE_STAR;
            $tagEntityParams->entity_biz_id = $removeFeedParams->feed_id;
            $tagEntityParams->entity_biz_type = TagEntityDTO::BIZ_TYPE_FEED;
            $tagService->deleteTagEntity($tagEntityParams);

            //feed移除圈子标
            $tagEntityParams = new TagEntityParams();
            $tagEntityParams->entity_biz_id = $removeFeedParams->star_id;
            $tagEntityParams->entity_biz_type = TagEntityDTO::BIZ_TYPE_STAR;
            $tagEntityParams->tag_biz_id = $removeFeedParams->feed_id;
            $tagEntityParams->tag_biz_type = TagEntityDTO::BIZ_TYPE_FEED;
            $tagService->deleteTagEntity($tagEntityParams);
        }

        //记录操作历史

        $groupManageViolationRecordDTO = new GroupManageViolationRecordDTO();
        $groupManageViolationRecordDTO->star_id = $removeFeedParams->star_id;
        $groupManageViolationRecordDTO->created_timestamp = time();
        $groupManageViolationRecordDTO->biz_type = TagEntityDTO::BIZ_TYPE_FEED;
        $groupManageViolationRecordDTO->biz_id = $removeFeedParams->feed_id;
        $groupManageViolationRecordDTO->reason = GroupManageViolationRecordDTO::REMOVE_FEED_REASON_MAP[$removeFeedParams->reason_id];
        $groupManageViolationRecordDTO->additional_desc = $removeFeedParams->additional_desc;

        $createRet = GroupServiceImpl::getInstance()->createViolationRecord($groupManageViolationRecordDTO);
        $this->render($createRet);
    }

    /**
     * 移除内容原因
     */
    public function removeFeedReasonCategory()
    {
        $result = new Result();
        $categoryList = GroupManageViolationRecordDTO::REMOVE_FEED_REASON_MAP;
        $result->setSuccessWithResult($categoryList);
        $this->render($result);
    }

    /**
     *移除管理员原因
     */
    public function removeManagerReasonCategory()
    {
        $result = new Result();
        $categoryList = GroupManageViolationRecordDTO::REMOVE_MANAGER_REASON_MAP;
        $result->setSuccessWithResult($categoryList);
        $this->render($result);
    }


    /**
     *编辑Ip信息
     */
    public function updateStarInfo()
    {
        /** @var UpdateGroupStarInfoParams $updateGroupStarInfoParams */
        $updateGroupStarInfoParams = $this->requestObject(UpdateGroupStarInfoParams::class);
        if ($updateGroupStarInfoParams->base_introduce != null) {
            $baseIntroduce = str_replace('：', ':', $updateGroupStarInfoParams->base_introduce);
            if (stripos($baseIntroduce, "\n")) {
                $baseIntroduceLineArray = explode("\n", $baseIntroduce);
            } else {
                $baseIntroduceLineArray = explode('\n', $baseIntroduce);
            }
            $baseIntroduceDetail = [];
            foreach ($baseIntroduceLineArray as $baseIntroduceLine) {
                $explodeData = explode(":", $baseIntroduceLine);
                if (count($explodeData) < 2) {
                    continue;
                }
                $map = ['key' => $explodeData[0], 'value' => $explodeData[1]];
                array_push($baseIntroduceDetail, $map);
            }
            $updateGroupStarInfoParams->base_introduce = json_encode($baseIntroduceDetail);
        }

        $updateStarParams = new UpdateStarParams();
        FUR_Core::copyProperties($updateGroupStarInfoParams, $updateStarParams);
        $updateStarParams->official_description = $updateGroupStarInfoParams->description;
        $updateStarParams->base_introduce = $updateGroupStarInfoParams->base_introduce;
        $updateStarParams->official_avatar = $updateGroupStarInfoParams->avatar;
        $updateRet = StarServiceImpl::getInstance()->updateStarInfo($updateStarParams);
        $this->render($updateRet);
    }


    /**
     *发起申请圈主/管理员申请
     */
    public function applyGroupManagement()
    {
        $result = new Result();
        /** @var ApplyManagerParams $applyFormParams */
        $applyFormParams = $this->requestObject(ApplyManagerParams::class);
        if ($applyFormParams->star_id == 0 && trim($applyFormParams->star_name) == '') {
            $result->setError(Config_Error::ERR_GROUP_APPLY_GROUP_INFO_IS_NULL);
            $this->render($result);
        }
        /**
         * 尝试去取得圈子id
         */
        if ($applyFormParams->star_id == 0) {
            $queryStarInfoRet = StarServiceImpl::getInstance()->getStarInfoByName($applyFormParams->star_name);
            if (Result::isSuccess($queryStarInfoRet)) {
                $applyFormParams->star_id = $queryStarInfoRet->data->id;
            }
        }


        if ($applyFormParams->star_id != 0) {
            $userRoleRet = GroupServiceImpl::getInstance()->queryUserManagerType($applyFormParams->user_id, $applyFormParams->star_id);

            //不允许申请同级别,或者更低级别的管理员
            if (Result::isSuccess($userRoleRet) && $userRoleRet->data != 0) {
                if ($userRoleRet->data <= $applyFormParams->form_type) {
                    $result->setError(Config_Error::ERR_GROUP_APPLY_USER_IS_MANAGER);
                    $this->render($result);
                }
            }

            /**
             * 查看管理员人数是否满了
             */
            $groupManagerNumRet = GroupServiceImpl::getInstance()->queryManagerNum($applyFormParams->star_id, $applyFormParams->form_type);
            if (Result::isSuccess($groupManagerNumRet)) {
                if ($applyFormParams->form_type == GroupManagerDTO::MANAGER_TYPE_MANAGER && $groupManagerNumRet->data >= 5) {
                    $result->setError(Config_Error::ERR_GROUP_APPLY_FORM_MANAGER_NUM);
                    $this->render($result);
                }
                if ($applyFormParams->form_type == GroupManagerDTO::MANAGER_TYPE_OWNER && $groupManagerNumRet->data >= 1) {
                    $result->setError(Config_Error::ERR_GROUP_APPLY_OWNER_IS_EXIST);
                    $this->render($result);
                }
            }
        }
        $result = GroupServiceImpl::getInstance()->applyManager($applyFormParams);
        $this->render($result);

    }

    /**
     * 查看申请记录
     */
    public function queryApplyGroupManagementRecord()
    {
        $result = new Result();
        $userId = $this->userId;
        $userApplyHistoryRet = GroupServiceImpl::getInstance()->queryUserApplyHistory($userId);
        $recordList = [];
        if (Result::isSuccess($userApplyHistoryRet)) {
            $userApplyHistory = $userApplyHistoryRet->data;
            /** @var GroupManagerApplyFormDTO $applyFormDTO */
            foreach ($userApplyHistory as $applyFormDTO) {
                $record = new ApplyForm4UserRecordResp();
                $record->id = $applyFormDTO->id;
                $record->apply_desc = $applyFormDTO->apply_desc;
                $record->contact = $applyFormDTO->contact;
                $record->form_type = $applyFormDTO->form_type;
                $record->form_status = $applyFormDTO->form_status;
                $record->created_time = TimeHelper::getPersonalTimeString($applyFormDTO->created_timestamp);
                if ($applyFormDTO->updated_timestamp) {
                    $record->updated_time = TimeHelper::getPersonalTimeString($applyFormDTO->updated_timestamp);
                }
                $record->group_name = '';
                if ($applyFormDTO->star_name) {
                    $record->group_name = $applyFormDTO->star_name;
                } elseif ($applyFormDTO->star_id) {
                    $starInfoRet = StarServiceImpl::getInstance()->getStarInfo($applyFormDTO->star_id);
                    if (Result::isSuccess($starInfoRet)) {
                        $record->group_name = $starInfoRet->data->star_name;
                    }
                }
                array_push($recordList, $record);
            }
        }
        $result->setSuccessWithResult($recordList);
        $this->render($result);
    }


    /**
     * 圈主权益说明
     */
    public
    function getGroupOwnerIntroduce()
    {
        $result = new Result();
        $result->setSuccessWithResult('圈主可以在入驻的明星/品牌圈子内管理模块，审核用户发布的内容，对管理员的入驻资格有审核权利，有专属认证标志。\n\n 更有机会获得享花星Mall最新联名商品的优先体验机会。');
        $this->render($result);
    }

    /**
     * 管理员权益说明
     */
    public
    function getGroupManagerIntroduce()
    {
        $result = new Result();
        $result->setSuccessWithResult('管理员由该IP圈主考核任命，可以在入驻的明星/品牌圈子管理用户发布的内容，且有专属认证标志。');
        $this->render($result);
    }

    /**
     * 圈子管理删除评论
     */
    public function deleteComment()
    {
        $result = new Result();
        $userId = $this->getRequestUserId();

        $commentId = $this->request('comment_id');
        $reasonId = $this->request('reason_id');
        $starId = (int)$this->request('star_id');
        $additional_desc = $this->request('additional_desc');


        if ($starId) {
            $userRoleRet = GroupServiceImpl::getInstance()->queryUserManagerType($userId, $starId);
            if (!Result::isSuccess($userRoleRet) || $userRoleRet->data == 0) {
                $result->setError(Config_Error::ERR_GROUP_USER_IS_NOT_MANAGER);
                $this->render($result);
            }
        } else {
            $queryRet = UserServiceImpl::getInstance()->queryWhiteUserById($userId);
            if (!Result::isSuccess($queryRet)) {
                $result->setError(Config_Error::ERR_GROUP_USER_IS_NOT_MANAGER);
                $this->render($result);
            }
        }

        $interactiveServiceImpl = InteractiveServiceImpl::getInstance();
        //将评论下面的回复删除
        if ($commentId == null || $userId == null) {
            $result->setError(Config_Error::ERR_DELETE_COMMENT_PARAMS_INFO_NO_RIGHT);
            $this->render($result);
        }
        $commentRet = $interactiveServiceImpl->queryCommentById($commentId);
        $interactiveCommentDTO = $commentRet->data;
        if (Result::isSuccess($commentRet) == false) {
            $result->setSuccessWithResult(true);
            $this->render($result);
        }

        /** @var InteractiveCommentDTO $interactiveCommentDTO */


        $delCommentRet = $interactiveServiceImpl->delComment($userId, $commentId, $interactiveCommentDTO->biz_id, false);
        if (!Result::isSuccess($delCommentRet)) {
            $result->setError(Config_Error::ERR_DELETE_FEED_NO_RIGHT);
            $this->render($result);
        }

        $interactiveServiceImpl->deleteReplyByComment($interactiveCommentDTO->biz_id, $interactiveCommentDTO->biz_type, $commentId);


        //更新overview
        if ($interactiveCommentDTO != null) {
            $interactiveServiceImpl->updateOverviewCount($interactiveCommentDTO->biz_id, $interactiveCommentDTO->biz_type, 'comment', -1);
        }

        /**
         * 增加热度
         */
        if ($commentRet->data->biz_type == \lib\AppConstant::BIZ_TYPE_FEED) {
            $triggerEventParams = new  TriggerEventParams();
            $triggerEventParams->feed_id = $commentRet->data->biz_id;
            $triggerEventParams->event_type = FeedHeatServiceImpl::EVENT_COMMENT;
            $triggerEventParams->action_type = FeedHeatServiceImpl::ACTION_TYPE_DECR;
            FeedHeatServiceImpl::getInstance()->triggerEvent($triggerEventParams);
        }

        /**
         * 操作日志
         */

        $groupManageViolationRecordDTO = new GroupManageViolationRecordDTO();
        $groupManageViolationRecordDTO->star_id = $starId;
        $groupManageViolationRecordDTO->created_timestamp = time();
        $groupManageViolationRecordDTO->biz_type = TagEntityDTO::BIZ_TYPE_COMMENT;
        $groupManageViolationRecordDTO->biz_id = $commentId;
        $groupManageViolationRecordDTO->reason = GroupManageViolationRecordDTO::REMOVE_FEED_REASON_MAP[$reasonId];
        $groupManageViolationRecordDTO->additional_desc = $additional_desc;

        $createRet = GroupServiceImpl::getInstance()->createViolationRecord($groupManageViolationRecordDTO);

        $result->setSuccessWithResult($delCommentRet->data);
        $this->render($result);

    }

    /**
     * 圈子管理删除回复
     */
    public function deleteReply()
    {
        $result = new Result();
        $replyId = $this->request('reply_id');
        $userId = $this->getRequestUserId();
        $reasonId = $this->request('reason_id');
        $starId = (int)$this->request('star_id');
        $additional_desc = $this->request('additional_desc');
        //查询详情
        $interactiveServiceImpl = InteractiveServiceImpl::getInstance();
        $replyRet = $interactiveServiceImpl->queryReplyById($replyId);
        if (Result::isSuccess($replyRet) == false) {
            $result->setSuccessWithResult(true);
            $this->render($result);
        }


        if ($starId) {
            $userRoleRet = GroupServiceImpl::getInstance()->queryUserManagerType($userId, $starId);
            if (!Result::isSuccess($userRoleRet) || $userRoleRet->data == 0) {
                $result->setError(Config_Error::ERR_GROUP_USER_IS_NOT_MANAGER);
                $this->render($result);
            }
        } else {
            $queryRet = UserServiceImpl::getInstance()->queryWhiteUserById($userId);
            if (!Result::isSuccess($queryRet)) {
                $result->setError(Config_Error::ERR_GROUP_USER_IS_NOT_MANAGER);
                $this->render($result);
            }
        }
        $isAdmin = 1;

        /**
         * 删除回复
         */
        $userId = $this->getRequestUserId();
        $result = $interactiveServiceImpl->delReply($userId, $replyId, $isAdmin);
        if (!Result::isSuccess($result)) {
            $this->render($result);
        }
        /** @var InteractiveReplyDTO $replyDTO */
        $replyDTO = $replyRet->data;

        $interactiveServiceImpl->updateOverviewCount($replyDTO->biz_id, $replyDTO->biz_type, 'reply', -1);

        /**
         * 增加热度
         */
        if ($replyRet->data->biz_type == \lib\AppConstant::BIZ_TYPE_FEED) {
            $triggerEventParams = new  TriggerEventParams();
            $triggerEventParams->feed_id = $replyRet->data->biz_id;
            $triggerEventParams->event_type = FeedHeatServiceImpl::EVENT_COMMENT;
            $triggerEventParams->action_type = FeedHeatServiceImpl::ACTION_TYPE_DECR;
            FeedHeatServiceImpl::getInstance()->triggerEvent($triggerEventParams);
        }


        /**
         * 操作日志
         */
        $groupManageViolationRecordDTO = new GroupManageViolationRecordDTO();
        $groupManageViolationRecordDTO->star_id = $starId;
        $groupManageViolationRecordDTO->created_timestamp = time();
        $groupManageViolationRecordDTO->biz_type = TagEntityDTO::BIZ_TYPE_REPLY;
        $groupManageViolationRecordDTO->biz_id = $replyId;
        $groupManageViolationRecordDTO->reason = GroupManageViolationRecordDTO::REMOVE_FEED_REASON_MAP[$reasonId];
        $groupManageViolationRecordDTO->additional_desc = $additional_desc;

        $result->setSuccessWithResult(true);
        $this->render($result);

    }


    private function getApplyFormList(int $starId): ?array
    {
        $applyFormList = [];
        $managerApplyFormListRet = GroupServiceImpl::getInstance()->queryManagerApplyFormList($starId);
        if (!Result::isSuccess($managerApplyFormListRet)) {
            return $applyFormList;
        }
        $managerApplyFormList = $managerApplyFormListRet->data;
        /** @var GroupManagerApplyFormDTO $managerApplyFormDTO */
        foreach ($managerApplyFormList as $managerApplyFormDTO) {
            $groupManagerApplyFormResp = new GroupManagerApplyFormResp();
            $groupManagerApplyFormResp->apply_desc = $managerApplyFormDTO->apply_desc;
            $groupManagerApplyFormResp->contact = $managerApplyFormDTO->contact;
            $groupManagerApplyFormResp->id = $managerApplyFormDTO->id;
            $groupManagerApplyFormResp->group_user_info = UserInfoConversionUtil::getInstance()->buildGroupUserInfoResp($managerApplyFormDTO->user_id, $starId);
            array_push($applyFormList, $groupManagerApplyFormResp);
        }

        return $applyFormList;
    }
}