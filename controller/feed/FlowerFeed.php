<?php

use business\feed\FeedInfoRespService;
use business\feed\FeedVirtualFeedBuildUtil;
use facade\request\feed\ConversionFeedParams;
use facade\request\feed\CreateFeedParams;
use facade\request\feed\creation\entity\FeedBaseEntity;
use facade\request\feed\creation\entity\FeedInformationEntity;
use facade\request\feed\creation\FeedContent;
use facade\request\feed\GetFeedDetailParams;
use facade\request\feed\GetFeedListParams;
use facade\request\feed\SearchParams;
use facade\request\product\GetRelatedReviewParams;
use facade\request\product\GetProductDetailParams;
use facade\request\tag\QueryTagEntityListParams;
use facade\request\user\QueryUserCollectListParams;
use facade\response\base\PageInfoResp;
use facade\response\feed\FeedListResp;
use facade\response\Result;
use lib\ArrayUtil;
use model\dto\FeedInfoDTO;
use model\dto\ProductItemDTO;
use model\dto\ProductSkuDTO;
use model\dto\tag\TagEntityDTO;
use model\dto\UserCollectDTO;
use service\feed\impl\FeedInfoRespServiceImpl;
use service\feed\impl\FeedServiceImpl;
use service\feed\impl\FlowerFeedConversionServiceImpl;
use service\product\impl\ProductReviewServiceImpl;
use service\product\impl\ProductServiceImpl;
use service\search\impl\SearchServiceImpl;
use service\tag\impl\TagServiceImpl;
use service\user\impl\CollectServiceImpl;
use \business\feed\FeedTagConversionUtil;
use service\user\impl\StarServiceImpl;
use  \facade\response\feed\FeedHashTagListResp;

class FlowerFeed extends FUR_Controller
{
    private int $userId;

    const DEFAULT_PAGE_SIZE = 10;

    public function __construct()
    {
        $this->userId = $this->getRequestUserId(false);
    }


    public function getFeedList()
    {
        $result = new Result();
        /** @var GetFeedListParams $getIndexFeedListParams */
        $getIndexFeedListParams = $this->requestObject(GetFeedListParams::class);

        $conversionFeedParams = new ConversionFeedParams();
        FUR_Core::copyProperties($getIndexFeedListParams, $conversionFeedParams);
        $conversionFeedParams->user_id = $this->userId;

        $pageSize = self::DEFAULT_PAGE_SIZE;
        $index = $getIndexFeedListParams->index;
        $feedListResp = new FeedListResp();


        $feedService = FeedServiceImpl::getInstance();

        $feedInfoDTOList = [];
        $feedInfoDTOListRet = $feedService->getFeedList($getIndexFeedListParams);
        if (Result::isSuccess($feedInfoDTOListRet)) {
            $feedInfoDTOList = $feedInfoDTOListRet->data;
        }

        $feedListArray = [];
        /**
         * 格式化数据
         */
        if (!empty($feedInfoDTOList)) {
            $feedInfoRespService = FlowerFeedConversionServiceImpl::getInstance();
            /** @var FeedInfoDTO $feedInfo */
            foreach ($feedInfoDTOList as $feedInfo) {
                if ($feedInfo == null) {
                    continue;
                }
                $feedInfoResp = $feedInfoRespService->conversionFeedInfoResp($feedInfo, $conversionFeedParams);
                if ($feedInfoResp == null) {
                    continue;
                }
                array_push($feedListArray, $feedInfoResp);
            }
        }
        $pageInfo = PageInfoResp::buildPageInfoRespBaseLine($index, $pageSize, count($feedListArray));
        $feedListResp->page_info = $pageInfo;


        $feedListResp->feed_info_list = $feedListArray;
        $result->setSuccessWithResult($feedListResp);

        $this->render($result);
    }


}