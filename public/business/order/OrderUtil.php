<?php

namespace business\order;


use facade\response\admin\OrderInfoResp;
use facade\response\Result;
use lib\PriceHelper;
use model\order\dto\OrderInfoDTO;
use model\order\dto\OrderProductDTO;
use model\product\dto\ProductItemDTO;
use service\admin\impl\OrderAdminServiceImpl;
use service\logistics\impl\LogisticsServiceImpl;
use service\order\impl\OrderServiceImpl;
use service\product\impl\ProductServiceImpl;
use service\user\impl\UserServiceImpl;

class OrderUtil
{

    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public static function conversionOrderListDetailInfo(OrderInfoDTO $orderInfoDTO): ?UserOrderDetailResp
    {
        $orderDetailResp = new UserOrderDetailResp();
        $orderService = OrderServiceImpl::getInstance();
        $orderProductListRet = $orderService->queryOrderProductList($orderInfoDTO->id);
        if (!Result::isSuccess($orderProductListRet)) {
            return null;
        }
        $orderDetailResp->product_list = [];
        $orderDetailResp->total_product_count = 0;
        /** @var OrderProductDTO $orderProduct */
        foreach ($orderProductListRet->data as $orderProduct) {
            $orderProductResp = new OrderProductResp();
            $orderProductResp->item_id = $orderProduct->item_id;
            $orderProductResp->count = $orderProduct->count;
            $productInfoRet = ProductServiceImpl::getInstance()->getProductItem($orderProduct->item_id);
            if (!Result::isSuccess($productInfoRet)) {
                $orderProductResp->product_name = '无效商品';
                array_push($orderDetailResp->product_list, $orderProductResp);
                continue;
            }
            /** @var ProductItemDTO $productItemDTO */
            $productItemDTO = $productInfoRet->data;
            $orderProductResp->product_name = $productItemDTO->product_name;
            $orderProductResp->main_image = $productItemDTO->main_image;
            $orderProductResp->product_price = $productItemDTO->current_price;
            $orderProductResp->format_product_price = PriceHelper::conversionDisplayPrice($orderProductResp->product_price);
            array_push($orderDetailResp->product_list, $orderProductResp);
            $orderDetailResp->total_product_count += $orderProductResp->count;
        }


        $orderDetailResp->order_sn = $orderInfoDTO->order_sn;
        $orderDetailResp->order_id = $orderInfoDTO->id;

        $orderDetailResp->pay_price = $orderInfoDTO->pay_price;
        $orderDetailResp->shipping_price = $orderInfoDTO->shipping_price;
        $orderDetailResp->discount_price = $orderInfoDTO->discount_price;
        $orderDetailResp->product_total_price = $orderInfoDTO->product_total_price;

        $orderDetailResp->format_pay_price = PriceHelper::conversionDisplayPrice($orderInfoDTO->pay_price);
        $orderDetailResp->format_shipping_price = PriceHelper::conversionDisplayPrice($orderInfoDTO->shipping_price);
        $orderDetailResp->format_discount_price = PriceHelper::conversionDisplayPrice($orderInfoDTO->discount_price);
        $orderDetailResp->format_product_total_price = PriceHelper::conversionDisplayPrice($orderInfoDTO->product_total_price);

        $orderDetailResp->pay_status = $orderInfoDTO->pay_status;
        $orderDetailResp->order_status = $orderInfoDTO->order_status;
        $orderDetailResp->created_timestamp = $orderInfoDTO->created_timestamp;
        $orderDetailResp->close_countdown = time() - $orderInfoDTO->created_timestamp;
        $orderDetailResp->shipping_word = self::getOrderShippingWord($orderInfoDTO);
        $orderDetailResp->close_countdown = self::getCloseCountdown($orderInfoDTO);
        $shippingArray = [];
        $shippingArray['receiver'] = $orderInfoDTO->receiver;
        $shippingArray['address'] = $orderInfoDTO->address;
        $shippingArray['phone'] = $orderInfoDTO->phone;
        $shippingArray['province'] = $orderInfoDTO->province;
        $shippingArray['city'] = $orderInfoDTO->city;
        $shippingArray['area'] = $orderInfoDTO->area;
        $orderDetailResp->shipping_info = $shippingArray;

        return $orderDetailResp;

    }


    /**
     * @param OrderInfoDTO $orderInfoDTO
     * @return string|null
     * 获取物流订单文本
     */
    public static function getOrderShippingWord(OrderInfoDTO &$orderInfoDTO): ?string
    {

        $orderStatus = $orderInfoDTO->order_status;
        $order_close_type = $orderInfoDTO->order_close_type;
        $updateTimestamp = $orderInfoDTO->updated_timestamp;
        $shippingWord = null;

        if ($orderStatus == OrderInfoDTO::ORDER_STATUS_TO_PAY) {
            $shippingWord = '订单将于30分钟内关闭,请及时支付';
        } elseif ($orderStatus == OrderInfoDTO::ORDER_STATUS_HAS_PAY) {
            $shippingWord = '商品正在打包，请耐心等待';
        } elseif ($orderStatus == OrderInfoDTO::ORDER_STATUS_WAIT_SEND) {
            $shippingWord = '包裹已经打包完毕，请耐心等待';
        } elseif ($orderStatus == OrderInfoDTO::ORDER_STATUS_WAIT_RECEIVE) {
            $statusStrRet = LogisticsServiceImpl::getInstance()->getLogisticsStatusStr($orderInfoDTO->express_company, $orderInfoDTO->express_no);
            if (Result::isSuccess($statusStrRet)) {
                $shippingWord = $statusStrRet->data;
            } else {
                $shippingWord = '您的包裹已经寄出';
            }
        } elseif ($orderStatus == OrderInfoDTO::ORDER_STATUS_FINISHED) {
            $shippingWord = '您的包裹已签收，期待再次光临';
        } elseif ($orderStatus == OrderInfoDTO::ORDER_STATUS_CLOSED && $order_close_type == OrderInfoDTO::ORDER_CLOSE_TYPE_BY_HAND) {
            $shippingWord = '订单已于' . date('Y-m-d H:i:s', $updateTimestamp) . '手动关闭';
        } elseif ($orderStatus == OrderInfoDTO::ORDER_STATUS_CLOSED && $order_close_type == OrderInfoDTO::ORDER_CLOSE_TYPE_AUTO) {
            $shippingWord = '订单已于' . date('Y-m-d H:i:s', $updateTimestamp) . '超时未支付关闭';
        }

        return $shippingWord;

    }


    /**
     * @param int $orderStatus
     * @return string
     * 查询订单状态文本
     */
    public function queryOrderStatusWord(int $orderStatus)
    {
        if ($orderStatus == OrderInfoDTO::ORDER_STATUS_TO_PAY) {
            return '待付款';
        } elseif ($orderStatus == OrderInfoDTO::ORDER_STATUS_HAS_PAY) {
            return '已支付';
        } elseif ($orderStatus == OrderInfoDTO::ORDER_STATUS_WAIT_SEND) {
            return '待发货';
        } elseif ($orderStatus == OrderInfoDTO::ORDER_STATUS_WAIT_RECEIVE) {
            return '待收货';
        } elseif ($orderStatus == OrderInfoDTO::ORDER_STATUS_FINISHED) {
            return '交易完成';
        } elseif ($orderStatus == OrderInfoDTO::ORDER_STATUS_CLOSED) {
            return '交易关闭';
        } elseif ($orderStatus == OrderInfoDTO::ORDER_STATUS_WAIT_COMMENT) {
            return '待评价';
        } elseif ($orderStatus == OrderInfoDTO::ORDER_STATUS_HAS_COMMENT) {
            return '已评价';
        }

    }


    public function queryOrderPayTypeWord(?int $payType)
    {

        if ($payType == OrderInfoDTO::ALI_APP_PAY_TYPE) {
            return '支付宝';
        } elseif ($payType == OrderInfoDTO::WECHAT_PAY_TYPE) {
            return '微信';
        } elseif ($payType == OrderInfoDTO::ALI_WEB_PAY_TYPE) {
            return '网页支付宝';
        } elseif ($payType == OrderInfoDTO::WECHAT_H5_PAY_TYPE) {
            return '网页微信';
        } elseif ($payType == OrderInfoDTO::WECHAT_JS_API_PAY_TYPE) {
            return '小程序微信';
        }


        return null;

    }


    /**
     * @return array|null
     * 订单导出标题
     */
    public function queryOrderDataExportTitle(): ?array
    {
        $title = ['订单号', '商家编号', '商家名称', '账户信息', '付款时间', '订单状态', '商品ID', '商品名称', 'SKU编码', '商品规格', '物品数量', '结算价（元）',
            '买家实付金额及方式', '订单金额', '联系人', '联系电话', '省', '市', '区', '邮寄地址'];

        return $title;

    }


    /**
     * @param OrderInfoDTO $orderInfoDTO
     * @return OrderInfoResp|null
     * 转换管理端订单信息
     */
    public function buildOrderInfo(OrderInfoDTO $orderInfoDTO): ?OrderInfoResp
    {

        $orderInfoResp = new OrderInfoResp();
        /** @var OrderInfoDTO $orderInfoDTO */

        $orderAdminService = OrderAdminServiceImpl::getInstance();
        $orderProductRet = $orderAdminService->getOrderProductByOrderId($orderInfoDTO->id);
        if (!Result::isSuccess($orderProductRet)) {
            \FUR_Log::warn("getOrderProductByOrderId fail", json_encode($orderInfoDTO));
            return null;
        }
        /**
         * 买家信息
         */
        $userInfoRet = UserServiceImpl::getInstance()->getValidUserInfo($orderInfoDTO->user_id);
        if (Result::isSuccess($userInfoRet)) {
            $orderInfoResp->order_user_info = $userInfoRet->data;
        }
        $orderInfoResp->order_product_info = $this->conversionOrderProduct($orderProductRet->data);
        $orderInfoResp->order_base_info = $orderInfoDTO;

        return $orderInfoResp;
    }


    /**
     * @param OrderProductDTO $orderProductsDTO
     * @return OrderProductDTO|null
     * 转换订单产品信息
     */
    private function conversionOrderProduct(OrderProductDTO &$orderProductsDTO): ?OrderProductDTO
    {

        $orderProductsDTO->product_specifications = json_decode($orderProductsDTO->product_specifications);
        $orderProductsDTO->shipping_price = PriceHelper::conversionDisplayPrice($orderProductsDTO->shipping_price);
        $orderProductsDTO->sku_price = PriceHelper::conversionDisplayPrice($orderProductsDTO->sku_price);
        $orderProductsDTO->total_price = PriceHelper::conversionDisplayPrice($orderProductsDTO->total_price);
        return $orderProductsDTO;

    }


    /**
     * @param array $allOrderListData
     * @return array|null
     *转换订单导出信息
     */
    public function conversionOrderDataExportInfo(array $allOrderListData): ?array
    {

        $n = 1;
        foreach ($allOrderListData as $orderInfoDTO) {
            $orderInfoResp = $this->buildOrderInfo($orderInfoDTO);
            if (!$orderInfoResp) {
                \FUR_Log::warn("getOrderBuildInfo fail", json_encode($orderInfoDTO));
                continue;
            }
            $OrderInfoConversionUtil = OrderUtil::getInstance();
            $orderProductInfo = $orderInfoResp->order_product_info;
            $orderProductBaseInfo = $orderInfoResp->order_base_info;
            $orderExportData[$n][] = 'xh' . $orderProductBaseInfo->order_sn;//订单号
            $orderExportData[$n][] = null;//商家编号
            $orderExportData[$n][] = '博源电子商务有限公司';//商家名称
            $orderExportData[$n][] = null;//账户信息
            $orderExportData[$n][] = date('Y-m-d H:i:s', $orderProductBaseInfo->pay_timestamp);//付款时间
            $orderExportData[$n][] = $OrderInfoConversionUtil->queryOrderStatusWord($orderProductBaseInfo->order_status);//$orderProductBaseInfo->order_status;//订单状态
            $orderExportData[$n][] = $orderProductInfo->sku_id;//商品ID
            $orderExportData[$n][] = $orderProductInfo->product_name;//商品名称
            $orderExportData[$n][] = null;//SKU编码
            $orderExportData[$n][] = $this->conversionOrderSpecific($orderProductInfo->product_specifications);//商品规格
            $orderExportData[$n][] = $orderProductInfo->count;//物品数量
            $payType = $OrderInfoConversionUtil->queryOrderPayTypeWord($orderProductBaseInfo->pay_type);
            $orderExportData[$n][] = $orderProductInfo->total_price;//结算价（元）
            if ($orderProductBaseInfo->pay_status != OrderInfoDTO::PAY_STATUS_FINISHED) {//没有成交
                $orderExportData[$n][] = 0;
            } else {
                $orderExportData[$n][] = $orderProductInfo->total_price . '-' . $payType;//买家实付金额及方式
            }
            $orderExportData[$n][] = $orderProductInfo->total_price;//订单金额
            $orderExportData[$n][] = $orderProductBaseInfo->receiver;//联系人
            $orderExportData[$n][] = $orderProductBaseInfo->phone;//联系电话
            $orderExportData[$n][] = $orderProductBaseInfo->province;//省
            $orderExportData[$n][] = $orderProductBaseInfo->city;//市
            $orderExportData[$n][] = $orderProductBaseInfo->area;//区
            $orderExportData[$n][] = $orderProductBaseInfo->address;//邮寄地址
            $n++;

        }

        return $orderExportData;

    }


    /**
     * @param array $specification
     * @return string|null转换商品规格
     */
    private function conversionOrderSpecific(array $specification): ?string
    {
        $array = [];
        if (count($specification)) {
            foreach ($specification as $k => $specificObj) {
                $array[$specificObj->name] = $specificObj->attr;
            }
        }
        $array = json_encode($array, JSON_UNESCAPED_UNICODE);
        return $array;

    }


    private static function getCloseCountdown(OrderInfoDTO $orderInfoDTO): ?int
    {
        if ($orderInfoDTO->order_status != 10) {
            return null;
        }

        if ($orderInfoDTO->pay_status == 1) {
            return null;
        }
        $close_countdown = 1800 - (time() - $orderInfoDTO->created_timestamp);
        if ($close_countdown < 0) {
            $close_countdown = 0;
        }
        return $close_countdown;

    }
}