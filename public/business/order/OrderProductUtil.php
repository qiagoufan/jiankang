<?php


namespace business\order;


use facade\response\order\product\OrderProductSpecificResp;
use model\product\dto\ProductSkuDTO;

class OrderProductUtil
{


    public static function conversionShoppingCartOrderProductSpecific(ProductSkuDTO &$productSkuDTO): ?OrderProductSpecificResp
    {
        $productSpecificationResp = new OrderProductSpecificResp();
        if (isset($productSkuDTO->indexes)) {
            $productSpecificationResp->index = $productSkuDTO->indexes;
        }
        $productSpecificationResp->specification = json_decode($productSkuDTO->specifications);
        return $productSpecificationResp;
    }


    public static function conversionOrderProductSpecific(OrderProductsDTO &$orderProductsDTO): ?OrderProductSpecificResp
    {
        $productSpecificationResp = new OrderProductSpecificResp();

        $productSpecificationResp->specification = json_decode($orderProductsDTO->product_specifications);
        return $productSpecificationResp;
    }

    public static function buildSpecificStr(?OrderProductSpecificResp &$orderProductSpecificResp): ?string
    {
        $display_product_specification_info = '';
        if ($orderProductSpecificResp->specification == null || empty($orderProductSpecificResp->specification)) {
            return $display_product_specification_info;
        }

        foreach ($orderProductSpecificResp->specification as $specification) {
            $display_product_specification_info .= $specification->attr . ";";
        }
        $display_product_specification_info = mb_substr($display_product_specification_info, 0, -1);
        return $display_product_specification_info;
    }

}