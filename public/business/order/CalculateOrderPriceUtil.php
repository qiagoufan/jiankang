<?php


namespace business\order;


use business\promotion\VoucherConversionUtil;
use facade\request\order\CalculateOrderPriceParams;
use facade\request\order\product\OrderProductParams;
use facade\response\order\CalculatePriceResp;
use facade\response\order\product\OrderProductResp;
use facade\response\Result;
use lib\PriceHelper;
use model\product\dto\ProductItemDTO;
use model\product\dto\ProductSkuDTO;
use model\promotion\dto\PromotionVoucherEntityDTO;
use service\product\impl\ProductServiceImpl;
use service\promotion\impl\PromotionVoucherServiceImpl;

class CalculateOrderPriceUtil
{

    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    const FREE_SHIPPING_PRICE = 9900;
    const SHIPPING_PRICE = 0;

    public function calculatePrice(CalculateOrderPriceParams &$calculateOrderPriceParams): ?Result
    {
        $result = new Result();
        $calculatePriceResp = new CalculatePriceResp();
        $totalProductPrice = 0;
        $productCount = 0;
        $product_list = $calculateOrderPriceParams->product_list;

        $calculatePriceResp->order_product_list = [];
        /** @var  OrderProductParams $orderProductInfo */
        foreach ($product_list as $orderProductInfo) {
            if ($orderProductInfo->count == 0) {
                $result->setError(\Config_Error::ERR_ORDER_SKU_COUNT_IS_ZERO);
                return $result;
            }

            $skuInfoRet = ProductServiceImpl::getInstance()->getProductSkuDetail($orderProductInfo->sku_id);

            if (Result::isSuccess($skuInfoRet) == false) {
                continue;
            }
            /** @var ProductSkuDTO $skuInfoDTO */
            $skuInfoDTO = $skuInfoRet->data;
            $inStock = 1;
            if ($skuInfoDTO->stock < $orderProductInfo->count) {
                $inStock = 0;
            }

            $itemInfoRet = ProductServiceImpl::getInstance()->getProductItem($skuInfoDTO->item_id);

            /** @var ProductItemDTO $itemInfo */
            $itemInfo = $itemInfoRet->data;

            $productPrice = $skuInfoDTO->current_price;
            $totalProductPrice = $productPrice * $orderProductInfo->count + $totalProductPrice;
//            $totalSuperValue = $skuInfoDTO->power_value * $orderProductInfo->count + $totalSuperValue;
            $productCount += $orderProductInfo->count;
            $orderProductResp = new OrderProductResp();
            $orderProductResp->item_id = $skuInfoDTO->item_id;
            $orderProductResp->sku_id = $skuInfoDTO->id;
            $orderProductResp->stock = $skuInfoDTO->stock;
            $orderProductResp->in_stock = $inStock;
            $orderProductResp->count = $orderProductInfo->count;
            $orderProductResp->publish_status = $skuInfoDTO->publish_status;
            $orderProductResp->sku_price = $productPrice;
            $orderProductResp->display_sku_price = PriceHelper::conversionDisplayPrice($productPrice);
            $orderProductResp->total_price = $productPrice * $orderProductInfo->count;
            $orderProductResp->product_name = $itemInfo->product_name;
            $orderProductResp->main_image = $itemInfo->main_image;
            $orderProductResp->sku_name = $skuInfoDTO->sku_name;

            array_push($calculatePriceResp->order_product_list, $orderProductResp);
        }
        $calculatePriceResp->product_total_price = $totalProductPrice;
        if ($totalProductPrice < self::FREE_SHIPPING_PRICE) {
            $calculatePriceResp->shipping_price = self::SHIPPING_PRICE;
        }

        $calculatePriceResp->product_count = $productCount;
        $voucherSn = $calculateOrderPriceParams->voucher_sn;
        $voucherSnList = $calculateOrderPriceParams->voucher_sn_list;
        $discountPrice = 0;
        //自动选择优惠券
        if ($voucherSn != null) {
            $discountPrice = $this->getVoucherDiscountPrice($totalProductPrice, $voucherSn);
            if ($discountPrice == null) {
                $result->setError(\Config_Error::ERR_VOUCHER_CANT_USE);
                return $result;
            }
        } elseif ($voucherSnList != null) {
            foreach ($voucherSnList as $voucherSnItem) {
                $voucherDiscountPrice = $this->getVoucherDiscountPrice($totalProductPrice, $voucherSnItem);
                if ($voucherDiscountPrice == null) {
                    $result->setError(\Config_Error::ERR_VOUCHER_CANT_USE);
                    return $result;
                }
                $discountPrice = $discountPrice + $voucherDiscountPrice;
            }
        } elseif ($calculateOrderPriceParams->need_chose_voucher) {
            $discountPrice = $this->getBestDiscountPrice($calculateOrderPriceParams, $totalProductPrice, $voucherSn);

        }
        if ($discountPrice != 0) {
            $calculatePriceResp->voucher_sn = $voucherSn;
            $calculatePriceResp->voucher_sn_list = $voucherSnList;
        }
        $product_total_price = $calculatePriceResp->product_total_price;
        if ($product_total_price < $discountPrice) {
            $discountPrice = $product_total_price;
        }
        $calculatePriceResp->pay_price = $product_total_price - $discountPrice;
        $calculatePriceResp->pay_price = $calculatePriceResp->pay_price + $calculatePriceResp->shipping_price;
        //计算全单折扣金额
        $discountRate = 1;
        if ($calculatePriceResp->product_total_price) {
            $discountRate = ($calculatePriceResp->pay_price - $calculatePriceResp->shipping_price) / $calculatePriceResp->product_total_price;
        }
        //计算每个商品的折扣后价格
        foreach ($calculatePriceResp->order_product_list as $orderProductResp) {
            $orderProductResp->pay_price = intval($orderProductResp->total_price * $discountRate);
            $orderProductResp->discount_rate = $discountRate;
            $orderProductResp->discount_price = $orderProductResp->total_price - $orderProductResp->pay_price;
        }

        $calculatePriceResp->discount_price = $discountPrice;
        $calculatePriceResp->display_pay_price = PriceHelper::conversionDisplayPrice($calculatePriceResp->pay_price);
        $calculatePriceResp->display_product_total_price = PriceHelper::conversionDisplayPrice($calculatePriceResp->product_total_price);
        $calculatePriceResp->display_discount_price = PriceHelper::conversionDisplayPrice($calculatePriceResp->discount_price);
        $calculatePriceResp->display_shipping_price = PriceHelper::conversionDisplayPrice($calculatePriceResp->shipping_price);
        $result->setSuccessWithResult($calculatePriceResp);
        return $result;
    }


    private function getVoucherDiscountPrice(int $product_total_price, ?string &$voucherSn): int
    {
        $voucherEntityRet = PromotionVoucherServiceImpl::getInstance()->getVoucherEntityBySn($voucherSn);
        if (!Result::isSuccess($voucherEntityRet)) {
            return 0;
        }
        $voucherDetail = VoucherConversionUtil::buildValidVoucherDetail($voucherEntityRet->data);
        if ($voucherDetail == null) {
            return 0;
        }
        //可用价格过滤
        if ($voucherDetail->price_limit >= $product_total_price) {
            return 0;
        }
        return $voucherDetail->amount;
    }

    private function getBestDiscountPrice(CalculateOrderPriceParams &$calculateOrderPriceParams, int $product_total_price, ?string &$voucherSn): ?int
    {
        $userId = $calculateOrderPriceParams->user_id;


        $best_discount = 0;

        $userVoucherListRet = PromotionVoucherServiceImpl::getInstance()->queryUserVoucherList($userId);
        if (!Result::isSuccess($userVoucherListRet)) {
            return 0;
        }
        $voucherEntityList = $userVoucherListRet->data;
        /** @var PromotionVoucherEntityDTO $voucherEntity */
        foreach ($voucherEntityList as $voucherEntity) {
            $voucherDetail = VoucherConversionUtil::buildValidVoucherDetail($voucherEntity);
            if ($voucherDetail == null) {
                continue;
            }
            //可用价格过滤
            if ($voucherDetail->price_limit >= $product_total_price) {
                continue;
            }
            //不是最大面额的不要
            if ($voucherDetail->amount < $best_discount) {
                continue;
            }
            $voucherSn = $voucherEntity->voucher_sn;
            $best_discount = $voucherDetail->amount;
        }
        return $best_discount;
    }


}