<?php

namespace business\order;


use business\user\UserInfoConversionUtil;
use facade\request\order\CreateOrderParams;
use facade\response\admin\OrderInfoResp;
use facade\response\order\CalculatePriceResp;
use facade\response\order\OrderDetailResp;
use facade\response\order\OrderDisplayInfoResp;
use facade\response\order\OrderTransactionResp;
use facade\response\order\product\OrderProductResp;
use facade\response\Result;
use lib\PriceHelper;
use lib\TimeHelper;
use model\order\dto\OrderInfoDTO;
use model\order\dto\OrderProductDTO;
use service\admin\impl\OrderAdminServiceImpl;
use service\logistics\impl\LogisticsServiceImpl;
use service\order\impl\OrderServiceImpl;
use service\user\impl\ShippingAddressServiceImpl;
use service\user\impl\UserServiceImpl;

class OrderInfoConversionUtil
{

    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    private static function makeOrderSn()
    {
        return date('YmdHis') . mt_rand(10000, 99999);
    }


    public static function buildOrderInfoDTO(CalculatePriceResp &$calculatePriceResp, CreateOrderParams &$createOrderParams): ?Result
    {
        \FUR_Log::debug("calculatePriceResp", $calculatePriceResp);
        $result = new Result();
        $orderInfoDTO = new OrderInfoDTO();
        $orderInfoDTO->order_sn = self::makeOrderSn();
        $orderInfoDTO->order_status = \Config_Const::ORDER_STATUS_TO_PAY;
        $orderInfoDTO->pay_status = \Config_Const::PAY_STATUS_NO_PAY;
        $orderInfoDTO->total_price = $calculatePriceResp->pay_price + $calculatePriceResp->discount_price;
        $orderInfoDTO->pay_price = $calculatePriceResp->pay_price;
        $shippingAddressDetailRet = ShippingAddressServiceImpl::getInstance()->getShippingAddressDetail($createOrderParams->user_id, $createOrderParams->shipping_id);
        if (!Result::isSuccess($shippingAddressDetailRet)) {
            $result->setError(\Config_Error::ERR_SHIPPING_INFO_IS_NOT_EXIST);
            return $result;
        }
        $shippingAddressDetailRet->data->id = null;
        \FUR_Core::copyProperties($shippingAddressDetailRet->data, $orderInfoDTO);

        $orderInfoDTO->user_id = $createOrderParams->user_id;
        $orderInfoDTO->created_timestamp = time();
        $orderInfoDTO->updated_timestamp = time();
        $orderInfoDTO->delete_status = 0;
        $orderInfoDTO->platform = $createOrderParams->platform;
        $orderInfoDTO->shipping_price = $calculatePriceResp->shipping_price;
        $orderInfoDTO->discount_price = $calculatePriceResp->discount_price;
        $orderInfoDTO->voucher_sn = $calculatePriceResp->voucher_sn;
        if ($calculatePriceResp->voucher_sn_list) {
            $orderInfoDTO->voucher_sn = implode(",", $calculatePriceResp->voucher_sn_list);
        }

        $result->setSuccessWithResult($orderInfoDTO);
        return $result;
    }

    public static function conversionOrderProductDTO(OrderProductResp &$orderProductResp, int $orderId): ?OrderProductDTO
    {
        $orderProductDTO = new OrderProductDTO();
        \FUR_Core::copyProperties($orderProductResp, $orderProductDTO);
        $orderProductDTO->order_id = $orderId;
        $orderProductDTO->shipping_price = 0;
        $orderProductDTO->created_timestamp = time();
        $orderProductDTO->updated_timestamp = time();
        return $orderProductDTO;
    }


    public static function conversionOrderDetailResp(?OrderInfoDTO &$orderInfoDTO): ?OrderDetailResp
    {
        $orderDetailResp = new OrderDetailResp();
        $orderDetailResp->order_id = $orderInfoDTO->id;
        $orderDetailResp->order_sn = $orderInfoDTO->order_sn;
        $orderDetailResp->order_tips = null;
        $orderDetailResp->shipping_info = self::getOrderShippingInfo($orderInfoDTO);
        $orderDetailResp->pay_price = $orderInfoDTO->pay_price;
        $orderDetailResp->display_pay_price = PriceHelper::conversionDisplayPrice($orderDetailResp->pay_price);
        $orderDetailResp->discount_price = intval($orderInfoDTO->discount_price);
        $orderDetailResp->display_discount_price = PriceHelper::conversionDisplayPrice($orderDetailResp->discount_price);
        $orderDetailResp->shipping_price = intval($orderInfoDTO->shipping_price);
        $orderDetailResp->display_shipping_price = PriceHelper::conversionDisplayPrice($orderDetailResp->shipping_price);
        $orderDetailResp->total_price = $orderInfoDTO->total_price;
        $orderDetailResp->display_total_price = PriceHelper::conversionDisplayPrice($orderDetailResp->total_price);
        $orderDetailResp->created_timestamp = $orderInfoDTO->created_timestamp;
        $orderDetailResp->display_created_time = date('Y-m-d H:i:s', $orderInfoDTO->created_timestamp);
        $orderDetailResp->order_status = $orderInfoDTO->order_status;
        $orderDetailResp->pay_status = $orderInfoDTO->pay_status;
        $orderDetailResp->pay_timestamp = $orderInfoDTO->pay_timestamp;
        $orderProductListRet = OrderServiceImpl::getInstance()->queryProductListByOrderId($orderInfoDTO->id);
        if (!Result::isSuccess($orderProductListRet)) {
            return $orderDetailResp;
        }
        $orderDetailResp->transaction_info = self::getOrderTransactionInfo($orderInfoDTO);
        $orderDetailResp->user_info = UserInfoConversionUtil::getInstance()->getSimpleUserInfoResp($orderInfoDTO->user_id, true);
        $orderDetailResp->order_product_list = [];
        $orderProductList = $orderProductListRet->data;
        /** @var OrderProductDTO $orderProductDTO */
        foreach ($orderProductList as $orderProductDTO) {
            $orderProductResp = new OrderProductResp();
            \FUR_Core::copyProperties($orderProductDTO, $orderProductResp);
            $orderProductResp->display_sku_price = PriceHelper::conversionDisplayPrice($orderProductDTO->sku_price);
            $orderProductResp->display_pay_price = PriceHelper::conversionDisplayPrice($orderProductDTO->pay_price);
            $orderProductResp->display_total_price = PriceHelper::conversionDisplayPrice($orderProductDTO->total_price);
            array_push($orderDetailResp->order_product_list, $orderProductResp);
        }

        return $orderDetailResp;
    }


    /**
     * @param OrderInfoDTO $orderInfoDTO
     * @return OrderTransactionResp|null
     * 获取交易信息
     */
    public static function getOrderTransactionInfo(OrderInfoDTO &$orderInfoDTO): ?OrderTransactionResp
    {
        $orderTransactionResp = new OrderTransactionResp();
        $orderTransactionResp->pay_type = $orderInfoDTO->pay_type;
        $orderTransactionResp->pay_timestamp = $orderInfoDTO->pay_timestamp;
        return $orderTransactionResp;
    }


    /**
     * @param OrderInfoDTO $orderInfoDTO
     * @return array|null
     * 获取订单物流信息
     */
    public static function getOrderShippingInfo(OrderInfoDTO &$orderInfoDTO): ?array
    {
        $shippingInfoArr = [];
        $shippingInfoArr['receiver'] = $orderInfoDTO->receiver;
        $shippingInfoArr['address'] = $orderInfoDTO->address;
        $shippingInfoArr['phone'] = $orderInfoDTO->phone;
        $shippingInfoArr['province'] = $orderInfoDTO->province;
        $shippingInfoArr['city'] = $orderInfoDTO->city;
        $shippingInfoArr['area'] = $orderInfoDTO->area;
        $shippingInfoArr['express_company'] = $orderInfoDTO->express_company;
        $shippingInfoArr['express_no'] = $orderInfoDTO->express_no;

        return $shippingInfoArr;
    }

    /**
     * @param OrderInfoDTO $orderInfoDTO
     * @return string|null
     * 获取物流订单文本
     */
    public static function getOrderTips(OrderInfoDTO &$orderInfoDTO): string
    {

        $orderStatus = $orderInfoDTO->order_status;
        $order_close_type = $orderInfoDTO->order_close_type;
        $updateTimestamp = $orderInfoDTO->updated_timestamp;
        $shippingWord = null;
        $payTimestamp = $orderInfoDTO->pay_timestamp;
        if ($orderStatus == \Config_Const::ORDER_STATUS_TO_PAY) {
            $shippingWord = '订单将于30分钟内关闭,请及时支付';
        } elseif ($orderStatus == \Config_Const::ORDER_STATUS_HAS_PAY) {
            if (time() - $payTimestamp > 300) {
                $shippingWord = '商品正在打包，请耐心等待';
            } else {
                $shippingWord = '订单已支付成功,商品即将打包';
            }
        } elseif ($orderStatus == \Config_Const::ORDER_STATUS_WAIT_SEND) {
            $shippingWord = '包裹已经打包完毕，请耐心等待';
        } elseif ($orderStatus == \Config_Const::ORDER_STATUS_WAIT_RECEIVE) {
            $statusStrRet = LogisticsServiceImpl::getInstance()->getLogisticsStatusStr($orderInfoDTO->express_company, $orderInfoDTO->express_no);
            if (Result::isSuccess($statusStrRet)) {
                $shippingWord = $statusStrRet->data;
            } else {
                $shippingWord = '您的包裹已经寄出';
            }
        } elseif ($orderStatus == \Config_Const::ORDER_STATUS_FINISHED) {
            $shippingWord = '订单已经完成，期待再次光临';
        } elseif ($orderStatus == \Config_Const::ORDER_STATUS_CLOSED && $order_close_type == \Config_Const::ORDER_CLOSE_TYPE_BY_HAND) {
            $shippingWord = '订单已于' . date('Y-m-d H:i:s', $updateTimestamp) . '手动关闭';
        } elseif ($orderStatus == \Config_Const::ORDER_STATUS_CLOSED && $order_close_type == \Config_Const::ORDER_CLOSE_TYPE_AUTO) {
            $shippingWord = '订单已于' . date('Y-m-d H:i:s', $updateTimestamp) . '超时未支付关闭';
        }

        return $shippingWord;

    }


    /**
     * @param int $orderStatus
     * @return string
     * 查询订单状态文本
     */
    public function queryOrderStatusWord(int $orderStatus)
    {
        if ($orderStatus == OrderInfoDTO::ORDER_STATUS_TO_PAY) {
            return '待付款';
        } elseif ($orderStatus == OrderInfoDTO::ORDER_STATUS_HAS_PAY) {
            return '已支付';
        } elseif ($orderStatus == OrderInfoDTO::ORDER_STATUS_WAIT_SEND) {
            return '待发货';
        } elseif ($orderStatus == OrderInfoDTO::ORDER_STATUS_WAIT_RECEIVE) {
            return '待收货';
        } elseif ($orderStatus == OrderInfoDTO::ORDER_STATUS_FINISHED) {
            return '交易完成';
        } elseif ($orderStatus == OrderInfoDTO::ORDER_STATUS_CLOSED) {
            return '交易关闭';
        } elseif ($orderStatus == OrderInfoDTO::ORDER_STATUS_WAIT_COMMENT) {
            return '待评价';
        } elseif ($orderStatus == OrderInfoDTO::ORDER_STATUS_HAS_COMMENT) {
            return '已评价';
        }

        return null;
    }


    public function queryOrderPayTypeWord(?int $payType)
    {

        if ($payType == OrderInfoDTO::ALI_APP_PAY_TYPE) {
            return '支付宝';
        } elseif ($payType == OrderInfoDTO::WECHAT_PAY_TYPE) {
            return '微信';
        } elseif ($payType == OrderInfoDTO::ALI_WEB_PAY_TYPE) {
            return '网页支付宝';
        } elseif ($payType == OrderInfoDTO::WECHAT_H5_PAY_TYPE) {
            return '网页微信';
        } elseif ($payType == OrderInfoDTO::WECHAT_JS_API_PAY_TYPE) {
            return '小程序微信';
        }


        return null;

    }


    /**
     * @return array|null
     * 订单导出标题
     */
    public function queryOrderDataExportTitle(): ?array
    {
        $title = ['订单号', '商家编号', '商家名称', '账户信息', '付款时间', '订单状态', '商品ID', '商品名称', 'SKU编码', '商品规格', '物品数量', '结算价（元）',
            '买家实付金额及方式', '订单金额', '联系人', '联系电话', '省', '市', '区', '邮寄地址'];

        return $title;

    }


    /**
     * @param OrderInfoDTO $orderInfoDTO
     * @return OrderInfoResp|null
     * 转换管理端订单信息
     */
    public function buildOrderInfo(OrderInfoDTO $orderInfoDTO): ?OrderInfoResp
    {

        $orderInfoResp = new OrderInfoResp();
        /** @var OrderInfoDTO $orderInfoDTO */

        $orderAdminService = OrderAdminServiceImpl::getInstance();
        $orderProductRet = $orderAdminService->getOrderProductByOrderId($orderInfoDTO->id);
        if (!Result::isSuccess($orderProductRet)) {
            return null;
        }

        $orderProductArr = [];
        foreach ($orderProductRet->data as $orderProductDTO) {
            $orderProductDTO = $this->conversionOrderProduct($orderProductDTO);
            array_push($orderProductArr, $orderProductDTO);
        }

        /**
         * 买家信息
         */
        $userInfoRet = UserServiceImpl::getInstance()->getValidUserInfo($orderInfoDTO->user_id);
        if (Result::isSuccess($userInfoRet)) {
            $orderInfoResp->order_user_info = $userInfoRet->data;
        }
        $orderInfoResp->order_product_info = $orderProductArr;
        $orderInfoResp->order_base_info = $this->conversionOrderBaseInfo($orderInfoDTO);

        return $orderInfoResp;
    }


    private function conversionOrderBaseInfo(OrderInfoDTO &$orderInfoDTO): ?OrderInfoDTO
    {
        $orderInfoDTO->pay_price = PriceHelper::conversionDisplayPrice($orderInfoDTO->pay_price);
        $orderInfoDTO->total_price = PriceHelper::conversionDisplayPrice($orderInfoDTO->total_price);
        return $orderInfoDTO;
    }


    /**
     * 转换订单产品信息
     */
    private function conversionOrderProduct(OrderProductDTO &$orderProductsDTO): ?OrderProductDTO
    {

        $orderProductsDTO->product_specifications = json_decode($orderProductsDTO->product_specifications);
        $orderProductsDTO->shipping_price = PriceHelper::conversionDisplayPrice($orderProductsDTO->shipping_price);
        $orderProductsDTO->sku_price = PriceHelper::conversionDisplayPrice($orderProductsDTO->sku_price);
        $orderProductsDTO->total_price = PriceHelper::conversionDisplayPrice($orderProductsDTO->total_price);
        return $orderProductsDTO;

    }


    public function conversionOrderPriceInfo(OrderInfoDTO &$orderInfoDTO): ?OrderDisplayInfoResp
    {
        /** @var OrderDisplayInfoResp $orderDisplayInfoResp */
        $orderDisplayInfoResp = new OrderDisplayInfoResp();
        \FUR_Core::copyProperties($orderInfoDTO, $orderDisplayInfoResp);
        $orderDisplayInfoResp->pay_price = PriceHelper::conversionDisplayPrice($orderDisplayInfoResp->pay_price);
        $orderDisplayInfoResp->total_price = PriceHelper::conversionDisplayPrice($orderDisplayInfoResp->total_price);
        $orderDisplayInfoResp->shipping_price = PriceHelper::conversionDisplayPrice($orderDisplayInfoResp->shipping_price);
        $orderDisplayInfoResp->discount_price = PriceHelper::conversionDisplayPrice($orderDisplayInfoResp->discount_price);

        return $orderDisplayInfoResp;
    }


    /**
     * @param array $allOrderListData
     * @return array|null
     *转换订单导出信息
     */
    public function conversionOrderDataExportInfo(array $allOrderListData): ?array
    {

        $n = 1;
        $orderExportData = [];
        foreach ($allOrderListData as $orderInfoDTO) {
            //todo:订单导出
            $orderInfoResp = $this->buildOrderInfo($orderInfoDTO);
            if (!$orderInfoResp) {
                \FUR_Log::warn("getOrderBuildInfo fail", json_encode($orderInfoDTO));
                continue;
            }


            $OrderInfoConversionUtil = OrderInfoConversionUtil::getInstance();
            $orderProductInfo = $orderInfoResp->order_product_info;


            $orderProductArr = [];
            foreach ($orderProductInfo as $orderProduct) {
                $orderProductArr['item_id'] = $orderProduct->item_id;
                $orderProductArr['sku_id'] = $orderProduct->sku_id . ';';
                $orderProductArr['product_name'] = $orderProduct->product_name;
                $orderProductArr['specifications'] = $this->conversionOrderSpecific($orderProduct->product_specifications) . ';';
                $orderProductArr['count'] += $orderProduct->count;
                $orderProductArr['totalPrice'] += $orderProduct->total_price;//结算价（元）
            }

            $orderProductBaseInfo = $orderInfoResp->order_base_info;
            $orderExportData[$n][] = 'xh' . $orderProductBaseInfo->order_sn;//订单号
            $orderExportData[$n][] = null;//商家编号
            $orderExportData[$n][] = '';//商家名称
            $orderExportData[$n][] = null;//账户信息
            $orderExportData[$n][] = date('Y-m-d H:i:s', $orderProductBaseInfo->pay_timestamp);//付款时间
            $orderExportData[$n][] = $OrderInfoConversionUtil->queryOrderStatusWord($orderProductBaseInfo->order_status);//$orderProductBaseInfo->order_status;//订单状态
            $orderExportData[$n][] = $orderProductArr['item_id'];//商品ID
            $orderExportData[$n][] = $orderProductArr['product_name'];//商品名称
            $orderExportData[$n][] = $orderProductArr['sku_id'];;//SKU编码
            $orderExportData[$n][] = $orderProductArr['specifications'];//商品规格
            $orderExportData[$n][] = $orderProductArr['count'];//物品数量
            $payType = $OrderInfoConversionUtil->queryOrderPayTypeWord($orderProductBaseInfo->pay_type);
            $orderExportData[$n][] = $orderProductArr['totalPrice'];//结算价（元）
            if ($orderProductBaseInfo->pay_status != OrderInfoDTO::PAY_STATUS_FINISHED) {//没有成交
                $orderExportData[$n][] = 0;
            } else {
                $orderExportData[$n][] = $orderProductArr['totalPrice'] . '-' . $payType;//买家实付金额及方式
            }
            $orderExportData[$n][] = $orderProductBaseInfo->pay_price;//订单金额
            $orderExportData[$n][] = $orderProductBaseInfo->receiver;//联系人
            $orderExportData[$n][] = $orderProductBaseInfo->phone;//联系电话
            $orderExportData[$n][] = $orderProductBaseInfo->province;//省
            $orderExportData[$n][] = $orderProductBaseInfo->city;//市
            $orderExportData[$n][] = $orderProductBaseInfo->area;//区
            $orderExportData[$n][] = $orderProductBaseInfo->address;//邮寄地址
            $n++;

        }


        return $orderExportData;

    }


    /**
     * @param array $specification
     * @return string|null转换商品规格
     */
    private function conversionOrderSpecific(array $specification): ?string
    {
        $array = [];
        if (count($specification)) {
            foreach ($specification as $k => $specificObj) {
                $array[$specificObj->name] = $specificObj->attr;
            }
        }
        $array = json_encode($array, JSON_UNESCAPED_UNICODE);
        return $array;

    }

}