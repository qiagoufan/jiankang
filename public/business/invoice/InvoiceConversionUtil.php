<?php

namespace business\invoice;

use facade\response\invoice\InvoiceInfoResp;
use facade\response\invoice\order\InvoiceOrderResp;
use facade\response\Result;
use lib\PriceHelper;
use model\invoice\dto\InvoiceInfoDTO;
use model\invoice\dto\InvoiceOrderListDTO;
use model\organization\dto\OrganizationCompanyDTO;
use model\organization\dto\OrganizationStoreDTO;
use model\repair\dto\RepairOrderInfoDTO;
use service\invoice\impl\InvoiceServiceImpl;
use service\organization\impl\OrganizationServiceImpl;
use service\repair\impl\RepairOrderServiceImpl;

class InvoiceConversionUtil
{


    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function buildInvoiceResp(?InvoiceInfoDTO &$invoiceInfoDTO): ?InvoiceInfoResp
    {
        if ($invoiceInfoDTO == null) {
            return null;
        }
        $invoiceInfoResp = new InvoiceInfoResp();
        $invoiceInfoResp->invoice_id = $invoiceInfoDTO->id;
        $invoiceInfoResp->created_timestamp = $invoiceInfoDTO->created_timestamp;
        $invoiceInfoResp->total_count = $invoiceInfoDTO->total_count;
        $invoiceInfoResp->invoicing_status = $invoiceInfoDTO->invoicing_status;
        $invoiceInfoResp->total_amount = PriceHelper::conversionDisplayPrice($invoiceInfoDTO->total_amount);
        $invoiceInfoResp->order_list = $this->buildInvoiceOrderList($invoiceInfoDTO);

        $companyInfoRet = OrganizationServiceImpl::getInstance()->getCompanyInfo($invoiceInfoDTO->company_id);

        /** @var OrganizationCompanyDTO $companyInfo */
        $companyInfo = $companyInfoRet->data;
        $invoiceInfoResp->company_name = $companyInfo->company_name;
        return $invoiceInfoResp;
    }


    private function buildInvoiceOrderList(?InvoiceInfoDTO &$invoiceInfoDTO): ?array
    {
        $orderList = [];
        $invoiceOrderListRet = InvoiceServiceImpl::getInstance()->queryInvoiceOrderList($invoiceInfoDTO->id);
        if (Result::isSuccess($invoiceOrderListRet) == false) {
            return $orderList;
        }

        try {
            /** @var InvoiceOrderListDTO $invoiceOrderInfo */
            foreach ($invoiceOrderListRet->data as $invoiceOrderInfo) {
                $invoiceOrderResp = new InvoiceOrderResp();
                $repairOrderInfoRet = RepairOrderServiceImpl::getInstance()->getRepairOrder($invoiceOrderInfo->repair_order_id);
                /** @var RepairOrderInfoDTO $repairOrderInfo */
                $repairOrderInfo = $repairOrderInfoRet->data;

                $invoiceOrderResp->repair_order_id = $repairOrderInfo->id;
                $invoiceOrderResp->customer_price = PriceHelper::conversionDisplayPrice($repairOrderInfo->customer_price);
                $invoiceOrderResp->created_timestamp = $repairOrderInfo->created_timestamp;
                $invoiceOrderResp->appointment_timestamp = $repairOrderInfo->appointment_timestamp;
                $invoiceOrderResp->store_manager_phone = $repairOrderInfo->store_manager_phone;
                $invoiceOrderResp->store_manager_name = $repairOrderInfo->store_manager_name;
                $invoiceOrderResp->address = $repairOrderInfo->customer_address;
                $invoiceOrderResp->store_id = $repairOrderInfo->store_id;
                $storeInfoRet = OrganizationServiceImpl::getInstance()->getStoreInfo($repairOrderInfo->store_id);

                /** @var OrganizationStoreDTO $storeInfoDTO */
                $storeInfoDTO = $storeInfoRet->data;
                $invoiceOrderResp->store_name = $storeInfoDTO->store_name;
                $invoiceOrderResp->store_num = $storeInfoDTO->store_num;

                array_push($orderList, $invoiceOrderResp);
            }
        } catch (\Exception $e) {
        }

        return $orderList;
    }
}