<?php

namespace business\repair;

use facade\request\maintain\GetMaintainTaskRankParams;
use facade\request\user\FollowParams;
use facade\response\repair\order\RepairCustomerInfoResp;
use facade\response\repair\order\RepairEngineerResp;
use facade\response\repair\order\RepairExtraInfoResp;
use facade\response\repair\order\RepairOrderBaseResp;
use facade\response\repair\order\RepairOrderDetailResp;
use facade\response\repair\RepairOrderInfoResp;
use facade\response\Result;
use facade\response\user\StarInfoResp;
use lib\NumHelper;
use lib\PriceHelper;
use model\category\dto\Produc;
use model\dto\UserStarDTO;
use model\organization\dto\OrganizationCompanyDTO;
use model\organization\dto\OrganizationStoreDTO;
use model\repair\dto\RepairOrderExtraInfoDTO;
use model\repair\dto\RepairOrderInfoDTO;
use model\user\dto\UserEngineerDTO;
use service\category\impl\CategoryServiceImpl;
use service\maintain\impl\MaintainLeaderboardServiceImpl;
use service\maintain\impl\MaintainTaskServiceImpl;
use service\organization\impl\OrganizationServiceImpl;
use service\repair\impl\RepairOrderServiceImpl;
use service\user\impl\FollowServiceImpl;
use service\user\impl\StarServiceImpl;
USE model\dto\MaintainTaskRecordDTO;
use service\user\impl\UserEngineerServiceImpl;

class RepairOrderConversionUtil
{


    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function buildRepairOrderInfoResp(?RepairOrderInfoDTO &$repairOrderInfoDTO, int $login_type): ?RepairOrderInfoResp
    {
        if ($repairOrderInfoDTO == null) {
            return null;
        }
        $repairOrderResp = new RepairOrderInfoResp();

        $repairOrderResp->base_info = $this->buildBaseInfo($repairOrderInfoDTO, $login_type);
        $repairOrderResp->engineer_info = $this->buildEngineerInfo($repairOrderInfoDTO);
        $repairOrderResp->order_detail = $this->buildDetail($repairOrderInfoDTO);
        $repairOrderResp->customer_info = $this->buildCustomerInfo($repairOrderInfoDTO);
        $repairOrderResp->extra_info = $this->buildExtraInfo($repairOrderInfoDTO);
        \FUR_Core::copyProperties($repairOrderInfoDTO, $repairOrderResp);
        return $repairOrderResp;
    }


    private function buildBaseInfo(RepairOrderInfoDTO &$repairOrderInfoDTO, $login_type): RepairOrderBaseResp
    {
        $repairOrderBaseInfo = new RepairOrderBaseResp();
        \FUR_Core::copyProperties($repairOrderInfoDTO, $repairOrderBaseInfo);

        $repairOrderBaseInfo->engineer_reward = PriceHelper::conversionDisplayPrice($repairOrderInfoDTO->engineer_reward);
        $repairOrderBaseInfo->customer_price = PriceHelper::conversionDisplayPrice($repairOrderInfoDTO->customer_price);
        $repairCategoryInfoRet = CategoryServiceImpl::getInstance()->queryCategoryById($repairOrderInfoDTO->order_category_id);
        /** @var Produc $repairCategoryInfo */
        $repairCategoryInfo = $repairCategoryInfoRet->data;


        $repairOrderBaseInfo->order_category_name = $repairCategoryInfo->category_name;
        return $repairOrderBaseInfo;
    }


    private function buildDetail(RepairOrderInfoDTO &$repairOrderInfoDTO): RepairOrderDetailResp
    {
        $repairOrderDetail = new RepairOrderDetailResp();
        $repairOrderDetail->images = json_decode($repairOrderInfoDTO->images);
        $repairOrderDetail->appointment_timestamp = $repairOrderInfoDTO->appointment_timestamp;
        $repairOrderDetail->customer_remark = $repairOrderInfoDTO->customer_remark;
        $repairOrderDetail->customer_phone_num = $repairOrderInfoDTO->customer_phone_num;
        $repairOrderDetail->customer_address = $repairOrderInfoDTO->customer_address;
        $repairOrderDetail->customer_name = $repairOrderInfoDTO->customer_name;
        return $repairOrderDetail;
    }

    private function buildEngineerInfo(RepairOrderInfoDTO &$repairOrderInfoDTO): RepairEngineerResp
    {

        $repairEngineerInfo = new RepairEngineerResp();
        if ($repairOrderInfoDTO->engineer_id == null) {
            return $repairEngineerInfo;
        }
        $repairEngineerInfo->engineer_id = $repairOrderInfoDTO->engineer_id;
        $engineerInfoRet = UserEngineerServiceImpl::getInstance()->getEngineerInfo($repairOrderInfoDTO->engineer_id);
        if (!Result::isSuccess($engineerInfoRet)) {
            return $repairEngineerInfo;
        }
        /** @var UserEngineerDTO $engineerInfo */
        $engineerInfo = $engineerInfoRet->data;
        $repairEngineerInfo->engineer_name = $engineerInfo->real_name;
        $repairEngineerInfo->contact_info = $engineerInfo->phone_num;
        return $repairEngineerInfo;
    }

    private function buildCustomerInfo(RepairOrderInfoDTO &$repairOrderInfoDTO): RepairCustomerInfoResp
    {
        $repairCustomerInfo = new RepairCustomerInfoResp();
        \FUR_Core::copyProperties($repairOrderInfoDTO, $repairCustomerInfo);
        $storeInfoRet = OrganizationServiceImpl::getInstance()->getStoreInfo($repairOrderInfoDTO->store_id);
        /** @var OrganizationStoreDTO $storeInfo */
        $storeInfo = $storeInfoRet->data;
        $repairCustomerInfo->store_name = $storeInfo->store_name;
        $repairCustomerInfo->store_address = $repairOrderInfoDTO->customer_address;
        $repairCustomerInfo->store_num = $storeInfo->store_num;
        $repairCustomerInfo->store_province = $storeInfo->store_province;
        $repairCustomerInfo->store_city = $storeInfo->store_city;
        $repairCustomerInfo->store_area = $storeInfo->store_area;
        $companyInfoRet = OrganizationServiceImpl::getInstance()->getCompanyInfo($repairOrderInfoDTO->company_id);
        /** @var OrganizationCompanyDTO $companyInfo */
        $companyInfo = $companyInfoRet->data;
        $repairCustomerInfo->company_name = $companyInfo->company_name;

        return $repairCustomerInfo;
    }

    private function buildExtraInfo(RepairOrderInfoDTO &$repairOrderInfoDTO): RepairExtraInfoResp
    {
        $extraInfoResp = new RepairExtraInfoResp();
        $orderExtraInfoRet = RepairOrderServiceImpl::getInstance()->getRepairOrderExtraInfo($repairOrderInfoDTO->id);
        if (!Result::isSuccess($orderExtraInfoRet)) {
            return $extraInfoResp;
        }
        /** @var RepairOrderExtraInfoDTO $orderExtraInfo */
        $orderExtraInfo = $orderExtraInfoRet->data;
        $extraInfoResp->repair_instructions = $orderExtraInfo->repair_instructions;
        $extraInfoResp->repair_bills = json_decode($orderExtraInfo->repair_bills);
        $extraInfoResp->repair_photos = json_decode($orderExtraInfo->repair_photos);
        return $extraInfoResp;
    }
}