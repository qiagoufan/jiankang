<?php

namespace business\repair;

use facade\request\maintain\GetMaintainTaskRankParams;
use facade\request\user\FollowParams;
use facade\response\repair\order\RepairCustomerInfoResp;
use facade\response\repair\order\RepairEngineerResp;
use facade\response\repair\order\RepairExtraInfoResp;
use facade\response\repair\order\RepairOrderBaseResp;
use facade\response\repair\order\RepairOrderDetailResp;
use facade\response\repair\RepairOrderInfoResp;
use facade\response\Result;
use facade\response\user\StarInfoResp;
use lib\NumHelper;
use lib\PriceHelper;
use model\category\dto\Produc;
use model\dto\UserStarDTO;
use model\organization\dto\OrganizationCompanyDTO;
use model\organization\dto\OrganizationStoreDTO;
use model\repair\dto\RepairOrderExtraInfoDTO;
use model\repair\dto\RepairOrderInfoDTO;
use model\user\dto\UserEngineerDTO;
use Mpdf\Mpdf;
use service\category\impl\CategoryServiceImpl;
use service\maintain\impl\MaintainLeaderboardServiceImpl;
use service\maintain\impl\MaintainTaskServiceImpl;
use service\organization\impl\OrganizationServiceImpl;
use service\repair\impl\RepairOrderServiceImpl;
use service\user\impl\FollowServiceImpl;
use service\user\impl\StarServiceImpl;
USE model\dto\MaintainTaskRecordDTO;
use service\user\impl\UserEngineerServiceImpl;

class RepairOrderPdfConversionUtil
{


    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function outputPdf($repairOrderId)
    {
        $mpdf = new Mpdf();
        try {
            $mpdf->allow_charset_conversion = true;
            $mpdf->autoScriptToLang = true;
            $mpdf->autoLangToFont = true;
            $mpdf->charset_in = 'utf-8';
            $mpdf->SetHTMLHeader($this->getHtmlHeader());
            $mpdf->WriteHTML($this->buildPdfHtml($repairOrderId));
//            $mpdf->Output();
            $mpdf->Output('维修报告.pdf', 'D');
        } catch (\Mpdf\MpdfException $e) {
        }
    }

    private function buildPdfHtml($repairOrderId): ?string
    {
        $host = $_SERVER['REQUEST_SCHEME'] . "://" . $_SERVER['HTTP_HOST'];
        $url = $host . '/repair/order/displayRepairOrder?repair_order_id=' . $repairOrderId;
        return file_get_contents($url);

    }

    private function getHtmlHeader()
    {
        $headerPath = APP_PATH . 'static' . DIRECTORY_SEPARATOR . "lubmaster.png";
        return "<img src='$headerPath' width='50px' height='50px'><br /><hr/>";
    }

}