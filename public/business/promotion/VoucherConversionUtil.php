<?php

namespace business\promotion;

use facade\response\promotion\VoucherManageDetailResp;
use facade\response\promotion\VoucherUserDetailResp;
use facade\response\Result;
use lib\PriceHelper;
use model\promotion\dto\PromotionVoucherDTO;
use model\promotion\dto\PromotionVoucherEntityDTO;
use service\promotion\data\VoucherDetail;
use service\promotion\impl\PromotionVoucherServiceImpl;

class VoucherConversionUtil
{

    public static function conversionManageVoucher(PromotionVoucherDTO &$voucherInfo): ?VoucherManageDetailResp
    {
        $voucherResp = new VoucherManageDetailResp();
        \FUR_Core::copyProperties($voucherInfo, $voucherResp);
        $voucherDetailJsonDecode = json_decode($voucherInfo->voucher_detail);

        $voucherResp->day_long = $voucherInfo->expire_timelong / 86400;
        $voucherDetail = new VoucherDetail();
        \FUR_Core::copyProperties($voucherDetailJsonDecode, $voucherDetail);
        if ($voucherInfo->voucher_type == \Config_Const::VOUCHER_TYPE_DISCOUNT) {
            $voucherResp->display_voucher_discount = PriceHelper::conversionDisplayPrice($voucherDetail->amount, false);
        }
        if (isset($voucherDetail->price_limit)) {
            $voucherResp->display_voucher_discount_qualification = self::buildDiscountQualification($voucherDetail->price_limit);
        }
        $voucherResp->voucher_detail = $voucherDetail;
        return $voucherResp;
    }

    public static function conversionVoucherInfo(PromotionVoucherDTO &$voucherInfo): ?VoucherUserDetailResp
    {
        $voucherResp = new VoucherUserDetailResp();
        if ($voucherInfo->voucher_status == \Config_Const::VOUCHER_STATUS_DELETE) {
            return null;
        }
        $voucherResp->voucher_name = $voucherInfo->voucher_name;

        /** @var VoucherDetail $voucherDetail */
        $voucherDetailJsonDecode = json_decode($voucherInfo->voucher_detail);
        $voucherDetail = new VoucherDetail();
        \FUR_Core::copyProperties($voucherDetailJsonDecode, $voucherDetail);
        if ($voucherInfo->voucher_type == \Config_Const::VOUCHER_TYPE_DISCOUNT) {
            $voucherResp->display_voucher_discount = PriceHelper::conversionDisplayPrice($voucherDetail->amount, false);
        }

        if (isset($voucherDetail->price_limit)) {
            $voucherResp->display_voucher_discount_qualification = self::buildDiscountQualification($voucherDetail->price_limit);
        }
        $voucherResp->display_time_range = date('Y.m.d', $voucherInfo->start_timestamp) . "-" . date('Y.m.d', $voucherInfo->end_timestamp);

        if ($voucherDetail->category_limit == null && $voucherDetail->sku_limit == null && $voucherDetail->tag_limit == null) {
            $voucherResp->voucher_qualification = '全场商品可用';
        } else {
            $voucherResp->voucher_qualification = '指定商品可用';
        }

        $voucherResp->voucher_detail = $voucherDetail;
        $voucherResp->voucher_id = $voucherInfo->id;
        $voucherResp->available_status = 1;
        return $voucherResp;
    }

    public static function conversionUserVoucher(PromotionVoucherEntityDTO &$voucherEntityDTO): ?VoucherUserDetailResp
    {
        $voucherResp = new VoucherUserDetailResp();
        $voucherInfoRet = PromotionVoucherServiceImpl::getInstance()->getVoucherInfo($voucherEntityDTO->voucher_id);
        if (Result::isSuccess($voucherInfoRet) == false) {
            return null;
        }

        /** @var PromotionVoucherDTO $voucherInfo */
        $voucherInfo = $voucherInfoRet->data;
        if ($voucherInfo->voucher_status == \Config_Const::VOUCHER_STATUS_DELETE) {
            return null;
        }
        $voucherResp->voucher_name = $voucherInfo->voucher_name;

        $voucherResp->voucher_sn = $voucherEntityDTO->voucher_sn;
        /** @var VoucherDetail $voucherDetail */
        $voucherDetailJsonDecode = json_decode($voucherInfoRet->data->voucher_detail);
        $voucherDetail = new VoucherDetail();
        \FUR_Core::copyProperties($voucherDetailJsonDecode, $voucherDetail);
        if ($voucherInfo->voucher_type == \Config_Const::VOUCHER_TYPE_DISCOUNT) {
            $voucherResp->display_voucher_discount = PriceHelper::conversionDisplayPrice($voucherDetail->amount, false);
        }

        if (isset($voucherDetail->price_limit)) {
            $voucherResp->display_voucher_discount_qualification = self::buildDiscountQualification($voucherDetail->price_limit);
        }
        $voucherResp->display_time_range = self::buildTimeRange($voucherEntityDTO);

        if ($voucherDetail->category_limit == null && $voucherDetail->sku_limit == null && $voucherDetail->tag_limit == null) {
            $voucherResp->voucher_qualification = '全场商品可用';
        } else {
            $voucherResp->voucher_qualification = '指定商品可用';
        }

        $now = time();
        $voucherResp->use_status = $voucherEntityDTO->use_status;
        if ($voucherEntityDTO->use_status == 1) {
            $voucherResp->enable_status = \Config_Const::VOUCHER_ENABLE_STATUS_USED;
            $voucherResp->unavailable_reason = '已使用';
        } elseif ($voucherEntityDTO->expire_timestamp < $now) {
            $voucherResp->enable_status = \Config_Const::VOUCHER_ENABLE_STATUS_EXPIRED;
            $voucherResp->unavailable_reason = '已过期';
        } elseif ($voucherEntityDTO->start_timestamp > $now) {
            $voucherResp->enable_status = \Config_Const::VOUCHER_ENABLE_STATUS_EXPIRED;
            $voucherResp->unavailable_reason = '未开始';
        } elseif ($voucherInfo->end_timestamp != 0 && $voucherInfo->end_timestamp < $now) {
            $voucherResp->enable_status = \Config_Const::VOUCHER_ENABLE_STATUS_EXPIRED;
            $voucherResp->unavailable_reason = '已过期';
        } elseif ($voucherInfo->voucher_status != \Config_Const::VOUCHER_STATUS_USEFUL) {
            $voucherResp->enable_status = \Config_Const::VOUCHER_ENABLE_STATUS_EXPIRED;
            $voucherResp->unavailable_reason = '已失效';
        } else {
            $voucherResp->enable_status = \Config_Const::VOUCHER_ENABLE_STATUS_VALID;
            $voucherResp->unavailable_reason = '';
        }
        $voucherResp->voucher_detail = $voucherDetail;

        return $voucherResp;
    }


    public
    static function buildValidVoucherDetail(PromotionVoucherEntityDTO &$voucherEntityDTO): ?VoucherDetail
    {
        $voucherInfoRet = PromotionVoucherServiceImpl::getInstance()->getVoucherInfo($voucherEntityDTO->voucher_id);
        if (Result::isSuccess($voucherInfoRet) == false) {
            return null;
        }
        /** @var PromotionVoucherDTO $voucherInfo */
        $voucherInfo = $voucherInfoRet->data;
        $now = time();
        if ($voucherEntityDTO->use_status == 1) {
            return null;
        } elseif ($voucherEntityDTO->expire_timestamp < $now) {
            return null;
        } elseif ($voucherEntityDTO->start_timestamp > $now) {
            return null;
        } elseif ($voucherInfo->end_timestamp != 0 && $voucherInfo->end_timestamp < $now) {
            return null;
        } elseif ($voucherInfo->voucher_status != \Config_Const::VOUCHER_STATUS_USEFUL) {
            return null;
        }

        /** @var VoucherDetail $voucherDetailJsonDecode */
        $voucherDetailJsonDecode = json_decode($voucherInfoRet->data->voucher_detail);
        $voucherDetail = new VoucherDetail();
        \FUR_Core::copyProperties($voucherDetailJsonDecode, $voucherDetail);
        return $voucherDetail;
    }


    private
    static function buildDiscountQualification(?int $amount): ?string
    {
        if ($amount == 0) {
            return '无门槛';
        }
        return PriceHelper::conversionDisplayPrice($amount, false);
    }

    private static function buildTimeRange(PromotionVoucherEntityDTO &$voucherEntityDTO): ?string
    {
        $timeRange = '';
        if ($voucherEntityDTO->start_timestamp && $voucherEntityDTO->expire_timestamp) {
            $timeRange = date('Y.m.d', $voucherEntityDTO->start_timestamp) . "-" . date('Y.m.d', $voucherEntityDTO->expire_timestamp);
        }
        return $timeRange;
    }


}