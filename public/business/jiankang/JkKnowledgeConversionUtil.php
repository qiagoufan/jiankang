<?php

namespace business\jiankang;

use facade\response\jiankang\knowledge\JkKnowledgeResp;
use lib\TimeHelper;
use model\jk\dto\JkKnowledgeDTO;

class JkKnowledgeConversionUtil
{


    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public static function conversionKnowledge(?JkKnowledgeDTO $jkKnowledgeDTO): ?JkKnowledgeResp
    {
        if ($jkKnowledgeDTO == null) {
            return null;
        }
        $jkKnowledgeResp = new JkKnowledgeResp();
        \FUR_Core::copyProperties($jkKnowledgeDTO, $jkKnowledgeResp);
        $jkKnowledgeResp->display_created_time = date('Y-m-d H:i:s', $jkKnowledgeDTO->created_timestamp);
        return $jkKnowledgeResp;

    }
}