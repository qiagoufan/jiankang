<?php

namespace business\product;

use facade\request\product\GetProductDetailParams;
use facade\request\tag\QueryTagEntityListParams;
use facade\request\tag\TagEntityParams;
use facade\request\user\UserCollectParams;
use facade\response\product\ProductItemResp;
use facade\response\product\ProductSkuPriceResp;
use facade\response\product\ProductSuperDetailResp;
use facade\response\Result;
use lib\business\OffZero;
use lib\PriceHelper;
use model\dto\ProductItemDTO;
use model\dto\ProductSkuDTO;
use model\dto\ProductSkuPriceDTO;
use model\dto\tag\TagEntityDTO;
use model\dto\UserStarDTO;
use service\product\impl\ProductServiceImpl;
use service\tag\impl\TagServiceImpl;
use service\user\impl\CollectServiceImpl;
use service\user\impl\StarServiceImpl;

class ProductDetailConversionUtil
{


    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public static function buildProductItemResp(?ProductItemDTO &$productItemDTO): ?ProductItemResp
    {
        if ($productItemDTO == null) {
            return null;
        }

        $productItemResp = new ProductItemResp();
        $productItemResp->product_name = $productItemDTO->product_name;
        $productItemResp->sku_id = $productItemDTO->default_sku_id;
        $productItemResp->stock = $productItemDTO->stock;
        $productItemResp->sale = $productItemDTO->sale;
        $productItemResp->main_image = $productItemDTO->main_image;
        $productItemResp->published_timestamp = $productItemDTO->published_timestamp;
        $productItemResp->original_price = PriceHelper::conversionDisplayPrice($productItemDTO->original_price);
        $productItemResp->sale_price = PriceHelper::conversionDisplayPrice($productItemDTO->current_price);
        return $productItemResp;
    }

    /**
     * 转换sku规格为字符串
     * 实例: 亮黑色; 8G+128G; 官方标配
     * @param ProductSkuDTO $productSkuDTO
     * @return string
     */
    public static function conversionSpecification(ProductSkuDTO &$productSkuDTO): string
    {
        $specificationDetail = '';
        $specificationStr = $productSkuDTO->specifications;
        $specificationJsonArray = json_decode($specificationStr);
        if ($specificationJsonArray == null) {
            return $specificationDetail;
        }

        $specificationAttrArray = [];
        foreach ($specificationJsonArray as $specificationJson) {
            array_push($specificationAttrArray, $specificationJson->attr);
        }
        return implode("; ", $specificationAttrArray);
    }


    public static function conversionProductPrice(ProductSkuPriceDTO &$productSkuPriceDTO): ProductSkuPriceResp
    {
        $productSkuPriceResp = new ProductSkuPriceResp();
        \FUR_Core::copyProperties($productSkuPriceDTO, $productSkuPriceResp);
        $productSkuPriceResp->current_price = PriceHelper::conversionDisplayPrice($productSkuPriceDTO->current_price);
        $productSkuPriceResp->current_price_formatted = PriceHelper::conversionDisplayPrice($productSkuPriceDTO->current_price, true);
        $productSkuPriceResp->former_price = PriceHelper::conversionDisplayPrice($productSkuPriceDTO->former_price);
        $productSkuPriceResp->former_price_formatted = PriceHelper::conversionDisplayPrice($productSkuPriceDTO->former_price, true);
        $productSkuPriceResp->price_tag = ProductSkuPriceDTO::PRICE_TAG;
        $productSkuPriceResp->tag_list = ProductSkuPriceDTO::TAG_LIST;
        $productSkuPriceResp->shipping_price = PriceHelper::conversionDisplayPrice($productSkuPriceDTO->shipping_price);
        $productSkuPriceResp->shipping_price_formatted = PriceHelper::conversionDisplayPrice($productSkuPriceDTO->shipping_price, true);
        $productSkuPriceResp->total_price = $productSkuPriceResp->current_price + $productSkuPriceResp->shipping_price;
        $productSkuPriceResp->total_price_formatted = PriceHelper::conversionDisplayPrice($productSkuPriceDTO->current_price + $productSkuPriceDTO->shipping_price, true);
        return $productSkuPriceResp;
    }

    public function buildProductSuperDetail(int $skuId, int $userId): ?ProductSuperDetailResp
    {
        $productSuperDetailResp = new ProductSuperDetailResp();
        /**
         * 查询商品详情
         */
        $productSkuDetailParams = new GetProductDetailParams();
        $productSkuDetailParams->sku_id = $skuId;
        $productSkuDetailRet = ProductServiceImpl::getInstance()->getProductDetail($productSkuDetailParams);
        if (!Result::isSuccess($productSkuDetailRet)) {
            return null;
        }
        /** @var  ProductSkuDTO $productSkuDTO */
        $productSkuDTO = $productSkuDetailRet->data;
        $productSuperDetailResp->sku_id = $skuId;
        $productSuperDetailResp->sku_name = $productSkuDTO->product_name;
        $productSuperDetailResp->title = $productSkuDTO->product_name;
        $productSuperDetailResp->sku_main_pic = $productSkuDTO->main_pic;
        $productSuperDetailResp->is_super = $productSkuDTO->is_super;
        $productSuperDetailResp->sku_background = $productSkuDTO->background_pic;
        $productSuperDetailResp->purchase_info = self::buildPurchaseInfo($productSkuDTO);

        $productSkuPriceRet = ProductServiceImpl::getInstance()->getProductSkuPrice($productSkuDTO);
        if (!Result::isSuccess($productSkuPriceRet)) {
            return null;
        }
        /** @var ProductSkuPriceDTO $productSkuPriceDTO */
        $productSkuPriceDTO = $productSkuPriceRet->data;
        $productSkuPriceResp = self::conversionProductPrice($productSkuPriceDTO);
        $productSuperDetailResp->sku_price = $productSkuPriceResp->current_price_formatted;
        /**
         *收藏信息
         */
        $userCollectParams = new  UserCollectParams();
        $userCollectParams->collect_biz_type = 'sku';
        $userCollectParams->collect_biz_id = $skuId;
        $userCollectParams->user_id = $userId;
        $collectDetailRet = CollectServiceImpl::getInstance()->getCollectDetail($userCollectParams);
        if (Result::isSuccess($collectDetailRet)) {
            $productSuperDetailResp->collect_status = $collectDetailRet->data->collect_status;
            $productSuperDetailResp->collect_count = $collectDetailRet->data->collect_count;
        }

        /**
         * 评论信息
         */
        $tagEntityParams = new TagEntityParams ();
        $tagEntityParams->tag_biz_type = TagEntityDTO::BIZ_TYPE_PRODUCT_SKU;
        $tagEntityParams->tag_biz_id = $skuId;
        $tagEntityParams->entity_biz_type = TagEntityDTO::BIZ_TYPE_FEED;
        $tagEntityCountRet = TagServiceImpl::getInstance()->getTagEntityCount($tagEntityParams);
        if (Result::isSuccess($tagEntityCountRet)) {
            $productSuperDetailResp->comment_count = $tagEntityCountRet->data;
        }


        /**
         * 补充明星信息
         */
        $this->patchSuperProductBindStar($productSuperDetailResp);
        return $productSuperDetailResp;

    }

    /**
     * 超级商品绑定明星
     * @param ProductSuperDetailResp $productSuperDetailResp
     */
    private function patchSuperProductBindStar(ProductSuperDetailResp &$productSuperDetailResp)
    {
        $skuId = $productSuperDetailResp->sku_id;
        /**
         * 获取相关明星标签
         */
        $tagService = TagServiceImpl::getInstance();
        $queryTagEntityListParams = new QueryTagEntityListParams ();
        $queryTagEntityListParams->tag_biz_type = TagEntityDTO::BIZ_TYPE_PRODUCT_SKU;
        $queryTagEntityListParams->tag_biz_id = $skuId;
        $queryTagEntityListParams->entity_biz_type = TagEntityDTO::BIZ_TYPE_STAR;
        $queryTagEntityListParams->index = 0;
        $queryTagEntityListParams->page_size = 1;

        $entityListRet = $tagService->queryTagEntityList($queryTagEntityListParams);
        if (!Result::isSuccess($entityListRet)) {
            return;
        }
        $entityList = $entityListRet->data;
        if (empty($entityList)) {
            return;
        }
        $starId = $entityList[0];
        $starRet = StarServiceImpl::getInstance()->getStarInfo($starId);
        if (!Result::isSuccess($starRet)) {
            return;
        }
        /** @var UserStarDTO $starInfo */
        $starInfo = $starRet->data;
        $productSuperDetailResp->star_name = $starInfo->star_name;
        $productSuperDetailResp->star_avatar = $starInfo->official_avatar;
        $productSuperDetailResp->star_id = $starInfo->id;
    }


    public static function buildPurchaseInfo(?ProductSkuDTO &$productSkuDTO): ?string
    {
        $purchaseInfo = "";
        $productSkuService = ProductServiceImpl::getInstance();
        /** @var ProductItemDTO $productItemDTO */

        if ($productSkuDTO == null) {
            return $purchaseInfo;
        }


        $productItemResult = $productSkuService->getProductItem($productSkuDTO->item_id);
        $productItemDTO = $productItemResult->data;

        if (!Result::isSuccess($productItemResult)) {
            return $purchaseInfo;
        }

        $purchaseInfo = OffZero::fakePurchaseInfo($productItemDTO, $productSkuDTO->id);
        if ($purchaseInfo != null) {
            return $purchaseInfo;
        }


        if ($productItemDTO->sale == 0 && $productItemDTO->stock == 0) {
            $productItemDTO->sale = ($productItemDTO->created_timestamp % 100) + 41;
        }


        if ($productItemDTO->sale != 0) {
            $purchaseInfo = $productItemDTO->sale . '人已付款';
        }
        return $purchaseInfo;
    }
}