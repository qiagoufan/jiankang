<?php

namespace business\product;

use facade\response\product\jk\detail\JkProductBaseResp;
use facade\response\product\jk\detail\JkProductDetailResp;
use facade\response\product\jk\detail\JkProductSkuResp;
use facade\response\product\jk\JkProductResp;
use facade\response\Result;
use lib\PriceHelper;
use model\product\dto\ProductItemDTO;
use model\product\dto\ProductSkuDTO;
use service\product\impl\ProductServiceImpl;

class JkProductConversionUtil
{

    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function buildProductResp(?ProductItemDTO &$productItemDTO): ?JkProductResp
    {
        if ($productItemDTO == null) {
            return null;
        }
        $productResp = new JkProductResp();
        $productResp->base_info = $this->buildBaseInfo($productItemDTO);
        $productResp->product_detail = $this->buildDetailInfo($productItemDTO);
        $productResp->spec_list = $this->buildSpecList($productItemDTO->id);
        return $productResp;
    }


    private function buildBaseInfo(ProductItemDTO &$productItemDTO): ?JkProductBaseResp
    {
        $productBaseInfo = new JkProductBaseResp();
        $productBaseInfo->item_id = $productItemDTO->id;
        $productBaseInfo->default_sku_id = $productItemDTO->default_sku_id;
        $productBaseInfo->sale = $productItemDTO->sale;
        $productBaseInfo->main_image = $productItemDTO->main_image;
        $productBaseInfo->category_id = $productItemDTO->cate_level_2;
        $productBaseInfo->stock = $productItemDTO->stock;
        $productBaseInfo->current_price = $productItemDTO->current_price;
        $productBaseInfo->regular_price = $productItemDTO->regular_price;
        $productBaseInfo->shipping_price = $productItemDTO->shipping_price;
        $productBaseInfo->display_current_price = PriceHelper::conversionDisplayPrice($productItemDTO->current_price);
        $productBaseInfo->display_regular_price = PriceHelper::conversionDisplayPrice($productItemDTO->regular_price);
        $productBaseInfo->display_shipping_price = PriceHelper::conversionDisplayPrice($productItemDTO->shipping_price);
        $productBaseInfo->summary = $productItemDTO->summary;
        $productBaseInfo->seq = $productItemDTO->seq;
        $productBaseInfo->created_timestamp = $productItemDTO->created_timestamp;
        $productBaseInfo->product_name = $productItemDTO->product_name;
        $productBaseInfo->publish_status = $productItemDTO->publish_status;
        return $productBaseInfo;
    }

    private function buildDetailInfo(ProductItemDTO &$productItemDTO): ?JkProductDetailResp
    {
        $productDetail = new JkProductDetailResp();
//        $productDetail->detail_list = json_decode($productItemDTO->product_desc);
        $productDetail->detail = $productItemDTO->product_desc;
        $productDetail->galley_image_list = json_decode($productItemDTO->gallery_image);
        return $productDetail;
    }

    private function buildSpecList(int $itemId): ?array
    {
        $specList = [];

        $skuListRet = ProductServiceImpl::getInstance()->queryProductSkuList($itemId);
        if (Result::isSuccess($skuListRet) == false) {
            return $specList;
        }

        $skuList = $skuListRet->data;

        /** @var ProductSkuDTO $skuInfo */
        foreach ($skuList as $skuInfo) {
            $jkProductSkuResp = new JkProductSkuResp();
            $jkProductSkuResp->sku_id = $skuInfo->id;
            $jkProductSkuResp->stock = $skuInfo->stock;
            $jkProductSkuResp->sale = $skuInfo->sale;
            $jkProductSkuResp->sku_name = $skuInfo->sku_name;
            $jkProductSkuResp->current_price = $skuInfo->current_price;
            $jkProductSkuResp->regular_price = $skuInfo->regular_price;
            $jkProductSkuResp->shipping_price = $skuInfo->shipping_price;
            $jkProductSkuResp->display_regular_price = PriceHelper::conversionDisplayPrice($skuInfo->regular_price);
            $jkProductSkuResp->display_current_price = PriceHelper::conversionDisplayPrice($skuInfo->current_price);
            $jkProductSkuResp->display_shipping_price = PriceHelper::conversionDisplayPrice($skuInfo->shipping_price);
            array_push($specList, $jkProductSkuResp);
        }
        return $specList;
    }


}