<?php

namespace business\product;


use facade\response\product\ProductReviewResp;
use facade\response\Result;
use lib\ClientRouteHelper;
use lib\TimeHelper;
use model\dto\ProductReviewDTO;
use model\dto\UserDTO;
use service\user\impl\UserServiceImpl;

class ProductReviewConversionUtil
{

    public static function fillReviewLink(ProductReviewDTO &$productReviewDTO): string
    {
        return ClientRouteHelper::buildReviewLink($productReviewDTO->item_id, $productReviewDTO->id);
    }

    public static function conversionReview(?ProductReviewDTO &$productReviewDTO): ?ProductReviewResp
    {
        $productReviewResp = new  ProductReviewResp();
        if ($productReviewDTO == null) {
            return null;
        }
        $productReviewResp->review_id = (int)$productReviewDTO->id;
        $productReviewResp->item_id = (int)$productReviewDTO->item_id;
        $productReviewResp->sku_id = (int)$productReviewDTO->sku_id;
        $productReviewResp->image_list = $productReviewDTO->image_list;
        $productReviewResp->publish_timestamp = $productReviewDTO->created_timestamp;
        $productReviewResp->publish_time_str = TimeHelper::getPersonalTimeString($productReviewResp->publish_timestamp);
        $productReviewResp->user_id = $productReviewDTO->customer_id;
        $productReviewResp->content = $productReviewDTO->content;
        $productReviewResp->specification_detail = $productReviewDTO->specification_detail;
        $productReviewResp->link = ProductReviewConversionUtil::fillReviewLink($productReviewDTO);
        $userInfoRet = UserServiceImpl::getInstance()->getValidUserInfo($productReviewResp->user_id);
        if (Result::isSuccess($userInfoRet)) {
            /**@var  UserDTO $userDTO * */
            $userDTO = $userInfoRet->data;
            $productReviewResp->nick_name = $userDTO->nick_name;
            $productReviewResp->user_avatar = $userDTO->avatar;
        }
        return $productReviewResp;
    }

}