<?php

namespace business\feed;

use facade\response\feed\content\AnalysisResultResp;
use model\feed\dto\FeedOutsiteLinkDTO;

class PickContentUtil
{

    const weibo_rule = '/render_data[\s\S]*__wb_performance_data/';
    const normal_rule = '/<p.*<\/p>/';
    const image_rule = '/<img.*?>/';

    const URL_RULE = '/[A-Za-z0-9]{4,5}:\/\/[^\s^"]*.[\w]/';
    const label_rule = '/<.*?>/';
    const br_label_rule = '/[<br\/>]{2,100}/';
    const a_label_rule = '/<a .*?<\/a>/';
    const span_label_rule = '/(<span .*?>)|(<\/span>)/';

    const PRODUCT_SPIDER_URL = 'http://59.110.235.173:8888/spider';

    public static function pickWeiboContent(string $link, AnalysisResultResp &$analysisResultResp, ?FeedOutsiteLinkDTO &$feedOutsiteLinkDTO)
    {

        $linkInfo = explode("/", $link);
        $weiboFeedId = $linkInfo[count($linkInfo) - 1];

        $rebuildLink = 'https://m.weibo.cn/status/' . $weiboFeedId;
        $content = \FUR_Curl::get($rebuildLink);

        preg_match(self::weibo_rule, $content, $result);

        if (empty($result)) {
            return;
        }
        self::buildFeedOutsiteLinkDTO($link, $content, $feedOutsiteLinkDTO);
        $content = $result[0];
        $content = str_replace("render_data = ", '', $content);
        $content = str_replace('var __wb_performance_data', '', $content);
        $content = str_replace("[0] || {};", '', $content);
        $jsonDecodeData = json_decode($content, true);
        $statusContent = $jsonDecodeData[0]['status'];
        $text = $statusContent['text'];
        $analysisResultResp->images = [];
        if (isset($statusContent['pics']) && is_array($statusContent['pics'])) {
            foreach ($statusContent['pics'] as $pic) {
                if (count($analysisResultResp->images) >= 9) {
                    break;
                }
                $image = $pic['url'];
                if (isset($pic['large']['url'])) {
                    $image = $pic['url'];
                }

                array_push($analysisResultResp->images, $image);

            }

        }

        $text = preg_replace(self::a_label_rule, '', $text);
        $text = preg_replace(self::span_label_rule, '', $text);
        $text = preg_replace("/<(img)(?:[^>]*(\[.*?\]))?[^>]*?(\/?)>/i", '$2', $text);
        $text = preg_replace(self::label_rule, '<br/>', $text);
        $text = preg_replace(self::br_label_rule, "", $text);
        $analysisResultResp->content = $text;


    }

    public static function pickNormalContent(string $link, AnalysisResultResp &$analysisResultResp, ?FeedOutsiteLinkDTO &$feedOutsiteLinkDTO)
    {
        $content = \FUR_Curl::get($link);
        self::buildFeedOutsiteLinkDTO($link, $content, $feedOutsiteLinkDTO);


        preg_match(self::normal_rule, $content, $result);
        if ($result == null) {
            return;
        }

        $content = $result[0];

        $analysisResultResp->images = [];
        preg_match_all(self::image_rule, $content, $imagesResult);

        if ($imagesResult) {
            foreach ($imagesResult[0] as $imageLabel) {
                preg_match(self::URL_RULE, $imageLabel, $imgLinkResult);
                if ($imgLinkResult) {
                    if (count($analysisResultResp->images) >= 9) {
                        break;
                    }
                    $image = $imgLinkResult[0];
                    $image = str_replace("http://", "https://", $image);
                    array_push($analysisResultResp->images, $image);
                }
            }
        }

        $text = preg_replace(self::a_label_rule, '', $content);
        $text = preg_replace(self::span_label_rule, '', $text);
        $text = preg_replace(self::image_rule, '', $text);
        $text = preg_replace(self::span_label_rule, '', $text);
        $text = preg_replace(self::label_rule, '<br/>', $text);
        $text = preg_replace(self::br_label_rule, "\n", $text);
        $text = trim($text, "\n");

        $analysisResultResp->content = $text;
    }


    public static function pickProductContent(string $link, AnalysisResultResp &$analysisResultResp, ?FeedOutsiteLinkDTO &$feedOutsiteLinkDTO)
    {

        $url = self::PRODUCT_SPIDER_URL . "?url=" . urlencode(urlencode($link));

        $content = \FUR_Curl::get($url, [], 10000);

        if ($content == false) {
            return;
        }

        $jsonDecodeContent = json_decode($content);
        if ($jsonDecodeContent->header->code != 0) {
            return;
        }
        $analysisResultResp->images = [$jsonDecodeContent->body->image];
        $analysisResultResp->content = "#" . trim($jsonDecodeContent->body->title) . "# 售价 ¥:" . $jsonDecodeContent->body->price;

        self::buildProductOutsiteLinkDTO($link, $jsonDecodeContent->body->title, $content, $feedOutsiteLinkDTO);
    }


    private static function buildFeedOutsiteLinkDTO(string $link, string $content, FeedOutsiteLinkDTO &$feedOutsiteLinkDTO)
    {
        $url_info = parse_url($link);
        $feedOutsiteLinkDTO->site_name = $url_info['host'];

        preg_match('/<TITLE>([\w\W]*?)<\/TITLE>/si', $content, $matches);
        if (!empty($matches[1])) {
            $feedOutsiteLinkDTO->link_title = $matches[1];
        }

        preg_match('/<META\s+name=["\']description["\']\s+content=["\']([\w\W]*?)["\']/si', $content, $matches);
        if (empty($matches[1])) {
            preg_match('/<META\s+content=["\']([\w\W]*?)["\']\s+name=["\']description["\']/si', $content, $matches);
        }
        if (empty($matches[1])) {
            preg_match('/<META\s+http-equiv=["\']description["\']\s+content=["\']([\w\W]*?)["\']/si', $content, $matches);
        }
        if (empty($matches[1])) {
            preg_match('/<META\s+content=["\']description["\']\s+http-equiv=["\']([\w\W]*?)["\']/si', $content, $matches);
        }
        if (!empty($matches[1])) {
            $feedOutsiteLinkDTO->summary = $matches[1];
        }
    }

    private static function buildProductOutsiteLinkDTO(string $link, string $productTitle, string $content, FeedOutsiteLinkDTO &$feedOutsiteLinkDTO)
    {
        $url_info = parse_url($link);
        $feedOutsiteLinkDTO->site_name = $url_info['host'];
        $feedOutsiteLinkDTO->link_title = $productTitle;
        $feedOutsiteLinkDTO->summary = $content;

    }
}