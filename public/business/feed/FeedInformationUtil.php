<?php


namespace business\feed;


class FeedInformationUtil
{
    public static function buildInformationUrl(int $infoId): string
    {
        return \FUR_Config::get_common('h5_site') . "/article-detail?id=" . $infoId;
    }

}