<?php


namespace business\feed;


use facade\request\feed\creation\entity\FeedBaseEntity;
use facade\request\feed\creation\entity\FeedImageEntity;
use facade\request\feed\creation\entity\FeedInformationEntity;
use facade\request\feed\creation\entity\FeedSubfeedEntity;
use facade\request\feed\creation\FeedContent;
use facade\request\product\GetRelatedReviewParams;
use facade\request\product\GetProductDetailParams;
use facade\request\tag\QueryTagEntityListParams;
use facade\response\feed\detail\decoration\FeedHashtag;
use facade\response\feed\detail\tag\FeedContentTag;
use facade\response\Result;
use lib\ClientRouteHelper;
use lib\AppConstant;
use model\dto\FeedInfoDTO;
use model\dto\tag\TagEntityDTO;
use model\feed\dto\FeedInformationDTO;
use model\search\dto\SearchResultDTO;
use service\feed\impl\FeedInformationServiceImpl;
use service\feed\impl\FeedServiceImpl;
use service\product\impl\ProductReviewServiceImpl;
use service\product\impl\ProductServiceImpl;
use service\tag\impl\TagServiceImpl;

class FeedVirtualFeedBuildUtil
{

    public static function buildSearchFeed(?array $searchResultDTOList): ?array
    {

        $feedInfoDTOList = [];
        if ($searchResultDTOList == null || empty($searchResultDTOList)) {
            return null;
        }

        $skuIdList = [];
        $starIdList = [];
        $itemIdList = [];
        /** @var SearchResultDTO $item */
        foreach ($searchResultDTOList as $item) {
            if ($item->content_type == AppConstant::BIZ_TYPE_SKU) {
                array_push($skuIdList, $item->id);
            } elseif ($item->content_type == AppConstant::BIZ_TYPE_SPU_ITEM) {
                array_push($itemIdList, $item->id);
            } elseif ($item->content_type == AppConstant::BIZ_TYPE_STAR) {
                array_push($starIdList, $item->id);
            }
        }

        $starFeedInfoDTO = self::buildStarFeed($starIdList);
        if ($starFeedInfoDTO != null) {
            $starFeedInfoDTO->template_type = FeedInfoDTO::TEMPLATE_TYPE_STANDARD_STAR_4;
            array_push($feedInfoDTOList, $starFeedInfoDTO);
        }
        $productFeedInfoDTO = self::buildProductFeed($skuIdList);
        if ($productFeedInfoDTO != null) {
            $productFeedInfoDTO->template_type = FeedInfoDTO::TEMPLATE_TYPE_PRODUCT_LIST_8;
            array_push($feedInfoDTOList, $productFeedInfoDTO);
        }

        return $feedInfoDTOList;

    }

    public static function buildProductFeed(?array $skuIdList): ?FeedInfoDTO
    {
        $feedInfoDTO = new FeedInfoDTO();

        if ($skuIdList == null || empty($skuIdList)) {
            return null;
        }
        $feedInfoDTO->id = 0;
        $feedInfoDTO->template_type = FeedInfoDTO::TEMPLATE_TYPE_STANDARD_SINGLE_PRODUCT_14;
        $feedInfoDTO->feed_type = FeedInfoDTO::FEED_TYPE_PRODUCT;
        $feedInfoDTO->author_id = 0;
        $feedInfoDTO->published_timestamp = time();
        $feedInfoDTO->virtual = FeedInfoDTO::VIRTUAL_FEED;
        $feedContent = new FeedContent();

        /**
         * 创建商品类型
         */
        $feedEntityList = [];
        $index = 0;
        foreach ($skuIdList as $skuId) {
            if ($skuId == null) {
                continue;
            }
            $feedEntityProduct = new FeedBaseEntity();
            $feedEntityProduct->entity_id = $skuId;
            $feedEntityProduct->index = $index;
            $feedEntityProduct->entity_type = FeedBaseEntity::ENTITY_TYPE_PRODUCT;
            array_push($feedEntityList, $feedEntityProduct);
            $index++;
        }

        $feedContent->feed_entity_list = $feedEntityList;
        $feedInfoDTO->content = json_encode($feedContent);
        return $feedInfoDTO;
    }

    public static function buildInformationFeed(?int $infoId = 0): ?FeedInfoDTO
    {

        if ($infoId == 0) {
            return null;
        }


        $infoDetailRet = FeedInformationServiceImpl::getInstance()->getFeedInformationDetail($infoId);
        if (Result::isSuccess($infoDetailRet) == false) {
            return null;
        }

        /** @var FeedInformationDTO $informationDTO */
        $informationDTO = $infoDetailRet->data;

        $feedInfoDTO = new FeedInfoDTO();
        $feedInfoDTO->id = 0;
        $feedInfoDTO->template_type = FeedInfoDTO::TEMPLATE_TYPE_ARTICLE_12;
        $feedInfoDTO->feed_type = FeedInfoDTO::FEED_TYPE_INFORMATION;
        $feedInfoDTO->author_id = (int)$informationDTO->author_id;
        $feedInfoDTO->published_timestamp = $informationDTO->created_timestamp;
        if ($feedInfoDTO->author_id == 0) {
//            $feedInfoDTO->virtual = FeedInfoDTO::VIRTUAL_FEED;
        }


        $feedContent = new FeedContent();
        $feedContent->content_title = $informationDTO->title;

        $feedEntityList = [];
        $feedEntityImage = new FeedImageEntity();
        $feedEntityImage->entity_id = $infoId;
        $feedEntityImage->index = 0;
        $feedEntityImage->image = $informationDTO->main_image;
        $feedEntityImage->entity_type = FeedBaseEntity::ENTITY_TYPE_IMAGE;
        $feedEntityImage->content = $informationDTO->title;

        $link = $informationDTO->outsite_link;
        if ($informationDTO->is_original == 1) {
            $feedContentTag = new FeedContentTag();
            $feedContentTag->icon = \FUR_Config::get_static_config('feed')['icon_content_tag_exclusive'];

//            $feedContentTag1 = new FeedContentTag();
//            $feedContentTag1->icon = \FUR_Config::get_static_config('feed')['icon_content_tag_top'];
            $feedContent->content_tag_list = [$feedContentTag];
            $link = FeedInformationUtil::buildInformationUrl($infoId);
        }
        $feedEntityImage->entity_link = $link;

        array_push($feedEntityList, $feedEntityImage);

        $feedContent->feed_extra_info = '';
        $feedContent->feed_entity_list = $feedEntityList;
        $feedInfoDTO->content = json_encode($feedContent);
        $feedInfoDTO->detail_url = $link;
        return $feedInfoDTO;
    }

    public
    static function buildStarFeed(?array $starIdList): ?FeedInfoDTO
    {
        if ($starIdList == null || empty($starIdList)) {
            return null;
        }

        $feedInfoDTO = new FeedInfoDTO();
        $feedInfoDTO->id = 0;
        $feedInfoDTO->template_type = FeedInfoDTO::TEMPLATE_TYPE_STANDARD_STAR_4;
        $feedInfoDTO->feed_type = FeedInfoDTO::FEED_TYPE_STAR_CARD;
        $feedInfoDTO->author_id = 0;
        $feedInfoDTO->published_timestamp = time();
        $feedInfoDTO->virtual = FeedInfoDTO::VIRTUAL_FEED;
        $feedContent = new FeedContent();

        /**
         * 创建商品类型
         */
        $feedEntityList = [];
        $index = 0;
        foreach ($starIdList as $starId) {
            $feedEntityProduct = new FeedBaseEntity();
            $feedEntityProduct->entity_id = $starId;
            $feedEntityProduct->index = $index;
            $feedEntityProduct->entity_type = FeedBaseEntity::ENTITY_TYPE_STAR;
            array_push($feedEntityList, $feedEntityProduct);
            $index++;
        }
        $feedContent->feed_entity_list = $feedEntityList;
        $feedInfoDTO->content = json_encode($feedContent);
        return $feedInfoDTO;
    }

    public static function buildSuperProductFeed(int $skuId): ?FeedInfoDTO
    {
        if ($skuId == 0) {
            return null;
        }
        $feedInfoDTO = new FeedInfoDTO();
        $feedInfoDTO->id = 0;
        $feedInfoDTO->template_type = FeedInfoDTO::TEMPLATE_TYPE_SUPER_PRODUCT_1;
        $feedInfoDTO->feed_type = FeedInfoDTO::FEED_TYPE_PRODUCT;
        $feedInfoDTO->author_id = 0;
        $feedInfoDTO->published_timestamp = time();
        $feedInfoDTO->virtual = FeedInfoDTO::VIRTUAL_FEED;
        $feedContent = new FeedContent();
        $feedEntityList = [];


        /**
         * 构建商品实体
         */

        $productSkuDetailParams = new  GetProductDetailParams();
        $productSkuDetailParams->sku_id = $skuId;
        $productSkuRet = ProductServiceImpl::getInstance()->getProductDetail($productSkuDetailParams);
        if (!Result::isSuccess($productSkuRet)) {
            return null;
        }
        $feedEntityProduct = new FeedBaseEntity();
        $feedEntityProduct->entity_id = $skuId;
        $feedEntityProduct->index = 0;
        $feedEntityProduct->entity_type = FeedBaseEntity::ENTITY_TYPE_PRODUCT;
        array_push($feedEntityList, $feedEntityProduct);


        /**
         * 构建评价实体
         */
        $queryTagEntityListParams = new  QueryTagEntityListParams ();
        $queryTagEntityListParams->tag_biz_id = $skuId;
        $queryTagEntityListParams->tag_biz_type = TagEntityDTO::BIZ_TYPE_PRODUCT_SUPER_SKU;
        $queryTagEntityListParams->page_size = 5;
        $queryTagEntityListParams->entity_biz_type = TagEntityDTO::BIZ_TYPE_FEED;


        $tagTagEntityListRet = TagServiceImpl::getInstance()->queryTagEntityList($queryTagEntityListParams);
        if (Result::isSuccess($tagTagEntityListRet)) {
            if (!empty($tagTagEntityListRet->data)) {
                $feedId = 0;
                foreach ($tagTagEntityListRet->data as $tagFeedId) {
                    $feedInfoRet = FeedServiceImpl::getInstance()->getFeedDetail($tagFeedId);
                    if (!Result::isSuccess($feedInfoRet)) {
                        continue;
                    }
                    if ($feedInfoRet->data->status != 1) {
                        continue;
                    }
                    $feedId = $tagFeedId;
                    break;
                }


                $feedEntitySubfeed = new FeedSubfeedEntity();
                $feedEntitySubfeed->entity_id = $feedId;
                $feedEntitySubfeed->index = 1;
                $feedEntitySubfeed->entity_type = FeedBaseEntity::ENTITY_TYPE_SUBFEED;
                $feedEntitySubfeed->entity_link = ClientRouteHelper::buildSuperProductLink($skuId, $feedId);


                array_push($feedEntityList, $feedEntitySubfeed);
            }
        }

        /**
         * 构建资讯实体
         */
        $queryTagEntityListParams = new  QueryTagEntityListParams ();
        $queryTagEntityListParams->tag_biz_id = $skuId;
        $queryTagEntityListParams->tag_biz_type = TagEntityDTO::BIZ_TYPE_PRODUCT_SKU;
        $queryTagEntityListParams->page_size = 3;
        $queryTagEntityListParams->entity_biz_type = TagEntityDTO::BIZ_TYPE_INFORMATION;

        $tagTagEntityListRet = TagServiceImpl::getInstance()->queryTagEntityList($queryTagEntityListParams);
        if (Result::isSuccess($tagTagEntityListRet)) {
            $tagTagEntityIdList = $tagTagEntityListRet->data;
            if (!empty($tagTagEntityIdList)) {
                $feedEntityInformation = new FeedInformationEntity();
                $feedEntityInformation->entity_id_list = $tagTagEntityIdList;
                $feedEntityInformation->index = 0;
                $feedEntityInformation->entity_type = FeedBaseEntity::ENTITY_TYPE_INFORMATION;
                $feedEntityInformation->entity_link = ClientRouteHelper::buildSuperProductLink($skuId);
                array_push($feedEntityList, $feedEntityInformation);
            }
        }

        $feedContent->feed_entity_list = $feedEntityList;
        /**
         * extra_info
         */
        $feedExtraInfo = [];
        $productSkuDetailParams = new GetProductDetailParams ();
        $productSkuDetailParams->sku_id = $skuId;
        $skuInfoRet = ProductServiceImpl::getInstance()->getProductDetail($productSkuDetailParams);
        if (Result::isSuccess($skuInfoRet)) {
            $feedExtraInfo['header_link_name'] = "#" . $skuInfoRet->data->product_name . "#";
            $feedExtraInfo['header_link_url'] = ClientRouteHelper::buildSuperProductLink($skuId);
        }

        $feedContent->feed_extra_info = json_encode($feedExtraInfo);


        $feedInfoDTO->content = json_encode($feedContent);
        return $feedInfoDTO;
    }

    public static function buildSuperProductFeedV2(int $skuId): ?FeedInfoDTO
    {
        if ($skuId == 0) {
            return null;
        }
        $feedInfoDTO = new FeedInfoDTO();
        $feedInfoDTO->id = 0;
        $feedInfoDTO->template_type = FeedInfoDTO::TEMPLATE_TYPE_POSTER_15;
        $feedInfoDTO->feed_type = FeedInfoDTO::FEED_TYPE_PRODUCT;
        $feedInfoDTO->author_id = 0;
        $feedInfoDTO->published_timestamp = time();
        $feedInfoDTO->virtual = FeedInfoDTO::VIRTUAL_FEED;
        $feedContent = new FeedContent();
        $feedEntityList = [];


        /**
         * 构建商品实体
         */
        $productSkuDetailParams = new GetProductDetailParams();
        $productSkuDetailParams->sku_id = $skuId;
        $productSkuRet = ProductServiceImpl::getInstance()->getProductDetail($productSkuDetailParams);
        if (!Result::isSuccess($productSkuRet)) {
            return null;
        }
        $feedEntityImage = new FeedImageEntity();
        $feedEntityImage->index = 0;
        $feedEntityImage->image = $productSkuRet->data->background_pic;
        $feedEntityImage->entity_type = FeedBaseEntity::ENTITY_TYPE_IMAGE;
        $feedEntityImage->entity_link = ClientRouteHelper::buildSuperProductLink($skuId);
        array_push($feedEntityList, $feedEntityImage);

        $feedContent->feed_entity_list = $feedEntityList;
//        /**
//         * extra_info
//         */
//        $feedExtraInfo = [];
//        $productSkuDetailParams = new ProductSkuDetailParams ();
//        $productSkuDetailParams->sku_id = $skuId;
//        $skuInfoRet = ProductServiceImpl::getInstance()->getProductSkuDetail($productSkuDetailParams);
//        if (Result::isSuccess($skuInfoRet)) {
//            $feedExtraInfo['header_link_name'] = "#" . $skuInfoRet->data->product_name . "#";
//            $feedExtraInfo['header_link_url'] = ClientRouteHelper::buildSuperProductLink($skuId);
//        }
//
//        $feedContent->feed_extra_info = json_encode($feedExtraInfo);
//

        $feedInfoDTO->content = json_encode($feedContent);
        return $feedInfoDTO;
    }


    public static function buildInformationProductFeed(int $informationId): ?FeedInfoDTO
    {
        if ($informationId == 0) {
            return null;
        }
        $feedInfoDTO = new FeedInfoDTO();
        $feedInfoDTO->id = 0;
        $feedInfoDTO->template_type = FeedInfoDTO::TEMPLATE_TYPE_ARTICLE_12;
        $feedInfoDTO->feed_type = FeedInfoDTO::FEED_TYPE_INFORMATION;
        $feedInfoDTO->author_id = 0;
        $feedInfoDTO->published_timestamp = time();
        $feedInfoDTO->virtual = FeedInfoDTO::VIRTUAL_FEED;
        $feedContent = new FeedContent();
        $feedEntityList = [];
        /**
         * 构建资讯实体
         */

        $feedEntityInformation = new FeedInformationEntity();
        $feedEntityInformation->entity_id_list = [$informationId];
        $feedEntityInformation->index = 0;
        $feedEntityInformation->entity_type = FeedBaseEntity::ENTITY_TYPE_INFORMATION;
//        $feedEntityInformation->entity_link = ClientRouteHelper::buildSuperProductLink($skuId);
        array_push($feedEntityList, $feedEntityInformation);

        $feedContent->feed_entity_list = $feedEntityList;

        $feedInfoDTO->content = json_encode($feedContent);
        return $feedInfoDTO;
    }
}