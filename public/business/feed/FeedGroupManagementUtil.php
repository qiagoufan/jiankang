<?php

namespace business\feed;

use business\product\ProductDetailConversionUtil;
use business\product\ProductReviewConversionUtil;
use business\star\StarConversionUtil;
use business\user\UserInfoConversionUtil;
use facade\request\feed\ConversionFeedParams;
use facade\request\feed\creation\entity\FeedBaseEntity;
use facade\request\feed\creation\entity\FeedImageEntity;
use facade\request\feed\creation\entity\FeedInformationEntity;
use facade\request\feed\creation\entity\FeedSubfeedEntity;
use facade\request\feed\creation\FeedContent;
use facade\request\maintain\GetMaintainTaskRankParams;
use facade\request\product\GetProductDetailParams;
use facade\request\RequestParams;
use facade\request\tag\QueryTagEntityListParams;
use facade\request\user\FollowParams;
use facade\request\user\UserCollectParams;
use facade\response\feed\detail\decoration\FeedHashtag;
use facade\response\feed\detail\element\FeedBaseElementResp;
use facade\response\feed\detail\element\FeedImageElementResp;
use facade\response\feed\detail\element\FeedInformationElementResp;
use facade\response\feed\detail\element\FeedLeaderboardElementResp;
use facade\response\feed\detail\element\FeedProductElementResp;
use facade\response\feed\detail\element\FeedReviewElementResp;
use facade\response\feed\detail\element\FeedStarElementResp;
use facade\response\feed\detail\element\FeedSubfeedElementResp;
use facade\response\feed\detail\element\FeedVideoElementResp;
use facade\response\feed\detail\element\fragment\InformationResp;
use facade\response\feed\detail\element\fragment\LeaderResp;
use facade\response\feed\detail\element\fragment\ProductTypeResp;
use facade\response\feed\detail\FeedDetailResp;
use facade\response\feed\group\UserGroupRightResp;
use facade\response\Result;
use facade\response\user\group\StarGroupManagementResp;
use facade\response\user\group\GroupUserInfoResp;
use lib\ClientRouteHelper;
use lib\PregHelper;
use lib\PriceHelper;
use lib\TimeHelper;
use model\dto\FeedInfoDTO;
use model\dto\InteractiveOverviewDTO;
use model\dto\media\VideoInfoDTO;
use model\dto\ProductItemDTO;
use model\dto\ProductReviewDTO;
use model\dto\ProductSkuDTO;
use model\dto\ProductSkuPriceDTO;
use model\dto\tag\TagEntityDTO;
use model\dto\UserDTO;
use model\dto\UserStarDTO;
use model\feed\dto\FeedInformationDTO;
use model\FeedModel;
use model\group\dto\GroupManagerDTO;
use service\feed\impl\FeedInfoRespServiceImpl;
use service\feed\impl\FeedInformationServiceImpl;
use service\feed\impl\FeedServiceImpl;
use service\interactive\impl\InteractiveServiceImpl;
use service\maintain\impl\MaintainLeaderboardServiceImpl;
use service\maintain\impl\MaintainTaskServiceImpl;
use service\media\impl\VideoServiceImpl;
use service\product\impl\ProductReviewServiceImpl;
use service\product\impl\ProductServiceImpl;
use service\tag\impl\TagServiceImpl;
use service\user\impl\CollectServiceImpl;
use service\user\impl\FollowServiceImpl;
use service\user\impl\StarServiceImpl;
use service\user\impl\UserServiceImpl;

class FeedGroupManagementUtil
{

    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function getIpGroupManagementResp(int $starId, ?RequestParams $requestParams = null): ?StarGroupManagementResp
    {
        $groupManagementResp = new StarGroupManagementResp();
        $ownerInfoRet = UserServiceImpl::getInstance()->getValidUserInfo(900021);
        if (Result::isSuccess($ownerInfoRet)) {
            $userInfoResp = UserInfoConversionUtil::getInstance()->conversionUserInfo($ownerInfoRet->data);
            $groupManagementResp->group_owner = $userInfoResp;
        }

        $groupManagementResp->group_manager_list = [];
        $ownerInfoRet = UserServiceImpl::getInstance()->getValidUserInfo(900023);
        if (Result::isSuccess($ownerInfoRet)) {
            $userInfoResp = UserInfoConversionUtil::getInstance()->conversionUserInfo($ownerInfoRet->data);
            array_push($groupManagementResp->group_manager_list, $userInfoResp);
        }
        $ownerInfoRet = UserServiceImpl::getInstance()->getValidUserInfo(900024);
        if (Result::isSuccess($ownerInfoRet)) {
            $userInfoResp = UserInfoConversionUtil::getInstance()->conversionUserInfo($ownerInfoRet->data);
            $groupManagementResp->group_owner = $userInfoResp;
            array_push($groupManagementResp->group_manager_list, $userInfoResp);
        }
        $groupManagementResp->enable_apply_group_manager = true;
        $groupManagementResp->enable_apply_group_owner = false;
        return $groupManagementResp;
    }

    public function buildUserGroupRightResp(int $managerType): ?UserGroupRightResp
    {
        $groupRight = new UserGroupRightResp();
        if ($managerType == 0) {
            return $groupRight;
        }

        $groupRight->right_enable_delete_feed = 1;
        if ($managerType == GroupManagerDTO::MANAGER_TYPE_OWNER) {
            $groupRight->right_enable_remove_manager = 1;
            $groupRight->right_enable_approve_manager = 1;
        }
        return $groupRight;
    }


    public function checkUserManagerRight(int $managerType): ?UserGroupRightResp
    {
        $groupRight = new UserGroupRightResp();
        if ($managerType == 0) {
            return $groupRight;
        }

        $groupRight->right_enable_delete_feed = 1;
        if ($managerType == GroupManagerDTO::MANAGER_TYPE_OWNER) {
            $groupRight->right_enable_remove_manager = 1;
            $groupRight->right_enable_approve_manager = 1;
        }
        return $groupRight;
    }


}