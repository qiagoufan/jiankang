<?php

namespace business\feed;

use business\product\ProductDetailConversionUtil;
use business\product\ProductReviewConversionUtil;
use business\star\StarConversionUtil;
use facade\request\feed\ConversionFeedParams;
use facade\request\feed\creation\entity\FeedBaseEntity;
use facade\request\feed\creation\entity\FeedImageEntity;
use facade\request\feed\creation\FeedContent;
use facade\request\maintain\GetMaintainTaskRankParams;
use facade\request\product\GetProductDetailParams;
use facade\request\tag\QueryTagEntityListParams;
use facade\request\tag\TagEntityParams;
use facade\request\user\FollowParams;
use facade\request\user\UserCollectParams;
use facade\response\feed\detail\element\FeedBaseElementResp;
use facade\response\feed\detail\element\FeedImageElementResp;
use facade\response\feed\detail\element\FeedInformationElementResp;
use facade\response\feed\detail\element\FeedLeaderboardElementResp;
use facade\response\feed\detail\element\FeedProductElementResp;
use facade\response\feed\detail\element\FeedReviewElementResp;
use facade\response\feed\detail\element\FeedStarElementResp;
use facade\response\feed\detail\element\FeedSubfeedElementResp;
use facade\response\feed\detail\element\FeedVideoElementResp;
use facade\response\feed\detail\element\fragment\InformationResp;
use facade\response\feed\detail\element\fragment\LeaderResp;
use facade\response\feed\detail\element\fragment\ProductTypeResp;
use facade\response\Result;
use lib\ClientRouteHelper;
use lib\PregHelper;
use lib\PriceHelper;
use lib\TimeHelper;
use lib\AppConstant;
use model\dto\FeedInfoDTO;
use model\dto\InteractiveOverviewDTO;
use model\dto\media\VideoInfoDTO;
use model\dto\ProductReviewDTO;
use model\dto\ProductSkuDTO;
use model\dto\ProductSkuPriceDTO;
use model\dto\tag\TagEntityDTO;
use model\dto\UserDTO;
use model\dto\UserStarDTO;
use model\feed\dto\FeedInformationDTO;
use service\feed\impl\FeedInfoRespServiceImpl;
use service\feed\impl\FeedInformationServiceImpl;
use service\feed\impl\FeedServiceImpl;
use service\interactive\impl\InteractiveServiceImpl;
use service\maintain\impl\MaintainLeaderboardServiceImpl;
use service\maintain\impl\MaintainTaskServiceImpl;
use service\media\impl\VideoServiceImpl;
use service\product\impl\ProductReviewServiceImpl;
use service\product\impl\ProductServiceImpl;
use service\tag\impl\TagServiceImpl;
use service\user\impl\CollectServiceImpl;
use service\user\impl\FollowServiceImpl;
use service\user\impl\StarServiceImpl;
use service\user\impl\UserServiceImpl;

class FeedDetailConversionUtil
{
    const ICON_TAG_LEADERBOARD = 'http://39.98.210.75/static/app_demo/tag_Ranking@3x.png';
    const SUPER_PRODUCT_BLUR_INFO = "?x-oss-process=image/blur,r_35,s_35";


    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function conversionVideoElement(&$feedVideoEntity, ConversionFeedParams &$conversionFeedParams): ?FeedVideoElementResp
    {
        /**
         * 小程序不允许视频播放
         */
        if ($conversionFeedParams->platform == 'wxMini') {
            return null;
        }

        if (empty($feedVideoEntity)) {
            return null;
        }
        $feedElement = new FeedVideoElementResp();
        $feedElement->index = $feedVideoEntity->index;
        $feedElement->element_type = FeedBaseElementResp::ELEMENT_TYPE_VIDEO;
        $feedElement->icon = \FUR_Config::get_static_config('feed')['icon_tag_video'];
        $feedElement->element_id = $feedVideoEntity->entity_id;
        $feedElement->link = '';

        $videoInfoRet = VideoServiceImpl::getInstance()->getVideoInfoById($feedElement->element_id);
        if (!Result::isSuccess($videoInfoRet)) {
            return null;
        }
        /** @var  VideoInfoDTO $videoInfo */
        $videoInfo = $videoInfoRet->data;

        $playUrl = $videoInfo->play_url ?? $videoInfo->original_play_url;

        $feedElement->video_height = (int)$videoInfo->video_height;
        $feedElement->video_width = (int)$videoInfo->video_width;
        $feedElement->video_duration = (int)$videoInfo->duration;
        $feedElement->cover = $videoInfo->cover_url;
        $feedElement->video_link = $playUrl;
        $feedElement->video_content = $videoInfo->title;
        if ($feedElement->cover == null) {
            //如果是首页,没有封面的素材就丢弃不展示
//            if ($conversionFeedParams == FeedInfoRespServiceImpl::SCENE_INDEX) {
//                return null;
//            }
            $feedElement->cover = \FUR_Config::get_static_config('video')['transcoding_cover'];
        }

        return $feedElement;
    }


    public function conversionProductElement(&$feedProductEntity, ConversionFeedParams &$conversionFeedParams): ?FeedProductElementResp
    {
        if (empty($feedProductEntity)) {
            return null;
        }
        $feedElement = new FeedProductElementResp();
        $feedElement->index = $feedProductEntity->index;
        $feedElement->element_type = FeedBaseElementResp::ELEMENT_TYPE_PRODUCT;
        $feedElement->element_id = $feedProductEntity->entity_id;

        $productSkuDetailParams = new  GetProductDetailParams();
        $productSkuDetailParams->sku_id = (int)$feedElement->element_id;
        $productService = ProductServiceImpl::getInstance();

        $productSkuRet = $productService->getProductDetail($productSkuDetailParams);
        if (!Result::isSuccess($productSkuRet)) {
            return null;
        }
        /**@var ProductSkuDTO $productSkuDTO */
        $productSkuDTO = $productSkuRet->data;

        if ($productSkuDTO->publish_status == ProductSkuDTO::PRODUCT_OFFLINE) {
            return null;
        }

        $feedElement->product_name = $productSkuDTO->product_name;
        $feedElement->subtitle = ProductDetailConversionUtil::buildPurchaseInfo($productSkuDTO);
        $feedElement->main_image = $productSkuDTO->main_pic;

        $feedElement->background = $productSkuDTO->background_pic;
        if ($feedElement->background != '') {
            $feedElement->background .= self::SUPER_PRODUCT_BLUR_INFO;
        }

        $feedElement->sku_id = $productSkuDTO->id;
        $feedElement->item_id = $productSkuDTO->item_id;
        $feedElement->is_super = $productSkuDTO->is_super;

        /**
         *收藏信息
         */
        if ($conversionFeedParams->user_id != 0) {


            $userCollectParams = new  UserCollectParams();
            $userCollectParams->collect_biz_type = 'sku';
            $userCollectParams->collect_biz_id = $feedElement->sku_id;
            $userCollectParams->user_id = $conversionFeedParams->user_id;
            $collectDetailRet = CollectServiceImpl::getInstance()->getCollectDetail($userCollectParams);
            if (Result::isSuccess($collectDetailRet)) {
                $feedElement->collect_status = $collectDetailRet->data->collect_status;
            }
        }

        $productType = new ProductTypeResp();
        $productType->product_type_icon = \FUR_Config::get_static_config('product')['type_icon_star_selection'];
        $productType->product_type_name = '星选商品';
        $feedElement->icon = \FUR_Config::get_static_config('feed')['icon_tag_product'];
        if ($feedElement->is_super) {
            $feedElement->icon = \FUR_Config::get_static_config('feed')['icon_tag_super_product'];
            $productType->product_type_icon = \FUR_Config::get_static_config('product')['type_icon_super_product'];
            $productType->product_type_name = '超级商品';
        }
        $feedElement->product_type = $productType;


        $productSkuPriceRet = $productService->getProductSkuPrice($productSkuDTO);
        if (Result::isSuccess($productSkuPriceRet)) {
            /** @var ProductSkuPriceDTO $productSkuPriceDTO */
            $productSkuPriceDTO = $productSkuPriceRet->data;
            $feedElement->format_price = PriceHelper::conversionDisplayPrice($productSkuPriceDTO->current_price, true);
        }
        if ($feedElement->is_super == 1) {
            $feedElement->link = ClientRouteHelper::buildSuperProductLink($feedElement->sku_id);
        } else {
            $feedElement->link = ClientRouteHelper::buildPdpLink($feedElement->sku_id);
        }
        return $feedElement;
    }

    public function conversionReviewElement(&$feedReviewEntity, ConversionFeedParams &$conversionFeedParams): ?FeedReviewElementResp
    {
        if (empty($feedReviewEntity)) {
            return null;
        }
        $feedElement = new FeedReviewElementResp();
        $feedElement->index = $feedReviewEntity->index;
        $feedElement->element_type = FeedBaseElementResp::ELEMENT_TYPE_REVIEW;
        $feedElement->icon = \FUR_Config::get_static_config('feed')['icon_tag_review'];
        $feedElement->element_id = $feedReviewEntity->entity_id;

        $reviewInfoResult = ProductReviewServiceImpl::getInstance()->getReviewInfo($feedElement->element_id);
        if (!Result::isSuccess($reviewInfoResult)) {
            return null;
        }
        /**@var ProductReviewDTO $productReviewDTO */
        $productReviewDTO = $reviewInfoResult->data;

        $feedElement->link = ProductReviewConversionUtil::fillReviewLink($productReviewDTO);
        $feedElement->image_list = $productReviewDTO->image_list;
        $feedElement->user_id = $productReviewDTO->customer_id;
        $feedElement->content = $productReviewDTO->content;

        $userInfoRet = UserServiceImpl::getInstance()->getValidUserInfo($feedElement->user_id);
        if (Result::isSuccess($userInfoRet)) {
            /**@var  UserDTO $userDTO * */
            $userDTO = $userInfoRet->data;
            $feedElement->nick_name = $userDTO->nick_name;
            $feedElement->avatar = $userDTO->avatar;
        }
        return $feedElement;
    }

    public function conversionInformationElement(&$feedInformationEntity, ConversionFeedParams &$conversionFeedParams): ?FeedInformationElementResp
    {
        if ($feedInformationEntity == null || empty($feedInformationEntity)) {
            return null;
        }
        $feedElement = new FeedInformationElementResp();
        $feedElement->index = $feedInformationEntity->index;
        $feedElement->element_type = FeedBaseElementResp::ELEMENT_TYPE_INFORMATION;
        $feedElement->icon = \FUR_Config::get_static_config('feed')['icon_tag_information'];
        if (isset($feedInformationEntity->entity_id)) {
            $feedElement->element_id = $feedInformationEntity->entity_id;
        }
        if (isset($feedInformationEntity->entity_link)) {
            $feedElement->link = $feedInformationEntity->entity_link;
        }

        $feedElement->information_list = [];
        //目前只有多个新闻绑定的,所以用列表就行了
        if (!isset($feedInformationEntity->entity_id_list) || empty($feedInformationEntity->entity_id_list)) {
            return null;
        }
        $feedInformationService = FeedInformationServiceImpl::getInstance();
        foreach ($feedInformationEntity->entity_id_list as $feedInformationId) {
            $feedInformationDetailRet = $feedInformationService->getFeedInformationDetail($feedInformationId);
            if (!Result::isSuccess($feedInformationDetailRet)) {
                continue;
            }
            /** @var FeedInformationDTO $feedInformationDTO */
            $feedInformationDTO = $feedInformationDetailRet->data;
            $information = new InformationResp();
            $information->link = $feedInformationDTO->outsite_link;
            $information->author_name = $feedInformationDTO->outsite_author;
            $information->publish_time_str = TimeHelper::getPersonalTimeString($feedInformationDTO->created_timestamp);
            if ($feedInformationDTO->is_original) {
                $information->link = FeedInformationUtil::buildInformationUrl($feedInformationId);
                $information->icon = \FUR_Config::get_static_config('feed')['icon_dujia'];
                $userInfoRet = UserServiceImpl::getInstance()->getValidUserInfo($feedInformationDTO->author_id);
                if (Result::isSuccess($userInfoRet) && $userInfoRet->data != null) {
                    $information->author_name = $userInfoRet->data->nick_name;
                }
            }
            if ($information->author_name == null) {
                $information->author_name = '享花用户';
            }
            $information->content = $feedInformationDTO->title;
            array_push($feedElement->information_list, $information);
        }

        return $feedElement;

    }

    public function conversionLeaderboardElement(&$feedLeaderboardEntity, ConversionFeedParams &$conversionFeedParams): ?FeedLeaderboardElementResp
    {
        if (empty($feedLeaderboardEntity)) {
            return null;
        }
        $feedElement = new FeedLeaderboardElementResp();
        $feedElement->index = $feedLeaderboardEntity->index;
        $feedElement->element_type = FeedBaseElementResp::ELEMENT_TYPE_LEADERBOARD;
        $feedElement->icon = self::ICON_TAG_LEADERBOARD;
        $feedElement->element_id = $feedLeaderboardEntity->entity_id;

        $feedElement->link = 'https://detail.tmall.com/item.htm?id=598378584625';


        $feedElement->leader_list = [];
        for ($i = 0; $i < 3; $i++) {
            $leader = new LeaderResp();
            $leader->avatar = 'https://tvax4.sinaimg.cn/crop.0.0.664.664.50/b60d4ac9ly8g8o9207ovtj20ig0igjsg.jpg?KID=imgbed,tva&Expires=1585894046&ssig=eRbd5XY8iC';
            $leader->user_id = 123;
            $leader->nick_name = "知名围观群众";
            $leader->score_str = '55,331 本';
            array_push($feedElement->leader_list, $leader);
        }

        return $feedElement;
    }

    public function conversionStarElement(&$feedStarEntity, ConversionFeedParams &$conversionFeedParams): ?FeedStarElementResp
    {
        if (empty($feedStarEntity)) {
            return null;
        }

        $starService = StarServiceImpl::getInstance();
        $feedElement = new FeedStarElementResp();
        $feedElement->index = $feedStarEntity->index;
        $feedElement->element_type = FeedBaseElementResp::ELEMENT_TYPE_STAR;
        $feedElement->icon = '';
        $feedElement->element_id = $feedStarEntity->entity_id;

        $starInfoRet = $starService->getStarInfo($feedStarEntity->entity_id);
        if (!Result::isSuccess($starInfoRet)) {
            return null;
        }

        /** @var UserStarDTO $starDTO */
        $starDTO = $starInfoRet->data;

        $feedElement->star_name = $starDTO->star_name;
        $feedElement->star_avatar = $starDTO->official_avatar;
        $feedElement->background = $starDTO->official_background;
        $feedElement->user_id = $starDTO->user_id;
        $feedElement->star_id = $starDTO->id;
        $feedElement->follow_status = 0;
        $feedElement->font_color = '#FFFFFF';
        $feedElement->ip_type = $starDTO->ip_type;


        $userId = $conversionFeedParams->user_id;


        $followService = FollowServiceImpl::getInstance();
        $followParams = new FollowParams();
        $followParams->user_id = $userId;
        $followParams->target_type = UserStarDTO::STAR_BIZ_TYPE;
        $followParams->target_id = $feedElement->star_id;
        $followRet = $followService->getFollowDetail($followParams);
        if (Result::isSuccess($followRet)) {
            $feedElement->follow_status = $followRet->data->follow_status;
        }

        
        if ($conversionFeedParams->scene == FeedInfoRespServiceImpl::SCENE_INDEX) {
            if ($conversionFeedParams->star_rank_type == FeedInfoRespServiceImpl::STAR_CARD_TYPE_POPULARITY) {
                if ($starDTO->ip_type == UserStarDTO::IP_TYPE_IDOL) {
                    $leaderboardValueRet = MaintainLeaderboardServiceImpl::getInstance()->getStarPopularityValue($feedElement->star_id);
                    $leaderboardValue = 0;
                    if (Result::isSuccess($leaderboardValueRet)) {
                        $leaderboardValue = intval($leaderboardValueRet->data);
                    }
                    $feedElement->subtitle_content = StarConversionUtil::buildStarPopularityValue($leaderboardValue);
                    $leaderboardRank = '未上榜';
                    /**
                     * 获取人气排名
                     */
                    $starRankRet = MaintainLeaderboardServiceImpl::getInstance()->getStarPopularityRank($feedElement->star_id);

                    if (Result::isSuccess($starRankRet) || $starRankRet->data <= 0 || $starRankRet->data >= 100000) {
                        $leaderboardRank = '未上榜';
                    } else {
                        $leaderboardRank = 'NO.' . $starRankRet->data;
                    }
                    $feedElement->rank = $leaderboardRank;

                }
            } elseif ($conversionFeedParams->star_rank_type == FeedInfoRespServiceImpl::STAR_CARD_TYPE_STAR_ENERGY) {
                if ($starDTO->ip_type == UserStarDTO::IP_TYPE_IDOL) {
                    $taskValue = 0;
                    $taskRank = '未上榜';
                    /**
                     * 获取能量值
                     */
                    $taskValueRet = MaintainTaskServiceImpl::getInstance()->getMaintainTaskValue($feedElement->star_id, GetMaintainTaskRankParams::RANK_TYPE_TASK_TOTAL);
                    if (Result::isSuccess($taskValueRet)) {
                        $taskValue = intval($taskValueRet->data);
                    }


                    $feedElement->subtitle_content = StarConversionUtil::buildStarTaskValue($taskValue);
                    /**
                     * 获取能量排名
                     */
                    $taskRankRet = MaintainTaskServiceImpl::getInstance()->getMaintainTaskRank($feedElement->star_id, GetMaintainTaskRankParams::RANK_TYPE_TASK_TOTAL);
                    if (Result::isSuccess($taskRankRet)) {
                        $taskRank = 'NO.' . intval($taskRankRet->data);
                    }
                    $feedElement->rank = $taskRank;
                }
            }

        } elseif ($conversionFeedParams->scene != FeedInfoRespServiceImpl::SCENE_SEARCH) {
            /**
             * 查找明星相关商品
             */
            $tagService = TagServiceImpl::getInstance();
            $queryTagEntityListParams = new QueryTagEntityListParams ();
            $queryTagEntityListParams->tag_biz_type = UserStarDTO::STAR_BIZ_TYPE;
            $queryTagEntityListParams->tag_biz_id = $starDTO->id;
            $queryTagEntityListParams->entity_biz_type = ProductSkuDTO::PRODUCT_SKU_BIZ_TYPE;
            $queryTagEntityListParams->page_size = 1;

            $productEntityListRet = $tagService->queryTagEntityList($queryTagEntityListParams);
            if (Result::isSuccess($productEntityListRet)) {
                $skuIdList = $productEntityListRet->data;
                if (!empty($skuIdList)) {
                    $skuId = $skuIdList[0];
                    $productSkuDetailParams = new GetProductDetailParams();
                    $productSkuDetailParams->sku_id = $skuId;
                    $productInfoRet = ProductServiceImpl::getInstance()->getProductDetail($productSkuDetailParams);
                    if (Result::isSuccess($productInfoRet)) {
                        $feedElement->subtitle_image = $productInfoRet->data->main_pic;
                        $feedElement->subtitle_content = $productInfoRet->data->product_name;
                    }
                }
            }
        } elseif ($conversionFeedParams->scene == FeedInfoRespServiceImpl::SCENE_SEARCH) {
            $feedElement->star_name = "#" . $feedElement->star_name . "#";
//            if ($starDTO->ip_type == UserStarDTO::IP_TYPE_IDOL) {
//                $leaderboardValueRet = MaintainLeaderboardServiceImpl::getInstance()->getStarPopularityValue($feedElement->star_id);
//                $leaderboardValue = 0;
//                if (Result::isSuccess($leaderboardValueRet)) {
//                    $leaderboardValue = intval($leaderboardValueRet->data);
//                }
//
//                $leaderboardRank = 0;
//                /**
//                 * 获取人气排名
//                 */
//                $starRankRet = MaintainLeaderboardServiceImpl::getInstance()->getStarPopularityRank($feedElement->star_id);
//
//                if (Result::isSuccess($starRankRet)) {
//                    $leaderboardRank = $starRankRet->data;
//                }
//                $feedElement->subtitle_content = StarConversionUtil::buildLeaderboardSummaryV1($leaderboardValue, $leaderboardRank);
//                $feedElement->rank = $leaderboardRank;
//            }

            $tagEntityParams = new TagEntityParams();
            $tagEntityParams->tag_biz_id = $feedElement->star_id;
            $tagEntityParams->tag_biz_type = AppConstant::BIZ_TYPE_STAR;
            $tagEntityParams->entity_biz_type = AppConstant::BIZ_TYPE_FEED;
            $tagEntityCountRet = TagServiceImpl::getInstance()->getTagEntityCount($tagEntityParams);
            if (Result::isSuccess($tagEntityCountRet) && $tagEntityCountRet->data) {
                $feedElement->subtitle_content = $tagEntityCountRet->data . '人正在讨论';
            }

        } else {
            $starRankRet = $starService->getStarRank($feedElement->star_id);
            if (!Result::isSuccess($starRankRet)) {
                $leaderboardRank = -1;
            } else {
                $leaderboardRank = $starRankRet->data;
            }
            $taskValueRet = MaintainTaskServiceImpl::getInstance()->getMaintainTaskValue($feedElement->star_id, GetMaintainTaskRankParams::RANK_TYPE_TASK_TOTAL);
            if (!Result::isSuccess($taskValueRet)) {
                $taskValue = 0;
            } else {
                $taskValue = intval($taskValueRet->data);
            }
            if ($starDTO->ip_type == UserStarDTO::IP_TYPE_IDOL) {
                $feedElement->subtitle_content = StarConversionUtil::buildStarRankSummary($leaderboardRank, $taskValue);
            }

        }

        return $feedElement;
    }

    public function conversionImageElement(&$feedImageEntity, ConversionFeedParams &$conversionFeedParams): ?FeedImageElementResp
    {
        /** @var  FeedImageEntity $feedImageEntity */
        if (empty($feedImageEntity)) {
            return null;
        }
        $feedElement = new FeedImageElementResp();
        $feedElement->index = $feedImageEntity->index;
        $feedElement->element_type = FeedBaseElementResp::ELEMENT_TYPE_IMAGE;

        $feedElement->icon = '';
        if ($conversionFeedParams != null && $conversionFeedParams->scene == FeedInfoRespServiceImpl::SCENE_INDEX) {
            $feedElement->icon = \FUR_Config::get_static_config('feed')['icon_tag_image'];
        }
        if (isset($feedImageEntity->entity_id)) {
            $feedElement->element_id = $feedImageEntity->entity_id;
        }
        if (isset($feedImageEntity->content)) {
            $feedElement->content = $feedImageEntity->content;
        }
        if (isset($feedImageEntity->icon) && $feedImageEntity->icon != null) {
            $feedElement->icon = $feedImageEntity->icon;
        }
        if (isset($feedImageEntity->width)) {
            $feedElement->image_width = $feedImageEntity->width;
        }
        if (isset($feedImageEntity->height)) {
            $feedElement->image_height = $feedImageEntity->height;
        }

        if (isset($feedImageEntity->image) == false) {
            return null;
        }
        try {
            $feedElement->image = $feedImageEntity->image;
            if (isset($feedImageEntity->original_image)) {
                $feedElement->original_image = $feedImageEntity->original_image;
            }
            if ($feedElement->original_image == null) {
                $feedElement->original_image = $feedImageEntity->image;
            }
        } catch (\Exception $e) {
            \FUR_Log::error('conversionImageElement:', json_encode($feedImageEntity));
        }
        if (isset($feedImageEntity->entity_link) && $feedImageEntity->entity_link != null) {
            $feedElement->link = $feedImageEntity->entity_link;
        }


        return $feedElement;
    }

    public function conversionSubfeedElement(&$feedSubfeedEntity, ConversionFeedParams &$conversionFeedParams): ?FeedSubfeedElementResp
    {
        if (empty($feedSubfeedEntity)) {
            return null;
        }
        $feedElement = new FeedSubfeedElementResp();
        $feedElement->index = $feedSubfeedEntity->index;
        $feedElement->element_type = FeedBaseElementResp::ELEMENT_TYPE_SUBFEED;
        $feedElement->icon = \FUR_Config::get_static_config('feed')['icon_tag_review'];
        $feedElement->feed_id = $feedSubfeedEntity->entity_id;
        $feedElement->element_id = $feedSubfeedEntity->entity_id;
        $feedElement->link = $feedSubfeedEntity->entity_link;
        $feedInfoRet = FeedServiceImpl::getInstance()->getFeedDetail($feedElement->element_id);
        if (!Result::isSuccess($feedInfoRet)) {
            return null;
        }
        /**@var FeedInfoDTO $feedInfoDTO */
        $feedInfoDTO = $feedInfoRet->data;


        /**
         * 图片相关
         */
        /** @var FeedContent $feedContent */
        $feedContent = json_decode($feedInfoDTO->content);
        if ($feedContent == null) {
            return null;
        }
        $feedEntityList = $feedContent->feed_entity_list;
        $feedElement->description = PregHelper::httpLinkConversion($feedContent->content_desc);
        $feedElement->image_list = [];
        if (!empty($feedEntityList)) {
            /**@var FeedBaseEntity $feedEntity */
            foreach ($feedEntityList as $feedEntity) {

                if ($feedEntity == null || $feedEntity->entity_type == null) {
                    continue;
                }
                if ($feedEntity->entity_type == FeedBaseEntity::ENTITY_TYPE_IMAGE) {
                    $imageElement = $this->conversionImageElement($feedEntity, $conversionFeedParams);
                }

                if ($imageElement == null) {
                    continue;
                }
                array_push($feedElement->image_list, $imageElement->image);
            }
        }

        /**
         * 作者信息
         */
        $userInfoRet = UserServiceImpl::getInstance()->getValidUserInfo($feedInfoDTO->author_id);
        if (Result::isSuccess($userInfoRet)) {
            /**@var  UserDTO $userDTO * */
            $userDTO = $userInfoRet->data;
            $feedElement->nick_name = $userDTO->nick_name;
            $feedElement->avatar = $userDTO->avatar;
            $feedElement->user_id = $userDTO->id;

        }

        /**
         * 评论的模块
         */
        $interactiveService = InteractiveServiceImpl::getInstance();
        $commentOverViewRet = $interactiveService->queryInteractiveOverview(FeedInfoDTO::FEED_BIZ_TYPE, $feedInfoDTO->id);
        if (Result::isSuccess($commentOverViewRet) == false) {
            $feedElement->content = '我来说几句';
        } else {
            /** @var InteractiveOverviewDTO $interactiveOverviewDTO */
            $interactiveOverviewDTO = $commentOverViewRet->data;

            if ($interactiveOverviewDTO->comment_count != 0) {
                $feedElement->highlight_content = (string)$interactiveOverviewDTO->comment_count;
                $feedElement->content = '人正在讨论';
            } else {
                $feedElement->content = '我来说几句';
            }
        }

        /**
         * 商品hashtag
         */
        if (isset($feedSubfeedEntity->entity_extra_info)) {
            $feedHashTagListJsonObj = json_decode($feedSubfeedEntity->entity_extra_info);
            $feedElement->hashtag_list = $feedHashTagListJsonObj;
        }
//        $feedHashTag = new FeedHashtag();
//        \FUR_Core::copyProperties($feedHashTagJsonObj, $feedHashTag);
//        $feedElement->hashtag = $feedHashTag;
        return $feedElement;
    }
}