<?php


namespace business\feed;

use business\product\ProductDetailConversionUtil;
use facade\request\product\GetProductDetailParams;
use facade\request\tag\TagEntityParams;
use lib\ClientRouteHelper;
use lib\AppConstant;
use model\dto\ProductSkuDTO;
use model\dto\tag\TagEntityDTO;
use service\product\impl\ProductServiceImpl;
use service\tag\impl\TagServiceImpl;
use service\user\impl\StarServiceImpl;
use facade\response\feed\FeedTagResp;
use \model\dto\UserStarDTO;
use facade\response\Result;

class FeedTagConversionUtil
{
    public static function buildInformationUrl(int $infoId): string
    {
        return \FUR_Config::get_common('h5_site') . "/article-detail?id=" . $infoId;
    }


    public static function buildStarFeedTagInfo(?UserStarDTO $userStarDTO): ?FeedTagResp
    {

        if ($userStarDTO == null) {
            return null;
        }

        $starName = $userStarDTO->star_name;
        $starAvatar = $userStarDTO->official_avatar;

        $total_user_num = 0;
        $tagEntityParams = new TagEntityParams();
        $tagEntityParams->tag_biz_type = TagEntityDTO::BIZ_TYPE_STAR;
        $tagEntityParams->tag_biz_id = $userStarDTO->id;
        $tagEntityParams->entity_biz_type = TagEntityDTO::BIZ_TYPE_FEED;
        $tagEntityCountRet = TagServiceImpl::getInstance()->getTagEntityCount($tagEntityParams);
        if (Result::isSuccess($tagEntityCountRet)) {
            $total_user_num = $tagEntityCountRet->data;
        }

        $interactiveInfo = $total_user_num . '人正在讨论';

        $icon = \FUR_Config::get_static_config('feed')['icon_hash_tag_ip_image'];
        $link = ClientRouteHelper::buildStarLink($userStarDTO->id, $userStarDTO->ip_type);

        $FeedTagResp = new FeedTagResp();
        $FeedTagResp->hashtag_id = $userStarDTO->id;
        $FeedTagResp->hashtag_name = '#' . $starName . '#';
        $FeedTagResp->hashtag_avatar = $starAvatar;
        $FeedTagResp->hashtag_interactive = $interactiveInfo;
        $FeedTagResp->hashtag_img = $icon;
        $FeedTagResp->hashtag_link = $link;
        $FeedTagResp->hashtag_type = AppConstant::BIZ_TYPE_STAR;
        return $FeedTagResp;
    }


    public static function buildProductFeedTagInfo(ProductSkuDTO $productSkuDTO): ?FeedTagResp
    {


        $icon = \FUR_Config::get_static_config('feed')['icon_hash_tag_image'];
        $link = ClientRouteHelper::buildPdpLink($productSkuDTO->id);

        $total_user_num = 0;
        $tagEntityParams = new TagEntityParams();
        $tagEntityParams->tag_biz_type = TagEntityDTO::BIZ_TYPE_PRODUCT_SKU;
        $tagEntityParams->tag_biz_id = $productSkuDTO->id;
        $tagEntityParams->entity_biz_type = TagEntityDTO::BIZ_TYPE_FEED;
        $tagEntityCountRet = TagServiceImpl::getInstance()->getTagEntityCount($tagEntityParams);
        if (Result::isSuccess($tagEntityCountRet)) {
            $total_user_num = $tagEntityCountRet->data;
        }
        $interactiveInfo = $total_user_num . '人正在讨论';

        $FeedTagResp = new FeedTagResp();
        $FeedTagResp->hashtag_id = $productSkuDTO->id;
        $FeedTagResp->hashtag_name = '#' . $productSkuDTO->product_name . '#';
        $FeedTagResp->hashtag_avatar = $productSkuDTO->main_pic;
        $FeedTagResp->hashtag_interactive = $interactiveInfo;
        $FeedTagResp->hashtag_img = $icon;
        $FeedTagResp->hashtag_link = $link;
        $FeedTagResp->hashtag_type = AppConstant::BIZ_TYPE_SKU;
        return $FeedTagResp;
    }


    public static function buildProductFeedTagInfoBySkuId(int $skuId): ?FeedTagResp
    {
        $productSkuDetailParams = new GetProductDetailParams();
        $productSkuDetailParams->sku_id = $skuId;

        $skuInfoRet = ProductServiceImpl::getInstance()->getProductDetail($productSkuDetailParams);
        if (Result::isSuccess($skuInfoRet) == false) {
            return null;
        }
        /** @var ProductSkuDTO $productSkuDTO */
        $productSkuDTO = $skuInfoRet->data;

        $icon = \FUR_Config::get_static_config('feed')['icon_hash_tag_image'];
        $link = ClientRouteHelper::buildPdpLink($skuId);

        $interactiveInfo = ProductDetailConversionUtil::buildPurchaseInfo($productSkuDTO);


        $FeedTagResp = new FeedTagResp();
        $FeedTagResp->hashtag_id = $skuId;
        $FeedTagResp->hashtag_name = '#' . $productSkuDTO->product_name . '#';
        $FeedTagResp->hashtag_avatar = $productSkuDTO->main_pic;
        $FeedTagResp->hashtag_interactive = $interactiveInfo;
        $FeedTagResp->hashtag_img = $icon;
        $FeedTagResp->hashtag_link = $link;
        $FeedTagResp->hashtag_type = AppConstant::BIZ_TYPE_SKU;
        return $FeedTagResp;
    }

    public static function buildOutsiteLinkTag(int $id, string $link): ?FeedTagResp
    {
        $linkName = '站外链接';

        $icon = \FUR_Config::get_static_config('feed')['icon_hash_tag_link_image'];
        $FeedTagResp = new FeedTagResp();
        $FeedTagResp->hashtag_id = $id;
        $FeedTagResp->hashtag_name = '#' . $linkName . '#';
        $FeedTagResp->hashtag_avatar = null;
        $FeedTagResp->hashtag_interactive = null;
        $FeedTagResp->hashtag_img = $icon;
        $FeedTagResp->hashtag_link = $link;
        $FeedTagResp->hashtag_type = AppConstant::BIZ_TYPE_OUTSITE;
        return $FeedTagResp;
    }
}