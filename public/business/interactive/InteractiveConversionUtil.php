<?php

namespace business\interactive;


class InteractiveConversionUtil
{

    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public static function parseContent(string $jsonContent): string
    {
        $content = '';
        $jsonObj = json_decode($jsonContent);
        if ($jsonObj != null && isset($jsonObj->text)) {
            $content = $jsonObj->text;
        }
        return $content;

    }

}

