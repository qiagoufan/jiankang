<?php


namespace business\maintain;

use facade\response\feed\StarRelatedActivityResp;
use facade\response\maintain\ActivityAudioInfoResp;
use facade\response\maintain\ActivityTextInfoResp;
use facade\response\Result;
use facade\request\feed\GetStarCardFeedListParams;
use facade\response\maintain\MaintainStarTaskInfoResp;
use model\dto\MaintainTaskRecordDTO;
use service\maintain\impl\MaintainTaskServiceImpl;
use facade\response\maintain\MaintainTaskListResp;
use service\user\impl\UserServiceImpl;
use facade\response\maintain\MaintainTaskHistoryResp;
use service\user\impl\StarServiceImpl;


class MaintainStarTaskConverisonUtil
{
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    //明星打卡列表
    public function conversionStarTaskList(GetStarCardFeedListParams $starParams): ?MaintainTaskListResp
    {


        $MaintainTaskListResp = new MaintainTaskListResp();

        /**
         * 打卡任务列表
         */
        $MaintainTaskListResp->task_list = $this->conversionTaskList($starParams);
        if ($MaintainTaskListResp->task_list == null) {
            return null;
        }


        /**
         * 给明星配置音频
         */
        $audio_config = \FUR_Config::get_audio_config('audio_config');

        if (isset($audio_config['star_audio_list']) && ($audio_config['star_audio_list'] != null)) {

            $star_audio_list = $audio_config['star_audio_list'];
            foreach ($star_audio_list as $key => $value) {
                if ($key == $starParams->star_id) {
                    $ActivityAudioInfoResp = new ActivityAudioInfoResp();
                    $ActivityAudioInfoResp->video_width = 720;
                    $ActivityAudioInfoResp->video_height = 1280;
                    $ActivityAudioInfoResp->video_duration = 17;
                    $ActivityAudioInfoResp->video_link = $value['video_link'];
                    $ActivityAudioInfoResp->video_content = '关注青山活动，一起和我保护地球吧!';
                    $ActivityAudioInfoResp->cover = $value['cover'];
                    $ActivityAudioInfoResp->type = 'audio';
                    $MaintainTaskListResp->task_audio_list = $ActivityAudioInfoResp;
                    break;
                }
            }
        }


        /**
         * 参与打卡的用户信息列表
         */
        $taskUserList = [];
        $starId = $starParams->star_id;
        $maintainTaskServiceImpl = MaintainTaskServiceImpl::getInstance();
        $userIdListRet = $maintainTaskServiceImpl->queryUserListByStarId($starId);
        if (Result::isSuccess($userIdListRet)) {
            foreach ($userIdListRet->data as $userId) {
                $userInfoRet = UserServiceImpl::getInstance()->getValidUserInfo($userId);
                if (Result::isSuccess($userInfoRet)) {
                    /**@var  UserDTO $userDTO * */
                    $userDTO = $userInfoRet->data;
                    $userArray['user_id'] = $userDTO->id;
                    $userArray['avatar'] = $userDTO->avatar;
                    array_push($taskUserList, $userArray);
                }
            }
            $MaintainTaskListResp->task_user_num = count($userIdListRet->data);
        }
        $MaintainTaskListResp->task_user_list = $taskUserList;


        /**
         * 打卡任务文本信息
         */
        $activityTextInfo = new ActivityTextInfoResp();
        $activityTextInfo->title = '青山行动';
        $activityTextInfo->title_logo = 'https://cdn.ipxmall.com/ipxmall/f660b30145ea17c90cd53fbb535c1fc9';
        $activityTextInfo->text = "保护地球计划，是由享花星Mall平台联合于梓贝，王晓天等愿意号召粉丝支持环保的艺人，音乐人们发起的一场长久性活动。\n\n 参与者可以每天前往明星主页“打卡”，代表自己当天遵守了此条环保计划。\n\n打卡所以获得的星能量，能给一定比例兑换农产品。";
        $MaintainTaskListResp->activity_text_info = $activityTextInfo;

        return $MaintainTaskListResp;


    }


    public function conversionTaskList(GetStarCardFeedListParams $starParams): ?array
    {

        $taskListArray = [];
        $starId = $starParams->star_id;

        $maintainTaskServiceImpl = MaintainTaskServiceImpl::getInstance();
        $queryRecordRet = $maintainTaskServiceImpl->queryTaskListByStarId($starId);


        if (Result::isSuccess($queryRecordRet)) {

            foreach ($queryRecordRet->data as $starTask) {
                $maintainStarTaskInfoResp = new MaintainStarTaskInfoResp();
                $maintainStarTaskInfoResp->task_id = $starTask->task_id;
                $taskInfoRet = $maintainTaskServiceImpl->queryTaskInfo($starTask->task_id);


                if (Result::isSuccess($taskInfoRet)) {
                    $maintainStarTaskInfoResp->task_name = $taskInfoRet->data->task_name;
                    $maintainStarTaskInfoResp->task_value = $taskInfoRet->data->value;
                } else {
                    continue;
                }

                //该任务下明星收到的公益值
                $starTotalValueRet = $maintainTaskServiceImpl->queryStarTotalValueByTaskId($starTask->task_id, $starParams->star_id);

                if (Result::isSuccess($starTotalValueRet)) {
                    $maintainStarTaskInfoResp->star_total_value = '星能量贡献总值: ' . $starTotalValueRet->data;
                } else {
                    $maintainStarTaskInfoResp->star_total_value = '星能量贡献总值: 0';
                }

                //该任务下用户贡献的
                if ($starParams->user_id != null) {
                    $userValueRet = $maintainTaskServiceImpl->queryUserValueByTaskId($starTask->task_id, $starTask->star_id, $starParams->user_id);
                    if (Result::isSuccess($userValueRet)) {
                        $maintainStarTaskInfoResp->user_value = '我已贡献: ' . $userValueRet->data;
                    }

                    //查询打卡状态
                    $userTaskStatusRet = $maintainTaskServiceImpl->queryUserTaskRecord($starParams->user_id, $starTask->star_id, $starTask->task_id);
                    if (Result::isSuccess($userTaskStatusRet) && ($userTaskStatusRet->data == true)) {
                        $maintainStarTaskInfoResp->task_status = 1;
                    }

                }

                if ($maintainStarTaskInfoResp->user_value != null) {
                    $maintainStarTaskInfoResp->star_value_summary = $maintainStarTaskInfoResp->star_total_value . '，' . $maintainStarTaskInfoResp->user_value;
                } else {
                    $maintainStarTaskInfoResp->star_value_summary = $maintainStarTaskInfoResp->star_total_value;
                }


                array_push($taskListArray, $maintainStarTaskInfoResp);

            }

        }

        return $taskListArray;

    }


    public function buildUserTaskHistory(MaintainTaskRecordDTO $maintainTaskRecordDTO): MaintainTaskHistoryResp
    {

        $MaintainTaskHistoryResp = new MaintainTaskHistoryResp();
        $MaintainTaskHistoryResp->task_value = $maintainTaskRecordDTO->value;
        $MaintainTaskHistoryResp->task_id = $maintainTaskRecordDTO->task_id;

        $taskInfoRet = MaintainTaskServiceImpl::getInstance()->queryTaskInfo($maintainTaskRecordDTO->task_id);

        if (Result::isSuccess($taskInfoRet)) {
            $MaintainTaskHistoryResp->task_name = $taskInfoRet->data->task_name;
        }

        $startInfoRet = StarServiceImpl::getInstance()->getStarInfo($maintainTaskRecordDTO->star_id);
        if (Result::isSuccess($startInfoRet)) {
            $MaintainTaskHistoryResp->star_name = $startInfoRet->data->star_name;
        }

        $MaintainTaskHistoryResp->task_time = $maintainTaskRecordDTO->created_timestamp;

        return $MaintainTaskHistoryResp;
    }


    /**
     * @return StarRelatedActivityResp
     * 明星主页相关活动
     *
     */
    public function buildStarRelatedAcitivity(): StarRelatedActivityResp
    {
        $StarRelatedActivityResp = new StarRelatedActivityResp();
        $StarRelatedActivityResp->cover = 'https://cdn.ipxmall.com/ipxmall/d94996924b00ac3375a901979ed29b7e';
        $StarRelatedActivityResp->link = 'http://pre-ipxh5.jfshare.com/article-detail?id=2';

        return $StarRelatedActivityResp;
    }


}