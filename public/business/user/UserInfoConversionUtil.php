<?php

namespace business\user;

use facade\request\user\FollowParams;
use facade\response\maintain\contribution\UserContributionResp;
use facade\response\Result;
use facade\response\user\achievement\UserMedalResp;
use facade\response\user\group\GroupUserInfoResp;
use facade\response\user\UserExtraInfoResp;
use facade\response\user\UserInfoResp;
use FUR_Core;
use model\group\dto\GroupManagerDTO;
use model\jk\UserCheckHistoryModel;
use model\user\dto\UserDTO;
use service\feed\impl\FeedServiceImpl;
use service\jk\impl\JkUserHistoryServiceImpl;
use service\maintain\impl\MaintainLeaderboardServiceImpl;
use service\maintain\impl\MaintainTaskServiceImpl;
use service\order\impl\OrderServiceImpl;
use service\user\impl\FollowServiceImpl;
use service\user\impl\UserInviteServiceImpl;
use service\user\impl\UserServiceImpl;

class UserInfoConversionUtil
{

    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function conversionUserInfo(UserDTO &$userDTO): ?UserInfoResp
    {
        $userInfoResp = new UserInfoResp();
        FUR_Core::copyProperties($userDTO, $userInfoResp);

        return $userInfoResp;
    }

    public static function buildSimpleUserInfo(UserDTO $userDTO, ?bool $userManage = false): ?UserInfoResp
    {

        $userInfoResp = new UserInfoResp();
        FUR_Core::copyProperties($userDTO, $userInfoResp);
        if ($userManage) {
            $loginInfoRet = UserServiceImpl::getInstance()->queryLoginAccountByUserId($userDTO->id, \Config_Const::USER_LOGIN_TYPE_PHONE);
            if (Result::isSuccess($loginInfoRet)) {
                $userInfoResp->phone = $loginInfoRet->data;
            }
            $userOrderCountRet = OrderServiceImpl::getInstance()->queryUserOrderCount($userDTO->id);
            if (Result::isSuccess($userOrderCountRet)) {
                $userInfoResp->order_count = $userOrderCountRet->data;
            }
            $userCheckCountRet = JkUserHistoryServiceImpl::getInstance()->getUserHistoryCount($userDTO->id);
            if (Result::isSuccess($userCheckCountRet)) {
                $userInfoResp->check_count = $userCheckCountRet->data;
            }
            $userInviteCountRet = UserInviteServiceImpl::getInstance()->queryUserInviteCount($userDTO->id);
            if (Result::isSuccess($userInviteCountRet)) {
                $userInfoResp->invite_count = $userInviteCountRet->data;
            }
        }
        return $userInfoResp;
    }

    public function getSimpleUserInfoResp(int $userId, ?bool $needPhone = false): ?UserInfoResp
    {
        $userInfoRet = UserServiceImpl::getInstance()->getValidUserInfo($userId);
        if (!Result::isSuccess($userInfoRet)) {
            return null;
        }
        $userDTO = $userInfoRet->data;
        $userInfoResp = new UserInfoResp();
        FUR_Core::copyProperties($userDTO, $userInfoResp);
        if ($needPhone) {
            $loginInfoRet = UserServiceImpl::getInstance()->queryLoginAccountByUserId($userId, \Config_Const::USER_LOGIN_TYPE_PHONE);
            if (Result::isSuccess($loginInfoRet)) {
                $userInfoResp->phone = $loginInfoRet->data;
            }
        }

        return $userInfoResp;
    }

    public function buildContributionResp(int $userId, int $starId)
    {
        $userContributionResp = new UserContributionResp();
        $userContributionResp->user_id = $userId;
        $userContributionResp->rank = '一';
        $userContributionResp->score = 0;
        $userInfoRet = UserServiceImpl::getInstance()->getValidUserInfo($userId);
        if (Result::isSuccess($userInfoRet)) {
            $userContributionResp->avatar = $userInfoRet->data->avatar;
            $userContributionResp->nick_name = $userInfoRet->data->nick_name;
        }
        $userRankRet = MaintainLeaderboardServiceImpl::getInstance()->getStarContributionUserRank($starId, $userId);
        if (Result::isSuccess($userRankRet)) {
            $rank = $userRankRet->data;
            if ($rank > 0) {
                $userContributionResp->rank = $rank;
            }
        }
        $userScoreRet = MaintainLeaderboardServiceImpl::getInstance()->getStarContributionUserScore($starId, $userId);
        if (Result::isSuccess($userScoreRet)) {
            $score = $userScoreRet->data;
            if ($score > 0) {
                $userContributionResp->score = $score;
            }
        }
        return $userContributionResp;
    }

    public function buildGroupUserInfoResp(int $userId, int $starId, ?int $managerType = 0): ?GroupUserInfoResp
    {
        $groupUserInfoResp = new GroupUserInfoResp();
        $groupUserInfoResp->user_id = $userId;
        $userInfoRet = UserServiceImpl::getInstance()->getValidUserInfo($userId);

        if (!Result::isSuccess($userInfoRet)) {
            return null;
        }
        $groupUserInfoResp->avatar = $userInfoRet->data->avatar;
        $groupUserInfoResp->nick_name = $userInfoRet->data->nick_name;

        if ($starId) {
            $summary = '';
            $userRankRet = MaintainLeaderboardServiceImpl::getInstance()->getStarContributionUserRank($starId, $userId);
            if (Result::isSuccess($userRankRet)) {
                $rank = $userRankRet->data;
                if ($rank > 0) {
                    $groupUserInfoResp->rank = $userRankRet->data;
                    $summary .= '贡献值排行：' . $userRankRet->data . ';';
                } else {
                    $groupUserInfoResp->rank = '未上榜';
                    $summary .= '贡献值排行：未上榜;';
                }
            }
            $userScoreRet = MaintainLeaderboardServiceImpl::getInstance()->getStarContributionUserScore($starId, $userId);
            if (Result::isSuccess($userScoreRet)) {
                $score = $userScoreRet->data;
                $groupUserInfoResp->score = $score;
                if ($score > 0) {
                    $summary .= '贡献值：' . $score;
                }
            }
            $groupUserInfoResp->summary = $summary;
            if ($managerType == GroupManagerDTO::MANAGER_TYPE_OWNER) {
                $groupUserInfoResp->user_medal_list = [];
                $userMedal = new UserMedalResp();
                $userMedal->medal_name = '圈主';
                $userMedal->medal_icon = \FUR_Config::get_static_config('user')['group_owner_medal'];
                array_push($groupUserInfoResp->user_medal_list, $userMedal);

            } elseif ($managerType == GroupManagerDTO::MANAGER_TYPE_MANAGER) {
                $groupUserInfoResp->user_medal_list = [];
                $userMedal = new UserMedalResp();
                $userMedal->medal_name = '管理员';
                $userMedal->medal_icon = \FUR_Config::get_static_config('user')['group_manager_medal'];
                array_push($groupUserInfoResp->user_medal_list, $userMedal);
            }
        }
        return $groupUserInfoResp;
    }


}