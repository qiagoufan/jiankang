<?php

namespace business\user;

use facade\request\user\FollowParams;
use facade\response\maintain\contribution\UserContributionResp;
use facade\response\Result;
use facade\response\user\achievement\UserMedalResp;
use facade\response\user\group\GroupUserInfoResp;
use facade\response\user\UserExtraInfoResp;
use facade\response\user\UserInfoResp;
use facade\response\user\yacht\YachtEngineerInfoResp;
use FUR_Core;
use model\group\dto\GroupManagerDTO;
use model\user\dto\UserDTO;
use model\user\dto\UserEngineerDTO;
use service\feed\impl\FeedServiceImpl;
use service\maintain\impl\MaintainLeaderboardServiceImpl;
use service\maintain\impl\MaintainTaskServiceImpl;
use service\user\impl\FollowServiceImpl;
use service\user\impl\UserServiceImpl;

class EngineerInfoConversionUtil
{


    public static function conversionEngineerInfo(UserEngineerDTO &$userEngineerDTO): ?YachtEngineerInfoResp
    {
        $engineerInfoResp = new YachtEngineerInfoResp();
        FUR_Core::copyProperties($userEngineerDTO, $engineerInfoResp);

        return $engineerInfoResp;
    }


}