<?php

namespace business\sign;

use facade\response\Result;
use facade\response\sign\SignTaskUserResp;
use model\sign\dto\SignInTaskDTO;
use model\sign\dto\SignInTaskRecordDTO;
use service\sign\impl\SignServiceImpl;

class SignTaskConversionUtil
{


    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function buildSignTaskUserResp(?SignInTaskDTO &$signInTaskDTO, ?int $userId = null, ?int $date = null): ?SignTaskUserResp
    {
        if ($signInTaskDTO == null) {
            return null;
        }

        $signTaskResp = new SignTaskUserResp();

        \FUR_Core::copyProperties($signInTaskDTO, $signTaskResp);
        $signTaskId = $signInTaskDTO->id;
        if (!$date) {
            $date = date('Ymd');
        }
        $signTaskResp->sign_type_id = $signInTaskDTO->sign_in_type_id;
        $signTaskResp->sign_task_id = $signInTaskDTO->id;
        $userTaskRecordRet = SignServiceImpl::getInstance()->queryUserSignTaskRecord($signTaskId, $userId, $date);
        if (!Result::isSuccess($userTaskRecordRet)) {
            return $signTaskResp;
        }
        /** @var SignInTaskRecordDTO $userTaskRecord */
        $userTaskRecord = $userTaskRecordRet->data;
        $signTaskResp->user_finish_status = $userTaskRecord->user_finish_status;
        $signTaskResp->sign_in_date = $userTaskRecord->sign_in_date;
        $signTaskResp->user_score = $userTaskRecord->user_score;
        return $signTaskResp;
    }
}