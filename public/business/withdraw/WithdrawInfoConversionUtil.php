<?php

namespace business\withdraw;

use facade\request\user\FollowParams;
use facade\response\invoice\order\InvoiceOrderResp;
use facade\response\maintain\contribution\UserContributionResp;
use facade\response\Result;
use facade\response\user\achievement\UserMedalResp;
use facade\response\user\group\GroupUserInfoResp;
use facade\response\user\UserExtraInfoResp;
use facade\response\user\UserInfoResp;
use facade\response\withdraw\order\WithdrawRepairOrderInfoResp;
use facade\response\withdraw\WithdrawInfoResp;
use FUR_Core;
use lib\PriceHelper;
use model\group\dto\GroupManagerDTO;
use model\invoice\dto\InvoiceInfoDTO;
use model\invoice\dto\InvoiceOrderListDTO;
use model\organization\dto\OrganizationStoreDTO;
use model\repair\dto\RepairOrderInfoDTO;
use model\user\dto\UserDTO;
use model\user\dto\UserEngineerDTO;
use model\withdraw\dto\WithdrawRecordDTO;
use service\feed\impl\FeedServiceImpl;
use service\invoice\impl\InvoiceServiceImpl;
use service\maintain\impl\MaintainLeaderboardServiceImpl;
use service\maintain\impl\MaintainTaskServiceImpl;
use service\organization\impl\OrganizationServiceImpl;
use service\repair\impl\RepairOrderServiceImpl;
use service\user\impl\FollowServiceImpl;
use service\user\impl\UserEngineerServiceImpl;
use service\user\impl\UserServiceImpl;

class WithdrawInfoConversionUtil
{

    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function conversionWithdrawInfo(WithdrawRecordDTO &$withdrawRecordDTO, ?bool $full_data = false): ?WithdrawInfoResp
    {
        $withdrawResp = new WithdrawInfoResp();

        $withdrawResp->withdraw_id = $withdrawRecordDTO->id;
        $withdrawResp->creatd_timestamp = $withdrawRecordDTO->created_timestamp;
        $withdrawResp->withdraw_total_amount = PriceHelper::conversionDisplayPrice($withdrawRecordDTO->withdraw_total_amount);
//        $withdrawResp->repair_order_id_list = json_decode($withdrawRecordDTO->repair_order_id_list);
        $withdrawResp->engineer_id = $withdrawRecordDTO->engineer_id;
        $withdrawResp->withdraw_card_num = $withdrawRecordDTO->withdraw_card_num;
        $withdrawResp->withdraw_card_real_name = $withdrawRecordDTO->withdraw_card_real_name;
        $withdrawResp->withdraw_card_bank = $withdrawRecordDTO->withdraw_card_bank;
        $withdrawResp->cash_out_status = $withdrawRecordDTO->cash_out_status;

        if ($full_data) {
            $engineerInfoRet = UserEngineerServiceImpl::getInstance()->getEngineerInfo($withdrawRecordDTO->engineer_id);
            /** @var UserEngineerDTO $engineerInfo */
            $engineerInfo = $engineerInfoRet->data;
            $withdrawResp->engineer_phone_num = $engineerInfo->phone_num;
            $withdrawResp->engineer_name = $engineerInfo->real_name;
            $withdrawResp->repair_order_list = $this->buildOrderList(json_decode($withdrawRecordDTO->repair_order_id_list));
        }

        return $withdrawResp;
    }


    private function buildOrderList(?array $repair_order_id_list): ?array
    {
        $orderList = [];

        try {
            /** @var int $repair_order_id */
            foreach ($repair_order_id_list as $repair_order_id) {
                $repairOrderResp = new WithdrawRepairOrderInfoResp();
                $repairOrderInfoRet = RepairOrderServiceImpl::getInstance()->getRepairOrder($repair_order_id);
                /** @var RepairOrderInfoDTO $repairOrderInfo */
                $repairOrderInfo = $repairOrderInfoRet->data;

                $repairOrderResp->repair_order_id = $repairOrderInfo->id;
                $repairOrderResp->engineer_reward = PriceHelper::conversionDisplayPrice($repairOrderInfo->engineer_reward);
                $repairOrderResp->created_timestamp = $repairOrderInfo->created_timestamp;
                $repairOrderResp->appointment_timestamp = $repairOrderInfo->appointment_timestamp;
                $repairOrderResp->store_manager_phone = $repairOrderInfo->store_manager_phone;
                $repairOrderResp->store_manager_name = $repairOrderInfo->store_manager_name;
                $repairOrderResp->address = $repairOrderInfo->customer_address;
                $repairOrderResp->store_id = $repairOrderInfo->store_id;
                $storeInfoRet = OrganizationServiceImpl::getInstance()->getStoreInfo($repairOrderInfo->store_id);

                /** @var OrganizationStoreDTO $storeInfoDTO */
                $storeInfoDTO = $storeInfoRet->data;
                $repairOrderResp->store_name = $storeInfoDTO->store_name;
                $repairOrderResp->store_num = $storeInfoDTO->store_num;
                array_push($orderList, $repairOrderResp);
            }
        } catch (\Exception $e) {
        }

        return $orderList;
    }


}