<?php

namespace business\star;

use facade\request\maintain\GetMaintainTaskRankParams;
use facade\request\user\FollowParams;
use facade\response\Result;
use facade\response\user\StarInfoResp;
use lib\NumHelper;
use model\dto\UserStarDTO;
use service\maintain\impl\MaintainLeaderboardServiceImpl;
use service\maintain\impl\MaintainTaskServiceImpl;
use service\user\impl\FollowServiceImpl;
use service\user\impl\StarServiceImpl;
USE model\dto\MaintainTaskRecordDTO;

class StarConversionUtil
{


    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    /**
     * 构造明星信息resp
     * @param int $starId
     * @param int $userId
     * @param bool|null $needMaintain
     * @return StarInfoResp|null
     */
    public function buildStarInfoResp(int $starId, int $userId, ?bool $needMaintain = true): ?StarInfoResp
    {
        if ($starId == 0) {
            return null;
        }

        $starService = StarServiceImpl::getInstance();
        $startInfoRet = $starService->getStarInfo($starId);
        if (!Result::isSuccess($startInfoRet)) {
            return null;
        }

        /** @var UserStarDTO $starDTO */
        $starDTO = $startInfoRet->data;

        /**
         * 构造基础信息
         */
        $starInfoResp = new StarInfoResp();
        $starInfoResp->star_id = $starDTO->id;
        $starInfoResp->star_avatar = $starDTO->official_avatar;
        $starInfoResp->star_name = $starDTO->star_name;
        $starInfoResp->star_background = $starDTO->official_background;
        $starInfoResp->star_description = $starDTO->official_description;
        $starInfoResp->star_summary = $starDTO->official_summary;
        if ($starDTO->ip_type != null) {
            $starInfoResp->ip_type = $starDTO->ip_type;
        }

        /**
         * 查看关注关系
         */

        $followParams = new FollowParams();
        $followParams->user_id = $userId;
        $followParams->target_id = $starDTO->id;
        $followParams->target_type = UserStarDTO::STAR_BIZ_TYPE;
        $followDetailRet = FollowServiceImpl::getInstance()->getFollowDetail($followParams);
        if (Result::isSuccess($followDetailRet)) {
            $starInfoResp->follow_status = $followDetailRet->data->follow_status;
            $starInfoResp->fans_count = $followDetailRet->data->fans_count;
        }


        /**
         * 查看打卡状态
         */
        $starInfoResp->task_status = $this->conversionTaskStatus($starInfoResp->follow_status, $starDTO->id, $userId);

        /**
         * 明星IP
         */
        if ($starDTO->ip_type == UserStarDTO::IP_TYPE_IDOL && $needMaintain) {

            $starInfoResp->star_base_introduce = $this->buildStarBaseIntroduce($starDTO);

            /**
             * 获取排名
             */
            $starRankRet = $starService->getStarRank($starId);
            if (!Result::isSuccess($starRankRet)) {
                $starInfoResp->rank = -1;
            } else {
                $starInfoResp->rank = $starRankRet->data;
            }

            /**
             * 获取人气排名
             */
            $starRankRet = MaintainLeaderboardServiceImpl::getInstance()->getStarPopularityRank($starId);

            if (!Result::isSuccess($starRankRet) || $starRankRet->data <= 0 || $starRankRet->data >= 100000) {
                $starInfoResp->leaderboard_rank = '未上榜';
            } else {
                $starInfoResp->leaderboard_rank = 'No.' . $starRankRet->data;
            }

            /**
             * 获取人气值
             */
            $leaderboardValueRet = MaintainLeaderboardServiceImpl::getInstance()->getStarPopularityValue($starId);
            if (!Result::isSuccess($leaderboardValueRet)) {
                $starInfoResp->leaderboard_value = 0;
            } else {
                $starInfoResp->leaderboard_value = $leaderboardValueRet->data == null ? 0 : $leaderboardValueRet->data;
            }


            /**
             * 获取能量值
             */
            $taskValueRet = MaintainTaskServiceImpl::getInstance()->getMaintainTaskValue($starId, GetMaintainTaskRankParams::RANK_TYPE_TASK_TOTAL);
            if (!Result::isSuccess($taskValueRet)) {
                $starInfoResp->task_value = 0;
            } else {
                $starInfoResp->task_value = intval($taskValueRet->data);
            }


            /**
             * 获取能量排名
             */
            $taskRankRet = MaintainTaskServiceImpl::getInstance()->getMaintainTaskRank($starId, GetMaintainTaskRankParams::RANK_TYPE_TASK_TOTAL);
            if (!Result::isSuccess($taskRankRet)) {
                $starInfoResp->task_rank = -1;
            } else {
                $starInfoResp->task_rank = intval($taskRankRet->data);
            }


            /**
             * 用户贡献值
             */
            $userScoreRet = MaintainLeaderboardServiceImpl::getInstance()->getStarContributionUserScore($starId, $userId);
            if (Result::isSuccess($userScoreRet)) {
                $score = $userScoreRet->data;
                if ($score > 0) {
                    $starInfoResp->user_contribution = $score;
                }
            }

            $starInfoResp->summary = self::buildStarRankSummary($starRankRet->data, $starInfoResp->task_value);
            $starInfoResp->task_summary = self::buildStarTaskSummary($starInfoResp->task_value, $starInfoResp->task_rank);
            $starInfoResp->fans_leader_summary = self::buildFansLeaderSummary($starInfoResp->fans_count, $starInfoResp->leaderboard_rank);


        } elseif ($starDTO->ip_type == UserStarDTO::IP_TYPE_FAMOUS && $needMaintain) {


            $starService = StarServiceImpl::getInstance();
            $rankRet = $starService->getDiscussionHeatRank($starDTO->id, UserStarDTO::STAR_BIZ_TYPE);

            if (!Result::isSuccess($rankRet) || $rankRet->data <= 0 || $rankRet->data >= 100000) {
                $starInfoResp->brand_leader_board_rank = '未上榜';
            } else {
                $starInfoResp->brand_leader_board_rank = 'No.' . $rankRet->data;
            }

            $valueRet = $starService->getDiscussionHeat($starDTO->id, UserStarDTO::STAR_BIZ_TYPE);
            if (Result::isSuccess($valueRet)) {
                $starInfoResp->brand_hot_value = intval($valueRet->data);
            }

        }


        return $starInfoResp;

    }


    /**
     *明星简介基础信息
     * @param UserStarDTO $starDTO
     */


    public function buildStarBaseIntroduce(UserStarDTO $starDTO): ?array
    {


        $baseIntroduceList = [];
        if ($starDTO->base_introduce != null) {
            $baseIntroduceList = json_decode($starDTO->base_introduce);
        }
        return $baseIntroduceList;

    }


    /**
     * @return string
     * 人气大赏/人气飙升榜显示
     */
    public static function buildLeaderboardSummary(int $leaderboard_value, int $leaderboard_rank): string
    {
        $summary = "";
        if ($leaderboard_rank > 100 || $leaderboard_rank <= 0) {
            $summary .= "暂未上榜";
        } else {
            $summary .= "当前人气大赏排名：No." . $leaderboard_rank;

        }

        if ($leaderboard_value != 0) {
            $summary .= "；人气值：" . NumHelper::getPersonalNumString($leaderboard_value);
        }

        return $summary;
    }

    public static function buildLeaderboardSummaryV1(int $leaderboard_value, int $leaderboard_rank): string
    {
        $summary = "";
        if ($leaderboard_rank > 100 || $leaderboard_rank <= 0) {
            $summary .= "未上榜";
        } else {
            $summary .= "人气大赏：No." . $leaderboard_rank;

        }

        if ($leaderboard_value != 0) {
            $summary .= " ; 人气值：" . NumHelper::getPersonalNumString($leaderboard_value);
        }

        return $summary;
    }

    /**
     * @param int $fans_count
     * @param int $
     * @return string
     * 粉丝数/人气排名
     */
    public static function buildFansLeaderSummary(?string $fans_count, ?string $leaderboard_rank): string
    {
        $summary = "";
        $summary .= $fans_count . '粉丝';
        $summary .= " · " . $leaderboard_rank;

        return $summary;
    }

    /**
     * @return string
     * 公益打赏/星能量飙升榜
     */
    public static function buildStarTaskSummary(int $taskValue, int $taskRank): string
    {
        $summary = "";
        if ($taskValue != 0) {
            $summary .= "星能量总值 " . NumHelper::getPersonalNumString($taskValue);
        } else {
            $summary .= "还没有获得星能量";
        }

        if ($taskRank <= 0) {
            $summary .= "  暂未上榜";
        } else {
            $summary .= " · 星能量排名No." . $taskRank;

        }

        return $summary;

    }


    /**
     * @param int $leaderboardRank
     * @param int $taskValue
     * @return string
     * 明星主页显示
     */
    public static function buildStarRankSummary(int $leaderboardRank, int $taskValue): string
    {
        $summary = "";

        if ($leaderboardRank <= 0) {
            $summary .= "人气大赏:No.-";
        } else {
            $summary .= "人气大赏:No" . $leaderboardRank;
        }

        $summary .= " · 星能量：" . NumHelper::getPersonalNumString($taskValue);

        return $summary;
    }


    public static function buildStarPopularityValue(int $leaderboardValue): string
    {
        if ($leaderboardValue <= 0) {
            $summary = "人气值：0";
        } else {
            $summary = "人气值:" . NumHelper::getPersonalNumString($leaderboardValue);
        }
        return $summary;
    }

    public static function buildStarTaskValue(int $taskValue): string
    {
        if ($taskValue <= 0) {
            $summary = "星能量总值：0";
        } else {
            $summary = "星能量总值:" . NumHelper::getPersonalNumString($taskValue);
        }
        return $summary;
    }

    /**
     * @param int $follow_status
     * @param int $starId
     * @param int $userId
     * @return int
     * 获取星能量榜单任务状态
     */
    public
    function conversionTaskStatus(int $follow_status, int $starId, int $userId): int
    {
        if ($follow_status == 0) {
            $taskStatus = MaintainTaskRecordDTO::TASK_STATUS_UNFOLLOW;//未关注

        } else {

            $maintainTaskServiceImpl = MaintainTaskServiceImpl::getInstance();
            $queryRecordRet = $maintainTaskServiceImpl->queryTaskListByStarId($starId);
            $taskStatus = MaintainTaskRecordDTO::TASK_STATUS_FINISHED;//打卡任务已完成

            if (Result::isSuccess($queryRecordRet)) {
                foreach ($queryRecordRet->data as $taskDTO) {
                    $finishRet = MaintainTaskServiceImpl::getInstance()->queryTaskFinishStatus($starId, $userId, $taskDTO->task_id);
                    if ($finishRet->data == false) {
                        $taskStatus = MaintainTaskRecordDTO::TASK_STATUS_UNFINISHED;//打卡任务未完成
                        break;
                    }
                }
            }
        }

        return $taskStatus;
    }
}