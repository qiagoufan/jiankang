<?php

namespace business\admin;

use \service\admin\impl\AdminLogServiceImpl;
use \model\dto\AdminLogDTO;
use service\user\impl\UserServiceImpl;
use facade\request\admin\CreateAdminLogParams;
use facade\response\Result;

class AdminLogUtil
{

    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function buildAdminLog(CreateAdminLogParams $CreateAdminLogParams)
    {

        $adminLogDTO = new AdminLogDTO();
        \FUR_Core::copyProperties($CreateAdminLogParams, $adminLogDTO);
        $adminLogDTO->created_timestamp = time();
        $adminLogDTO->operate_type = $_SERVER['REQUEST_URI'];
        $adminInfoRet = UserServiceImpl::getInstance()->getValidUserInfo($adminLogDTO->user_id);
        $adminLogDTO->user_name = $adminInfoRet->data->nick_name;

        if (!Result::isSuccess($adminInfoRet)) {
            $adminLogDTO->user_name = '';
        }

        $adminLogServiceImpl = AdminLogServiceImpl::getInstance();
        $adminLogServiceImpl->insertAdminLog($adminLogDTO);
    }
}

