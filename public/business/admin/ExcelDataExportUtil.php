<?php

namespace business\admin;


class ExcelDataExportUtil
{

    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    /**
     * @param string $head
     * @param array $title
     * @param array $data
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     * @throws \PHPExcel_Writer_Exception
     * 数据导出EXCEL
     */
    public static function dataExport($head = '', $title = [], $data = [])
    {

        //表格头的输出
        $objectPHPExcel = new \PHPExcel();
        $objectPHPExcel->setActiveSheetIndex(0);
        $cacheMethod = \PHPExcel_CachedObjectStorageFactory::cache_in_memory_gzip;
        $cacheSettings = array();
        \PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);


        //报表头的输出
        $objectPHPExcel->getActiveSheet()->mergeCells('B1:G1');
        $objectPHPExcel->getActiveSheet()->setCellValue('B1', $head);
        $objectPHPExcel->setActiveSheetIndex(0)->getStyle('B1')->getFont()->setSize(24);
        $objectPHPExcel->setActiveSheetIndex(0)->getStyle('B1')->getAlignment()->setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $letter = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $i = 2;
        $num = 0;
        foreach ($title as $key => $value) {
            $objectPHPExcel->getActiveSheet()->setCellValue($letter[$num] . $i, $value);
            $num++;
        }
        $i = 3;
        foreach ($data as $rows) {
            $num = 0;
            foreach ($rows as $value) {
                //明细的输出
                $objectPHPExcel->getActiveSheet()->setCellValue($letter[$num] . $i, trim($value));
                $num++;
            }
            $i++;
        }

        //清除缓冲区,避免乱码
        ob_end_clean();


        //弹出提示下载文件
        $outFileName = iconv("UTF-8", "GB2312", $head);//解决文件名乱码
        header('Content-Type : application/vnd.ms-excel;charset=utf-8');
        header('Content-Disposition:attachment;filename="' . "$outFileName" . '.xls"');
        $objWriter = \PHPExcel_IOFactory::createWriter($objectPHPExcel, 'Excel5');
        $objWriter->save('php://output');


    }


}

