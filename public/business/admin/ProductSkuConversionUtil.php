<?php

namespace business\admin;


use facade\request\admin\CreateProductDetailParams;
use facade\request\admin\EditProductItemParams;
use facade\request\admin\ProductDescParams;
use facade\request\admin\UpdateProductInfoParams;
use facade\request\tag\CreateTagEntityParams;
use facade\request\tag\QueryTagEntityListParams;
use facade\request\tag\TagEntityParams;
use facade\response\admin\ProductItemResp;
use \facade\response\admin\ProductItemSpecificationsResp;
use facade\response\Result;
use lib\PriceHelper;
use model\dto\tag\TagEntityDTO;
use model\product\dto\ProductItemDTO;
use model\product\ProductSkuModel;
use model\ProductSkuPriceModel;
use service\admin\impl\ProductAdminServiceImpl;
use service\aliyun\AliyunBase;
use service\tag\impl\TagServiceImpl;

class ProductSkuConversionUtil
{

    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public static function updateProductSku(UpdateProductInfoParams $updateProductParams)
    {

        $productSkuDTO = new ProductSkuDTO();
        $productSkuDTO->item_id = $updateProductParams->item_id ? $updateProductParams->item_id : null;
        $productSkuDTO->product_name = $updateProductParams->product_name ? $updateProductParams->product_name : null;
        $productSkuDTO->is_super = $updateProductParams->is_super ? $updateProductParams->is_super : 0;
        $productSkuDTO->background_pic = $updateProductParams->background_pic ? AliyunBase::replaceCdnUrl($updateProductParams->background_pic) : '';
        $productSkuDTO->brand_id = $updateProductParams->brand_id ? $updateProductParams->brand_id : null;
        $productSkuDTO->updated_timestamp = time();
        unset($productSkuDTO->created_timestamp);
        if ($updateProductParams->description != null) {
            $productDescParams = new ProductDescParams();
            $productDescParams->title = $updateProductParams->description;
            $productSkuDTO->description = json_encode($productDescParams);
        }
        $productSkuDTO->summary = $updateProductParams->summary ? $updateProductParams->summary : null;
        $productSkuDTO->main_pic = AliyunBase::replaceCdnUrl($updateProductParams->main_pic);
        $productSkuDTO->gallery_image = json_encode(AliyunBase::replaceImageObjectArrayCdnUrl($updateProductParams->gallery_image));
        $productSkuDTO->detail_image = json_encode(AliyunBase::replaceImageObjectArrayCdnUrl($updateProductParams->detail_image));

        //更新统一的sku信息
        $productSkuModel = ProductSkuModel::getInstance();
        $productSkuModel->updateProductSku($productSkuDTO);
        $productPriceModel = ProductSkuPriceModel::getInstance();

        //更新不同的信息
        if ($updateProductParams->specifications != null) {
            $specifications = $updateProductParams->specifications;
            foreach ($specifications as $specification) {

                $skuId = $specification->sku_id;

                //更新库存
                $updateProductSkuDTO = new ProductSkuDTO();
                $updateProductSkuDTO->stock = intval($specification->stock);
                $updateProductSkuDTO->indexes = $specification->indexes;
                $updateProductSkuDTO->id = $specification->sku_id;
                $updateProductSkuDTO->is_super = isset($updateProductParams->is_super) ? $updateProductParams->is_super : null;
                $updateProductSkuDTO->cate_level_1 = $updateProductParams->cate_level_1 ? $updateProductParams->cate_level_1 : null;
                $updateProductSkuDTO->cate_level_2 = $updateProductParams->cate_level_2 ? $updateProductParams->cate_level_2 : null;
                $updateProductSkuDTO->cate_level_3 = $updateProductParams->cate_level_3 ? $updateProductParams->cate_level_3 : null;
                $productSkuModel->updateProductSkuById($updateProductSkuDTO);
                //更新商品价格
                $productPriceDTO = new ProductSkuPriceDTO();
                if ($specification->current_price && $specification->former_price && $specification->sku_id && isset($updateProductParams->shipping_price)) {
                    $productPriceDTO->current_price = $specification->current_price * 100;
                    $productPriceDTO->former_price = $specification->former_price * 100;
                    $productPriceDTO->sku_id = $specification->sku_id;
                    $productPriceDTO->shipping_price = $updateProductParams->shipping_price * 100;
                    $productPriceModel->updateProductSkuPrice($productPriceDTO);
                }
                //更新商品+明星标签
                self::updateSkuStarTag($skuId, $updateProductParams->star_id);
            }
        }
        return true;
    }


    /**
     * @param ProductItemDTO $productItemDTO
     * @param string $sku
     * @return ProductSkuDTO|null
     * 转换创建商品基础信息
     */
    public static function conversionCreateProductSku(ProductItemDTO $productItemDTO, string $sku): ?ProductSkuDTO
    {

        $productSkuDTO = new ProductSkuDTO();
        $productSkuDTO->item_id = $productItemDTO->id;
        $productSkuDTO->gallery_image = $productItemDTO->gallery_image;
        $productSkuDTO->cate_level_1 = $productItemDTO->cate_level_1;
        $productSkuDTO->cate_level_2 = $productItemDTO->cate_level_2;
        $productSkuDTO->cate_level_3 = $productItemDTO->cate_level_3;
        $productSkuDTO->detail_image = $productItemDTO->detail_image;
        $productSkuDTO->background_pic = $productItemDTO->background_pic;
        $productSkuDTO->description = $productItemDTO->product_desc;

        if ($productItemDTO->product_desc != null) {
            $productDescParams = new ProductDescParams();
            $productDescParams->title = $productItemDTO->product_desc;
            $productSkuDTO->description = json_encode($productDescParams);
        }

        $productSkuDTO->summary = $productItemDTO->summary;
        $productSkuDTO->brand_id = $productItemDTO->brand_id;
        $productSkuDTO->product_name = $productItemDTO->product_name;


        $sku = json_decode($sku);
        $productSkuDTO->indexes = $sku->indexes;
        $productSkuDTO->stock = $sku->stock;
        $productSkuDTO->main_pic = $productItemDTO->main_pic;
        $productSkuDTO->audited_status = CreateProductDetailParams::AUDIT_STATUS_UNFINISHED;
        $productSkuDTO->publish_status = $productItemDTO->publish_status;
        $productSkuDTO->presale_timestamp = time();
        $productSkuDTO->specifications = json_encode($sku->detail);
        $productSkuDTO->created_timestamp = time();
        $productSkuDTO->production_timestamp = time();
        $productSkuDTO->expire_timestamp = $productSkuDTO->created_timestamp + 864000;//暂定
        $productSkuDTO->updated_timestamp = time();
        if (isset($sku->id)) {
            $productSkuDTO->id = $sku->id;
        }
        return $productSkuDTO;
    }


    /**
     * @param CreateProductDetailParams $createProductDetailParams
     * @param string $sku
     * @return ProductSkuPriceDTO|null
     * 转换创建商品价格信息
     */
    public static function conversionCreateProductSkuPrice(CreateProductDetailParams $createProductDetailParams, string $sku): ?ProductSkuPriceDTO
    {

        $sku = json_decode($sku);
        $productSkuPriceDTO = new ProductSkuPriceDTO();
        $productSkuPriceDTO->shipping_price = $createProductDetailParams->shipping_price * 100;
        $productSkuPriceDTO->former_price = $sku->former_price * 100;
        $productSkuPriceDTO->current_price = $sku->current_price * 100;
        $productSkuPriceDTO->sku_id = $sku->id;

        return $productSkuPriceDTO;

    }


    public static function createSkuStarTag(?int $skuId, ?array $starIdArr)
    {

        if ($skuId != null && $starIdArr != null) {
            /**
             * 给商品打上明星标
             */
            $tagService = TagServiceImpl::getInstance();
            if ($starIdArr != []) {
                foreach ($starIdArr as $ipId) {
                    $createTagEntityParams = new  CreateTagEntityParams();
                    $createTagEntityParams->tag_biz_id = $skuId;
                    $createTagEntityParams->tag_biz_type = TagEntityDTO::BIZ_TYPE_PRODUCT_SKU;
                    $createTagEntityParams->entity_biz_id = $ipId;
                    $createTagEntityParams->entity_biz_type = TagEntityDTO::BIZ_TYPE_STAR;
                    $tagService->createTagEntity($createTagEntityParams);
                }
            }

            /**
             * 给明星打上商品标
             */
            if ($starIdArr != []) {
                foreach ($starIdArr as $ipId) {
                    $createTagEntityParams = new  CreateTagEntityParams();
                    $createTagEntityParams->entity_biz_id = $skuId;
                    $createTagEntityParams->entity_biz_type = TagEntityDTO::BIZ_TYPE_PRODUCT_SKU;
                    $createTagEntityParams->tag_biz_id = $ipId;
                    $createTagEntityParams->tag_biz_type = TagEntityDTO::BIZ_TYPE_STAR;
                    $tagService->createTagEntity($createTagEntityParams);
                }
            }
        }

    }


    public static function updateSkuStarTag(int $skuId, array $starIdArr)
    {

        $tagService = TagServiceImpl::getInstance();

        $queryTagEntityListParams = new QueryTagEntityListParams();
        $queryTagEntityListParams->tag_biz_id = $skuId;
        $queryTagEntityListParams->tag_biz_type = TagEntityDTO::BIZ_TYPE_PRODUCT_SKU;
        $queryTagEntityListParams->entity_biz_type = TagEntityDTO::BIZ_TYPE_STAR;
        $queryTagEntityListParams->page_size = 20;


        //删除之前明星下的这个商品标记
        $starTagEntityListRet = $tagService->queryTagEntityList($queryTagEntityListParams);
        if (Result::isSuccess($starTagEntityListRet)) {
            $starIdList = $starTagEntityListRet->data;
            if (empty($starIdList) == false) {
                foreach ($starIdList as $originalStarId) {
                    $tagEntityParams = new  TagEntityParams();
                    $tagEntityParams->tag_biz_id = $originalStarId;
                    $tagEntityParams->tag_biz_type = TagEntityDTO::BIZ_TYPE_STAR;
                    $tagEntityParams->entity_biz_id = $skuId;
                    $tagEntityParams->entity_biz_type = TagEntityDTO::BIZ_TYPE_PRODUCT_SKU;
                    $tagService->deleteTagEntity($tagEntityParams);
                }


            }
        }


        //删旧标记
        if ($skuId != 0) {
            $tagEntityParams = new  TagEntityParams();
            $tagEntityParams->tag_biz_id = $skuId;
            $tagEntityParams->tag_biz_type = TagEntityDTO::BIZ_TYPE_PRODUCT_SKU;
            $tagEntityParams->entity_biz_type = TagEntityDTO::BIZ_TYPE_STAR;
            $tagService->cleanTagEntity($tagEntityParams);
        }


        if ($skuId != 0 && $starIdArr != []) {
            foreach ($starIdArr as $starId) {
                /**
                 * 给商品打上明星标
                 */
                $createTagEntityParams = new  CreateTagEntityParams();
                $createTagEntityParams->tag_biz_id = $skuId;
                $createTagEntityParams->tag_biz_type = TagEntityDTO::BIZ_TYPE_PRODUCT_SKU;
                $createTagEntityParams->entity_biz_id = $starId;
                $createTagEntityParams->entity_biz_type = TagEntityDTO::BIZ_TYPE_STAR;
                $tagService->createTagEntity($createTagEntityParams);

                /**
                 * 给明星打上sku标
                 */
                $createTagEntityParams = new  CreateTagEntityParams();
                $createTagEntityParams->entity_biz_id = $skuId;
                $createTagEntityParams->entity_biz_type = TagEntityDTO::BIZ_TYPE_PRODUCT_SKU;
                $createTagEntityParams->tag_biz_id = $starId;
                $createTagEntityParams->tag_biz_type = TagEntityDTO::BIZ_TYPE_STAR;
                $tagService->createTagEntity($createTagEntityParams);
            }

        }

    }


    public static function conversionProductImgArr(?array $productImageArr)
    {
        $imageArr = [];
        if ($productImageArr != null) {
            foreach ($productImageArr as $imageItem) {
                array_push($imageArr, $imageItem->image);
            }
        }
        return $imageArr;
    }


    public static function conversionProductGallery(?array $productImageArr)
    {
        $imageArr = [];
        if ($productImageArr != null) {
            foreach ($productImageArr as $k => $imageItem) {
                $object = new \stdClass();
                $object->id = $k;
                $object->description = '';
                $object->image = $imageItem;
                array_push($imageArr, $object);
            }
        }
        return $imageArr;
    }


    public static function conversionProductItem(EditProductItemParams $createProductItemParams): ?ProductItemDTO
    {

        $productItemDTO = new ProductItemDTO();
        \FUR_Core::copyProperties($createProductItemParams, $productItemDTO);

        $productItemDTO->product_name = $createProductItemParams->product_name;
        if ($createProductItemParams->categories != null) {
            $categories = $createProductItemParams->categories;
            $productItemDTO->cate_level_1 = $categories[0];
            $productItemDTO->cate_level_2 = $categories[1];
            $productItemDTO->cate_level_3 = $categories[2];

        }


        if ($createProductItemParams->product_desc != null) {
            $productDescParams = new ProductDescParams();
            $productDescParams->title = $createProductItemParams->product_desc;
            $productItemDTO->product_desc = json_encode($productDescParams);
        }


        if ($createProductItemParams->gallery_image != null) {
            $productItemDTO->gallery_image = json_encode(self::conversionProductGallery($createProductItemParams->gallery_image));
        }


        if ($createProductItemParams->detail_image != null) {
            $productItemDTO->detail_image = json_encode(self::conversionProductGallery($createProductItemParams->detail_image));
        }


        return $productItemDTO;
    }


    public function conversionItemDetailResp(ProductItemDTO $productItemDTO): ?ProductItemResp
    {
        $productItemResp = new ProductItemResp();
        $productAdminService = ProductAdminServiceImpl::getInstance();

        \FUR_Core::copyProperties($productItemDTO, $productItemResp);
        $productItemResp->sale = $productItemDTO->sale;
        $productItemResp->stock = $productItemDTO->stock;
        $productItemResp->id = $productItemDTO->id;
        $productItemResp->name = $productItemDTO->product_name;
        $productItemResp->created_timestamp = $productItemDTO->created_timestamp;
        $productItemResp->publish_status = $productItemDTO->publish_status;
        $priceRangeRet = $productAdminService->queryPriceRangeByItemId($productItemDTO->id);
        if (Result::isSuccess($priceRangeRet)) {
            $priceRangeData = $priceRangeRet->data;
            $productItemResp->low_price = PriceHelper::conversionDisplayPrice($priceRangeData->low_price);
            $productItemResp->high_price = PriceHelper::conversionDisplayPrice($priceRangeData->high_price);
        }

        $productItemResp->default_sku_id = $productItemDTO->default_sku_id;
        $productItemResp->detail_image = json_decode($productItemDTO->detail_image);
        $productItemResp->main_pic = $productItemDTO->main_pic;
        $productItemResp->gallery_image = json_decode($productItemDTO->gallery_image);
        $productItemResp->is_super = $productItemDTO->is_super;

        return $productItemResp;
    }

    public function conversionItemSpecification(string $specifications)
    {


        $specificationArray = json_decode($specifications);
        $newSpecificationArray = [];
        foreach ($specificationArray as $k1 => $specification) {

            $productItemSpecificationsResp = new ProductItemSpecificationsResp();
            $productItemSpecificationsResp->name = $specification->name;

            foreach ($specification->attr as $k2 => $attr) {
                $itemArr = [];
                $itemArr['row'] = $k1;
                $itemArr['col'] = $k2;
                $itemArr['name'] = $specification->name;
                $itemArr['attr'] = $attr;
                array_push($productItemSpecificationsResp->items, $itemArr);
            }

            array_push($newSpecificationArray, $productItemSpecificationsResp);
        }

        return $newSpecificationArray;

    }


    public function reverseConversionItemSpecification(string $specifications)
    {
        $newSpecificationArray = [];

        $specifications = json_decode($specifications);

        foreach ($specifications as $k1 => $specification) {

            $object = new \stdClass();
            $object->name = $specification->name;

            $object1 = new \stdClass();
            foreach ($specification->items as $k2 => $item) {

                $object1->$k2 = $item->attr;
                $object->attr = $object1;
            }

            array_push($newSpecificationArray, $object);


        }

        $newSpecificationArray = json_encode($newSpecificationArray);

        return $newSpecificationArray;
    }

}

