<?php

use facade\response\Result;
use Firebase\JWT\JWT;
use service\user\info\JwtPayloadInfo;

if (!defined('PUBLIC_PATH')) {
    return false;
}

class FUR_Controller
{
    const ERROR_NONE = 0;
    public static $session_is_start = false;


    public function session_start()
    {
        if (self::$session_is_start == false) {
            session_start();
            self::$session_is_start = true;
        }
    }

    /**
     * 安全过滤类-全局变量过滤
     * 在Controller初始化的时候已经运行过该变量，对全局变量进行处理
     *
     * @return
     *
     */
    public static function filter()
    {
        if (is_array($_SERVER)) {
            foreach ($_SERVER as $k => $v) {
                if (isset ($_SERVER [$k])) {
                    $_SERVER [$k] = str_replace(array(
                        '<',
                        '>',
                        '"',
                        "'",
                        '%3C',
                        '%3E',
                        '%22',
                        '%27',
                        '%3c',
                        '%3e'
                    ), '', $v);
                }
            }
        }
        unset ($_ENV, $HTFUR_GET_VARS, $HTFUR_POST_VARS, $HTFUR_COOKIE_VARS, $HTFUR_SERVER_VARS, $HTFUR_ENV_VARS);
        self::filter_slashes($_GET);
        self::filter_slashes($_POST);
        self::filter_slashes($_COOKIE);
        self::filter_slashes($_FILES);
        self::filter_slashes($_REQUEST);
    }

    /**
     * 安全过滤类-加反斜杠，放置SQL注入
     *
     * @param string $value
     *            需要过滤的值
     * @return string
     */
    public static function filter_slashes(&$value)
    {
        if (get_magic_quotes_gpc())
            return false; // 开启魔术变量
        $value = ( array )$value;
        foreach ($value as $key => $val) {
            if (is_array($val)) {
                self::filter_slashes($value [$key]);
            } else {
                $value [$key] = urldecode(addslashes($val));
            }
        }
    }

    /**
     * 安全过滤类-过滤javascript,css,iframes,object等不安全参数 过滤级别高
     *
     * @param string $value
     *            需要过滤的值
     * @return string
     */
    public static function filter_script($value)
    {
        $value = preg_replace("/(javascript:)?on(click|load|key|mouse|error|abort|unload|change|dblclick|move|reset|resize|submit)/i", "&111n\\2", $value);
        $value = preg_replace("/<script(.*?)>(.*?)<\/script>/si", "", $value);
        $value = preg_replace("/<iframe(.*?)>(.*?)<\/iframe>/si", "", $value);
        $value = preg_replace("/<object.+<\/object>/iesU", '', $value);
        return $value;
    }

    /**
     * 安全过滤类-过滤HTML标签
     *
     * @param string $value
     *            需要过滤的值
     * @return string
     */
    public static function filter_html($value)
    {
        if (function_exists('htmlspecialchars'))
            return htmlspecialchars($value);
        return str_replace(array(
            "&",
            '"',
            "'",
            "<",
            ">"
        ), array(
            "&amp;",
            "&quot;",
            "&#039;",
            "&lt;",
            "&gt;"
        ), $value);
    }

    /**
     * 安全过滤类-对进入的数据加下划线 防止SQL注入
     *
     * @param string $value
     *            需要过滤的值
     * @return string
     */
    public static function filter_sql($value)
    {
        $sql = array(
            "select",
            'insert',
            "update",
            "delete",
            "\'",
            "\/\*",
            "\.\.\/",
            "\.\/",
            "union",
            "into",
            "load_file",
            "outfile"
        );
        $sql_re = array(
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
        );
        return str_replace($sql, $sql_re, $value);
    }

    /**
     * 安全过滤类-通用数据过滤
     *
     * @param string $value
     *            需要过滤的变量
     * @return string|array
     */
    public static function filter_escape(&$value)
    {
        if (is_array($value)) {
            foreach ($value as $k => $v) {
                $value [$k] = self::filter_str($v);
            }
        } else {
            $value = self::filter_str($value);
        }
        return $value;
    }

    /**
     * 安全过滤类-字符串过滤 过滤特殊有危害字符
     *
     * @param string $value
     *            需要过滤的值
     * @return string
     */
    public static function filter_str($value)
    {
        $value = htmlspecialchars($value);
        return $value;
    }

    /**
     * 私有路径安全转化
     *
     * @param string $fileName
     * @return string
     */
    public static function filter_dir($fileName)
    {
        $tmpname = strtolower($fileName);
        $temp = array(
            '://',
            "\0",
            ".."
        );
        if (str_replace($temp, '', $tmpname) !== $tmpname) {
            return false;
        }
        return $fileName;
    }

    /**
     * 过滤目录
     *
     * @param string $path
     * @return array
     */
    public static function filter_path($path)
    {
        $path = str_replace(array(
            "'",
            '#',
            '=',
            '`',
            '$',
            '%',
            '&',
            ';'
        ), '', $path);
        return rtrim(preg_replace('/(\/){2,}|(\\\){1,}/', '/', $path), '/');
    }

    /**
     * 过滤PHP标签
     *
     * @param string $string
     * @return string
     */
    public static function filter_phptag($string)
    {
        return str_replace(array(
            '<?',
            '?>'
        ), array(
            '&lt;?',
            '?&gt;'
        ), $string);
    }

    /**
     * 安全过滤类-返回函数
     *
     * @param string $value
     *            需要过滤的值
     * @return string
     */
    public static function str_out($value)
    {
        $badstr = array(
            "&",
            '"',
            "'",
            "<",
            ">",
            "%3C",
            "%3E"
        );
        $newstr = array(
            "&amp;",
            "&quot;",
            "&#039;",
            "&lt;",
            "&gt;",
            "&lt;",
            "&gt;"
        );
        $value = str_replace($newstr, $badstr, $value);
        return stripslashes($value); // 下划线
    }

    protected function get($value, $default = "", $isFilter = true)
    {
        if (!is_array($value)) {
            $temp = '';
            if (isset ($_GET [$value]))
                $temp = $_GET [$value];
            $temp = ($isFilter === true) ? self::filter_escape($temp) : $temp;
            return $temp;
        } else {
            $temp = array();
            foreach ($value as $val) {
                $temp [$val] = '';
                if (isset ($_GET [$val]))
                    $temp [$val] = $_GET [$val];
                $temp [$val] = ($isFilter === true) ? self::filter_escape($temp [$val]) : $temp [$val];
            }
            return $temp;
        }
    }

    protected function post($value, $default = "", $isFilter = true)
    {
        if (!is_array($value)) {
            $temp = '';
            if (isset ($_POST [$value])) {
                $temp = $_POST [$value];
            }
            $temp = ($isFilter === true) ? self::filter_escape($temp) : $temp;
            return $temp;
        } else {
            $temp = array();
            foreach ($value as $val) {
                $temp [$val] = '';
                if (isset ($_POST [$val])) {
                    $temp [$val] = $_POST [$val];
                }
                $temp [$val] = ($isFilter === true) ? self::filter_escape($temp [$val]) : $temp [$val];
            }
            return $temp;
        }
    }

    protected function getRequestUserPayloadInfo(bool $force = true): JwtPayloadInfo
    {

        $result = $this->parsePayloadInfoFromToken();
        if ($result->errorCode != 0 && $force) {
            $this->render($result);
        }
        if ($result->data == null) {
            return null;
        }
        $jwtPayloadInfo = new JwtPayloadInfo();
        FUR_Core::copyProperties($result->data, $jwtPayloadInfo);
        return $jwtPayloadInfo;
    }

    private function parsePayloadInfoFromToken(): Result
    {
        $token = "";

        $result = new Result();
        if (isset($_SERVER['HTTP_AUTHORIZATION'])) {
            $token = $_SERVER['HTTP_AUTHORIZATION'];
        }
        if ($token == "") {
            $result->setError(Config_Error::ERR_INVALID_TOKEN);
            return $result;
        }
        $token = substr($token, 7);
        try {
            $payload = JWT::decode($token, FUR_Config::get_common('jwt_key'), FUR_Config::get_common('jwt_alg'));
            if (time() > $payload->exp) {
                $result->setError(Config_Error::ERR_TOKEN_EXPIRED);
                return $result;
            }
            $result->setSuccessWithResult($payload);
        } catch (Exception $e) {
            $result->setError(Config_Error::ERR_INVALID_TOKEN);
            return $result;
        }
        return $result;
    }

    protected function getRequestUserId(bool $force = true): int
    {

        $result = $this->parseUserIdFromToken();
        if ($result->errorCode != 0 && $force) {
            $this->render($result);
        }
        return $result->data;
    }

    private function parseUserIdFromToken(): Result
    {
        $token = "";
        $userId = 0;

        $result = new Result();
        if (isset($_SERVER['HTTP_AUTHORIZATION'])) {
            $token = $_SERVER['HTTP_AUTHORIZATION'];
        }
        $result->setData($userId);
        if ($token == "") {
            $result->setError(Config_Error::ERR_INVALID_TOKEN);
            return $result;
        }
        $token = substr($token, 7);
        try {
            $payload = JWT::decode($token, FUR_Config::get_common('jwt_key'), FUR_Config::get_common('jwt_alg'));
            if (time() > $payload->exp) {
                $result->setError(Config_Error::ERR_TOKEN_EXPIRED);
                return $result;
            }
            $userId = $payload->user_id;
            $result->setSuccessWithResult($userId);
        } catch (Exception $e) {
            $result->setError(Config_Error::ERR_INVALID_TOKEN);
            return $result;
        }
        return $result;
    }

    protected function request($value = null, $default = "", $isFilter = true)
    {


        $content_type = $_SERVER['CONTENT_TYPE'];

        $temp = null;
        if (strpos($content_type, "json")) {
            $temp = $this->jsonRequest($value, $default, $isFilter);
        }
        if ($temp != null) {
            return $temp;
        }


        if (!is_array($value)) {
            $temp = (isset ($_REQUEST [$value])) ? $_REQUEST [$value] : $default;
            $temp = ($isFilter === true) ? self::filter_escape($temp) : $temp;
            FUR_Log::debug('request_' . $value, $temp);
            return $temp;
        } else {
            $temp = array();
            foreach ($value as $val) {
                $temp [$val] = '';
                if (isset ($_REQUEST [$val])) {
                    $temp [$val] = $_REQUEST [$val];
                }
                $temp [$val] = ($isFilter === true) ? self::filter_escape($temp [$val]) : $temp [$val];
            }

            return $temp;
        }
        return null;
    }

    protected function requestObject($clazz, bool $isFilter = true)
    {

        $class = new $clazz;
        try {
            $content_type = $_SERVER['CONTENT_TYPE'];
            $jsonQuery = false;
            if (strpos($content_type, "json")) {
                $stdRequest = $this->jsonRequest();
                if ($stdRequest) {
                    $jsonQuery = true;
                }
                FUR_Core::copyProperties($stdRequest, $class);
            }

            if ($jsonQuery == false) {
                $reflectionClass = new ReflectionClass($class);
                $classProperties = $reflectionClass->getProperties();
                if (empty($classProperties)) {
                    return null;
                }

                foreach ($classProperties as $property) {
                    $key = $property->getName();
                    $v = $this->request($key);
                    if ($v != null) {
                        $class->$key = $v;
                    }
                }
            }
        } catch (Exception $e) {

        }
        return $class;

    }

    protected function jsonRequest($value = null, $default = "", $isFilter = true)
    {
        $context = file_get_contents("php://input");


        FUR_Log::debug('request', $context);
        if (empty($context)) {
            return $default;
        }
        $jsonObj = json_decode($context);
        if ($value == null) {
            return $jsonObj;
        }
        if (!isset($jsonObj->$value)) {
            return $default;
        }
        $data = $jsonObj->$value;
        $data = ($isFilter === true) ? self::filter_escape($data) : $data;

        return $data;
    }


    protected function error($errCode, $errMsg)
    {
        $view = new FUR_View ();
        $view->set_template_config();
        $view->assign('errorCode', $errCode);
        $view->assign('errorMessage', $errMsg);
        $view->display('error_page');
    }

    protected function notice($notice_type, $info = "", $back_url = "")
    {
        // notice =suc or err
        $view = new FUR_View ();
        $view->set_template_config();
        $view->assign('notice_type', $notice_type);
        $view->assign('info', $info);
        $view->assign('back_url', $back_url);
        $view->assign('static_file_url', FUR_Config::get_common("static_file_url"));
        $view->display('header');
        $view->display('common/notice');
    }

    protected function response_ajax($data = array(), $code = 0)
    {
        echo json_encode($data);
    }


    protected function render(Result $result = null)
    {

        FUR_Log::debug('response :', json_encode($result));
        if ($result == null) {
            $result = new Result();
            $result->setSuccessWithResult();
        }
        if ($result->data === null) {
//            $result->data = new stdClass();
        }


        $json_data = json_encode($result, JSON_UNESCAPED_UNICODE);
        header("Content-Type:application/json; charset=utf-8");
        header("Cache-Control:no-store,no-cache,must-revalidate");

        // 跨域CORS
        $allow_cross_domain = false;
        if (isset ($_SERVER ["HTFUR_REFERER"])) {
            $referer_info = parse_url($_SERVER ["HTFUR_REFERER"]);
            if (isset ($_SERVER ["HTFUR_REFERER"]) && strpos($referer_info ['host'], 'ipxmall.com')) {
                $allow_cross_domain = true;
            }
            if ($allow_cross_domain) {
                $cors_host = $referer_info ['scheme'] . "://" . $referer_info ['host'];
                header("Access-Control-Allow-Origin:" . $cors_host);
            }
        }

        $callback = isset ($_REQUEST ['callback']) ? $_REQUEST ['callback'] : '';
        if (is_string($callback) && isset ($callback [0])) {
            if (!$allow_cross_domain) {
                exit ($json_data);
            }
            $callback = htmlspecialchars($callback);
            exit ("{$callback}({$json_data})");
        }
        exit ($json_data);
    }

    protected function display($template, array $data = array())
    {
        $view = new FUR_View ();
        $view->set_template_config();
        if ($data != null) {
            foreach ($data as $k => $v) {
                $view->assign($k, $v);
            }
        }
        $view->assign('static_file_url', FUR_Config::get_common("static_file_url"));
        $view->display($template);
    }

    protected function redirect($url)
    {
        header("Location:$url");
    }


}

