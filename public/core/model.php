<?php

use db\NewRedisSlice;
use db\RedisAdapter;

if (!defined('PUBLIC_PATH')) {
    return false;
}

class FUR_Model
{
    const PAGE_NUM = 20;

    public function __construct()
    {
        $className = get_called_class();
    }

    public static function buildArray($object)
    {
        $data = [];
        $objectVars = get_object_vars($object);
        if (empty($objectVars)) {
            return $data;
        }
        foreach ($objectVars as $k => $v) {
            if ($v === null) {
                continue;
            }
            $data[$k] = $v;
        }
        return $data;
    }


    protected function select_mysql_db($module, $share_id = "")
    {
        if ($share_id == "") {
            return db\MysqlAdapter::singleMySQL($module);
        } else {
            return db\MysqlAdapter::shareMySQL($module);
        }
    }


    protected function getFieldsByClass($clazz)
    {
        $clazz = new $clazz();
        $reflect = new ReflectionClass($clazz);
        $props = $reflect->getProperties(ReflectionProperty::IS_PUBLIC | ReflectionProperty::IS_PROTECTED);
        $props = array_map(function ($v) {
            return $v->getName();
        }, $props);

        return $props;
    }


    protected function share_redis($module, $mode = "master")
    {
        return RedisAdapter::shareRedis($module, $mode);
    }

    protected function single_redis($module, $mode = "master")
    {
        return RedisAdapter::singleRedis($module, $mode);
    }


    protected function get_redis($module)
    {
        return NewRedisSlice::get_instance($module);
    }
}

