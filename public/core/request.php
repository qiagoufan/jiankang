<?php

class FUR_Request {

    private static $_type = 'page';
    private static $_type_api = 'api';
    private static $_type_page = 'page';
	
	/**
	 * Request-获取POST信息
	 * @param  string $name POST的键值名称
	 * @return string
	 */
	public static function get_post($name = '') {
		if (empty($name)) return $_POST;
		return (isset($_POST[$name])) ? $_POST[$name] : '';
	}
	
	/**
	 * Request-获取GET方法的值
	 * @param  string $name GET的键值名称
	 * @return string
	 */
	public static function get_get($name = '') {
		if (empty($name)) return $_GET;
		return (isset($_GET[$name])) ? $_GET[$name] : '';
	}
	
	/**
	 * Request-获取COOKIE信息
	 * @param  string $name COOKIE的键值名称
	 * @return string
	 */
	public static function get_cookie($name = '') {
		if ($name == '') return $_COOKIE;
		return (isset($_COOKIE[$name])) ? $_COOKIE[$name] : '';
	}
	
	/**
	 * Request-获取SESSION信息
	 * @param  string $name SESSION的键值名称
	 * @return string
	 */
	public static function get_session($name = '') {
		if ($name == '') return $_SESSION;
		return (isset($_SESSION[$name])) ? $_SESSION[$name] : '';
	}
	
	/**
	 * Request-获取ENV信息
	 * @param  string $name ENV的键值名称
	 * @return string
	 */
	public static function get_env($name = '') {
		if ($name == '') return $_ENV;
		return (isset($_ENV[$name])) ? $_ENV[$name] : '';
	}
	
	/**
	 * Request-获取SERVICE信息
	 * @param  string $name SERVER的键值名称
	 * @return string
	 */
	public static function get_service($name = '') {
		if ($name == '') return $_SERVER;
		return (isset($_SERVER[$name])) ? $_SERVER[$name] : '';
	}
	
	/**
	 *	Request-获取当前正在执行脚本的文件名
	 *  @return string
	 */
	public static function get_php_self() {
		return self::get_service('PHP_SELF');
	}
	
	/**
	 *	Request-获取当前正在执行脚本的文件
	 *  @return string
	 */
	public static function get_service_name() {
		return self::get_service('SERVER_NAME');
	}
	
	/**
	 *	Request-获取请求时间
	 *  @return int
	 */
	public static function get_request_time() {
		return self::get_service('REQUEST_TIME');
	}
	
	/**
	 * Request-获取useragent信息
	 * @return string
	 */
	public static function get_useragent() {
		return self::get_service('HTTP_USER_AGENT');	
	}	
	
	/**
	 * Request-获取URI信息
	 * @return string
	 */
	public static function get_uri() {
		return self::get_service('REQUEST_URI');
	}

    /**
     * 获取当前浏览器请求地址
     * @return string
     */
    public static function get_current_url(){
        return 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
    }
	
	/**
	 * Request-判断是否为POST方法提交
	 * @return bool
	 */
	public static function is_post() {
		return (strtolower(self::get_service('REQUEST_METHOD')) == 'post') ? true : false;
	}
	
	/**
	 * Request-判断是否为GET方法提交
	 * @return bool
	 */
	public static function is_get() {
		return (strtolower(self::get_service('REQUEST_METHOD')) == 'get') ? true : false;
	}
	
	/**
	 * Request-判断是否为AJAX方式提交
	 * @return bool
	 */
	public static function is_ajax() {
		if (self::get_service('HTTP_X_REQUESTED_WITH') && strtolower(self::get_service('HTTP_X_REQUESTED_WITH')) == 'xmlhttprequest') return true;
		if (self::get_post('is_ajax') || self::get_get('is_ajax')) return true; //程序中自定义AJAX标识
		return false;
	}

    /**
     * 请求设置为api
     */
    public static function set_to_api(){
        self::$_type = self::$_type_api;
    }

    /**
     * 请求设置为page
     */
    public static function set_to_page(){
        self::$_type = self::$_type_page;
    }

    /**
     * 是否是api请求
     * @return bool
     */
    public static function is_api(){
        return self::$_type == self::$_type_api ? true : false;
    }

    /**
     * 是否是页面请求
     * @return bool
     */
    public static function is_page(){
        return self::$_type == self::$_type_page ? true : false;
    }
	
}
