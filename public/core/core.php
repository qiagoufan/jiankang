<?php
if (!defined('PUBLIC_PATH')) {
    return false;
}

class FUR_Core
{
    public static $api;

    /**
     * 程序执行入口-路由解析-调用controller/action
     */
    public static function init()
    {
        self::init_env();
        $Dispatcher = new FUR_Dispatcher ();
        $Dispatcher->dispatcher();
        if (isset ($_GET ['m'], $_GET ['c'], $_GET ['a'])) {
            $uri = sprintf('%s/%s/%s', $_GET ['m'], $_GET ['c'], $_GET ['a']);
        }
        $Run = new FUR_Run ();
        $Run->run();
    }

    /**
     * 获取区分测试和正式环境的变量
     *
     * @return string
     */
    public static function get_server_type()
    {
        if (SERVER_TYPE == 'development') {
            $server_type = 'development';
        } elseif (SERVER_TYPE == 'staging') {
            $server_type = 'staging';
        } else {
            $server_type = 'production';
        }
        return $server_type;
    }

    public static function copyProperties(&$source, &$target)
    {
        if ($source == null || empty($source)) {
            return;
        }
        try {
            $reflectionClass = new ReflectionClass($target);
            $classProperties = $reflectionClass->getProperties();
            if (empty($classProperties)) {
                return;
            }
            foreach ($classProperties as $property) {
                $key = $property->getName();
                if (property_exists($source, $key)) {
                    $target->$key = $source->$key;
                }
            }
        } catch (ReflectionException $e) {
        }


    }


    public static function buildArray($object)
    {
        $data = [];
        $objectVars = get_object_vars($object);
        if (empty($objectVars)) {
            return $data;
        }
        foreach ($objectVars as $k => $v) {
            if ($v === null) {
                continue;
            }
            $data[$k] = $v;
        }
        return $data;
    }

    public function buildClass($clazz, array $array)
    {
        $class = new $clazz;
        if (empty($array)) {
            return $class;
        }
        foreach ($array as $k => $v) {
            $class->$k = $v;
        }
        return $class;
    }

    /**
     * 开发环境打开错误显示
     */
    public static function init_env()
    {
        $server_type = self::get_server_type();
        if ($server_type == 'development') {
            error_reporting(E_ALL);
            ini_set('display_errors', 'On');
        }
    }

    /**
     * 初始化视图模板
     *
     * @return FUR_View
     */
    public static function init_view()
    {
        FUR_Config::set_res_path();
        $view = new FUR_View ();
        $view->set_template_config(); // 设置模板
        return $view;
    }

    /**
     * 获取加载测试或者正式配置文件的路劲
     *
     * @return string
     */
    public static function get_config_path()
    {
        $server_type = self::get_server_type();
        if ($server_type == 'development') {
            $config_path = CONF_PATH . 'test' . DIRECTORY_SEPARATOR;
        } elseif ($server_type == 'staging') {
            $config_path = CONF_PATH . 'staging' . DIRECTORY_SEPARATOR;
        } else {
            $config_path = CONF_PATH . 'release' . DIRECTORY_SEPARATOR;
        }
        return $config_path;
    }

    /**
     * 当类找不到时 自动加载类
     */
    public static function autoload($class)
    {
        $class = ltrim($class, '\\');
        $class = strtr($class, '\\', DIRECTORY_SEPARATOR);
        if (substr($class, 0, 4) == 'FUR_') {
            $class = str_replace('_', DIRECTORY_SEPARATOR, substr($class, 4));
            $filename = CORE_PATH . strtolower($class) . '.php';
        } elseif (substr($class, 0, 7) == 'Config_') {
            $class = str_replace('_', DIRECTORY_SEPARATOR, substr($class, 7));
            $filename = CONF_PATH . strtolower($class) . '.php';
        } else {
            $filename = PUBLIC_PATH . ($class) . '.php';
        }
        // 先在public目录中查找,如果没有找到文件,则尝试去index.php的平行目录找
        if (file_exists($filename)) {
            include $filename;
        } else {
            $filename = APP_PATH . ($class) . '.php';
            if (file_exists($filename)) {
                include $filename;
            }
        }

    }

    /**
     * 接口返回成功数据
     *
     * @param array $data
     * @param string $type
     */
    public static function response_succ($data = array())
    {
        $errorCode = Config_Errno::SUCC;
        $errorMessage = Config_Errmsg::SUCC;
        self::response_data($errorCode, $errorMessage, $data);
    }

    /**
     * json返回数据
     *
     * @param
     *            $errorCode
     * @param string $errorMessage
     * @param array $data
     * @param string $type
     */
    public static function response_data($errorCode, $errorMessage = '', $data = array())
    {

        if (isset ($_GET ['m'], $_GET ['c'], $_GET ['a'])) {
            $uri = sprintf('%s/%s/%s', $_GET ['m'], $_GET ['c'], $_GET ['a']);
            $log = $_SERVER ['REQUEST_URI'];
        }

        $return_data = array(
            'errorCode' => $errorCode,
            'errorMessage' => $errorMessage,
            'data' => $data
        );
        $json_data = json_encode($return_data, JSON_UNESCAPED_UNICODE);
        if (self::is_https()) {
            $json_data = str_ireplace("http:\/\/", "https:\/\/", $json_data);
        }
        header("Content-Type:text/javascript");
        header("Cache-Control:no-store,no-cache,must-revalidate");

        // 跨域CORS
        $allow_cross_domain = false;
        if (isset ($_SERVER ["HTFUR_REFERER"])) {
            $referer_info = parse_url($_SERVER ["HTFUR_REFERER"]);
            if (isset ($_SERVER ["HTFUR_REFERER"]) && strpos($referer_info ['host'], 'youku.com')) {
                $allow_cross_domain = true;
            }
            if ($allow_cross_domain) {
                $cors_host = $referer_info ['scheme'] . "://" . $referer_info ['host'];
                header("Access-Control-Allow-Origin:" . $cors_host);
            }
        }

        $callback = isset ($_REQUEST ['callback']) ? $_REQUEST ['callback'] : '';
        if (is_string($callback) && isset ($callback [0])) {
            if (!$allow_cross_domain) {
                exit ($json_data);
            }
            $callback = htmlspecialchars($callback);
            exit ("{$callback}({$json_data})");
        } else {
            exit ($json_data);
        }
    }


    /**
     * 【静态】XSS过滤，输出内容过滤
     * 1.
     * 框架支持全局XSS过滤机制-全局开启将消耗PHP运行
     * 2. 手动添加XSS过滤函数，在模板页面中直接调用
     *
     * @param string $string
     *            需要过滤的字符串
     * @param string $type
     *            encode HTML处理 | decode 反处理
     * @return string
     */
    public static function output($string, $type = 'encode')
    {
        $html = array(
            "&",
            '"',
            "'",
            "<",
            ">",
            "%3C",
            "%3E"
        );
        $html_code = array(
            "&amp;",
            "&quot;",
            "&#039;",
            "&lt;",
            "&gt;",
            "&lt;",
            "&gt;"
        );
        if ($type == 'encode') {
            if (function_exists('htmlspecialchars'))
                return htmlspecialchars($string);
            $str = str_replace($html, $html_code, $string);
        } else {
            if (function_exists('htmlspecialchars_decode'))
                return htmlspecialchars_decode($string);
            $str = str_replace($html_code, $html, $string);
        }
        return $str;
    }

    /**
     * 返回404错误页面
     */
    public static function return404()
    {
        header("HTTP/1.0 404 Not Found");
        self::_error_page('404', "404 Not Found");
        exit ();
    }

    /**
     * 错误输出页面
     *
     * @param
     *            $errCode
     * @param
     *            $errorMessage
     */
    public static function _error_page($errCode, $errorMessage)
    {
        FUR_Log::warn('error_handler_message', $errorMessage);
        echo $errorMessage;
    }


    /**
     * 设置一个用户定义的错误处理函数
     *
     * @param
     *            $errorCode
     * @param
     *            $errstr
     * @param
     *            $errfile
     * @param
     *            $errline
     */
    public static function error_handler($errorCode, $errstr, $errfile, $errline)
    {
        $errinfo = "[ $errorCode ] $errstr on line $errline in file $errfile";

        FUR_Log::error('error_handler', $errinfo);
    }

    /**
     * Register a function for execution on shutdown
     */
    public static function shutdown_handler()
    {
        $e = error_get_last();
        if ($e == null) {
            return;
        }
        switch ($e ['type']) {
            case E_ERROR :
            case E_PARSE :
            case E_CORE_ERROR :
            case E_COMPILE_ERROR :
            case E_USER_ERROR :
                self::error_handler($e ["type"], $e ["message"], $e ["file"], $e ["line"]);
                break;
        }
    }

    /**
     * 异常处理函数
     *
     * @param
     *            $exception
     */
    public static function exception_handler($exception)
    {
        $errorCode = $exception->getCode();
        $errorMessage = $exception->getMessage();
        $errorTraceAsString = $exception->getTraceAsString();

        $uri = $_SERVER ['REQUEST_URI'];
        $posi = strpos($uri, '?');
        $uri = substr($uri, 0, $posi);

        FUR_Log::warn("Uncaught exception: {$uri} {$errorCode}", $errorMessage);
        FUR_Log::warn("Uncaught exception: {$uri} {$errorCode}", $errorTraceAsString);

        self::response_data(-1, $errorMessage);

    }

    public static function is_https()
    {
        if ($_SERVER ["SERVER_PORT"] == "443") {
            return true;
        }
        if (isset ($_SERVER ['HTFUR_X_REAL_SCHEME']) && $_SERVER ['HTFUR_X_REAL_SCHEME'] == "https") {
            return true;
        }

        if (isset ($_SERVER ['HTFUR_X_FORWARDED_PROTO']) && $_SERVER ['HTFUR_X_FORWARDED_PROTO'] == "https") {
            return true;
        }
        if (!empty ($_SERVER ['HTTPS']) && strtolower($_SERVER ['HTTPS']) !== 'off') {
            return true;
        }
        return false;
    }
}

// 自动加载
spl_autoload_register(array('FUR_Core', 'autoload'));

//if (SERVER_TYPE == null) {
// 设置错误处理函数
set_error_handler(array('FUR_Core', 'error_handler'));
// 致命错误处理函数
register_shutdown_function(array('FUR_Core', 'shutdown_handler'));
////设置一个用户定义的异常处理函数
set_exception_handler(array('FUR_Core', 'exception_handler'));
//}
