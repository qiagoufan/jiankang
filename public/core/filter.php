<?php

class FUR_Filter {
    
    /**
     * 安全过滤类-获取GET或者POST的参数值，经过过滤
     * 如果不指定$type类型，则获取同名的，POST优先
     * $isfilter 默认开启，强制转换请求的数据
     * 该方法在Controller层中，获取所有GET或者POST数据，都需要走这个接口
     * @param  string|array $value 参数
     * @param  string|array $type 获取GET或者POST参数，P - POST ， G - GET, U - PUT , D -DEL
     * @param  bool         $isfilter 变量是否过滤
     * @return string|array
     */
    public static function get_gp($value, $type = null,  $isfilter = false) {
        if (!is_array($value)) {
            $temp = '';
            if ($type === null) {
                if (isset($_GET[$value])) $temp = $_GET[$value];
                if (isset($_POST[$value])) $temp = $_POST[$value];
            } else {
                $temp = (strtoupper($type) == 'G') ? $_GET[$value] : $_POST[$value];
            }
            $temp = ($isfilter === true) ? self::filter_escape($temp) : $temp;
            return $temp;
        } else {
            $temp = array();
            foreach ($value as $val) {
                if ($type === null) {
                    if (isset($_GET[$val])) $temp[$val] = $_GET[$val];
                    if (isset($_POST[$val])) $temp[$val] = $_POST[$val];
                } else {
                    $temp[$val] = (strtoupper($type) == 'G') ? $_GET[$val] : $_POST[$val];
                }
                $temp[$val] = ($isfilter === true) ? self::filter_escape($temp[$val]) : $temp[$val];
            }
            return $temp;
        }
    }
    
    /**
     * 安全过滤类-全局变量过滤
     * 在Controller初始化的时候已经运行过该变量，对全局变量进行处理
     * @return
     */
    public static function filter() {
        if (is_array($_SERVER)) {
            foreach ($_SERVER as $k => $v) {
                if (isset($_SERVER[$k])) {
                    $_SERVER[$k] = str_replace(array('<','>','"',"'",'%3C','%3E','%22','%27','%3c','%3e'), '', $v);
                }
            }
        }
        unset($_ENV, $HTFUR_GET_VARS, $HTFUR_POST_VARS, $HTFUR_COOKIE_VARS, $HTFUR_SERVER_VARS, $HTFUR_ENV_VARS);
        self::filter_slashes($_GET);
        self::filter_slashes($_POST);
        self::filter_slashes($_COOKIE);
        self::filter_slashes($_FILES);
        self::filter_slashes($_REQUEST);
    }
    
    /**
     * 安全过滤类-加反斜杠，放置SQL注入
     * @param  string $value 需要过滤的值
     * @return string
     */
    public static function filter_slashes(&$value) {
        if (get_magic_quotes_gpc()) return false; //开启魔术变量
        $value = (array) $value;
        foreach ($value as $key => $val) {
            if (is_array($val)) {
                self::filter_slashes($value[$key]);
            } else {
                $value[$key] = urldecode(addslashes($val));
            }
        }
    }
    
    /**
     * 安全过滤类-过滤javascript,css,iframes,object等不安全参数 过滤级别高
     * @param  string $value 需要过滤的值
     * @return string
     */
    public static function filter_script($value) {
        $value = preg_replace("/(javascript:)?on(click|load|key|mouse|error|abort|move|unload|change|dblclick|move|reset|resize|submit)/i","&111n\\2",$value);
        $value = preg_replace("/<script(.*?)>(.*?)<\/script>/si","",$value);
        $value = preg_replace("/<iframe(.*?)>(.*?)<\/iframe>/si","",$value);
        $value = preg_replace ("/<object.+<\/object>/iesU", '', $value);
        return $value;
    }
    
    /**
     * 安全过滤类-过滤HTML标签
     * @param  string $value 需要过滤的值
     * @return string
     */
    public static function filter_html($value) {
        if (function_exists('htmlspecialchars')) return htmlspecialchars($value);
        return str_replace(array("&", '"', "'", "<", ">"), array("&amp;", "&quot;", "&#039;", "&lt;", "&gt;"), $value);
    }
    
    /**
     * 安全过滤类-对进入的数据加下划线 防止SQL注入
     * @param  string $value 需要过滤的值
     * @return string
     */
    public static function filter_sql($value) {
        $sql = array("select", 'insert', "update", "delete", "\'", "\/\*", 
                        "\.\.\/", "\.\/", "union", "into", "load_file", "outfile");
        $sql_re = array("","","","","","","","","","","","");
        return str_replace($sql, $sql_re, $value);
    }
    
    /**
     * 安全过滤类-通用数据过滤
     * @param string $value 需要过滤的变量
     * @return string|array
     */
    public static function filter_escape(&$value) {
        if (is_array($value)) {
            foreach ($value as $k => $v) {
                $value[$k] = self::filter_str($v);
            }
        } else {
            $value = self::filter_str($value);
        }
        return $value;
    }
    
    /**
     * 安全过滤类-字符串过滤 过滤特殊有危害字符
     * @param  string $value 需要过滤的值
     * @return string
     */
    public static function filter_str($value) {
        $value = htmlspecialchars($value);
        return $value;
    }
    
    /**
     * 私有路径安全转化
     * @param string $fileName
     * @return string
     */
    public static function filter_dir($fileName) {
        $tmpname = strtolower($fileName);
        $temp = array('://',"\0", "..");
        if (str_replace($temp, '', $tmpname) !== $tmpname) {
            return false;
        }
        return $fileName;
    }
    
    /**
     * 过滤目录
     * @param string $path
     * @return array
     */
    public static function filter_path($path) {
        $path = str_replace(array("'",'#','=','`','$','%','&',';'), '', $path);
        return rtrim(preg_replace('/(\/){2,}|(\\\){1,}/', '/', $path), '/');
    }
    
    /**
     * 过滤PHP标签
     * @param string $string
     * @return string
     */
    public static function filter_phptag($string) {
        return str_replace(array('<?', '?>'), array('&lt;?', '?&gt;'), $string);
    }
    
    /**
     * 安全过滤类-返回函数
     * @param  string $value 需要过滤的值
     * @return string
     */
    public static function str_out($value) {
        $badstr = array("&", '"', "'", "<", ">", "%3C", "%3E");
        $newstr = array("&amp;", "&quot;", "&#039;", "&lt;", "&gt;", "&lt;", "&gt;");
        $value  = str_replace($newstr, $badstr, $value);
        return stripslashes($value); //下划线
    }
    
}

