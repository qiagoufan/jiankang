<?php

class FUR_Run
{

    public function run()
    {
        $this->filter();
        // 验证方法是否合法，如果请求参数不正确，则直接返回404
        $controllerObj = $this->check_request();
        $this->run_action($controllerObj); // 正常流程Action
    }


    private function check_request()
    {
        $controller = FUR_Dispatcher::$controller;
        $class = FUR_Dispatcher::$class;
        if ($controller == "" || $class == "") {
            FUR_Core::return404();
        }

        $controller = basename($controller);
        $class = ucfirst(basename($class));

        $controller = $controller . DIRECTORY_SEPARATOR;
        $path = CONTROLLER_PATH;

        $controllerFilePath = $path . $controller . $class . '.php';
        if (!file_exists($controllerFilePath)) {
            FUR_Core::return404();
        }
        include($controllerFilePath);
        $obj = new $class ();

        return $obj;
    }

    /**
     * 框架运行控制器中的Action函数
     * 1.
     * 获取Action中的a参数
     * 2. 检测是否在白名单中，不在则选择默认的
     * 3. 检测方法是否存在，不存在则运行默认的
     * 4. 运行函数
     *
     * @param object $controller
     *            控制器对象
     * @return file
     */
    private function run_action($controller)
    {
        $action = FUR_Dispatcher::$action;
        if ($action == "") {
            $action = 'index';
        }
        if (!method_exists($controller, $action) && !method_exists($controller, "__call")) {
            FUR_Core::return404();
        }
        $controller->$action ();
    }

    /**
     * m-c-a数据处理
     *
     * @return string
     */
    private function filter()
    {
        if (isset ($_GET ['m'])) {
            if (!$this->_filter($_GET ['m']))
                unset ($_GET ['m']);
        }
        if (isset ($_GET ['c'])) {
            if (!$this->_filter($_GET ['c']))
                unset ($_GET ['c']);
        }
        if (isset ($_GET ['a'])) {
            if (!$this->_filter($_GET ['a']))
                unset ($_GET ['a']);
        }
    }

    private function _filter($str)
    {
        return preg_match('/^[A-Za-z0-9_]+$/', trim($str));
    }
}