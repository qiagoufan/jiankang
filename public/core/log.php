<?php

class FUR_Log
{
    const FATAL = 0;
    const ERROR = 1;
    const WARN = 2;
    const INFO = 3;
    const DEBUG = 4;
    const NOTICE = 5;
    private static $default_file_size = '102400000'; // 默认日志文件大小
    private static $setting; // 配置
    private static $filePath = NULL;
    private static $fileName = NULL;
    private static $fileMDATE = NULL;

    /**
     *
     * @name : getLogLevel
     * @return int
     *
     * @todo : 获取日志等级
     * @author : eifel.deng
     * @version 1.0
     */
    private static function getLogLevel()
    {
        $level = 0;

        if (is_null(self::$setting)) {
            self::$setting = \FUR_Config::get_log();

            if (!self::$setting) {
                self::$setting = array();
            }
        }

        if (isset (self::$setting ['log_level'])) {
            $level = intval(self::$setting ['log_level']);
        }

        if ($level > 5)
            $level = 5;

        if ($level < 0)
            $level = 0;

        return $level;
    }

    public static function get_default_log_name()
    {
        self::$setting = \FUR_Config::get_log();

        if (!self::$setting) {
            self::$setting = array();
        }

        self::$filePath = isset (self::$setting ['path']) ? self::$setting ['path'] : (APP_PATH . 'log');
        if (!is_dir(self::$filePath)) {
            if (mkdir(self::$filePath, 0755, TRUE)) {
                self::$filePath = realpath(self::$filePath);
            }
        }

        self::$fileName = 'php_service.log';
        return self::$filePath . DIRECTORY_SEPARATOR . self::$fileName;
    }


    public static function notice($name, $msg = "")
    {
        static::write($name, $msg, 'NOTICE');
    }

    public static function info($name, $msg = "")
    {
        static::write($name, $msg, 'INFO');
    }

    public static function biz($name, $msg = "", $biz_type)
    {
        static::write($name, $msg, 'INFO');
    }

    public static function debug($name, $msg = "")
    {
        static::write($name, $msg, 'DEBUG');
    }

    public static function warn($name, $msg = "")
    {
        static::write($name, $msg, 'WARN');

    }

    public static function error($name, $msg = "")
    {

        static::write($name, $msg, 'ERROR');

    }

    public static function fatal($name, $msg = "")
    {

        static::write($name, $msg, 'FATAL');

    }

    public static function write($name, $msg, $level = 'debug')
    {
        $className = get_called_class();
        $level_num = constant($className . '::' . strtoupper($level));
        if (!method_exists($className, $level)) {
            return;
        }
        if ($level_num > static::getLogLevel()) {
            return;
        }
        $server_ip = isset ($_SERVER ['SERVER_ADDR']) ? $_SERVER ['SERVER_ADDR'] : "";
        $client_ip = FUR_Helper::get_ip();
        $path_info = isset ($_SERVER ['PATH_INFO']) ? $_SERVER ['PATH_INFO'] : "";
        $msg = sprintf('[%s] %s %s [%s] : %s %s %s', $server_ip, date('Y-m-d H:i:s'), $client_ip, strtoupper($level), $path_info, print_r($name, true), print_r($msg, true));
        static::writeToFile($msg);
    }

    private
    static function writeToFile($msg)
    {
        $log_path = self::get_default_log_name();
        if (is_file($log_path) && self::$fileMDATE == null) {
            self::$fileMDATE = date("Ymd", filemtime($log_path));
            $now_date = date("Ymd");
            if (self::$default_file_size < filesize($log_path) || self::$fileMDATE != $now_date) {
                self::rename_old_log($log_path, self::$fileMDATE);
            }
        }
        $msg .= PHP_EOL;
        error_log($msg, 3, $log_path);
    }


    private
    static function rename_old_log($log_path, $file_m_date)
    {
        $index = 1;
        while (true) {
            $old_file_path = $log_path . '_' . $file_m_date . "_" . $index;
            if (!file_exists($old_file_path)) {
                rename($log_path, $old_file_path);
                break;
            }
            $index++;
        }
    }


}
