<?php

class FUR_Config
{
    static $_config;

    public static function load_system_config($config_file, $key = '')
    {
        if (!isset (self::$_config [$config_file])) {
            $config_path = CONF_PATH . DIRECTORY_SEPARATOR;
            $filename = $config_path . $config_file . '.php';
            self::$_config [$config_file] = include $filename;
        }
        if ($key != '') {
            return isset (self::$_config [$config_file] [$key]) ? self::$_config [$config_file] [$key] : '';
        }
        return self::$_config [$config_file];

    }

    /**
     * 获取config下面配置文件
     *
     * @param
     *            $key
     * @return mixed
     */
    public static function load_config($config_file, $key = '')
    {
        if (!isset (self::$_config [$config_file])) {
            $config_path = FUR_Core::get_config_path();
            $filename = $config_path . $config_file . '.php';
            self::$_config [$config_file] = include $filename;
        }
        if ($key != '') {
            return isset (self::$_config [$config_file] [$key]) ? self::$_config [$config_file] [$key] : '';
        } else {
            return self::$_config [$config_file];
        }
    }

    /**
     * 获取config文件夹下面的common.php
     *
     * @param
     *            $name
     * @return string
     */
    public static function get_common($name)
    {
        $config = self::load_config('common');
        return isset ($config [$name]) ? $config [$name] : null;
    }

    /**
     * 设置静态资源文件路劲
     */
    public static function set_res_path()
    {
        $res_path = self::get_common('res_path');
        if (!defined('RES_PATH'))
            define('RES_PATH', $res_path);
    }


    public static function get_aliyun_config($name)
    {
        $config = self::load_config('aliyun_config');
        return isset ($config [$name]) ? $config [$name] : '';
    }

    public static function get_static_config($name)
    {
        $config = self::load_config('static_config');
        return isset ($config [$name]) ? $config [$name] : '';
    }

    public static function get_redis($name = '')
    {
        $config = self::load_config('redis');
        return isset ($config [$name]) ? $config [$name] : $config;
    }

    public static function get_socialite_config($name = '')
    {
        $config = self::load_config('socialite_config');
        return isset ($config [$name]) ? $config [$name] : $config;
    }

    public static function get_log($name = '')
    {
        $config = self::load_config('log');
        return isset ($config [$name]) ? $config [$name] : $config;
    }

    public static function get_mysql_config($name = '')
    {
        $config = self::load_config('mysql');
        return isset ($config [$name]) ? $config [$name] : $config;
    }

    public static function get_es_config($name = '')
    {
        $config = self::load_config('es_config');
        return isset ($config [$name]) ? $config [$name] : $config;
    }

    public static function get_alipay_config($name = '')
    {
        $config = self::load_config('alipay_config');
        return isset($config[$name]) ? $config[$name] : $config;
    }

    public static function get_apple_config($name = '')
    {
        $config = self::load_config('apple_config');
        return isset($config[$name]) ? $config[$name] : $config;
    }

    public static function get_wx_mini_config($name = '')
    {
        $config = self::load_config('wx_mini_config');
        return isset($config[$name]) ? $config[$name] : $config;
    }

    public static function get_wechat_pay_config($name = '')
    {
        $config = self::load_config('wechat_pay_config');
        return isset($config[$name]) ? $config[$name] : $config;
    }

    public static function get_ota_config($name = '')
    {
        $config = self::load_config('ota_config');
        return isset($config[$name]) ? $config[$name] : $config;
    }

    public static function get_product_config($name = '')
    {
        $config = self::load_config('product_config');
        return isset($config[$name]) ? $config[$name] : $config;
    }

    public static function get_operation_config($name = '')
    {
        $config = self::load_config('operation_config');
        return isset($config[$name]) ? $config[$name] : $config;
    }


    public static function get_share_config($name = '')
    {
        $config = self::load_config('share_config');
        return isset($config[$name]) ? $config[$name] : $config;

    }

    public static function get_express_config($name = '')
    {
        $config = self::load_config('express_config');
        return isset($config[$name]) ? $config[$name] : $config;

    }

    public static function get_audio_config($name = '')
    {
        $config = self::load_config('audio_config');
        return isset($config[$name]) ? $config[$name] : $config;

    }

    public static function get_qiniu_config($name = '')
    {
        $config = self::load_config('qiniu_config');
        return isset($config[$name]) ? $config[$name] : $config;

    }

}
