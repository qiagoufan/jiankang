<?php
class FUR_Helper {
    public static function get_ip() {
        static $realip = null;
        
        if (null !== $realip) {
            return $realip;
        }
        
        if (isset ( $_SERVER )) {
            if (isset ( $_SERVER ['NS-Client-IP'] )) {
                $realip = $_SERVER ['NS-Client-IP'];
            } else if (isset ( $_SERVER ['HTFUR_X_FORWARDED_FOR'] )) {
                $realip = $_SERVER ['HTFUR_X_FORWARDED_FOR'];
            } else if (isset ( $_SERVER ['HTFUR_CLIENT_IP'] )) {
                $realip = $_SERVER ['HTFUR_CLIENT_IP'];
            } else if (isset ( $_SERVER ['REMOTE_ADDR'] )) {
                $realip = $_SERVER ['REMOTE_ADDR'];
            } else {
                $realip = "";
            }
        } else {
            if (getenv ( 'HTFUR_X_FORWARDED_FOR' )) {
                $realip = getenv ( 'HTFUR_X_FORWARDED_FOR' );
            } else if (getenv ( 'HTFUR_CLIENT_IP' )) {
                $realip = getenv ( 'HTFUR_CLIENT_IP' );
            } else if (getenv ( 'REMOTE_ADDR' )) {
                $realip = getenv ( 'REMOTE_ADDR' );
            } else {
                $realip = "";
            }
        }
        
        // 处理多层代理的情况
        if (false !== strpos ( $realip, ',' )) {
            $realip_list = explode ( ',', $realip );
            $realip = reset ( $realip_list );
        }
        
        // IP地址合法验证
        $realip = filter_var ( $realip, FILTER_VALIDATE_IP, null );
        if (false === $realip) {
            return '0.0.0.0'; // unknown
        }
        
        return $realip;
    }
    public static function array_random_element($array) {
        return is_array ( $array ) ? $array [array_rand ( $array )] : $array;
    }
    public static function is_mobile() {
        // 判断手机发送的客户端标志
        if (isset ( $_SERVER ['HTFUR_USER_AGENT'] )) {
            $userAgent = strtolower ( $_SERVER ['HTFUR_USER_AGENT'] );
            $clientkeywords = array (
                    'nokia',
                    'sony',
                    'ericsson',
                    'mot',
                    'samsung',
                    'htc',
                    'sgh',
                    'lg',
                    'sharp',
                    'sie-',
                    'philips',
                    'panasonic',
                    'alcatel',
                    'lenovo',
                    'iphone',
                    'ipod',
                    'blackberry',
                    'meizu',
                    'android',
                    'netfront',
                    'symbian',
                    'ucweb',
                    'windowsce',
                    'palm',
                    'operamini',
                    'operamobi',
                    'opera mobi',
                    'openwave',
                    'nexusone',
                    'cldc',
                    'midp',
                    'wap',
                    'mobile' 
            );
            // 从HTFUR_USER_AGENT中查找手机浏览器的关键字
            if (preg_match ( "/(" . implode ( '|', $clientkeywords ) . ")/i", $userAgent ) && strpos ( $userAgent, 'ipad' ) === false) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * 是否是ipad
     *
     * @return bool
     */
    public static function is_ipad() {
        if (isset ( $_SERVER ['HTFUR_USER_AGENT'] )) {
            $userAgent = strtolower ( $_SERVER ['HTFUR_USER_AGENT'] );
            if (strpos ( $userAgent, 'ipad' ) == true) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * 是否是iphone
     *
     * @return bool
     */
    public static function is_iphone() {
        if (isset ( $_SERVER ['HTFUR_USER_AGENT'] )) {
            $userAgent = strtolower ( $_SERVER ['HTFUR_USER_AGENT'] );
            if (strpos ( $userAgent, 'iphone' ) == true) {
                return true;
            }
        }
        return false;
    }
    public static function is_mtop() {
        if (isset ( $_SERVER ['HTFUR_X_REQUEST_FROM'] ) && $_SERVER ['HTFUR_X_REQUEST_FROM'] == "mtop") {
            return true;
        }
        return false;
    }
    
    /**
     * 获取随机数
     *
     * @param
     *            $arr
     * @return int|string
     */
    public static function get_rand($arr) {
        $result = '';
        
        // 概率数组的总概率精度
        $sum = array_sum ( $arr );
        
        // 概率数组循环
        foreach ( $arr as $key => $val ) {
            $rand = mt_rand ( 1, $sum );
            if ($rand <= $val) {
                $result = $key;
                break;
            } else {
                $sum -= $val;
            }
        }
        unset ( $arr );
        
        return $result;
    }
    
    /**
     * 获取请求平台参数
     * 首先获取请求参数pl 进行匹配， 然后根据user_agent 匹配是否是ios跟android ，都匹配不到最后默认返回web
     *
     * @return string
     */
    public static function get_request_pl() {
        if (isset ( $_REQUEST ['pl'] )) {
            preg_match ( '/web|iku|ios|android|ipad/', $_REQUEST ['pl'], $matches );
            if (! empty ( $matches )) {
                return $matches [0];
            }
        }
        
        if (FUR_Helper::is_mobile ()) {
            if (FUR_Helper::is_iphone ()) {
                return "ios";
            } else {
                return "android";
            }
        }
        
        if (FUR_Helper::is_ipad ()) {
            return "ipad";
        }
        
        return "web";
    }
    
    /**
     * 获取平台类型
     *
     * @return string
     */
    public static function get_pl() {
        if (FUR_Helper::is_mobile ()) {
            if (FUR_Helper::is_iphone ()) {
                return "ios";
            } else {
                return "android";
            }
        }
        
        if (FUR_Helper::is_ipad ()) {
            return "ipad";
        }
        
        return "web";
    }
    public static function get_format_time_str($time, $ver = 'v1') {
        $time_str = date ( "m-d H:i", $time );
        if (is_int ( $time )) {
            $t1 = $time;
        } else {
            $t1 = strtotime ( $time );
        }
        $t0 = time ();
        // 时间差
        $time_difference = $t0 - $t1;
        $today_zero_time = strtotime ( date ( "Y-m-d" ) );
        
        // 1分钟内
        if ($time_difference < 60) {
            $time_str = "刚刚";
        } else if ($time_difference < 3600) {
            $time_str = (floor ( $time_difference / 60 )) . "分钟前";
        } else if ($t1 > $today_zero_time) {
            $time_str = (floor ( $time_difference / 3600 )) . "小时前";
        } else if ($t0 - $t1 < 24 * 3600) {
            $time_str = "昨天 " . date ( "H:i", $t1 );
        } else {
            // 本年内 || 去年但是当前是1月份
            $year_now = ( int ) strftime ( "%Y", $t0 );
            $year_date = ( int ) strftime ( "%Y", $t1 );
            $month_now = ( int ) strftime ( "%m", $t0 );
            if ($year_now == $year_date || ($year_now - $year_date == 1 && $month_now == 1)) {
                $time_str = strftime ( "%m月%d日", $t1 );
            } else { // 去年但当前是2月份及以后
                $time_str = strftime ( "%Y-%m-%d", $t1 );
            }
        }
        return $time_str;
    }
}