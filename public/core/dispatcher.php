<?php
class FUR_Dispatcher {
    public static $controller = "main";
    public static $class = "index";
    public static $action = "main";
    
    /**
     * 路由分发-路由分发核心函数
     * 1.
     * 判断是否开启分发
     * 2. 获取request信息
     * 3. 解析URI
     */
    public function dispatcher() {
        $request = $this->get_request ();
        $this->parse_rewrite_uri ( $request );
        return true;
    }
    
    /**
     * 路由分发，获取Uri数据参数
     * 1.
     * 对Service变量中的uri进行过滤
     * 2. 配合全局站点url处理request
     *
     * @return string
     */
    private function get_request() {
        $filter_param = array (
                '<',
                '>',
                '"',
                "'",
                '%3C',
                '%3E',
                '%22',
                '%27',
                '%3c',
                '%3e' 
        );
        $uri = str_replace ( $filter_param, '', $_SERVER ['REQUEST_URI'] );
        $posi = strpos ( $uri, '?' );
        if ($posi)
            $uri = substr ( $uri, 0, $posi );
        if (strpos ( $uri, '.php' )) {
            $uri = explode ( '.php', $uri );
            $uri = $uri [1];
        }
        return $uri;
    }
    
    /**
     * 解析rewrite方式的路由
     * 1.
     * 解析index.php/user/new/username/?id=100
     * 2. 解析成数组，array()
     *
     * @param string $request            
     */
    private function parse_rewrite_uri($request) {
        if (! $request) {
            return false;
        }
        $request = trim ( $request, '/' );
        if ($request == '') {
            return false;
        }
        $request = explode ( '/', $request );
        if (! is_array ( $request ) || count ( $request ) == 0) {
            return false;
        }
        if (isset ( $request [0] )) {
            self::$controller = $request [0];
            $_GET ['m'] = $request [0];
        }
        if (isset ( $request [1] )) {
            self::$class = $request [1];
            $_GET ['c'] = $request [1];
        }
        if (isset ( $request [2] )) {
            self::$action = $request [2];
            $_GET ['a'] = $request [2];
        }
        if (! isset ( $_SERVER ['PATH_INFO'] ) || $_SERVER ['PATH_INFO'] == "") {
            $_SERVER ['PATH_INFO'] = "/" . self::$controller . "/" . self::$class . "/" . self::$action;
        }
        return $request;
    }
}