<?php

namespace db;

class MysqlShard
{
    private $pdo = null;
    private $servers = null;

    // PDO默认参数
    private static $pdo_default_opt = array(
        // \PDO::ATTR_EMULATE_PREPARES => false,
        \PDO::MYSQL_ATTR_INIT_COMMAND => 'set names utf8mb4',
        \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
        \PDO::ATTR_PERSISTENT => false
    );
    private $result;
    private $rows;

    public function __construct($config)
    {
        $this->servers = $config;
    }

    protected function connect()
    {
        if (!isset ($this->servers ['db']) && isset ($this->servers ['database'])) {
            $this->servers ['db'] = $this->servers ['database'];
            unset ($this->servers ['database']);
        }

        if (!isset ($this->servers ['host'], $this->servers ['port'], $this->servers ['db'], $this->servers ['user'], $this->servers ['password'])) {
            \FUR_Log::fatal('MySQL', 'MySQL configure error');

            throw new \Exception ('MySQL configure error', ErrorCode::MYSQL_SETTING);
        }

        $dsn = 'mysql:host=' . $this->servers ['host'] . ';port=' . $this->servers ['port'] . ';dbname=' . $this->servers ['db'];

        try {
            $this->pdo = new \PDO ($dsn, $this->servers ['user'], $this->servers ['password'], self::$pdo_default_opt);
        } catch (\Exception $e) {
            \FUR_Log::fatal('MYSQL', $e->getMessage());
            throw new \Exception ($e->getMessage(), ErrorCode::MYSQL_TIMEOUT);
        }
    }

    protected function select_db($shard_id)
    {
        if (empty ($shard_id)) {
            return false;
        }
        $servers = $this->servers;
        $db_pos = 0;
        $sharded = false;
        $db_found = FALSE;

        if ($this->servers ['rule'] [0] > 1) {
            $db_pos = $this->hash_db($shard_id);
            $sharded = true;
        }

        if (isset ($servers [$db_pos])) {
            $this->servers ['host'] = $servers [$db_pos] ['host'];
            $this->servers ['port'] = $servers [$db_pos] ['port'];
            $this->servers ['user'] = $servers [$db_pos] ['user'];
            $this->servers ['password'] = $servers [$db_pos] ['password'];

            if (isset ($servers [$db_pos] ['database'])) {
                $this->servers ['db'] = $servers [$db_pos] ['database'];
                $this->servers ['database'] = $this->servers ['db'];
                return;
            }
            $db_found = TRUE;
        } else {
            foreach ($servers as $key => $value) {
                if (strpos($key, '-')) {
                    list ($s, $e) = explode('-', $key);
                    if ($db_pos >= ( int )$s && $db_pos <= ( int )$e) {
                        $this->servers ['host'] = $value ['host'];
                        $this->servers ['port'] = $value ['port'];
                        $this->servers ['user'] = $value ['user'];
                        $this->servers ['password'] = $value ['password'];

                        if (isset ($value ['database'])) {
                            $this->servers ['db'] = $value ['database'];
                            $this->servers ['database'] = $value ['database'];
                            return;
                        }

                        $db_found = TRUE;
                        break;
                    }
                }
            }
        }

        $dbname = $this->servers ['database'] . ($sharded ? ('_' . $db_pos) : '');
        if (!$db_found || empty ($dbname)) {
            \FUR_Log::fatal('MySQL', 'MySQL configure error');
            throw new \Exception ('MySQL configure error', ErrorCode::MYSQL_SETTING);
        }
        $this->servers ['db'] = $dbname;
    }

    protected function hash_db($shard_id)
    {
        $h = sprintf("%u", crc32($shard_id));
        $s = $this->servers ['rule'] [0];
        $h = intval(fmod($h, $s));
        return $h;
    }

    protected function hash_tbl($shard_id)
    {
        $h = sprintf("%u", crc32($shard_id));
        $s = $this->servers ['rule'] [0];
        $h = floor($h / $s);
        $s = $this->servers ['rule'] [1];
        $h = intval(fmod($h, $s));
        return $h;
    }

    public function getTable($shard_id, $table_name)
    {
        $tb_pos = 0;
        $sharded = false;
        if (isset ($this->servers ['rule'] [1]) && $this->servers ['rule'] [1] > 1) {
            $tb_pos = $this->hash_tbl($shard_id);
            $sharded = true;
        }
        $tb_name = trim($table_name) . ($sharded ? ('_' . $tb_pos) : '');
        return $tb_name;
    }


    protected function executeQuery($sql, array $bind = NULL, $clazz = null)
    {
        $query = $this->buildQuery($sql, $bind);
        if ($clazz != null) {
            $this->result = $query->fetchAll(\PDO::FETCH_CLASS, $clazz);
        } else {
            $this->result = $query->fetchAll(\PDO::FETCH_OBJ);
        }
        $this->rows = $query->rowcount();
        return $this->result;
    }

    protected function execute($sql, array $bind = NULL)
    {
        $query = $this->buildQuery($sql, $bind);
        if (preg_match('/^\s*(?:SELECT|PRAGMA|SHOW|EXPLAIN)\s/i', $sql)) {
            $this->result = $query->fetchAll(\PDO::FETCH_ASSOC);
            $this->rows = $query->rowcount();
        } else {
            $this->rows = $this->result = $query->rowCount();
        }
        return $this->result;
    }

    private function buildQuery(&$sql, array &$bind = NULL): \PDOStatement
    {
        if (empty ($sql)) {
            return false;
        }
        $this->connect();
        $query = null;
        try {
            if (is_null($bind)) {
                $query = $this->pdo->query($sql);
            } else {
                $query = $this->pdo->prepare($sql);
                if (!is_object($query)) {
                    throw new \Exception ('DB error', ErrorCode::MYSQL_EXECUTE);
                }

                foreach ($bind as $key => $value) {
                    $query->bindValue(
                        is_string($key) ? $key : $key + 1, $value,
                        is_int($value) ? \PDO::PARAM_INT : \PDO::PARAM_STR
                    );
                }

                if (!$query->execute()) {
                    throw new \Exception ('DB error', ErrorCode::MYSQL_EXECUTE);
                }
            }
            if (!$query) {
                throw new \Exception ('Db error', ErrorCode::MYSQL_EXECUTE);
            }
        } catch (\Exception $e) {
            \FUR_Log::error('Db error', $e->getMessage());
            \FUR_Log::error('Db error', $e->getTraceAsString());
            \FUR_Log::error('Db error', $e->getFile());
            \FUR_Log::error('Db error', $e->getLine());
            throw $e;
        }

        return $query;
    }

    /**
     * Return auto-detected PDO data type of specified value
     *
     * @param $val mixed
     * @public
     *
     * @return int
     */
    protected function type($val)
    {
        foreach (array(
                     'null' => 'NULL',
                     'bool' => 'BOOL',
                     'string' => 'STR',
                     'int' => 'INT',
                     'float' => 'STR'
                 ) as $php => $pdo)
            if (call_user_func('is_' . $php, $val))
                return constant('\PDO::PARAM_' . $pdo);
        return \PDO::PARAM_LOB;
    }

    public function select($sql, array $args = NULL, $clazz = null, $shard_id = "")
    {
        if (empty ($sql)) {
            return false;
        }

        if ($shard_id) {
            $this->select_db($shard_id);
        }
        return $this->executeQuery($sql, $args, $clazz);
    }

    public function query($sql, array $args = NULL, $shard_id = "")
    {
        if (empty ($sql)) {
            return false;
        }

        if ($shard_id) {
            $this->select_db($shard_id);
        }
        return $this->execute($sql, $args);
    }

    public function delete($table, array $args = NULL, $shard_id = "")
    {
        if (empty ($table)) {
            return false;
        }
        if (empty ($args)) {
            return false;
        }
        if ($shard_id) {
            $this->select_db($shard_id);
        }


        $bind = array();
        $cond = '';
        foreach ($args as $pkey => $val) {
            $cond .= ($cond ? ' AND ' : '') . $pkey . '=:c_' . $pkey;
            $bind [':c_' . $pkey] = $val;
        }
        $sql = 'DELETE FROM ' . $table . ' WHERE ' . $cond . ';';
        return $this->execute($sql, $bind);
    }

    public function selectOne($sql, array $args = NULL, $clazz = null, $shard_id = "")
    {
        if (empty ($sql)) {
            return false;
        }

        if ($shard_id) {
            $this->select_db($shard_id);
        }
        $recordList = $this->executeQuery($sql, $args, $clazz);
        if ($recordList == null) {
            return null;
        }
        return $recordList[0];
    }

    /**
     *
     *
     * @author : eifel.deng
     * @todo : 插入一条数据
     * @version 1.0
     */
    public function insert($table, $data, $share_id = ""): int
    {
        if (!is_array($data)) {
            $data = (array)($data);
        }

        $last_insert_id = 0;
        if (empty ($table) || empty ($data)) {
            return $last_insert_id;
        }
        $tb_name = $this->getTable($share_id, $table);
        // Insert record
        $fields = $values = '';
        $bind = array();
        foreach ($data as $field => $val) {
            $fields .= ($fields ? ',' : '') . ('`' . $field . '`');
            $values .= ($values ? ',' : '') . ':' . $field;
            $bind [':' . $field] = $val;
        }
        if (empty ($bind)) {
            return $last_insert_id;
        }
        $sql = 'INSERT INTO ' . $table . ' (' . $fields . ') ' . 'VALUES (' . $values . ');';
        $return = $this->execute($sql, $bind);
        $last_insert_id = $this->pdo->lastinsertid();

        if ($last_insert_id) {
            return $last_insert_id;
        }
        return $return;
    }

    public function update($table_name, $data, array $args = NULL, $shard_id = NULL)
    {

        if (empty ($table_name) || empty ($data)) {
            return false;
        }
        if (!is_array($data)) {
            $data = (array)($data);
        }

        /**
         * 做一个id不可变更的约束
         */
        if (isset($data['id'])) {
            unset($data['id']);
        }

        $tb_name = $this->getTable($shard_id, $table_name);
        $set = $cond = '';
        $bind = array();
        foreach ($data as $field => $val) {
            if ($val === null) {
                continue;
            }
            $set .= ($set ? ',' : '') . ('`' . $field . '`') . '=:' . $field;
            $bind [':' . $field] = $val;
        }
        foreach ($args as $pkey => $val) {
            $cond .= ($cond ? ' AND ' : '') . $pkey . '=:c_' . $pkey;
            $bind [':c_' . $pkey] = $val;
        }

        if (empty ($set)) {
            return false;
        }
        $sql = 'UPDATE ' . $tb_name . ' SET ' . $set . ($cond ? (' WHERE ' . $cond) : '') . ';';
        return $this->execute($sql, $bind);
    }

    public function rows()
    {
        return $this->rows;
    }


    public function __destruct()
    {
        unset ($this->pdo);
    }
}

