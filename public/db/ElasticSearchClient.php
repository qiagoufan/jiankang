<?php

namespace db;


use Elasticsearch\ClientBuilder;
use Exception;

class ElasticSearchClient
{

    private static $configs = null;

    public static function insert($index, $type, $id, $body, $module = 'default')
    {

        $client = self::build_client($module);
        if ($client == null) {
            return false;
        }


        $params = [
            'index' => $index,
            'type' => $type,
            'id' => $id,
            'body' => $body
        ];
        try {
            $response = $client->index($params);
            return ($response['_shards']['successful'] > 0);
        } catch (Exception $e) {
            \FUR_Log::warn("es fail",$e->getTraceAsString());
            return false;
        }
        return false;

    }


    public static function update($index, $type, $id, $body, $module = 'default')
    {

        $client = self::build_client($module);
        if ($client == null) {
            return false;
        }


        $params = [
            'index' => $index,
            'type' => $type,
            'id' => $id,
            'body' => $body
        ];
        try {
            $response = $client->update($params);
            return ($response['_shards']['failed'] == 0);
        } catch (Exception $e) {
            \FUR_Log::warn("es fail",$e->getTraceAsString());
            return false;
        }
        return false;

    }

    public static function delete($index, $type, $id, $module = 'default')
    {

        $client = self::build_client($module);
        if ($client == null) {
            return false;
        }
        $params = [
            'index' => $index,
            'type' => $type,
            'id' => $id];
        try {
            $response = $client->delete($params);
            return ($response['_shards']['successful'] > 0);
        } catch (Exception $e) {
            \FUR_Log::warn("es fail",$e->getTraceAsString());
            return false;
        }

        return false;

    }

    public static function search($index, $type, $body, $from = 0, $size = 10, $module = 'default')
    {

        $client = self::build_client($module);
        if ($client == null) {
            return false;
        }


        $params = [
            'index' => $index,
            'type' => $type,
            'size' => $size,
//            'from' => $from,
            'body' => $body];
        try {
            $response = $client->search($params);
            return ($response['hits']['hits']);
        } catch (Exception $e) {

            \FUR_Log::warn("es fail",$e->getTraceAsString());
            return false;
        }
        return false;

    }

    public static function get($index, $type, $id, $module = 'default')
    {
        $client = self::build_client($module);
        if ($client == null) {
            return false;
        }
        $params = [
            'index' => $index,
            'type' => $type,
            'id' => $id
        ];
        try {
            $response = $client->get($params);
            return $response;
        } catch (Exception $e) {
            \FUR_Log::warn("es fail",$e->getTraceAsString());
            return false;
        }
        return false;

    }


    private
    static function build_client($module)
    {
        $config = self::get_config($module);
        if ($config == null) {
            return null;
        }
        $hosts = $config['hosts'];
        return ClientBuilder::create()->setHosts($hosts)->build();

    }


    private
    static function get_config($module)
    {
        if (!isset(self::$configs[$module])) {
            self::$configs[$module] = \FUR_Config::get_es_config($module);
        }
        return self::$configs[$module];
    }
}

