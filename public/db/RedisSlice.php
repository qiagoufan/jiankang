<?php

namespace db;
include_once PUBLIC_PATH.'mapi.php';
include_once PUBLIC_PATH.'timetick.php';
class RedisSlice extends RedisAbstract {
	protected $nodes = array ();
	const SLICE = 100;
	public function __construct(array $servers, $mode = 'master') {
		if ('slave' != $mode)
			$mode = 'master';
		
		foreach ( $servers as $server ) {
			
			if (! isset ( $server ['bid'] ) || ! isset ( $server ['eid'] ))
				continue;
			
			$serverId = trim ( $server ['bid'] );
			unset ( $server ['range'] );
			
			if ('slave' == $mode && is_array ( $server ['slave'] ) && 0 < count ( $server ['slave'] )) {
				$_ndx = array_rand ( $server ['slave'] );
				$slave = $server ['slave'] [$_ndx];
				
				if (! isset ( $slave ['host'], $slave ['port'] ))
					continue;
				
				$slave ['port'] = intval ( $slave ['port'] );
				$slave ['disabled'] = isset ( $slave ['disabled'] ) ? intval ( $slave ['disabled'] ) : 0;
				$this->nodes [$serverId] = $slave;
			} else {
				unset ( $server ['slave'] );
				if (! isset ( $server ['ip'], $server ['port'] ))
					continue;
				$server ['port'] = intval ( $server ['port'] );
				$server ['disabled'] = isset ( $server ['disabled'] ) ? intval ( $server ['disabled'] ) : 0;
				$this->nodes [$serverId] = $server;
			}
		}
		
		if (empty ( $this->nodes )) {
			\FUR_Log::error ( 'Redis', array (
					'args' => $servers,
					'mode' => $mode,
					'method' => 'RedisSlice',
					'class' => 'RedisSlice',
					'error' => 'Configure error' 
			) );
			throw new \Exception ( 'Redis Configure empty', \db\ErrorCode::REDIS_SETTING );
		}
	}
	protected function proxy($func, array $args) {
		if (1 > count ( $args )) {
			\FUR_Log::fatal ( 'Redis', array (
					'class' => 'RedisSlice',
					'method' => 'proxy',
					'func' => $func,
					'args' => $args,
					'error' => 'miss key' 
			) );
			
			throw new \Exception ( 'Miss paramter slice key', \db\ErrorCode::REDIS_SLICE_KEY_MISS );
		}
		$redis = $this->selectRedis ( $args [0] );
		
		$murl = "redis_" . $func;
		$redisinstance = "";
		$node = $this->getNode($args [0]);
		if (isset ( $this->nodes [$node] )) {
			$setting = $this->nodes [$node];
			if (isset ($setting['ip'], $setting['port'])){
				$redisinstance = $setting['ip'].":".$setting['port'];
			}
		}
		try{
			$result = call_user_func_array ( array (
					$redis,
					$func 
			), $args );
			return $result;
		}catch(\Exception $e){
			throw $e;
		}
	}
	
	public function getNode($key) {
		$keyparts = explode ( ':', $key );
		$crcKey = array_pop ( $keyparts );
// 		if (isset($keyparts [1] )&&( $keyparts [1] == "user_mainpage_point" || $keyparts [1] == "user_point")) {
// 			$nodeIndex = ((int)$crcKey) % self::SLICE;
// 		} else {
//  			$nodeIndex = abs ( crc32 ( $crcKey ) ) % self::SLICE;
// 		}
		$nodeIndex = ((int)$crcKey) % self::SLICE;
		unset ( $keyparts );
		$nodes = $this->nodes;
		foreach ( $nodes as $node ) {
			$begin = $node ['bid'];
			$end = $node ['eid'];
			$node_id = $begin;
			if ($nodeIndex >= $begin && $nodeIndex <= $end) {
				return $node_id;
			}
		}
		
		\FUR_Log::error ( 'Redis', array (
				'class' => 'RedisSlice',
				'method' => '',
				'key' => $key,
				'crcKey' => $crcKey,
				'nodeIndex' => $nodeIndex 
		) );
		
		throw new \Exception ( 'Redis node not exist', \db\ErrorCode::REDIS_NODE_NOT_EXIST );
	}
	public function selectNode($node) {
		if (! isset ( $this->nodes [$node] )) {
			\FUR_Log::error ( 'Redis', array (
					'class' => 'RedisSlice',
					'method' => 'selectNode',
					'node' => $node 
			) );
			throw new \Exception ( 'Redis node not exist!', \db\ErrorCode::REDIS_NODE_NOT_EXIST );
		}
		return $this->connect ( $this->nodes [$node] );
	}
	public function selectRedis($key) {
		$node =$this->getNode($key);
		return $this->selectNode ($node );
	}
}

