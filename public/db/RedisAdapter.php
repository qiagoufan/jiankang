<?php

namespace db;

class RedisAdapter {
	private static $tables = NUll;
	private static $pools = array ();
	private static function getConfig($module) {
		if (is_null ( self::$tables )) {
			self::$tables = \FUR_Config::get_redis ();
		}
		if (! self::$tables || ! isset ( self::$tables [$module] )) {
			\FUR_Log::fatal ( 'Redis', array (
					'class' => 'RedisAdapter',
					'method' => 'getConfig',
					'module' => $module 
			) );
			
			throw new \Exception ( 'Redis configure miss' );
		}
		
		return self::$tables [$module];
	}
	public static function singleRedis($module, $mode = 'master') {
		if ('slave' != $mode)
			$mode = 'master';
		
		$poolIndex = strtolower ( sprintf ( '%s_%s', $module, $mode ) );
		
		if (! isset ( self::$pools [$poolIndex] )) {
			$server = self::getConfig ( $module );
			$redis = new RedisSlice ( $server, $mode );
			
			self::$pools [$poolIndex] = $redis;
		}
		
		return self::$pools [$poolIndex];
	}
	public static function shareRedis($module, $mode = 'master') {
		if ('slave' != $mode) {
			$mode = 'master';
		}
		$poolIndex = strtolower ( sprintf ( '%s_%s', $module, $mode ) );
		
		if (! isset ( self::$pools [$poolIndex] )) {
			
			$server = self::getConfig ( $module );
			
			$redis = new RedisSlice ( $server, $mode );
			self::$pools [$poolIndex] = $redis;
		}
		
		return self::$pools [$poolIndex];
	}
}

