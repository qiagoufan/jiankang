<?php

namespace db;


class MysqlAdapter {
	private static $tables = null;
	private static $pools = array ();
	public static function singleMySQL($module) {
		$config = self::config ( $module );
		if (! isset ( $config ['host'], $config ['port'], $config ['database'], $config ['user'], $config ['password'] )) {
			\FUR_Log::fatal ( 'MySQL', array (
					'class' => 'MysqlAdapter',
					'method' => 'singleMySQL',
					'module' => $module,
					'error' => 'MySQL configure error' 
			) );
			throw new \Exception ( 'MySQL configure error', \db\ErrorCode::MYSQL_SETTING );
		}
		
		$config ['db'] = $config ['database'];
		unset ( $config ['database'] );
		return new MysqlShard ( $config );
	}
	public static function shareMySQL($module) {
		$config = self::config ( $module );
		$setting = array ();
		
		if (is_array ( $config ))
			foreach ( $config as $key => $row ) {
				if (isset ( $row ['host'], $row ['port'], $row ['user'], $row ['password'] )) {
					$setting [$key] = $row;
				}
			}
		
		if (empty ( $setting ) || ! isset ( $config ['rule'] )) {
		    \FUR_Log::fatal ( 'MySQL', array (
					'class' => 'MysqlAdapter',
					'method' => 'shareMySQL',
					'module' => $module,
					'error' => 'MySQL configure error' 
			) );
			throw new \Exception ( 'MySQL configure error', \db\ErrorCode::MYSQL_SETTING );
		}
		
		$setting ['rule'] = $config ['rule'];
		if (isset ( $config ['database'] ))
			$setting ['database'] = $config ['database'];
		return new MysqlShard ( $setting );
	}
	private static function config($module) {
		if (is_null ( self::$tables )) {
			self::$tables = \FUR_Config::get_mysql_config ( 'mysql' );
		}
		
		if (! self::$tables || ! isset ( self::$tables [$module] )) {
		    \FUR_Log::fatal ( 'MySQL', array (
					'class' => 'MysqlAdapter',
					'method' => 'config',
					'module' => $module 
			) );
			throw new \Exception ( 'MySQL configure miss', \db\ErrorCode::MYSQL_SETTING );
		}
		return self::$tables [$module];
	}
}

