<?php

namespace db;

class NewRedisSlice
{
    private static $_redis;
    private static $_redis_config;
    private static $_pools;
    public $name = "";
    const REDIS_TIMEOUT = 0.1;
    const REDIS_PERSISTENT = true;
    const REDIS_RETRY_INTERVAL = 2;

    public static function get_instance($name)
    {
        if (!isset (self::$_pools [$name])) {
            $redis_slice = new self ();
            $redis_slice->name = $name;
            self::$_pools [$name] = $redis_slice;
        }
        return self::$_pools [$name];
    }

    public function select()
    {
        $name = $this->name;
        if (!isset (self::$_redis_config [$name])) {
            self::$_redis_config [$name] = \FUR_Config::get_redis($name);
        }
        if (self::$_redis_config [$name] == null) {
            throw new \Exception ("Redis  {$name} setting not found ", \db\ErrorCode::REDIS_SETTING);
        }
        return self::$_redis_config [$name];
    }

    public function connect($splice)
    {
        $name = $this->name;
        $setting = array();
        $connect_key = "";
        $nodes = $this->select($name);
        // 获取分片配置
        foreach ($nodes as $node) {
            $begin = $node ['bid'];
            $end = $node ['eid'];
            $node_id = $begin;
            if ($splice >= $begin && $splice <= $end) {
                $setting = $node;
                break;
            }
        }

        if (!isset ($setting ['ip'], $setting ['port'])) {
            throw new \Exception ("Redis {$name} setting error", \db\ErrorCode::REDIS_SETTING);
        }

        $host = trim($setting ['ip']);
        $port = intval($setting ['port']);
//        \FUR_Log::debug('REDIS', array(
//            'host' => $host,
//            'port' => $port
//        ));


        $connect_key = sprintf("%s_%s", $host, $port);
        // 是否已经有连接
        if (!isset (self::$_redis [$connect_key])) {
            $timeout = self::REDIS_TIMEOUT;
            if (isset ($setting ['timeout'])) {
                $timeout = intval($setting ['timeout']);
                if ($timeout <= 0) {
                    $timeout = self::REDIS_TIMEOUT;
                }
            }
            $redis = new \Redis ();
            $connection = FALSE;

            if (self::REDIS_PERSISTENT) {
                $connection = $redis->pconnect($host, $port, $timeout, $connect_key, self::REDIS_RETRY_INTERVAL);
            } else {
                $connection = $redis->connect($host, $port, $timeout, NULL, self::REDIS_RETRY_INTERVAL);
            }
            if (!$connection) {
                \FUR_Log::error('REDIS', "Connect to redis {$connect_key} timeout");
                throw new \Exception ('Connect to redis timeout', \db\ErrorCode::REDIS_TIMEOUT);
            }

            if (isset ($setting ['password']) && ($passwd = $setting ['password']) && !$redis->auth($passwd)) {
                \FUR_Log::error('REDIS', "Redis Auth {$connect_key} fail");
                throw new \Exception ('Redis auth fail', \db\ErrorCode::REDIS_AUTH);
            }
            self::$_redis [$connect_key] = $redis;
        }
        return self::$_redis [$connect_key];
    }

    public function __call($func, array $args)
    {
        $result = $this->proxy($func, $args);
        return $result;
    }

    // 获取分片
    public function get_slice($key, $type = "crc32", $slice_num = 100)
    {
        $slice = 0;
        $explode_list = explode(":", $key);
        $slice_part = array_pop($explode_list);
        if ($type == "crc32") {
            $slice_part = crc32($slice_part);
            $slice = $slice_part % $slice_num;
        } else {
            $slice = $slice_part % $slice_num;
        }
        return $slice;
    }

    private function proxy($func, array $args)
    {
        $slice = $this->get_slice($args[0]);
        $redis = $this->connect($slice);

        try {
            return call_user_func_array(array($redis, $func), $args);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function __destruct()
    {
        if (self::$_redis == null) {
            return;
        }
        foreach (self::$_redis as $v) {
            $v->close();
        }
    }
}

