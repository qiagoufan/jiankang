<?php

namespace db;

include_once PUBLIC_PATH . 'mapi.php';
include_once PUBLIC_PATH . 'timetick.php';
abstract class RedisAbstract {
    protected static $connects = array ();
    const REDIS_TIMEOUT = 0.1;
    const REDIS_RETRY_INTERVAL = 10;
    const REDIS_PERSISTENT = true;
    abstract protected function proxy($func, array $args);
    protected function connect($setting) {
        if (! isset ( $setting ['ip'], $setting ['port'] )) {
            throw new \Exception ( 'Redis setting error', \db\ErrorCode::REDIS_SETTING );
        }
        
        $host = trim ( $setting ['ip'] );
        $port = intval ( $setting ['port'] );

        if (isset ( $setting ['disabled'] ) && $setting ['disabled']) {
            \FUR_Log::error ( 'REDIS', 'Redis is down for maintenance!' );
            throw new \Exception ( 'Redis is down for maintenance!', \db\ErrorCode::REDIS_DISABLE );
        }
        
        $connectKey = sprintf ( "%s_%s", $host, $port );
        
        if (! isset ( self::$connects [$connectKey] )) {
            $timeout = self::REDIS_TIMEOUT;
            
            if (isset ( $setting ['timeout'] )) {
                $timeout = intval ( $setting ['timeout'] );
                
                if ($timeout < 0) {
                    $timeout = self::REDIS_TIMEOUT;
                }
            }
            $redis = new \Redis ();
            $connection = FALSE;
            
            if (self::REDIS_PERSISTENT) {
                $connection = $redis->pconnect ( $host, $port, $timeout, $connectKey, self::REDIS_RETRY_INTERVAL );
            } else {
                $connection = $redis->connect ( $host, $port, $timeout, NULL, self::REDIS_RETRY_INTERVAL );
            }
            
            $connectres = 0;
            if (! $connection) {
                $connectres = - 1;
            }
            
            if (! $connection) {
                \FUR_Log::error ( 'REDIS', 'Connect to redis timeout' );
                throw new \Exception ( 'Connect to redis timeout', \db\ErrorCode::REDIS_TIMEOUT );
            }
            
            if (isset ( $setting ['password'] ) && ($passwd = $setting ['password']) && ! $redis->auth ( $passwd )) {
                \FUR_Log::error ( 'REDIS', 'Redis Auth fail' );
                throw new \Exception ( 'Redis auth fail', \db\ErrorCode::REDIS_AUTH );
            }
            
            if ('cli' != php_sapi_name ()) {
                // 可解决CLI模式下redis超时，无法连接的问题
                self::$connects [$connectKey] = $redis;
            } else
                return $redis;
        }
        
        return self::$connects [$connectKey];
    }
    public function __call($func, array $args) {
        $result = $this->proxy ( $func, $args );
        return $result;
    }
}

