<?php
namespace db;

class ErrorCode {
    const REDIS_SETTING = 2001;
    const REDIS_DISABLE = 2002;
    const REDIS_TIMEOUT = 2003;
    const REDIS_AUTH = 2004;
    const REDIS_NODE_NOT_EXIST = 2005;
    const REDIS_SLICE_KEY_MISS = 2006;

    const MYSQL_TIMEOUT = 2010;
    const MYSQL_EXECUTE = 2011;
    const MYSQL_SETTING = 2012;
}

