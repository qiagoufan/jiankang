<?php

namespace lib;

class PhoneHelper
{

    const AES_KEY = '2c30604705fd91d2';

    public static function encodePhone(?int $phone): ?string
    {
        return "ph" . dechex($phone << 2);

    }

    public static function decodePhone(?string $phone, ?string $platform = null): ?int
    {
        return $phone;
        //h5和微信的的通过aes解密
        if ($platform == AppConstant::PLATFORM_H5 || $platform == AppConstant::PLATFORM_WX_MINI || $platform == AppConstant::PLATFORM_ADMIN || $platform == AppConstant::PLATFORM_WX_H5) {
            return openssl_decrypt($phone, 'AES-128-ECB', self::AES_KEY);
        }

        //安卓和ios
        if (substr($phone, 0, 2) != "ph") {
            return null;
        }

        $phoneStr = substr($phone, 2);
        $phone = hexdec($phoneStr);
        return $phone >> 2;
    }
}