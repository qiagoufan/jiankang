<?php


namespace lib;


class NumHelper
{

    public static function getPersonalNumString(int $number): string
    {
        $countStr = '';
        if ($number <= 10000) {
            $countStr = $number;
        } elseif ($number > 10000) {
            $countStr = number_format($number / 10000, 1) . '万';
        }
        return $countStr;
    }

}