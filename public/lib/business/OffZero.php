<?php

namespace lib\business;

use facade\response\Result;
use FUR_Config;
use model\dto\FeedInfoDTO;
use model\dto\ProductItemDTO;
use model\dto\ProductSkuPriceDTO;
use service\product\impl\ProductServiceImpl;
use service\user\impl\UserServiceImpl;

class OffZero
{


    public static $register_count = 0;

    public static function offZeroSwitch(): int
    {
        return FUR_Config::get_common('off_zero_switch');
    }

    public static function getRegisterUserCount(): int
    {
        if (self::$register_count == 0) {
            $registerUserCountRet = UserServiceImpl::getInstance()->getRegisterUserCount();
            self::$register_count = $registerUserCountRet->data;
        }
        return self::$register_count;
    }


    public static function fakePurchaseInfo(?ProductItemDTO $productItemDTO, int $skuId): ?string
    {
        $purchaseInfo = null;
        if (self::offZeroSwitch() == 0) {
            return $purchaseInfo;
        }
        $priceInfoRet = ProductServiceImpl::getInstance()->getProductPriceBySkuId($skuId);
        $price = 0;
        if (Result::isSuccess($priceInfoRet)) {
            /** @var ProductSkuPriceDTO $productSkuPriceDTO */
            $productSkuPriceDTO = $priceInfoRet->data;
            $price = $productSkuPriceDTO->current_price;
        }

        $price = $price / 100;

        $fakeSale = 50;
        $fakeDailySale = 0;
        $fakeStock = 0;
        if ($price > 5000) {
            $fakeSale = $productItemDTO->created_timestamp % 5;
            $fakeDailySale = 0.23;
            $fakeStock = $productItemDTO->created_timestamp % 3;
        } elseif ($price > 1000) {
            $fakeSale = $productItemDTO->created_timestamp % 50;
            $fakeDailySale = 1.7;
            $fakeStock = $productItemDTO->created_timestamp % 7;
        } elseif ($price > 100) {
            $fakeSale = $productItemDTO->created_timestamp % 150 + 50;
            $fakeDailySale = 3.4;
            $fakeStock = $productItemDTO->created_timestamp % 11;
        } else {
            $fakeSale = $productItemDTO->created_timestamp % 400 + 100;
            $fakeDailySale = 17;
            $fakeStock = $productItemDTO->created_timestamp % 13;
        }

        $sale = $productItemDTO->sale + $fakeSale;
        $days = round((time() - $productItemDTO->created_timestamp) / 86400);
        if ($days < 60) {
            $sale = $sale + intval($fakeDailySale * $days);
        }

        $stock = $productItemDTO->stock;
        if ($stock == 0) {
            $stock = $fakeStock;
        }
        $purchaseInfo = $sale . '人已付款,仅剩' . $stock . '件';

        return $purchaseInfo;
    }

    public static function fakeHeat(FeedInfoDTO &$feedInfoDTO): ?int
    {
        $userCount = self::getRegisterUserCount();
        $heat = 0;
//        $hours = round(time() - $feedInfoDTO->created_timestamp / 3600);
//        if ($hours > 168) {
//            $heat = $userCount * 0.1 + $feedInfoDTO->created_timestamp % 1763;
//        } elseif ($hours > 120) {
//            $heat = $userCount * 0.083 + $feedInfoDTO->created_timestamp % 1233;
//        } elseif ($hours > 72) {
//            $heat = $userCount * 0.061 + $feedInfoDTO->created_timestamp % 641;
//        } elseif ($hours > 24) {
//            $heat = $userCount * 0.047 + $feedInfoDTO->created_timestamp % 233;
//        } elseif ($hours > 6) {
//            $heat = $userCount * 0.015 + $feedInfoDTO->created_timestamp % 71;
//        } elseif ($hours > 3) {
//            $heat = $userCount * 0.009 + $feedInfoDTO->created_timestamp % 33;
//        } elseif ($hours > 1) {
//            $heat = $userCount * 0.001+$feedInfoDTO->created_timestamp % 7;
//        } else {
//            $heat = 0;
//        }

        return intval($heat);

    }


}