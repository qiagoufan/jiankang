<?php


namespace lib;


class PregHelper
{

    const URL_RULE = '/[A-Za-z0-9]{4,5}:\/\/[^\s]*.[\w]/';

    public static function httpLinkConversion(?string $content)
    {
        if ($content == null) {
            return null;
        }
        return preg_replace(self::URL_RULE, ' <a href="$0">网页链接</a> ', $content);
    }

    public static function extractHttpLink(?string $content)
    {
        if ($content == null) {
            return null;
        }
        preg_match(self::URL_RULE, $content, $result);
        if ($result != null) {
            return $result[0];
        }
        return null;
    }

}