<?php

namespace lib;

class IpHelper {
    const URL = 'http://ip.taobao.com//service/getIpInfo.php';
    public static function get_city($ip = "") {
        if ($ip == "") {
            $ip = \FUR_Helper::get_ip ();
        }
        $decode_data = self::get_ip_data ( $ip );
        if ($decode_data != null) {
            return $decode_data ['data'] ['city'];
        }
    }
    public static function get_ip_data($ip = "") {
        if ($ip == "") {
            $ip = \FUR_Helper::get_ip ();
        }
        $url = self::URL . "?ip=" . $ip;
        $data = \FUR_Curl::get ( $url );
        if ($data != NULL) {
            $decode_data = json_decode ( $data, true );
            if ($decode_data ['code'] == 0) {
                return $decode_data;
            }
        }
        return null;
    }
}