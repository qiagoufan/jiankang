<?php


namespace lib\aliyun\uploader;


use OSS\Core\OssUtil;

class AliyunVodDownloader
{
    public function __construct($saveLocalDir = null)
    {
        if (isset($saveLocalDir)) {
            $this->saveLocalDir = $saveLocalDir;
        } else {
            $this->saveLocalDir =APP_PATH. 'log/tmp_dlfiles/';
        }
    }

    public function downloadFile($downloadUrl, $localFileName, $fileSize = null)
    {
        $localPath = $this->saveLocalDir . $localFileName;

        if (isset($fileSize)) {
            $lsize = @filesize($localPath);
            if ($lsize > 0 and $lsize == $fileSize) {
                return $localPath;
            }
        }
        if (!is_dir($this->saveLocalDir)) {
            @mkdir($this->saveLocalDir, 0777, true);
        }

        $sfp = @fopen($downloadUrl, "rb");
        if ($sfp === false) {
            throw new Exception("download file fail while reading " . $downloadUrl,
                AliyunVodError::VOD_ERR_FILE_DOWNLOAD);
        }

        $dfp = @fopen(OssUtil::encodePath($localPath), "ab+");
        if ($sfp === false) {
            throw new Exception("download file fail while writing " . $localPath,
                AliyunVodError::VOD_ERR_FILE_DOWNLOAD);
        }
        while (!feof($sfp)) {
            $contents = fread($sfp, 8 * 1024);
            fwrite($dfp, $contents);
        }

        fclose($sfp);
        fclose($dfp);

        return $localPath;
    }

    public function getSaveLocalDir()
    {
        return $this->saveLocalDir;
    }

    public function setSaveLocalDir($localDir)
    {
        return $this->saveLocalDir = $localDir;
    }

    private $saveLocalDir = null;
}
