<?php


namespace lib\aliyun\uploader;


class FileVodBase64Saver
{

    private $saveLocalDir = null;

    public function __construct($saveLocalDir = null)
    {
        if (isset($saveLocalDir)) {
            $this->saveLocalDir = $saveLocalDir;
        } else {
            $this->saveLocalDir = APP_PATH . '/file/';
        }
    }

    public function saveFile($base64Str, $localFileName = null, $fileSize = null)
    {
        if ($localFileName == null) {
            $localFileName = 'tmp' . uniqid();
        }

        $imageData = base64_decode($base64Str);
        $localPath = $this->saveLocalDir . $localFileName;

        $dfp = @fopen($localPath, "ab+");
        @fwrite($dfp, $imageData);
        fclose($dfp);
        return $localPath;
    }


}
