<?php

namespace lib\aliyun\uploader;

class AliyunVodUtils
{
    const VOD_MAX_TITLE_LENGTH = 128;
    const VOD_MAX_DESCRIPTION_LENGTH = 1024;

    public static function convertOssInternal($ossUrl, $ecsRegion = null, $isEnableSSL = false)
    {
        if (!$isEnableSSL) {
            $ossUrl = str_replace("https:", "http:", $ossUrl);
        }

        if (!is_string($ossUrl) || !is_string($ecsRegion)) {
            return $ossUrl;
        }
        $availableRegions = array('cn-qingdao', 'cn-beijing', 'cn-zhangjiakou', 'cn-huhehaote', 'cn-hangzhou', 'cn-shanghai', 'cn-shenzhen',
            'cn-hongkong', 'ap-southeast-1', 'ap-southeast-2', 'ap-southeast-3',
            'ap-northeast-1', 'us-west-1', 'us-east-1', 'eu-central-1', 'me-east-1');

        if (!in_array($ecsRegion, $availableRegions)) {
            return $ossUrl;
        }

        $ossUrl = str_replace("https:", "http:", $ossUrl);
        return str_replace(sprintf("oss-%s.aliyuncs.com", $ecsRegion),
            sprintf("oss-%s-internal.aliyuncs.com", $ecsRegion), $ossUrl);
    }

    public static function getFileName($fileUrl)
    {
        $fileUrl = urldecode($fileUrl);
        $pos = strrpos($fileUrl, '?');
        $briefPath = $fileUrl;
        if ($pos !== false) {
            $briefPath = substr($fileUrl, 0, $pos);
        }
        return array($briefPath, basename($briefPath));
    }

    public static function getFileExtension($fileName)
    {
        $pos = strrpos($fileName, '.');
        if ($pos !== false) {
            return substr($fileName, $pos + 1);
        }
        return null;
    }

    // 考虑分隔符为"/" 或 "\"(windows)
    public static function replaceFileName($filePath, $replace)
    {
        if (strlen($filePath) <= 0 || strlen($replace) <= 0) {
            return $filePath;
        }
        $filePath = urldecode($filePath);
        $separator = '/';
        $start = strrpos($filePath, $separator);
        if ($start === false) {
            $separator = '\\';
            $start = strrpos($filePath, '\\');
            if ($start === false) {
                return false;
            }
        }
        return substr($filePath, 0, $start) . $separator . $replace;
    }

    public static function mkDir($filePath)
    {
        if (strlen($filePath) <= 0) {
            return true;
        }
        $filePath = urldecode($filePath);
        $start = strrpos($filePath, '/');
        if ($start === false) {
            return false;
        }
        $fileDir = substr($filePath, 0, $start);
        if (!is_dir($fileDir)) {
            return mkdir($fileDir, 0777, true);
        }
        return true;

    }

    // 截取字符串，在不超过最大字节数前提下确保中文字符不被截断出现乱码
    public static function subString($strVal, $maxBytes)
    {
        $i = mb_strlen($strVal);
        while (strlen($strVal) > $maxBytes) {
            $i--;
            if ($i <= 0) {
                return '';
            }
            $strVal = mb_substr($strVal, 0, $i);
        }
        return $strVal;
    }

    public static function getCurrentTimeStr()
    {
        return date("Y-m-d H:i:s");

    }

    public static function startsWith($haystack, $needle)
    {
        return strncmp($haystack, $needle, strlen($needle)) === 0;
    }

    public static function endsWith($haystack, $needle)
    {
        return $needle === '' || substr_compare($haystack, $needle, -strlen($needle)) === 0;
    }


}
