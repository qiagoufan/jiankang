<?php



namespace lib\aliyun\uploader;


class AliyunVodError
{
    const VOD_ERR_FILE_READ = 10000;
    const VOD_ERR_FILE_DOWNLOAD = 10001;
    const VOD_ERR_M3U8_FILE_REWRITE = 10002;
    const VOD_INVALID_M3U8_SLICE_FILE = 10003;
}