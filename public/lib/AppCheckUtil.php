<?php


namespace lib;


use facade\request\RequestParams;

class AppCheckUtil
{

    const IPHONE_CHECKING_VERSION = '1.0.1';
    const PLATFORM_IPHONE = 'iPhone';
    const PLATFORM_IPAD = 'iPad';

    public static function isAppleCheckVersion(RequestParams $requestParams): bool
    {
        if ($requestParams->app_version = self::IPHONE_CHECKING_VERSION) {
            if ($requestParams->platform == self::PLATFORM_IPAD || $requestParams->platform == self::PLATFORM_IPHONE) {
                return true;
            }
        }

        return false;

    }

    public static function isApple(RequestParams $requestParams): bool
    {
        if ($requestParams->platform == self::PLATFORM_IPAD || $requestParams->platform == self::PLATFORM_IPHONE) {
            return true;
        }
        return false;
    }

}