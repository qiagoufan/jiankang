<?php

namespace lib;

class Menu {
    public static $_instance;
    public static function get_instance() {
        if (! self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }
    public function init_menu($user_name = '', $user_id = 0) {
        $tab_name = \FUR_Dispatcher::$controller;
        $url_path = "/" . \FUR_Dispatcher::$controller . "/" . \FUR_Dispatcher::$class . "/" . \FUR_Dispatcher::$action;
        
        $menu_list = \FUR_Config::load_config ( 'menu_config', 'menu_list' );
        $menu_html = "";
        if ($menu_list != null) {
            foreach ( $menu_list as $menu ) {
                $menu_size = count ( $menu );
                if ($menu_size == 0) {
                    continue;
                }
                if ($menu [0] ['tab_name'] == $tab_name) {
                    $menu_html .= '<li class="active open">';
                } else {
                    $menu_html .= '<li>';
                }
                $menu_html .= '<a href="' . $menu [0] ['url'] . '">';
                $menu_html .= '<i class="' . $menu [0] ['icon_class'] . '"></i>';
                $menu_html .= '<span class="title">' . $menu [0] ['title'] . '</span>';
                
                if ($menu [0] ['tab_name'] == $tab_name) {
                    $menu_html .= '<span class="selected"></span>';
                }
                if ($menu_size > 1) {
                    $menu_html .= '<i class="icon-arrow"></i>';
                }
                $menu_html .= '</a>';
                
                if ($menu_size > 1) {
                    $menu_html .= '<ul class="sub-menu">';
                    for($i = 1; $i < $menu_size; $i ++) {
                        $menu_split_list = explode ( "/", $menu [$i] ['url'] );
                        $is_same_controller = isset ( $menu_split_list [1] ) && \FUR_Dispatcher::$controller == $menu_split_list [1];
                        $is_same_class = isset ( $menu_split_list [2] ) && \FUR_Dispatcher::$class == $menu_split_list [2];
                        if ($is_same_controller && $is_same_class) {
                            $menu_html .= '<li class="active open">';
                        } else {
                            $menu_html .= '<li>';
                        }
                        $menu_html .= '<a href="' . $menu [$i] ['url'] . '">';
                        $menu_html .= '<span class="title">' . $menu [$i] ['title'] . '</span>';
                        $menu_html .= '</a>';
                        $menu_html .= '</li>';
                    }
                    $menu_html .= '</ul>';
                }
                $menu_html .= "</li>";
            }
            return $menu_html;
        }
    }
}
