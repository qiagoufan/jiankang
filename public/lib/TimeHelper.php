<?php


namespace lib;


use Carbon\Carbon;

class TimeHelper
{
    const SECONDS_FIVE_MIN = 300;
    const SECONDS_ONE_HOUR = 3600;
    const SECONDS_TWELVE_HOUR = 43200;
    const SECONDS_ONE_DAY = 86400;
    const SECONDS_THREE_DAY = 259200;
    const SECONDS_ONE_WEEK = 604800;
    const SECONDS_TWO_WEEK = 1209600;
    const SECONDS_THIRTY_DAY = 2592000;

    const MAX_INT = 2140000000;

    public static function getPersonalTimeString(?int $timestamp)
    {
        return date('Y-m-d H:i:s', $timestamp);
        Carbon::setLocale('zh_CN');
        $carbon = Carbon::createFromTimestamp($timestamp);
        return $carbon->diffForHumans();
    }

    public static function getLastOfMonth(int $timestamp, $format = 'Ymd')
    {
        Carbon::setLocale('zh_CN');
        $carbon = Carbon::createFromTimestamp($timestamp);
        return $carbon->lastOfMonth()->format($format);
    }

    public static function getTimeMs()
    {
        list($msec, $sec) = explode(" ", microtime());
        return (float)sprintf('%.0f', (floatval($msec) + floatval($sec)) * 1000);
    }
}