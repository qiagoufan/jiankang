<?php


namespace lib;


class GenerationDTOUtil extends \FUR_Model
{

    public function generateDb($module, $share_id = ''): bool
    {
        $mysql = $this->select_mysql_db($module, $share_id);
        $tableList = $mysql->query('show tables');
        if ($tableList == null) {
            return true;
        }
        if (is_writeable(MODEL_PATH) == false) {
            echo MODEL_PATH . 'can not write';
        }

        foreach ($tableList as $tableInfo) {
            foreach ($tableInfo as $key => $full_table_name) {
                $this->generateTable($full_table_name, $module, $share_id);
            }
        }
        return true;
    }

    public function generateTable($full_table_name, $module, $share_id = '')
    {
        $mysql = $this->select_mysql_db($module, $share_id);
        $table_pre = \FUR_Config::load_system_config('system_config', 'table_pre');


        if ($table_pre != null && !str_ends_with($table_pre, '_')) {
            $table_pre = $table_pre . "_";
        }

        $table_name = substr($full_table_name, strlen($table_pre));
        $table_name_split = explode('_', $table_name);
        $biz_name = $table_name_split[0];
        $biz_path = MODEL_PATH . $biz_name;
        if (!is_dir($biz_path)) {
            mkdir($biz_path);
        }
        $dto_path = $biz_path . DIRECTORY_SEPARATOR . 'dto' . DIRECTORY_SEPARATOR;
        if (!is_dir($dto_path)) {
            mkdir($dto_path);
        }
        $class_name = $this->getClassName($table_name_split);
        $file_path = $dto_path. $class_name . "DTO.php";
        $table_column_list = $mysql->query("SHOW FULL COLUMNS FROM $full_table_name");
        $this->writeFile($file_path, $class_name, $biz_name, $table_column_list);
        echo "generate table:" . $full_table_name . " success \n";
    }


    private function getClassName(array $table_name_split)
    {
        $file_name = '';
        foreach ($table_name_split as $fragment) {
            $file_name .= ucfirst($fragment);
        }
        return $file_name;
    }

    private function writeFile($file_path, $class_name, $biz_name, $table_column_list)
    {
        $fopen = fopen($file_path, "w");
        $content = "<?php \n \n";

        $content .= "namespace model\\" . $biz_name . "\\dto;";
        $content .= " \n \n";
        $content .= sprintf("class %sDTO { \n\n", $class_name);
        foreach ($table_column_list as $table_column) {
            $content .= "\t //" . $table_column['Comment'] . "\n";
            $content .= "\t public $" . $table_column['Field'] . ";\n";
        }
        $content .= " \n \n }";
        try {
            fwrite($fopen, $content);
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
    }
}