<?php

namespace lib;

class Kuaidi100Helper
{
    const CUSTOMER = '9484684491D15BDDA9DFA38D7FC94A09';
    const KEY = 'ueRUKMyJ4136';
    const QUERY_URL = 'http://poll.kuaidi100.com/poll/query.do';


    public static function getLogisticsInfo(string $express_company, string $express_no)
    {
        $params['com'] = $express_company;
        $params['num'] = $express_no;
        $post_data['customer'] = self::CUSTOMER;
        $post_data["param"] = json_encode($params);
        $post_data["sign"] = md5($post_data["param"] . self::KEY . $post_data["customer"]);
        $post_data["sign"] = strtoupper($post_data["sign"]);
        $data = \FUR_Curl::post(self::QUERY_URL, http_build_query($post_data));

        return $data;
    }


}