<?php

namespace lib;

class PriceHelper
{
    public static function conversionDisplayPrice(?int $price = 0, ?bool $needCurrencySymbol = false)
    {
        if ($needCurrencySymbol) {
            return '¥' . number_format($price / 100, 2, '.', '');
        } else {
            return (string)number_format($price / 100, 2, '.', '');
        }
    }


}