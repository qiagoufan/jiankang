<?php

namespace lib;


class Advertisement {
    const STATUS_NEW = 0;
    public static function get_img_url($img_path) {
        return \FUR_Config::load_config ( 'common', 'upload_path_url' ) . $img_path;
    }
}