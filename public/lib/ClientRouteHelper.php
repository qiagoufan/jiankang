<?php


namespace lib;


class ClientRouteHelper
{

    public static function buildSuperProductLink(int $skuId, int $feedId = 0)
    {
        $link = 'xh://star/super-detail?skuId=' . $skuId;
        if ($feedId != 0) {
            $link .= "&feedId=" . $feedId;
        }
        return $link;
    }

    public static function buildPdpLink($skuId)
    {
        return 'xh://product/detail?skuId=' . $skuId;
    }

    public static function buildReviewLink($itemId, $reviewId)
    {
        return sprintf('xh://product/review?itemId=%s&reviewId=%s', $itemId, $reviewId);
    }


    public static function buildStarLink($starId, $ipType = 1)
    {
        return 'xh://star/detail?starId=' . $starId . "&ipType=" . $ipType;
    }

    public static function buildFeedLink($feedId)
    {
        return 'xh://media/detail?feedId=' . $feedId;
    }

    public static function buildTheatreChallengeLink($challengeId)
    {
        return 'xh://challenge/detail?challengeId=' . $challengeId;
    }

    public static function buildUploadLink($starId = 0, $skuId = 0)
    {
        $url = 'xh://center/upload-feed?';
        if ($starId != 0) {
            $url .= 'starId=' . $starId;
        }
        if ($skuId != 0) {
            $url .= '&skuId=' . $skuId;
        }
        return $url;
    }
}