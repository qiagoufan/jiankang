<?php


namespace lib;

class AppConstant
{

    const DEFAULT_PAGE_SIZE = 10;

    const PLATFORM_IPHONE = 'iPhone';
    const PLATFORM_IPAD = 'iPad';
    const PLATFORM_ANDROID = 'android';
    const PLATFORM_H5 = 'h5';
    const PLATFORM_WX_MINI = 'wxMini';
    const PLATFORM_ADMIN = 'admin';
    const PLATFORM_WX_H5 = 'wx';


    const BIZ_TYPE_STAR = 'star';
    const BIZ_TYPE_SKU = 'sku';
    const BIZ_TYPE_FEED = 'feed';
    const BIZ_TYPE_SPU_ITEM = 'item';
    const BIZ_TYPE_OUTSITE = 'outsite';
    const BIZ_TYPE_CHALLENGE = 'challenge';
    const BIZ_TYPE_USER = 'user';

}