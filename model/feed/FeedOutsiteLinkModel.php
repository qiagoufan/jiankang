<?php


namespace model\feed;


use model\feed\dto\FeedOutsiteLinkDTO;

class FeedOutsiteLinkModel extends \FUR_Model
{
    public static $_instance;


    const FEED_REDIS_MODULE = 'feed';

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function insert(FeedOutsiteLinkDTO $feedOutsiteLinkDTO): int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert('feed_outsite_link', $feedOutsiteLinkDTO);
    }

    public function getRecord(int $id): ?FeedOutsiteLinkDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne('select *  from feed_outsite_link where id =? ', [$id],FeedOutsiteLinkDTO::class);
    }


    public function queryRecordByLink(string $link): ?FeedOutsiteLinkDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne('select *  from feed_outsite_link where link_address =? limit 1', [$link],FeedOutsiteLinkDTO::class);
    }

    public function updateRecord(FeedOutsiteLinkDTO $feedOutsiteLinkDTO): ?FeedOutsiteLinkDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->update('feed_outsite_link', $feedOutsiteLinkDTO, ['id' => $feedOutsiteLinkDTO->id]);
    }


}