<?php


namespace model\feed;


use model\feed\dto\FeedInformationDTO;

class FeedInformationModel extends \FUR_Model
{
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function insertRecord(FeedInformationDTO $feedInformationDTO): int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert('feed_information', $feedInformationDTO);
    }


    public function getDetail(int $id): ?FeedInformationDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne('select * from feed_information where id =?', [$id], FeedInformationDTO::class);
    }


    public function getInformationList(int $offset, int $pageSize): ?array
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->select('select * from feed_information order by  id desc limit ?,? ', [$offset, $pageSize], FeedInformationDTO::class);

    }


    public function queryInformationListCount(): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $data = $mysql->selectOne('select count(*)as total_count   from feed_information ');
        if ($data != null) {
            return $data->total_count;
        }
        return 0;
    }


    public function deleteRecord(int $InformationId): ?bool
    {
        $mysql = $this->select_mysql_db('main');
        $data = ['status' => 0, 'updated_timestamp' => time()];
        $args = ['id' => $InformationId];
        return $mysql->update('feed_information', $data, $args);

    }


    public function updateInformationById(FeedInformationDTO $feedInformationDTO): ?bool
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->update('feed_information', $feedInformationDTO, ['id' => $feedInformationDTO->id]);

    }

}