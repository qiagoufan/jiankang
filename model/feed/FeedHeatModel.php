<?php


namespace model\feed;


use model\feed\dto\FeedHeatDTO;

class FeedHeatModel extends \FUR_Model
{
    public static $_instance;


    const FEED_REDIS_MODULE = 'feed';

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function incrPointCache(FeedHeatDTO $feedHeatDTO): int
    {
        $redis = $this->get_redis(self::FEED_REDIS_MODULE);
        $key = $this->buildHeatKey($feedHeatDTO->feed_id);
        return $redis->incr($key, $feedHeatDTO->point);
    }

    private function buildHeatKey(int $feedId)
    {
        return 'feed_heat:' . $feedId;
    }

    public function incrHeatPoint(FeedHeatDTO $feedHeatDTO): int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->query('INSERT INTO feed_heat (feed_id,heat,created_timestamp) 
                                    VALUES(?,?,?) ON DUPLICATE KEY UPDATE updated_timestamp=?,heat=heat+?',
            [$feedHeatDTO->feed_id, $feedHeatDTO->point, $feedHeatDTO->created_timestamp, $feedHeatDTO->updated_timestamp, $feedHeatDTO->point]);

    }


    public function getPoint(int $feedId): int
    {
        $heatPoint = 0;
        $mysql = $this->select_mysql_db('main');
        $data = $mysql->selectOne('select heat from feed_heat where feed_id =?', [$feedId]);
        if ($data) {
            $heatPoint = $data->heat;
        }
        return $heatPoint;
    }

    public function getPointCache(int $feedId): int
    {
        $redis = $this->get_redis(self::FEED_REDIS_MODULE);
        $key = $this->buildHeatKey($feedId);
        return $redis->get($key);
    }


}