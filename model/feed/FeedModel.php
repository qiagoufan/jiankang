<?php


namespace model\feed;

use facade\request\feed\GetFeedListParams;
use facade\request\admin\GetAllFeedListParams;
use facade\request\feed\QueryManageFeedListParams;
use facade\request\PageParams;
use model\feed\dto\FeedInfoDTO;
use model\feed\dto\FeedSendDTO;

class FeedModel extends \FUR_Model
{
    public static $_instance;

    const FEED_REDIS_MODULE = 'feed';

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function insertRecord(FeedInfoDTO $feedInfoDTO): int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert('feed_info', $feedInfoDTO);
    }

    public function insertUserSendRecord(FeedSendDTO $feedSendDTO): int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert('feed_send', $feedSendDTO);
    }

    public function updateUserSendHistory(FeedSendDTO $feedSendDTO): int
    {
        $key = $this->buildUserFeedSendHistoryKey($feedSendDTO->user_id);
        $score = $feedSendDTO->published_timestamp;
        $member = $feedSendDTO->feed_id;
        $redis = $this->get_redis(self::FEED_REDIS_MODULE);
        return $redis->zadd($key, $score, $member);
    }


    public function removeUserSendHistory(int $userId, int $feeId): int
    {
        $key = $this->buildUserFeedSendHistoryKey($userId);
        $member = $feeId;
        $redis = $this->get_redis(self::FEED_REDIS_MODULE);
        return $redis->zrem($key, $member);
    }

    public function updateUserReceiveList(array &$feedReceiveDTOList, int $userId): int
    {
        if (empty($feedReceiveDTOList)) {
            return 0;
        }
        $key = $this->buildUserFeedReceiveListKey($userId);
        $redis = $this->get_redis(self::FEED_REDIS_MODULE);
        $num = 0;
        foreach ($feedReceiveDTOList as $feedReceiveDTO) {
            $redis->zadd($key, $feedReceiveDTO->published_timestamp, $feedReceiveDTO->feed_id);
            $num++;
        }

        return $num;

    }

    public function getUserReceiveList(int $userId, int $start, int $pageSize): ?array
    {
        $key = $this->buildUserFeedReceiveListKey($userId);
        $redis = $this->get_redis(self::FEED_REDIS_MODULE);
        return $redis->zrevrange($key, $start, $start + $pageSize);
    }

    public function getUserPostCount(int $userId): ?int
    {
        $rowCount = 0;
        $mysql = $this->select_mysql_db('main');
        $data = $mysql->selectOne("select count(*)  as row_count  from feed_info  WHERE author_id = ?  and status !=0 and status != -2", [$userId]);
        if ($data != null) {
            $rowCount = $data->row_count;
        }
        return $rowCount;
    }


    public function getUserPostCountCache(int $userId): ?int
    {

        $key = $this->buildUserFeedSendHistoryKey($userId);
        $redis = $this->get_redis(self::FEED_REDIS_MODULE);
        return $redis->zcard($key);

    }

    /**
     * 根据开始时间和结束时间倒序获取该时间段内用户发送的feed列表
     * @param int $userId
     * @param int $startTimestamp
     * @param int $endTimestamp
     * @return
     */
    public function getUserSendHistory(int $userId, int $startTimestamp, int $endTimestamp): ?array
    {
        $key = $this->buildUserFeedSendHistoryKey($userId);
        $redis = $this->get_redis(self::FEED_REDIS_MODULE);
        $args = ['withscores' => true, 'limit' => [0, 20]];

        $feedIdList = [];
        if ($startTimestamp > $endTimestamp) {
            $feedIdList = $redis->ZREVRANGEBYSCORE($key, $startTimestamp, $endTimestamp, $args);
        } else {
            $feedIdList = $redis->ZREVRANGEBYSCORE($key, $endTimestamp, $startTimestamp, $args);
        }
        return $feedIdList;
    }

    public function queryFeedDetail(int $feedId): ?FeedInfoDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select * from feed_info where id =? and status !=0 limit 1 ", [$feedId], FeedInfoDTO::class);
    }

    public function setFeedDelete(int $feedId): bool
    {
        $mysql = $this->select_mysql_db('main');
        $feedDTO = new FeedInfoDTO();
        $feedDTO->status = FeedInfoDTO::FEED_STATUS_DELETE;
        $mysql->update('feed_info', $feedDTO, ['id' => $feedId]);
        return true;
    }

    public function deleteFeed(int $feedId): bool
    {
        $mysql = $this->select_mysql_db('main');
        $mysql->delete('feed_info', ['id' => $feedId]);
        return true;
    }


    public function deleteUserSendHistory(int $feedId): bool
    {
        $mysql = $this->select_mysql_db('main');
        $mysql->delete('feed_send', ['feed_id' => $feedId]);
        return true;
    }


    public function queryUserFeedList(PageParams $params): ?array
    {
        $mysql = $this->select_mysql_db('main');
        $page_size = intval($params->page_size);
        $index = intval($params->index);
        $offset = $page_size * $index;

        return $mysql->select("select * from feed_info where author_id =? order by id desc  limit ?, ? ", [$params->user_id, $offset, $page_size], FeedInfoDTO::class);

    }

    public function queryFeedList(GetFeedListParams $getFeedListParams): ?array
    {

        $mysql = $this->select_mysql_db('main');
        $page_size = intval($getFeedListParams->page_size);
        $index = intval($getFeedListParams->index);
        $offset = $page_size * $index;
        $sql = 'select * from feed_info where 1=1 ';
        if ($getFeedListParams->is_publish) {
            $sql = 'select feed_info.* from feed_info left join interactive_overview on feed_info.id=interactive_overview.biz_id where 1=1 and  feed_info.status > 0 order by interactive_overview.like_count desc,feed_info.id desc';
        } else {
            if ($getFeedListParams->keyword) {
                $keyword = "'%" . $getFeedListParams->keyword . "%'";
                $sql .= " and (feed_title like $keyword or content like $keyword) ";
            }
            if ($getFeedListParams->feed_category_id) {
                $sql .= " and feed_category_id=" . $getFeedListParams->feed_category_id;
            }
            if ($getFeedListParams->scene == \Config_Const::FEED_SCENE_INDEX) {
                $sql .= " and status > 0 ";
            }
            $sql = $sql . " order by id desc";
        }


        return $mysql->select($sql . " limit ?, ? ", [$offset, $page_size], FeedInfoDTO::class);

    }

    public function queryFeedCount(GetFeedListParams $getFeedListParams): ?int
    {

        $mysql = $this->select_mysql_db('main');

        $totalCount = 0;
        $sql = 'select count(*) as total_count from feed_info where 1=1';
        if ($getFeedListParams->keyword) {
            $keyword = "'%" . $getFeedListParams->keyword . "%'";
            $sql .= " and (feed_title like $keyword or content like $keyword)";
        }
        if ($getFeedListParams->feed_status) {
            $sql .= " and feed_group_id=" . $getFeedListParams->feed_status;
        }
        if ($getFeedListParams->scene == \Config_Const::FEED_SCENE_INDEX) {
            $sql .= " and status > 0";
        }
        $data = $mysql->select($sql);
        if ($data) {
            $totalCount = $data->total_count;
        }
        return $totalCount;

    }

    public function getIndexFeedIdList(GetFeedListParams $getFeedListParams): ?array
    {

        $mysql = $this->select_mysql_db('main');
        $page_size = intval($getFeedListParams->page_size);
        $index = intval($getFeedListParams->index);
        $offset = $page_size * $index;
        return $mysql->select("select feed_id from featured_feed where 1=1 order by rand desc  limit ?, ? ", [$offset, $page_size]);
    }

    public function getFeatureFeedIdListByTemplateType(int $index, int $templateType, int $pageSize): ?array
    {

        $mysql = $this->select_mysql_db('main');
        $offset = $pageSize * $index;
        return $mysql->select("select feed_id from featured_feed where templateType =? order by rand desc  limit ?, ? ", [$templateType, $offset, $pageSize]);
    }

    public function queryFeedByUserId(array $followUserList): ?array
    {
        $mysql = $this->select_mysql_db('main');
        $followUserList = implode(",", $followUserList);;
        return $mysql->select("select * from feed_info  WHERE author_id IN (?) and status !=0 order by created_timestamp desc  ", [$followUserList], FeedInfoDTO::class);

    }


    public function queryFeedListByUserId(int $userId, int $offset, int $pageSize): ?array
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->select("select * from feed_info  WHERE author_id = ? and status !=0  order by created_timestamp desc limit ?,?  ", [$userId, $offset, $pageSize], FeedInfoDTO::class);

    }


    public function queryUserPostFeedListCount(int $visitUserId, int $userId): ?int
    {
        $mysql = $this->select_mysql_db('main');
        if ($visitUserId == $userId) {
            //主态
            $data = $mysql->selectOne("select count(*) as row_count from feed_info   WHERE author_id = ? and status !=0", [$visitUserId]);
        } else {
            //客态
            $data = $mysql->selectOne("select count(*) as row_count from feed_info  WHERE author_id = ? and status=?", [$visitUserId, FeedInfoDTO::FEED_STATUS_NORMAL]);
        }
        $rowCount = 0;

        if (isset($data)) {
            $rowCount = $data->row_count;
        }

        return $rowCount;

    }

    public function getFeedInfoRespCache(int $feedId, string $field)
    {

        $key = $this->buildUserFeedInfoRespKey($feedId);
        $redis = $this->get_redis(self::FEED_REDIS_MODULE);
        return $redis->hget($key, $field);
    }

    public function setFeedInfoRespExpire(int $feedId, int $seconds)
    {

        $key = $this->buildUserFeedInfoRespKey($feedId);
        $redis = $this->get_redis(self::FEED_REDIS_MODULE);
        return $redis->expire($key, $seconds);
    }

    public function setFeedInfoRespCache(int $feedId, string $field, string $value): ?bool
    {

        $key = $this->buildUserFeedInfoRespKey($feedId);
        $redis = $this->get_redis(self::FEED_REDIS_MODULE);
        return $redis->hset($key, $field, $value);
    }

    private function buildUserFeedInfoRespKey(int $feedId): string
    {
        return 'feed_info_resp_hash:' . $feedId;
    }

    private function buildUserFeedSendHistoryKey(int $userId): string
    {
        return 'feed_send_history:' . $userId;
    }

    private function buildUserFeedReceiveListKey(int $userId): string
    {
        return 'feed_receive_list:' . $userId;
    }


    public function queryManageFeedList(QueryManageFeedListParams $params): ?array
    {
        $feedStatus = $params->feed_status;
        $pageSize = $params->page_size;
        $index = $params->index;
        $offset = $pageSize * $index;
        $mysql = $this->select_mysql_db('main');
        $args = [$offset, $pageSize];
        $condition = ' where 1=1 ';
        if ($feedStatus) {
            $condition = $condition . " and status =?";
            $args = array_merge([$feedStatus], $args);
        }
        if ($params->keyword) {
            $keyword = "'%" . $params->keyword . "%'";
            $condition = $condition . " and ( feed_title like " . $keyword . ")";
        }
        return $mysql->select('select * from feed_info ' . $condition . " order by id desc limit ?,?", $args, FeedInfoDTO::class);
    }


    public function queryManageFeedCount(QueryManageFeedListParams $params): ?int
    {
        $totalCount = 0;

        $feedStatus = $params->feed_status;
        $mysql = $this->select_mysql_db('main');
        $args = [];
        $condition = ' where 1=1 ';
        if ($feedStatus) {
            $condition = $condition . " and status =?";
            $args = array_merge([$feedStatus], $args);
        }
        if ($params->keyword) {
            $keyword = "'%" . $params->keyword . "%'";
            $condition = $condition . " and ( feed_title like " . $keyword . ")";
        }

        $data = $mysql->selectOne('select count(*) as total_count from feed_info ' . $condition, $args, FeedInfoDTO::class);
        if ($data) {
            $totalCount = $data->total_count;
        }
        return $totalCount;

    }

    public function updateFeedInfo(FeedInfoDTO $feedInfoDTO): ?bool
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->update('feed_info', $feedInfoDTO, ['id' => $feedInfoDTO->id]);
    }


}