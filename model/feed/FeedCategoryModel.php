<?php


namespace model\feed;


use model\feed\dto\FeedCategoryDTO;

class FeedCategoryModel extends \FUR_Model
{
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function getFeedCategory($id): ?FeedCategoryDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select * from feed_category where id =?", [$id], FeedCategoryDTO::class);

    }

    public function insertFeedCategory(?FeedCategoryDTO $FeedCategoryDTO): ?int
    {

        $mysql = $this->select_mysql_db('main');
        return $mysql->insert("feed_category", $FeedCategoryDTO);
    }

    public function deleteFeedCategory(?int $id): ?int
    {

        $mysql = $this->select_mysql_db('main');
        return $mysql->delete("feed_category", ['id' => $id]);
    }

    public function updateFeedCategory(?FeedCategoryDTO $FeedCategoryDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->update("feed_category", $FeedCategoryDTO, ['id' => $FeedCategoryDTO->id]);

    }

    public function getFeedCategoryList(): ?array
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->select("select * from feed_category order by id desc ", [], FeedCategoryDTO::class);

    }


}