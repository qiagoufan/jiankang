<?php 
 
namespace model\feed\dto; 
 
class FeedGroupDTO { 

	 //主键
	 public $id;
	 //分组名称
	 public $feed_group_name;
	 //创建时间
	 public $created_timestamp;
	 //更新时间
	 public $updated_timestamp;
 
 
 }