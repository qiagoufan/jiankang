<?php 
 
namespace model\feed\dto; 
 
class FeedOutsiteLinkDTO { 

	 //primary key
	 public $id;
	 //链接最大长度
	 public $link_address;
	 //链接标题
	 public $link_title;
	 //站点名称
	 public $site_name;
	 //简单说明
	 public $summary;
	 //创建时间
	 public $created_timestamp;
	 //更新时间
	 public $updated_timestamp;
	 //????
	 public $status;
	 //????:1.??.2??
	 public $link_type;
 
 
 }