<?php 
 
namespace model\feed\dto; 
 
class FeedSendDTO { 

	 //主键
	 public $id;
	 //发送者
	 public $user_id;
	 //feed id
	 public $feed_id;
	 //发布时间
	 public $published_timestamp;
 
 
 }