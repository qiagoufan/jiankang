<?php 
 
namespace model\feed\dto; 
 
class FeedTopDTO { 

	 //主键
	 public $id;
	 //feed id
	 public $feed_id;
	 //创建时间
	 public $created_timestamp;
	 //更新时间
	 public $updated_timestamp;
	 //是否生效1.是0.否
	 public $status;
 
 
 }