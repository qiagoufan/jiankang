<?php 
 
namespace model\feed\dto; 
 
class FeedInformationDTO { 

	 //主键
	 public $id;
	 //标题
	 public $title;
	 //主图
	 public $main_image;
	 //??
	 public $content;
	 //站外链接
	 public $outsite_link;
	 //创建时间
	 public $created_timestamp;
	 //更新时间
	 public $updated_timestamp;
	 //????1.?0.?
	 public $is_original;
	 //????
	 public $outsite_author;
	 //????
	 public $outsite_source;
	 //??id
	 public $author_id;
	 //1.??0.??
	 public $status;
 
 
 }