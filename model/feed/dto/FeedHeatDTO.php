<?php


namespace model\feed\dto;


class FeedHeatDTO
{

    public $id;
    public $feed_id;
    public $point;
    public $created_timestamp;
    public $updated_timestamp;

}