<?php 
 
namespace model\feed\dto; 
 
class FeedReceiveDTO { 

	 //主键
	 public $id;
	 //接收者
	 public $user_id;
	 //feedid
	 public $feed_id;
	 //发布时间
	 public $published_timestamp;
 
 
 }