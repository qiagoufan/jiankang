<?php 
 
namespace model\feed\dto; 
 
class FeedInfoDTO { 

	 //主键
	 public $id;
	 //创作者
	 public $author_id;
	 //类型
	 public $feed_type;
	 //是否属于虚拟feed
	 public $virtual;
	 //媒资详情
	 public $content;
	 //状态
	 public $status;
	 //发布时间
	 public $published_timestamp;
	 //创建时间
	 public $created_timestamp;
	 //更新时间
	 public $updated_timestamp;
	 //模板类型
	 public $template_type;
	 //链接地址
	 public $detail_url;
	 //标题
	 public $feed_title;
	 //封面
	 public $cover_url;
	 //feed分类
	 public $feed_category_id;
 
 
 }