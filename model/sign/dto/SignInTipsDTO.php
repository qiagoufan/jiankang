<?php 
 
namespace model\sign\dto; 
 
class SignInTipsDTO { 

	 //主键
	 public $id;
	 //打卡区id
	 public $sign_in_type_id;
	 //内容
	 public $content;
	 //日期
	 public $show_date;
	 //图片
	 public $image;
	 //创建时间
	 public $created_timestamp;
	 //更新时间
	 public $updated_timestamp;
 
 
 }