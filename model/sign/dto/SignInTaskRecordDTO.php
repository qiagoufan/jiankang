<?php 
 
namespace model\sign\dto; 
 
class SignInTaskRecordDTO { 

	 //主键
	 public $id;
	 //类型id
	 public $sign_in_type_id;
	 //任务id
	 public $sign_in_task_id;
	 //用户id
	 public $user_id;
	 //任务完成情况
	 public $user_finish_status;
	 //任务得分
	 public $user_score;
	 //打卡日期
	 public $sign_in_date;
	 //创建时间
	 public $created_timestamp;
	 //更新时间
	 public $updated_timestamp;
 
 
 }