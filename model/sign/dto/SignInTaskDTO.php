<?php 
 
namespace model\sign\dto; 
 
class SignInTaskDTO { 

	 //
	 public $id;
	 //打卡id
	 public $sign_in_type_id;
	 //打卡任务名称
	 public $sign_in_task_name;
	 //打卡任务说明
	 public $sign_in_task_detail;
	 //打卡数值
	 public $task_value;
	 //打卡数值单位
	 public $task_value_unit;
	 //创建时间
	 public $created_timestamp;
	 //更新时间
	 public $updated_timestamp;
	 //任务背景
	 public $sign_in_task_bg;
 
 
 }