<?php 
 
namespace model\sign\dto; 
 
class SignInTypeDTO { 

	 //主键
	 public $id;
	 //打卡类型
	 public $sign_type_name;
	 //状态
	 public $status;
	 //创建时间
	 public $created_timestamp;
	 //更新时间
	 public $updated_timestamp;
	 //背景图片
	 public $sign_type_bg;
	 //排序号
	 public $seq;
 
 
 }