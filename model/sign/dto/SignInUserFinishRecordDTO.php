<?php 
 
namespace model\sign\dto; 
 
class SignInUserFinishRecordDTO { 

	 //主键
	 public $id;
	 //用户id
	 public $user_id;
	 //签到区
	 public $sign_type_id;
	 //签到日期
	 public $sign_in_date;
	 //创建时间
	 public $created_timestamp;
	 //更新时间
	 public $updated_timestamp;
 
 
 }