<?php


namespace model\sign;


use facade\request\PageParams;
use model\sign\dto\SignInTaskDTO;
use model\sign\dto\SignInTaskRecordDTO;
use model\sign\dto\SignInTipsDTO;
use model\sign\dto\SignInTypeDTO;
use model\sign\dto\SignInUserFinishRecordDTO;

class SignModel extends \FUR_Model
{
    public static $_instance;


    public static $mysql;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function createSignType(SignInTypeDTO $signInTypeDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert("sign_in_type", $signInTypeDTO);
    }

    public function updateSignType(SignInTypeDTO $signInTypeDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $args = ['id' => $signInTypeDTO->id];
        return $mysql->update("sign_in_type", $signInTypeDTO, $args);
    }

    public function getSignTypeDetail(int $signTypeId): ?SignInTypeDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select * from sign_in_type where id =?", [$signTypeId], SignInTypeDTO::class);
    }

    public function querySignTypeList(int $index, int $pageSize): ?array
    {
        $mysql = $this->select_mysql_db('main');
        $offset = $index * $pageSize;
        return $mysql->select("select * from sign_in_type  order by seq asc,id desc  limit ?,?", [$offset, $pageSize], SignInTypeDTO::class);
    }

    public function querySignTypeCount(): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $totalCount = 0;
        $data = $mysql->selectOne("select count(*) as total_count from sign_in_type  ");
        if ($data) {
            $totalCount = $data->total_count;
        }
        return $totalCount;
    }

    public function deleteSignType(int $signTypeId): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->query("delete  from sign_in_type where id =?", [$signTypeId]);
    }

    public function createSignTask(SignInTaskDTO $signInTaskDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert("sign_in_task", $signInTaskDTO);
    }

    public function updateSignTask(SignInTaskDTO $signInTaskDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $args = ['id' => $signInTaskDTO->id];
        return $mysql->update("sign_in_task", $signInTaskDTO, $args);
    }

    public function getSignTaskDetail(int $signTaskId): ?SignInTaskDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select * from sign_in_task where id =?", [$signTaskId], SignInTaskDTO::class);
    }

    public function deleteSignTask(int $signTaskId): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->query("delete  from sign_in_task where id =?", [$signTaskId]);
    }

    public function getSignTaskList(int $signTypeId): ?array
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->select("select * from sign_in_task where sign_in_type_id =? order by id desc", [$signTypeId], SignInTaskDTO::class);

    }

    public function insertSignTaskRecord(SignInTaskRecordDTO $signInTaskRecordDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert("sign_in_task_record", $signInTaskRecordDTO);
    }

    public function deleteUserTaskRecord(int $signTaskId, int $userId, int $date): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $args = ['user_id' => $userId, 'sign_in_task_id' => $signTaskId, 'sign_in_date' => $date];
        return $mysql->delete("sign_in_task_record", $args);
    }

    public function queryUserSignTaskRecord(int $signTaskId, int $userId, int $date): ?SignInTaskRecordDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select * from sign_in_task_record where  user_id=? and sign_in_task_id =? and sign_in_date =?", [$userId, $signTaskId, $date], SignInTaskRecordDTO::class);
    }

    public function queryUserSignTaskRecordList(int $signTaskId, int $userId,  $startDate, $endDate): ?array
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->select("select * from sign_in_task_record where  user_id=? and sign_in_task_id =?  and sign_in_date>=? and sign_in_date<=? order by id asc ", [$userId, $signTaskId, $startDate,$endDate], SignInTaskRecordDTO::class);
    }


    public function querySignTypeTaskList(int $signTypeId, PageParams $params): ?array
    {
        $mysql = $this->select_mysql_db('main');

        return $mysql->select("select * from sign_in_task where sign_in_type_id =? order by id desc  ", [$signTypeId], SignInTaskDTO::class);
    }

    public function querySignTypeTaskCount(int $signTypeId): ?int
    {
        $mysql = $this->select_mysql_db('main');

        $totalCount = 0;
        $data = $mysql->selectOne("select count(*) as total_count from sign_in_task where sign_in_type_id =? ", [$signTypeId]);
        if ($data) {
            $totalCount = $data->total_count;
        }
        return $totalCount;
    }

    public function queryUserFinishCount(int $userId, int $signInDate, int $signTypeId): ?int
    {
        $mysql = $this->select_mysql_db('main');

        $totalCount = 0;
        $data = $mysql->selectOne("select  count(*) as total_count  from sign_in_task_record where  user_id=? and  sign_in_date  =? and sign_in_type_id =? and user_finish_status=1", [$userId, $signInDate, $signTypeId]);

        if ($data) {
            $totalCount = $data->total_count;
        }
        return $totalCount;
    }

    public function insetUserFinishRecord(SignInUserFinishRecordDTO $signInUserFinishRecordDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert("sign_in_user_finish_record", $signInUserFinishRecordDTO);
    }

    public function queryUserSignFinishRecordList(int $signTypeId, int $userId, $startDate, $endDate): ?array
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->select("select * from sign_in_user_finish_record where user_id=? and sign_type_id=? and sign_in_date>=? and sign_in_date<=? order by id asc ", [$userId, $signTypeId, $startDate, $endDate], SignInUserFinishRecordDTO::class);
    }

    public function queryUserSignFinishRecord(int $signTypeId, int $userId, $date): ?SignInUserFinishRecordDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select * from sign_in_user_finish_record where user_id=? and sign_type_id=? and sign_in_date=? limit 1 ", [$userId, $signTypeId, $date], SignInUserFinishRecordDTO::class);
    }


    public function createSignTips(SignInTipsDTO $signInTipsDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert("sign_in_tips", $signInTipsDTO);
    }

    public function updateSignTips(SignInTipsDTO $signInTipsDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $args = ['id' => $signInTipsDTO->id];
        return $mysql->update("sign_in_tips", $signInTipsDTO, $args);
    }

    public function getSignTipsDetail(int $signTipsId): ?SignInTipsDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select * from sign_in_tips where id =?", [$signTipsId], SignInTipsDTO::class);
    }

    public function getLatestTips(int $signTypeId, int $showDate): ?SignInTaskDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select *  from sign_in_tips where sign_in_type_id =? and show_date <=? order by show_date desc limit 1", [$signTypeId, $showDate], SignInTaskDTO::class);
    }


    public function deleteSignTips(int $signTipsId)
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->query("delete  from sign_in_tips where id =?", [$signTipsId]);
    }

    public function getSignTipsList(int $signTypeId, PageParams $params): ?array
    {
        $mysql = $this->select_mysql_db('main');
        $offset = $params->index * $params->page_size;
        return $mysql->select("select * from sign_in_tips where sign_in_type_id =? order by show_date desc limit  ?,?", [$signTypeId, $offset, $params->page_size], SignInTaskDTO::class);

    }

    public function getSignTipsCount(int $signTypeId): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $count = 0;
        $data = $mysql->selectOne("select count(*) as total_count from sign_in_tips where sign_in_type_id =? ", [$signTypeId]);
        if ($data) {
            $count = $data->total_count;
        }
        return $count;
    }

}