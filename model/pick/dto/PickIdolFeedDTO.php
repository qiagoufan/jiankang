<?php 
 
namespace model\pick\dto; 
 
class PickIdolFeedDTO { 

	 //primary key
	 public $id;
	 //爱豆的feedid
	 public $idol_feed_id;
	 //爱豆的feed标题
	 public $idol_feed_title;
	 //爱豆的图片
	 public $idol_feed_images;
	 //爱豆的内容
	 public $idol_feed_text;
	 //爱豆的作者名字
	 public $idol_feed_author;
	 //爱豆的推送时间
	 public $idol_feed_publish_timestamp;
	 //爬取时间
	 public $created_timestamp;
	 //更新时间
	 public $updated_timestamp;
	 //????id
	 public $idol_star_id;
 
 
 }