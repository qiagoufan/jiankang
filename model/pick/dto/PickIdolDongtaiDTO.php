<?php 
 
namespace model\pick\dto; 
 
class PickIdolDongtaiDTO { 

	 //primary key
	 public $id;
	 //爱豆的动态id
	 public $idol_dongtai_id;
	 //爱豆的微博id
	 public $idol_weibo_id;
	 //爱豆的明星id
	 public $idol_star_id;
	 //爱豆的动态类型
	 public $idol_dongtai_type;
	 //微博动态的动作
	 public $idol_dongtai_action;
	 //??????
	 public $idol_dongtai_text;
	 //微博动态图片
	 public $idol_dongtai_image;
	 //????
	 public $weibo_url;
	 //爬取时间
	 public $created_timestamp;
	 //更新时间
	 public $updated_timestamp;
 
 
 }