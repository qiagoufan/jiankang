<?php


namespace model\pick\dto;


use facade\request\user\LoginByPhoneParams;
use facade\request\user\RegisterParams;

class IdolDongtaiDTO
{


    public $id;
    public $idol_dongtai_id;
    public $idol_weibo_id;
    public $idol_star_id;
    public $idol_dongtai_type;
    public $idol_dongtai_action;
    public $idol_dongtai_text;
    public $idol_dongtai_image;
    public $weibo_url;
    public $created_timestamp;
    public $updated_timestamp;
}