<?php


namespace model\pick\dto;


use facade\request\user\LoginByPhoneParams;
use facade\request\user\RegisterParams;

class IdolFeedDTO
{


    public $id;
    public $idol_star_id;
    public $idol_feed_id;
    public $idol_feed_title;
    public $idol_feed_images;
    public $idol_feed_text;
    public $idol_feed_author;
    public $idol_feed_publish_timestamp;
    public $created_timestamp;
    public $updated_timestamp;
}