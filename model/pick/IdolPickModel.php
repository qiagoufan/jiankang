<?php


namespace model\pick;


use model\pick\dto\IdolDongtaiDTO;
use model\pick\dto\IdolFeedDTO;

class IdolPickModel extends \FUR_Model
{
    public static $_instance;


    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function insertIdolDongtai(IdolDongtaiDTO $dongtaiDTO): int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert('pick_idol_dongtai', $dongtaiDTO);
    }


    public function queryIdolDongtaiByDongtaiId(string $idolDongtaiID): ?IdolDongtaiDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select * from pick_idol_dongtai where idol_dongtai_id =?  ", [$idolDongtaiID], IdolDongtaiDTO::class);
    }

    public function insertIdolFeed(IdolFeedDTO $idolFeedDTO): int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert('pick_idol_feed', $idolFeedDTO);
    }

    public function queryIdolFeedByFeedId(string $idolFeedId): ?IdolFeedDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select * from pick_idol_feed where idol_feed_id=? ", [$idolFeedId], IdolFeedDTO::class);
    }


}