<?php

namespace model\user;

use model\user\dto\UserLoginInfoDTO;

class UserLoginModel extends \FUR_Model
{
    public static $_instance;


    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function queryUserId(string $login_account, string $login_type): int
    {
        $userId = 0;
        $mysql = $this->select_mysql_db('main');
        $userDTO = $mysql->selectOne("select * from user_login_info where login_account = ? and login_type =?  limit 1", [$login_account, $login_type], UserLoginInfoDTO::class);

        if ($userDTO != null) {
            $userId = $userDTO->user_id;
        }
        return $userId;
    }

    public function queryUserLoginInfoDTO(string $login_account, string $login_type): ?UserLoginInfoDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select * from user_login_info where login_account = ? and login_type =?  limit 1", [$login_account, $login_type], UserLoginInfoDTO::class);
    }


    public function insertUserLoginRecord(UserLoginInfoDTO $loginInfoDTO): int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert('user_login_info', $loginInfoDTO);
    }

    public function queryUserLoginInfoByUserId(int $user_id, string $login_type): ?UserLoginInfoDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select * from user_login_info where user_id = ? and login_type =?  limit 1", [$user_id, $login_type], UserLoginInfoDTO::class);

    }


}