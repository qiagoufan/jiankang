<?php


namespace model\user;


use model\user\dto\UserPushTokenDTO;

class UserPushTokenModel extends \FUR_Model
{
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function updateUserPushToken(UserPushTokenDTO $userPushTokenDTO): int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->query('INSERT INTO user_push_token (user_id,device_id, platform, push_token,brand,created_timestamp,updated_timestamp) 
                                    VALUES(?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE push_token=?',
            [$userPushTokenDTO->user_id,
                $userPushTokenDTO->device_id,
                $userPushTokenDTO->platform,
                $userPushTokenDTO->push_token,
                $userPushTokenDTO->brand,
                $userPushTokenDTO->created_timestamp,
                $userPushTokenDTO->updated_timestamp,
                $userPushTokenDTO->push_token]);
    }


}