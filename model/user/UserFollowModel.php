<?php


namespace model;


use lib\TimeHelper;
use model\dto\UserFollowDTO;

class UserFollowModel extends \FUR_Model
{
    public static $_instance;

    const FOLLOW_REDIS_MODULE = 'follow';


    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function updateFansCount(int $targetId, string $targetType, int $number): bool
    {
        $key = $this->buildFansKey($targetId, $targetType);
        $redis = $this->get_redis(self::FOLLOW_REDIS_MODULE);
        return $redis->incrby($key, $number);

    }


    public function getFansCountFromDb(int $targetId, string $targetType)
    {
        $fansCount = 0;
        $mysql = $this->select_mysql_db('main');
        $data = $mysql->selectOne(" select count(*) as fans_count from user_follow where target_id=? and target_type =? order by follow_timestamp desc ", [$targetId, $targetType]);
        if ($data != null) {
            $fansCount = $data->fans_count;
        }

        return $fansCount;
    }

    public function getFansCount(int $targetId, string $targetType): int
    {
        $key = $this->buildFansKey($targetId, $targetType);
        $redis = $this->get_redis(self::FOLLOW_REDIS_MODULE);
        $count = (int)$redis->get($key);
        if ($count <= 0) {
            $count = $this->getFansCountFromDb($targetId, $targetType);
            $redis->set($key, $count);
        }
        return $count;

    }

    public function updateFocusCount(int $userId, string $targetType, int $number): bool
    {
        $key = $this->buildFocusKey($userId);
        $redis = $this->get_redis(self::FOLLOW_REDIS_MODULE);
        return $redis->hincrby($key, $targetType, $number);
    }


    public function getFocusCount(int $userId, string $targetType): ?int
    {
        $key = $this->buildUserFocusKey($userId, $targetType);
        $redis = $this->get_redis(self::FOLLOW_REDIS_MODULE);
        return $redis->zcard($key);
    }


    public function isFollowFromCache(int $userId, int $targetId, string $targetType): bool
    {
        $key = $this->buildUserFocusKey($userId, $targetType);
        $redis = $this->get_redis(self::FOLLOW_REDIS_MODULE);
        $timestamp = $redis->zscore($key, $targetId);
        return ($timestamp != null);
    }

    public function saveFollowCache(int $userId, int $targetId, int $timestamp, string $targetType): bool
    {
        $key = $this->buildUserFocusKey($userId, $targetType);
        $redis = $this->get_redis(self::FOLLOW_REDIS_MODULE);
        return $redis->zadd($key, $timestamp, $targetId);
    }

    public function unFollowCache(int $userId, int $targetId, string $targetType): bool
    {
        $key = $this->buildUserFocusKey($userId, $targetType);
        $redis = $this->get_redis(self::FOLLOW_REDIS_MODULE);
        return $redis->zrem($key, $targetId);
    }

    public function getFocusListFromCache(int $userId, string $targetType, int $index = 0, int $pageSize = 10): ?array
    {
        $start = $index * $pageSize;
        $key = $this->buildUserFocusKey($userId, $targetType);
        $redis = $this->get_redis(self::FOLLOW_REDIS_MODULE);
        return $redis->zrevrange($key, $start, $start + $pageSize - 1);
    }

    public function getFocusListFromDb(int $userId, string $targetType, int $offset, int $pageSize): array
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->select(" select * from user_follow where user_id=? and target_type =? order by follow_timestamp desc limit ?,?", [$userId, $targetType, $offset, $pageSize]);
    }

    public function queryRecordFromDb(int $userId, int $targetId, string $targetType): ?UserFollowDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne(" select * from user_follow where user_id=? and target_id=? and target_type=? limit 1 ",
            [$userId, $targetId, $targetType], UserFollowDTO::class);
    }

    public function saveFollowRecord(UserFollowDTO $userFollowDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert('user_follow', $userFollowDTO);
    }

    public function getRank(int $targetId, string $targetType): ?int
    {
        $key = $this->buildRankKey($targetType);
        $redis = $this->get_redis(self::FOLLOW_REDIS_MODULE);
        $data = $redis->zrevrank($key, $targetId);
        if ($data === false) {
            return -1;
        }
        return $data;
    }

    public function updateRankScore(int $targetId, string $targetType, int $score)
    {
        $key = $this->buildRankKey($targetType);
        $redis = $this->get_redis(self::FOLLOW_REDIS_MODULE);
        $score = $score . '.' . (TimeHelper::MAX_INT - time());
        return $redis->zAdd($key, $score, $targetId);
    }

    public function getLeaderboard(string $targetType, int $start, int $end): ?array
    {
        $key = $this->buildRankKey($targetType);
        $redis = $this->get_redis(self::FOLLOW_REDIS_MODULE);
        return $redis->zRevRange($key, $start, $end);

    }

    public function deleteFollowRecord(int $userId, int $targetId, string $targetType): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $args = ['user_id' => $userId, 'target_id' => $targetId, 'target_type' => $targetType];
        return $mysql->delete('user_follow', $args);
    }

    private function buildUserFocusKey(int $userId, string $targetType = ''): string
    {
        return 'follow_user_focus:' . $userId . ':' . $targetType;
    }

    private function buildFansKey(int $targetId, string $targetType): string
    {
        return 'follow_fans_count:' . $targetType . ':' . $targetId;
    }

    private function buildFocusKey(int $userId): string
    {
        return $key = 'follow_user_focus_count:' . $userId;
    }

    private function buildRankKey(string $targetType): string
    {
        return $key = 'follow_rank:' . $targetType;
    }
}