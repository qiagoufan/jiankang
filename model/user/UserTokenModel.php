<?php


namespace model\user;


use model\user\dto\UserTokenDTO;

class UserTokenModel extends \FUR_Model
{
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function insertRecord(UserTokenDTO $userTokenDTO): int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert('user_token', $userTokenDTO);
    }

    public function deleteUserToken(int $userId): int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->delete('user_token', ['user_id' => $userId]);
    }

    public function queryToken(string $refreshToken): ?UserTokenDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select * from user_token where refresh_token = ? limit 1", [$refreshToken], UserTokenDTO::class);
    }
}