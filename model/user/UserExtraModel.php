<?php


namespace model;


class UserExtraModel extends \FUR_Model
{
    public static $_instance;


    const USER_REDIS_MODULE = 'user';

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    private function buildUserExtraKey(int $userId): string
    {
        return 'user_extra:' . $userId;
    }


    public function updateUserExtraCount(int $userId, string $field, int $number): ?bool
    {
        $key = $this->buildUserExtraKey($userId);
        $redis = $this->get_redis(self::USER_REDIS_MODULE);
        return $redis->hincrby($key, $field, $number);
    }

    public function updateUserExtra(int $userId, string $field, string $value): ?bool
    {
        $key = $this->buildUserExtraKey($userId);
        $redis = $this->get_redis(self::USER_REDIS_MODULE);
        return $redis->hset($key, $field, $value);
    }

    public function getUserExtra(int $userId, string $field)
    {
        $key = $this->buildUserExtraKey($userId);
        $redis = $this->get_redis(self::USER_REDIS_MODULE);
        $ret = $redis->hget($key, $field);
        return $ret;
    }
}