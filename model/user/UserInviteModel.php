<?php

namespace model\user;

use facade\request\PageParams;
use model\user\dto\UserInviteRecordDTO;

class UserInviteModel extends \FUR_Model
{
    public static $_instance;


    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function insertRecord(UserInviteRecordDTO $userInviteRecordDTO): ?int
    {

        $mysql = $this->select_mysql_db('main');
        return $mysql->insert('user_invite_record', $userInviteRecordDTO);
    }

    public function queryUserInviteCount($userId): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $totalCount = 0;
        $data = $mysql->selectOne('select count(*) as total_count from user_invite_record where user_id =?   ', [$userId]);
        if ($data) {
            $totalCount = $data->total_count;
        }
        return $totalCount;

    }

    public function queryUserInviteRecord(PageParams $params): ?array
    {
        $mysql = $this->select_mysql_db('main');
        $offset = $params->index * $params->page_size;
        return $mysql->select('select * from user_invite_record where user_id =? order by id desc limit ?,?', [$params->user_id, $offset, $params->page_size], UserInviteRecordDTO::class);
    }


}