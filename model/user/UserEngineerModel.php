<?php

namespace model\user;

use facade\request\admin\SearchUserListParams;
use facade\request\engineer\QueryEngineerListParams;
use model\user\dto\UserEngineerDTO;

class UserEngineerModel extends \FUR_Model
{
    public static $_instance;

    const EXPIRE_TIME = 86400;


    const USER_REDIS_MODULE = 'user';

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function queryEngineerInfoByPhone(string $phone): ?UserEngineerDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select * from user_engineer where phone_num = ? limit 1", [$phone], UserEngineerDTO::class);
    }

    public function searchEngineer(?string $keyword): ?array
    {
        $mysql = $this->select_mysql_db('main');
        $keyword = '%' . $keyword . '%';
        return $mysql->select("select * from user_engineer where phone_num like ? or real_name like ? limit 20", [$keyword, $keyword], UserEngineerDTO::class);
    }

    public function getEngineerInfo(int $engineerId): ?UserEngineerDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select * from user_engineer where  id = ? limit 1", [$engineerId], UserEngineerDTO::class);
    }

    public function updateEngineerInfo(UserEngineerDTO $UserEngineerDTO): int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->update('user_engineer', $UserEngineerDTO, ['id' => $UserEngineerDTO->id]);
    }

    public function deleteEngineer(UserEngineerDTO $UserEngineerDTO): int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->update('user_engineer', $UserEngineerDTO, ['id' => $UserEngineerDTO->id]);
    }


    public function insertEngineerInfo(UserEngineerDTO $UserEngineerDTO): int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert('user_engineer', $UserEngineerDTO);
    }


    public function queryEngineerList(QueryEngineerListParams $params): ?array
    {
        $mysql = $this->select_mysql_db('main');
        $index = $params->index;
        $pageSize = $params->page_size;
        $offset = $index * $pageSize;
        $keyword = $params->keyword;

        if ($keyword) {
            $keyword = '%' . $keyword . '%';
            $data = $mysql->select('select * from user_engineer where (phone_num like ? or real_name like ?) order by id desc  limit ?,?', [$keyword, $keyword, $offset, $pageSize], UserEngineerDTO::class);

        } else {
            $data = $mysql->select('select * from user_engineer order by id desc  limit ?,?', [$offset, $pageSize], UserEngineerDTO::class);
        }
        return $data;
    }

    public function queryEngineerCount(QueryEngineerListParams $params): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $keyword = $params->keyword;
        if ($keyword) {
            $keyword = '%' . $keyword . '%';
            $data = $mysql->selectOne('select  count(*) as row_count from user_engineer where (phone_num like ? or real_name like ?) ', [$keyword, $keyword]);
        } else {
            $data = $mysql->selectOne('select count(*) as row_count from user_engineer where  1=1', []);
        }
        $row_count = 0;
        if ($data != null) {
            $row_count = $data->row_count;
        }
        return $row_count;
    }


}