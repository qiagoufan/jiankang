<?php


namespace model\user;



use model\user\dto\UserShippingAddressDTO;

class UserShippingAddressModel extends \FUR_Model
{
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function insertRecord(UserShippingAddressDTO $shippingAddressDTO): int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert('user_shipping_address', $shippingAddressDTO);
    }

    public function queryShippingAddressList(int $user_id): ?array
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->select("select * from user_shipping_address where user_id =? order by is_default desc,id desc ", [$user_id], UserShippingAddressDTO::class);
    }


    public function queryUserDefaultAddress(int $user_id): ?UserShippingAddressDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select * from user_shipping_address where user_id =? and is_default=1 limit 1", [$user_id], UserShippingAddressDTO::class);
    }

    public function queryShippingAddressDetail(int $user_id, int $id): ?UserShippingAddressDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select * from user_shipping_address where id =? and user_id =? limit 1 ", [$id, $user_id], UserShippingAddressDTO::class);
    }

    public function updateRecord(UserShippingAddressDTO $shippingAddressDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $args = ['id' => $shippingAddressDTO->id, 'user_id' => $shippingAddressDTO->user_id];
        return $mysql->update("user_shipping_address", $shippingAddressDTO, $args);
    }

    public function updateUserShippingAddressToNotDefault(int $user_id): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $args = ['user_id' => $user_id];
        $data = ['is_default' => 0];
        return $mysql->update("user_shipping_address", $data, $args);
    }

    public function deleteRecord(int $user_id, int $id): ?bool
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->delete("user_shipping_address ", ['id' => $id, 'user_id' => $user_id]);
    }
}