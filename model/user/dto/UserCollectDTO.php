<?php 
 
namespace model\user\dto; 
 
class UserCollectDTO { 

	 //主键
	 public $id;
	 //用户id
	 public $user_id;
	 //收藏的id
	 public $collect_biz_id;
	 //收藏类型
	 public $collect_biz_type;
	 //创建时间
	 public $created_timestamp;
	 //更新时间
	 public $updated_timestamp;
 
 
 }