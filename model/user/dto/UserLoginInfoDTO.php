<?php 
 
namespace model\user\dto; 
 
class UserLoginInfoDTO { 

	 //主键
	 public $id;
	 //用户id
	 public $user_id;
	 //登录类型
	 public $login_type;
	 //登录号
	 public $login_account;
	 //登录信息
	 public $login_certificate;
	 //创建时间
	 public $created_timestamp;
	 //更新时间
	 public $updated_timestamp;
 
 
 }