<?php


namespace model\user\dto;


use facade\request\user\LoginByPhoneParams;
use facade\request\user\RegisterParams;

class UserExtraDTO
{
    const FIELD_USER_FEED_LATEST_VISIT_TIME = 'feed_visit_time';
    const FIELD_FOCUS_COUNT = "user_focus_count";

}