<?php 
 
namespace model\user\dto; 
 
class UserFeedbackDTO { 

	 //主键
	 public $id;
	 //用户id
	 public $user_id;
	 //平台
	 public $platform;
	 //联系方式
	 public $contact;
	 //客户端版本
	 public $app_version;
	 //反馈内容
	 public $feedback_content;
	 //反馈图片列表
	 public $feedback_imgs;
	 //反馈类型
	 public $feedback_type;
	 //反馈状态
	 public $feedback_status;
	 //创建时间
	 public $created_timestamp;
	 //更新时间
	 public $updated_timestamp;
 
 
 }