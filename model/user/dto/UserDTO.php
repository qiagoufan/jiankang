<?php 
 
namespace model\user\dto; 
 
class UserDTO { 

	 //用户id
	 public $id;
	 //1:普通用户2.明星用户3.管理员
	 public $user_type;
	 //性别 0其他, 1:男, 2女
	 public $sex;
	 //真实姓名
	 public $real_name;
	 //昵称
	 public $nick_name;
	 //头像
	 public $avatar;
	 //背景
	 public $background;
	 //生日
	 public $birthday;
	 //用户状态1:正常,0:已注销
	 public $status;
	 //地址
	 public $location;
	 //注册时间
	 public $created_timestamp;
	 //更新时间
	 public $updated_timestamp;
	 //上次登录时间
	 public $latest_login_timestamp;
	 //上次登录ip
	 public $latest_login_ip;
	 //平台
	 public $platform;
	 //省
	 public $province;
	 //市
	 public $city;
	 //区
	 public $area;
	 //设备号
	 public $device_id;
	 //更新昵称的时间
	 public $update_nick_name_timestamp;
	 //????openid
	 public $socialite_open_id;
	 //??????
	 public $socialite_provider;
	 //用户名
	 public $user_name;
	 //密码
	 public $password;
	 //身高
	 public $height;
	 //体重
	 public $weight;
	 //婚姻情况
	 public $marital_status;
	 //职业
	 public $profession;
	 //身份证号
	 public $id_num;
	 //文化程度
	 public $education;
	 //邀请码
	 public $invite_code;
	 //医保类型
	 public $insurance_type;
	 //微信是否同步
	 public $syn_status;
	 //用户备注
	 public $user_remark;
 
 
 }