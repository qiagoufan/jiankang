<?php 
 
namespace model\user\dto; 
 
class UserThirdPartyInfoDTO { 

	 //主键
	 public $id;
	 //open_id
	 public $open_id;
	 //weixin,qq,weibo,apple
	 public $source;
	 //用户id
	 public $user_id;
	 //创建时间
	 public $created_timestamp;
	 //调用接口凭证
	 public $access_token;
	 //用户统一标识
	 public $unionid;
	 //用户刷新 token
	 public $refresh_token;
	 //用户刷新 token过期时间
	 public $expires_in;
	 //最后更新时间
	 public $updated_timestamp;
 
 
 }