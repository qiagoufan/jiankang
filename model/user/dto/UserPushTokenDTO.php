<?php 
 
namespace model\user\dto; 
 
class UserPushTokenDTO { 

	 //主键
	 public $id;
	 //用户id
	 public $user_id;
	 //手机号
	 public $platform;
	 //设备id
	 public $device_id;
	 //推送token
	 public $push_token;
	 //品牌
	 public $brand;
	 //注册时间
	 public $created_timestamp;
	 //更新时间
	 public $updated_timestamp;
 
 
 }