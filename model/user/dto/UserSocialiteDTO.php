<?php 
 
namespace model\user\dto; 
 
class UserSocialiteDTO { 

	 //主键
	 public $id;
	 //User ID.
	 public $user_id;
	 //Provider union ID.
	 public $open_id;
	 //Provider Type.
	 public $provider;
	 //创建时间
	 public $created_timestamp;
	 //更新时间
	 public $updated_timestamp;
 
 
 }