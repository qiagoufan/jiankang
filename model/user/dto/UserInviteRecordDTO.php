<?php 
 
namespace model\user\dto; 
 
class UserInviteRecordDTO { 

	 //主键
	 public $id;
	 //用户id
	 public $user_id;
	 //邀请的用户
	 public $invite_user_id;
	 //
	 public $created_timestamp;
	 //
	 public $updated_timestamp;
 
 
 }