<?php 
 
namespace model\user\dto; 
 
class UserTokenDTO { 

	 //主键
	 public $id;
	 //user_id
	 public $user_id;
	 //刷新token
	 public $refresh_token;
	 //过期时间
	 public $expire_timestamp;
	 //设备id
	 public $device_id;
	 //创建时间
	 public $create_timestamp;
 
 
 }