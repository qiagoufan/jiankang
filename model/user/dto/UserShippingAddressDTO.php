<?php 
 
namespace model\user\dto; 
 
class UserShippingAddressDTO { 

	 //主键
	 public $id;
	 //地址
	 public $address;
	 //用户id
	 public $user_id;
	 //收货人
	 public $receiver;
	 //省
	 public $province;
	 //市
	 public $city;
	 //区
	 public $area;
	 //是否是默认
	 public $is_default;
	 //联系电话
	 public $phone;
	 //创建时间
	 public $created_timestamp;
	 //更新时间
	 public $updated_timestamp;
 
 
 }