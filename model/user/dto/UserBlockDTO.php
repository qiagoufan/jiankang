<?php 
 
namespace model\user\dto; 
 
class UserBlockDTO { 

	 //primary key
	 public $id;
	 //用户id
	 public $user_id;
	 //被屏蔽的用户id
	 public $block_user_id;
	 //创建时间戳
	 public $created_timestamp;
	 //更新时间戳
	 public $updated_timestamp;
 
 
 }