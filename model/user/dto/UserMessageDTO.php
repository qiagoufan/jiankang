<?php 
 
namespace model\user\dto; 
 
class UserMessageDTO { 

	 //主键
	 public $id;
	 //消息图片
	 public $image;
	 //标题
	 public $title;
	 //副标题
	 public $subtitle;
	 //链接地址
	 public $link;
	 //创建时间
	 public $created_timestamp;
 
 
 }