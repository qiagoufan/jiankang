<?php 
 
namespace model\user\dto; 
 
class UserFollowDTO { 

	 //主键
	 public $id;
	 //用户id
	 public $user_id;
	 //关注的id
	 public $target_id;
	 //关注的业务类型
	 public $target_type;
	 //关注的时间
	 public $follow_timestamp;
 
 
 }