<?php 
 
namespace model\user\dto; 
 
class UserEngineerDTO { 

	 //用户id
	 public $id;
	 //
	 public $engineer_type;
	 //性别 0其他, 1:男, 2女
	 public $sex;
	 //真实姓名
	 public $real_name;
	 //头像
	 public $avatar;
	 //生日
	 public $birthday;
	 //用户状态1:正常,0:已注销
	 public $status;
	 //地址
	 public $location;
	 //注册时间
	 public $created_timestamp;
	 //更新时间
	 public $updated_timestamp;
	 //上次登录时间
	 public $latest_login_timestamp;
	 //上次登录ip
	 public $latest_login_ip;
	 //平台
	 public $platform;
	 //省
	 public $province;
	 //市
	 public $city;
	 //区
	 public $area;
	 //设备号
	 public $device_id;
	 //手机号
	 public $phone_num;
	 //提现卡号
	 public $withdraw_card_num;
	 //提现姓名
	 public $withdraw_card_real_name;
    //提现卡号
    public $withdraw_card_bank;
 
 
 }