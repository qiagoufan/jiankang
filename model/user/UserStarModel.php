<?php

namespace model;

use model\dto\UserDTO;
use model\dto\UserStarDTO;
use model\dto\UserTokenDTO;

class UserStarModel extends \FUR_Model
{
    public static $_instance;

    const DEFAULT_EXPIRE_TIME = 86400 * 10;


    const USER_REDIS_MODULE = 'user';

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function getStarInfo(int $starId): ?UserStarDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select * from user_star where id = ? limit 1", [$starId], UserStarDTO::class);
    }

    public function getStarInfoCache(int $starId): ?UserStarDTO
    {

        $redis = $this->get_redis(self::USER_REDIS_MODULE);
        $key = $this->buildStarInfoKey($starId);
        $starInfoStr = $redis->get($key);
        if ($starInfoStr == null) {
            return null;
        }

        $starInfoJSonObj = json_decode($starInfoStr);
        $userStarDTO = new UserStarDTO();
        \FUR_Core::copyProperties($starInfoJSonObj, $userStarDTO);
        return $userStarDTO;
    }

    public function setStarInfoCache(UserStarDTO $userStarDTO): bool
    {
        if ($userStarDTO == null) {
            return null;
        }
        $redis = $this->get_redis(self::USER_REDIS_MODULE);
        $key = $this->buildStarInfoKey($userStarDTO->id);
        $starInfoStr = json_encode($userStarDTO);
        $redis->set($key, $starInfoStr);
        $redis->expire($key, self::DEFAULT_EXPIRE_TIME);
        return true;
    }

    public function cleanStarInfoCache(int $starId): bool
    {

        $redis = $this->get_redis(self::USER_REDIS_MODULE);
        $key = $this->buildStarInfoKey($starId);
        $redis->del($key);
        return true;
    }

    public function insertRecord(UserStarDTO $userStarDTO): int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert('user_star', $userStarDTO);
    }

    public function updateUserStarDTO(UserStarDTO $userStarDTO): int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->update('user_star', $userStarDTO, ['id' => $userStarDTO->id]);
    }

    public function searchStar(string $starName, int $offset, int $pageSize): ?array
    {
        $mysql = $this->select_mysql_db('main');
        $starName = '%' . $starName . '%';
        return $mysql->select("select  *  from user_star where status=1 and star_name like ? order by id desc limit ?,?", [$starName, $offset, $pageSize], UserStarDTO::class);
    }

    public function queryStarByName(string $starName): ?UserStarDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select  *  from user_star where status=1 and star_name = ? limit 1", [$starName], UserStarDTO::class);
    }

    private function buildStarInfoKey(int $starId): string
    {
        return 'user_star_info:' . $starId;
    }


    public function queryStarList(int $offset, int $pageSize, ?string $starName, ?int $ipType): ?array
    {
        $mysql = $this->select_mysql_db('main');
        if ($starName != null) {
            $starName = '%' . $starName . '%';
            return $mysql->select('select * from  user_star  where status=1 and ip_type=? and (star_name  like ? or official_description like ?) order by id desc limit ?,? ',
                [$ipType, $starName, $starName, $offset, $pageSize],
                UserStarDTO::class);
        }
        return $mysql->select("select  *  from user_star where status=1 and  ip_type=? order by id desc limit ?,?", [$ipType, $offset, $pageSize], UserStarDTO::class);
    }

    public function queryAllStarList(int $offset, int $pageSize, ?string $starName): ?array
    {
        $mysql = $this->select_mysql_db('main');
        if ($starName != null) {
            $starName = '%' . $starName . '%';

            return $mysql->select('select * from  user_star  where  star_name  like ?  order by id desc  limit ?,? ', [$starName, $offset, $pageSize], UserStarDTO::class);

        }
        return $mysql->select("select  *  from user_star   order by id  desc  limit ?,?", [$offset, $pageSize], UserStarDTO::class);
    }


    public function queryStarTotalCount(?string $keyword = null): ?int
    {
        $mysql = $this->select_mysql_db('main');
        if ($keyword != null) {
            $keyword = '%' . $keyword . '%';
            $data = $mysql->selectOne("select count(*) as  row_count from  user_star  where  star_name  like ? ", [$keyword]);
        } else {
            $data = $mysql->selectOne("select  count(*) as  row_count from user_star ");
        }
        $row_count = null;
        if ($data) {
            $row_count = $data->row_count;
        }
        return $row_count;
    }


    public function deleteStarFromDB(int $starId): ?bool
    {
        $mysql = $this->select_mysql_db('main');
        $data = ['status' => 0, 'updated_timestamp' => time()];
        $args = ['id' => $starId];
        return $mysql->update('user_star', $data, $args);

    }


    private function buildDiscussionHeatKey(string $bizType): string
    {
        return 'user_discussion_heat:' . $bizType;
    }


    public function getDiscussionHeatValue(string $bizType, int $starId): ?string
    {

        $key = $this->buildDiscussionHeatKey($bizType);
        $redis = $this->get_redis(self::USER_REDIS_MODULE);
        return $redis->ZSCORE($key, $starId);

    }


    public function getDiscussionHeatRank(int $starId, string $bizType)
    {
        $key = $this->buildDiscussionHeatKey($bizType);
        $redis = $this->get_redis(self::USER_REDIS_MODULE);
        $data = $redis->zrevrank($key, $starId);
        if ($data === false) {
            return -1;
        }
        return $data + 1;
    }


    public function updateDiscussionHeat(int $starId, string $bizType, int $score)
    {

        $key = $this->buildDiscussionHeatKey($bizType);
        $redis = $this->get_redis(self::USER_REDIS_MODULE);
        return $redis->zincrby($key, $score, $starId);
    }


    public function getUserStarDiscussionList(string $bizType, int $start, int $pageSize): ?array
    {
        $key = $this->buildDiscussionHeatKey($bizType);
        $redis = $this->get_redis(self::USER_REDIS_MODULE);
        return $redis->zrevrange($key, $start, $start + $pageSize);
    }

}