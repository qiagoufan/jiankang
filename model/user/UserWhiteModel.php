<?php


namespace model\user;

use model\user\dto\UserWhiteDTO;

class UserWhiteModel extends \FUR_Model
{
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    /**
     * @param int $userId
     * @return UserWhiteDTO|null
     * 查询记录
     */
    public function queryRecord(int $userId): ?UserWhiteDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne('select * from user_whitelist  where user_id=? limit 1', [$userId], UserWhiteDTO::class);
    }


    /**
     * 插入记录
     * @param UserWhiteDTO $userWhiteListDTO
     * @return int|null
     */
    public function insertRecord($userWhiteListDTO): ?int
    {

        $mysql = $this->select_mysql_db('main');
        return $mysql->insert('user_whitelist', $userWhiteListDTO);
    }


    /**
     * @param int $userId
     * @return bool|null
     * 删除记录
     */
    public function deleteRecord(int $recordId): ?bool
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->delete('user_whitelist', ['id' => $recordId]);
    }


    /**
     * @param int $offset
     * @param int $pageSize
     * @return array|null
     * 获取白名单
     */
    public function queryUserWhiteList(int $offset, int $pageSize): ?array
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->select('select * from user_whitelist limit ?,?', [$offset, $pageSize], UserWhiteDTO::class);
    }
}