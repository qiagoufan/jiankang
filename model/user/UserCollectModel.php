<?php

namespace model;

use model\dto\UserCollectDTO;
use model\dto\UserStarDTO;

class UserCollectModel extends \FUR_Model
{
    public static $_instance;


    const USER_REDIS_MODULE = 'user';

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function insertRecord(UserCollectDTO $userCollectDTO): int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert('user_collect', $userCollectDTO);
    }

    public function queryCollectCount(UserCollectDTO $userCollectDTO): int
    {
        return 0;
    }

    public function saveRecordCache(?UserCollectDTO $userCollectDTO): ?bool
    {
        if ($userCollectDTO == null) {
            return null;
        }
        $redis = $this->get_redis(self::USER_REDIS_MODULE);
        $key = $this->buildUserCollectKey($userCollectDTO->user_id);
        $member = $this->buildUserCollectMember($userCollectDTO->collect_biz_type, $userCollectDTO->collect_biz_id);
        $redis->sadd($key, $member);
        return true;
    }

    public function hasCollect(?UserCollectDTO $userCollectDTO): bool
    {
        if ($userCollectDTO == null) {
            return null;
        }
        $redis = $this->get_redis(self::USER_REDIS_MODULE);
        $key = $this->buildUserCollectKey($userCollectDTO->user_id);
        $member = $this->buildUserCollectMember($userCollectDTO->collect_biz_type, $userCollectDTO->collect_biz_id);
        return $redis->sismember($key, $member);
    }

    public function removeFromCache(?UserCollectDTO $userCollectDTO)
    {
        if ($userCollectDTO == null) {
            return null;
        }
        $redis = $this->get_redis(self::USER_REDIS_MODULE);
        $key = $this->buildUserCollectKey($userCollectDTO->user_id);
        $member = $this->buildUserCollectMember($userCollectDTO->collect_biz_type, $userCollectDTO->collect_biz_id);
        return $redis->srem($key, $member);
    }

    public function deleteRecord(int $userId, int $collectBizId, string $collectBizType): int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->delete('user_collect', ['user_id' => $userId, 'collect_biz_id' => $collectBizId, 'collect_biz_type' => $collectBizType]);
    }


    public function queryCollectById(int $userId, int $collectBizId, string $collectBizType): ?UserCollectDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne('select * from  user_collect where user_id = ? and collect_biz_id =? and collect_biz_type =? ', [$userId, $collectBizId, $collectBizType], UserCollectDTO::class);
    }

    public function queryCollectList(int $userId, int $offset, int $pageSize): ?array
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->select('select * from  user_collect where user_id = ?  order by id desc limit ?,?', [$userId, $offset, $pageSize], UserCollectDTO::class);
    }

    public function updateCollectCount(int $bizId, string $bizType, int $number): bool
    {
        $key = $this->buildCollectCountKey($bizType, $bizId);
        $redis = $this->get_redis(self::USER_REDIS_MODULE);
        return $redis->incrby($key, $number);

    }

    public function getCollectCount(int $bizId, string $bizType): int
    {
        $key = $this->buildCollectCountKey($bizType, $bizId);
        $redis = $this->get_redis(self::USER_REDIS_MODULE);
        return (int)$redis->get($key);

    }

    private function buildUserCollectKey(int $userId): string
    {
        return 'user_collect_set:' . $userId;
    }

    private function buildCollectCountKey(string $bizType, int $bizId): string
    {
        return 'collect_count:' . $bizType . ":" . $bizId;
    }

    private function buildUserCollectMember(string $bizType, int $bizId): string
    {
        return $bizType . '|' . $bizId;
    }


}