<?php

namespace model;

use model\dto\UserMessageDTO;
use facade\request\PageParams;

class UserMessageModel extends \FUR_Model
{

    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function __construct()
    {
        $redis = $this->get_redis('unlogin_message_redis');
        $this->redis = $redis;
    }


    public function queryMessageList(int $userId, int $offset, int $pageSize): ?array
    {

        $mysql = $this->select_mysql_db('main');
        $this->setUserReadTime($userId);
        return $mysql->select("select * from user_message order by id desc  limit ?,?", [$offset, $pageSize], UserMessageDTO::class);

    }


    public function getUserUnreadMessage(int $userId): ?int
    {

        $userLatestReadTime = $this->getUserReadTime($userId);
        $mysql = $this->select_mysql_db('main');
        if ($userLatestReadTime) {
            $data = $mysql->selectOne("select count(*) as row_count from user_message where created_timestamp > ? ", [$userLatestReadTime]);
        } else {
            $data = $mysql->selectOne("select count(*) as row_count from user_message");
        }
        $rowCount = 0;
        if ($data != null) {
            $rowCount = $data->row_count;
        }

        return $rowCount;

    }


    public function getUserReadTime($userId)
    {
        $key = "userExtra:$userId";
        $result = $this->redis->hget($key, 'userMsgReadTime');
        return $result;

    }


    public function setUserReadTime($userId)
    {
        $key = "userExtra:$userId";
        $this->redis->hset($key, 'userMsgReadTime', time());
    }


}






