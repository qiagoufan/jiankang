<?php

namespace model\user;

use facade\request\admin\SearchUserListParams;
use facade\request\user\QueryUserListParams;
use model\user\dto\UserDTO;

class UserModel extends \FUR_Model
{
    public static $_instance;

    const EXPIRE_TIME = 86400;


    const USER_REDIS_MODULE = 'user';

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function queryUserIdByPhone(string $phone): int
    {
        $userId = 0;
        $mysql = $this->select_mysql_db('main');
        $userDTO = $mysql->selectOne("select id from user where phone = ? limit 1", array($phone), UserDTO::class);
        if ($userDTO != null) {
            $userId = $userDTO->id;
        }
        return $userId;
    }

    public function queryUserIdByInviteCode(string $inviteCode): int
    {
        $userId = 0;
        $mysql = $this->select_mysql_db('main');
        $userDTO = $mysql->selectOne("select id from user where invite_code = ? limit 1", [$inviteCode], UserDTO::class);
        if ($userDTO != null) {
            $userId = $userDTO->id;
        }
        return $userId;
    }

    public function queryUserIdBySocialite(?string $socialiteOpenId, ?string $provider): int
    {
        $userId = 0;
        $mysql = $this->select_mysql_db('main');
        $userDTO = $mysql->selectOne("select id from user where socialite_open_id = ? and socialite_provider =? and status = ? limit 1", array($socialiteOpenId, $provider, \Config_Const::USER_STATUS_VALID), UserDTO::class);
        if ($userDTO != null) {
            $userId = $userDTO->id;
        }
        return $userId;
    }

    public function deleteUserById(int $userId): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $data = ['status' => 0, 'updated_timestamp' => time()];
        return $mysql->update('user', $data, ['id' => $userId]);
    }

    public function queryAdminInfoByPhone(string $phone): ?UserDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select id from user where phone = ? and user_type=? limit 1", array($phone, \Config_Const::USER_TYPE_ADMIN), UserDTO::class);

    }

    public function getUserInfo(int $userId): ?UserDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select * from user where id = ? limit 1", [$userId], UserDTO::class);
    }

    public function getUserInfoByNickName(string $nickName): ?UserDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select * from user where nick_name = ? limit 1", [$nickName], UserDTO::class);
    }

    public function getUserCache(int $userId): ?UserDTO
    {
        $redis = $this->get_redis(self::USER_REDIS_MODULE);
        $key = $this->buildUserInfoKey($userId);
        $userInfoStr = $redis->get($key);
        if ($userInfoStr == null) {
            return null;
        }

        $userInfoJsonObj = json_decode($userInfoStr);
        $userDTO = new UserDTO();
        \FUR_Core::copyProperties($userInfoJsonObj, $userDTO);
        return $userDTO;
    }

    public function saveUserCache(UserDTO $userDTO): ?bool
    {
        $redis = $this->get_redis(self::USER_REDIS_MODULE);
        $key = $this->buildUserInfoKey($userDTO->id);
        $userInfoStr = json_encode($userDTO);
        $redis->set($key, $userInfoStr);
        $redis->EXPIRE($key, self::EXPIRE_TIME);
        return true;
    }

    public function deleteUserCache(int $userId): ?bool
    {
        $redis = $this->get_redis(self::USER_REDIS_MODULE);
        $key = $this->buildUserInfoKey($userId);
        $redis->del($key);
        return true;
    }

    public function updateUserInfo(UserDTO $userDTO): int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->update('user', $userDTO, ['id' => $userDTO->id]);
    }

    public function deleteUser(UserDTO $userDTO): int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->update('user', $userDTO, ['id' => $userDTO->id]);
    }


    public function insertUserRecord(UserDTO $userDTO): int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert('user', $userDTO);
    }

    private function buildUserInfoKey(int $userId): string
    {
        return 'user_info:' . $userId;
    }


    public function queryUserList(QueryUserListParams $params): ?array
    {
        $offset = $params->index * $params->page_size;
        $pageSize = $params->page_size;
        $mysql = $this->select_mysql_db('main');
        $args = [$offset, $pageSize];
        $sqlCondition = 'where status!=0 ';
        if ($params->keyword) {
            $keyword = '%' . $params->keyword . '%';
            $sqlCondition .= ' and (nick_name like ? or real_name like ?)';
            $args = array_merge([$keyword, $keyword], $args);
        }

        return $mysql->select('select * from user ' . $sqlCondition . " order by id desc  limit ?,?", $args, UserDTO::class);
    }

    /**
     * @return int|null
     * 查询用户总数
     */
    public function queryUserTotalCount(QueryUserListParams $params): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $args = [];
        $sqlCondition = 'where status!=0 ';
        if ($params->keyword) {
            $keyword = '%' . $params->keyword . '%';
            $sqlCondition .= ' and (nick_name like ? or real_name like ?)';
            $args = array_merge([$keyword, $keyword], $args);
        }

        $data = $mysql->selectOne('select count(*) as total_count from user ' . $sqlCondition, $args);
        $total_count = 0;
        if ($data != null) {
            $total_count = $data->total_count;
        }
        return $total_count;
    }


    /**
     * @return int|null
     * 随机获取马甲用户id
     */
    public function getMajiaUserId(): ?int
    {
        //马甲用户90000-909917
        $mysql = $this->select_mysql_db('main');
        $data = $mysql->selectOne('select id from user where id between 900000 and 909910 order by rand()  limit 1', []);
        if ($data != null) {
            $userId = $data->id;
        } else {
            $userId = 0;
        }
        return $userId;
    }


    public function getMaxUserId(): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $data = $mysql->selectOne('select  max(id) as user_id from user');
        if ($data != null) {
            $userId = $data->user_id;
        } else {
            $userId = 1000000;
        }
        return $userId;
    }


    public function blockUser(int $userId, int $blockUserId): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->query('INSERT INTO user_block (user_id,block_user_id,created_timestamp) 
                                    VALUES(?,?,?) ON DUPLICATE KEY UPDATE updated_timestamp=?', [$userId, $blockUserId, time(), time()]);
    }

    public function getBlockUserIdList(int $userId): array
    {
        $mysql = $this->select_mysql_db('main');
        $data = $mysql->select('select * from user_block where user_id =? order by id desc limit 100', [$userId]);
        $userIdList = [];
        if (!empty($data)) {
            foreach ($data as $record) {
                array_push($userIdList, intval($record->block_user_id));
            }
        }

        return $userIdList;
    }

    public function getBlockUserCount(int $userId): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $data = $mysql->selectOne('select count(*) as total from user_block where user_id =? ', [$userId]);
        return $data->total;
    }

    public function blockUserCache(int $userId, int $blockUserId)
    {
        $redis = $this->get_redis(self::USER_REDIS_MODULE);
        $key = $this->buildUserBlockKey($userId);
        return $redis->sadd($key, $blockUserId);
    }

    public function unblockUser(int $userId, int $blockUserId): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->delete('user_block', ['user_id' => $userId, 'block_user_id' => $blockUserId]);
    }


    public function unblockUserCache(int $userId, int $blockUserId)
    {
        $redis = $this->get_redis(self::USER_REDIS_MODULE);
        $key = $this->buildUserBlockKey($userId);
        return $redis->srem($key, $blockUserId);
    }

    public function isBlock(int $userId, int $blockUserId): bool
    {
        $redis = $this->get_redis(self::USER_REDIS_MODULE);
        $key = $this->buildUserBlockKey($userId);
        return (bool)$redis->sismember($key, $blockUserId);
    }

    private function buildUserBlockKey(int $userId): string
    {
        return 'user_block:' . $userId;
    }
}