<?php

namespace model;

use model\dto\UserDTO;
use model\dto\UserSocialiteDTO;
use model\dto\UserTokenDTO;

class UserSocialiteModel extends \FUR_Model
{
    public static $_instance;


    const USER_REDIS_MODULE = 'user';

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function queryUserIdByOpenId(string $openId, string $provider): ?int
    {
        $userId = null;
        $mysql = $this->select_mysql_db('main');
        $userSocialiteDTO = $mysql->selectOne("select * from user_socialite where open_id = ? and provider =? limit 1", array($openId, $provider), UserSocialiteDTO::class);

        if ($userSocialiteDTO != null) {
            $userId = $userSocialiteDTO->user_id;
        }
        return $userId;
    }

    public function deleteRecordByOpenId(string $openId, string $provider): ?int
    {
        $userId = null;
        $mysql = $this->select_mysql_db('main');
        $args = ['open_id' => $openId, 'provider' => $provider];
        return $mysql->delete('user_socialite', $args);
    }

    public function deleteRecordByUserId(int $userId): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $args = ['user_id' => $userId];
        return $mysql->delete('user_socialite', $args);
    }

    public function insertUserSocialite(UserSocialiteDTO $userSocialiteDTO): int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert('user_socialite', $userSocialiteDTO);
    }


    public function setWeixinMiniSessionKeyCache(string $openid, string $sessionKey)
    {
        $redis = $this->get_redis(self::USER_REDIS_MODULE);
        $key = 'wx_mini:session_key:' . $openid;
        $redis->set($key, $sessionKey);
        $redis->EXPIRE($key, 86400 * 3);
        return true;
    }

    public function getWeixinMiniSessionKeyCache(string $openid)
    {
        $redis = $this->get_redis(self::USER_REDIS_MODULE);
        $key = 'wx_mini:session_key:' . $openid;
        return $redis->get($key);
    }

    public function queryUserSocialiteInfo(string $provider, int $userId): ?UserSocialiteDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select * from user_socialite where user_id = ? and provider =? limit 1", array($userId, $provider), UserSocialiteDTO::class);
    }


}