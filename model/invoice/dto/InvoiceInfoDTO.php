<?php 
 
namespace model\invoice\dto; 
 
class InvoiceInfoDTO { 

	 //
	 public $id;
	 //公司id
	 public $company_id;
	 //总价
	 public $total_amount;
	 //总数
	 public $total_count;
	 //开票状态
	 public $invoicing_status;
	 //创建时间
	 public $created_timestamp;
	 //更新时间
	 public $updated_timestamp;
 
 
 }