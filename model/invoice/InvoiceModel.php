<?php


namespace model\invoice;


use facade\request\invoice\QueryInvoiceListParams;
use model\invoice\dto\InvoiceInfoDTO;
use model\invoice\dto\InvoiceOrderListDTO;

class InvoiceModel extends \FUR_Model
{
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function createInvoiceInfo(InvoiceInfoDTO $invoiceInfoDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert('invoice_info', $invoiceInfoDTO);
    }


    public function createInvoiceOrder(InvoiceOrderListDTO $invoiceOrderListDTO): ?int
    {

        $mysql = $this->select_mysql_db('main');
        return $mysql->insert('invoice_order_list', $invoiceOrderListDTO);
    }

    public function queryInvoiceInfo(int $invoiceId): ?InvoiceInfoDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne('select * from invoice_info where id =?', [$invoiceId], InvoiceInfoDTO::class);
    }

    public function queryInvoiceList(QueryInvoiceListParams $params): ?array
    {
        $mysql = $this->select_mysql_db('main');
        $offset = $params->index * $params->page_size;
        $sql = 'select * from invoice_info ';
        $condition = 'where 1=1 ';

        if ($params->company_id) {
            $condition .= ' and company_id =' . $params->company_id;
        }
        if ($params->invoicing_status) {
            $condition .= ' and invoicing_status =' . $params->invoicing_status;
        }

        return $mysql->select($sql . $condition . ' order by id desc  limit ?,?', [$offset, $params->page_size], InvoiceInfoDTO::class);
    }

    public function queryInvoiceOrderList(?int $invoiceId): ?array
    {

        $mysql = $this->select_mysql_db('main');
        return $mysql->select('select * from invoice_order_list  where  invoice_id =?  order by id desc  ', [$invoiceId], InvoiceOrderListDTO::class);
    }


    public function updateInvoiceInfo(InvoiceInfoDTO $invoiceInfoDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->update('invoice_info', $invoiceInfoDTO, ['id' => $invoiceInfoDTO->id]);

    }

    public function queryInvoiceCount(QueryInvoiceListParams $params): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $total_count = 0;
        $sql = 'select count(*) as total_count from invoice_info ';
        $condition = 'where 1=1 ';
        if ($params->company_id) {
            $condition .= ' and company_id =' . $params->company_id;
        }
        if ($params->invoicing_status) {
            $condition .= ' and invoicing_status =' . $params->invoicing_status;
        }


        $data = $mysql->selectOne($sql . $condition);
        if ($data) {
            $total_count = $data->total_count;
        }
        return $total_count;
    }
}