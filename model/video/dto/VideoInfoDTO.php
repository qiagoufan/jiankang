<?php 
 
namespace model\video\dto; 
 
class VideoInfoDTO { 

	 //主键
	 public $id;
	 //上传者id
	 public $user_id;
	 //视频高度
	 public $video_height;
	 //视频宽度
	 public $video_width;
	 //时长
	 public $video_duration;
	 //封面图
	 public $cover;
	 //文件大小
	 public $file_size;
	 //视频文件链接
	 public $video_link;
	 //转码状态
	 public $transcoding_status;
	 //审核状态
	 public $audit_status;
	 //创建时间
	 public $created_timestamp;
	 //更新时间
	 public $updated_timestamp;
 
 
 }