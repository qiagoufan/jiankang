<?php 
 
namespace model\message\dto; 
 
class MessageInteractiveDTO { 

	 //主键
	 public $id;
	 //发送用户id
	 public $send_user_id;
	 //接收用户id
	 public $receive_user_id;
	 //消息图片
	 public $image;
	 //业务id
	 public $biz_id;
	 //业务类型
	 public $biz_type;
	 //消息状态 1.已读0.未读
	 public $read_status;
	 //创建时间
	 public $created_timestamp;
	 //更新时间
	 public $updated_timestamp;
	 //feed id
	 public $feed_id;
 
 
 }