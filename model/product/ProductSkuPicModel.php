<?php

namespace model;

use model\dto\ProductGalleryDTO;
use model\dto\ProductSkuDTO;

class ProductSkuPicModel extends \FUR_Model
{
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function querySkuPicList(int $skuId, int $picCategory): ?array
    {
        $mysql = $this->select_mysql_db('main');
        //过滤查询参数
        $props = $this->getFieldsByClass(ProductGalleryDTO::class);
        $props = implode(',', $props);
        return $mysql->select("select  " . $props . "  from product_sku_pic where  sku_id=? and category = ?", [$skuId, $picCategory], ProductGalleryDTO::class);

    }

}