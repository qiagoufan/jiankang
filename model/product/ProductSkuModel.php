<?php

namespace model\product;

use lib\TimeHelper;
use model\product\dto\ProductSkuDTO;

class ProductSkuModel extends \FUR_Model
{
    public static $_instance;

    const PRODUCT_REDIS_MODULE = 'product';

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function insertRecord(ProductSkuDTO $productSkuDTO): int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert('product_sku', $productSkuDTO);
    }


    private function buildProductSkuInfoDTOKey(int $skuId): string
    {
        return 'product_sku_info:' . $skuId;
    }


    public function updateProductSku(ProductSkuDTO $productSkuDTO): bool
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->update('product_sku', $productSkuDTO, ['item_id' => $productSkuDTO->item_id]);
    }

    public function updateProductSkuById(ProductSkuDTO $productSkuDTO): bool
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->update('product_sku', $productSkuDTO, ['id' => $productSkuDTO->id]);
    }

    public function querySkuDetailFromDB(int $skuId): ?ProductSkuDTO
    {

        $mysql = $this->select_mysql_db('main');
        $result = $mysql->selectOne("select * from product_sku where id =? limit 1 ", [$skuId], ProductSkuDTO::class);

        return $result;
    }

    public function updateSkuStock(int $skuId, int $saleCount): ?int
    {

        $mysql = $this->select_mysql_db('main');
        $sql = "update product_sku 
                    set stock=stock-$saleCount,sale=sale+$saleCount 
                    where id =$skuId and (stock-$saleCount)>=0";
        return $mysql->query($sql);
    }


    public function queryProductRest(int $itemId, string $indexes): ?ProductSkuDTO
    {
        $mysql = $this->select_mysql_db('main');
        $result = $mysql->selectOne("select * from product_sku where item_id =? and indexes=? and is_delete=0 limit 1 ", [$itemId, $indexes], ProductSkuDTO::class);
        return $result;
    }


    public function delSkuInfoDTOCache(int $skuId)
    {
        $key = $this->buildProductSkuInfoDTOKey($skuId);
        if ($key) {
            $this->redis->del($key);
        }
    }


    public function setSkuInfoDTOCache(?ProductSkuDTO $productSkuDTO): ?bool
    {
        if ($productSkuDTO == null) {
            return false;
        }
        $key = $this->buildProductSkuInfoDTOKey($productSkuDTO->id);
        $redis = $this->get_redis(self::PRODUCT_REDIS_MODULE);

        $properties = get_object_vars($productSkuDTO);
        if ($properties == null) {
            return false;
        }
        foreach ($properties as $filed => $value) {
            $redis->hset($key, $filed, $value);
        }
        $redis->EXPIRE($key, TimeHelper::SECONDS_ONE_HOUR);
        return true;
    }

    public function incrbySkuCount(?int $skuId, ?string $field, ?int $value): ?bool
    {
        if ($skuId == 0) {
            return false;
        }
        $key = $this->buildProductSkuInfoDTOKey($skuId);
        $redis = $this->get_redis(self::PRODUCT_REDIS_MODULE);
        $redis->HINCRBY($key, $field, $value);
        return true;
    }

    public function getSkuInfoDTOCache(?int $skuId): ?ProductSkuDTO
    {
        if ($skuId == 0) {
            return null;
        }
        $key = $this->buildProductSkuInfoDTOKey($skuId);
        $redis = $this->get_redis(self::PRODUCT_REDIS_MODULE);

        $dataArray = $redis->hgetall($key);
        if (empty($dataArray)) {
            return null;
        }

        $productSkuDTO = new ProductSkuDTO();
        foreach ($dataArray as $field => $value) {
            if (property_exists(ProductSkuDTO::class, $field)) {
                if ($value != null) {
                    $productSkuDTO->$field = $value;
                }
            }
        }
        return $productSkuDTO;
    }


    /**
     * @param int $skuId
     * @param int $stock
     * @return int|null加库存
     */

    public function updateProductStockSale(ProductSkuDTO $productSkuDTO): ?int

    {
        $mysql = $this->select_mysql_db('main');
        $data = ['stock' => $productSkuDTO->stock, 'sale' => $productSkuDTO->sale];
        $args = ['id' => $productSkuDTO->id];

        return $mysql->update("product_sku", $data, $args);
    }


    public function queryProductSkuListByItemId(int $itemId): ?array
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->select("select * from product_sku where item_id =? and is_delete=0 order by id asc ", [$itemId], ProductSkuDTO::class);
    }


    public function queryProductSkuIdListByItemId(int $itemId): ?array
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->select("select id from product_sku where item_id =?  order by id desc ", [$itemId]);
    }


    public function deleteSkuByItem(int $itemId): ?bool
    {
        $mysql = $this->select_mysql_db('main');

        $data = ['is_delete' => 1];
        $args = ['item_id' => $itemId];

        return $mysql->update("product_sku", $data, $args);
    }

    public function countProductItemStock(int $itemId): ?int
    {
        $row_count = 0;
        $mysql = $this->select_mysql_db('main');
        $data = $mysql->selectOne("select sum(stock) as row_count from product_sku where item_id =? and is_delete=0 limit 1 ", [$itemId], ProductSkuDTO::class);

        if ($data != null) {
            $row_count = $data->row_count;
        }
        return $row_count;
    }

    public function getDistinctProductSkuList(int $offset, int $pageSize): ?array
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->select("select * from product_sku  where publish_status =1 group by item_id order by id desc  limit ?,? ", [$offset, $pageSize], ProductSkuDTO::class);
    }


    /**
     * @param int $publish_status
     * @return array|null
     * 根据上下架状态获取skuid list
     */
    public function getSkuByStatus(int $publish_status): ?array
    {
        $mysql = $this->select_mysql_db('main');
        $data = $mysql->select("select id from product_sku where publish_status=?", [$publish_status]);

        $skuIdList = [];
        if (!empty($data)) {
            foreach ($data as $record) {
                array_push($skuIdList, intval($record->id));
            }
        }

        return $skuIdList;
    }
}