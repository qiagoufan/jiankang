<?php

namespace model;

use model\dto\ProductSkuBrandDTO;


class ProductSkuBrandModel extends \FUR_Model
{
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function queryBrandList(): ?array
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->select("select * from product_sku_brand", [], ProductSkuBrandDTO::class);

    }


    public function queryBrandInfo(int $brandId): ?array
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne('select * from product_sku_brand where id =? ', [$brandId], ProductSkuBrandDTO::class);
    }

}