<?php 
 
namespace model\product\dto; 
 
class ProductCategoryDTO { 

	 //主键id
	 public $id;
	 //一级分类id
	 public $cate_level1_id;
	 //一级分类名称
	 public $cate_level1_name;
	 //二级分类id
	 public $cate_level2_id;
	 //二级分类名称
	 public $cate_level2_name;
	 //三级分类id
	 public $cate_level3_id;
	 //三级分类名称
	 public $cate_level3_name;
	 //分类名称
	 public $category_name;
	 //父分类id
	 public $parent_id;
	 //分类层级
	 public $category_level;
	 //分类状态1.正常0.删除
	 public $category_status;
	 //封面
	 public $category_cover;
	 //排序号
	 public $seq;
	 //创建时间
	 public $created_timestamp;
	 //更新时间
	 public $updated_timestamp;
 
 
 }