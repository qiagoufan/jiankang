<?php 
 
namespace model\product\dto; 
 
class ProductFlowerExtraInfoDTO { 

	 //主键
	 public $id;
	 //商品id
	 public $item_id;
	 //规格描述
	 public $specifications;
	 //品牌故事
	 public $brand_story;
	 //物流说明
	 public $logistics_desc;
	 //保养心得
	 public $care_instructions;
	 //特别说明
	 public $special_note;
	 //售后
	 public $after_sale_instructions;
	 //创建时间
	 public $created_timestamp;
	 //更新时间
	 public $updated_timestamp;
 
 
 }