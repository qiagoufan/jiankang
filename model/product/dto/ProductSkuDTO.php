<?php 
 
namespace model\product\dto; 
 
class ProductSkuDTO { 

	 //商品ID
	 public $id;
	 //item
	 public $item_id;
	 //商品编码
	 public $product_code;
	 //商品名称
	 public $sku_name;
	 //商品摘要
	 public $summary;
	 //条形码
	 public $bar_code;
	 //品牌表的ID
	 public $brand_id;
	 //商品展示主图
	 public $main_image;
	 //一级分类ID
	 public $cate_level_1;
	 //二级分类ID
	 public $cate_level_2;
	 //三级分类ID
	 public $cate_level_3;
	 //卖家id
	 public $seller_id;
	 //上架时间
	 public $published_timestamp;
	 //上下架状态：0下架,1上架
	 public $publish_status;
	 //商品录入时间
	 public $created_timestamp;
	 //最后修改时间
	 public $updated_timestamp;
	 //商品重量
	 public $weight;
	 //商品长度
	 public $length;
	 //商品高度
	 public $height;
	 //商品宽度
	 public $width;
	 //商品总销量
	 public $sale;
	 //商品库存
	 public $stock;
	 //是否删除
	 public $is_delete;
	 //售价
	 public $current_price;
	 //运费
	 public $shipping_price;
	 //正价
	 public $regular_price;
 
 
 }