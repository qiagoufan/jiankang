<?php 
 
namespace model\product\dto; 
 
class ProductItemDTO { 

	 //spu id
	 public $id;
	 //名称
	 public $product_name;
	 //商品类型
	 public $product_type;
	 //品牌
	 public $brand_id;
	 //商品总销量
	 public $sale;
	 //商品总库存
	 public $stock;
	 //卖家id
	 public $seller_id;
	 //创建时间
	 public $created_timestamp;
	 //更新时间
	 public $updated_timestamp;
	 //删除时间
	 public $deleted_timestamp;
	 //全部规格参数数据
	 public $specifications;
	 //上架时间
	 public $published_timestamp;
	 //1.上架0.下架
	 public $publish_status;
	 //状态
	 public $product_status;
	 //一级目录
	 public $cate_level_1;
	 //二级目录
	 public $cate_level_2;
	 //三级目录
	 public $cate_level_3;
	 //评价数
	 public $review_count;
	 //默认的sku_id
	 public $default_sku_id;
	 //画廊图片
	 public $gallery_image;
	 //商品主图
	 public $main_image;
	 //商品简介
	 public $summary;
	 //商品详情
	 public $product_desc;
	 //运费
	 public $shipping_price;
	 //展示售价,单位分
	 public $current_price;
	 //原价
	 public $regular_price;
	 //排序号
	 public $seq;
 
 
 }