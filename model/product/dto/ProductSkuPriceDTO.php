<?php 
 
namespace model\product\dto; 
 
class ProductSkuPriceDTO { 

	 //主键
	 public $id;
	 //skuid
	 public $sku_id;
	 //售价
	 public $current_price;
	 //原价
	 public $regular_price;
	 //item id
	 public $item_id;
	 //运费
	 public $shipping_price;
 
 
 }