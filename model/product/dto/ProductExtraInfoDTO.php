<?php


namespace model\product\dto;


class ProductExtraInfoDTO
{

    public int $id;
    public ?int  $item_id = 0;
    public ?string $specifications = '';
    public ?string $brand_story = null;
    public ?string $care_instructions = null;
    public ?string $special_note = null;
    public ?string $logistics_desc = null;
    public ?string $after_sale_instructions = null;
    public ?int $created_timestamp = 0;
    public ?int $updated_timestamp = null;
}