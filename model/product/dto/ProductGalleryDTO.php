<?php


namespace model\product\dto;


class ProductGalleryDTO
{
    const PIC_CATEGORY_DETAIL = 1;


    public $id;
    public $image;
    public $description;


}