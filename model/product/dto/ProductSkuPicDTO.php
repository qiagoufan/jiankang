<?php 
 
namespace model\product\dto; 
 
class ProductSkuPicDTO { 

	 //主键
	 public $id;
	 //链接
	 public $image;
	 //描述
	 public $description;
	 //创建时间
	 public $created_timestamp;
	 //更新时间
	 public $updated_timestamp;
	 //删除时间
	 public $deleted_timestamp;
	 //skuid
	 public $sku_id;
	 //1.详情页
	 public $category;
 
 
 }