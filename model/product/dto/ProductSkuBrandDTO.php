<?php 
 
namespace model\product\dto; 
 
class ProductSkuBrandDTO { 

	 //主键
	 public $id;
	 //品牌名称
	 public $brand_name;
	 //更新时间
	 public $updated_timestamp;
	 //创建时间
	 public $created_timestamp;
 
 
 }