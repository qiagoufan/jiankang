<?php 
 
namespace model\product\dto; 
 
class ProductReviewDTO { 

	 //主键
	 public $id;
	 //商品ID
	 public $item_id;
	 //订单id
	 public $order_id;
	 //用户ID
	 public $customer_id;
	 //sku_id
	 public $sku_id;
	 //评论内容
	 public $content;
	 //购买商品的规格详情
	 public $specification_detail;
	 //审核状态：0未审核，1已审核
	 public $audit_status;
	 //评论时间
	 public $audit_time;
	 //评分(1-5)
	 public $score;
	 //图片列表
	 public $image_list;
	 //短视频列表
	 public $video_list;
	 //更新时间
	 public $created_timestamp;
	 //最后修改时间
	 public $updated_timestamp;
 
 
 }