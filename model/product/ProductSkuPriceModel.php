<?php


namespace model;

use model\dto\ProductItemDTO;
use model\dto\ProductSkuDTO;
use model\dto\ProductSkuPriceDTO;

class ProductSkuPriceModel extends \FUR_Model
{
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function queryProductSkuPrice(int $skuId): ?ProductSkuPriceDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select * from product_sku_price where sku_id =? limit 1 ", [$skuId], ProductSkuPriceDTO::class);
    }

    public function updateProductSkuPrice(ProductSkuPriceDTO $productSkuPriceDTO): bool
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->update('product_sku_price', $productSkuPriceDTO, ['sku_id' => $productSkuPriceDTO->sku_id]);
    }

    public function queryProductItemPriceRange(int $itemId): ?ProductSkuPriceDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select  min(current_price) as low_price,max(current_price) as high_price  from product_sku_price  INNER 
JOIN product_sku  on product_sku_price.sku_id=product_sku.id 
where product_sku.item_id =? and product_sku.is_delete=0;", [$itemId], ProductSkuPriceDTO::class);
    }


    public function insertRecord(ProductSkuPriceDTO $productSkuPriceDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert('product_sku_price', $productSkuPriceDTO);
    }

}