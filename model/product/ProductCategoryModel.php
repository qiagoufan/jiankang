<?php

namespace model\product;

use model\product\dto\ProductCategoryDTO;

class ProductCategoryModel extends \FUR_Model
{
    public static $_instance;


    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function queryCategoryById(int $cateId): ?ProductCategoryDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select * from product_category where id=?", [$cateId], ProductCategoryDTO::class);

    }

    public function updateCategory(ProductCategoryDTO $categoryDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->update('product_category', $categoryDTO, ['id' => $categoryDTO->id]);
    }


    public function updateCategoryParentName($parentId, $parentName, $level): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $keyName = "cate_level" . $level . "_name";
        return $mysql->query("update product_category set $keyName=? where parent_id=?", [$parentName, $parentId]);
    }


    public function getChildrenCategory(int $childrenLevel, int $parentId): ?array
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->select("select * from product_category where parent_id=? and category_level=?", [$parentId, $childrenLevel], ProductCategoryDTO::class);
    }

      public function queryCategoryList(): ?array
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->select("select * from product_category where category_status=? order by seq desc, id desc  ", [1], ProductCategoryDTO::class);
    }

    public function queryCategoryListByParentId($parentId): ?array
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->select("select * from product_category where parent_id=? and category_status=? order by seq desc, id desc  ", [$parentId,1], ProductCategoryDTO::class);
    }


    public function insertCategory(ProductCategoryDTO $categoryDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert("product_category", $categoryDTO);
    }

    public function queryProductCateByNameAndLevel(String $cateName, int $level): ?ProductCategoryDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select * from product_category where category_name=? and  category_level =?", [$cateName, $level], ProductCategoryDTO::class);
    }

    public function deleteCategoryById($category_id)
    {
        $mysql = $this->select_mysql_db('main');
        $categoryDTO = new ProductCategoryDTO();
        $categoryDTO->category_status = 0;
        $categoryDTO->id = $category_id;
        return $mysql->update('product_category', $categoryDTO, ['id' => $categoryDTO->id]);
    }
}