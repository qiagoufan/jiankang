<?php


namespace model;

use model\dto\ProductReviewDTO;
use facade\request\admin\AuditProductReviewParams;

class ProductReviewModel extends \FUR_Model
{
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function queryProductReviewListByItemId(int $itemId, int $offset = 0, int $pageSize = 10): ?array
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->select("select *  from product_review where item_id =? and audit_status =1 order by id desc  limit ?,? ", [$itemId, $offset, $pageSize], ProductReviewDTO::class);
    }

    public function queryProductReviewListBySkuId(int $skuId, int $offset = 0, int $pageSize = 10): ?array
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->select("select *  from product_review where sku_id =? and audit_status =1  order by id desc  limit ?,? ", [$skuId, $offset, $pageSize], ProductReviewDTO::class);
    }

    /**
     * 插入review详情
     * @param ProductReviewDTO $productReviewDTO
     * @return int|null
     */
    public function insertRecord(ProductReviewDTO $productReviewDTO): ?int
    {

        $mysql = $this->select_mysql_db('main');
        return $mysql->insert('product_review', $productReviewDTO);
    }

    public function updateReviewCount(int $itemId): ?int
    {

        $mysql = $this->select_mysql_db('main');
        return $mysql->query('update product_item set review_count =review_count+1 where id =?', [$itemId]);
    }

    public function getReviewCount(int $itemId): ?int
    {

        $mysql = $this->select_mysql_db('main');
        $data = $mysql->selectOne('select review_count from  product_item  where id =?', [$itemId]);
        if ($data != null) {
            return $data->review_count;
        }
        return 0;
    }

    public function queryProductReviewInfo(int $itemId): ?ProductReviewDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select *  from product_review where id =?  ", [$itemId], ProductReviewDTO::class);
    }


    public function getUserReviewList(int $orderId): ?ProductReviewDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select id from product_review where order_id =? ", [$orderId], ProductReviewDTO::class);

    }


    /**
     * @param $searchUserListParams
     * @return array|null
     * 获取商品评价列表
     */
    public function getProductReviewList($searchReviewListParams): ?array
    {
        $pageSize = $searchReviewListParams->page_size;
        $index = $searchReviewListParams->index;
        $start_time = $searchReviewListParams->start_time;
        $end_time = $searchReviewListParams->end_time;
        $mysql = $this->select_mysql_db('main');
        if ($start_time != null && $end_time != null) {
            return $mysql->select("select * from product_review where created_timestamp between  ?and ? limit ? offset ?  ", [$pageSize, $start_time, $end_time, $index * $pageSize], ProductReviewDTO::class);
        }

        return $mysql->select("select * from product_review limit ? offset ? ", [$pageSize, $index * $pageSize], ProductReviewDTO::class);
    }


    /**
     * @return int|null
     * 获取商品评价总数
     */
    public function queryProductReviewCount(): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $data = $mysql->selectOne("select count(*) as row_count  from product_review ", [], ProductReviewDTO::class);
        $row_count = 0;
        if ($data != null) {
            $row_count = $data->row_count;
        }
        return $row_count;
    }


    /**
     * @param int $productReviewId
     * @return int|null
     * 审核商品评价
     */
    public function updateProductReview(AuditProductReviewParams $auditProductReviewParams): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $data = ['audit_status' => $auditProductReviewParams->audit_status];
        $args = ['id' => $auditProductReviewParams->product_review_id];

        return $mysql->update("product_review", $data, $args);

    }

}