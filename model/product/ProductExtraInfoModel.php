<?php

namespace model\product;


use model\product\dto\ProductExtraInfoDTO;

class ProductExtraInfoModel extends \FUR_Model
{
    public static $_instance;
    const PRODUCT_REDIS_MODULE = 'product';

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function __construct()
    {
        $redis = $this->get_redis('product');
        $this->redis = $redis;
    }


    public function getExtraInfo(int $itemId): ?ProductExtraInfoDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select * from product_flower_extra_info where item_id = ?", [$itemId], ProductExtraInfoDTO::class);
    }

    public function insertExtraInfo(ProductExtraInfoDTO $productExtraInfoDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert("product_flower_extra_info", $productExtraInfoDTO);
    }

    public function updateExtraInfo(ProductExtraInfoDTO $productExtraInfoDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $args = ['item_id' => $productExtraInfoDTO->item_id];
        return $mysql->update("product_flower_extra_info", $productExtraInfoDTO, $args);
    }

    public function deleteExtraInfo(ProductExtraInfoDTO $productExtraInfoDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $args = ['item_id' => $productExtraInfoDTO->item_id];
        return $mysql->delete("product_flower_extra_info", $args);
    }

}