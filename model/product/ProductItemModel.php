<?php

namespace model\product;


use facade\request\product\GetProductItemListParams;
use facade\request\product\QueryAdminProductItemListParams;
use lib\AppConstant;
use lib\TimeHelper;
use model\product\dto\ProductItemDTO;

class ProductItemModel extends \FUR_Model
{
    public static $_instance;
    const PRODUCT_REDIS_MODULE = 'product';

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function __construct()
    {
        $redis = $this->get_redis('product');
        $this->redis = $redis;
    }


    public function insertProductItem(ProductItemDTO $productItemDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert("product_item", $productItemDTO);
    }


    public function getProductItem(int $itemId): ?ProductItemDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select  *  from product_item where id =? limit 1 ", [$itemId], ProductItemDTO::class);

    }


    public function deleteProductItem(int $itemId): ?bool
    {

        $mysql = $this->select_mysql_db('main');
        $data = ['product_status' => 0, 'updated_timestamp' => time()];
        $args = ['id' => $itemId];
        return $mysql->update("product_item", $data, $args);
    }


    private function buildProductItemKey(int $itemId): string
    {
        return 'product_item:' . $itemId;
    }


    public function queryPublishProductItemList(GetProductItemListParams $params): ?array
    {
        $pageSize = $params->page_size;
        $index = $params->index;
        $offset = $index * $pageSize;
        $mysql = $this->select_mysql_db('main');
        $condition = ' where publish_status =1 and product_status =1 ';
        if ($params->product_category_id) {
            $condition .= ' and ( cate_level_2=' . $params->product_category_id . " or cate_level_1=" . $params->product_category_id . ") ";
        }
        if ($params->keyword) {
            $condition .= ' and product_name like ' . "'%" . $params->keyword . "%' ";
        }

        $sql = 'select * from product_item ' . $condition . '  order by seq desc,id desc limit ?, ? ';
        return $mysql->select($sql, [$offset, $pageSize], ProductItemDTO::class);
    }

    public function queryProductByIdList(array $idList): ?array
    {
        $mysql = $this->select_mysql_db('main');
        $sql = 'select * from product_item  where id in (' . implode(",", $idList) . ') and publish_status =1';
        return $mysql->select($sql, [], ProductItemDTO::class);

    }

    public function queryProductItemList(QueryAdminProductItemListParams $params): ?array
    {
        $pageSize = $params->page_size;
        $index = $params->index;
        $offset = $index * $pageSize;
        $mysql = $this->select_mysql_db('main');
        $condition = ' where 1=1 ';
        if ($params->product_category_id) {
            $condition .= ' and cate_level_2=' . $params->product_category_id;
        }
        if ($params->publish_status != 0) {
            $condition .= ' and publish_status= ' . $params->publish_status;
        }
        if ($params->keyword) {
            $condition .= ' and product_name like ' . "'%" . $params->keyword . "%'";
        }
        $sql = 'select * from product_item ' . $condition . '  order by seq desc,id desc limit ?, ? ';
        return $mysql->select($sql, [$offset, $pageSize], ProductItemDTO::class);
    }


    public function queryProductItemCount(QueryAdminProductItemListParams $params): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $condition = ' where 1=1 ';
        if ($params->product_category_id) {
            $condition .= ' and cate_level_2=' . $params->product_category_id;
        }
        if ($params->publish_status != 0) {
            $condition .= ' and publish_status= ' . $params->publish_status;
        }
        if ($params->keyword) {
            $condition .= ' and product_name like ' . "'%" . $params->keyword . "%'";
        }
        $data = $mysql->selectOne($sql = 'select count(*) as row_count from product_item ' . $condition);

        if ($data) {
            $row_count = $data->row_count;
        }
        return $row_count;
    }


    public function queryPriceRangeByItemId(int $itemId): ?ProductItemDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select  min(current_price) as low_price,max(current_price) as high_price  from product_sku_price  INNER 
JOIN product_sku  on product_sku_price.sku_id=product_sku.id 
where product_sku.item_id =? and product_sku.is_delete=0;", [$itemId], ProductItemDTO::class);
    }


    public function updateItemPublishStatus(int $itemId, int $status): ?bool
    {
        $mysql = $this->select_mysql_db('main');
        $data = ['publish_status' => $status, 'updated_timestamp' => time()];
        $args = ['id' => $itemId];
        return $mysql->update("product_item", $data, $args);
    }


    public function updateProductItem(ProductItemDTO $productItemDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $args = ['id' => $productItemDTO->id];
        return $mysql->update("product_item", $productItemDTO, $args);
    }

    public function updateItemStock(int $itemId, int $saleCount): ?int
    {

        $mysql = $this->select_mysql_db('main');
        $sql = "update product_item 
                    set stock=stock-$saleCount,sale=sale+$saleCount 
                    where id =$itemId and (stock-$saleCount)>=0";

        return $mysql->query($sql);
        return $mysql->update("xh_product_item", $data, $args);
    }


    public function updateItemStockSale(ProductItemDTO $productItemDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $data = ['stock' => $productItemDTO->stock, 'sale' => $productItemDTO->sale];
        $args = ['id' => $productItemDTO->id];
        return $mysql->update("product_item", $data, $args);
    }

    public function getIndexPageProductItemList(int $index, int $mod): ?array
    {
        $mysql = $this->select_mysql_db('main');
        if ($index == 0) {
//            return $mysql->select("select * from product_item  where publish_status =1   order by id desc  limit 3 ", ProductItemDTO::class);
            return $mysql->select("select * from product_item  where publish_status =1  and  id % ?= ? order by id desc  limit 3 ", [$mod, $index], ProductItemDTO::class);

        } else {
            return $mysql->select("select * from product_item  where publish_status =1  and  id % ?= ? order by id desc  limit 3 ", [$mod, $index], ProductItemDTO::class);

        }


    }

    public function getIndexDefaultProductItemList(int $offset, int $pageSize): ?array
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->select("select * from product_item  where publish_status =1   order by id desc  limit ?,? ", [$offset, $pageSize], ProductItemDTO::class);


    }

}