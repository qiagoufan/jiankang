<?php 
 
namespace model\operation\dto; 
 
class OperationAdvertisingDTO { 

	 //主键
	 public $id;
	 //名称
	 public $title;
	 //位置id
	 public $space_id;
	 //链接参数
	 public $link;
	 //链接类型
	 public $link_type;
	 //数据详情
	 public $data;
	 //更新时间
	 public $updated_timestamp;
	 //创建时间
	 public $created_timestamp;
	 //过期时间
	 public $expire_timestamp;
	 //显示状态
	 public $status;
	 //排序
	 public $sort;
	 //类型
	 public $type;
 
 
 }