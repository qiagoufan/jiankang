<?php


namespace model\operation;


use facade\request\PageParams;
use  model\operation\dto\OperationAdvertisingDTO;

class AdvertisingModel extends \FUR_Model
{
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function queryRecord(int $id): ?OperationAdvertisingDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select * from operation_advertising where id=?  limit 1", [$id], OperationAdvertisingDTO::class);
    }

    public function insertRecord(OperationAdvertisingDTO $advertisingDTO): int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert('operation_advertising', $advertisingDTO);
    }

    public function updateAdvertising(OperationAdvertisingDTO $advertisingDTO): int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->update('operation_advertising', $advertisingDTO, ['id' => $advertisingDTO->id]);
    }


    public function queryAdvertisingListBySpace(int $spaceId): ?array
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->select("select * from operation_advertising   
                                    where space_id =? and status=1 
                                    order by sort desc   
                                    limit 100", [$spaceId], OperationAdvertisingDTO::class);
    }


    public function queryAdvertisingList(PageParams $params): ?array
    {
        $offset = $params->index * $params->page_size;
        $mysql = $this->select_mysql_db('main');
        return $mysql->select("select * from operation_advertising   
                                    where  status=1 
                                    order by sort desc   
                                    limit ?,?", [$offset, $params->page_size], OperationAdvertisingDTO::class);
    }

    public function queryAdvertisingCount(): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $data = $mysql->select("select count(*) as total_count from operation_advertising   
                                    where status=1 ", []);
        $totalCount = 0;
        if ($data) {
            $totalCount = $data->total_count;
        }
        return $totalCount;
    }

    public function queryAdvertisingCountBySpace(int $spaceId): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $data = $mysql->select("select count(*) as total_count from operation_advertising   
                                    where space_id =? and status=1 ", [$spaceId]);
        $totalCount = 0;
        if ($data) {
            $totalCount = $data->total_count;
        }
        return $totalCount;
    }


    public function deleteAdvertising(int $id): ?bool
    {
        $mysql = $this->select_mysql_db('main');
        $data = ['status' => 0, 'updated_timestamp' => time()];
        $args = ['id' => $id];
        return $mysql->update('operation_advertising', $data, $args);

    }


    public function queryTotalCount(): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $data = $mysql->selectOne('select count(*)as total_count   from operation_advertising where status = 1');
        if ($data != null) {
            return $data->total_count;
        }
        return 0;
    }


}