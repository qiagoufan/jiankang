<?php


namespace model\activity\dto;

class XhActivityHuobaoxiaojiuFragmentDTO
{
    const FRAGMENT_STATUS_USED = 1;
    const FRAGMENT_STATUS_UNUSED = 0;


    const FRAGMENT_NUM_AWARD = 6;

    public $id;
    public $fragment_num;
    public $award_id;
    public $fragment_code;
    public $user_id;
    public $created_timestamp;
    public $updated_timestamp;
    public $fragment_status;

}