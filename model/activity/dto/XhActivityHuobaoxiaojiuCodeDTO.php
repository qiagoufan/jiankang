<?php


namespace model\activity\dto;

class XhActivityHuobaoxiaojiuCodeDTO
{

    //通用码
    public const CODE_TYPE_GENERAL = 0;
    //一等奖
    public const CODE_TYPE_FIRST_PRIZE = 1;
    //二等奖
    public const CODE_TYPE_SECOND_PRIZE = 2;
    //三等奖
    public const CODE_TYPE_THIRD_PRIZE = 3;


    public const CODE_STATUS_EXPIRE = -1;
    public const CODE_STATUS_UNUSED = 0;
    public const CODE_STATUS_USED = 1;


    public $id;
    public $code;
    public $award_id;
    public $code_status;
    public $user_id;
    public $use_timestamp;
}