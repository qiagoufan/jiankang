<?php


namespace model\activity\dto;

class XhActivityHuobaoxiaojiuAwardDTO
{
    const AWARD_STATUS_VALID = 1;
    const AWARD_STATUS_INVALID = 0;

    public $id;
    public $award_title;
    public $award_type;
    public $award_img;
    public $award_status;
    public $created_timestamp;
    public $updated_timestamp;


}