<?php


namespace model\activity\dto;

class XhActivityHuobaoxiaojiuUserAwardDTO
{
    const RECEIVE_STATUS_USED = 1;
    const RECEIVE_STATUS_UNUSED = 0;

    public $id;
    public $user_id;
    public $address;
    public $receiver;
    public $receive_status;
    public $phone_num;
    public $province;
    public $city;
    public $area;
    public $express_company;
    public $express_num;
    public $created_timestamp;
    public $updated_timestamp;
    public $fragment_id_list;
    public $award_id;
    public $withdraw_order_num;
    public $withdraw_payment_num;
    public $delivery_timestamp;
    public $withdraw_timestamp;


}