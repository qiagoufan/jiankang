<?php


namespace model\activity;

use facade\request\admin\SearchUserListParams;
use model\activity\dto\XhActivityHuobaoxiaojiuAwardDTO;
use model\activity\dto\XhActivityHuobaoxiaojiuCodeDTO;
use model\activity\dto\XhActivityHuobaoxiaojiuFragmentDTO;
use model\activity\dto\XhActivityHuobaoxiaojiuUserAwardDTO;
use model\dto\UserDTO;

class XhActivityHuobaoxiaojiuModel extends \FUR_Model
{
    public const REDIS_MODULE = 'user';

    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    /**
     * 获取code详情
     * @param string $code
     * @return XhActivityHuobaoxiaojiuCodeDTO|null
     */
    public function getCodeDetail(string $code): ?XhActivityHuobaoxiaojiuCodeDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne('select *  from activity_huobaoxiaojiu_code where code =? ', [$code], XhActivityHuobaoxiaojiuCodeDTO::class);
    }

    /**
     * 把code 改为已被用过
     * @param XhActivityHuobaoxiaojiuCodeDTO $codeDTO
     * @return int
     */
    public function updateCodeToBeUsed(XhActivityHuobaoxiaojiuCodeDTO $codeDTO): int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->update('activity_huobaoxiaojiu_code', $codeDTO, ['code' => $codeDTO->code]);
    }

    public function insertFragment(XhActivityHuobaoxiaojiuFragmentDTO $fragmentDTO): int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert('activity_huobaoxiaojiu_fragment', $fragmentDTO);
    }


    public function getUserFragmentCountList(int $userId): ?array
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->select("select count(*) as fragment_count,fragment_num from activity_huobaoxiaojiu_fragment where user_id = ? and fragment_status =0  group by fragment_num", [$userId]);
    }

    /**
     * 获取用户的碎片
     * @param int $userId
     * @param int $fragmentType
     * @return XhActivityHuobaoxiaojiuFragmentDTO|null
     */
    public function getUserFragmentByType(int $userId, int $fragmentNum): ?XhActivityHuobaoxiaojiuFragmentDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select * from activity_huobaoxiaojiu_fragment where user_id = ? and fragment_num =? and fragment_status =0 limit 1", [$userId, $fragmentNum], XhActivityHuobaoxiaojiuFragmentDTO::class);
    }


    public function updateFragmentToBeUsed(array $fragmentIdList)
    {
        $mysql = $this->select_mysql_db('main');
        foreach ($fragmentIdList as $fragmentId) {
            $mysql->update('activity_huobaoxiaojiu_fragment', ['fragment_status' => XhActivityHuobaoxiaojiuFragmentDTO::FRAGMENT_STATUS_USED], ['id' => $fragmentId]);
        }

    }

    public function insertUserAward(XhActivityHuobaoxiaojiuUserAwardDTO $userAwardDTO): int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert('activity_huobaoxiaojiu_user_award', $userAwardDTO);
    }


    public function insertCode(XhActivityHuobaoxiaojiuCodeDTO $codeDTO): int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert('activity_huobaoxiaojiu_code', $codeDTO);
    }


    public function getUserAwardDetail(int $userAwardId): ?XhActivityHuobaoxiaojiuUserAwardDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne('select * from activity_huobaoxiaojiu_user_award where id = ?', [$userAwardId], XhActivityHuobaoxiaojiuUserAwardDTO::class);
    }

    public function getUserAwardList(int $userId): ?array
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->select('select * from activity_huobaoxiaojiu_user_award where user_id = ? order by id desc ', [$userId], XhActivityHuobaoxiaojiuUserAwardDTO::class);
    }


    public function getAwardInfoList(): ?array
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->select('select * from activity_huobaoxiaojiu_award order by id asc  ', [], XhActivityHuobaoxiaojiuAwardDTO::class);
    }

    public function getAwardInfo(int $id): ?XhActivityHuobaoxiaojiuAwardDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne('select * from activity_huobaoxiaojiu_award where id = ?', [$id], XhActivityHuobaoxiaojiuAwardDTO::class);
    }

    public function updateUserAward(XhActivityHuobaoxiaojiuUserAwardDTO $userAwardDTO)
    {
        $mysql = $this->select_mysql_db('main');

        return $mysql->update('activity_huobaoxiaojiu_user_award', $userAwardDTO, ['id' => $userAwardDTO->id]);
    }

    public function queryUserAwardFragmentCount(int $userId, int $awardId): int
    {
        $mysql = $this->select_mysql_db('main');
        $data = $mysql->selectOne('select count(*) as total_count from activity_huobaoxiaojiu_fragment where user_id = ? and award_id =?', [$userId, $awardId]);
        if ($data == null) {
            return 0;
        }
        return  intval($data->total_count);
    }


    public function getAwardList(int $start, int $offset)
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->select('select * from activity_huobaoxiaojiu_user_award  order by id desc  limit ?,?', [$start, $offset], XhActivityHuobaoxiaojiuUserAwardDTO::class);
    }

    public function getAwardCount(): int
    {
        $mysql = $this->select_mysql_db('main');
        $data = $mysql->selectOne('select count(*) as total_count from activity_huobaoxiaojiu_user_award ');
        return intval($data->total_count);
    }

    public function lockUser(int $userId): bool
    {
        $redis = $this->get_redis(self::REDIS_MODULE);
        $key = 'activity_lock:' . $userId;
        $ret = $redis->setnx($key, 1);
        if ($ret == 0) {
            return false;
        }
        $redis->EXPIRE($key, 20);
        return true;
    }

    public function unlockUser(int $userId): bool
    {
        $redis = $this->get_redis(self::REDIS_MODULE);
        $key = 'activity_lock:' . $userId;
        return (bool)$redis->del($key);

    }


}