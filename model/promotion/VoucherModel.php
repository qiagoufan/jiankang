<?php


namespace model\promotion;

use facade\request\PageParams;
use model\promotion\dto\PromotionVoucherDTO;

class VoucherModel extends \FUR_Model
{
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function getVoucherInfo(int $voucherId): ?PromotionVoucherDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne('select * from promotion_voucher where  id =? limit 1', [$voucherId], PromotionVoucherDTO::class);

    }

    public function queryEnableVoucherList(): ?array
    {
        $mysql = $this->select_mysql_db('main');
        $now = time();
        return $mysql->select('select * from promotion_voucher where id >0 and  start_timestamp<? and  end_timestamp >? and  voucher_status >0  order by id desc limit 10', [$now, $now], PromotionVoucherDTO::class);
    }


    public function updateVoucherUsedCount(int $voucherId, $usedCount): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $sql = "update promotion_voucher set  voucher_used_count=voucher_used_count+$usedCount where id =$voucherId";
        return $mysql->query($sql);

    }

    public function updateVoucherDispatchCount(int $voucherId, $dispatchCount): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $sql = "update promotion_voucher set  voucher_distribute_count = voucher_distribute_count+$dispatchCount where id =$voucherId";
        return $mysql->query($sql);

    }

    public function updateVoucherCount(int $voucherId, $count): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $sql = "update promotion_voucher set  voucher_count=voucher_count-$count where id =$voucherId and voucher_count-$count>=0";
        return $mysql->query($sql);

    }


    public function deleteVoucher(int $voucherId): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $data = ['voucher_status' => 0, 'updated_timestamp' => time()];
        return $mysql->update('promotion_voucher', $data, ['id' => $voucherId]);
    }

    public function updateVoucherStatus(int $voucherId, int $voucherStatus): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $data = ['voucher_status' => $voucherStatus, 'updated_timestamp' => time()];
        return $mysql->update('promotion_voucher', $data, ['id' => $voucherId]);
    }


    public function updateVoucher(PromotionVoucherDTO $promotionVoucherDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->update('promotion_voucher', $promotionVoucherDTO, ['id' => $promotionVoucherDTO->id]);
    }

    public function createdVoucher(PromotionVoucherDTO $promotionVoucherDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert('promotion_voucher', $promotionVoucherDTO);
    }

    public function queryVoucherList(PageParams $params): ?array
    {
        $mysql = $this->select_mysql_db('main');
        $offset = $params->index * $params->page_size;
        return $mysql->select('select * from promotion_voucher where voucher_status !=0 order by id desc limit ?,?', [$offset, $params->page_size], PromotionVoucherDTO::class);
    }

    public function queryVoucherCount(PageParams $params): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $totalCount = 0;
        $data = $mysql->selectOne('select count(*) as total_count from promotion_voucher where voucher_status !=0');
        if ($data) {
            $totalCount = $data->total_count;
        }
        return $totalCount;
    }
}