<?php


namespace model\promotion;

use model\promotion\dto\PromotionVoucherEntityDTO;
use model\promotion\dto\PromotionVoucherDTO;

class VoucherEntityModel extends \FUR_Model
{
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function updateVoucherEntity(PromotionVoucherEntityDTO $promotionVoucherEntityDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->update('promotion_voucher_entity', $promotionVoucherEntityDTO, ['voucher_sn' => $promotionVoucherEntityDTO->voucher_sn]);
    }

    public function insertVoucherEntity(PromotionVoucherEntityDTO $promotionVoucherEntityDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert('promotion_voucher_entity', $promotionVoucherEntityDTO);
    }


    public function queryVoucherEntityBySn(string $voucherSn): ?PromotionVoucherEntityDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne('select * from promotion_voucher_entity where  voucher_sn =? limit 1', [$voucherSn], PromotionVoucherEntityDTO::class);
    }

    public function queryVoucherListByUserId(int $userId): ?array
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->select('select * from promotion_voucher_entity where  user_id =? order by id desc  limit 100', [$userId], PromotionVoucherEntityDTO::class);
    }

    public function queryUserVoucherEntityByVoucherId(int $userId, int $voucherId): ?PromotionVoucherEntityDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne('select * from promotion_voucher_entity where  user_id =? and voucher_id=? order by id desc  limit 1', [$userId, $voucherId], PromotionVoucherEntityDTO::class);

    }

}