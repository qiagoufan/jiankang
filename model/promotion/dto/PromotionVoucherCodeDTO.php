<?php 
 
namespace model\promotion\dto; 
 
class PromotionVoucherCodeDTO { 

	 //
	 public $id;
	 //
	 public $voucher_id;
	 //
	 public $voucher_code;
	 //优惠券状态
	 public $voucher_code_status;
	 //订单id
	 public $order_id;
	 //所有者id
	 public $owner_user_id;
	 //创建时间
	 public $created_timestamp;
	 //更新时间
	 public $updated_timestamp;
	 //领取时间
	 public $received_timestamp;
	 //使用时间
	 public $used_timestamp;
 
 
 }