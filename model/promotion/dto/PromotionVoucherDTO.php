<?php 
 
namespace model\promotion\dto; 
 
class PromotionVoucherDTO { 

	 //primary key
	 public $id;
	 //优惠券类型
	 public $voucher_type;
	 //优惠券状态
	 public $voucher_status;
	 //优惠券渠道
	 public $voucher_channel;
	 //开始可用时间
	 public $start_timestamp;
	 //结束可用时间
	 public $end_timestamp;
	 //优惠券名称
	 public $voucher_name;
	 //优惠券详情
	 public $voucher_detail;
	 //优惠券数量
	 public $voucher_count;
	 //领取数
	 public $voucher_distribute_count;
	 //已使用的数量
	 public $voucher_used_count;
	 //创建时间
	 public $created_timestamp;
	 //更新时间
	 public $updated_timestamp;
	 //????
	 public $expire_timelong;
 
 
 }