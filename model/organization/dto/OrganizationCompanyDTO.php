<?php 
 
namespace model\organization\dto; 
 
class OrganizationCompanyDTO { 

	 //
	 public $id;
	 //
	 public $company_name;
	 //
	 public $created_timestamp;
	 //
	 public $updated_timestamp;
	 //
	 public $company_license;
	 //
	 public $service_start_timestamp;
	 //
	 public $service_end_timestamp;
	 //
	 public $contact_person;
	 //
	 public $contact_info;
	 //
	 public $emergency_contact_person;
	 //
	 public $emergency_contact_info;
	 //
	 public $company_status;
	 //
	 public $company_license_pic;
	 //
	 public $account_name;
	 //
	 public $account_password;
 
 
 }