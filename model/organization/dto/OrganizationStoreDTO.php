<?php 
 
namespace model\organization\dto; 
 
class OrganizationStoreDTO { 

	 //
	 public $id;
	 //
	 public $company_id;
	 //
	 public $store_name;
	 //
	 public $store_status;
	 //
	 public $store_address;
	 //
	 public $store_province;
	 //
	 public $store_city;
	 //
	 public $store_area;
	 //
	 public $store_street;
	 //
	 public $created_timestamp;
	 //
	 public $updated_timestamp;
	 //
	 public $store_num;
	 //
	 public $store_manager;
	 //
	 public $store_manager_contact_info;
	 //
	 public $store_level;
 
 
 }