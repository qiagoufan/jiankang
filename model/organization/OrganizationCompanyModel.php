<?php

namespace model\organization;


use facade\request\organization\QueryOrganizationCompanyParams;
use lib\TimeHelper;
use model\organization\dto\OrganizationCompanyDTO;

class OrganizationCompanyModel extends \FUR_Model
{
    private static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function insertCompany(OrganizationCompanyDTO $organizationCompanyDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert('organization_company', $organizationCompanyDTO);
    }


    public function queryCompanyById(int $companyId): ?OrganizationCompanyDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select * from organization_company where id =? limit 1 ", [$companyId], OrganizationCompanyDTO::class);
    }

    public function queryCompanyByAccountName(?string $accountName): ?OrganizationCompanyDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select * from organization_company where account_name=?", [$accountName], OrganizationCompanyDTO::class);
    }

    public function queryCompanyByName(?string $companyName): ?OrganizationCompanyDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select * from organization_company where company_name=?", [$companyName], OrganizationCompanyDTO::class);
    }

    public function queryCompanyList(QueryOrganizationCompanyParams $params): ?array
    {
        $mysql = $this->select_mysql_db('main');
        $pageSize = $params->pageSize;
        $offset = $params->index * $params->pageSize;
        $sql = "select * from organization_company where 1=1  ";
        if ($params->company_license != null) {
            $sql .= " and company_license ='" . $params->company_license . "' ";
        }
        if ($params->company_name != null) {
            $sql .= " and company_name like '%" . $params->company_name . "%' ";
        }
        if ($params->service_start_timestamp != null) {
            $sql .= " and service_start_timestamp <=" . $params->service_start_timestamp . " ";
        }
        if ($params->service_end_timestamp != null) {
            $sql .= " and service_end_timestamp >" . $params->service_end_timestamp . " ";
        }
        $sql .= " and company_status =" . $params->company_status . " ";
        $sql .= " order by id desc limit $offset,$pageSize";
        return $mysql->select($sql, [], OrganizationCompanyDTO::class);
    }

    public function queryCompanyCount(QueryOrganizationCompanyParams $params): int
    {
        $mysql = $this->select_mysql_db('main');
        $count = 0;
        $sql = "select count(*) as total_count from organization_company where 1=1  ";
        if ($params->company_license != null) {
            $sql .= " and company_license ='" . $params->company_license . "' ";
        }
        if ($params->company_name != null) {
            $sql .= " and company_name like '%" . $params->company_name . "%' ";
        }
        if ($params->service_start_timestamp != null) {
            $sql .= " and service_start_timestamp <=" . $params->service_start_timestamp . " ";
        }
        if ($params->service_end_timestamp != null) {
            $sql .= " and service_end_timestamp >=" . $params->service_end_timestamp . " ";
        }
        $sql .= " and company_status =" . $params->company_status . " ";


        $data = $mysql->selectOne($sql, [], OrganizationCompanyDTO::class);
        if ($data) {
            $count = $data->total_count;
        }
        return $count;

    }

    public function updateCompany(OrganizationCompanyDTO $organizationCompanyDTO): int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->update("organization_company", $organizationCompanyDTO, ['id' => $organizationCompanyDTO->id]);

    }


    public function updateCompanyStatus(int $companyId, int $status)
    {
        $mysql = $this->select_mysql_db('main');
        $organizationCompanyDTO = new OrganizationCompanyDTO();
        $organizationCompanyDTO->company_status = $status;
        $organizationCompanyDTO->updated_timestamp = TimeHelper::getTimeMs();
        return $mysql->update("organization_company", $organizationCompanyDTO, ['id' => $companyId]);

    }
}
