<?php

namespace model\organization;


use facade\request\organization\QueryOrganizationStoreParams;
use model\organization\dto\OrganizationStoreDTO;
use model\organization\dto\OrganizationStoreManagerDTO;

class OrganizationStoreModel extends \FUR_Model
{
    private static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function insertCompanyStore(OrganizationStoreDTO $organizationStoreDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert('organization_store', $organizationStoreDTO);
    }


    public function queryCompanyStoreById(int $storeId): ?OrganizationStoreDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select * from organization_store where id =? and store_status =1 limit 1 ", [$storeId], OrganizationStoreDTO::class);
    }


    public function queryCompanyStoreByName(int $companyId, string $storeNum, string $storeName): ?OrganizationStoreDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select * from organization_store where company_id=?  and    store_name=? and store_num=?  and store_status =1", [$companyId, $storeName, $storeNum], OrganizationStoreDTO::class);
    }


    public function queryCompanyStoreList(QueryOrganizationStoreParams $queryParams): ?array
    {

        $mysql = $this->select_mysql_db('main');
        $sql = 'select * from organization_store where store_status =1 ';
        if ($queryParams->company_id) {
            $sql .= " and company_id='" . $queryParams->company_id . "' ";
        }
        if ($queryParams->store_num) {
            $sql .= " and store_num='" . $queryParams->store_num . "' ";
        }
        if ($queryParams->store_province) {
            $sql .= " and store_province='" . $queryParams->store_province . "' ";
        }
        if ($queryParams->store_city) {
            $sql .= " and store_city = '" . $queryParams->store_city . "' ";
        }
        if ($queryParams->store_level) {
            $sql .= " and store_level= '" . $queryParams->store_level . "' ";
        }
        if ($queryParams->store_name) {
            $sql .= " and store_name like '%" . $queryParams->store_name . "%' ";
        }
        if ($queryParams->store_address) {
            $sql .= " and store_address like '%" . $queryParams->store_address . "%' ";
        }
        $offset = $queryParams->index * $queryParams->page_size;
        return $mysql->select($sql . " limit ?,?", [$offset, $queryParams->page_size], OrganizationStoreDTO::class);
    }

    public function queryCompanyStoreCount(QueryOrganizationStoreParams $queryParams): int
    {
        $mysql = $this->select_mysql_db('main');
        $count = 0;
        $sql = 'select count(*) as total_count from organization_store where store_status =1 ';
        if ($queryParams->company_id) {
            $sql .= " and company_id='" . $queryParams->company_id . "' ";
        }
        if ($queryParams->store_num) {
            $sql .= " and store_num='" . $queryParams->store_num . "' ";
        }
        if ($queryParams->store_province) {
            $sql .= " and store_province='" . $queryParams->store_province . "' ";
        }
        if ($queryParams->store_city) {
            $sql .= " and store_city = '" . $queryParams->store_city . "' ";
        }
        if ($queryParams->store_level) {
            $sql .= " and store_level= '" . $queryParams->store_level . "' ";
        }
        if ($queryParams->store_name) {
            $sql .= " and store_name like '%" . $queryParams->store_name . "%' ";
        }
        if ($queryParams->store_address) {
            $sql .= " and store_address like '%" . $queryParams->store_address . "%' ";
        }
        $data = $mysql->selectOne($sql, []);
        if ($data) {
            $count = $data->total_count;
        }
        return $count;

    }

    public function updateCompanyStore(OrganizationStoreDTO $organizationStoreDTO): int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->update("organization_store", $organizationStoreDTO, ['id' => $organizationStoreDTO->id]);

    }


    public function updateCompanyStoreStatusByCompanyId(int $companyId, int $storeStatus)
    {
        $mysql = $this->select_mysql_db('main');
        $organizationStoreDTO = new OrganizationStoreDTO();
        $organizationStoreDTO->store_status = $storeStatus;
        return $mysql->update("organization_store", $organizationStoreDTO, ['company_id' => $companyId]);

    }

    public function deleteCompanyStore(int $storeId)
    {
        $mysql = $this->select_mysql_db('main');
        $organizationStoreDTO = new OrganizationStoreDTO();
        $organizationStoreDTO->store_status = \Config_Const::COMPANY_STATUS_DELETED;
        return $mysql->update("organization_store", $organizationStoreDTO, ['id' => $storeId]);
    }

    public function cleanStoreManager(int $storeId): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->delete('organization_store_manager', ['store_id' => $storeId]);
    }

    public function insertStoreManagerInfo(OrganizationStoreManagerDTO $storeManagerDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert('organization_store_manager', $storeManagerDTO);
    }

    public function queryLatestStoreManagerByPhoneNum(string $phoneNum): ?OrganizationStoreManagerDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne('select * from organization_store_manager where contact_info=? order by id desc  limit 1', [$phoneNum], OrganizationStoreManagerDTO::class);
    }
    public function queryStoreManagerListByPhoneNum(string $phoneNum): ?array
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->select('select * from organization_store_manager where contact_info=? order by id desc  limit 100', [$phoneNum], OrganizationStoreManagerDTO::class);
    }



    public function getStoreManagerList(int $storeId): ?array
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->select('select * from organization_store_manager where store_id =? order by id desc ', [$storeId], OrganizationStoreManagerDTO::class);
    }
}
