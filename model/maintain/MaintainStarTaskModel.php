<?php

namespace model\maintain;

use model\dto\MaintainStarTaskDTO;

class MaintainStarTaskModel extends \FUR_Model
{
    public static $_instance;


    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    /**
     * @param MaintainStarTaskDTO $maintainStarTaskDTO
     * @return int|null
     * 插入明星关联的任务
     */
    public function insertRecord(MaintainStarTaskDTO $maintainStarTaskDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert('maintain_star_task', $maintainStarTaskDTO);
    }


    /**
     * @param int $starId
     * @return array|null
     * 查询明星关联的任务
     */
    public function queryTaskListByStarId(int $starId): ?array
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->select('select * from maintain_star_task where  star_id=? and task_status= 1', [$starId], MaintainStarTaskDTO::class);
    }


    /**
     * @param int $starId
     * @param int $taskId
     * 查询明星已经绑定的任务
     */
    public function queryStarExistTask(int $starId, int $taskId): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $data = $mysql->selectOne('select count(*) as sum_count from maintain_star_task where  star_id=? and task_id= ?', [$starId, $taskId], MaintainStarTaskDTO::class);

        $sum_count = 0;
        if ($data != null) {
            $sum_count = $data->sum_count;
        }

        return $sum_count;
    }


    /**
     * @return array|null
     * 查询任务列表下的明星id
     */
    public
    function queryDistinctStarId(int $offset, int $pageSize): ?array
    {
        $mysql = $this->select_mysql_db('main');
        $data = $mysql->select('select distinct(star_id)  from maintain_star_task limit ?,? ', [$offset, $pageSize]);
        return $data;
    }
}