<?php

namespace model\maintain;

use lib\TimeHelper;
use model\dto\InteractiveReplyDTO;
use model\dto\MaintainLeaderboardRecordDTO;

class MaintainLeaderboardRecordModel extends \FUR_Model
{
    public static $_instance;


    const REDIS_MODULE = 'maintain';


    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    /**
     * @param MaintainLeaderboardRecordDTO $maintainLeaderboardRecordDTO
     * @return int|null
     * 插入打榜记录
     */
    public function insertRecord(MaintainLeaderboardRecordDTO $maintainLeaderboardRecordDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert('maintain_user_leaderboard_record', $maintainLeaderboardRecordDTO);
    }


    /**
     * @param int $userId
     * @param int $starId
     * @param array $recordTypeList
     * @return int|null
     */
    public function queryTotalValueByType(int $userId, int $starId, array $recordTypeList): ?int
    {
        $sumValue = 0;
        $mysql = $this->select_mysql_db('main');
        $recordTypeListStr = '';
        foreach ($recordTypeList as $recordType) {
            $recordTypeListStr .= "'" . $recordType . "',";
        }
        $recordTypeListStr = mb_substr($recordTypeListStr, 0, -1);
        $data = $mysql->selectOne("select sum(value) as total_value from maintain_user_leaderboard_record where user_id=? and star_id =? and record_type in  ($recordTypeListStr) ", [$userId, $starId]);
        if ($data != null) {
            $sumValue = $data->total_value;
        }
        return $sumValue;
    }

    /**
     *更新人气排行榜
     * @param int $starId
     * @param int $score
     * @param string $rankType
     * @return int
     */
    public function updateStarRank(int $starId, int $score, string $rankType)
    {
        $key = $this->buildRankKey($rankType);
        $redis = $this->get_redis(self::REDIS_MODULE);
        $score = $score . '.' . (TimeHelper::MAX_INT - time());
        return $redis->zAdd($key, $score, $starId);
    }

    public function getStarScore(int $starId, string $rankType)
    {
        $key = $this->buildRankKey($rankType);
        $redis = $this->get_redis(self::REDIS_MODULE);
        return $redis->ZSCORE($key, $starId);
    }

    public function getStarRank(int $starId, string $rankType)
    {
        $key = $this->buildRankKey($rankType);
        $redis = $this->get_redis(self::REDIS_MODULE);
        $data = $redis->zrevrank($key, $starId);
        if ($data === false) {
            return -1;
        }
        return $data + 1;
    }

    public function getStarLeaderboard(string $rankType, int $start, int $end)
    {
        $key = $this->buildRankKey($rankType);
        $redis = $this->get_redis(self::REDIS_MODULE);
        return $redis->zRevRange($key, $start, $end);
    }


    public function updateStarContribution(int $starId, int $value)
    {
        $key = $this->buildStarContributionKey($starId);
        $redis = $this->get_redis(self::REDIS_MODULE);
        return $redis->incrby($key, $value);
    }

    public function getStarContributionScore(int $starId)
    {
        $key = $this->buildStarContributionKey($starId);
        $redis = $this->get_redis(self::REDIS_MODULE);
        return $redis->get($key);
    }

    public function updateStarContributionUserLeaderboard(int $starId, int $userId, int $score)
    {
        $key = $this->buildStarContributionLeaderboardKey($starId);
        $redis = $this->get_redis(self::REDIS_MODULE);
        $score = $score . '.' . (TimeHelper::MAX_INT - time());

        return $redis->zadd($key, $score, $userId);
    }

    public function getStarContributionUserRank(int $starId, int $userId)
    {
        $key = $this->buildStarContributionLeaderboardKey($starId);
        $redis = $this->get_redis(self::REDIS_MODULE);
        $data = $redis->zrevrank($key, $userId);
        if ($data === false) {
            return -1;
        }
        return $data + 1;
    }

    public function getStarContributionUserScore(int $starId, int $userId)
    {
        $key = $this->buildStarContributionLeaderboardKey($starId);
        $redis = $this->get_redis(self::REDIS_MODULE);
        return $redis->ZSCORE($key, $userId);
    }

    public function getStarContributionUserLeaderboard(int $starId, int $start, int $end)
    {
        $key = $this->buildStarContributionLeaderboardKey($starId);
        $redis = $this->get_redis(self::REDIS_MODULE);
        return $redis->zRevRange($key, $start, $end);
    }

    private function buildRankKey(string $rankType): string
    {

        return 'star_rank:' . $rankType;
    }


    private function buildStarContributionKey(int $starId): string
    {
        return 'star_contribution:' . $starId;
    }

    private function buildStarContributionLeaderboardKey(int $starId): string
    {
        return 'star_contribution_leaderboard:' . $starId;
    }


}