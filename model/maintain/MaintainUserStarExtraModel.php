<?php

namespace model\maintain;

use model\dto\MaintainUserStarExtraDTO;

class MaintainUserStarExtraModel extends \FUR_Model
{
    public static $_instance;

    const MAINTAIN_REDIS_MODULE = 'maintain';

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function insertRecord(MaintainUserStarExtraDTO $maintainUserStarExtraDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert('maintain_user_star_extra', $maintainUserStarExtraDTO);
    }


    /**
     * 查询统计记录
     */
    public function queryUserRecord(int $userId, int $starId): ?MaintainUserStarExtraDTO
    {

        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select * from maintain_user_star_extra where  user_id=? and star_id=?  limit 1 ", [$userId, $starId], MaintainUserStarExtraDTO::class);
    }


    /**
     * @param MaintainUserStarExtraDTO $maintainUserStarExtraDTO
     * @return bool|null
     * 更新统计记录
     */
    public function updateRecord(MaintainUserStarExtraDTO $maintainUserStarExtraDTO): ?bool
    {
        $mysql = $this->select_mysql_db('main');
        $args = ['user_id' => $maintainUserStarExtraDTO->user_id, 'star_id' => $maintainUserStarExtraDTO->star_id];
        return $mysql->update("maintain_user_star_extra", $maintainUserStarExtraDTO, $args);
    }




}