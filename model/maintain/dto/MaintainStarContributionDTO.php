<?php 
 
namespace model\maintain\dto; 
 
class MaintainStarContributionDTO { 

	 //primary key
	 public $id;
	 //star_id
	 public $star_id;
	 //贡献值
	 public $contribution_value;
	 //创建时间
	 public $created_timestamp;
	 //更新时间
	 public $updated_timestamp;
 
 
 }