<?php 
 
namespace model\maintain\dto; 
 
class MaintainUserStarExtraDTO { 

	 //primary key
	 public $id;
	 //用户id
	 public $user_id;
	 //ip
	 public $star_id;
	 //打卡任务总分
	 public $task_total_value;
	 //打榜总分
	 public $leaderboard_total_value;
	 //创建时间
	 public $created_timestamp;
	 //更新时间
	 public $updated_timestamp;
 
 
 }