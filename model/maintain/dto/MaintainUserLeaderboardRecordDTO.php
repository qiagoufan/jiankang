<?php 
 
namespace model\maintain\dto; 
 
class MaintainUserLeaderboardRecordDTO { 

	 //主键id
	 public $id;
	 //用户id
	 public $user_id;
	 //明星id
	 public $star_id;
	 //值变化
	 public $value;
	 //创建时间
	 public $created_timestamp;
	 //更新时间
	 public $updated_timestamp;
	 //????
	 public $record_type;
 
 
 }