<?php 
 
namespace model\maintain\dto; 
 
class MaintainStarTaskDTO { 

	 //primary key
	 public $id;
	 //star_id
	 public $star_id;
	 //task_id
	 public $task_id;
	 //创建时间
	 public $created_timestamp;
	 //更新时间
	 public $updated_timestamp;
	 //0-无效 1-有效
	 public $task_status;
 
 
 }