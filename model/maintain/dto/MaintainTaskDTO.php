<?php 
 
namespace model\maintain\dto; 
 
class MaintainTaskDTO { 

	 //primary key
	 public $id;
	 //任务名称
	 public $task_name;
	 //值变化
	 public $value;
	 //创建时间
	 public $created_timestamp;
	 //更新时间
	 public $updated_timestamp;
	 //状态1.正常0.删除
	 public $status;
 
 
 }