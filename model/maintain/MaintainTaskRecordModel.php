<?php

namespace model\maintain;

use model\dto\MaintainStarTaskDTO;
use model\dto\MaintainTaskCreateDTO;
use model\dto\MaintainTaskDTO;
use model\dto\MaintainTaskRecordDTO;

class MaintainTaskRecordModel extends \FUR_Model
{
    public static $_instance;

    const MAINTAIN_REDIS_MODULE = 'maintain';

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    /**
     * @param MaintainTaskRecordDTO $MaintainTaskRecordDTO
     * @return int|null插入打卡记录
     */
    public function insertRecord(MaintainTaskRecordDTO $MaintainTaskRecordDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert('maintain_user_task_record', $MaintainTaskRecordDTO);

    }


    /**
     * @param int $userId
     * @param int $starId
     * @param int $taskId
     * @return int|null
     * 查询打卡记录
     */
    public function queryRecord(int $userId, int $starId, int $taskId): ?MaintainTaskDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne('select * from maintain_user_task_record where  user_id=?  and star_id=? and task_id=? order by created_timestamp desc limit 1 ', [$userId, $starId, $taskId], MaintainTaskDTO::class);


    }


    /**
     * @param int $starId
     * @param int $taskId
     * @return int|null
     * 查询明星任务下的星能量总值
     */
    public function queryStarValueByTaskId(int $taskId, int $starId): ?int
    {

        $mysql = $this->select_mysql_db('main');
        $data = $mysql->selectOne('select sum(value) as sum_count from maintain_user_task_record where task_id=? and star_id=? ', [$taskId, $starId]);


        if ($data != null) {
            $sumCount = $data->sum_count;
        }

        return $sumCount;
    }


    /**
     * @param int $starId
     * @param int $taskId
     * @return int|null
     * 查询明星任务下的用户贡献的星能量总值
     */
    public function queryUserValueByTaskId(int $taskId, int $starId, ?int $userId): ?int
    {

        $mysql = $this->select_mysql_db('main');
        $data = $mysql->selectOne('select sum(value) as sum_count from maintain_user_task_record where task_id=? and star_id=? and user_id=? ', [$taskId, $starId, $userId]);


        if ($data != null) {
            $sumCount = $data->sum_count;
        }

        return $sumCount;
    }


    /**
     * @param string $rankType
     * @param int $start
     * @param int $end
     * @return array|null
     * 获取星能量榜单排名
     */
    public function getMaintainTaskRankList(string $rankType, int $start, int $end): ?array
    {
        $key = $this->buildRankKey($rankType);
        $redis = $this->get_redis(self::MAINTAIN_REDIS_MODULE);
        return $redis->zRevRange($key, $start, $end);
    }


    /**
     * @param int $starId
     * @param string $rankType
     * @return int
     * 获取星能量排名
     */
    public function getMaintainTaskRank(int $starId, string $rankType)
    {
        $key = $this->buildRankKey($rankType);
        $redis = $this->get_redis(self::MAINTAIN_REDIS_MODULE);
        $data = $redis->zrevrank($key, $starId) + 1;

        return $data;
    }


    /**
     * @param string $rankType
     * @return mixed
     * 获取上榜明星数
     */
    public function getMaintainTaskRankCount(string $rankType)
    {

        $key = $this->buildRankKey($rankType);
        $redis = $this->get_redis(self::MAINTAIN_REDIS_MODULE);
        return $redis->zcard($key);
    }


    /**
     * @param int $targetId
     * @param string $targetType
     * @param int $score
     * @return mixed
     * 更新星能量榜单分值
     */
    public function updateTaskRankScore(int $targetId, string $rankType, int $score)
    {
        $key = $this->buildRankKey($rankType);
        $redis = $this->get_redis(self::MAINTAIN_REDIS_MODULE);
        return $redis->zincrby($key, $score, $targetId);
    }


    private function buildRankKey(string $rankType): string
    {
        return $key = 'task_rank:' . $rankType;
    }


    /**
     * @param int $starId
     * @param string $rankType
     * @return mixed
     * 获取星能量榜单值
     */
    public function getMaintainTaskValue(int $starId, string $rankType)
    {
        $key = $this->buildRankKey($rankType);
        $redis = $this->get_redis(self::MAINTAIN_REDIS_MODULE);
        return $redis->ZSCORE($key, $starId);

    }


    /**
     * @param MaintainTaskRecordDTO $maintainTaskRecordDTO
     * @return int
     * 更新用户打卡记录缓存
     */
    public function updateUserCreateTaskRecord(MaintainTaskCreateDTO $maintainTaskCreateDTO): int
    {

        $key = $this->buildCreateTaskRecordKey($maintainTaskCreateDTO->user_id);
        $score = $maintainTaskCreateDTO->created_timestamp;
        $member = $maintainTaskCreateDTO->task_id;
        $redis = $this->get_redis(self::MAINTAIN_REDIS_MODULE);
        return $redis->zadd($key, $score, $member);

    }


    /**
     * @param int $userId
     * @return string
     */
    private function buildCreateTaskRecordKey(int $userId): string
    {
        return 'maintain_task_record:' . $userId;
    }


    /**
     * @param int $userId
     * @return int|null
     * 获取星能量总值缓存
     */
    public function getUserTaskTotalValue(int $userId): ?int
    {
        $key = $this->buildCreateTaskRecordKey($userId);
        $redis = $this->get_redis(self::MAINTAIN_REDIS_MODULE);
        return $redis->zcard($key);
    }


    /**
     * @param int $userId
     * @param int $starId
     * @return array|null
     * 查询用户在明星下的打卡信息
     */
    public function queryUserTaskHistoryForStar(int $userId, int $starId): ?array
    {
        $mysql = $this->select_mysql_db('main');
        $data = $mysql->select('select * from maintain_user_task_record where user_id=? and star_id=? order by created_timestamp desc ', [$userId, $starId], MaintainTaskRecordDTO::class);

        return $data;

    }


    /**
     * @return int
     * 查询贡献总值
     */
    public function queryTotalValueForStar(int $userId, int $starId): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $data = $mysql->selectOne('select count(*) as sum_count from maintain_user_task_record where user_id=? and star_id=? ', [$userId, $starId]);
        $sum_count = 0;

        if ($data != null) {
            $sum_count = $data->sum_count;
        }
        return $sum_count;
    }


    /**
     * @param int $starId
     * @param int $userId
     * @param int $taskId
     * @return int|null
     * 查询各任务当天完成情况
     */
    public function queryStarTaskFinishStatus(int $starId, int $userId, int $taskId): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $data = $mysql->selectOne('select * from maintain_user_task_record where star_id=? and user_id=? and task_id=? and value =1 and  date_format(from_unixtime(created_timestamp),\'%Y-%m-%d\') = date_format(now(),\'%Y-%m-%d\')  limit 1', [$starId, $userId, $taskId]);

        if ($data != null) {
            return true;
        }
        return false;
    }


    /**
     * @param int $starId
     * @return array|null
     * 查询明星任务下用户打卡人数
     */
    public function queryUserListByStarId(int $starId): ?array
    {
        $mysql = $this->select_mysql_db('main');
        $data = $mysql->select('select distinct(user_id)  from maintain_user_task_record where star_id=? ', [$starId]);
        $array = [];
        if ($data != null) {
            foreach ($data as $key) {
                array_push($array, $key->user_id);
            }
        }

        return $array;

    }
}