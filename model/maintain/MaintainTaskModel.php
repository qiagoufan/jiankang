<?php

namespace model\maintain;

use model\dto\MaintainTaskDTO;

class MaintainTaskModel extends \FUR_Model
{
    public static $_instance;


    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    /**
     * @param MaintainTaskDTO $maintainTaskDTO
     * @return int|null
     * 插入任务
     */
    public function insertRecord(MaintainTaskDTO $maintainTaskDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert('maintain_task', $maintainTaskDTO);
    }


    /**
     * @param int $taskId
     * @return MaintainTaskDTO|null
     * 查询有效任务
     */
    public function queryRecord(int $taskId): ?MaintainTaskDTO
    {

        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne('select * from maintain_task where  id=? and status=1 limit 1 ', [$taskId], MaintainTaskDTO::class);
    }


    /**
     * @param int $index
     * @param int $pageSize
     * @return array|null
     * 获取任务列表
     */
    public function queryAllTaskList(int $offset, int $pageSize): ?array
    {
        $mysql = $this->select_mysql_db('main');
        $data = $mysql->select('select * from maintain_task limit ?,?', [$offset, $pageSize]);

        return $data;
    }
}