<?php 
 
namespace model\hot\dto; 
 
class HotTagDTO { 

	 //primary key
	 public $id;
	 //标签业务类型
	 public $biz_tag_type;
	 //标签业务id
	 public $biz_tag_id;
	 //创建者id
	 public $user_id;
	 //创建时间
	 public $created_timestamp;
 
 
 }