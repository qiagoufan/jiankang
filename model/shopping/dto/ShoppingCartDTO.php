<?php 
 
namespace model\shopping\dto; 
 
class ShoppingCartDTO { 

	 //主键
	 public $id;
	 //用户id
	 public $user_id;
	 //商品id
	 public $item_id;
	 //skuid
	 public $sku_id;
	 //总数
	 public $count;
	 //创建时间
	 public $created_timestamp;
	 //更新时间
	 public $updated_timestamp;
 
 
 }