<?php


namespace model\shoppingCart;


use model\shoppingCart\dto\ShoppingCartDTO;

class ShoppingCartModel extends \FUR_Model
{
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function insertRecord(ShoppingCartDTO $shoppingCartDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert('shopping_cart', $shoppingCartDTO);
    }

    public function queryUserCartCount(int $userId): int
    {
        $mysql = $this->select_mysql_db('main');
        $data = $mysql->selectOne('select count(*) as total_count from shopping_cart where user_id =?', [$userId]);
        if ($data) {
            return $data->total_count;
        }
        return 0;
    }


    public function queryUserCart(int $userId): ?array
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->select('select * from shopping_cart where user_id =? order by updated_timestamp desc ', [$userId], ShoppingCartDTO::class);
    }

    public function updateUserCart(ShoppingCartDTO $shoppingCartDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $data = \FUR_Core::buildArray($shoppingCartDTO);
        return $mysql->update('shopping_cart', $data, ['id' => $shoppingCartDTO->id]);
    }

    public function removeFromUserCart(int $userId, ?int $itemId = 0, ?int $skuId = 0): ?int
    {
        $mysql = $this->select_mysql_db('main');
        if ($skuId) {
            return $mysql->delete('shopping_cart', ['user_id' => $userId, 'sku_id' => $skuId]);
        }
        return $mysql->delete('shopping_cart', ['user_id' => $userId, 'item_id' => $itemId]);

    }

    public function queryUserRecordByProduct(int $userId, ?int $itemId = 0, ?int $skuId = 0): ?ShoppingCartDTO
    {
        $mysql = $this->select_mysql_db('main');
        $record = null;
        if ($skuId) {
            $record = $mysql->selectOne('select * from shopping_cart where user_id =? and sku_id=? ', [$userId, $skuId], ShoppingCartDTO::class);
        } else {
            $record = $mysql->selectOne('select * from shopping_cart where user_id =? and item_id=?', [$userId, $itemId], ShoppingCartDTO::class);
        }
        return $record;
    }


}