<?php


namespace model\shoppingCart\dto;

use facade\request\user\LoginByPhoneParams;
use facade\request\user\RegisterParams;

class ShoppingCartDTO
{
    public int $id;
    public int $user_id;
    public int $item_id;
    public int $sku_id;
    public int $count;
    public int $created_timestamp;
    public int $updated_timestamp;
}