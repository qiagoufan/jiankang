<?php


namespace model\repair;


use model\repair\dto\RepairOrderExtraInfoDTO;

class RepairOrderExtraModel extends \FUR_Model
{
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function insertRecord(RepairOrderExtraInfoDTO $repairOrderExtraInfoDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert('repair_order_extra_info', $repairOrderExtraInfoDTO);
    }


    public function updateExtraInfo(RepairOrderExtraInfoDTO $repairOrderExtraInfoDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->update('repair_order_extra_info', $repairOrderExtraInfoDTO, ['repair_order_id' => $repairOrderExtraInfoDTO->repair_order_id]);
    }

    public function getExtraInfoByOrderId(int $repairOrderId): ?RepairOrderExtraInfoDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne('select * from repair_order_extra_info where repair_order_id =?', [$repairOrderId], RepairOrderExtraInfoDTO::class);
    }

}