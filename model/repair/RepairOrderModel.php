<?php


namespace model\repair;


use facade\request\repair\QueryRepairOrderListParams;
use model\repair\dto\RepairOrderInfoDTO;

class RepairOrderModel extends \FUR_Model
{
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function insertRecord(RepairOrderInfoDTO $repairOrderInfoDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert('repair_order_info', $repairOrderInfoDTO);
    }


    public function updateRepairInfo(RepairOrderInfoDTO $repairOrderInfoDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->update('repair_order_info', $repairOrderInfoDTO, ['id' => $repairOrderInfoDTO->id]);
    }

    public function queryWithdrawAbleAmount(int $engineerId): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $data = $mysql->selectOne('select sum(engineer_reward) as total_amount from repair_order_info where engineer_id=? and cash_out_status=? and order_status=?', [$engineerId, \Config_Const::CASH_OUT_STATUS_NEW, \Config_Const::REPAIR_ORDER_CONFIRM_PRICE]);
        if ($data != null) {
            return $data->total_amount;
        }
        return 0;
    }


    public function getOrderDetail(int $repairOrderId): ?RepairOrderInfoDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne('select * from repair_order_info where id =?', [$repairOrderId], RepairOrderInfoDTO::class);
    }

    public function queryRepairOrderList(QueryRepairOrderListParams $params): ?array
    {
        $mysql = $this->select_mysql_db('main');
        $offset = $params->index * $params->page_size;
        $sql = 'select * from repair_order_info ';
        $condition = 'where 1=1 ';

        if ($params->repair_order_id) {
            $condition .= ' and id =' . $params->repair_order_id;
        }
        if ($params->store_id_list) {
            $storeIdList = implode(",", $params->store_id_list);
            $condition .= ' and store_id in (' . $storeIdList . ') ';
        }
        if ($params->order_status) {
            $condition .= ' and order_status =' . $params->order_status;
        }
        if ($params->invoicing_status !== null) {
            $condition .= ' and invoicing_status =' . $params->invoicing_status;
        }
        if ($params->cash_out_status) {
            $condition .= ' and cash_out_status =' . $params->cash_out_status;
        }
        if ($params->order_category_id) {
            $condition .= ' and order_category_id =' . $params->order_category_id;
        }
        if ($params->order_status_list) {
            $statusList = implode(",", $params->order_status_list);
            $condition .= ' and order_status in (' . $statusList . ') ';
        }
        if ($params->repair_order_id_list) {
            $idList = implode(",", $params->repair_order_id_list);
            $condition .= ' and id in (' . $idList . ') ';
        }
        if ($params->store_id) {
            $condition .= ' and store_id =' . $params->store_id;
        }
        if ($params->engineer_id) {
            $condition .= ' and engineer_id =' . $params->engineer_id;
        }
        if ($params->company_id) {
            $condition .= ' and company_id =' . $params->company_id;
        }
        if ($params->created_timestamp) {
            $condition .= ' and created_timestamp <=' . $params->created_timestamp;
        }
        if ($params->appointment_timestamp) {
            $condition .= ' and appointment_timestamp <=' . $params->appointment_timestamp;
        }
        return $mysql->select($sql . $condition . ' order by id desc  limit ?,?', [$offset, $params->page_size], RepairOrderInfoDTO::class);


    }

    public function queryRepairOrderCount(QueryRepairOrderListParams $params): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $total_count = 0;
        $sql = 'select count(*) as total_count from repair_order_info ';
        $condition = 'where 1=1 ';

        if ($params->repair_order_id) {
            $condition .= ' and id =' . $params->repair_order_id;
        }

        if ($params->store_id_list) {
            $storeIdList = implode(",", $params->store_id_list);
            $condition .= ' and store_id in (' . $storeIdList . ') ';
        }
        if ($params->order_status) {
            $condition .= ' and order_status =' . $params->order_status;
        }
        if ($params->invoicing_status) {
            $condition .= ' and invoicing_status =' . $params->invoicing_status;
        }
        if ($params->cash_out_status) {
            $condition .= ' and cash_out_status =' . $params->cash_out_status;
        }
        if ($params->order_category_id) {
            $condition .= ' and order_category_id =' . $params->order_category_id;
        }
        if ($params->order_status_list) {
            $statusList = implode(",", $params->order_status_list);
            $condition .= ' and order_status in (' . $statusList . ') ';
        }
        if ($params->repair_order_id_list) {
            $idList = implode(",", $params->repair_order_id_list);
            $condition .= ' and id in (' . $idList . ') ';
        }
        if ($params->store_id) {
            $condition .= ' and store_id =' . $params->store_id;
        }
        if ($params->engineer_id) {
            $condition .= ' and engineer_id =' . $params->engineer_id;
        }
        if ($params->company_id) {
            $condition .= ' and company_id =' . $params->company_id;
        }
        if ($params->created_timestamp) {
            $condition .= ' and created_timestamp <=' . $params->created_timestamp;
        }
        if ($params->appointment_timestamp) {
            $condition .= ' and appointment_timestamp <=' . $params->appointment_timestamp;
        }
        $data = $mysql->selectOne($sql . $condition);
        if ($data) {
            $total_count = $data->total_count;
        }
        return $total_count;

    }
}