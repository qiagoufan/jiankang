<?php 
 
namespace model\repair\dto; 
 
class RepairOrderExtraInfoDTO { 

	 //
	 public $id;
	 //维修订单id
	 public $repair_order_id;
	 //维修说明
	 public $repair_instructions;
	 //维修照片
	 public $repair_photos;
	 //维修票据
	 public $repair_bills;
	 //创建时间
	 public $created_timestamp;
	 //更新时间
	 public $updated_timestamp;
 
 
 }