<?php 
 
namespace model\repair\dto; 
 
class RepairOrderInfoDTO { 

	 //
	 public $id;
	 //店长手机号
	 public $store_manager_phone;
	 //店长姓名
	 public $store_manager_name;
	 //公司id
	 public $company_id;
	 //店铺id
	 public $store_id;
	 //工程师id
	 public $engineer_id;
	 //订单版本
	 public $order_version;
	 //订单状态
	 public $order_status;
	 //支付状态
	 public $pay_status;
	 //客户开票状态
	 public $invoicing_status;
	 //预约修理时间
	 public $appointment_timestamp;
	 //订单分类
	 public $order_category_id;
	 //工程师酬劳
	 public $engineer_reward;
	 //工程师提现状态
	 public $cash_out_status;
	 //客户价格
	 public $customer_price;
	 //客户备注
	 public $customer_remark;
	 //联系人
	 public $customer_name;
	 //联系电话
	 public $customer_phone_num;
	 //维修地址
	 public $customer_address;
	 //图片
	 public $images;
	 //创建时间
	 public $created_timestamp;
	 //更新时间
	 public $updated_timestamp;
 
 
 }