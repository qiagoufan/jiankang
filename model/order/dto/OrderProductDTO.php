<?php 
 
namespace model\order\dto; 
 
class OrderProductDTO { 

	 //主键
	 public $id;
	 //订单id
	 public $order_id;
	 //itemid
	 public $item_id;
	 //购买的商品id
	 public $sku_id;
	 //购买数量
	 public $count;
	 //购买时单价(单位:分)
	 public $sku_price;
	 //折扣的金额
	 public $pay_price;
	 //总价(单位:分)
	 public $total_price;
	 //创建时间
	 public $created_timestamp;
	 //更新时间
	 public $updated_timestamp;
	 //sku名称
	 public $sku_name;
	 //购买时商品名称
	 public $product_name;
	 //购买时商品主图
	 public $main_image;
	 //购买时商品规格
	 public $product_specifications;
	 //商品运费价格
	 public $shipping_price;
 
 
 }