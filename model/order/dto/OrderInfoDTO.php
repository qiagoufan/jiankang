<?php 
 
namespace model\order\dto; 
 
class OrderInfoDTO { 

	 //订单id
	 public $id;
	 //购买用户id
	 public $user_id;
	 //交易号
	 public $order_sn;
	 //订单总价(单位:分)单价x数量
	 public $total_price;
	 //外部交易号
	 public $payment_trade_no;
	 //支付价格(单位:分)实际支付价格
	 public $pay_price;
	 //折扣价格
	 public $discount_price;
	 //支付状态0：取消1:未完成, 2:已完成, -1:异常
	 public $pay_status;
	 //支付时间
	 public $pay_timestamp;
	 //支付类型1.支付宝2.微信
	 public $pay_type;
	 //收货人
	 public $receiver;
	 //收货地址
	 public $address;
	 //联系电话
	 public $phone;
	 //发货时间
	 public $delivery_timestamp;
	 //10.待付款20.已付款30.待发货 40.待收货50.交易完成60.交易关闭70.待评价80.已评价
	 public $order_status;
	 //创建时间
	 public $created_timestamp;
	 //变更时间
	 public $updated_timestamp;
	 //订单平台来源
	 public $platform;
	 //评价状态1.已评价0.未评价
	 public $review_status;
	 //1.已删除0.未删除
	 public $delete_status;
	 //省
	 public $province;
	 //市
	 public $city;
	 //区
	 public $area;
	 //关闭类型1.手动关闭2.自动超时关闭
	 public $order_close_type;
	 //快递公司
	 public $express_company;
	 //快递单号
	 public $express_no;
	 //备注
	 public $remark;
	 //是否匿名
	 public $anonymous_status;
	 //使用的优惠券
	 public $voucher_sn;
	 //邮费
	 public $shipping_price;
 
 
 }