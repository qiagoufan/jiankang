<?php 
 
namespace model\order\dto; 
 
class OrderTransactionDTO { 

	 //
	 public $id;
	 //交易单号
	 public $order_sn;
	 //交易的用户ID
	 public $user_id;
	 //交易金额(单位:分)
	 public $total_price;
	 //支付类型 
	 public $pay_type;
	 //0：取消, 1:未完成, 2:已完成, -1:异常
	 public $pay_status;
	 //交易完成时间
	 public $pay_timestamp;
	 //
	 public $created_timestamp;
	 //
	 public $updated_timestamp;
	 //外部交易号
	 public $payment_trade_no;
 
 
 }