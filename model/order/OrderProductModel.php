<?php


namespace model\order;


use model\order\dto\OrderProductDTO;

class OrderProductModel extends \FUR_Model
{
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function insertRecord(OrderProductDTO $orderProductsDTO): int
    {
        $mysql = $this->select_mysql_db('main');
        $result = $mysql->insert('order_product', $orderProductsDTO);
        return $result;
    }


    public function queryOrderProductList(int $orderId): ?array
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->select("select * from order_product where order_id =?  ", [$orderId], OrderProductDTO::class);

    }

}