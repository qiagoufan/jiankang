<?php


namespace model\order;


use facade\request\admin\QueryAllOrderListParams;
use facade\request\order\QueryOrderListParams;
use facade\response\Result;
use model\order\dto\OrderInfoDTO;
use model\order\dto\OrderProductDTO;

class OrderModel extends \FUR_Model
{
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function insertRecord(OrderInfoDTO $orderInfoDTO): int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert('order_info', $orderInfoDTO);

    }


    public function queryOrderInfoById(int $orderId): ?OrderInfoDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select * from order_info where id =? limit 1 ", [$orderId], OrderInfoDTO::class);
    }


    public function queryOrderInfoList(int $userId, int $orderType): array
    {
        $mysql = $this->select_mysql_db('main');
        //获取所有订单
        if ($orderType == QueryOrderListParams::ORDER_TYPE_ALL) {
            return $mysql->select("select  * from order_info where  user_id=? and delete_status=0 order by created_timestamp desc", [$userId], OrderInfoDTO::class);
        }
        //待付款
        if ($orderType == QueryOrderListParams::ORDER_TYPE_TO_PAY) {
            $orderStatus = QueryOrderListParams::ORDER_STATUS_TO_PAY;
        }
        //待发货
        if ($orderType == QueryOrderListParams::ORDER_TYPE_TO_SEND) {
            $orderStatus = QueryOrderListParams::ORDER_STATUS_HAS_PAY;
        }
        //待收货
        if ($orderType == QueryOrderListParams::ORDER_TYPE_TO_RECEIVE) {
            $orderStatus = QueryOrderListParams::ORDER_STATUS_SHIPPED;
        }
        //待评价
        if ($orderType == QueryOrderListParams::ORDER_TYPE_FINISH) {
            $orderStatus = QueryOrderListParams::ORDER_STATUS_FINISHED;
        }


        return $mysql->select("select  * from order_info where  user_id=? and order_status=?  and delete_status=0  order by created_timestamp desc", [$userId, $orderStatus], OrderInfoDTO::class);
    }


    /**
     * @param OrderInfoDTO $orderInfoDTO
     * @return int|null更新订单信息
     */
    public function updateOrderInfoAfterPay(OrderInfoDTO $orderInfoDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $args = ['order_sn' => $orderInfoDTO->order_sn];
        return $mysql->update("order_info", $orderInfoDTO, $args);
    }


    /**
     * @param int $orderId
     * @return int|null删除订单
     */
    public function deleteOrder(int $orderId): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $data = ['delete_status' => 1, 'updated_timestamp' => time()];
        $args = ['id' => $orderId];
        return $mysql->update("order_info", $data, $args);
    }

    /**
     * @param int $orderId
     */
    public function confirmReceiveOrder(int $orderId, int $userId): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $data = ['order_status' => \Config_Const::ORDER_STATUS_FINISHED, 'updated_timestamp' => time()];
        $args = ['id' => $orderId, 'user_id' => $userId];
        return $mysql->update("order_info", $data, $args);
    }


    /**
     * @param OrderInfoDTO $orderInfoDTO
     * @return Result|null取消订单,更新orderInfo
     */
    public function cancelOrder(int $orderId): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $data = ['pay_status' => 0, 'order_status' => \Config_Const::ORDER_STATUS_CLOSED, 'order_close_type' => \Config_Const::ORDER_CLOSE_TYPE_BY_HAND, 'updated_timestamp' => time()];
        $args = ['id' => $orderId];
        return $mysql->update("order_info", $data, $args);
    }


    /**
     * @param int $orderSn
     * @return OrderInfoDTO|null根据ordersn取出订单信息
     */
    public function getOrderByOrderSn($orderSn): ?OrderInfoDTO
    {
        $mysql = $this->select_mysql_db('main');
        $result = $mysql->selectOne("select * from order_info where order_sn =? limit 1 ", [$orderSn], OrderInfoDTO::class);
        return $result;
    }


    public function updateOrderStatusAfterComment(int $orderId): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $data = ['order_status' => \Config_Const::ORDER_STATUS_HAS_COMMENT, 'updated_timestamp' => time()];
        $args = ['id' => $orderId];
        return $mysql->update("order_info", $data, $args);

    }

    /**
     * @param int $userId
     * @return OrderInfoDTO|null
     * 获取订单提醒数
     */
    public function queryOrderRemindNum(int $userId): ?OrderInfoDTO
    {
        $mysql = $this->select_mysql_db('main');
        $data = $mysql->selectOne("select COUNT(if(order_status=10,true,null)) AS wait_pay,COUNT(if(order_status=40,true,null)) AS wait_receive, COUNT(if(order_status=70,true,null)) AS wait_review,   COUNT(if(order_status=20,true,null)) AS wait_ship   from order_info where user_id=?
and delete_status=0 ", [$userId], OrderInfoDTO::class);
        if ($data != null) {
            return $data;
        }
    }


    /**
     * @return array
     * 获取所有订单列表
     */

    public function queryAllOrderList(QueryAllOrderListParams $params): ?array
    {
        $order_sn = $params->order_sn;
        $index = $params->index;
        $pageSize = $params->page_size;
        $offset = $index * $pageSize;
        $mysql = $this->select_mysql_db('main');


        $sql = 'select * from  order_info where delete_status = 0 ';
        if ($order_sn) {
            $order_sn = "'%$order_sn%'";
            $sql .= " and order_sn like $order_sn ";
        }
        if ($params->query_time_type) {
            $query_time_type = $params->query_time_type;
            if ($params->start_timestamp) {
                $sql .= " and $query_time_type >= " . $params->start_timestamp;
            }
            if ($params->end_timestamp) {
                $sql .= " and $query_time_type <= " . $params->end_timestamp;
            }
        }
        if ($params->order_status_type != QueryOrderListParams::ORDER_TYPE_ALL) {
            //待付款
            if ($params->order_status_type == QueryOrderListParams::ORDER_TYPE_TO_PAY) {
                $sql .= " and order_status = " . QueryOrderListParams::ORDER_STATUS_TO_PAY;
            }
            //待发货
            if ($params->order_status_type == QueryOrderListParams::ORDER_TYPE_TO_SEND) {
                $sql .= " and order_status = " . QueryOrderListParams::ORDER_STATUS_HAS_PAY;
            }
            //已发货
            if ($params->order_status_type == QueryOrderListParams::ORDER_TYPE_TO_RECEIVE) {
                $sql .= " and order_status = " . QueryOrderListParams::ORDER_STATUS_SHIPPED;
            }
            //完成
            if ($params->order_status_type == QueryOrderListParams::ORDER_TYPE_FINISH) {
                $sql .= " and order_status >=" . QueryOrderListParams::ORDER_STATUS_FINISHED;
            }
        }

        return $mysql->select($sql . " order by id desc  limit ?,?", [$offset, $pageSize], OrderInfoDTO::class);
    }

    /**
     * @return int|null
     * 管理后台查询订单总数
     */
    public function queryOrderListCount(QueryAllOrderListParams $params): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $order_sn = $params->order_sn;
        $mysql = $this->select_mysql_db('main');


        $sql = 'select count(*) as  total_count from  order_info where delete_status = 0 ';
        if ($order_sn) {
            $order_sn = "'%$order_sn%'";
            $sql .= " and order_sn like $order_sn ";
        }
        if ($params->query_time_type) {
            $query_time_type = $params->query_time_type;
            if ($params->start_timestamp) {
                $sql .= " and $query_time_type >= " . $params->start_timestamp;
            }
            if ($params->end_timestamp) {
                $sql .= " and $query_time_type <= " . $params->end_timestamp;
            }
        }
        if ($params->order_status_type != QueryOrderListParams::ORDER_TYPE_ALL) {
            //待付款
            if ($params->order_status_type == QueryOrderListParams::ORDER_TYPE_TO_PAY) {
                $sql .= " and order_status = " . QueryOrderListParams::ORDER_STATUS_TO_PAY;
            }
            //待发货
            if ($params->order_status_type == QueryOrderListParams::ORDER_TYPE_TO_SEND) {
                $sql .= " and order_status = " . QueryOrderListParams::ORDER_STATUS_HAS_PAY;
            }
            //已发货
            if ($params->order_status_type == QueryOrderListParams::ORDER_TYPE_TO_RECEIVE) {
                $sql .= " and order_status = " . QueryOrderListParams::ORDER_STATUS_SHIPPED;
            }
            //完成
            if ($params->order_status_type == QueryOrderListParams::ORDER_TYPE_FINISH) {
                $sql .= " and order_status >=" . QueryOrderListParams::ORDER_STATUS_FINISHED;
            }
        }

        $totalCount = 0;
        $data = $mysql->selectOne($sql);
        if ($data) {
            $totalCount = $data->total_count;
        }
        return $totalCount;
    }


    public function deliverOrder(string $orderSn, string $expressCompany, string $expressNo): ?bool
    {
        $mysql = $this->select_mysql_db('main');
        $data = ['order_status' => \Config_Const::ORDER_STATUS_WAIT_RECEIVE,
            'updated_timestamp' => time(),
            'express_company' => $expressCompany,
            'express_no' => $expressNo,
            'delivery_timestamp' => time()
        ];
        $args = ['order_sn' => $orderSn];
        return $mysql->update("order_info", $data, $args);
    }


    /**
     * @param int $orderId
     * @return OrderProductDTO|null
     * 根据订单id查询订单产品信息
     */
    public function queryOrderProductByOrderId(int $orderId): ?OrderProductDTO
    {
        $mysql = $this->select_mysql_db('main');
        $data = $mysql->selectOne("select * from  order_products where order_id =? limit 1 ", [$orderId], OrderProductDTO::class);
        return $data;

    }


    /**
     * @return array|null
     * 获取超时未支付订单
     */
    public function queryUnpaidOrders(): ?array
    {
        $orderStatus = OrderInfoDTO::ORDER_STATUS_TO_PAY;
        $payStatus = OrderInfoDTO::PAY_STATUS_UNFINISHED;
        $mysql = $this->select_mysql_db('main');
        $time = time() - 1800;//默认超过30min关闭
        $data = $mysql->select("select * from  order_info where order_status =? and pay_status =? and created_timestamp <?", [$orderStatus, $payStatus, $time], OrderInfoDTO::class);
        return $data;
    }

    /**
     * @param int $orderId
     * @return bool|null
     * 关闭未支付订单
     */
    public function updateOrderClosed(int $orderId): ?bool
    {
        $mysql = $this->select_mysql_db('main');
        $args = ['id' => $orderId];
        $data = ['order_status' => OrderInfoDTO::ORDER_STATUS_CLOSED, 'pay_status' => OrderInfoDTO::PAY_STATUS_CANCEL, 'order_close_type' => OrderInfoDTO::ORDER_CLOSE_TYPE_AUTO, 'updated_timestamp' => time()];
        return $mysql->update("order_info", $data, $args);

    }

    public function getUserProcessingOrderCount(int $userId): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $row = $mysql->selectOne("select count(*) as count from order_info where user_id =?  and  order_status <?  and order_status > ? and delete_status = 0  ", [$userId, OrderInfoDTO::ORDER_STATUS_FINISHED, OrderInfoDTO::ORDER_STATUS_TO_PAY]);
        return $row->count;
    }


    /**
     * @param int $userId
     * @return OrderInfoDTO|null
     * 管理后台获取用户已支付订单信息
     */
    public function getOrderPaidByUserId(int $userId): ?OrderInfoDTO
    {
        $mysql = $this->select_mysql_db('main');
        $row = $mysql->selectOne("select count(*)as total_order_number, sum(pay_price) as total_consumption ,max(created_timestamp)as latest_order_time from order_info where user_id =? and pay_status = 2  ", [$userId], OrderInfoDTO::class);
        return $row;
    }


    public function queryOrderExportList(string $startTime, string $endTime): ?array
    {
        $mysql = $this->select_mysql_db('main');
        $data = $mysql->select("select * from order_info where created_timestamp between ? and ? order by updated_timestamp desc", [$startTime, $endTime], OrderInfoDTO::class);

        return $data;
    }

    public function queryUserOrderCount(int $userId): ?int
    {

        $totalCount = 0;
        $mysql = $this->select_mysql_db('main');
        $data = $mysql->selectOne("select  COUNT(*) as total_count from order_info where user_id =? and order_status != 0", [$userId]);
        if ($data) {
            $totalCount = $data->total_count;
        }
        return $totalCount;
    }


}