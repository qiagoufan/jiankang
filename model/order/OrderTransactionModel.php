<?php

namespace model\order;

use model\order\dto\OrderTransactionDTO;


class OrderTransactionModel extends \FUR_Model
{
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function insertRecord(OrderTransactionDTO $orderTransactionDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert('order_transaction', $orderTransactionDTO);
    }


    public function queryRecord(int $order_sn): ?OrderTransactionDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select * from order_transaction where order_sn =? limit 1 ", [$order_sn], OrderTransactionDTO::class);
    }

}