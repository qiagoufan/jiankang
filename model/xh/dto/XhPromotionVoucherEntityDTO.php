<?php 
 
namespace model\xh\dto; 
 
class XhPromotionVoucherEntityDTO { 

	 //primary key
	 public $id;
	 //优惠券id
	 public $voucher_id;
	 //用户id
	 public $user_id;
	 //优惠码
	 public $voucher_code;
	 //使用状态
	 public $use_status;
	 //使用时间
	 public $use_timestamp;
	 //优惠券sn码
	 public $voucher_sn;
	 //创建时间
	 public $created_timestamp;
	 //更新时间
	 public $updated_timestamp;
	 //???
	 public $order_id;
	 //????
	 public $received_timestamp;
	 //??????
	 public $start_timestamp;
	 //??????
	 public $expire_timestamp;
 
 
 }