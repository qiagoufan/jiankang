<?php 
 
namespace model\group\dto; 
 
class GroupManagerDTO { 

	 //primary key
	 public $id;
	 //ip的id
	 public $star_id;
	 //用户id
	 public $manager_user_id;
	 //类型 1-owner,2-管理员
	 public $manager_type;
	 //创建时间
	 public $created_timestamp;
	 //更新时间
	 public $updated_timestamp;
 
 
 }