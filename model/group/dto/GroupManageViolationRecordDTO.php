<?php 
 
namespace model\group\dto; 
 
class GroupManageViolationRecordDTO { 

	 //primary key
	 public $id;
	 //业务类型
	 public $biz_type;
	 //业务id
	 public $biz_id;
	 //操作原因
	 public $reason;
	 //操作人id
	 public $operator_id;
	 //starid
	 public $star_id;
	 //附加说明
	 public $additional_desc;
	 //创建时间
	 public $created_timestamp;
	 //更新时间
	 public $updated_timestamp;
 
 
 }