<?php


namespace model\group;

use model\group\dto\GroupManagerApplyFormDTO;
use model\group\dto\GroupManagerDTO;
use model\group\dto\GroupManageViolationRecordDTO;

class GroupModel extends \FUR_Model
{
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    /**
     * 创建申请表
     * @param GroupManagerApplyFormDTO $groupManagerApplyFormDTO
     * @return int|null
     */
    public function createApplyForm(GroupManagerApplyFormDTO $groupManagerApplyFormDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert('group_manager_apply_form', $groupManagerApplyFormDTO);
    }

    public function createGroupManager(GroupManagerDTO $groupManagerDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert('group_manager', $groupManagerDTO);
    }

    public function createViolationRecord(GroupManageViolationRecordDTO $groupManageViolationRecordDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert('group_manage_violation_record', $groupManageViolationRecordDTO);
    }

    /**
     * 查询管理员列表
     * @param int $starId
     * @return array|null
     */
    public function queryAllManagerList(int $starId): ?array
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->select('select * from group_manager where star_id =?  order by created_timestamp desc ', [$starId,], GroupManagerDTO::class);
    }

    /**
     * 查询圈主信息
     * @param int $starId
     * @return array|null
     */
    public function queryGroupOwner(int $starId): ?GroupManagerDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne('select * from group_manager where star_id =?  and  manager_type =? ', [$starId, GroupManagerDTO::MANAGER_TYPE_OWNER], GroupManagerDTO::class);
    }


    /**
     * 查询管理员数量
     * @param int $starId
     * @param int $managerType
     * @return int|null
     */
    public function queryManagerNum(int $starId, int $managerType): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $data = $mysql->selectOne('select count(*) as total_count from group_manager where star_id =? and manager_type =? ', [$starId, $managerType]);
        if ($data == null) {
            return 0;
        }
        return $data->total_count;

    }


    /**
     * 查看用户管理员身份
     * @param int $starId
     * @param int $userId
     * @return GroupManagerDTO|null
     */
    public function queryUserManagerInfo(int $starId, int $userId): ?GroupManagerDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne('select * from group_manager where star_id =? and manager_user_id =? ', [$starId, $userId], GroupManagerDTO::class);
    }

    /**
     * 查询用户的申请历史表
     * @param int $userId
     * @return array|null
     */
    public function queryUserApplyHistoryList(int $userId): ?array
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->select('select * from group_manager_apply_form where user_id =? order by created_timestamp desc limit 50', [$userId], GroupManagerApplyFormDTO::class);
    }


    /**
     * 查询管理员申请表
     * @param int $starId
     * @return array|null
     */
    public function queryManagerApplyFormList(int $starId): ?array
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->select('select * from group_manager_apply_form where star_id =? and form_type =? and form_status =1 order by created_timestamp asc limit 50 ', [$starId, GroupManagerDTO::MANAGER_TYPE_MANAGER], GroupManagerApplyFormDTO::class);
    }

    /**
     * 查询圈主申请表
     * @param int $starId
     * @return array|null
     */
    public function queryOwnerApplyFormList(int $offset, int $pageSzie): ?array
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->select('select * from group_manager_apply_form where form_type =?  order by created_timestamp desc limit ?,? ', [GroupManagerDTO::MANAGER_TYPE_OWNER, $offset, $pageSzie], GroupManagerApplyFormDTO::class);
    }

    public function queryOwnerApplyFormCount(): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $data = $mysql->selectOne('select count(*) as total_count from group_manager_apply_form where  form_type =?  ', [GroupManagerDTO::MANAGER_TYPE_OWNER], GroupManagerApplyFormDTO::class);
        return $data->total_count;
    }


    /**
     * @param int $applyFormId
     * @return array|null
     */
    public function queryApplyFormDetail(int $applyFormId): ?GroupManagerApplyFormDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne('select * from group_manager_apply_form where id =?  ', [$applyFormId], GroupManagerApplyFormDTO::class);
    }


    public function updateApplyFormStatus(int $applyFormId, int $applyFormStatus): ?int
    {
        $mysql = $this->select_mysql_db('main');

        $args = ['id' => $applyFormId];
        $data = ['form_status' => $applyFormStatus, 'updated_timestamp' => time()];
        return $mysql->update("group_manager_apply_form", $data, $args);
    }


    /**
     * 删除管理员账号
     * @param int $managerUserId
     * @param int $starId
     * @return int|null
     */
    public function deleteGroupManager(int $managerUserId, int $starId): ?int
    {
        $mysql = $this->select_mysql_db('main');

        $args = ['star_id' => $starId, 'manager_user_id' => $managerUserId];
        $data = $mysql->delete('group_manager ', $args);
        if ($data == null) {
            return 0;
        }
        return $data->total_count;

    }

    public function cleanUserGroupManager(int $managerUserId): ?int
    {
        $mysql = $this->select_mysql_db('main');

        $args = ['manager_user_id' => $managerUserId];
        $data = $mysql->delete('group_manager ', $args);
        if ($data == null) {
            return 0;
        }
        return $data->total_count;

    }
}