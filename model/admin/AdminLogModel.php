<?php

namespace model\admin;

use model\dto\AdminLogDTO;

class AdminLogModel extends \FUR_Model
{
    public static $_instance;


    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function insertRecord(AdminLogDTO $adminLogDTO)
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert('admin_log', $adminLogDTO);
    }

}