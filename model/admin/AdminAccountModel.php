<?php

namespace model\admin;

use model\admin\dto\AdminAccountDTO;

class AdminAccountModel extends \FUR_Model
{
    public static $_instance;


    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function queryByAccount(string $account): ?AdminAccountDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne('select * from admin_account where account_name=?', [$account], AdminAccountDTO::class);
    }

    public function queryByPhone(string $phone): ?AdminAccountDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne('select * from admin_account where phone=?', [$phone], AdminAccountDTO::class);
    }


    public function queryByAdminAccountId(int $adminAccountId): ?AdminAccountDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne('select * from admin_account where id=?', [$adminAccountId], AdminAccountDTO::class);
    }
    public function updatePassword(string $account, string $password): ?int
    {
        $adminAccountDTO = new AdminAccountDTO();
        $adminAccountDTO->account_name = $account;
        $adminAccountDTO->password = $password;
        $adminAccountDTO->updated_timestamp = time();
        $mysql = $this->select_mysql_db('main');

        return $mysql->update('admin_account', $adminAccountDTO, ['account_name' => $account]);
    }

}