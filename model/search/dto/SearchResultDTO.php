<?php


namespace model\search\dto;

use facade\request\user\LoginByPhoneParams;
use facade\request\user\RegisterParams;

class SearchResultDTO
{
    public $id;
    public $content;
    public $sort;
    public $content_type;
}