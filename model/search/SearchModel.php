<?php


namespace model\search;

use model\search\dto\SearchResultDTO;

class SearchModel extends \FUR_Model
{
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function getItemRelatedContent(?string $keyword, int $offset = 0, int $pageSize = 5): array
    {
        $mysql = $this->select_mysql_db('main');
        $keyword = '%' . $keyword . '%';
        return $mysql->select('select any_value(product_name) as content,any_value(id)  as id ,is_super  from  product_sku  where summary  like ? or product_name like ? group by item_id  limit ?,?  ',
            [$keyword, $keyword, $offset, $pageSize],
            SearchResultDTO::class);
    }

    public function getStarRelatedContent(?string $keyword, int $offset = 0, int $pageSize = 5): array
    {
        $mysql = $this->select_mysql_db('main');
        $keyword = '%' . $keyword . '%';
        return $mysql->select('select star_name as content,id from  user_star  where star_name  like ?   limit ?,? ',   [$keyword, $offset, $pageSize],
            SearchResultDTO::class);
    }


    public function getSuperItemRelatedContent(?string $keyword, int $offset = 0, int $pageSize = 5): array
    {
        $mysql = $this->select_mysql_db('main');
        if ($keyword) {
            $keyword = "%" . $keyword . "%";
            return $mysql->select('select any_value(product_name) as content,any_value(id)  as id  from  product_sku  where product_name like ? and  is_super=1 group by item_id  limit ?,?  ',
                [$keyword, $offset, $pageSize],
                SearchResultDTO::class);
        } else {
            return $mysql->select('select any_value(product_name) as content,any_value(id)  as id  from  product_sku  where is_super=1 group by item_id  limit ?,?',
                [$offset, $pageSize],
                SearchResultDTO::class);
        }

    }
}