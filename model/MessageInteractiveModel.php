<?php


namespace model;


use model\dto\InteractiveLikeDTO;
use model\dto\MessageInteractiveDTO;

class MessageInteractiveModel extends \FUR_Model
{
    public static $_instance;
    const USER_REDIS_MODULE = 'user';

    const MESSAGE_INTERACTIVE_LIKE_TYPE = 'like';
    const MESSAGE_INTERACTIVE_COMMENT_TYPE = 'comment';
    const MESSAGE_INTERACTIVE_REPLY_TYPE = 'reply';

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function insertRecord(MessageInteractiveDTO $messageInteractiveDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert('message_interactive', $messageInteractiveDTO);
    }


    /**
     * 根据回复id查询记录
     * @param int $replyId
     * @return
     */
    public function queryRecordById(int $replyId): ?MessageInteractiveDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select * from message_interactive where id =? limit 1 ", [$replyId], MessageInteractiveDTO::class);
    }


    /**
     * @param int $interactiveType
     * @param int $receiveUserId
     * @return MessageInteractiveDTO|null
     * 查询互动消息列表
     */
    public function queryInteractiveMessageList(int $interactiveType, int $receiveUserId, int $index, int $pageSize): ?array
    {
        $receiveUserId = 1000015;
        $mysql = $this->select_mysql_db('main');
        return $mysql->select("select * from message_interactive where interactive_type=? and receive_user_id=? limit ? offset ?", [$interactiveType, $receiveUserId, $pageSize, $index * $pageSize], MessageInteractiveDTO::class);
    }


    /**
     * @param int $userId
     * @return bool|null
     * 更新用户未读信息
     */
    public function updateUserUnreadMessage(int $userId): ?bool
    {
        $mysql = $this->select_mysql_db('main');
        $data = ['read_status' => MessageInteractiveDTO::READ_STATUS_TRUE, 'updated_timestamp' => time()];
        return $mysql->update("message_interactive", $data, ['receive_user_id' => $userId, 'read_status' => MessageInteractiveDTO::READ_STATUS_FALSE]);

    }


    public function buildInteractiveMsgCountKey(int $userId)
    {
        return 'user_interactive_message_count:' . $userId;
    }


    public function updateInteractiveMessageCount(int $userId): ?int
    {

        $key = $this->buildInteractiveMsgCountKey($userId);
        $field = self::MESSAGE_INTERACTIVE_LIKE_TYPE;
        $redis = $this->get_redis(self::USER_REDIS_MODULE);
        return $redis->hincrby($key, $field, 1);

    }


    public function clearInteractiveMessageCount(int $userId): ?int
    {
        $key = $this->buildInteractiveMsgCountKey($userId);
        $field = self::MESSAGE_INTERACTIVE_LIKE_TYPE;
        $redis = $this->get_redis(self::USER_REDIS_MODULE);
        return $redis->hset($key, $field, 0);
    }


    public function getInteractiveMessageCount(int $userId): ?int
    {
        $key = $this->buildInteractiveMsgCountKey($userId);
        $field = self::MESSAGE_INTERACTIVE_LIKE_TYPE;
        $redis = $this->get_redis(self::USER_REDIS_MODULE);
        return $redis->hget($key, $field);
    }

}