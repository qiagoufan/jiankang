<?php


namespace model\security;


use facade\request\PageParams;
use model\security\dto\SecurityWordDTO;

class SecurityWordModel extends \FUR_Model
{
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function getSecurityWord($id): ?SecurityWordDTO
    {

        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select * from security_word where id =?", [$id], SecurityWordDTO::class);
    }

    public function insertSecurityWord(?SecurityWordDTO $securityWordDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert("security_word", $securityWordDTO);
    }

    public function deleteSecurityWord($id): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->delete("security_word", ['id' => $id]);
    }

    public function updateSecurityWord(?SecurityWordDTO $securityWordDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->update("security_word", $securityWordDTO, ['id' => $securityWordDTO->id]);

    }

    public function getAllSecurityWordList(): ?array
    {
        $mysql = $this->select_mysql_db('main');
        $sql = 'select * from security_word ';
        return $mysql->select($sql, [], SecurityWordDTO::class);
    }


    public function getSecurityWordList(PageParams $pageParams): ?array
    {
        $mysql = $this->select_mysql_db('main');
        $offset = $pageParams->page_size * $pageParams->index;
        $pageSize = $pageParams->page_size;
        $sql = 'select * from security_word where 1=1 ';
        $args = [];

        $sql .= ' order by id desc limit ?,?';
        $args = array_merge($args, [$offset, $pageSize]);
        return $mysql->select($sql, $args, SecurityWordDTO::class);

    }


    public function getSecurityWordCount(PageParams $pageParams): ?int
    {
        $mysql = $this->select_mysql_db('main');

        $totalCount = 0;
        $sql = 'select count(*) as total_count from security_word where 1=1 ';
        $args = [];

        $data = $mysql->selectOne($sql, $args);
        if ($data) {
            $totalCount = $data->total_count;
        }
        return $totalCount;

    }

}