<?php 
 
namespace model\verification\dto; 
 
class VerificationCodeDTO { 

	 //主键
	 public $id;
	 //用户id
	 public $user_id;
	 //验证码
	 public $code;
	 //过期时间
	 public $expire_timestamp;
	 //手机号码
	 public $phone;
	 //验证动作
	 public $action;
	 //创建时间
	 public $created_timestamp;
	 //错误次数
	 public $error_count;
 
 
 }