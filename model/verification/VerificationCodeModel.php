<?php

namespace model\verification;


use model\verification\dto\VerificationCodeDTO;

class VerificationCodeModel extends \FUR_Model
{
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function queryRecordByPhone(string $phone, string $action): ?VerificationCodeDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select * from verification_code where phone = ? and action =? order by id desc limit 1", [$phone, $action], VerificationCodeDTO::class);
    }

    public function insertRecord(VerificationCodeDTO $verificationCodeDTO): int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert('verification_code', $verificationCodeDTO);
    }

    public function updateRecordErrorCount(VerificationCodeDTO $verificationCodeDTO): int
    {
        $mysql = $this->select_mysql_db('main');
        $data = ['error_count' => $verificationCodeDTO->error_count + 1];
        $args = ['id' => $verificationCodeDTO->id];
        return $mysql->update('verification_code', $data, $args);
    }

    public function queryRecordCount(string $phone, string $action, int $todayTimestamp): int
    {
        $rowCount = 0;
        $mysql = $this->select_mysql_db('main');
        $data = $mysql->selectOne("select count(*) as row_count from verification_code where phone = ? and action = ? and created_timestamp > ?", [$phone, $action, $todayTimestamp]);
        if ($data != null) {
            $rowCount = $data->row_count;
        }
        return $rowCount;
    }

    public function deleteCode(string $phone, string $code)
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->delete("verification_code", ['phone' => $phone, 'code' => $code]);
    }


}