<?php 
 
namespace model\check\dto; 
 
class CheckInTaskDTO { 

	 //
	 public $id;
	 //打卡id
	 public $check_in_type_id;
	 //打卡任务名称
	 public $check_in_task_name;
	 //打卡任务类型
	 public $check_in_task_type;
	 //打卡数值
	 public $task_value;
	 //打卡数值单位
	 public $task_value_unit;
	 //创建时间
	 public $created_timestamp;
	 //更新时间
	 public $updated_timestamp;
 
 
 }