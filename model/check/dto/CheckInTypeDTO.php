<?php 
 
namespace model\check\dto; 
 
class CheckInTypeDTO { 

	 //主键
	 public $id;
	 //打卡类型
	 public $check_in_type;
	 //状态
	 public $status;
	 //创建时间
	 public $created_timestamp;
	 //更新时间
	 public $updated_timestamp;
 
 
 }