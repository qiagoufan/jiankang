<?php 
 
namespace model\check\dto; 
 
class CheckInTaskRecordDTO { 

	 //主键
	 public $id;
	 //任务id
	 public $check_in_task_id;
	 //用户id
	 public $user_id;
	 //任务完成情况
	 public $task_satus;
	 //任务得分
	 public $task_score;
	 //打卡日期
	 public $check_in_date;
	 //创建时间
	 public $created_timestamp;
	 //更新时间
	 public $updated_timestamp;
 
 
 }