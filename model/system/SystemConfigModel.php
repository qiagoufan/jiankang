<?php


namespace model\system;


use model\system\dto\SystemConfigDTO;

class SystemConfigModel extends \FUR_Model
{
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function insertConfig(SystemConfigDTO $systemConfigDTO): int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert('system_config', $systemConfigDTO);
    }


    public function updateConfig(SystemConfigDTO $systemConfigDTO): int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->update('system_config', $systemConfigDTO, ['config_key' => $systemConfigDTO->config_key]);
    }


    public function queryConfigList(): ?array
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->select('select * from system_config', [], SystemConfigDTO::class);
    }


    public function queryConfigDetail(string $key): ?SystemConfigDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne('select * from system_config where config_key =?', [$key], SystemConfigDTO::class);
    }
}