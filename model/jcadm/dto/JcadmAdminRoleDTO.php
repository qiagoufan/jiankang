<?php 
 
namespace model\jcadm\dto; 
 
class JcadmAdminRoleDTO { 

	 //
	 public $id;
	 //名称
	 public $name;
	 //简介
	 public $description;
	 //权限
	 public $url;
	 //是否启用
	 public $status;
 
 
 }