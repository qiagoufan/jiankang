<?php 
 
namespace model\jcadm\dto; 
 
class JcadmUserLevelDTO { 

	 //
	 public $id;
	 //名称
	 public $name;
	 //简介
	 public $description;
	 //图片
	 public $img;
	 //是否启用
	 public $status;
	 //创建时间
	 public $create_time;
	 //更新时间
	 public $update_time;
	 //删除时间
	 public $delete_time;
 
 
 }