<?php 
 
namespace model\jcadm\dto; 
 
class JcadmAdminUserDTO { 

	 //
	 public $id;
	 //用户名
	 public $username;
	 //密码
	 public $password;
	 //昵称
	 public $nickname;
	 //头像
	 public $avatar;
	 //角色
	 public $role;
	 //是否启用
	 public $status;
	 //删除时间
	 public $delete_time;
 
 
 }