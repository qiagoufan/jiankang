<?php 
 
namespace model\jcadm\dto; 
 
class JcadmSettingGroupDTO { 

	 //
	 public $id;
	 //作用模块
	 public $module;
	 //名称
	 public $name;
	 //描述
	 public $description;
	 //代码
	 public $code;
	 //排序
	 public $sort_number;
	 //自动生成菜单
	 public $auto_create_menu;
	 //自动生成配置文件
	 public $auto_create_file;
	 //图标
	 public $icon;
	 //创建时间
	 public $create_time;
	 //更新时间
	 public $update_time;
	 //删除时间
	 public $delete_time;
 
 
 }