<?php 
 
namespace model\jcadm\dto; 
 
class JcadmAttachmentDTO { 

	 //
	 public $id;
	 //后台用户ID
	 public $admin_user_id;
	 //前台用户ID
	 public $user_id;
	 //原文件名
	 public $original_name;
	 //保存文件名
	 public $save_name;
	 //系统完整路径
	 public $save_path;
	 //系统完整路径
	 public $url;
	 //后缀
	 public $extension;
	 //类型
	 public $mime;
	 //大小
	 public $size;
	 //MD5
	 public $md5;
	 //SHA1
	 public $sha1;
	 //创建时间
	 public $create_time;
	 //更新时间
	 public $update_time;
	 //删除时间
	 public $delete_time;
 
 
 }