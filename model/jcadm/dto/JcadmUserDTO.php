<?php 
 
namespace model\jcadm\dto; 
 
class JcadmUserDTO { 

	 //
	 public $id;
	 //头像
	 public $avatar;
	 //用户名
	 public $username;
	 //昵称
	 public $nickname;
	 //手机号
	 public $mobile;
	 //用户等级
	 public $user_level_id;
	 //密码
	 public $password;
	 //是否启用
	 public $status;
	 //创建时间
	 public $create_time;
	 //更新时间
	 public $update_time;
	 //删除时间
	 public $delete_time;
 
 
 }