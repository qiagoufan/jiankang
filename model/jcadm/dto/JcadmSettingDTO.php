<?php 
 
namespace model\jcadm\dto; 
 
class JcadmSettingDTO { 

	 //
	 public $id;
	 //所属分组
	 public $setting_group_id;
	 //名称
	 public $name;
	 //描述
	 public $description;
	 //代码
	 public $code;
	 //设置配置及内容
	 public $content;
	 //排序
	 public $sort_number;
	 //创建时间
	 public $create_time;
	 //更新时间
	 public $update_time;
	 //删除时间
	 public $delete_time;
 
 
 }