<?php 
 
namespace model\jcadm\dto; 
 
class JcadmAdminMenuDTO { 

	 //
	 public $id;
	 //父级菜单
	 public $parent_id;
	 //名称
	 public $name;
	 //url
	 public $url;
	 //图标
	 public $icon;
	 //等级
	 public $is_show;
	 //排序
	 public $sort_id;
	 //记录日志方法
	 public $log_method;
 
 
 }