<?php 
 
namespace model\withdraw\dto; 
 
class WithdrawRecordDTO { 

	 //
	 public $id;
	 //维修单
	 public $repair_order_id_list;
	 //工程师id
	 public $engineer_id;
	 //提现金额
	 public $withdraw_total_amount;
	 //提现卡号
	 public $withdraw_card_num;
    //提现卡姓名
    public $withdraw_card_real_name;
    //提现卡姓名
    public $withdraw_card_bank;
	 //结算状态
	 public $cash_out_status;
	 //创建时间
	 public $created_timestamp;
	 //更新时间
	 public $updated_timestamp;
 
 
 }