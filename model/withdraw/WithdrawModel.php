<?php


namespace model\withdraw;


use facade\request\withdraw\QueryWithdrawListParams;
use model\withdraw\dto\WithdrawRecordDTO;

class WithdrawModel extends \FUR_Model
{
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function insertRecord(WithdrawRecordDTO $withdrawRecordDTO): ?int
    {

        $mysql = $this->select_mysql_db('main');
        return $mysql->insert('withdraw_record', $withdrawRecordDTO);
    }


    public function updateWithdrawRecord(WithdrawRecordDTO $withdrawRecordDTO): ?int
    {

        $mysql = $this->select_mysql_db('main');
        return $mysql->update('withdraw_record', $withdrawRecordDTO, ['id' => $withdrawRecordDTO->id]);
    }


    public function queryRecordById(int $withdrawId): ?WithdrawRecordDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne('select * from withdraw_record where id=? limit 1', [$withdrawId], WithdrawRecordDTO::class);
    }

    public function queryEngineerWithdrawRecordList(int $engineerId, int $index, int $pageSize): ?array
    {
        $mysql = $this->select_mysql_db('main');
        $offset = $index * $pageSize;
        return $mysql->select('select * from withdraw_record where engineer_id=? order by id desc limit ?,?', [$engineerId, $offset, $pageSize], WithdrawRecordDTO::class);

    }


    public function queryWithdrawRecordList(QueryWithdrawListParams $params): ?array
    {
        $mysql = $this->select_mysql_db('main');
        $pageSize = $params->page_size;
        $offset = $params->index * $pageSize;
        $condition = 'where 1=1 ';

        if ($params->engineer_id) {
            $condition .= ' and engineer_id =' . $params->engineer_id;
        }
        if ($params->cash_out_status) {
            $condition .= ' and cash_out_status =' . $params->cash_out_status;
        }
        return $mysql->select('select * from withdraw_record ' . $condition . ' order by id desc limit ?,?', [$offset, $pageSize], WithdrawRecordDTO::class);

    }

    public function queryWithdrawRecordCount(QueryWithdrawListParams $params): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $total_count = 0;
        $sql = 'select count(*) as total_count from withdraw_record ';
        $condition = 'where 1=1 ';

        if ($params->engineer_id) {
            $condition .= ' and engineer_id =' . $params->engineer_id;
        }
        if ($params->cash_out_status) {
            $condition .= ' and cash_out_status =' . $params->cash_out_status;
        }
        $data = $mysql->selectOne($sql . $condition);
        if ($data) {
            $total_count = $data->total_count;
        }

        return $total_count;

    }

}