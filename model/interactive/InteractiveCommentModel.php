<?php


namespace model\interactive;

use facade\request\interactive\QueryInteractiveCommentListParams;
use model\interactive\dto\InteractiveCommentDTO;

class InteractiveCommentModel extends \FUR_Model
{
    public static $_instance;

    const INTERACTIVE_REDIS_MODULE = 'interactive';
    const INTERACTIVE_COMMENT_TYPE = 'comment';

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function insertRecord(InteractiveCommentDTO $interactiveCommentDTO): int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert('interactive_comment', $interactiveCommentDTO);
    }


    /**
     * @param int $userId
     * @param int $commentId
     * @return bool|null软删除评论
     */
    public function deleteRecord(int $userId, int $commentId, ?bool $isAdmin): ?bool
    {
        $mysql = $this->select_mysql_db('main');
        $data = ['comment_status' => \Config_Const::FEED_STATUS_DELETED];
        $args = ['id' => $commentId, 'comment_user_id' => $userId];
        if ($isAdmin) {
            $args = ['id' => $commentId];
        }
        return $mysql->update("interactive_comment", $data, $args);
    }


    public function queryCommentListByBizId(QueryInteractiveCommentListParams $queryParams): ?array
    {
        $bizId = $queryParams->biz_id;
        $bizType = $queryParams->biz_type;
        $page_size = $queryParams->page_size;
        $index = $queryParams->index;
        $mysql = $this->select_mysql_db('main');
        return $mysql->select("select * from interactive_comment where biz_id =? and biz_type=? and comment_status=1 order by created_timestamp desc limit ? offset ? ",
            [$bizId, $bizType, $page_size, $index * $page_size], InteractiveCommentDTO::class);
    }


    public function queryCommentCountByBizId(string $bizType, int $bizId): ?int
    {
        $rowCount = 0;


        $mysql = $this->select_mysql_db('main');
        $data = $mysql->selectOne("select count(*) as row_count from interactive_comment where biz_id=? and biz_type=? and comment_status=1  ", [$bizId, $bizType]);
        if ($data != null) {
            $rowCount = $data->row_count;
        }

        return $rowCount;
    }


    public function queryCommentById(int $commentId): ?InteractiveCommentDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select * from interactive_comment where id =? and comment_status=1 limit 1 ", [$commentId], InteractiveCommentDTO::class);
    }


    public function queryCommentPeopleCount(int $bizId, string $bizType): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $data = $mysql->selectOne("select count(distinct (comment_user_id)) as row_count from interactive_comment where biz_id=? and biz_type=? and comment_status=1 group by biz_id ", [$bizId, $bizType]);


        if ($data != null) {
            $rowCount = $data->row_count;
        }

        return $rowCount;

    }


    public function queryCommentCountByUserId(int $userId): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $data = $mysql->selectOne("select count(*) as comment_count from interactive_comment where comment_user_id=?  ", [$userId]);

        if ($data != null) {
            $rowCount = $data->comment_count;
        }
        return $rowCount;
    }


    private function buildInteractiveCountKey(string $bizType, int $bizId): string
    {
        return 'interactive_count:' . $bizType . ":" . $bizId;
    }


    /**
     * @param int $bizId
     * @param string $bizType
     * @param int $number
     * @return bool
     * 更新feed评论数
     */
    public function updateCommentCountCache(int $bizId, string $bizType, int $number): bool
    {
        $key = $this->buildInteractiveCountKey($bizType, $bizId);
        $redis = $this->get_redis(self::INTERACTIVE_REDIS_MODULE);
        $targetType = self::INTERACTIVE_COMMENT_TYPE;
        return $redis->hincrby($key, $targetType, $number);

    }


    /**
     * @param int $bizId
     * @param string $bizType
     * @return int|null
     * 获取feed评论数
     */
    public function getCommentCountCache(int $bizId, string $bizType): ?int
    {
        $key = $this->buildInteractiveCountKey($bizType, $bizId);
        $redis = $this->get_redis(self::INTERACTIVE_REDIS_MODULE);
        $targetType = self::INTERACTIVE_COMMENT_TYPE;
        return (int)$redis->hget($key, $targetType);
    }


}