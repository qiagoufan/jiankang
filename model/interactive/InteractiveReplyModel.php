<?php


namespace model\interactive;

use Config_Const;
use facade\request\interactive\QueryInteractiveReplyListParams;
use model\interactive\dto\InteractiveReplyDTO;

class InteractiveReplyModel extends \FUR_Model
{
    public static $_instance;

    const INTERACTIVE_REDIS_MODULE = 'interactive';
    const INTERACTIVE_REPLY_TYPE = 'reply';

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function insertRecord(InteractiveReplyDTO $interactiveReplyDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert('interactive_reply', $interactiveReplyDTO);
    }

    /**
     * 删除评论下面的所有回复
     * @param int $commentId
     * @return int|null
     */
    public function deleteReplyRecordByCommentId(int $commentId): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $data = ['reply_status' => \Config_Const::FEED_STATUS_DELETED];
        return $mysql->update("interactive_reply", $data, ['comment_id' => $commentId]);
    }


    /**
     * 更改为删除状态
     * @param $replyId
     * @param $userId
     * @return int|null
     */
    public function deleteRecord(int $replyId, int $userId, int $isAdmin): ?bool
    {
        $mysql = $this->select_mysql_db('main');
        $data = ['reply_status' => \Config_Const::FEED_STATUS_DELETED];
        $args = ['id' => $replyId, 'reply_user_id' => $userId];
        if ($isAdmin == 1) {
            $args = ['id' => $replyId];
        }
        return $mysql->update("interactive_reply", $data, $args);
    }


    /**
     * 根据回复id查询记录
     * @param int $replyId
     * @return InteractiveReplyDTO|null
     */
    public function queryRecordById(int $replyId): ?InteractiveReplyDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select * from interactive_reply where id =? limit 1 ", [$replyId], InteractiveReplyDTO::class);

    }


    public function queryReplyListByCommentId(QueryInteractiveReplyListParams $params): ?array
    {
        $page_size = $params->page_size;
        $index = $params->index;
        $mysql = $this->select_mysql_db('main');
        return $mysql->select("select * from interactive_reply where comment_id =? and reply_status=1  order by created_timestamp desc limit ? offset ? ", [$params->comment_id, $page_size, $index * $page_size], InteractiveReplyDTO::class);
    }


    public function queryReplyCount(int $commentId, int $bizId, string $bizType): ?int
    {
        $rowCount = 0;
        $replyCount = $this->getReplyCountCache($bizId, $bizType);
        if ($replyCount != null) {
            $rowCount = $replyCount;
            return $rowCount;
        }

        $mysql = $this->select_mysql_db('main');
        $data = $mysql->selectOne("select count(*) as row_count  from interactive_reply where  comment_id=? and biz_type=? and reply_status=1", [$commentId, $bizType]);
        if ($data != null) {
            $rowCount = $data->row_count;
        }

        $this->updateReplyCountCache($bizId, $bizType, $rowCount);
        return $rowCount;
    }


    public function queryReplyExistCount(int $commentId): ?int
    {
        $rowCount = 0;
        $mysql = $this->select_mysql_db('main');
        $data = $mysql->selectOne("select count(*) as row_count  from interactive_reply where  comment_id=? and reply_status=1 ", [$commentId]);


        if ($data != null) {
            $rowCount = $data->row_count;
        }

        return $rowCount;

    }


    public function queryReplyCountByUserId(int $userId): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $data = $mysql->selectOne("select count(*) as reply_count from interactive_reply where reply_user_id=?  ", [$userId]);

        if ($data != null) {
            $rowCount = $data->reply_count;
        }
        return $rowCount;
    }


    private function buildInteractiveCountKey(string $bizType, int $bizId): string
    {
        return 'interactive_count:' . $bizType . ":" . $bizId;
    }


    /**
     * @param int $bizId
     * @param string $bizType
     * @param int $number
     * @return bool
     * 更新feed评论数
     */
    public function updateReplyCountCache(int $bizId, string $bizType, int $number): bool
    {
        $key = $this->buildInteractiveCountKey($bizType, $bizId);
        $redis = $this->get_redis(self::INTERACTIVE_REDIS_MODULE);
        $targetType = self::INTERACTIVE_REPLY_TYPE;
        return $redis->hincrby($key, $targetType, $number);

    }


    /**
     * @param int $bizId
     * @param string $bizType
     * @return int|null
     * 获取feed评论数
     */
    public function getReplyCountCache(int $bizId, string $bizType): ?int
    {
        $key = $this->buildInteractiveCountKey($bizType, $bizId);
        $redis = $this->get_redis(self::INTERACTIVE_REDIS_MODULE);
        $targetType = self::INTERACTIVE_REPLY_TYPE;
        return (int)$redis->hget($key, $targetType);
    }

}