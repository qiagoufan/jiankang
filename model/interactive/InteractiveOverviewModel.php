<?php


namespace model\interactive;


use model\interactive\dto\InteractiveOverviewDTO;

class InteractiveOverviewModel extends \FUR_Model
{
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function insertRecord(InteractiveOverviewDTO $InteractiveOverviewDTO): int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert('interactive_overview', $InteractiveOverviewDTO);
    }


    public function updateRecord(InteractiveOverviewDTO $interactiveOverviewDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $args = ['biz_id' => $interactiveOverviewDTO->biz_id, 'biz_type' => $interactiveOverviewDTO->biz_type];
        return $mysql->update("interactive_overview", $interactiveOverviewDTO, $args);
    }


    public function queryRecord(string $biz_type, int $biz_id): ?InteractiveOverviewDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select * from interactive_overview where  biz_id=? and biz_type =?  limit 1 ", [$biz_id, $biz_type], InteractiveOverviewDTO::class);
    }


}