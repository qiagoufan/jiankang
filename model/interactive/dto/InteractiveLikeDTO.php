<?php 
 
namespace model\interactive\dto; 
 
class InteractiveLikeDTO { 

	 //主键
	 public $id;
	 //用户id
	 public $user_id;
	 //点赞id
	 public $like_biz_id;
	 //点赞类型
	 public $like_biz_type;
	 //点赞状态 1.正常2.取消
	 public $like_status;
	 //创建时间
	 public $created_timestamp;
	 //更新时间
	 public $updated_timestamp;
 
 
 }