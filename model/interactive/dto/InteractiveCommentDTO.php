<?php 
 
namespace model\interactive\dto; 
 
class InteractiveCommentDTO { 

	 //主键
	 public $id;
	 //评论用户id
	 public $comment_user_id;
	 //评论内容
	 public $comment_content;
	 //业务类型
	 public $biz_type;
	 //业务id
	 public $biz_id;
	 //创建时间
	 public $created_timestamp;
	 //更新时间
	 public $updated_timestamp;
	 //评论状态1.正常0.删除-1.异常
	 public $comment_status;
 
 
 }