<?php 
 
namespace model\interactive\dto; 
 
class InteractiveOverviewDTO { 

	 //主键
	 public $id;
	 //业务类型
	 public $biz_type;
	 //业务id
	 public $biz_id;
	 //评论总数
	 public $comment_count;
	 //回复总数
	 public $reply_count;
	 //创建时间
	 public $created_timestamp;
	 //更新时间
	 public $updated_timestamp;
	 //???
	 public $like_count;
	 //转发数
	 public $share_count;
 
 
 }