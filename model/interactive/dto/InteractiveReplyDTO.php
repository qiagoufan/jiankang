<?php 
 
namespace model\interactive\dto; 
 
class InteractiveReplyDTO { 

	 //主键
	 public $id;
	 //回复的人
	 public $reply_user_id;
	 //评论id
	 public $comment_id;
	 //业务id
	 public $biz_id;
	 //业务类型
	 public $biz_type;
	 //@的人
	 public $reply_to_user_id;
	 //@的回复id
	 public $reply_to_id;
	 //回复内容
	 public $reply_content;
	 //1.正常0.删除-1异常
	 public $reply_status;
	 //创建时间
	 public $created_timestamp;
	 //更新时间
	 public $updated_timestamp;
 
 
 }