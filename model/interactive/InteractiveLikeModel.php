<?php


namespace model\interactive;


use model\interactive\dto\InteractiveLikeDTO;


class InteractiveLikeModel extends \FUR_Model
{
    public static $_instance;

    const INTERACTIVE_REDIS_MODULE = 'interactive';
    const INTERACTIVE_LIKE_TYPE = 'like';


    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    /**
     * @param InteractiveLikeDTO $interactiveLikeDTO
     * @return int|null
     * 添加点赞记录
     */
    public function insertRecord(InteractiveLikeDTO $interactiveLikeDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert('interactive_like', $interactiveLikeDTO);
    }


    /**
     * @param int $userId
     * @param int $likeId
     * @return bool|null
     * 取消点赞
     */
    public function deleteRecord(int $userId, int $bizId, string $bizType): ?bool
    {
        $mysql = $this->select_mysql_db('main');

        return $mysql->delete('interactive_like', ['like_biz_id' => $bizId, 'user_id' => $userId, 'like_biz_type' => $bizType]);
    }


    /**
     * @param InteractiveLikeDTO $interactiveLikeDTO
     * @return int|null
     * 更新记录
     */
    public function updateRecord(int $userId, int $bizId): ?bool
    {
        $mysql = $this->select_mysql_db('main');
        $data = ['like_status' => 1, 'updated_timestamp' => time()];
        return $mysql->update("interactive_like", $data, ['like_biz_id' => $bizId, 'user_id' => $userId]);
    }

    /**
     * @param int $userId
     * @param int $bizId
     * @return InteractiveLikeDTO|null
     * 查询点赞记录
     */
    public function queryRecord(int $userId, int $bizId, string $bizType): ?InteractiveLikeDTO
    {

        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select * from interactive_like where user_id =? and like_biz_id=? and like_biz_type=?   limit 1 ", [$userId, $bizId, $bizType], InteractiveLikeDTO::class);

    }


    /**
     * @param int $userId
     * @return int|null
     * 查询点赞feed列表
     */
    public function queryUserLikeList(int $userId, string $bizType, int $offset, int $pageSize): ?array
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->select('select * from  interactive_like where user_id =? and like_biz_type=? and like_status=1 order by updated_timestamp desc limit ?,?', [$userId, $bizType, $offset, $pageSize], InteractiveLikeDTO::class);
    }


    private function buildInteractiveLikeKey(int $userId): string
    {
        return 'user_like:' . $userId;
    }


    private function buildInteractiveCountKey(string $bizType, int $bizId): string
    {
        return 'interactive_count:' . $bizType . ":" . $bizId;
    }


    private function buildUserLikeMember(string $bizType, int $bizId): string
    {
        return $bizType . '|' . $bizId;
    }

    public function addInteractiveLikeCache(?int $userId, ?int $bizId, ?string $bizType)
    {
        $redis = $this->get_redis(self::INTERACTIVE_REDIS_MODULE);
        $this->updateLikeCountCache($bizId, $bizType, 1);
        $key = $this->buildInteractiveLikeKey($userId);
        $member = $this->buildUserLikeMember($bizType, $bizId);
        return $redis->sadd($key, $member);
    }


//    public function isLike(int $userId, int $bizId, string $bizType): bool
//    {
//        $redis = $this->get_redis(self::INTERACTIVE_REDIS_MODULE);
//        $key = $this->buildInteractiveLikeKey($userId);
//        $member = $this->buildUserLikeMember($bizType, $bizId);
//        return (bool)$redis->sismember($key, $member);
//    }


    public function removeInteractiveLikeCache(?int $userId, ?int $bizId, ?string $bizType)
    {
        $redis = $this->get_redis(self::INTERACTIVE_REDIS_MODULE);
        $key = $this->buildInteractiveLikeKey($userId);
        $this->updateLikeCountCache($bizId, $bizType, -1);
        $member = $this->buildUserLikeMember($bizType, $bizId);
        return $redis->srem($key, $member);
    }

    public function updateLikeCountCache(int $bizId, string $bizType, int $number): bool
    {
        $key = $this->buildInteractiveCountKey($bizType, $bizId);
        $redis = $this->get_redis(self::INTERACTIVE_REDIS_MODULE);
        $targetType = self::INTERACTIVE_LIKE_TYPE;
        return $redis->hincrby($key, $targetType, $number);

    }


    public function getLikeCountCache(int $bizId, string $bizType): ?int
    {
        $key = $this->buildInteractiveCountKey($bizType, $bizId);
        $redis = $this->get_redis(self::INTERACTIVE_REDIS_MODULE);
        $targetType = self::INTERACTIVE_LIKE_TYPE;
        return $redis->hget($key, $targetType);
    }


}