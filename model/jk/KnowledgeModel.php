<?php


namespace model\jk;


use facade\request\jk\knowledge\QueryKnowledgeListParams;
use model\jk\dto\JkKnowledgeCategoryDTO;
use model\jk\dto\JkKnowledgeDTO;

class KnowledgeModel extends \FUR_Model
{
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function getJkKnowledgeCategory($id): ?JkKnowledgeCategoryDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select * from jk_knowledge_category where id =?", [$id], JkKnowledgeCategoryDTO::class);

    }

    public function insertJkKnowledgeCategory(?JkKnowledgeCategoryDTO $jkKnowledgeCategoryDTO): ?int
    {

        $mysql = $this->select_mysql_db('main');
        return $mysql->insert("jk_knowledge_category", $jkKnowledgeCategoryDTO);
    }

    public function deleteJkKnowledgeCategory(?int $id): ?int
    {

        $mysql = $this->select_mysql_db('main');
        return $mysql->delete("jk_knowledge_category", ['id' => $id]);
    }

    public function updateJkKnowledgeCategory(?JkKnowledgeCategoryDTO $jkKnowledgeCategoryDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->update("jk_knowledge_category", $jkKnowledgeCategoryDTO, ['id' => $jkKnowledgeCategoryDTO->id]);

    }

    public function getJkKnowledgeCategoryList(): ?array
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->select("select * from jk_knowledge_category order by id desc ", [], JkKnowledgeCategoryDTO::class);

    }

    public function getJkKnowledge($id): ?JkKnowledgeDTO
    {

        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select * from jk_knowledge where id =?", [$id], JkKnowledgeDTO::class);
    }

    public function insertJkKnowledge(?JkKnowledgeDTO $jkKnowledgeDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert("jk_knowledge", $jkKnowledgeDTO);
    }

    public function deleteJkKnowledge($id): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->delete("jk_knowledge", ['id' => $id]);
    }

    public function updateJkKnowledge(?JkKnowledgeDTO $jkKnowledgeDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->update("jk_knowledge", $jkKnowledgeDTO, ['id' => $jkKnowledgeDTO->id]);

    }

    public function getJkKnowledgeList(QueryKnowledgeListParams $queryKnowledgeListParams): ?array
    {
        $mysql = $this->select_mysql_db('main');
        $offset = $queryKnowledgeListParams->page_size * $queryKnowledgeListParams->index;
        $pageSize = $queryKnowledgeListParams->page_size;
        $sql = 'select * from jk_knowledge where 1=1 ';
        $args = [];

        if ($queryKnowledgeListParams->category_id) {
            $sql .= ' and category_id =? ';
            $args = array_merge($args, [$queryKnowledgeListParams->category_id]);
        } elseif ($queryKnowledgeListParams->is_publish) {
            $sql .= ' and publish_status  =1 ';

        }
        $sql .= ' order by id desc limit ?,?';
        $args = array_merge($args, [$offset, $pageSize]);
        return $mysql->select($sql, $args, JkKnowledgeDTO::class);

    }

    public function getManageJkKnowledgeList(QueryKnowledgeListParams $queryKnowledgeListParams): ?array
    {
        $mysql = $this->select_mysql_db('main');
        $offset = $queryKnowledgeListParams->page_size * $queryKnowledgeListParams->index;
        $pageSize = $queryKnowledgeListParams->page_size;
        $sql = 'select * from jk_knowledge where 1=1 ';
        $args = [];

        if ($queryKnowledgeListParams->category_id) {
            $sql .= ' and category_id =? ';
            $args = array_merge($args, [$queryKnowledgeListParams->category_id]);
        }
        $sql .= ' order by id desc limit ?,?';
        $args = array_merge($args, [$offset, $pageSize]);
        return $mysql->select($sql, $args, JkKnowledgeDTO::class);

    }


    public function getJkKnowledgeCount(QueryKnowledgeListParams $queryKnowledgeListParams): ?int
    {
        $mysql = $this->select_mysql_db('main');

        $totalCount = 0;
        $sql = 'select count(*) as total_count from jk_knowledge_category where 1=1 ';
        $args = [];
        if ($queryKnowledgeListParams->category_id) {
            $sql .= ' and category_id =? ';
            array_push($args, $queryKnowledgeListParams->category_id);
        }
        $data = $mysql->selectOne($sql, $args);
        if ($data) {
            $totalCount = $data->total_count;
        }
        return $totalCount;

    }

}