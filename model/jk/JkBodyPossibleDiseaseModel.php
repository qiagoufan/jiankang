<?php


namespace model\jk;


use model\jk\dto\JkBodyPossibleDiseaseDTO;

class JkBodyPossibleDiseaseModel extends \FUR_Model
{
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function insertJkPossibleDisease(?JkBodyPossibleDiseaseDTO $diseaseDTO): ?int
    {

        $mysql = $this->select_mysql_db('main');
        return $mysql->insert("jk_body_possible_disease", $diseaseDTO);
    }

    public function cleanPossibleDisease(?int $symptomId): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->delete("jk_body_possible_disease", ['symptom_id' => $symptomId]);

    }


}