<?php


namespace model\jk;


use model\jk\dto\JkBodyListDTO;

class JkBodyModel extends \FUR_Model
{
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function getBodyList(): ?array
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->select("select * from jk_body_list ", [], JkBodyListDTO::class);
    }


}