<?php


namespace model\jk;


use facade\request\jk\bodyCheck\manage\QuerySymptomParams;
use facade\request\jk\bodyCheck\QuerySymptomListParams;
use facade\request\PageParams;
use model\jk\dto\JkBodyAgeSymptomDTO;
use model\jk\dto\JkBodySymptomDTO;

class JkBodySymptomModel extends \FUR_Model
{
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function insertJkBodySymptom(?JkBodySymptomDTO $symptomDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert("jk_body_symptom", $symptomDTO);
    }


    public function updateJkBodySymptom(?JkBodySymptomDTO $symptomDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->update("jk_body_symptom", $symptomDTO, ['id' => $symptomDTO->id]);
    }

    public function updateJkBodySymptomHaveQuestion(?int $symptomId, int $haveQuestion): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->update("jk_body_symptom", ['have_question' => $haveQuestion], ['id' => $symptomId]);
    }


    public function insertHumanSymptomAge(?JkBodyAgeSymptomDTO $data): ?int
    {

        $mysql = $this->select_mysql_db('main');
        return $mysql->insert("jk_body_age_symptom", $data);
    }

    public function cleanSymptomAgeBySymptomId(?int $symptomId): ?int
    {

        $mysql = $this->select_mysql_db('main');
        return $mysql->delete("jk_body_age_symptom", ['symptom_id' => $symptomId]);
    }


    public function querySymptomList(QuerySymptomListParams $params): ?array
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->select("select jk_body_symptom.* from jk_body_symptom 
                            inner join  jk_body_age_symptom on jk_body_age_symptom.symptom_id =jk_body_symptom.id 
                            where human_body_id =? and age=? and sex =? ", [$params->body_id, $params->age, $params->sex], JkBodySymptomDTO::class);

    }

    public function querySymptomDetail($symptomId): ?JkBodySymptomDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select jk_body_symptom.* from jk_body_symptom  where id =?  ", [$symptomId], JkBodySymptomDTO::class);

    }

    public function deleteSymptom($symptomId): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->delete("jk_body_symptom", ['id' => $symptomId]);

    }

    public function queryManageSymptomList(QuerySymptomParams $params): ?array
    {
        $mysql = $this->select_mysql_db('main');
        $offset = $params->index * $params->page_size;
        $pageSize = $params->page_size;
        $sql = "select * from jk_body_symptom where 1=1 ";
        if ($params->keyword) {
            $keyword = "'%" . $params->keyword . "%'";
            $sql = $sql . ' and symptom like' . $keyword;
        }
        if ($params->human_body_id) {
            $sql = $sql . ' and human_body_id =' . $params->human_body_id;
        }

        return $mysql->select($sql . " order by id desc limit ?,? ", [$offset, $pageSize], JkBodySymptomDTO::class);

    }

    public function querySymptomAgeList(?int $symptomId): ?array
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->select("select * from jk_body_age_symptom  where  symptom_id =? order by  age asc  ", [$symptomId], JkBodyAgeSymptomDTO::class);

    }

    public function queryManageSymptomCount(QuerySymptomParams $params): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $sql = "select count(*) as total_count from jk_body_symptom where 1=1 ";
        if ($params->keyword) {
            $keyword = "'%" . $params->keyword . "%'";
            $sql = $sql . ' and symptom like' . $keyword;
        }
        if ($params->human_body_id) {
            $sql = $sql . ' and human_body_id =' . $params->human_body_id;
        }

        $data = $mysql->selectOne($sql);
        $totalCount = 0;
        if ($data) {
            $totalCount = $data->total_count;
        }
        return $totalCount;

    }

}