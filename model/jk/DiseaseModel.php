<?php


namespace model\jk;

use facade\request\jk\bodyCheck\manage\QueryDiseaseParams;
use facade\request\PageParams;
use model\jk\dto\JkBodyDiseaseDetailDTO;

class DiseaseModel extends \FUR_Model
{
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function getDisease($id): ?JkBodyDiseaseDetailDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select * from jk_body_disease_detail where id =?", [$id], JkBodyDiseaseDetailDTO::class);

    }

    public function insertDisease(?JkBodyDiseaseDetailDTO $jkDiseaseDTO): ?int
    {

        $mysql = $this->select_mysql_db('main');
        return $mysql->insert("jk_body_disease_detail", $jkDiseaseDTO);
    }

    public function deleteDisease(?int $id): ?int
    {

        $mysql = $this->select_mysql_db('main');
        return $mysql->delete("jk_body_disease_detail", ['id' => $id]);
    }

    public function updateDisease(?JkBodyDiseaseDetailDTO $jkDiseaseDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->update("jk_body_disease_detail", $jkDiseaseDTO, ['id' => $jkDiseaseDTO->id]);

    }

    public function updateDiseaseClassName($classId, $className): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->update("jk_body_disease_detail", ['class_name' => $className], ['class_id' => $classId]);

    }

    public function getDiseaseList(QueryDiseaseParams $params): ?array
    {
        $offset = $params->page_size * $params->index;
        $mysql = $this->select_mysql_db('main');
        $sql = "select id,name,class_name,created_timestamp,is_hurry from jk_body_disease_detail where 1=1 ";
        if ($params->class_name) {
            $class_name = "'%" . $params->class_name . "%'";
            $sql = $sql . ' and class_name like' . $class_name;
        }
        if ($params->disease_name) {
            $disease_name = "'%" . $params->disease_name . "%'";
            $sql = $sql . ' and name like' . $disease_name;
        }
        return $mysql->select($sql . " order by id desc limit ?,?", [$offset, intval($params->page_size)], JkBodyDiseaseDetailDTO::class);

    }

    public function getDiseaseCountByClassId($classId): ?int
    {
        $mysql = $this->select_mysql_db('main');

        $totalCount = 0;
        $data = $mysql->selectOne("select count(*) as total_count from jk_body_disease_detail  WHERE class_id =?", [$classId]);
        if ($data) {
            $totalCount = $data->total_count;
        }
        return $totalCount;
    }

    public function getDiseaseCount(QueryDiseaseParams $params): ?int
    {
        $mysql = $this->select_mysql_db('main');

        $totalCount = 0;
        $sql = "select count(*) as total_count from jk_body_disease_detail where 1=1 ";
        if ($params->class_name) {
            $class_name = "'%" . $params->class_name . "%'";
            $sql = $sql . ' and class_name like' . $class_name;
        }
        if ($params->disease_name) {
            $disease_name = "'%" . $params->disease_name . "%'";
            $sql = $sql . ' and name like' . $disease_name;
        }
        $data = $mysql->selectOne($sql);
        if ($data) {
            $totalCount = $data->total_count;
        }
        return $totalCount;


    }
}