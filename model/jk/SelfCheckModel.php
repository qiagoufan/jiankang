<?php


namespace model\jk;


use facade\request\jk\knowledge\QueryKnowledgeListParams;
use facade\request\PageParams;
use model\jk\dto\JkSelfCheckCategoryDTO;
use model\jk\dto\JkSelfCheckQuestionDTO;
use model\jk\dto\JkSelfCheckResultDTO;

class SelfCheckModel extends \FUR_Model
{
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function getJkSelfCheckCategory($id): ?JkSelfCheckCategoryDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select * from jk_self_check_category where id =?", [$id], JkSelfCheckCategoryDTO::class);

    }

    public function insertJkSelfCheckCategory(?JkSelfCheckCategoryDTO $JkSelfCheckCategoryDTO): ?int
    {

        $mysql = $this->select_mysql_db('main');
        return $mysql->insert("jk_self_check_category", $JkSelfCheckCategoryDTO);
    }

    public function deleteJkSelfCheckCategory(?int $id): ?int
    {

        $mysql = $this->select_mysql_db('main');
        return $mysql->delete("jk_self_check_category", ['id' => $id]);
    }

    public function deleteJkSelfCheckQuestionById(?int $category_id): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->delete("jk_self_check_question", ['category_id' => $category_id]);
    }

    public function deleteJkSelfCheckResultById(?int $category_id): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->delete("jk_self_check_result", ['category_id' => $category_id]);
    }

    public function updateJkSelfCheckCategory(?JkSelfCheckCategoryDTO $JkSelfCheckCategoryDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->update("jk_self_check_category", $JkSelfCheckCategoryDTO, ['id' => $JkSelfCheckCategoryDTO->id]);

    }

    public function getJkSelfCheckCategoryList(): ?array
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->select("select * from jk_self_check_category order by seq desc, id desc ", [], JkSelfCheckCategoryDTO::class);

    }

    public function queryJkSelfCheckCategoryList(PageParams $params): ?array
    {
        $mysql = $this->select_mysql_db('main');
        $offset = $params->index * $params->page_size;
        return $mysql->select("select * from jk_self_check_category  order by seq desc, id desc limit ?,?", [$offset, $params->page_size], JkSelfCheckCategoryDTO::class);

    }


    public function queryJkSelfCheckCategoryCount(): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $totalCount = 0;
        $data = $mysql->selectOne("select count(*) as total_count from jk_self_check_category  ");
        if ($data) {
            $totalCount = $data->total_count;
        }
        return $totalCount;

    }

    public function queryResultByScore(int $categoryId, int $score): ?JkSelfCheckResultDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select * from jk_self_check_result where category_id =? and start_score<=? and end_score>=? limit 1", [$categoryId, $score, $score], JkSelfCheckResultDTO::class);
    }

    public function getJkSelfCheckQuestionList($categoryId): ?array
    {
        $mysql = $this->select_mysql_db('main');

        return $mysql->select('select * from jk_self_check_question where category_id =? order by question_sort_num asc  ', [$categoryId], JkSelfCheckQuestionDTO::class);

    }

    public function getJkSelfCheckResultList($categoryId): ?array
    {
        $mysql = $this->select_mysql_db('main');

        return $mysql->select('select * from jk_self_check_result where category_id =? order by id asc  ', [$categoryId], JkSelfCheckResultDTO::class);

    }

    public function insertJkSelfCheckQuestionDTO(?JkSelfCheckQuestionDTO $jkSelfCheckQuestionDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert("jk_self_check_question", $jkSelfCheckQuestionDTO);
    }

    public function insertJkSelfCheckResultDTO(?JkSelfCheckResultDTO $resultDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert("jk_self_check_result", $resultDTO);
    }
}