<?php

namespace model\jk\dto;

class JkBodySymptomDTO
{

    //
    public $id;
    //
    public $human_body_id;
    //
    public $have_question;
    //
    public $symptom;
    //
    public $symptom_image;
    //
    public $created_timestamp;
    //
    public $updated_timestamp;

    public $calculation_type;


}