<?php 
 
namespace model\jk\dto; 
 
class JkUserCheckHistoryDTO { 

	 //主键
	 public $id;
	 //用户id
	 public $user_id;
	 //检测类型
	 public $check_type;
	 //检测结果
	 public $check_result;
	 //
	 public $created_timestamp;
	 //
	 public $updated_timestamp;
 
 
 }