<?php 
 
namespace model\jk\dto; 
 
class JkBodyDiseaseDetailDTO { 

	 //
	 public $id;
	 //
	 public $name;
	 //
	 public $letter;
	 //
	 public $first_letter;
	 //
	 public $is_hurry;
	 //
	 public $class_id;
	 //
	 public $class_name;
	 //
	 public $introduction;
	 //
	 public $checking;
	 //
	 public $concurrent;
	 //
	 public $diagnosis;
	 //
	 public $prevention;
	 //
	 public $reason_description;
	 //
	 public $symptom_description;
	 //
	 public $treatment;
	 //
	 public $created_timestamp;
	 //
	 public $updated_timestamp;
 
 
 }