<?php 
 
namespace model\jk\dto; 
 
class JkKnowledgeDTO { 

	 //主键
	 public $id;
	 //封面
	 public $knowledge_cover;
	 //标题
	 public $knowledge_title;
	 //详情
	 public $knowledge_content;
	 //
	 public $category_id;
	 //
	 public $created_timestamp;
	 //
	 public $updated_timestamp;
	 //0-未推荐,1已推荐
	 public $publish_status;
 
 
 }