<?php

namespace model\jk\dto;

class JkRiskCheckCategoryDTO
{

    //主键
    public $id;
    //父id
    public $parent_id;
    //封面
    public $category_cover;
    //
    public $category_name;
    //
    public $created_timestamp;
    //
    public $updated_timestamp;
    //排序号
    public $seq;

    public $calculation_type;


}