<?php 
 
namespace model\jk\dto; 
 
class JkSelfCheckResultDTO { 

	 //主键
	 public $id;
	 //起始分值
	 public $start_score;
	 //结束分值
	 public $end_score;
	 //治疗建议
	 public $suggest;
	 //症状
	 public $disease;
	 //
	 public $category_id;
	 //
	 public $created_timestamp;
	 //
	 public $updated_timestamp;
 
 
 }