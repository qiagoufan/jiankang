<?php 
 
namespace model\jk\dto; 
 
class JkSelfCheckQuestionDTO { 

	 //主键
	 public $id;
	 //标题
	 public $question_title;
	 //选项
	 public $question_option;
	 //排序
	 public $question_sort_num;
	 //
	 public $category_id;
	 //
	 public $created_timestamp;
	 //
	 public $updated_timestamp;
 
 
 }