<?php


namespace model\jk;

use model\jk\dto\JkHospitalClassDTO;

class HospitalClassModel extends \FUR_Model
{
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function getHospitalClass($id): ?JkHospitalClassDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select * from jk_hospital_class where id =?", [$id], JkHospitalClassDTO::class);

    }

    public function insertHospitalClass(?JkHospitalClassDTO $jkHospitalClassDTO): ?int
    {

        $mysql = $this->select_mysql_db('main');
        return $mysql->insert("jk_hospital_class", $jkHospitalClassDTO);
    }

    public function deleteHospitalClass(?int $id): ?int
    {

        $mysql = $this->select_mysql_db('main');
        return $mysql->delete("jk_hospital_class", ['id' => $id]);
    }

    public function updateHospitalClass(?JkHospitalClassDTO $jkHospitalClassDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->update("jk_hospital_class", $jkHospitalClassDTO, ['id' => $jkHospitalClassDTO->id]);

    }

    public function getHospitalClassList(): ?array
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->select("select * from jk_hospital_class order by id desc ", [], JkHospitalClassDTO::class);

    }


}