<?php


namespace model\jk;


use facade\request\jk\knowledge\QueryKnowledgeListParams;
use facade\request\PageParams;
use model\jk\dto\JkRiskCheckCategoryDTO;
use model\jk\dto\JkRiskCheckOptionDTO;
use model\jk\dto\JkRiskCheckQuestionDTO;
use model\jk\dto\JkRiskCheckResultDTO;

class RiskCheckModel extends \FUR_Model
{
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function getJkRiskCheckCategory($id): ?JkRiskCheckCategoryDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select * from jk_risk_check_category where id =?", [$id], JkRiskCheckCategoryDTO::class);

    }

    public function insertJkRiskCheckCategory(?JkRiskCheckCategoryDTO $JkRiskCheckCategoryDTO): ?int
    {

        $mysql = $this->select_mysql_db('main');
        return $mysql->insert("jk_risk_check_category", $JkRiskCheckCategoryDTO);
    }

    public function deleteJkRiskCheckCategory(?int $id): ?int
    {

        $mysql = $this->select_mysql_db('main');
        return $mysql->delete("jk_risk_check_category", ['id' => $id]);
    }

    public function deleteJkRiskCheckQuestionById(?int $category_id): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->delete("jk_risk_check_question", ['category_id' => $category_id]);
    }

    public function deleteJkRiskCheckResultById(?int $category_id): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->delete("jk_risk_check_result", ['category_id' => $category_id]);
    }

    public function updateJkRiskCheckCategory(?JkRiskCheckCategoryDTO $JkRiskCheckCategoryDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->update("jk_risk_check_category", $JkRiskCheckCategoryDTO, ['id' => $JkRiskCheckCategoryDTO->id]);

    }

    public function getJkRiskCheckCategoryList(): ?array
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->select("select * from jk_risk_check_category order by seq desc, id desc ", [], JkRiskCheckCategoryDTO::class);

    }


    public function getJkRiskCheckCategoryList1(): ?array
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->select("select * from jk_risk_check_category where parent_id =0 order by seq desc, id desc ", [], JkRiskCheckCategoryDTO::class);

    }

    public function queryJkRiskCheckCategoryList(PageParams $params): ?array
    {
        $mysql = $this->select_mysql_db('main');
        $offset = $params->index * $params->page_size;
        return $mysql->select("select * from jk_risk_check_category  order by seq desc, id desc limit ?,?", [$offset, $params->page_size], JkRiskCheckCategoryDTO::class);

    }

    public function queryJkRiskCheckLevel2CategoryList(PageParams $params): ?array
    {
        $mysql = $this->select_mysql_db('main');
        $offset = $params->index * $params->page_size;
        return $mysql->select("select * from jk_risk_check_category where parent_id!=0  order by seq desc, id desc limit ?,?", [$offset, $params->page_size], JkRiskCheckCategoryDTO::class);

    }

    public function queryJkRiskCheckCategoryCountByParentId($parentId): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $totalCount = 0;
        $data = $mysql->selectOne("select count(*) as total_count from jk_risk_check_category  where parent_id =?", [$parentId]);
        if ($data) {
            $totalCount = $data->total_count;
        }
        return $totalCount;

    }

    public function queryJkRiskCheckCategoryCount(): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $totalCount = 0;
        $data = $mysql->selectOne("select count(*) as total_count from jk_risk_check_category  ");
        if ($data) {
            $totalCount = $data->total_count;
        }
        return $totalCount;

    }


    public function queryJkRiskCheckLevel2CategoryCount(): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $totalCount = 0;
        $data = $mysql->selectOne("select count(*) as total_count from jk_risk_check_category where  parent_id !=0");
        if ($data) {
            $totalCount = $data->total_count;
        }
        return $totalCount;

    }

    public function queryResultByScore(int $categoryId, int $score): ?JkRiskCheckResultDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select * from jk_risk_check_result where category_id =? and start_score<=? and end_score>=? limit 1", [$categoryId, $score, $score], JkRiskCheckResultDTO::class);
    }

    public function getJkRiskCheckQuestionList($categoryId): ?array
    {
        $mysql = $this->select_mysql_db('main');

        return $mysql->select('select * from jk_risk_check_question where category_id =? order by question_sort_num asc  ', [$categoryId], JkRiskCheckQuestionDTO::class);

    }


    public function queryJkRiskCheckQuestionList(PageParams $params): ?array
    {
        $mysql = $this->select_mysql_db('main');
        $offset = $params->index * $params->page_size;
        return $mysql->select('select * from jk_risk_check_question where 1=1 order by id asc  limit ?,? ', [$offset, $params->page_size], JkRiskCheckQuestionDTO::class);

    }

    public function queryJkRiskCheckQuestionCount(PageParams $params): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $totalCount = 0;
        $data = $mysql->selectOne('select count(*) as total_count from jk_risk_check_question ');
        if ($data) {
            $totalCount = $data->total_count;
        }
        return $totalCount;

    }


    public function getJkRiskCheckResultList($categoryId): ?array
    {
        $mysql = $this->select_mysql_db('main');

        return $mysql->select('select * from jk_risk_check_result where category_id =? order by id asc  ', [$categoryId], JkRiskCheckResultDTO::class);

    }

    public function insertJkRiskCheckQuestionDTO(?JkRiskCheckQuestionDTO $jkRiskCheckQuestionDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert("jk_risk_check_question", $jkRiskCheckQuestionDTO);
    }

    public function insertJkRiskCheckOptionDTO(?JkRiskCheckOptionDTO $optionDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert("jk_risk_check_option", $optionDTO);
    }

    public function queryOptionList(?int $questionId): ?array
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->select("select * from jk_risk_check_option where question_id =? order by id asc ", [$questionId], JkRiskCheckOptionDTO::class);
    }


    public function cleanOptionList(?int $questionId): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->delete("jk_risk_check_option", ['question_id' => $questionId]);
    }


    public function queryOption(?int $optionId): ?JkRiskCheckOptionDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select * from jk_risk_check_option where id =?", [$optionId], JkRiskCheckOptionDTO::class);
    }


    public function insertJkRiskCheckResultDTO(?JkRiskCheckResultDTO $resultDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert("jk_risk_check_result", $resultDTO);
    }
}