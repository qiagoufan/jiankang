<?php


namespace model\jk;


use facade\request\PageParams;
use model\jk\dto\JkUserCheckHistoryDTO;

class UserCheckHistoryModel extends \FUR_Model
{
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function getDetail($id): ?JkUserCheckHistoryDTO
    {

        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select * from jk_user_check_history where id =?", [$id], JkUserCheckHistoryDTO::class);
    }

    public function insertRecord(?JkUserCheckHistoryDTO $data): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert("jk_user_check_history", $data);
    }


    public function getUserHistoryList(PageParams $params): ?array
    {
        $mysql = $this->select_mysql_db('main');
        $pageSize = $params->page_size;
        $offset = $params->index * $pageSize;
        $userId = $params->user_id;

        return $mysql->select('select * from jk_user_check_history where user_id =? order by id  desc  limit ?,?  ', [$userId, $offset, $pageSize], JkUserCheckHistoryDTO::class);

    }

    public function getUserHistoryCount($userId): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $totalCount = 0;
        $data = $mysql->selectOne('select count(*) as total_count from jk_user_check_history where user_id =?   ', [$userId]);
        if ($data) {
            $totalCount = $data->total_count;
        }
        return $totalCount;

    }

}