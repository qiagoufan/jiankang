<?php


namespace model\jk;


use model\jk\dto\JkBodyOptionDTO;
use model\jk\dto\JkBodyQuestionDTO;

class JkBodySymptomQuestionModel extends \FUR_Model
{
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function queryQuestionList(int $symptomId): ?array
    {

        $mysql = $this->select_mysql_db('main');
        return $mysql->select("select * from jk_body_question where symptom_id =? order by seq asc ", [$symptomId], JkBodyQuestionDTO::class);
    }

    public function queryOptionList(int $questionId): ?array
    {

        $mysql = $this->select_mysql_db('main');
        return $mysql->select("select * from jk_body_option where question_id =? order by seq asc ", [$questionId], JkBodyOptionDTO::class);
    }

    public function insertJkBodyQuestion(?JkBodyQuestionDTO $questionDTO): ?int
    {

        $mysql = $this->select_mysql_db('main');
        return $mysql->insert("jk_body_question", $questionDTO);
    }

    public function insertJkBodyOption(?JkBodyOptionDTO $optionDTO): ?int
    {

        $mysql = $this->select_mysql_db('main');
        return $mysql->insert("jk_body_option", $optionDTO);
    }

    public function queryOptionInfo(int $id): ?JkBodyOptionDTO
    {

        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select * from jk_body_option where id =? ", [$id], JkBodyOptionDTO::class);
    }

    public function cleanJkBodyQuestion(?int $symptomId): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->delete("jk_body_question", ['symptom_id' => $symptomId]);
    }

    public function cleanJkBodyOption(?int $questionId): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->delete("jk_body_option", ['question_id' => $questionId]);
    }
}