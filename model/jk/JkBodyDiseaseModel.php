<?php


namespace model\jk;


use facade\request\jk\bodyCheck\QueryPossibleDiseaseListParams;
use model\jk\dto\JkBodyDiseaseDetailDTO;
use model\jk\dto\JkBodyPossibleDiseaseDTO;

class JkBodyDiseaseModel extends \FUR_Model
{
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function getDiseaseDetail(int $id): ?JkBodyDiseaseDetailDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select * from jk_body_disease_detail where id =?", [$id], JkBodyDiseaseDetailDTO::class);
    }

    public function queryPossibleDiseaseList(QueryPossibleDiseaseListParams $params): ?array
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->select("select jk_body_disease_detail.id,jk_body_disease_detail.name,jk_body_disease_detail.class_id ,jk_body_disease_detail.is_hurry
                                    from jk_body_possible_disease  inner join jk_body_disease_detail 
                                    on jk_body_possible_disease.disease_id = jk_body_disease_detail.id
                                    where symptom_id =? and age =? and sex =?", [$params->symptom_id, $params->age, $params->sex]);
    }

    public function queryPossibleDiseaseListWithScore(QueryPossibleDiseaseListParams $params, int $score): ?array
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->select("select jk_body_disease_detail.id,jk_body_disease_detail.name,jk_body_disease_detail.class_id ,jk_body_disease_detail.is_hurry
                                    from jk_body_possible_disease  inner join jk_body_disease_detail 
                                    on jk_body_possible_disease.disease_id = jk_body_disease_detail.id
                                    where symptom_id =? and age =? and sex =? and start_score<=? and end_score>=?", [$params->symptom_id, $params->age, $params->sex, $score, $score]);
    }

    public function queryPossibleDiseaseListBySymptomId($symptomId): ?array
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->select("select * from jk_body_possible_disease
                                    where symptom_id =? order by id asc ", [$symptomId], JkBodyPossibleDiseaseDTO::class);
    }


    public function insertJkPossibleDisease(?JkBodyPossibleDiseaseDTO $diseaseDTO): ?int
    {

        $mysql = $this->select_mysql_db('main');
        return $mysql->insert("jk_body_possible_disease", $diseaseDTO);
    }


}