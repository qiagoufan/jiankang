<?php 
 
namespace model\theatre\dto; 
 
class TheatreCalendarDTO { 

	 //primary key
	 public $id;
	 //事件时间点
	 public $event_timestamp;
	 //????:2020311
	 public $event_date;
	 //????
	 public $event_biz_type;
	 //事件id
	 public $event_biz_id;
	 //事件附属信息
	 public $event_extra_info;
	 //????
	 public $sort_num;
	 //创建时间
	 public $created_timestamp;
	 //更新时间
	 public $updated_timestamp;
 
 
 }