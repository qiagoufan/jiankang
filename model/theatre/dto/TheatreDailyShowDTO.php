<?php 
 
namespace model\theatre\dto; 
 
class TheatreDailyShowDTO { 

	 //主键
	 public $id;
	 //日期
	 public $show_date;
	 //当日排序
	 public $sort;
	 //详情
	 public $show_content;
	 //剧场类型
	 public $show_type;
	 //创建时间
	 public $created_timestamp;
	 //更新时间
	 public $updated_timestamp;
	 //状态
	 public $show_status;
 
 
 }