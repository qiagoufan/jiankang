<?php 
 
namespace model\theatre\dto; 
 
class TheatreChallengePrizeDTO { 

	 //primary key
	 public $id;
	 //挑战活动的id
	 public $challenge_id;
	 //奖品名称
	 public $prize_name;
	 //奖品sku id
	 public $prize_sku_id;
	 //创建时间
	 public $created_timestamp;
	 //更新时间
	 public $updated_timestamp;
	 //???
	 public $sort;
 
 
 }