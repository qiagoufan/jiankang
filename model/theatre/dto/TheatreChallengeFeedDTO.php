<?php 
 
namespace model\theatre\dto; 
 
class TheatreChallengeFeedDTO { 

	 //primary key
	 public $id;
	 //挑战id
	 public $challenge_id;
	 //帖子id
	 public $feed_id;
	 //是否是置顶
	 public $is_top;
	 //作者id
	 public $author_id;
	 //创建时间
	 public $created_timestamp;
	 //更新时间
	 public $updated_timestamp;
	 //???
	 public $like_count;
 
 
 }