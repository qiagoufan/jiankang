<?php


namespace model\theatre;

use model\theatre\dto\TheatreDailyShowDTO;

class TheatreModel extends \FUR_Model
{
    public static $_instance;

    const FEED_REDIS_MODULE = 'feed';

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function insertRecord(TheatreDailyShowDTO $theatreDailyShowDTO): int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert('theatre_daily_show', $theatreDailyShowDTO);
    }


    public function queryValidShowListByDate(string $date): ?array
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->select("select * from theatre_daily_show where show_date =? and show_status =1 order by sort ", [$date], TheatreDailyShowDTO::class);
    }

    public function queryShowInfoById(int $id): ?TheatreDailyShowDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select * from theatre_daily_show where id =? ", [$id], TheatreDailyShowDTO::class);
    }

    public function queryShowList(int $start, int $limit): ?array
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->select("select * from theatre_daily_show where  show_status =1 order by show_date desc ,id asc  limit ?,? ", [$start, $limit], TheatreDailyShowDTO::class);
    }

    public function queryShowCount(): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $data = $mysql->selectOne("select count(*) as total_count from theatre_daily_show where  show_status =1 ");
        if ($data != null) {
            return $data->total_count;
        }
        return 0;
    }

    public function setShowDelete(int $id): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $data = ['show_status' => 0, 'updated_timestamp' => time()];
        $args = ['id' => $id];
        return $mysql->update("theatre_daily_show", $data, $args);
    }


}