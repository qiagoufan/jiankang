<?php


namespace model\theatre;

use model\theatre\dto\TheatreChallengeDTO;
use model\theatre\dto\TheatreChallengeFeedDTO;
use model\theatre\dto\TheatreChallengePrizeDTO;
use model\theatre\dto\TheatreDailyShowDTO;

class TheatreChallengeModel extends \FUR_Model
{
    public static $_instance;


    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function queryValidChallengeList(int $timestamp): ?array
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->select("select * from theatre_challenge where challenge_start_timestamp <? and challenge_end_timestamp >? and challenge_status =1 order by id desc ", [$timestamp, $timestamp], TheatreChallengeDTO::class);
    }

    public function queryChallengeInfo(int $challengeId): ?TheatreChallengeDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select * from theatre_challenge where id =? and challenge_status =1   ", [$challengeId], TheatreChallengeDTO::class);
    }

    public function queryChallengePrizeList(int $challengeId): ?array
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->select("select * from theatre_challenge_prize where challenge_id =? order by sort asc   ", [$challengeId], TheatreChallengePrizeDTO::class);
    }

    public function queryChallengeFeedList(int $challengeId, int $index, int $pageSize, int $sortType = 0): ?array
    {
        $mysql = $this->select_mysql_db('main');
        $sortSql = ' order by created_timestamp desc ';
        if ($sortType == TheatreChallengeFeedDTO::SORT_TYPE_LIKE) {
            $sortSql = ' order by like_count desc ';
        }
        $offset = $index * $pageSize;
        return $mysql->select("select * from theatre_challenge_feed where challenge_id =? and is_top= 0  " . $sortSql . " limit ?,?", [$challengeId, $offset, $pageSize], TheatreChallengeFeedDTO::class);
    }

    public function queryChallengeTopFeedList(int $challengeId): ?array
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->select("select * from theatre_challenge_feed where challenge_id =?  and is_top=  1", [$challengeId], TheatreChallengeFeedDTO::class);
    }

    public function queryChallengeAuthorFeedList(int $challengeId, int $pageSize): ?array
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->select("select * from theatre_challenge_feed where challenge_id =? group by author_id  limit 0,?", [$challengeId, $pageSize], TheatreChallengeFeedDTO::class);
    }

    public function updateChallengeFeedLikeCount(int $feedId, int $likeCount)
    {
        $mysql = $this->select_mysql_db('main');
        $args = ['feed_Id' => $feedId];
        $data = ['like_count' => 1, 'updated_timestamp' => time()];

        return $mysql->update("theatre_challenge_feed", $data, $args);
    }

    public function queryChallengeFeedInfo(int $feedId): ?TheatreChallengeFeedDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select * from theatre_challenge_feed where feed_id =?   ", [$feedId], TheatreChallengeDTO::class);
    }

    public function insertChallengeFeed(TheatreChallengeFeedDTO $challengeFeedDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->iinsert("theatre_challenge_feed", $challengeFeedDTO);
    }

}