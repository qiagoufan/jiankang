<?php


namespace model\theatre;

use model\theatre\dto\TheatreCalendarDTO;
use model\theatre\dto\TheatreDailyShowDTO;

class TheatreCalendarModel extends \FUR_Model
{
    public static $_instance;


    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function insertRecord(TheatreCalendarDTO $theatreCalendarDTO): int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert('theatre_calendar', $theatreCalendarDTO);
    }


    public function queryCalendarListByDate(int $eventStartDate, int $eventEndDate): ?array
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->select("select * from theatre_calendar where event_date >? and event_date <? order by event_date desc ,sort_num asc  ", [$eventStartDate, $eventEndDate], TheatreCalendarDTO::class);
    }

    public function queryCalendarCountByDate(int $eventDate): int
    {
        $mysql = $this->select_mysql_db('main');
        $count = 0;
        $data = $mysql->selectOne("select count(*) as total_count from theatre_calendar where event_date =?  ", [$eventDate]);
        if ($data) {
            $count = $data->total_count;
        }

        return $count;

    }

}