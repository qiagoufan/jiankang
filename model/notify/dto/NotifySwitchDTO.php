<?php 
 
namespace model\notify\dto; 
 
class NotifySwitchDTO { 

	 //主键
	 public $id;
	 //用户id
	 public $user_id;
	 //设备号
	 public $device_id;
	 //push开关
	 public $push_switch;
	 //创建时间
	 public $create_timestamp;
	 //更新时间
	 public $update_timestamp;
	 //平台
	 public $platform;
 
 
 }