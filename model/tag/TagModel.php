<?php


namespace model\tag;


use model\tag\dto\TagEntityDTO;
use model\tag\dto\TagInfoDTO;

class TagModel extends \FUR_Model
{
    public static $_instance;

    const TAG_REDIS_MODULE = 'tag';

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function insertTagInfo(TagInfoDTO $tagInfoDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert('tag_info', $tagInfoDTO);
    }


    public function updateTagEntityCount(int $tagId, ?int $count = 1)
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->query('update tag_info set entity_count =entity_count+ ? where id =?', [$tagId, $count]);
    }


    public function queryTagInfoByTagName(string $tagName): ?TagInfoDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select * from tag_info where tag_name =? limit 1 ", [$tagName], TagInfoDTO::class);
    }

    public function queryTagInfoById(int $tagId): ?TagInfoDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select * from tag_info where id =? limit 1 ", [$tagId], TagInfoDTO::class);
    }

    public function queryTagInfoByBiz(int $tagBizId, string $tagBizType): ?TagInfoDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select * from tag_info where tag_biz_id =? and tag_biz_type = ? limit 1 ", [$tagBizId, $tagBizType], TagInfoDTO::class);

    }

    public function queryTagIdByBiz(int $tagBizId, string $tagBizType): ?int
    {
        $mysql = $this->select_mysql_db('main');
        $tagInfoDTO = $mysql->selectOne("select id from tag_info where tag_biz_id =? and tag_biz_type = ? limit 1 ", [$tagBizId, $tagBizType], TagInfoDTO::class);

        if ($tagInfoDTO == null) {
            return 0;
        }
        return $tagInfoDTO->id;
    }

    public function insertTagEntity(TagEntityDTO $tagEntityDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert('tag_entity', $tagEntityDTO);
    }

    public function setTagInfoIdCache(TagInfoDTO $tagInfoDTO): ?bool
    {
        return null;
        $key = $this->buildTagInfoKey($tagInfoDTO->tag_biz_type, $tagInfoDTO->tag_biz_id);
        $redis = $this->get_redis(self::TAG_REDIS_MODULE);
        return $redis->set($key, $tagInfoDTO->id);
    }

    public function getTagIdCache(string $tagBizType, string $tagBizId): ?string
    {
        return null;
        $key = $this->buildTagInfoKey($tagBizType, $tagBizId);
        $redis = $this->get_redis(self::TAG_REDIS_MODULE);
        return $redis->get($key);
    }

    /**
     * tag实体内增加一条记录
     * @param TagEntityDTO $tagEntityDTO
     * @return bool
     */
    public function addTagEntityCache(TagEntityDTO $tagEntityDTO): ?bool
    {
        return null;
        $key = $this->buildTagEntityKey($tagEntityDTO->entity_biz_type, $tagEntityDTO->tag_id);
        $redis = $this->get_redis(self::TAG_REDIS_MODULE);
        return $redis->zadd($key, $tagEntityDTO->created_timestamp, $tagEntityDTO->entity_biz_id);
    }


    public function addFullSiteTagEntityCache(TagEntityDTO $tagEntityDTO): ?bool
    {
        return null;
        $key = $this->buildFullBizTagEntityKey($tagEntityDTO->tag_id);
        $value = $this->buildFullBizTagEntityValue($tagEntityDTO->entity_biz_type, $tagEntityDTO->entity_biz_id);

        $redis = $this->get_redis(self::TAG_REDIS_MODULE);
        return $redis->zadd($key, $tagEntityDTO->created_timestamp, $value);
    }

    public function removeFullSiteTagEntityCache(TagEntityDTO $tagEntityDTO): ?bool
    {
        return null;
        $key = $this->buildFullBizTagEntityKey($tagEntityDTO->tag_id);
        $redis = $this->get_redis(self::TAG_REDIS_MODULE);
        $value = $this->buildFullBizTagEntityValue($tagEntityDTO->entity_biz_type, $tagEntityDTO->entity_biz_id);
        return $redis->zadd($key, $tagEntityDTO->created_timestamp, $value);
    }

    public function queryFullSiteTagEntityListFromCache(int $tagId, int $start, int $pageSize): ?array
    {
        return null;
        $key = $this->buildFullBizTagEntityKey($tagId);
        $redis = $this->get_redis(self::TAG_REDIS_MODULE);
        $offset = $start + $pageSize - 1;
        $list = $redis->zrevrange($key, $start, $offset);

        $tagEntityDTOList = [];
        if ($list) {
            foreach ($list as $value) {
                try {
                    list($entityBizType, $entityBizId) = explode("|", $value);
                    $tagEntityDTO = new TagEntityDTO();
                    $tagEntityDTO->entity_biz_id = $entityBizId;
                    $tagEntityDTO->entity_biz_type = $entityBizType;
                    $tagEntityDTO->tag_id = $tagId;
                    array_push($tagEntityDTOList, $tagEntityDTO);
                } catch (\Exception $e) {
                }
            }
        }
        return $tagEntityDTOList;
    }


    public function cleanTagEntityCache(TagEntityDTO $tagEntityDTO)
    {
        return null;
        $key = $this->buildTagEntityKey($tagEntityDTO->entity_biz_type, $tagEntityDTO->tag_id);
        $redis = $this->get_redis(self::TAG_REDIS_MODULE);
        $redis->del($key);
        return true;
    }

    public function removeTagEntityCache(TagEntityDTO $tagEntityDTO): ?bool
    {
        return null;
        $key = $this->buildTagEntityKey($tagEntityDTO->entity_biz_type, $tagEntityDTO->tag_id);
        $redis = $this->get_redis(self::TAG_REDIS_MODULE);
        $redis->zrem($key, $tagEntityDTO->entity_biz_id);
        return true;
    }

    /**
     * tag实体标内查询记录是否存在
     * @param TagEntityDTO $tagEntityDTO
     * @return bool|null
     */
    public function hasTagEntityCache(TagEntityDTO $tagEntityDTO): ?bool
    {
        return null;
        $key = $this->buildTagEntityKey($tagEntityDTO->entity_biz_type, $tagEntityDTO->tag_id);
        $redis = $this->get_redis(self::TAG_REDIS_MODULE);
        $rank = $redis->zrank($key, $tagEntityDTO->entity_biz_id);

        return ($rank != null);
    }

    public function queryTagEntityListFromCache(int $tagId, ?string $entityBizType, int $start, int $pageSize): ?array
    {
        return null;
        $key = $this->buildTagEntityKey($entityBizType, $tagId);
        $redis = $this->get_redis(self::TAG_REDIS_MODULE);
        $offset = $start + $pageSize - 1;
        return $redis->zrevrange($key, $start, $offset);
    }

    public function getTagEntityCount(int $tagId, ?string $entityBizType): ?int
    {
        return null;
        $key = $this->buildTagEntityKey($entityBizType, $tagId);
        $redis = $this->get_redis(self::TAG_REDIS_MODULE);
        return $redis->ZCARD($key);
    }


    public function deleteTagEntity(int $tagId, int $entityTagId, string $entityBizType): ?bool
    {
        $mysql = $this->select_mysql_db('main');
        $ret = $mysql->delete('tag_entity', ['entity_biz_id' => $entityTagId, 'entity_biz_type' => $entityBizType, 'tag_id' => $tagId]);
        return ($ret != null);
    }

    public function cleanTagEntity(TagEntityDTO $tagEntityDTO): ?bool
    {
        $tagId = $tagEntityDTO->tag_id;
        $entityBizType = $tagEntityDTO->entity_biz_type;
        $mysql = $this->select_mysql_db('main');
        $ret = $mysql->delete('tag_entity', ['tag_id' => $tagId, 'entity_biz_type' => $entityBizType]);
        return ($ret != null);
    }


    public function queryTagEntityList(int $tagId, ?string $entityBizType, int $offset, int $pageSize): ?array
    {
        $mysql = $this->select_mysql_db('main');
        if ($entityBizType == null) {
            $data = $mysql->select("select * from tag_entity where tag_id =? order by id desc limit ?, ?",
                [$tagId, $offset, $pageSize],
                TagEntityDTO::class);
        } else {
            $data = $mysql->select("select * from tag_entity where tag_id =? and entity_biz_type = ?  order by id desc limit ?, ?",
                [$tagId, $entityBizType, $offset, $pageSize],
                TagEntityDTO::class);
        }
        return $data;
    }

    public function hasTagEntity(int $tagId, int $entityTagId, string $entityBizType): ?bool
    {
        $mysql = $this->select_mysql_db('main');
        $data = $mysql->selectOne("select id from tag_entity where entity_biz_id =? and entity_biz_type = ? and tag_id = ?",
            [$entityTagId, $entityBizType, $tagId], TagEntityDTO::class);
        return $data != null;
    }

    private function buildTagInfoKey(string $tagBizType, int $tagBizId)
    {
        return 'tag_info:' . $tagBizType . '_' . $tagBizId;
    }

    private function buildTagEntityKey(string $entityBizType, int $tagId)
    {
        return 'tag_zset:' . $tagId . ':' . $entityBizType;
    }

    private function buildFullBizTagEntityKey(int $tagId)
    {
        return 'tag_zset_full:' . $tagId;
    }

    private function buildFullBizTagEntityValue(string $entityBizType, int $entityTagId)
    {
        return $entityBizType . "|" . $entityTagId;
    }
}