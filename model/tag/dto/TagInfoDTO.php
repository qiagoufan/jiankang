<?php 
 
namespace model\tag\dto; 
 
class TagInfoDTO { 

	 //主键
	 public $id;
	 //标签类型
	 public $tag_category_id;
	 //标签名称
	 public $tag_name;
	 //标签说明
	 public $tag_desc;
	 //标签状态
	 public $tag_status;
	 //标签业务类型
	 public $tag_biz_type;
	 //标签业务id
	 public $tag_biz_id;
	 //实体标数量
	 public $entity_count;
	 //创建时间
	 public $created_timestamp;
	 //更新时间
	 public $updated_timestamp;
 
 
 }