<?php 
 
namespace model\tag\dto; 
 
class TagEntityDTO { 

	 //主键
	 public $id;
	 //标签id
	 public $tag_id;
	 //业务类型
	 public $entity_biz_type;
	 //业务id
	 public $entity_biz_id;
	 //创建时间
	 public $created_timestamp;
	 //更新时间
	 public $updated_timestamp;
 
 
 }