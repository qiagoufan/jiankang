<?php


namespace model\tag;


use model\dto\tag\HotTagDTO;

class HotTagModel extends \FUR_Model
{
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function insertHotTag(HotTagDTO $hotTagDTO): ?int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert('hot_tag', $hotTagDTO);
    }


    public function queryHotTagList(int $offset, int $pageSize): ?array
    {
        
        $mysql = $this->select_mysql_db('main');
        $data = $mysql->select("select * from hot_tag  order by id desc limit ?, ?", [$offset, $pageSize],
            HotTagDTO::class);

        return $data;
    }

    public function deleteHotTag(HotTagDTO $hotTagDTO): ?bool
    {

        $tagId = $hotTagDTO->id;
        $mysql = $this->select_mysql_db('main');
        $ret = $mysql->delete('hot_tag', ['id' => $tagId]);

        return ($ret != null);
    }


    public function queryHotTagInfoByBiz(string $tag_biz_type, int $tag_biz_id): ?HotTagDTO
    {
        $mysql = $this->select_mysql_db('main');
        $data = $mysql->selectOne("select * from hot_tag  where biz_tag_type=? and biz_tag_id=?  limit 1", [$tag_biz_type, $tag_biz_id],
            HotTagDTO::class);
        return $data;

    }

}