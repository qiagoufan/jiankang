<?php


namespace model;


use model\dto\NotifySwitchDTO;

class NotifySwitchModel extends \FUR_Model
{
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function insertRecord(NotifySwitchDTO $notifySwitchDTO): int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert("notify_switch", $notifySwitchDTO);
    }

    public function updateRecord(NotifySwitchDTO $notifySwitchDTO): int
    {
        $mysql = $this->select_mysql_db('main');

        $args = ['device_id' => $notifySwitchDTO->device_id];
        return $mysql->update("notify_switch", $notifySwitchDTO, $args);

    }

    public function queryNotifySwitchDetail(string $device_id): ?NotifySwitchDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne("select * from notify_switch where device_id =? limit 1 ", [$device_id], NotifySwitchDTO::class);
    }

}