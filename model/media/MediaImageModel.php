<?php

namespace model\media;

use model\dto\media\ImageInfoDTO;
use model\dto\media\VideoInfoDTO;

class MediaImageModel extends \FUR_Model
{
    public static $_instance;


    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function insertImageInfo(ImageInfoDTO $imageInfoDTO): int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert('media_image', $imageInfoDTO);
    }


    public function queryImageById(int $id): ?ImageInfoDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne('select * from media_image where id = ?', [$id], ImageInfoDTO::class);
    }

    public function queryImageByImageId(string $imageId): ?ImageInfoDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne('select * from media_image where image_id = ?', [$imageId], ImageInfoDTO::class);
    }
}