<?php

namespace model\media;

use model\dto\media\VideoInfoDTO;

class MediaVideoModel extends \FUR_Model
{
    public static $_instance;


    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    /**
     * 插入短视频信息
     * @param VideoInfoDTO $videoInfoDTO
     * @return int
     */
    public function insertVideoInfo(VideoInfoDTO $videoInfoDTO): int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->insert('media_video', $videoInfoDTO);
    }


    /**
     * 更新短视频信息
     * @param VideoInfoDTO $videoInfoDTO
     * @return int
     */
    public function updateVideo(VideoInfoDTO $videoInfoDTO): int
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->update('media_video', $videoInfoDTO, ['id' => $videoInfoDTO->id]);
    }

    public function queryVideoById(int $id): ?VideoInfoDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne('select * from media_video where id = ?', [$id], VideoInfoDTO::class);
    }

    public function queryVideoByVideoId(string $videoId): ?VideoInfoDTO
    {
        $mysql = $this->select_mysql_db('main');
        return $mysql->selectOne('select * from media_video where video_id = ?', [$videoId], VideoInfoDTO::class);
    }
}