<?php


namespace model\media\dto;

class ImageInfoDTO
{
    const CHECK_STATUS_NEW = 1;

    public $id;
    public $image_url;
    public $original_url;
    public $user_id;
    public $file_size;
    public $image_width;
    public $image_height;
    public $image_id;
    public $check_status;
    public $created_timestamp;
    public $updated_timestamp;

}