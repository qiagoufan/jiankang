<?php 
 
namespace model\media\dto; 
 
class MediaVideoTranscodeDTO { 

	 //主键
	 public $id;
	 //阿里云视频id
	 public $video_id;
	 //视频id
	 public $vid;
	 //转码模板组id
	 public $template_group_id;
	 //取值范围：[1-10]
	 public $priority;
	 //转码状态
	 public $transcode_status;
	 //转码任务id
	 public $job_id;
	 //创建时间
	 public $created_timestamp;
	 //更新时间
	 public $updated_timestamp;
 
 
 }