<?php


namespace model\media\dto;

class VideoInfoDTO
{
    const CHECK_STATUS_NEW = 1;

    const TRANSCODE_STATUS_NEW = 1;

    public $id;
    public $video_id;
    public $original_play_url;
    public $original_file_size;
    public $play_url;
    public $play_file_size;
    public $cover_url;
    public $title;
    public $user_id;
    public $video_width;
    public $video_height;
    public $duration;
    public $transcode_status;
    public $check_status;
    public $created_timestamp;
    public $updated_timestamp;

}