<?php 
 
namespace model\media\dto; 
 
class MediaImageDTO { 

	 //主键id
	 public $id;
	 //图片地址
	 public $image_url;
	 //原始文件地址
	 public $original_url;
	 //上传的用户id
	 public $user_id;
	 //文件大小
	 public $file_size;
	 //图片后缀
	 public $image_ext;
	 //图片宽度
	 public $image_width;
	 //图片高度
	 public $image_height;
	 //阿里云图片id
	 public $image_id;
	 //审核状态
	 public $check_status;
	 //创建时间
	 public $created_timestamp;
	 //更新时间
	 public $updated_timestamp;
 
 
 }