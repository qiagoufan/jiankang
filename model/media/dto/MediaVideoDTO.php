<?php 
 
namespace model\media\dto; 
 
class MediaVideoDTO { 

	 //主键
	 public $id;
	 //阿里云短视频id
	 public $video_id;
	 //原画播放地址
	 public $original_play_url;
	 //原画文件大小
	 public $original_file_size;
	 //播放地址
	 public $play_url;
	 //播放地址的文件大小
	 public $play_file_size;
	 //封面信息
	 public $cover_url;
	 //标题
	 public $title;
	 //上传者id
	 public $user_id;
	 //视频宽度
	 public $video_width;
	 //视频高度
	 public $video_height;
	 //视频时长
	 public $duration;
	 //视频状态
	 public $transcode_status;
	 //审核状态
	 public $check_status;
	 //创建时间
	 public $created_timestamp;
	 //更新时间
	 public $updated_timestamp;
 
 
 }