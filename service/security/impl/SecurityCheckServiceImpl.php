<?php

namespace service\security\impl;


use facade\request\PageParams;
use facade\response\Result;
use facade\service\security\SecurityCheckService;
use model\security\SecurityWordModel;
use model\security\dto\SecurityWordDTO;

class SecurityCheckServiceImpl implements SecurityCheckService
{


    public static $_instance;


    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function checkContent(?string $string): ?Result
    {
        $result = new Result();
        $list = [];
        $wordList = SecurityWordModel::getInstance()->getAllSecurityWordList();
        foreach ($wordList as $word) {
            array_push($list, $word->word);
        }


        $count = 0; //违规词的个数
        $sensitiveWord = '';  //违规词
        $stringAfter = $string;  //替换后的内容
        $pattern = "/" . implode("|", $list) . "/i"; //定义正则表达式
        if (preg_match_all($pattern, $string, $matches)) { //匹配到了结果
            $patternList = $matches[0];  //匹配到的数组
            $count = count($patternList);
            $sensitiveWord = implode(',', $patternList); //敏感词数组转字符串
            $replaceArray = array_combine($patternList, array_fill(0, count($patternList), '*')); //把匹配到的数组进行合并，替换使用
            $stringAfter = strtr($string, $replaceArray); //结果替换
        }
        if ($count == 0) {
            $result->setSuccessWithResult(true);
        } else {
            $result->setError(\Config_Error::ERR_SECURITY_WORD);

        }
        return $result;
    }

    public function addWord(?string $content): ?Result
    {
        $wordDTO = new SecurityWordDTO();
        $wordDTO->word = $content;
        $wordDTO->created_timestamp = time();
        $id = SecurityWordModel::getInstance()->insertSecurityWord($wordDTO);
        $result = new Result();
        $result->setSuccessWithResult($id);
        return $result;
    }

    public function getDetail(int $id): ?Result
    {
        $data = SecurityWordModel::getInstance()->getSecurityWord($id);
        $result = new Result();
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function updateWord(SecurityWordDTO $securityWordDTO): ?Result
    {
        $securityWordDTO->updated_timestamp = time();
        $data = SecurityWordModel::getInstance()->updateSecurityWord($securityWordDTO);
        $result = new Result();
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function deleteWord(int $id): ?Result
    {
        $data = SecurityWordModel::getInstance()->deleteSecurityWord($id);
        $result = new Result();
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function getWordList(): ?Result
    {
        $data = SecurityWordModel::getInstance()->getAllSecurityWordList();
        $result = new Result();
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function queryWordList(PageParams $pageParams): ?Result
    {
        $data = SecurityWordModel::getInstance()->getSecurityWordList($pageParams);
        $result = new Result();
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function queryWordCount(PageParams $pageParams): ?Result
    {
        $data = SecurityWordModel::getInstance()->getSecurityWordCount($pageParams);
        $result = new Result();
        $result->setSuccessWithResult($data);
        return $result;
    }
}