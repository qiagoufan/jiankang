<?php

namespace service\sign\impl;

use facade\request\PageParams;
use facade\request\sign\EditSignTaskParams;
use facade\request\sign\EditSignTipsParams;
use facade\request\sign\EditSignTypeParams;
use facade\request\sign\SignUserTaskParams;
use facade\response\Result;
use model\sign\dto\SignInTaskDTO;
use model\sign\dto\SignInTaskRecordDTO;
use model\sign\dto\SignInTipsDTO;
use model\sign\dto\SignInTypeDTO;
use model\sign\dto\SignInUserFinishRecordDTO;
use model\sign\SignModel;

class SignServiceImpl
{
    public static $_instance;


    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function createSignType(EditSignTypeParams $editSignParams): ?Result
    {
        $result = new Result();

        $signTypeDTO = new SignInTypeDTO();
        $signTypeDTO->created_timestamp = time();
        $signTypeDTO->status = 1;
        $signTypeDTO->sign_type_name = $editSignParams->sign_type_name;
        $signTypeDTO->sign_type_bg = $editSignParams->sign_type_bg;
        $signTypeDTO->seq = $editSignParams->seq;
        $data = SignModel::getInstance()->createSignType($signTypeDTO);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function updateSignType(EditSignTypeParams $editSignParams): ?Result
    {
        $result = new Result();

        $signTypeDTO = SignModel::getInstance()->getSignTypeDetail($editSignParams->id);
        if ($signTypeDTO == null) {
            $result->setError(\Config_Error::ERR_JK_SIGN_TASK_TYPE_NOT_CHOOSE);
            return $result;
        }
        $signTypeDTO->updated_timestamp = time();
        $signTypeDTO->sign_type_name = $editSignParams->sign_type_name;
        if ($editSignParams->sign_type_bg) {
            $signTypeDTO->sign_type_bg = $editSignParams->sign_type_bg;
        }
        $signTypeDTO->seq = $editSignParams->seq;
        $data = SignModel::getInstance()->updateSignType($signTypeDTO);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function getSignTypeDetail(int $signTypeId): ?Result
    {
        $result = new Result();
        $signTypeDTO = SignModel::getInstance()->getSignTypeDetail($signTypeId);
        if ($signTypeDTO == null) {
            $result->setError(\Config_Error::ERR_JK_SIGN_TASK_TYPE_NOT_CHOOSE);
            return $result;
        }
        $result->setSuccessWithResult($signTypeDTO);
        return $result;
    }

    public function querySignTypeList(PageParams $params): ?Result
    {
        $result = new Result();
        $typeList = SignModel::getInstance()->querySignTypeList($params->index, $params->page_size);
        $result->setSuccessWithResult($typeList);
        return $result;
    }

    public function querySignTypeCount(): ?Result
    {
        $result = new Result();
        $data = SignModel::getInstance()->querySignTypeCount();
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function deleteSignType(int $signTypeId): ?Result
    {
        $result = new Result();
        $typeList = SignModel::getInstance()->deleteSignType($signTypeId);
        $result->setSuccessWithResult($typeList);
        return $result;
    }

    public function createSignTask(EditSignTaskParams $editSignParams): ?Result
    {
        $result = new Result();
        $signTaskDTO = new SignInTaskDTO();
        \FUR_Core::copyProperties($editSignParams, $signTaskDTO);
        $signTaskDTO->created_timestamp = time();
        $data = SignModel::getInstance()->createSignTask($signTaskDTO);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function updateSignTask(SignInTaskDTO $signInTaskDTO): ?Result
    {
        $result = new Result();

        $signInTaskDTO->updated_timestamp = time();
        $data = SignModel::getInstance()->updateSignTask($signInTaskDTO);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function getSignTaskDetail(int $signTaskId): ?Result
    {
        $result = new Result();
        $data = SignModel::getInstance()->getSignTaskDetail($signTaskId);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function deleteSignTask(int $signTaskId): ?Result
    {
        $result = new Result();
        $typeList = SignModel::getInstance()->deleteSignTask($signTaskId);
        $result->setSuccessWithResult($typeList);
        return $result;
    }

    public function querySignTaskList(int $signTypeId, PageParams $params): ?Result
    {
        $result = new Result();
        $typeList = SignModel::getInstance()->querySignTypeTaskList($signTypeId, $params);
        $result->setSuccessWithResult($typeList);
        return $result;
    }

    public function userSign(SignUserTaskParams $params): ?Result
    {
        $result = new Result();

        $signTaskInfo = SignModel::getInstance()->getSignTaskDetail($params->sign_task_id);
        if ($signTaskInfo == null) {
            $result->setError(\Config_Error::ERR_JK_SIGN_TASK_NOT_EXIST);
            return $result;
        }
        $finish_status = 0;
        if ($signTaskInfo->task_value <= $params->user_score) {
            $finish_status = 1;
        }

        $sign_in_date = date('Ymd');
        //删除旧的,再插入新的
        SignModel::getInstance()->deleteUserTaskRecord($params->sign_task_id, $params->user_id, $sign_in_date);
//        $data = SignModel::getInstance()->queryUserSignTaskRecord($params->sign_task_id, $params->user_id, $sign_in_date);
//        if ($data) {
//            $result->setError(\Config_Error::ERR_JK_SIGN_TASK_HAS_SIGN);
//            return $result;
//        }


        $signDTO = new SignInTaskRecordDTO();
        $signDTO->user_id = $params->user_id;
        $signDTO->sign_in_date = $sign_in_date;
        $signDTO->user_score = $params->user_score;
        $signDTO->sign_in_type_id = $signTaskInfo->sign_in_type_id;
        $signDTO->sign_in_task_id = $signTaskInfo->id;
        $signDTO->user_finish_status = $finish_status;
        $signDTO->created_timestamp = time();

        $id = SignModel::getInstance()->insertSignTaskRecord($signDTO);

        $totalTaskNum = SignModel::getInstance()->querySignTypeTaskCount($signTaskInfo->sign_in_type_id);
        $totalUserFinishNum = SignModel::getInstance()->queryUserFinishCount($params->user_id, $sign_in_date, $signTaskInfo->sign_in_type_id);
        if ($totalUserFinishNum == $totalTaskNum) {
            $signFinishDTO = new SignInUserFinishRecordDTO();
            $signFinishDTO->user_id = $params->user_id;
            $signFinishDTO->created_timestamp = time();
            $signFinishDTO->sign_in_date = $sign_in_date;
            $signFinishDTO->sign_type_id = $signTaskInfo->sign_in_type_id;
            SignModel::getInstance()->insetUserFinishRecord($signFinishDTO);
        }

        $result->setSuccessWithResult($id);
        return $result;
    }

    public function queryUserSignTaskRecord(int $signTaskId, int $userId, int $date): ?Result
    {
        $result = new Result();
        $data = SignModel::getInstance()->queryUserSignTaskRecord($signTaskId, $userId, $date);
        $result->setSuccessWithResult($data);
        return $result;
    }
    public function queryUserSignTaskRecordList(int $signTaskId, int $userId, $startDate, $endDate): ?Result
    {
        $result = new Result();
        $data = SignModel::getInstance()->queryUserSignTaskRecordList($signTaskId, $userId, $startDate, $endDate);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function queryUserSignFinishRecordList(int $signTypeId, int $userId, $startDate, $endDate): ?Result
    {
        $result = new Result();
        $data = SignModel::getInstance()->queryUserSignFinishRecordList($signTypeId, $userId, $startDate, $endDate);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function queryUserSignFinishRecord(int $signTypeId, int $userId, $date): ?Result
    {
        $result = new Result();

        $data = SignModel::getInstance()->queryUserSignFinishRecord($signTypeId, $userId, $date);
        $result->setSuccessWithResult($data);
        return $result;
    }


    public function createSignTips(EditSignTipsParams $editSignTipsParams): ?Result
    {
        $result = new Result();
        $signTipsDTO = new SignInTipsDTO();
        \FUR_Core::copyProperties($editSignTipsParams, $signTipsDTO);
        $signTipsDTO->created_timestamp = time();
        $data = SignModel::getInstance()->createSignTips($signTipsDTO);

        $result->setSuccessWithResult($data);
        return $result;
    }

    public function updateSignTips(SignInTipsDTO $signInTipsDTO): ?Result
    {
        $result = new Result();

        $signInTipsDTO->updated_timestamp = time();
        $data = SignModel::getInstance()->updateSignTips($signInTipsDTO);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function getSignTipsDetail(int $signTipsId): ?Result
    {

        $result = new Result();
        $data = SignModel::getInstance()->getSignTipsDetail($signTipsId);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function deleteSignTips(int $signTipsId): ?Result
    {
        $result = new Result();
        $data = SignModel::getInstance()->deleteSignTips($signTipsId);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function getSignTipsList(int $signTypeId, PageParams $params): ?Result
    {
        $result = new Result();
        $data = SignModel::getInstance()->getSignTipsList($signTypeId, $params);
        $result->setSuccessWithResult($data);
        return $result;

    }

    public function getSignTipsCount(int $signTypeId): ?Result
    {
        $result = new Result();
        $data = SignModel::getInstance()->getSignTipsCount($signTypeId);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function getLatestTips(int $signTypeId, int $showDate): ?Result
    {
        $result = new Result();
        $data = SignModel::getInstance()->getLatestTips($signTypeId, $showDate);
        $result->setSuccessWithResult($data);
        return $result;
    }
}
