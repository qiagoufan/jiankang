<?php

namespace service\operation\impl;

use facade\request\PageParams;
use facade\response\Result;
use facade\service\operation\AdvertisingService;
use model\operation\AdvertisingModel;
use model\operation\dto\OperationAdvertisingDTO;

class AdvertisingServiceImpl implements AdvertisingService
{
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function getAdvertisingList(PageParams $params): ?Result
    {
        $result = new Result();
        $advertisingList = AdvertisingModel::getInstance()->queryAdvertisingList($params);
        $result->setSuccessWithResult($advertisingList);
        return $result;
    }

    public function getAdvertisingListBySpace(int $spaceId): ?Result
    {
        $result = new Result();
        $advertisingList = AdvertisingModel::getInstance()->queryAdvertisingListBySpace($spaceId);
        $result->setSuccessWithResult($advertisingList);
        return $result;
    }

    public function queryAdvertisingCount(int $spaceId): ?Result
    {
        $result = new Result();
        $count = AdvertisingModel::getInstance()->queryAdvertisingCountBySpace($spaceId);
        $result->setSuccessWithResult($count);
        return $result;

    }

    public function getAdvertisingDetail(int $advertising_id): ?Result
    {
        $result = new Result();
        $advertisingDetail = AdvertisingModel::getInstance()->queryRecord($advertising_id);
        $result->setSuccessWithResult($advertisingDetail);
        return $result;
    }

    public function createAdvertising(OperationAdvertisingDTO $advertisingDTO): ?Result
    {
        $result = new Result();
        $recordId = AdvertisingModel::getInstance()->insertRecord($advertisingDTO);
        $result->setSuccessWithResult($recordId);
        return $result;
    }

    public function updateAdvertising(OperationAdvertisingDTO $advertisingDTO): ?Result
    {
        $result = new Result();
        $ret = AdvertisingModel::getInstance()->updateAdvertising($advertisingDTO);
        $result->setSuccessWithResult($ret);
        return $result;
    }

    public function deleteAdvertising(int $advertising_id): ?Result
    {
        $result = new Result();
        $ret = AdvertisingModel::getInstance()->deleteAdvertising($advertising_id);
        $result->setSuccessWithResult($ret);
        return $result;
    }
}