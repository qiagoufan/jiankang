<?php

namespace service\alipay\impl;
require_once(PUBLIC_PATH . '/lib/alipay/AopSdk.php');

use facade\response\order\QueryOrderPayInfoResp;
use facade\service\alipay\AlipayService;
use facade\response\Result;
use facade\request\order\CreateOrderPayInfoParams;
use Monolog\Logger;


class AlipayServiceImpl implements AlipayService
{

    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    /**
     * APP支付订单
     */
    public function createAliPayOrder($pay_type, $product_name, $total_price, $order_sn): ?Result
    {

        $result = new Result();
        $aop = new \AopClient ();

        $aop->gatewayUrl = 'https://openapi.alipay.com/gateway.do';
        $aop->appId = \FUR_Config::get_alipay_config('ALIPAY_APP_ID');
        $aop->alipayrsaPublicKey = \FUR_Config::get_alipay_config('ALIPAY_PUBLIC_KEY');
        $aop->rsaPrivateKey = \FUR_Config::get_alipay_config('MERCHANT_PRIVATE_KEY');
        $aop->apiVersion = '1.0';
        $aop->signType = 'RSA2';
        $aop->postCharset = 'UTF-8';
        $aop->format = 'json';


        $content['subject'] = $product_name;//商品的标题/交易标题/订单标题/订单关键字等。
        $content['out_trade_no'] = $order_sn;
        $content['timeout_express'] = "30m";//该笔订单允许的最晚付款时间，逾期将关闭交易。取值范围：1m～15d。m-分钟，h-小时，d-天，1c-当天（1c-当天的情况下，无论交易何时创建，都在0点关闭）。 该参数数值不接受小数点， 如 1.5h，可转换为 90m。注：若为空，则默认为15d。
        $content['total_amount'] = number_format(floatval($total_price) / 100, 2, ".", "");//    订单总金额，单位为元，精确到小数点后两位，取值范围[0.01,100000000]，
        $content['product_code'] = "QUICK_MSECURITY_PAY";//    销售产品码，商家和支付宝签约的产品码，为固定值QUICK_MSECURITY_PAY
        $content['goods_type'] = "0";
        if ($pay_type == 1) {
            $request = new \AlipayTradeAppPayRequest();
            $request->setNotifyUrl(\FUR_Config::get_alipay_config('ALIPAY_CALLBACKURL'));
            $request->setBizContent(json_encode($content));
            $executeResult = $aop->sdkExecute($request);
            $result->setSuccessWithResult($executeResult);
            return $result;
        }

        if ($pay_type == 3) {
            $request = new \AlipayTradeWapPayRequest ();
            $request->setNotifyUrl(\FUR_Config::get_alipay_config('ALIPAY_CALLBACKURL'));
            $request->setReturnUrl(\FUR_Config::get_alipay_config('ALI_WEB_PAY_REDIRECT_URL'));
            $request->setBizContent(json_encode($content));
            $executeResult = $aop->pageExecute($request);
            $result->setSuccessWithResult($executeResult);
            return $result;
        }

    }


    /**
     * @param $order_sn
     * @param $payment_trade_no
     * @return Result|null
     * @throws \Exception
     * 查询支付结果
     */
    public function queryAliPayResult($order_sn, $payment_trade_no): ?Result
    {
        $queryResult = new Result();
        $aop = new \AopClient ();
        $ueryOrderPayInfoResp = new QueryOrderPayInfoResp();
        $aop->gatewayUrl = \FUR_Config::get_alipay_config('GATE_WAY_URL');
        $aop->appId = \FUR_Config::get_alipay_config('ALIPAY_APP_ID');
        $aop->rsaPrivateKey = \FUR_Config::get_alipay_config('MERCHANT_PRIVATE_KEY');
        $aop->alipayrsaPublicKey = \FUR_Config::get_alipay_config('ALIPAY_PUBLIC_KEY');
        $aop->apiVersion = '1.0';
        $aop->signType = 'RSA2';
        $aop->postCharset = 'UTF-8';
        $aop->format = 'json';
        $request = new \AlipayTradeQueryRequest ();

        $content['out_trade_no'] = $order_sn;
        $content['trade_no'] = $payment_trade_no;
        $request->setBizContent(json_encode($content));
        $result = $aop->execute($request);

        $responseNode = str_replace(".", "_", $request->getApiMethodName()) . "_response";
        $resultCode = $result->$responseNode->code;
        if (!empty($resultCode) && $resultCode == 10000) {
            $ueryOrderPayInfoResp->result = 1;
            $ueryOrderPayInfoResp->order_info = $result;
        } else {
            $ueryOrderPayInfoResp->result = 0;
            $ueryOrderPayInfoResp->order_info = $result;
        }
        $queryResult->setSuccessWithResult(($ueryOrderPayInfoResp));
        return $queryResult;

    }


}