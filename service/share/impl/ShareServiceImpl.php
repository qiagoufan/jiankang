<?php


namespace service\share\impl;


use facade\request\feed\ConversionFeedParams;
use facade\request\product\GetProductDetailParams;
use facade\request\share\GetShareInfoParams;
use facade\service\share\ShareService;
use facade\response\Result;
use service\feed\impl\FeedServiceImpl;
use service\product\impl\ProductServiceImpl;
use facade\response\share\GetShareInfoResp;
use service\user\impl\StarServiceImpl;
use service\feed\impl\FeedInfoRespServiceImpl;

class ShareServiceImpl implements ShareService
{
    public static $_instance;


    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function getShareInfoToThird(GetShareInfoParams $getShareInfoParams): ?Result
    {
        $result = new Result();
        $type = $getShareInfoParams->share_type;
        if ($type == GetShareInfoParams::SHARE_TYPE_PRODUCT || $type == GetShareInfoParams::SHARE_TYPE_SUPER_PRODUCT) {

            $biz_id = $getShareInfoParams->biz_id;
            $productServiceImpl = ProductServiceImpl::getInstance();
            $productSkuDetailParams = new GetProductDetailParams();
            $productSkuDetailParams->sku_id = $biz_id;
            $productDetailRet = $productServiceImpl->getProductDetail($productSkuDetailParams);
            if (!Result::isSuccess($productDetailRet)) {
                $result->setError(\Config_Error::ERR_PRODUCT_ID_IS_NULL);
                return $result;
            }
            $shareData = $productDetailRet->data;
        } elseif ($type == GetShareInfoParams::SHARE_TYPE_STAR_FEED) {
            $biz_id = $getShareInfoParams->biz_id;
            $starService = StarServiceImpl::getInstance();
            $startInfoRet = $starService->getStarInfo($biz_id);

            if (!Result::isSuccess($startInfoRet)) {
                $result->setError(\Config_Error::ERR_STAR_FEED_IS_NULL);
                return $result;
            }

            /** @var UserStarDTO $starDTO */
            $shareData = $startInfoRet->data;
        } elseif ($type == GetShareInfoParams::SHARE_TYPE_MATERIALS_FEED) {
            $biz_id = $getShareInfoParams->biz_id;
            $feedServiceImpl = FeedServiceImpl::getInstance();
            $feedDetailRet = $feedServiceImpl->getFeedDetail($biz_id);
            $conversionFeedParams = new ConversionFeedParams();
            $conversionFeedParams->user_id = $getShareInfoParams->user_id;
            $feedInfoResp = FeedInfoRespServiceImpl::getInstance()->conversionFeedInfoResp($feedDetailRet->data, $conversionFeedParams);
            $shareData = $feedInfoResp;
        } else {
            $result->setError(\Config_Error::ERR_SHARE_TYPE_IS_INCORRECT);
            return $result;
        }
        $result->setSuccessWithResult($shareData);
        return $result;

    }

}