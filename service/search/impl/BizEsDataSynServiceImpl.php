<?php

namespace service\search\impl;

use facade\request\feed\search\DeleteEsDocumentParams;
use facade\request\feed\search\EsDataSynParams;
use facade\request\feed\search\WriteEsDocumentParams;
use facade\response\Result;
use facade\service\search\BizEsDataSynService;
use lib\AppConstant;
use model\dto\ProductItemDTO;
use model\dto\UserStarDTO;
use service\product\impl\ProductServiceImpl;
use service\user\impl\StarServiceImpl;


class BizEsDataSynServiceImpl implements BizEsDataSynService
{

    const DEFAULT_PAGE_SIZE = 5;

    const DEFAULT_INDEX = 'default';


    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function setBizRecord(EsDataSynParams $dataSynchronizeParams): ?Result
    {
        $result = new Result();
        /**
         * 先清除,再写入
         */
        $this->delBizRecord($dataSynchronizeParams);

        /**
         * 写入ES
         */
        $createEsDocumentParams = new WriteEsDocumentParams();
        $createEsDocumentParams->index = SearchServiceImpl::DEFAULT_INDEX;
        $createEsDocumentParams->type = $dataSynchronizeParams->biz_type;
        $createEsDocumentParams->id = $dataSynchronizeParams->biz_id;
        $createEsDocumentParams->body = $this->getBizBody($dataSynchronizeParams);
        if ($createEsDocumentParams->body == null) {
            $result->setError(\Config_Error::ERR_SEARCH_ES_BODY_IS_NULL);
        }

        return SearchServiceImpl::getInstance()->createEsDoc($createEsDocumentParams);

    }

    public function delBizRecord(EsDataSynParams $dataSynchronizeParams): ?Result
    {
        $deleteEsDocumentParams = new DeleteEsDocumentParams();
        $deleteEsDocumentParams->index = SearchServiceImpl::DEFAULT_INDEX;
        $deleteEsDocumentParams->type = $dataSynchronizeParams->biz_type;
        $deleteEsDocumentParams->id = $dataSynchronizeParams->biz_id;
        return SearchServiceImpl::getInstance()->deleteEsDoc($deleteEsDocumentParams);
    }


    private function getBizBody(EsDataSynParams &$dataSynchronizeParams)
    {
        $body = null;
        if ($dataSynchronizeParams->biz_type == AppConstant::BIZ_TYPE_STAR) {
            $starInfoRet = StarServiceImpl::getInstance()->getStarInfo($dataSynchronizeParams->biz_id);
            if (Result::isSuccess($starInfoRet) == false) {
                return $body;
            }

            /** @var UserStarDTO $starInfoDTO */
            $starInfoDTO = $starInfoRet->data;
            $body = \FUR_Core::buildArray($starInfoDTO);
            if (is_array($body)) {
                $body['keyword'] = $starInfoDTO->star_name;
            }

        } elseif ($dataSynchronizeParams->biz_type == AppConstant::BIZ_TYPE_SPU_ITEM) {
            $itemInfoRet = ProductServiceImpl::getInstance()->getProductItem($dataSynchronizeParams->biz_id);
            if (Result::isSuccess($itemInfoRet) == false) {
                return $body;
            }

            /** @var ProductItemDTO $itemInfoDTO */
            $itemInfoDTO = $itemInfoRet->data;
            $body = \FUR_Core::buildArray($itemInfoDTO);
            if (is_array($body)) {
                $body['keyword'] = $itemInfoDTO->product_name;
            }
        }
        return $body;
    }
}