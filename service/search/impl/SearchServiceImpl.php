<?php

namespace service\search\impl;

use db\ElasticSearchClient;
use facade\request\feed\search\WriteEsDocumentParams;
use facade\request\feed\search\DeleteEsDocumentParams;
use facade\request\feed\SearchParams;
use facade\request\product\GetProductDetailParams;
use facade\request\tag\QueryHotTagListParams;
use facade\response\Result;
use facade\response\search\SearchKeywordResp;
use facade\service\search\SearchService;
use lib\AppConstant;
use model\search\dto\SearchResultDTO;
use model\search\ShoppingCartModel;
use service\product\impl\ProductServiceImpl;
use service\tag\impl\HotTagServiceImpl;
use service\user\impl\StarServiceImpl;


class SearchServiceImpl implements SearchService
{

    const DEFAULT_PAGE_SIZE = 5;

    const DEFAULT_INDEX = 'default';


    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function getRecommendWordList(SearchParams $searchParams): ?Result
    {
        $result = new Result();
        $searchKeywordResp = new SearchKeywordResp();
        $keyword = $searchParams->keyword;
        $scene = $searchParams->scene == null ? 'index_page' : $searchParams->scene;
        $recommendKeywordList = [];

        /**
         * 默认推荐热词
         */
        if ($keyword == "") {
            $queryHotTagListParams = new QueryHotTagListParams();
            $hotTagServiceImpl = HotTagServiceImpl::getInstance();
            $hotTagListRet = $hotTagServiceImpl->getHotTagList($queryHotTagListParams);

            if (Result::isSuccess($hotTagListRet)) {
                $hotTagList = $hotTagListRet->data;
                $starServiceImpl = StarServiceImpl::getInstance();
                $productSkuServiceImpl = ProductServiceImpl::getInstance();
                foreach ($hotTagList as $tag) {
                    if ($tag->biz_tag_type == 'star') {
                        $starInfoRet = $starServiceImpl->getStarInfo($tag->biz_tag_id);
                        if (!Result::isSuccess($starInfoRet)) {
                            continue;
                        }
                        array_push($recommendKeywordList, $starInfoRet->data->star_name);
                        if ($scene == 'index_page') {
                            break;
                        }
                    } elseif ($tag->biz_tag_type == 'sku') {
                        $productSkuDetailParams = new GetProductDetailParams();
                        $productSkuDetailParams->sku_id = $tag->biz_tag_id;
                        $productSkuDetailResult = $productSkuServiceImpl->getProductDetail($productSkuDetailParams);
                        if (!Result::isSuccess($productSkuDetailResult)) {
                            continue;
                        }
                        array_push($recommendKeywordList, $productSkuDetailResult->data->product_name);
                        if ($scene == 'index_page') {
                            break;
                        }
                    }
                }
            }
        } else {
            $itemSearchResultDTOList = ShoppingCartModel::getInstance()->getItemRelatedContent($keyword);
            if ($itemSearchResultDTOList != null) {
                /** @var SearchResultDTO $searchResultDTO */
                foreach ($itemSearchResultDTOList as $searchResultDTO) {
                    array_push($recommendKeywordList, $searchResultDTO->content);
                }
            }
            $starSearchResultDTOList = ShoppingCartModel::getInstance()->getStarRelatedContent($keyword);
            if ($starSearchResultDTOList != null) {
                /** @var SearchResultDTO $searchResultDTO */
                foreach ($starSearchResultDTOList as $searchResultDTO) {
                    array_push($recommendKeywordList, $searchResultDTO->content);
                }
            }
            shuffle($recommendKeywordList);
        }


        $wordList = explode(" ", $keyword);
        $searchKeywordResp->word_list = $wordList;
        $searchKeywordResp->recommend_keyword_list = $recommendKeywordList;
        $result->setSuccessWithResult($searchKeywordResp);
        return $result;
    }

    public function getSearchDTOList(SearchParams $searchParams): ?Result
    {
        $result = new Result();
        $keyword = $searchParams->keyword;
        $searchResultDTOList = [];
        $offset = $searchParams->index * self::DEFAULT_PAGE_SIZE;
        $pageSize = self::DEFAULT_PAGE_SIZE;
        /**
         * 查ip的
         */

        $body = [
            'query' => [
                'match' => [
                    'star_name' => $keyword
                ]
            ]
        ];
        $searchIpResultList = ElasticSearchClient::search(SearchServiceImpl::DEFAULT_INDEX, AppConstant::BIZ_TYPE_STAR, $body, $offset, $pageSize);


        if ($searchIpResultList != null) {
            foreach ($searchIpResultList as $document) {
                $searchResultDTO = new SearchResultDTO();
                $searchResultDTO->content_type = AppConstant::BIZ_TYPE_STAR;
                $searchResultDTO->id = $document['_id'];
                array_push($searchResultDTOList, $searchResultDTO);
            }
        }

        /**
         * 查商品的
         */
        $body = [
            'query' => [
                'match' => [
                    'keyword' => $keyword
                ]
            ]
        ];
        $searchItemResultList = ElasticSearchClient::search(SearchServiceImpl::DEFAULT_INDEX, AppConstant::BIZ_TYPE_SPU_ITEM, $body, $offset, $pageSize);

        if ($searchItemResultList != null && !empty($searchItemResultList)) {
            foreach ($searchItemResultList as $document) {
                try {
                    $searchResultDTO = new SearchResultDTO();
                    $searchResultDTO->content_type = AppConstant::BIZ_TYPE_SKU;
                    $searchResultDTO->id = $document['_source']['default_sku_id'];
                    array_push($searchResultDTOList, $searchResultDTO);
                } catch (\Exception $e) {
                    continue;
                }
            }
        }
        $result->setSuccessWithResult($searchResultDTOList);
        return $result;
    }

    public function getItemSearchDTOList(SearchParams $searchParams): ?Result
    {
        $result = new Result();
        $keyword = $searchParams->keyword;
        $searchResultDTOList = [];
        $offset = $searchParams->index * self::DEFAULT_PAGE_SIZE;
        $pageSize = self::DEFAULT_PAGE_SIZE;

        $searchRecordList = ShoppingCartModel::getInstance()->getItemRelatedContent($keyword, $offset, $pageSize);

        if ($searchRecordList != null) {
            /** @var SearchResultDTO $searchResultDTO */
            foreach ($searchRecordList as $searchResultDTO) {
                $searchResultDTO->content_type = 'sku';
                array_push($searchResultDTOList, $searchResultDTO);
            }
        }

        $result->setSuccessWithResult($searchResultDTOList);
        return $result;
    }

    public function getStarSearchDTOList(SearchParams $searchParams): ?Result
    {
        $result = new Result();
        $keyword = $searchParams->keyword;
        $searchResultDTOList = [];
        $offset = $searchParams->index * self::DEFAULT_PAGE_SIZE;
        $pageSize = self::DEFAULT_PAGE_SIZE;


        $searchRecordList = ShoppingCartModel::getInstance()->getStarRelatedContent($keyword, $offset, $pageSize);
        if ($searchRecordList != null) {
            /** @var SearchResultDTO $searchResultDTO */
            foreach ($searchRecordList as $searchResultDTO) {
                $searchResultDTO->content_type = 'star';
                array_push($searchResultDTOList, $searchResultDTO);
            }
        }

        $result->setSuccessWithResult($searchResultDTOList);
        return $result;
    }


    /**
     * @param SearchParams $searchParams
     * @return Result|null
     * 获取后台管理端产品页ip列表
     */
    public function getStarAdminDTOlist(SearchParams $searchParams): ?Result
    {

        $result = new Result();
        $keyword = $searchParams->keyword;
        $offset = $searchParams->index * self::DEFAULT_PAGE_SIZE;
        $pageSize = $searchParams->page_size;
        $searchRecordList = ShoppingCartModel::getInstance()->getStarRelatedContent($keyword, $offset, $pageSize);
        $result->setSuccessWithResult($searchRecordList);
        return $result;

    }


    /**
     * @param SearchParams $searchProductSkuParams
     * @return Result|null
     * 获取后台管理端商品列表
     */
    public function getProductSkuAdminList(SearchParams $searchParams): ?Result
    {
        $result = new Result();
        $keyword = $searchParams->keyword;
        $offset = $searchParams->index * self::DEFAULT_PAGE_SIZE;
        $pageSize = $searchParams->page_size;
        $searchRecordList = ShoppingCartModel::getInstance()->getSuperItemRelatedContent($keyword, $offset, $pageSize);
        $result->setSuccessWithResult($searchRecordList);
        return $result;
    }

    public function createEsDoc(WriteEsDocumentParams $writeEsDocumentParams)
    {
        $result = new Result();

        $body = [];
        if (is_array($writeEsDocumentParams->body)) {
            $body = $writeEsDocumentParams->body;
        } elseif (is_object($writeEsDocumentParams->body)) {
            $body = \FUR_Core::buildArray($writeEsDocumentParams->body);
        }
        $boolRet = ElasticSearchClient::insert($writeEsDocumentParams->index, $writeEsDocumentParams->type, $writeEsDocumentParams->id, $body);
        $result->setSuccessWithResult($boolRet);
        return $result;
    }


    public function updateEsDoc(WriteEsDocumentParams $writeEsDocumentParams)
    {
        $result = new Result();

        $doc = [];
        if (is_array($writeEsDocumentParams->body)) {
            $doc = $writeEsDocumentParams->body;
        } elseif (is_object($writeEsDocumentParams->body)) {
            $doc = \FUR_Core::buildArray($writeEsDocumentParams->body);
        }
        $body = ['doc' => $doc];
        $boolRet = ElasticSearchClient::update($writeEsDocumentParams->index, $writeEsDocumentParams->type, $writeEsDocumentParams->id, $body);
        $result->setSuccessWithResult($boolRet);
        return $result;
    }

    public function deleteEsDoc(DeleteEsDocumentParams $deleteEsDocumentParams)
    {
        $result = new Result();
        $boolRet = ElasticSearchClient::delete($deleteEsDocumentParams->index, $deleteEsDocumentParams->type, $deleteEsDocumentParams->id);
        $result->setSuccessWithResult($boolRet);
        return $result;
    }
}