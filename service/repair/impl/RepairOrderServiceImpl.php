<?php

namespace service\repair\impl;

use Config_Error;
use facade\request\repair\CreateRepairOrderParams;
use facade\request\repair\QueryRepairOrderListParams;
use facade\response\Result;
use facade\service\repair\RepairOrderService;
use lib\TimeHelper;
use model\organization\dto\OrganizationStoreDTO;
use model\organization\dto\OrganizationStoreManagerDTO;
use model\repair\dto\RepairOrderExtraInfoDTO;
use model\repair\dto\RepairOrderInfoDTO;
use model\repair\RepairOrderExtraModel;
use model\repair\RepairOrderModel;
use service\organization\impl\OrganizationServiceImpl;

class RepairOrderServiceImpl implements RepairOrderService
{
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function createRepairOrder(CreateRepairOrderParams $createRepairOrderParams): ?Result
    {
        $result = new Result();
        $repairOrderDTO = new RepairOrderInfoDTO();

        $storeId = $createRepairOrderParams->store_id;
        if ($createRepairOrderParams->order_category_id == null) {
            $result->setSuccessWithResult(Config_Error::ERR_REPAIR_ORDER_CATEGORY_IS_NULL);
            return $result;
        }
        if (!is_string($createRepairOrderParams->images)) {
            $createRepairOrderParams->images = json_encode($createRepairOrderParams->images);
        }
        \FUR_Core::copyProperties($createRepairOrderParams, $repairOrderDTO);
        //查店长信息
        $storeManagerInfoRet = OrganizationServiceImpl::getInstance()->queryStoreManagerByPhone($createRepairOrderParams->unique_id);
        if (!Result::isSuccess($storeManagerInfoRet)) {
            $result->setSuccessWithResult(Config_Error::ERR_LOGIN_STORE_MANAGER_NOT_EXIST);
            return $result;
        }
        /** @var OrganizationStoreManagerDTO $storeManagerInfo */
        $storeManagerInfo = $storeManagerInfoRet->data;
        //查店铺信息
        $storeInfoRet = OrganizationServiceImpl::getInstance()->getStoreInfo($storeId);
        if (!Result::isSuccess($storeManagerInfoRet)) {
            $result->setSuccessWithResult(Config_Error::ERR_STORE_NOT_FOUND);
            return $result;
        }


        /** @var OrganizationStoreDTO $storeInfo */
        $storeInfo = $storeInfoRet->data;

        $repairOrderDTO->store_manager_name = $storeManagerInfo->store_manager;
        $repairOrderDTO->store_manager_phone = $storeManagerInfo->contact_info;
        $repairOrderDTO->store_id = $storeId;
        $repairOrderDTO->company_id = $storeInfo->company_id;
        $repairOrderDTO->created_timestamp = TimeHelper::getTimeMs();
        $repairOrderDTO->order_status = \Config_Const::REPAIR_ORDER_STATUS_NEW;
        $repairOrderDTO->cash_out_status = 0;
        $repairOrderDTO->order_version = 0;
        $repairOrderDTO->pay_status = 0;
        $repairOrderDTO->invoicing_status = 0;
        $repairOrderDTO->customer_address = $repairOrderDTO->customer_address ?? $storeInfo->store_address;
        $repairOrderDTO->customer_name = $repairOrderDTO->customer_name ?? $storeManagerInfo->store_manager;
        $repairOrderDTO->customer_phone_num = $repairOrderDTO->customer_phone_num ?? $storeManagerInfo->contact_info;


        $id = RepairOrderModel::getInstance()->insertRecord($repairOrderDTO);
        $result->setSuccessWithResult($id);
        return $result;
    }

    public function queryRepairOrderList(QueryRepairOrderListParams $params): ?Result
    {
        $result = new Result();
        $orderList = RepairOrderModel::getInstance()->queryRepairOrderList($params);
        $result->setSuccessWithResult($orderList);
        return $result;
    }

    public function queryRepairOrderCount(QueryRepairOrderListParams $params): ?Result
    {
        $result = new Result();
        $data = RepairOrderModel::getInstance()->queryRepairOrderCount($params);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function distributeRepairOrder(int $repairOrderId, int $engineerId): ?Result
    {
        $result = new Result();
        $repairOrderInfo = RepairOrderModel::getInstance()->getOrderDetail($repairOrderId);

        if ($repairOrderInfo == null) {
            $result->setError(Config_Error::ERR_REPAIR_ORDER_NOT_EXIST);
            return $result;
        }
        if ($repairOrderInfo->order_status > \Config_Const::REPAIR_ORDER_ENGINEER_ACCEPT) {
            $result->setError(Config_Error::ERR_REPAIR_ORDER_HAS_ENGINEER);
            return $result;
        }

        $repairOrderInfo->engineer_id = $engineerId;
        $repairOrderInfo->updated_timestamp = TimeHelper::getTimeMs();
        $repairOrderInfo->order_status = \Config_Const::REPAIR_ORDER_ENGINEER_ACCEPT;
        $rows = RepairOrderModel::getInstance()->updateRepairInfo($repairOrderInfo);
        $result->setSuccessWithResult($rows);
        return $result;
    }

    public function getRepairOrder(int $repairOrderId): ?Result
    {
        $result = new Result();
        $repairOrderInfo = RepairOrderModel::getInstance()->getOrderDetail($repairOrderId);
        $result->setSuccessWithResult($repairOrderInfo);
        return $result;
    }

    public function updateRepairOrder(RepairOrderInfoDTO $repairOrderInfoDTO): ?Result
    {
        $result = new Result();
        $repairOrderInfoDTO->updated_timestamp = TimeHelper::getTimeMs();
        $rows = RepairOrderModel::getInstance()->updateRepairInfo($repairOrderInfoDTO);
        $result->setSuccessWithResult($rows);
        return $result;
    }

    public function getRepairOrderExtraInfo(int $repairOrderId): ?Result
    {
        $result = new Result();
        $repairOrderExtraInfo = RepairOrderExtraModel::getInstance()->getExtraInfoByOrderId($repairOrderId);
        if ($repairOrderExtraInfo == null) {
            $this->initRepairOrderExtraInfo($repairOrderId);
            $repairOrderExtraInfo = RepairOrderExtraModel::getInstance()->getExtraInfoByOrderId($repairOrderId);
        }
        $result->setSuccessWithResult($repairOrderExtraInfo);
        return $result;
    }

    private function initRepairOrderExtraInfo(int $repairOrderId): ?Result
    {
        $result = new Result();
        $repairOrderExtraInfo = new RepairOrderExtraInfoDTO();
        $repairOrderExtraInfo->repair_order_id = $repairOrderId;
        $repairOrderExtraInfo->created_timestamp = TimeHelper::getTimeMs();
        $id = RepairOrderExtraModel::getInstance()->insertRecord($repairOrderExtraInfo);
        $result->setSuccessWithResult($id);
        return $result;
    }

    public function updateRepairOrderExtraInfo(RepairOrderExtraInfoDTO $repairOrderExtraInfoDTO): ?Result
    {
        $result = new Result();
        $repairOrderExtraInfoDTO->updated_timestamp = TimeHelper::getTimeMs();
        $rows = RepairOrderExtraModel::getInstance()->updateExtraInfo($repairOrderExtraInfoDTO);
        $result->setSuccessWithResult($rows);
        return $result;
    }

    public function queryWithdrawAbleAmount(int $engineerId): ?Result
    {
        $result = new Result();
        $amount = RepairOrderModel::getInstance()->queryWithdrawAbleAmount($engineerId);
        $result->setSuccessWithResult($amount);
        return $result;
    }
}