<?php

namespace service\shoppingCart\impl;


use facade\request\shoppingCart\UpdateCartParams;
use facade\request\shoppingCart\RemoveFromCartParams;
use facade\response\Result;
use facade\service\shoppingCart\ShoppingCartService;
use model\shoppingCart\dto\ShoppingCartDTO;
use model\shoppingCart\ShoppingCartModel;


class ShoppingCartServiceImpl implements ShoppingCartService
{

    const MAX_SHOPPING_CART_SIZE = 128;

    public static $_instance;


    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function updateCart(UpdateCartParams $addToCartParams): ?Result
    {
        $result = new Result();
        $dbRecord = ShoppingCartModel::getInstance()->queryUserRecordByProduct($addToCartParams->user_id, $addToCartParams->item_id, $addToCartParams->sku_id);
        if ($dbRecord) {
            $dbRecord->count = $addToCartParams->count;
            $dbRecord->updated_timestamp = time();
            ShoppingCartModel::getInstance()->updateUserCart($dbRecord);
        } else {
            $userCartCount = ShoppingCartModel::getInstance()->queryUserCartCount($addToCartParams->user_id);
            if ($userCartCount > self::MAX_SHOPPING_CART_SIZE) {
                $result->setError(\Config_Error::ERR_SHOPPING_CART_MAX_SIZE);
                return $result;
            }
            $shoppingCartDTO = new ShoppingCartDTO();
            $shoppingCartDTO->count = $addToCartParams->count;
            $shoppingCartDTO->item_id = $addToCartParams->item_id;
            $shoppingCartDTO->sku_id = $addToCartParams->sku_id;
            $shoppingCartDTO->user_id = $addToCartParams->user_id;
            $shoppingCartDTO->created_timestamp = time();
            $shoppingCartDTO->updated_timestamp = time();
            ShoppingCartModel::getInstance()->insertRecord($shoppingCartDTO);
        }
        $result->setSuccessWithResult(true);
        return $result;

    }

    public function removeFromCart(int $userId, ?array $itemIdList): ?Result
    {
        $result = new Result();
        if ($itemIdList == null) {
            $result->setSuccessWithResult(true);
            return $result;
        }
        foreach ($itemIdList as $itemId) {
            ShoppingCartModel::getInstance()->removeFromUserCart($userId, $itemId, 0);
        }
        $result->setSuccessWithResult(true);
        return $result;
    }

    public function getUserCart(int $userId): ?Result
    {
        $result = new Result();
        $shoppingCartDTOList = ShoppingCartModel::getInstance()->queryUserCart($userId);
        $result->setSuccessWithResult($shoppingCartDTOList);
        return $result;
    }
}