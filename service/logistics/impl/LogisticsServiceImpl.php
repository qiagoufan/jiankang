<?php

namespace service\logistics\impl;

use Config_Error;
use facade\response\logistics\LogisticsDetailResp;
use facade\response\logistics\LogisticsRecordResp;
use facade\response\Result;
use facade\service\logistics\LogisticsService;
use lib\Kuaidi100Helper;
use model\order\dto\OrderInfoDTO;

class LogisticsServiceImpl implements LogisticsService
{

    //快递单当前状态，包括0在途，1揽收，2疑难，3签收，4退签，5派件，6退回，7转单，10待清关，11清关中，12已清关，13清关异常，14收件人拒签等13个状态

    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function getOrderLogisticsDetail(OrderInfoDTO $orderInfoDTO): ?Result
    {
        $expressCompany = $orderInfoDTO->express_company;
        $expressNo = $orderInfoDTO->express_no;
        $result = new Result();
        $kuaidi100Data = Kuaidi100Helper::getLogisticsInfo($expressCompany, $expressNo);

        if ($kuaidi100Data == null) {
            $result->setError(Config_Error::ERR_LOGISTICS_KUAIDI100_QUERY_FAIL);
            return $result;
        }

        $logisticsDetailResp = new LogisticsDetailResp();
        \FUR_Core::copyProperties($orderInfoDTO, $logisticsDetailResp);
        $logisticsDetailResp->express_company_name = \FUR_Config::get_express_config('express_company_list')[$orderInfoDTO->express_company];
        $this->conversionData($kuaidi100Data, $logisticsDetailResp);
        if ($logisticsDetailResp == null) {
            $result->setError(Config_Error::ERR_LOGISTICS_KUAIDI100_DATA_FAIL);
            return $result;
        }
        $result->setSuccessWithResult($logisticsDetailResp);
        return $result;
    }


    private function conversionData(string $data, LogisticsDetailResp &$logisticsDetailResp)
    {

        $jsonDecodeData = json_decode($data);
        if ($jsonDecodeData == null || isset($jsonDecodeData->state) == false) {
            return null;
        }

        $logisticsDetailResp->state = $jsonDecodeData->state;
        $logisticsDetailResp->record_list = [];
        if ($jsonDecodeData->data != null) {
            foreach ($jsonDecodeData->data as $item) {
                $logisticsRecordResp = new LogisticsRecordResp ();
                $logisticsRecordResp->time = $item->time;
                $logisticsRecordResp->ftime = $item->ftime;
                $logisticsRecordResp->context = $item->context;
                array_push($logisticsDetailResp->record_list, $logisticsRecordResp);
            }
        }
    }

    public function getLogisticsStatusStr(?string $expressCompany, ?string $expressNo): ?Result
    {
        $result = new Result();
        $statusStr = "您的包裹已寄出";
        if ($expressCompany == null || $expressNo == null) {
            $result->setSuccessWithResult($statusStr);
            return $result;
        }

        $kuaidi100Data = Kuaidi100Helper::getLogisticsInfo($expressCompany, $expressNo);
        if ($kuaidi100Data == null) {
            $result->setSuccessWithResult($statusStr);
            return $result;
        }
        $logisticsDetailResp = new LogisticsDetailResp();
        $logisticsDetailResp = $this->conversionData($kuaidi100Data, $logisticsDetailResp);
        if ($logisticsDetailResp == null) {
            $result->setSuccessWithResult($statusStr);
            return $result;
        }

        if ($logisticsDetailResp->record_list != null && !empty($logisticsDetailResp->record_list)) {
            /** @var LogisticsRecordResp $latestRecord */
            $latestRecord = $logisticsDetailResp->record_list[0];
            $statusStr = $latestRecord->context;
        }
        $result->setSuccessWithResult($statusStr);
        return $result;
    }

    public function getLogisticsDetail(?string $expressCompany, ?string $expressNo): ?Result
    {
        $result = new Result();

        $logisticsDetailResp = new LogisticsDetailResp();
        $logisticsDetailResp->express_company_name = \FUR_Config::get_express_config('express_company_list')[$expressCompany];
        $logisticsDetailResp->express_no = $expressNo;

        $kuaidi100Data = Kuaidi100Helper::getLogisticsInfo($expressCompany, $expressNo);
        if ($kuaidi100Data == null) {
            $result->setSuccessWithResult($logisticsDetailResp);
            return $result;
        }

        $this->conversionData($kuaidi100Data, $logisticsDetailResp);
        $result->setSuccessWithResult($logisticsDetailResp);
        return $result;
    }
}