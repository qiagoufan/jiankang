<?php

namespace service\category\impl;

use facade\request\category\EditCategoryParams;
use facade\response\Result;
use facade\service\category\CategoryService;
use lib\TimeHelper;
use model\product\ProductCategoryModel;
use model\product\dto\ProductCategoryDTO;

class CategoryServiceImpl implements CategoryService
{
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function createCategory(EditCategoryParams $editCategoryParams): ?Result
    {
        $result = new Result();
        $cateDTO = new ProductCategoryDTO();
        \FUR_Core::copyProperties($editCategoryParams, $cateDTO);
        $cateDTO->created_timestamp = TimeHelper::getTimeMs();
        $cateDTO->category_status = 1;
        $id = ProductCategoryModel::getInstance()->insertCategory($cateDTO);

        $cateDTO->id = $id;
        if ($editCategoryParams->category_level == 1) {
            $cateDTO->cate_level1_id = $id;
        } elseif ($editCategoryParams->category_level == 2) {
            $cateDTO->cate_level2_id = $id;
        } elseif ($editCategoryParams->category_level == 3) {
            $cateDTO->cate_level3_id = $id;
        }
        ProductCategoryModel::getInstance()->updateCategory($cateDTO);
        $result->setSuccessWithResult($id);
        return $result;
    }

    public function updateCategory(EditCategoryParams $editCategoryParams): ?Result
    {
        $result = new Result();
        $cateDTO = ProductCategoryModel::getInstance()->queryCategoryById($editCategoryParams->category_id);
        if ($cateDTO->category_level == 1 && $editCategoryParams->parent_id != 0) {
            $result->setError(\Config_Error::ERR_PRODUCT_EDIT_CATEGORY_LEVEL1_ERROR);
            return $result;
        }
        if ($cateDTO->category_level == 2 && $editCategoryParams->parent_id == 0) {
            $result->setError(\Config_Error::ERR_PRODUCT_EDIT_CATEGORY_LEVEL2_ERROR);
            return $result;
        }
        $cateDTO->category_name = $editCategoryParams->category_name;
        $cateDTO->seq = $editCategoryParams->seq;
        $cateDTO->updated_timestamp = time();
        if ($editCategoryParams->category_cover) {
            $cateDTO->category_cover = $editCategoryParams->category_cover;
        }
        $cateDTO->category_status = 1;

        if ($cateDTO->category_level == 1) {
            $cateDTO->cate_level1_name = $editCategoryParams->category_name;
        } elseif ($cateDTO->category_level == 2) {
            $cateDTO->parent_id = $editCategoryParams->parent_id;
            $cateDTO->cate_level1_id = $editCategoryParams->parent_id;
            $cateDTO->cate_level2_name = $editCategoryParams->category_name;
            \FUR_Log::info($cateDTO);
        } elseif ($cateDTO->category_level == 3) {
            $cateDTO->cate_level3_name = $editCategoryParams->category_name;
        }
        $data = ProductCategoryModel::getInstance()->updateCategory($cateDTO);
        ProductCategoryModel::getInstance()->updateCategoryParentName($editCategoryParams->category_id, $editCategoryParams->category_name, $cateDTO->category_level);


        $result->setSuccessWithResult($data);
        return $result;
    }

    public function queryCategoryByNameAndLevel(string $categoryName, int $level): ?Result
    {
        $result = new Result();
        $categoryInfo = ProductCategoryModel::getInstance()->queryProductCateByNameAndLevel($categoryName, $level);
        $result->setSuccessWithResult($categoryInfo);
        return $result;
    }

    public function queryCategoryList(): ?Result
    {
        $result = new Result();
        $categoryList = ProductCategoryModel::getInstance()->queryCategoryList();
        $result->setSuccessWithResult($categoryList);
        return $result;
    }

    public function queryCategoryListByParentId($parentId): ?Result
    {
        $result = new Result();
        $categoryList = ProductCategoryModel::getInstance()->queryCategoryListByParentId($parentId);
        $result->setSuccessWithResult($categoryList);
        return $result;
    }

    public function queryCategoryById($cateId): ?Result
    {
        $result = new Result();
        $categoryInfo = ProductCategoryModel::getInstance()->queryCategoryById($cateId);
        $result->setSuccessWithResult($categoryInfo);
        return $result;
    }

    public function deleteCategoryById($cateId): ?Result
    {
        $result = new Result();
        $data = ProductCategoryModel::getInstance()->deleteCategoryById($cateId);
        $result->setSuccessWithResult($data);
        return $result;
    }
}