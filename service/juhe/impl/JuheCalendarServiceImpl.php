<?php


namespace service\juhe\impl;


use facade\response\juhe\JuheCalendarResp;
use facade\response\Result;
use facade\service\juhe\JuheCalendarService;

class JuheCalendarServiceImpl implements JuheCalendarService
{

    const JUHE_APP_KEY = '70eae8973bbe2783e0be5413aeba9108';


    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function getDateInfo(string $date): ?Result
    {
        $result = new Result();
        $timestamp = strtotime($date);

        $queryDate = date('Y-n-j', $timestamp);
        $url = 'http://v.juhe.cn/calendar/day';

        $queryParams['date'] = $queryDate;
        $queryParams['key'] = self::JUHE_APP_KEY;
        $url .= '?' . http_build_query($queryParams);
        $data = \FUR_Curl::get($url);

        $calendarRet = json_decode($data);
        if ($calendarRet == null || $calendarRet->error_code != 0) {
            $result->setError(\Config_Error::ERR_JUHE_API_FAIL);
            return $result;
        }
        $calendarData = $calendarRet->result->data;
        $calendarResp = new  JuheCalendarResp();
        if (isset($calendarData->holiday)) {
            $calendarResp->holiday = $calendarData->holiday;
        }
        $calendarResp->animals_year = $calendarData->animalsYear;
        $calendarResp->avoid = $calendarData->avoid;
        $calendarResp->lunar = $calendarData->lunar;
        $calendarResp->lunar_year = $calendarData->lunarYear;
        $calendarResp->suit = $calendarData->suit;

        $result->setSuccessWithResult($calendarResp);
        return $result;

    }
}