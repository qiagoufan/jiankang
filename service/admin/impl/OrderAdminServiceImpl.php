<?php

namespace service\admin\impl;


use facade\request\order\QueryOrderListParams;
use facade\response\Result;
use model\order\OrderModel;
use \facade\request\admin\QueryAllOrderListParams;

class OrderAdminServiceImpl
{


    public static $_instance;


    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    /**
     * @param int $order_type
     * @return Result|null
     * 获取订单列表
     */
    public function getOrderList(QueryAllOrderListParams $params): ?Result
    {
        $result = new Result();
        $orderModel = OrderModel::getInstance();
        $orderList = $orderModel->queryAllOrderList($params);
        $result->setSuccessWithResult($orderList);
        return $result;
    }


    /**
     * @param int $orderId
     * @return Result|null
     * 更新发货
     */
    public function deliveryOrder(string $orderSn, string $expressCompany, string $expressNo): ?Result
    {
        $result = new Result();
        $orderModel = OrderModel::getInstance();
        $orderList = $orderModel->deliverOrder($orderSn, $expressCompany, $expressNo);
        $result->setSuccessWithResult($orderList);
        return $result;
    }


    public function getOrderProductByOrderId(int $orderId): ?Result
    {
        $result = new Result();
        $orderModel = OrderModel::getInstance();
        $orderList = $orderModel->queryOrderProductByOrderId($orderId);
        $result->setSuccessWithResult($orderList);
        return $result;
    }


    /**
     * @return Result|null
     * 获取订单列表数量
     */
    public function queryOrderListCount(QueryAllOrderListParams $queryOrderListParams): ?Result
    {
        $result = new Result();
        $orderModel = OrderModel::getInstance();
        $orderList = $orderModel->queryOrderListCount($queryOrderListParams);
        $result->setSuccessWithResult($orderList);
        return $result;
    }


    public function queryOrderDataExportList(string $startTime, string $endTime): ?Result
    {

        $result = new Result();
        $orderModel = OrderModel::getInstance();
        $orderList = $orderModel->queryOrderExportList($startTime, $endTime);
        $result->setSuccessWithResult($orderList);
        return $result;

    }

}