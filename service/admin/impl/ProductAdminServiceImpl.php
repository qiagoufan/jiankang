<?php

namespace service\admin\impl;

use facade\request\admin\EditProductItemParams;
use facade\request\product\GetProductItemListParams;
use facade\request\product\QueryAdminProductItemListParams;
use facade\response\Result;
use facade\service\admin\ProductAdminService;
use model\product\dto\ProductCategoryDTO;
use model\product\dto\ProductExtraInfoDTO;
use model\product\dto\ProductItemDTO;
use model\product\dto\ProductSkuDTO;
use model\product\ProductExtraInfoModel;
use model\product\ProductItemModel;
use model\product\ProductSkuModel;
use service\category\impl\CategoryServiceImpl;

class ProductAdminServiceImpl
{


    public static $_instance;


    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function createProductItem(EditProductItemParams $editProductItemParams): ?Result
    {
        $result = new Result();

        $editProductItemParams->current_price = $editProductItemParams->current_price * 100;
        $editProductItemParams->regular_price = $editProductItemParams->regular_price * 100;


        $categoryInfoRet = CategoryServiceImpl::getInstance()->queryCategoryById($editProductItemParams->category_id);
        /** @var ProductCategoryDTO $categoryInfo */
        $categoryInfo = $categoryInfoRet->data;
        $level1Id = $categoryInfo->cate_level1_id;
        $level2Id = $categoryInfo->cate_level2_id;

        $productItemDTO = new ProductItemDTO();
        \FUR_Core::copyProperties($editProductItemParams, $productItemDTO);
        $productItemDTO->created_timestamp = time();
        $productItemDTO->product_desc = html_entity_decode($productItemDTO->product_desc);
        $productItemDTO->publish_status = 1;
        $productItemDTO->product_status = 1;
        $productItemDTO->gallery_image = json_encode($editProductItemParams->gallery_image);
        $productItemDTO->published_timestamp = time();
        $productItemDTO->sale = 0;
        $productItemDTO->shipping_price = 0;
        $productItemDTO->cate_level_1 = $level1Id;
        $productItemDTO->cate_level_2 = $level2Id;

        $productItemModel = ProductItemModel::getInstance();
        $itemId = $productItemModel->insertProductItem($productItemDTO);
        $productItemDTO->id = $itemId;
        //再插入一个默认sku

        $skuDTO = new ProductSkuDTO();
        $skuDTO->item_id = $itemId;
        $skuDTO->sku_name = '默认';
        $skuDTO->stock = $editProductItemParams->stock;
        $skuDTO->sale = 0;
        $skuDTO->current_price = $editProductItemParams->current_price;
        $skuDTO->regular_price = $editProductItemParams->regular_price;
        $skuDTO->regular_price = $editProductItemParams->regular_price;
        $skuDTO->shipping_price = 0;
        $skuDTO->cate_level_1 = $level1Id;
        $skuDTO->cate_level_2 = $level2Id;
        $skuDTO->publish_status = 1;
        $skuDTO->is_delete = 0;
        $skuDTO->published_timestamp = time();
        $skuDTO->created_timestamp = time();


        $skuId = ProductSkuModel::getInstance()->insertRecord($skuDTO);


        //更新默认sku
        $productItemDTO->default_sku_id = $skuId;
        ProductItemModel::getInstance()->updateProductItem($productItemDTO);

        $result->setSuccessWithResult($itemId);
        return $result;
    }


    public function queryProductItemList(QueryAdminProductItemListParams $params): ?Result
    {
        $result = new Result();
        $productSkuModel = ProductItemModel::getInstance();
        $listRet = $productSkuModel->queryProductItemList($params);
        $result->setSuccessWithResult($listRet);
        return $result;
    }


    public function queryItemCount(QueryAdminProductItemListParams $params): ?Result
    {
        $result = new Result();
        $productItemModel = ProductItemModel::getInstance();
        $itemCount = $productItemModel->queryProductItemCount($params);
        $result->setSuccessWithResult($itemCount);
        return $result;
    }


    public function getProductItemDetailById(int $itemId): ?Result
    {
        $result = new Result();
        $productItemModel = ProductItemModel::getInstance();
        $productItemRet = $productItemModel->getProductItem($itemId);
        $result->setSuccessWithResult($productItemRet);
        return $result;
    }


    public function updateProductInfo(EditProductItemParams $editProductItemParams): ?Result
    {
        $result = new Result();
        $productItemDTO = ProductItemModel::getInstance()->getProductItem($editProductItemParams->item_id);
        $editProductItemParams->current_price = $editProductItemParams->current_price * 100;
        $editProductItemParams->regular_price = $editProductItemParams->regular_price * 100;


        $categoryInfoRet = CategoryServiceImpl::getInstance()->queryCategoryById($editProductItemParams->category_id);
        /** @var ProductCategoryDTO $categoryInfo */
        $categoryInfo = $categoryInfoRet->data;
        $level1Id = $categoryInfo->cate_level1_id;
        $level2Id = $categoryInfo->cate_level2_id;

        if (empty($editProductItemParams->gallery_image) && $productItemDTO->gallery_image == null) {
            $result->setError(\Config_Error::ERR_PRODUCT_GALLEY_IMAGE_IS_NULL);
            return $result;
        }
        \FUR_Core::copyProperties($editProductItemParams, $productItemDTO);
        $productItemDTO->created_timestamp = time();
        $productItemDTO->product_desc = html_entity_decode($productItemDTO->product_desc);
        $productItemDTO->publish_status = 1;

        if ($editProductItemParams->gallery_image != null && !empty($editProductItemParams->gallery_image)) {
            $productItemDTO->gallery_image = json_encode($editProductItemParams->gallery_image);
        }
        $productItemDTO->published_timestamp = time();
        $productItemDTO->sale = 0;
        $productItemDTO->shipping_price = 0;
        $productItemDTO->regular_price = $editProductItemParams->regular_price;
        $productItemDTO->current_price = $editProductItemParams->current_price;
        $productItemDTO->cate_level_1 = $level1Id;
        $productItemDTO->cate_level_2 = $level2Id;

        $productItemModel = ProductItemModel::getInstance();
        $itemId = $productItemModel->updateProductItem($productItemDTO);


        $productSkuDTO = ProductSkuModel::getInstance()->querySkuDetailFromDB($productItemDTO->default_sku_id);

        $productSkuDTO->current_price = $editProductItemParams->current_price;
        $productSkuDTO->regular_price = $editProductItemParams->regular_price;
        $productSkuDTO->cate_level_1 = $level1Id;
        $productSkuDTO->cate_level_2 = $level2Id;
        ProductSkuModel::getInstance()->updateProductSkuById($productSkuDTO);

        $result->setSuccessWithResult($itemId);
        return $result;
    }


    public function updateProductItemPublishStatus(int $itemId, int $status): ?Result
    {
        $result = new Result();
        $rows = ProductItemModel::getInstance()->updateItemPublishStatus($itemId, $status);
        $result->setSuccessWithResult($rows);
        return $result;

    }


    public function updateProductStock(int $itemId, ?int $stock = null, ?int $sale = null): ?Result
    {
        $result = new Result();
        $productItemDTO = ProductItemModel::getInstance()->getProductItem($itemId);
        if ($stock != null) {
            $productItemDTO->stock = $stock;
        }
        if ($sale != null) {
            $productItemDTO->sale = $sale;
        }
        $productItemDTO->updated_timestamp = time();
        $rows = ProductItemModel::getInstance()->updateItemStockSale($productItemDTO);
        $result->setSuccessWithResult($rows);
        return $result;
    }

    public function deleteProduct(int $itemId): ?Result
    {
        $result = new Result();
        ProductItemModel::getInstance()->deleteProductItem($itemId);
        return $result;
    }
}