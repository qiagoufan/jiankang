<?php

namespace service\admin\impl;

use facade\request\user\QueryUserListParams;
use facade\response\Result;
use model\user\UserModel;


class UserAdminServiceImpl
{
    public static $_instance;


    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function getUserList(QueryUserListParams $params): ?Result
    {
        $result = new Result();
        $userModel = UserModel::getInstance();
        $userList = $userModel->queryUserList($params);
        $result->setSuccessWithResult($userList);
        return $result;
    }


    public function getUserTotalCount(QueryUserListParams $params): ?Result
    {
        $result = new Result();
        $userModel = UserModel::getInstance();
        $data = $userModel->queryUserTotalCount($params);
        $result->setSuccessWithResult($data);
        return $result;

    }


    public function deleteUser(int $userId): ?Result
    {
        $result = new Result();
        $data = UserModel::getInstance()->deleteUserById($userId);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function queryUserInfo(int $userId): ?Result
    {
        $result = new Result();
        $data = UserModel::getInstance()->getUserInfo($userId);
        $result->setSuccessWithResult($data);
        return $result;
    }

}