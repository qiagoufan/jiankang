<?php


namespace service\admin\impl;

use facade\response\Result;
use facade\service\admin\AdminAccountService;
use model\admin\AdminAccountModel;

class AdminAccountServiceImpl implements AdminAccountService
{

    public static $_instance;


    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function queryAdminByAccount(string $account): ?Result
    {
        // TODO: Implement queryAdminByAccount() method.
    }


    public function queryAdminById(int $adminAccountId): ?Result
    {
        $result = new Result();
        $adminAccountDTO = AdminAccountModel::getInstance()->queryByAdminAccountId($adminAccountId);
        $result->setSuccessWithResult($adminAccountDTO);
        return $result;
    }

    public function queryAdminByPhone(string $phone): ?Result
    {
        $result = new Result();
        $adminAccountDTO = AdminAccountModel::getInstance()->queryByPhone($phone);
        $result->setSuccessWithResult($adminAccountDTO);
        return $result;
    }


    public function adminAccountLogin(string $account, string $password): Result
    {
        $result = new Result();
        $data = AdminAccountModel::getInstance()->queryByAccount($account);

        if ($data == null) {
            $result->setError(\Config_Error::ERR_ADMIN_USER_IS_NOT_EXIST);
            return $result;
        }
        if (md5($password . \Config_Const::ADMIN_ACCOUNT_SALT) != $data->password) {
            $result->setError(\Config_Error::ERR_ADMIN_ACCOUNT_PASSWORD_IS_ERROR);
            return $result;
        }
        $result->setSuccessWithResult($data->id);
        return $result;
    }
}