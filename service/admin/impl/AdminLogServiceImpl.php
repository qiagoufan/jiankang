<?php


namespace service\admin\impl;

use facade\service\admin\AdminLogService;
use model\dto\AdminLogDTO;
use model\admin\AdminLogModel;
use facade\response\Result;

class AdminLogServiceImpl implements AdminLogService
{

    public static $_instance;


    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function insertAdminLog(AdminLogDTO $adminLogDTO)
    {
        $result = new Result();
        $adminLogModel = AdminLogModel::getInstance();
        $insertId = $adminLogModel->insertRecord($adminLogDTO);
        $result->setSuccessWithResult($insertId);
        return $result;

    }

}