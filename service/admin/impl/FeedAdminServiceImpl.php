<?php


namespace service\admin\impl;

use facade\service\admin\FeedAdminService;
use facade\response\Result;
use model\dto\FeedInfoDTO;
use model\FearturedFeedModel;
use model\FeedModel;
use facade\request\admin\GetAllFeedListParams;
use model\FeedTopModel;
use model\dto\FeedTopDTO;
use model\dto\FeaturedFeedDTO;

class FeedAdminServiceImpl implements FeedAdminService
{

    public static $_instance;

    const CACHE_TIME = 60;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    /**
     * @param GetAllFeedListParams $getAllFeedListParams
     * @return Result|null
     * 获取所有fee列表
     */
    public function getAllFeedList(GetAllFeedListParams $getAllFeedListParams): ?Result
    {
        $result = new Result();
        $feedModel = FeedModel::getInstance();
        $listRet = $feedModel->queryAllFeedList($getAllFeedListParams);
        $result->setSuccessWithResult($listRet);
        return $result;

    }


    /**
     * @param FeedInfoDTO $feedInfoDTO
     * @return Result|null
     * 更新feed状态
     */
    public function updateFeedStatus(FeedInfoDTO $feedInfoDTO): ?Result
    {
        $result = new Result();
        $feedModel = FeedModel::getInstance();
        $deleteRet = $feedModel->updateFeedStatus($feedInfoDTO);
        $result->setSuccessWithResult($deleteRet);
        return $result;
    }


    /**
     * @return Result|null
     * 获取feed列表总数
     */
    public function queryFeedListCount(GetAllFeedListParams $getAllFeedListParams): ?Result
    {
        $result = new Result();
        $feedModel = FeedModel::getInstance();
        $totalCount = $feedModel->queryFeedListCount($getAllFeedListParams);
        $result->setSuccessWithResult($totalCount);
        return $result;
    }


    /**
     * @param FeedTopDTO $feedTopDTO
     * @return Result|null
     * 创建feed置顶信息
     */
    public function createFeedTopInfo(FeedTopDTO $feedTopDTO): ?Result
    {
        $result = new Result();
        $feedTopModel = FeedTopModel::getInstance();
        $totalCount = $feedTopModel->insertRecord($feedTopDTO);
        $result->setSuccessWithResult($totalCount);
        return $result;

    }


    /**
     * @param FeedTopDTO $feedTopDTO
     * @return Result|null
     * 更新feed置顶状态
     */
    public function updateFeedTopStatus(FeedTopDTO $feedTopDTO): ?Result
    {
        $result = new Result();
        $feedTopModel = FeedTopModel::getInstance();
        $bool = $feedTopModel->updateFeedTopStatus($feedTopDTO);
        $result->setSuccessWithResult($bool);
        return $result;
    }


    /**
     * @param int $feedId
     * @return Result|null
     * 根据id获取置顶详情
     */
    public function queryFeedTopDetail(int $feedId): ?Result
    {
        $result = new Result();
        $feedTopModel = FeedTopModel::getInstance();
        $feedTopModel = $feedTopModel->queryFeedTopDetail($feedId);
        $result->setSuccessWithResult($feedTopModel);
        return $result;
    }


    /**
     * @param FeaturedFeedDTO $featuredFeedDTO
     * @return Result|null
     * 给feed加精选
     */
    public function addFeaturedFeed(FeaturedFeedDTO $featuredFeedDTO): ?Result
    {
        $result = new Result();
        $featuredFeedModel = FearturedFeedModel::getInstance();
        $existRecord = $featuredFeedModel->queryRecord($featuredFeedDTO->feed_id);

        if (!$existRecord) {
            $insertRet = $featuredFeedModel->insertRecord($featuredFeedDTO);
            $result->setSuccessWithResult($insertRet);
        }

        return $result;
    }


    /**
     * @param int $feedId
     * @return bool|null
     * 取消给feed加精选
     */
    public function deleteFeaturedFeed(int $feedId): ?Result
    {
        $result = new Result();
        $featuredFeedModel = FearturedFeedModel::getInstance();
        $deleteRet = $featuredFeedModel->deleteRecord($feedId);
        $result->setSuccessWithResult($deleteRet);
        return $result;

    }


    public function queryFeaturedFeedId(): ?Result
    {
        $result = new Result();
        $featuredFeedModel = FearturedFeedModel::getInstance();
        $feedIdArray = $featuredFeedModel->queryFeaturedFeedId();

        $result->setSuccessWithResult($feedIdArray);
        return $result;
    }


    public function getFeedDetail(int $feedId): ?Result
    {
        $result = new Result();
        $feedModel = FeedModel::getInstance();
        $feedInfoDTO = $feedModel->queryFeedDetail($feedId);
        if (!$feedInfoDTO) {
            $result->setError(\Config_Error::ERR_FEED_NOT_EXIST);
            return $result;
        }

        $result->setSuccessWithResult($feedInfoDTO);
        return $result;

    }
}