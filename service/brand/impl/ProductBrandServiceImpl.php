<?php

namespace service\brand\impl;

use facade\service\brand\ProductBrandService;
use facade\response\Result;
use model\ProductSkuBrandModel;

class ProductBrandServiceImpl implements ProductBrandService
{
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function getBrandInfo(int $brandId): Result
    {

        $result = new Result();
        $indexBannerModel = ProductSkuBrandModel::getInstance();
        $bannerList = $indexBannerModel->queryBrandInfo($brandId);
        $result->setSuccessWithResult($bannerList);
        return $result;

    }


}