<?php

namespace service\withdraw\impl;

use facade\request\withdraw\QueryWithdrawListParams;
use facade\response\Result;
use facade\service\withdraw\WithdrawService;
use lib\TimeHelper;
use model\withdraw\dto\WithdrawRecordDTO;
use model\withdraw\WithdrawModel;

class WithdrawServiceImpl implements WithdrawService
{
    public static $_instance;


    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function createWithDrawRecord(WithdrawRecordDTO $withdrawRecordDTO)
    {
        $result = new Result();
        $withdrawRecordDTO->created_timestamp = TimeHelper::getTimeMs();
        $data = WithdrawModel::getInstance()->insertRecord($withdrawRecordDTO);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function updateWithDrawRecord(WithdrawRecordDTO $withdrawRecordDTO)
    {
        $result = new Result();
        $withdrawRecordDTO->updated_timestamp = TimeHelper::getTimeMs();
        $data = WithdrawModel::getInstance()->updateWithdrawRecord($withdrawRecordDTO);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function queryRecordById(int $withdrawId)
    {
        $result = new Result();
        $data = WithdrawModel::getInstance()->queryRecordById($withdrawId);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function queryEngineerWithdrawRecordList(int $engineerId, int $index, int $pageSize)
    {
        $result = new Result();
        $data = WithdrawModel::getInstance()->queryEngineerWithdrawRecordList($engineerId, $index, $pageSize);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function queryWithdrawRecordList(QueryWithdrawListParams $params)
    {
        $result = new Result();
        $data = WithdrawModel::getInstance()->queryWithdrawRecordList($params);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function queryWithdrawRecordCount(QueryWithdrawListParams $params)
    {
        $result = new Result();
        $data = WithdrawModel::getInstance()->queryWithdrawRecordCount($params);
        $result->setSuccessWithResult($data);
        return $result;
    }

}
