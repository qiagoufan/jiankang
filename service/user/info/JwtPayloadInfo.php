<?php

namespace service\user\info;


use lib\TimeHelper;
use model\user\dto\UserDTO;
use model\user\dto\UserTokenDTO;

class JwtPayloadInfo
{

    public $iss;
    public $aud;
    public $iat;
    public $nbf;
    public $sub;
    public $exp;
    public $jti;

    public $unique_id;
    public $login_type;

    public static function buildJwtPayloadInfo(UserDTO $userDTO): JwtPayloadInfo
    {
        $payload = new  JwtPayloadInfo();
        $payload->user_id = $userDTO->id;
        $payload->iss = \FUR_Config::get_common('site');
        $payload->aud = \FUR_Config::get_common('site');
        $payload->iat = time();
        $payload->nbf = time();
        $payload->exp = time() + \Config_Const::JWT_TOKEN_EXPIRE_TIME;

        return $payload;
    }

    public static function buildJwtPayloadInfoV2(string $uniqueId, int $loginType): JwtPayloadInfo
    {
        $now = time();
        $payload = new  JwtPayloadInfo();
        $payload->unique_id = $uniqueId;
        $payload->login_type = $loginType;
        $payload->iss = \FUR_Config::get_common('site');
        $payload->aud = \FUR_Config::get_common('site');
        $payload->iat = $now;
        $payload->nbf = $now;
        $payload->exp = $now + \Config_Const::JWT_TOKEN_EXPIRE_TIME;
        return $payload;
    }
}