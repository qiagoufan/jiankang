<?php


namespace service\user\impl;

use facade\request\user\CreateStarParams;
use facade\request\user\UserCollectParams;
use facade\request\user\QueryUserCollectListParams;
use facade\request\user\UpdateStarParams;
use facade\response\Result;
use facade\response\user\collect\CollectDetailResp;
use facade\service\user\CollectService;
use facade\service\user\StarService;
use model\dto\UserCollectDTO;
use model\dto\UserStarDTO;
use model\UserCollectModel;
use model\UserStarModel;

class CollectServiceImpl implements CollectService
{

    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function saveUserCollect(UserCollectParams $collectParams): Result
    {
        $result = new Result();
        if ($collectParams->collect_biz_id == 0) {
            $result->setError(\Config_Error::ERR_COLLECT_BIZ_ID_NOT_EXIST);
            return $result;
        }

        $userCollectModel = UserCollectModel::getInstance();

        $userCollectDTO = new UserCollectDTO();
        \FUR_Core::copyProperties($collectParams, $userCollectDTO);
        $hasCollect = $userCollectModel->hasCollect($userCollectDTO);
        if ($hasCollect) {
            $result->setSuccessWithResult(1);
            return $result;
        }
        $userCollectDTO->created_timestamp = time();
        
        $userCollectModel->saveRecordCache($userCollectDTO);
        try {
            $id = $userCollectModel->insertRecord($userCollectDTO);
        } catch (\Exception $e) {
            $result->setSuccessWithResult(0);
            return $result;
        }
        $userCollectDTO->id = $id;

        $userCollectModel->updateCollectCount($collectParams->collect_biz_id, $collectParams->collect_biz_type, 1);

        $result->setSuccessWithResult($id);
        return $result;

    }

    public function deleteUserCollect(UserCollectParams $collectParams): Result
    {
        $result = new Result();
        $userCollectModel = UserCollectModel::getInstance();
        $userCollectDTO = $userCollectModel->queryCollectById($collectParams->user_id, $collectParams->collect_biz_id, $collectParams->collect_biz_type);

        /**
         * 可能删了db 没删掉缓存,这样直接操作也不会出意外,就放在鉴权前面了
         */
        $userCollectModel->removeFromCache($userCollectDTO);
        if ($userCollectDTO == null) {
            $result->setSuccessWithResult(false);
            return $result;
        }

        $userCollectModel->deleteRecord($collectParams->user_id, $collectParams->collect_biz_id, $collectParams->collect_biz_type);
        $userCollectModel->updateCollectCount($collectParams->collect_biz_id, $collectParams->collect_biz_type, -1);
        $result->setSuccessWithResult(true);
        return $result;
    }


    public function queryUserCollectList(QueryUserCollectListParams $queryUserCollectListParams): Result
    {
        $result = new Result();
        $index = $queryUserCollectListParams->index;
        $pageSize = $queryUserCollectListParams->page_size;
        $userId = $queryUserCollectListParams->user_id;

        $offset = $index * $pageSize;

        $userCollectModel = UserCollectModel::getInstance();
        $userCollectList = $userCollectModel->queryCollectList($userId, $offset, $pageSize);


        $result->setSuccessWithResult($userCollectList);
        return $result;
    }

    public function isCollect(UserCollectParams $collectParams): Result
    {
        $result = new Result();
        $userCollectModel = UserCollectModel::getInstance();
        $userCollectDTO = new UserCollectDTO();
        $userCollectDTO->collect_biz_id = $collectParams->collect_biz_id;
        $userCollectDTO->collect_biz_type = $collectParams->collect_biz_type;
        $userCollectDTO->user_id = (int)$collectParams->user_id;
        $ret = $userCollectModel->hasCollect($userCollectDTO);
        $result->setSuccessWithResult($ret);
        return $result;
    }

    public function getCollectDetail(UserCollectParams $collectParams): Result
    {
        $result = new Result();
        $userCollectModel = UserCollectModel::getInstance();
        $userCollectDTO = new UserCollectDTO();
        $userCollectDTO->collect_biz_id = $collectParams->collect_biz_id;
        $userCollectDTO->collect_biz_type = $collectParams->collect_biz_type;
        $userCollectDTO->user_id = $collectParams->user_id;
        $ret = $userCollectModel->hasCollect($userCollectDTO);

        $collectDetailResp = new CollectDetailResp();
        $collectDetailResp->collect_status = (int)$ret;
        $collectDetailResp->collect_count = $userCollectModel->getCollectCount($collectParams->collect_biz_id, $collectParams->collect_biz_type);

        $result->setSuccessWithResult($collectDetailResp);
        return $result;
    }
}