<?php

namespace service\user\impl;

use facade\request\user\ShippingAddressParams;
use facade\response\Result;
use facade\service\user\ShippingAddressService;
use model\user\dto\UserShippingAddressDTO;
use model\user\UserShippingAddressModel;

class ShippingAddressServiceImpl implements ShippingAddressService
{
    public static $_instance;


    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function getUserShippingAddressList(int $userId): ?Result
    {
        $result = new Result();
        $shippingAddressModel = UserShippingAddressModel::getInstance();
        $shippingAddressDTOList = $shippingAddressModel->queryShippingAddressList($userId);

        $result->setSuccessWithResult($shippingAddressDTOList);
        return $result;
    }


    public function getShippingAddressDetail(int $userId, int $addressId): ?Result
    {
        $result = new Result();
        $shippingAddressModel = UserShippingAddressModel::getInstance();
        $shippingAddressDTO = $shippingAddressModel->queryShippingAddressDetail($userId, $addressId);
        $result->setSuccessWithResult($shippingAddressDTO);
        return $result;
    }

    public function createShippingAddress(ShippingAddressParams $shippingAddressParams): ?Result
    {
        $result = new Result();
        $shippingAddressDTO = new UserShippingAddressDTO();
        \FUR_Core::copyProperties($shippingAddressParams, $shippingAddressDTO);


        $shippingAddressModel = UserShippingAddressModel::getInstance();

        /**
         * 第一个收货地址,需要改为默认的
         */
        $shippingAddressDTOList = $shippingAddressModel->queryShippingAddressList($shippingAddressParams->user_id);
        if ($shippingAddressDTOList == null || empty($shippingAddressDTOList)) {
            $shippingAddressDTO->is_default = 1;
        }
        if ($shippingAddressDTO->is_default) {
            $shippingAddressModel->updateUserShippingAddressToNotDefault($shippingAddressParams->user_id);
        }

        $shippingAddressDTO->created_timestamp = time();
        $id = $shippingAddressModel->insertRecord($shippingAddressDTO);

        $result->setSuccessWithResult($id);

        return $result;

    }

    public function updateShippingAddress(ShippingAddressParams $shippingAddressParams): ?Result
    {
        $result = new Result();
        $shippingAddressDTO = new UserShippingAddressDTO();
        \FUR_Core::copyProperties($shippingAddressParams, $shippingAddressDTO);
        $shippingAddressDTO->id = $shippingAddressParams->shipping_id;

        $shippingAddressModel = UserShippingAddressModel::getInstance();
        if ($shippingAddressDTO->is_default) {
            $shippingAddressModel->updateUserShippingAddressToNotDefault($shippingAddressParams->user_id);
        }

        $shippingAddressDTO->updated_timestamp = time();
        $id = $shippingAddressModel->updateRecord($shippingAddressDTO);

        $result->setSuccessWithResult($id);

        return $result;
    }

    public function deleteShippingAddress(int $userId, int $addressId): ?Result
    {
        $result = new Result();
        $shippingAddressDTO = new UserShippingAddressDTO();
        \FUR_Core::copyProperties($shippingAddressParams, $shippingAddressDTO);

        $shippingAddressModel = UserShippingAddressModel::getInstance();

        $row = $shippingAddressModel->deleteRecord($userId, $addressId);

        $result->setSuccessWithResult(true);

        return $result;
    }


    public function getUserDefaultAddress(int $uerId): ?Result
    {
        $result = new Result();
        $shippingAddressModel = UserShippingAddressModel::getInstance();
        $address = $shippingAddressModel->queryUserDefaultAddress($uerId);
        $result->setSuccessWithResult($address);
        return $result;
    }
}