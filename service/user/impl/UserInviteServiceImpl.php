<?php

namespace service\user\impl;

use facade\request\PageParams;
use facade\response\Result;
use model\user\dto\UserInviteRecordDTO;
use model\user\UserInviteModel;

class UserInviteServiceImpl
{
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function queryUserInviteRecord(PageParams $params): ?Result
    {

        $result = new Result();
        $data = UserInviteModel::getInstance()->queryUserInviteRecord($params);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function queryUserInviteCount($userId): ?Result
    {
        $result = new Result();
        $data = UserInviteModel::getInstance()->queryUserInviteCount($userId);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function insertUserInviteRecord(UserInviteRecordDTO $userInviteRecordDTO): ?Result
    {

        $result = new Result();
        $userInviteRecordDTO->created_timestamp = time();
        $data = UserInviteModel::getInstance()->insertRecord($userInviteRecordDTO);
        $result->setSuccessWithResult($data);
        return $result;
    }
}