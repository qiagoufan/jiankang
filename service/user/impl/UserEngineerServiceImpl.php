<?php

namespace service\user\impl;

use facade\request\engineer\QueryEngineerListParams;
use facade\response\Result;
use facade\service\user\UserEngineerService;
use lib\TimeHelper;
use model\user\dto\UserEngineerDTO;
use model\user\UserEngineerModel;

class UserEngineerServiceImpl implements UserEngineerService
{
    const COOL_DOWN_UPDATE_NAME_TIME = 86400;
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function queryEngineerInfoByPhone(?string $phoneNum): Result
    {
        $result = new  Result();
        $engineerInfo = UserEngineerModel::getInstance()->queryEngineerInfoByPhone($phoneNum);
        $result->setSuccessWithResult($engineerInfo);
        return $result;
    }

    public function getEngineerInfo(?int $engineerId): Result
    {
        $result = new   Result();
        $engineerInfo = UserEngineerModel::getInstance()->getEngineerInfo($engineerId);
        $result->setSuccessWithResult($engineerInfo);
        return $result;
    }

    public function registerEngineer(?string $phoneNum): Result
    {
        $now = TimeHelper::getTimeMs();
        $result = new   Result();
        $engineerDTO = new UserEngineerDTO();
        $engineerDTO->phone_num = $phoneNum;
        $engineerDTO->engineer_type = \Config_Const::ENGINEER_TYPE_NORMAL;
        $engineerDTO->created_timestamp = $now;
        $engineerDTO->status = 1;


        $engineerInfo = UserEngineerModel::getInstance()->insertEngineerInfo($engineerDTO);
        $result->setSuccessWithResult($engineerInfo);
        return $result;
    }

    public function searchEngineer(?string $keyword): Result
    {
        $result = new  Result();
        $data = UserEngineerModel::getInstance()->searchEngineer($keyword);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function queryEngineerList(QueryEngineerListParams $params): Result
    {
        $result = new  Result();
        $data = UserEngineerModel::getInstance()->queryEngineerList($params);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function queryEngineerCount(QueryEngineerListParams $params): Result
    {
        $result = new  Result();
        $data = UserEngineerModel::getInstance()->queryEngineerCount($params);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function updateEngineerInfo(UserEngineerDTO $userEngineerDTO): Result
    {
        $result = new  Result();
        $userEngineerDTO->updated_timestamp = TimeHelper::getTimeMs();
        $data = UserEngineerModel::getInstance()->updateEngineerInfo($userEngineerDTO);
        $result->setSuccessWithResult($data);
        return $result;
    }
}