<?php

namespace service\user\impl;

use facade\request\user\FeedbackParams;
use facade\response\Result;
use facade\service\user\FeedbackService;
use model\dto\FeedbackDTO;
use model\FeedbackModel;

class FeedbackServiceImpl implements FeedbackService
{
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function sendFeedback(FeedbackParams $feedbackParams): ?Result
    {
        $feedbackDTO = new FeedbackDTO();
        \FUR_Core::copyProperties($feedbackParams, $feedbackDTO);
        $feedbackModel = FeedbackModel::getInstance();

        $feedbackDTO->feedback_status = FeedbackDTO::FEEDBACK_STATUS_NEW;

        $feedbackDTO->created_timestamp = time();
        if ($feedbackDTO->feedback_type == "") {
            $feedbackDTO->feedback_type = FeedbackDTO::FEEDBACK_TYPE_NORMAL;
        }
        $feedbackDTO->feedback_imgs = json_encode($feedbackParams->feedback_imgs);

        $feedbackId = $feedbackModel->insertRecord($feedbackDTO);

        $result = new Result();
        $result->setSuccessWithResult($feedbackId);
        return $result;
    }
}