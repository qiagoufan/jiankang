<?php


namespace service\user\impl;

use facade\request\feed\search\DeleteEsDocumentParams;
use facade\request\feed\search\EsDataSynParams;
use facade\request\feed\search\WriteEsDocumentParams;
use facade\request\user\CreateStarParams;
use facade\request\user\FollowParams;
use facade\request\user\UpdateStarParams;
use facade\response\Result;
use facade\service\user\StarService;
use lib\AppConstant;
use model\dto\UserStarDTO;
use model\maintain\MaintainUserStarExtraModel;
use model\UserStarModel;
use service\search\impl\BizEsDataSynServiceImpl;
use service\search\impl\SearchServiceImpl;

class StarServiceImpl implements StarService
{


    public static $_instance;


    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function saveStarInfo(CreateStarParams $createStarParams): Result
    {
        $result = new Result();
        $userStarDTO = new UserStarDTO();
        \FUR_Core::copyProperties($createStarParams, $userStarDTO);

        $userStarDTO->created_timestamp = time();
        $userStarModel = UserStarModel::getInstance();
        $starId = $userStarModel->insertRecord($userStarDTO);


        $userStarDTO->id = $starId;
        $userStarModel->setStarInfoCache($userStarDTO);


        $dataSynParams = new  EsDataSynParams();
        $dataSynParams->biz_id = $starId;
        $dataSynParams->biz_type = AppConstant::BIZ_TYPE_STAR;
        BizEsDataSynServiceImpl::getInstance()->setBizRecord($dataSynParams);


        $result->setSuccessWithResult($starId);
        return $result;
    }

    public function updateStarInfo(UpdateStarParams $updateStarParams): Result
    {
        $result = new Result();
        $userStarDTO = new UserStarDTO();
        $userStarDTO->id = $updateStarParams->star_id;
        \FUR_Core::copyProperties($updateStarParams, $userStarDTO);

        $userStarDTO->updated_timestamp = time();
        $userStarModel = UserStarModel::getInstance();


        $userStarModel->updateUserStarDTO($userStarDTO);

        $userStarModel->cleanStarInfoCache($userStarDTO->id);

        $starId = $userStarDTO->id;

        /**
         * 更新ES
         */
        $dataSynParams = new  EsDataSynParams();
        $dataSynParams->biz_id = $starId;
        $dataSynParams->biz_type = AppConstant::BIZ_TYPE_STAR;
        BizEsDataSynServiceImpl::getInstance()->setBizRecord($dataSynParams);

        $result->setSuccessWithResult(true);
        return $result;
    }

    public function getStarInfo(int $starId): Result
    {
        $result = new Result();

        $userStarModel = UserStarModel::getInstance();

        /** @var UserStarDTO $userStarDTO */
        $userStarDTO = $userStarModel->getStarInfoCache($starId);
        if ($userStarDTO == null) {
            $userStarDTO = $userStarModel->getStarInfo($starId);
            if ($userStarDTO != null) {
                $userStarModel->setStarInfoCache($userStarDTO);
            }

        }

        if ($userStarDTO != null) {
            if ($userStarDTO->ip_type == 0) {
                $userStarDTO->ip_type = UserStarDTO::IP_TYPE_IDOL;
            }
        }
        if ($userStarDTO->official_background) {
            $userStarDTO->official_background .= '?x-oss-process=image/resize,m_fill,h_560,w_750';
        }


        $result->setSuccessWithResult($userStarDTO);
        return $result;
    }


    //todo:修改排名规则
    public function getStarRank(int $starId): Result
    {
        $result = new Result();
        $rank = 999999;
        $followService = FollowServiceImpl::getInstance();
        $followParams = new FollowParams();
        $followParams->target_id = $starId;
        $followParams->target_type = UserStarDTO::STAR_BIZ_TYPE;
        $rankRet = $followService->getRank($followParams);
        if (Result::isSuccess($rankRet)) {
            $rank = $rankRet->data;
        }
        $result->setSuccessWithResult($rank);
        return $result;
    }

    public function searchStar(string $keyword, int $index = 0, int $pageSize = 10): Result
    {
        $result = new Result();
        $starModel = UserStarModel::getInstance();

        $offset = $index * $pageSize;

        $starDTOList = $starModel->searchStar($keyword, $offset, $pageSize);
        $result->setSuccessWithResult($starDTOList);
        return $result;
    }

    public function getStarInfoByName(string $starName): Result
    {
        $result = new Result();
        $starModel = UserStarModel::getInstance();
        $starDTO = $starModel->queryStarByName($starName);
        $result->setSuccessWithResult($starDTO);
        return $result;
    }


    public function getStarList(int $index, int $pageSize, ?string $starName = null, ?int $ip_type = UserStarDTO::IP_TYPE_IDOL): ?Result
    {
        $result = new Result();
        $starModel = UserStarModel::getInstance();

        $offset = $index * $pageSize;
        $starDTOList = $starModel->queryStarList($offset, $pageSize, $starName, $ip_type);
        $result->setSuccessWithResult($starDTOList);
        return $result;

    }

    public function getAllStarList(int $index, int $pageSize, ?string $starName = null): ?Result
    {
        $result = new Result();
        $starModel = UserStarModel::getInstance();

        $offset = $index * $pageSize;
        $starDTOList = $starModel->queryAllStarList($offset, $pageSize, $starName);
        $result->setSuccessWithResult($starDTOList);
        return $result;

    }

    public function queryStarTotalCount(?string $keyword = null): ?Result
    {
        $result = new Result();
        $starModel = UserStarModel::getInstance();
        $starDTOList = $starModel->queryStarTotalCount($keyword);
        $result->setSuccessWithResult($starDTOList);
        return $result;

    }


    /**
     * @return Result|null
     * 获取明星人气值
     */
    public function getLeaderBoardValue(int $userId, int $starId): ?Result
    {
        $result = new Result();
        $maintainLeaderboatdRecordModel = MaintainUserStarExtraModel::getInstance();
        $countValue = $maintainLeaderboatdRecordModel->queryUserRecord($userId, $starId);
        $result->setSuccessWithResult($countValue);
        return $result;
    }


    public function deleteStarInfo(int $starId): ?Result
    {
        $result = new Result();
        $starModel = UserStarModel::getInstance();
        $starModel->cleanStarInfoCache($starId);
        $delRet = $starModel->deleteStarFromDB($starId);
        $result->setSuccessWithResult($delRet);


        /**
         *删除ES
         */
        $dataSynParams = new  EsDataSynParams();
        $dataSynParams->biz_id = $starId;
        $dataSynParams->biz_type = AppConstant::BIZ_TYPE_STAR;
        BizEsDataSynServiceImpl::getInstance()->delBizRecord($dataSynParams);
        return $result;

    }


    /**
     * @param int $starId
     * @return Result|null
     * 增加讨论热度
     */
    public function addDiscussionHeat(int $starId, string $bizType): Result
    {
        $result = new Result();
        $starModel = UserStarModel::getInstance();
        $starModel->updateDiscussionHeat($starId, $bizType, 1);
        $result->setSuccessWithResult(true);
        return $result;
    }


    /**
     * @param int $starId
     * @param string $bizType
     * @return Result
     * 减去讨论热度
     */
    public function reduceDiscussionHeat(int $starId, string $bizType): Result
    {

        $result = new Result();
        $starModel = UserStarModel::getInstance();
        $heatValue = $starModel->getDiscussionHeatValue($bizType, $starId);
        if ($heatValue > 0) {
            $starModel->updateDiscussionHeat($starId, $bizType, -1);
        }
        $result->setSuccessWithResult(true);
        return $result;

    }


    public function getDiscussionHeat(int $starId, string $bizType): ?Result
    {

        $result = new Result();
        $starModel = UserStarModel::getInstance();
        $heatValue = $starModel->getDiscussionHeatValue($bizType, $starId);
        $result->setSuccessWithResult($heatValue);
        return $result;
    }

    public function getDiscussionHeatRank(int $starId, string $bizType): ?Result
    {

        $result = new Result();
        $starModel = UserStarModel::getInstance();
        $rank = $starModel->getDiscussionHeatRank($starId, $bizType);
        $result->setSuccessWithResult($rank);
        return $result;

    }


    public function getStarDiscussionList(string $bizType, string $pageSize, int $index): ?Result
    {

        $result = new Result();
        $starModel = UserStarModel::getInstance();
        $start = $index * $pageSize;
        $starList = $starModel->getUserStarDiscussionList($bizType, $start, $pageSize);

        $result->setSuccessWithResult($starList);
        return $result;
    }

}