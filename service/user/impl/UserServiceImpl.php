<?php

namespace service\user\impl;

use Config_Error;
use facade\request\RequestParams;
use facade\request\socialite\SocialiteParams;
use facade\request\user\LoginByAccountParams;
use facade\request\user\LoginByPhoneParams;
use facade\request\user\RegisterParams;
use facade\request\user\UpdateUserInfoParams;
use facade\response\Result;
use facade\response\user\JwtTokenResp;
use facade\service\user\UserService;
use Firebase\JWT\JWT;
use model\user\dto\UserDTO;
use model\user\dto\UserLoginInfoDTO;
use model\user\dto\UserTokenDTO;
use model\user\UserLoginModel;
use model\user\UserModel;
use model\user\UserTokenModel;
use model\user\UserWhiteModel;
use service\user\info\JwtPayloadInfo;
use service\user\info\LoginCertificateInfo;

class UserServiceImpl implements UserService
{
    const COOL_DOWN_UPDATE_NAME_TIME = 86400;
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    /** 用手机号登录
     * @param LoginByPhoneParams $loginParams
     * @return Result
     */

    public function loginByPhone(LoginByPhoneParams $loginParams): Result
    {
        $result = new Result();
        $userModel = UserModel::getInstance();
        $userId = $userModel->queryUserIdByPhone($loginParams->phone);


        /**
         * 如果用户是第一次登陆
         */
        if ($userId == 0) {
            $result->setError(\Config_Error::ERR_USER_NOT_EXIST);
            return $result;
        }
        /**
         * 获取用户信息
         */
        $userInfo = $userModel->getUserInfo($userId);
        if ($userInfo == null) {
            $result->setError(\Config_Error::ERR_USER_NOT_EXIST);
            return $result;
        }


        $jwtToken = $this->makeUserRefreshToken($userInfo, $loginParams->device_id);
        $result->setSuccessWithResult($jwtToken);
        return $result;
    }


    public function getUserLoginToken(int $userId, RequestParams $requestParams): Result
    {
        $result = new Result();
        $userModel = UserModel::getInstance();
        /**
         * 获取用户信息
         */
        $userInfo = $userModel->getUserInfo($userId);
        if ($userInfo == null) {
            $result->setError(\Config_Error::ERR_USER_NOT_EXIST);
            return $result;
        }


        $jwtToken = $this->makeUserRefreshToken($userInfo, $requestParams->device_id);
        $result->setSuccessWithResult($jwtToken);
        return $result;
    }

    public function getValidUserInfo(int $userId): ?Result
    {
        $result = new Result();
        $userModel = UserModel::getInstance();
        /** @var UserDTO $userDTO */
        $userDTO = $userModel->getUserInfo($userId);

        if ($userDTO == null || $userDTO->status != \Config_Const::USER_STATUS_VALID) {
            $userDTO = null;
            $result->setSuccessWithResult($userDTO);
            return $result;
        }

        $result->setSuccessWithResult($userDTO);
        return $result;
    }


    public function queryUserIdByPhone(string $phone): Result
    {
        $result = new Result();
        $userId = UserLoginModel::getInstance()->queryUserId($phone, \Config_Const::USER_LOGIN_TYPE_PHONE);
        $result->setSuccessWithResult($userId);
        return $result;
    }


    public function queryUserIdByInviteCode(string $inviteCode)
    {
        $result = new Result();
        $userId = UserModel::getInstance()->queryUserIdByInviteCode($inviteCode);
        $result->setSuccessWithResult($userId);
        return $result;
    }

    public function queryUserIdByOpenId(string $openId, string $loginType): Result
    {
        $result = new Result();
        $userId = UserLoginModel::getInstance()->queryUserId($openId, $loginType);
        $result->setSuccessWithResult($userId);
        return $result;
    }


    private function updateUserLatestLoginInfo(int $userId)
    {
        $userModel = UserModel::getInstance();
        $loginUserDTO = new UserDTO();
        $loginUserDTO->latest_login_ip = \FUR_Helper::get_ip();
        $loginUserDTO->latest_login_timestamp = time();
        $loginUserDTO->id = $userId;
        $userModel->updateUserInfo($loginUserDTO);
    }

    public function updateUserInfo(UpdateUserInfoParams $updateUserInfoParams): ?Result
    {
        $result = new Result();
        $userModel = UserModel::getInstance();

        $userDTO = new UserDTO();
        \FUR_Core::copyProperties($updateUserInfoParams, $userDTO);
        $userDTO->id = $updateUserInfoParams->user_id;
        $userDTO->updated_timestamp = time();
        if ($updateUserInfoParams->name) {
            $userDTO->nick_name = $updateUserInfoParams->name;
        }
        $userDTO->device_id = $updateUserInfoParams->device_id;


        $userModel->updateUserInfo($userDTO);
        $result->setSuccessWithResult(true);
        return $result;
    }

    public function refreshToken(string $refreshToken, string $deviceId): ?Result
    {
        $result = new Result();


        if ($refreshToken == null) {
            \FUR_Log::info("token 是null:" . $refreshToken);
            $result->setError(\Config_Error::ERR_TOKEN_REFRESH_FAIL);
            return $result;
        }

        $userTokenModel = UserTokenModel::getInstance();
        $userTokenDTO = $userTokenModel->queryToken($refreshToken);

        /**
         * token不存在
         */
        if ($userTokenDTO == null) {

            \FUR_Log::info("token 不存在:" . $refreshToken);
            $result->setError(\Config_Error::ERR_TOKEN_REFRESH_FAIL);
            return $result;
        }

        /**
         * 设备不匹配
         */
        if ($userTokenDTO->device_id != $deviceId) {

            \FUR_Log::info("设备 不匹配:" . $refreshToken);
            $result->setError(\Config_Error::ERR_TOKEN_REFRESH_FAIL);
            return $result;
        }

        /**
         * refresh 超过最大时间
         */
        if (time() - $userTokenDTO->expire_timestamp > UserTokenDTO::REFRESH_MAX_EXPIRE_TIME) {

            \FUR_Log::info("超过最大时间:" . $refreshToken);
            $result->setError(\Config_Error::ERR_TOKEN_REFRESH_FAIL);
            return $result;
        }
        $userModel = UserModel::getInstance();
        $userId = $userTokenDTO->user_id;
        $userInfo = $userModel->getUserInfo($userId);
        /**
         *用户不存在
         */
        if ($userInfo == null) {
            \FUR_Log::info("用户不存在:" . $refreshToken);
            $result->setError(\Config_Error::ERR_TOKEN_REFRESH_FAIL);
            return $result;
        }


        $jwtToken = $this->makeUserRefreshToken($userInfo, $deviceId);
        $result->setSuccessWithResult($jwtToken);
        return $result;
    }

    private function makeUserRefreshToken(UserDTO $userInfo, ?string $deviceId): ?JwtTokenResp
    {
        $userId = $userInfo->id;

        /**
         * 针对无法取得设备id的设备增加一个随机device
         */
        if ($deviceId == null) {
            $ip = \FUR_Helper::get_ip();
            $agent = $_SERVER['HTTP_USER_AGENT'] ? $_SERVER['HTTP_USER_AGENT'] : "";
            $deviceId = "h5_" . md5($ip . $agent);
        }
        /**
         * 生成token
         */
        $jwtPayload = JwtPayloadInfo::buildJwtPayloadInfo($userInfo);
        $accessToken = JWT::encode($jwtPayload, \FUR_Config::get_common('jwt_key'));

        /**
         * 检查白名单
         */

        $jwtToken = JwtTokenResp::buildToken($accessToken, null, $userId);
        $this->updateUserLatestLoginInfo($userId);


        return $jwtToken;
    }

    public function logout(int $userId): ?Result
    {
        $userTokenModel = UserTokenModel::getInstance();
        $userTokenModel->deleteUserToken($userId);
        $result = new Result();
        $result->setSuccessWithResult();
        return $result;
    }

    public function queryUserIdBySocialite(string $openId, string $provider): ?Result
    {
        $userLoginModel = UserLoginModel::getInstance();
        $userId = $userLoginModel->queryUserId($openId, $provider);
        $result = new Result();
        $result->setSuccessWithResult($userId);
        return $result;
    }


    public function register(RegisterParams $registerParams): ?Result
    {
        $result = new Result();
        $userId = UserLoginModel::getInstance()->queryUserId($registerParams->login_account, $registerParams->login_type);
        if ($userId) {
            $result->setSuccessWithResult($userId);
            return $result;
        }

        $userDTO = RegisterParams::buildUserDTO($registerParams);
        $userDTO->invite_code = substr(md5(time() . rand(0, 1111111111)), 0, 10);
        $userId = UserModel::getInstance()->insertUserRecord($userDTO);

        $registerParams->user_id = $userId;
        $userLoginDTO = RegisterParams::buildUserLoginDTO($registerParams);
        UserLoginModel::getInstance()->insertUserLoginRecord($userLoginDTO);

        $result->setSuccessWithResult($userId);
        return $result;
    }


    public function cancelUser(int $userId): Result
    {
        $result = new Result();
        $userModel = UserModel::getInstance();
        $userDTO = $userModel->getUserInfo($userId);
        $userDTO->phone = $userDTO->phone . "_del_" . time();
        $userDTO->nick_name = '已注销' . uniqid();
        $userDTO->status = UserDTO::USER_STATUS_DELETED;
        $userDTO->nick_name = '已注销' . uniqid();
        $userModel->deleteUser($userDTO);
        $result->setSuccessWithResult(true);
        return $result;
    }

    public function queryUserIdByNickName(string $nickName): Result
    {
        $result = new Result();
        $userId = 0;
        $userDTO = UserModel::getInstance()->getUserInfoByNickName($nickName);
        if ($userDTO != null) {
            $userId = $userDTO->id;
        }
        $result->setSuccessWithResult($userId);
        return $result;
    }

    public function formatValidSocialiteNickName(string $nickName): Result
    {
        $result = new Result();
        if ($nickName == null) {
            $nickName = "小花仙" . uniqid();
        }

        if (mb_strlen($nickName) > 16) {
            $nickName = mb_substr($nickName, 0, 16);
        }
        $userDTO = UserModel::getInstance()->getUserInfoByNickName($nickName);
        if ($userDTO != null) {
            $nickName = $nickName . rand(0, 100000);
        }
        $result->setSuccessWithResult($nickName);
        return $result;
    }


    public function getUserFeedBackList()
    {
        $result = new Result();
        $feedBackModel = FeedbackModel::getInstance();
        $list = $feedBackModel->queryFeedBackList();
        $result->setSuccessWithResult($list);
        return $result;
    }

    public function blockUser(int $userId, int $blockUserId): Result
    {
        $result = new Result();
        $userModel = UserModel::getInstance();
        $blockUserCount = $userModel->getBlockUserCount($userId);
        if ($blockUserCount >= 100) {
            $result->setError(\Config_Error::ERR_USER_BLOCK_MAX);
            return $result;
        }

        $userModel->blockUser($userId, $blockUserId);
        $userModel->blockUserCache($userId, $blockUserId);
        $result->setSuccessWithResult(true);
        return $result;
    }

    public function registerMajiaUser(RegisterParams $registerParams, int $majiaUserId): Result
    {
        $result = new Result();

        $userModel = UserModel::getInstance();
        $userId = $userModel->queryUserIdByPhone($registerParams->phone);
        if ($userId != 0) {
            $result->setError(\Config_Error::ERR_USER_HAS_REGISTER);
        }

        $userDTO = UserDTO::buildUserDTOByRegisterParams($registerParams);
        $userDTO->id = $majiaUserId;
        $userModel = UserModel::getInstance();
        $userId = $userModel->insertUserRecord($userDTO);
        $result->setSuccessWithResult($userId);
        return $result;
    }


    public function unblockUser(int $userId, int $blockUserId): Result
    {
        $result = new Result();
        $userModel = UserModel::getInstance();

        $userModel->unblockUser($userId, $blockUserId);
        $userModel->unblockUserCache($userId, $blockUserId);
        $result->setSuccessWithResult(true);
        return $result;
    }

    public function getBlockUserIdList(int $userId): Result
    {
        $result = new Result();
        $userModel = UserModel::getInstance();
        $userIdList = $userModel->getBlockUserIdList($userId);
        $result->setSuccessWithResult($userIdList);
        return $result;
    }

    public function isBlock(int $userId, int $blockUserId): Result
    {
        $result = new Result();
        $isBlock = false;
        if ($userId == 0) {
            $result->setSuccessWithResult($isBlock);
            return $result;
        }
        $isBlock = UserModel::getInstance()->isBlock($userId, $blockUserId);
        $result->setSuccessWithResult($isBlock);
        return $result;
    }


    public function registerBySocialite(SocialiteParams $socialiteParams): ?Result
    {
        $result = new Result();

        $openId = $socialiteParams->open_id;
        $provider = $socialiteParams->provider;

        $userId = UserLoginModel::getInstance()->queryUserId($socialiteParams->open_id, $socialiteParams->provider);

        if ($userId != 0) {
            $result->setError(Config_Error::ERR_USER_SOCIALITE_HAS_BIND);
            return $result;
        }
        $registerParams = RegisterParams::buildParamsBySocialite($socialiteParams, $openId, $provider);
        $registerRet = $this->register($registerParams);
        if (!Result::isSuccess($registerRet)) {
            return $registerRet;
        }
        $userId = $registerRet->data;
        if ($userId == 0) {
            $result->setError(Config_Error::ERR_USER_SOCIALITE_REGISTER_FAIL);
            return $result;
        }
        $result->setSuccessWithResult($userId);
        return $result;
    }

    public
    function bindPhone(int $userId, string $phoneNum): ?Result
    {
        $result = new Result();
        /**
         * 查看手机号是否注册过用户,注册过的话就返回错误
         */
        $userModel = UserModel::getInstance();
        $userIdByPhone = $userModel->queryUserIdByPhone($phoneNum);
        if ($userIdByPhone != 0) {
            $result->setError(\Config_Error::ERR_USER_PHONE_HAS_BIND);
            return $result;
        }
        $userDTO = $userModel->getUserInfo($userId);
        $userDTO->phone = $phoneNum;
        $userDTO->phone_verified_timestamp = time();

        $userModel->updateUserInfo($userDTO);
        $userModel->saveUserCache($userDTO);
        $result->setSuccessWithResult($phoneNum);
        return $result;
    }

    public
    function bindPhoneCheck(int $userId): Result
    {
        $result = new Result();
        $userInfoRet = $this->getValidUserInfo($userId);
        if (!Result::isSuccess($userInfoRet)) {
            $result->setError(Config_Error::ERR_USER_IS_INVALID);
            return $result;
        }
        /** @var  UserDTO $userInfoDTO */
        $userInfoDTO = $userInfoRet->data;
        if ($userInfoDTO->phone == null) {
            $result->setError(Config_Error::ERR_USER_MUST_BIND_PHONE);
            return $result;
        }

        $result->setSuccessWithResult(true);
        return $result;
    }


    public
    function queryBlackUserById(int $userId): ?Result
    {
        $result = new Result();
        $userBlackModel = UserBlackModel::getInstance();
        $userBlackDTO = $userBlackModel->queryRecord($userId);
        $result->setSuccessWithResult($userBlackDTO);
        return $result;

    }


    public
    function queryWhiteUserById(int $userId): ?Result
    {
        $result = new Result();
        $userWhiteModel = UserWhiteModel::getInstance();
        $userWhiteDTO = $userWhiteModel->queryRecord($userId);
        $result->setSuccessWithResult($userWhiteDTO);
        return $result;
    }


    public
    function queryBlackUserList(int $index, int $pageSize): ?Result
    {

        $result = new Result();
        $userBlackModel = UserBlackModel::getInstance();
        $userBlackList = $userBlackModel->queryUserBlackList($index, $pageSize);
        $result->setSuccessWithResult($userBlackList);
        return $result;

    }


    public
    function createUserBlack(UserBlackDTO $userBlackDTO): ?Result
    {
        $result = new Result();
        $userBlackModel = UserBlackModel::getInstance();
        $id = $userBlackModel->insertRecord($userBlackDTO);
        $result->setSuccessWithResult($id);
        return $result;
    }

    public
    function queryUserWhiteList(int $index, int $pageSize): ?Result
    {

        $result = new Result();
        $userWhiteModel = UserWhiteModel::getInstance();
        $userWhiteList = $userWhiteModel->queryUserWhiteList($index, $pageSize);
        $result->setSuccessWithResult($userWhiteList);
        return $result;

    }


    public
    function deleteUserBlack(int $recordId): ?Result
    {
        $result = new Result();
        $userBlackModel = UserBlackModel::getInstance();
        $id = $userBlackModel->deleteRecord($recordId);
        $result->setSuccessWithResult($id);
        return $result;
    }

    public
    function deleteUserWhite(int $recordId): ?Result
    {
        $result = new Result();
        $userWhiteModel = UserWhiteModel::getInstance();
        $id = $userWhiteModel->deleteRecord($recordId);
        $result->setSuccessWithResult($id);
        return $result;
    }


    public function getRegisterUserCount(): ?Result
    {
        $result = new Result();
        $maxUserId = UserModel::getInstance()->getMaxUserId();
        /**
         * 减少100W起始数+1W马甲号
         */
        $userCount = $maxUserId - 1000000 + 10000;
        $result->setSuccessWithResult($userCount);
        return $result;
    }


    public function loginByAccount(LoginByAccountParams $loginByAccountParams): Result
    {
        $result = new Result();
        $userLoginInfoDTO = UserLoginModel::getInstance()->queryUserLoginInfoDTO($loginByAccountParams->login_account, \Config_Const::USER_LOGIN_TYPE_ACCOUNT);
        if ($userLoginInfoDTO == null) {
            $result->setError(\Config_Error::ERR_USER_NOT_EXIST);
            return $result;
        }

        $encryptPassword = $this->encryptPassword($loginByAccountParams->password);
        $loginCertificateInfo = $this->decodeLoginCertificate($userLoginInfoDTO->login_certificate);
        if ($encryptPassword != $loginCertificateInfo->password) {
            $result->setError(\Config_Error::ERR_PASSWORD_ERROR);
            return $result;
        }

        $userInfo = UserModel::getInstance()->getUserInfo($userLoginInfoDTO->user_id);
        if ($userInfo->status == \Config_Const::USER_STATUS_DELETED) {
            $result->setError(\Config_Error::ERR_USER_IS_INVALID);
            return $result;
        }

        $jwtToken = $this->makeUserRefreshToken($userInfo, $loginByAccountParams->device_id);
        $result->setSuccessWithResult($jwtToken);
        return $result;

    }

    private function encryptPassword($password)
    {
        return md5(\FUR_Config::get_common('password_salt') . $password);

    }

    public function queryLoginAccountByUserId(?int $userId, ?string $loginType): ?Result
    {
        $account = null;
        $record = UserLoginModel::getInstance()->queryUserLoginInfoByUserId($userId, $loginType);
        $result = new Result();
        if ($record) {
            $account = $record->login_account;
        }
        $result->setSuccessWithResult($account);
        return $result;
    }

    private function decodeLoginCertificate(?string $loginCertificate): LoginCertificateInfo
    {
        $decodeObj = json_decode($loginCertificate);
        $loginCertificate = new LoginCertificateInfo();
        if ($decodeObj != null) {
            \FUR_Core::copyProperties($decodeObj, $loginCertificate);
        }
        return $loginCertificate;
    }

    public function bindUserSocialite(int $userId, string $openId, string $provider): Result
    {
        $userLoginInfoDTO = new UserLoginInfoDTO();
        $userLoginInfoDTO->user_id = $userId;
        $userLoginInfoDTO->created_timestamp = time();
        $userLoginInfoDTO->login_type = $provider;
        $userLoginInfoDTO->login_account = $openId;
        $recordId = UserLoginModel::getInstance()->insertUserLoginRecord($userLoginInfoDTO);
        $result = new Result();
        $result->setSuccessWithResult($recordId > 0);
        return $result;
    }

    public function bindUserNewLoginInfo(RegisterParams $registerParams): Result
    {
        $userLoginInfoDTO = new UserLoginInfoDTO();
        $userLoginInfoDTO->user_id = $registerParams->user_id;
        $userLoginInfoDTO->created_timestamp = time();
        $userLoginInfoDTO->login_type = $registerParams->login_type;
        $userLoginInfoDTO->login_account = $registerParams->login_account;
        $recordId = UserLoginModel::getInstance()->insertUserLoginRecord($userLoginInfoDTO);
        $result = new Result();
        $result->setSuccessWithResult($recordId > 0);
        return $result;
    }
}