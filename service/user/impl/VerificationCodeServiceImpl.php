<?php

namespace service\user\impl;

use Carbon\Carbon;
use Config_Error;
use facade\request\aliyun\SendSmsParams;
use facade\request\user\VerificationCodeParams;
use facade\response\Result;
use facade\service\user\VerificationCodeService;
use FUR_Validate;
use lib\PhoneHelper;
use lib\TimeHelper;
use model\verification\dto\VerificationCodeDTO;
use model\verification\VerificationCodeModel;
use service\aliyun\impl\SmsServiceImpl;

class VerificationCodeServiceImpl implements VerificationCodeService
{
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function sendCode(VerificationCodeParams &$verificationCodeParams): Result
    {
        $now = TimeHelper::getTimeMs();
        $result = new Result();
        $action = $verificationCodeParams->action;
        $phone = $verificationCodeParams->phone;
        /**
         * 验证手机是否合法
         */
        if (!preg_match(FUR_Validate::$regx['mobile'], $phone)) {
            $result->setError(Config_Error::ERR_INVALID_PHONE);
            return $result;
        }

        /**
         * 查询用户是否发过验证码
         */
        $verificationCodeModel = VerificationCodeModel::getInstance();

        /** @var VerificationCodeDTO $verificationCodeRecord */
        $verificationCodeRecord = $verificationCodeModel->queryRecordByPhone($phone, $action);
        if ($verificationCodeRecord != null) {
            /**
             * 验证上一次是否发送过验证码
             */
            if ($verificationCodeRecord->created_timestamp + \Config_Const::VERIFICATION_CODE_INTERVAL_TIME_LONG > $now) {
                $result->setError(Config_Error::ERR_VERIFICATION_FREQUENT_ACTION);
                return $result;
            }
            /**
             * 验证每日最大次数
             */
            $todayTimestamp = Carbon::today()->getTimestamp() * 1000;
            $todaySendCount = $verificationCodeModel->queryRecordCount($phone, $action, $todayTimestamp);
            if ($todaySendCount >= \Config_Const::VERIFICATION_CODE_DAILY_MAX_SEND_COUNT) {
                $result->setError(Config_Error::ERR_DAILY_MAX_SEND_COUNT);
                return $result;
            }
        }

        /**
         *添加记录
         */
        $verificationCodeDTO = $this->buildVerificationCodeDTO($verificationCodeParams);
        $id = $verificationCodeModel->insertRecord($verificationCodeDTO);

        /**
         * 发短信
         */
        $smsParams = new SendSmsParams();
        $smsParams->phone = $phone;
        $smsParams->templateCode = SendSmsParams::TEMPLATE_CODE_LOGIN;
        $smsParams->templateParam = json_encode(['code' => $verificationCodeDTO->code]);
        $smsParams->outId = $id;
        SmsServiceImpl::getInstance()->sendSms($smsParams);

        $result->setSuccessWithResult(true);
        return $result;
    }

    public function verifyCode(VerificationCodeParams &$verificationCodeParams): Result
    {
        $now = TimeHelper::getTimeMs();
        $result = new Result();
        $action = $verificationCodeParams->action;
        $phone = $verificationCodeParams->phone;

        /**
         * 验证手机是否合法
         */
        if (!preg_match(FUR_Validate::$regx["mobile"], $phone)) {
            $result->setError(Config_Error::ERR_INVALID_PHONE);
            return $result;
        }

        /**
         * 查询用户的上一次验证码
         */
        $verificationCodeModel = VerificationCodeModel::getInstance();

        /** @var VerificationCodeDTO $verificationCodeRecord */
        $verificationCodeRecord = $verificationCodeModel->queryRecordByPhone($phone, $action);
        if ($verificationCodeRecord == null) {
            $result->setError(Config_Error::ERR_VERIFICATION_VALID_CODE);
            return $result;
        }
        /**
         * 验证验证码错误次数
         */
        if ($verificationCodeRecord->error_count >= \Config_Const::VERIFICATION_CODE_MAX_VERIFICATION_COUNT) {
            $result->setError(Config_Error::ERR_MAX_VERIFICATION_COUNT);
            return $result;
        }
        /**
         * 验证验证码是否正确
         */
        if ($verificationCodeRecord->code != $verificationCodeParams->code) {
            $verificationCodeModel->updateRecordErrorCount($verificationCodeRecord);
            $result->setError(Config_Error::ERR_VERIFICATION_ERROR_CODE);
            return $result;
        }

        /**
         * 验证验证码是否过期
         */
        if ($verificationCodeRecord->expire_timestamp < $now) {
            $result->setError(Config_Error::ERR_VERIFICATION_EXPIRE_CODE);
            return $result;
        }

        $result->setSuccessWithResult(true);
        return $result;
    }

    public function deleteCode(string $phone, string $code): Result
    {
        $result = new Result();
        VerificationCodeModel::getInstance()->deleteCode($phone, $code);
        $result->setSuccessWithResult(true);
        return $result;
    }

    private function buildVerificationCodeDTO(VerificationCodeParams $verificationCodeParams)
    {
        $verificationCodeDTO = new VerificationCodeDTO();
        $verificationCodeDTO->action = $verificationCodeParams->action;
        $verificationCodeDTO->code = random_int(100000, 999999);
        $verificationCodeDTO->phone = $verificationCodeParams->phone;
        $verificationCodeDTO->user_id = $verificationCodeParams->user_id;
        $verificationCodeDTO->created_timestamp = TimeHelper::getTimeMs();
        $verificationCodeDTO->expire_timestamp = TimeHelper::getTimeMs() + \Config_Const::VERIFICATION_CODE_EXPIRE_TIME_LONG;
        return $verificationCodeDTO;
    }
}