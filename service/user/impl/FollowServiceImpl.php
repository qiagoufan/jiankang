<?php


namespace service\user\impl;

use facade\request\user\FollowParams;
use facade\request\user\GetFollowLeaderboardParams;
use facade\response\Result;
use facade\response\user\follow\FollowDetailResp;
use facade\service\user\FollowService;
use model\dto\MaintainUserStarExtraDTO;
use model\dto\UserFollowDTO;
use model\dto\UserStarDTO;
use model\UserFollowModel;
use service\maintain\impl\MaintainLeaderboardServiceImpl;
use facade\request\maintain\CreateLeaderboardRecordParams;
use model\dto\MaintainLeaderboardRecordDTO;

class FollowServiceImpl implements FollowService
{
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    /**
     * @param FollowParams $followParams
     * @return Result|null
     */
    public function follow(FollowParams $followParams): ?Result
    {
        $result = new Result();
        $userId = $followParams->user_id;
        $targetId = $followParams->target_id;
        $targetType = $followParams->target_type;
        $userFollowModel = UserFollowModel::getInstance();
        /**
         * 参数校验
         */
        if ($userId == 0 || $targetId == 0) {
            $result->setError(\Config_Error::ERR_FOLLOW_PARAMS_INVALID);
            return $result;
        }
        if ($userId == $targetId) {
            $result->setError(\Config_Error::ERR_FOLLOW_CANT_BE_SELF);
            return $result;
        }

        /**
         * 已经关注过就不用重复关注了
         */
        $isFollow = $userFollowModel->isFollowFromCache($userId, $targetId, $targetType);
        if ($isFollow) {
            $result->setSuccessWithResult($isFollow);
            return $result;
        }


        $followTimestamp = time();

        /**
         * 存储redis
         */
        $userFollowModel->saveFollowCache($userId, $targetId, $followTimestamp, $targetType);

        /** 存储db */
        if ($userFollowModel->queryRecordFromDb($userId, $targetId, $targetType) != null) {
            $result->setSuccessWithResult(true);
            return $result;
        }

        /**
         *存储db record
         */
        $userFollowDTO = new UserFollowDTO();
        $userFollowDTO->target_id = $targetId;
        $userFollowDTO->user_id = $userId;
        $userFollowDTO->target_type = $targetType;
        $userFollowDTO->follow_timestamp = $followTimestamp;
        $userFollowModel->saveFollowRecord($userFollowDTO);

        /**
         * 用户数据变更
         */
        /** 增加我的关注数 */
        $userFollowModel->updateFocusCount($userId, $targetType, 1);
        /** 增加目标的粉丝数 */
        $userFollowModel->updateFansCount($targetId, $targetType, 1);
        /** 更改follow数排名 */
        $fansCount = $userFollowModel->getFansCount($targetId, $targetType);
        $userFollowModel->updateRankScore($targetId, $targetType, $fansCount);


        /**更新人气排行 */
        $this->updateMaintainLeaderboard($followParams, MaintainLeaderboardRecordDTO::RECORD_TYPE_FOLLOW);
        $result->setSuccessWithResult(true);
        return $result;

    }

    /**
     * @param FollowParams $followParams
     * @return Result|null
     */
    public function unFollow(FollowParams $followParams): ?Result
    {
        $result = new Result();
        $userId = $followParams->user_id;
        $targetId = $followParams->target_id;
        $targetType = $followParams->target_type;
        $userFollowModel = UserFollowModel::getInstance();
        if ($userId == 0 || $targetId == 0) {
            $result->setError(\Config_Error::ERR_FOLLOW_PARAMS_INVALID);
            return $result;
        }

        /**
         * 未关注的话,什么都不用做
         */
        $isFollow = $userFollowModel->isFollowFromCache($userId, $targetId, $targetType);
        if ($isFollow == false) {
            $result->setSuccessWithResult($isFollow);
            return $result;
        }


        /**
         * 删除record
         */
        $userFollowModel->deleteFollowRecord($userId, $targetId, $targetType);
        /**
         * redis删掉记录
         */
        $userFollowModel->unFollowCache($userId, $targetId, $targetType);

        /**
         * 用户数据变更
         */
        /** 增加我的关注数 */
        $userFollowModel->updateFocusCount($userId, $targetType, -1);
        /** 增加目标的粉丝数 */
        $userFollowModel->updateFansCount($targetId, $targetType, -1);
        /**
         * 更改follow数排名
         */
        $fansCount = $userFollowModel->getFansCount($targetId, $targetType);
        $userFollowModel->updateRankScore($targetId, $targetType, $fansCount);

        /**
         * 更新人气排行
         */
        $this->updateMaintainLeaderboard($followParams, MaintainLeaderboardRecordDTO::RECORD_TYPE_UNFOLLOW);

        $result->setSuccessWithResult(true);
        return $result;
    }

    /**
     * 查询用户关注列表
     * @param FollowParams $followParams
     * @return Result|null
     */
    public function getUserFocusList(FollowParams $followParams): ?Result
    {
        $result = new Result();
        $userId = $followParams->user_id;
        $targetType = $followParams->target_type;
        $index = $followParams->index;
        $pageSize = 10;
        $offset = $index * $pageSize;
        $userFollowModel = UserFollowModel::getInstance();
        $focusList = $userFollowModel->getFocusListFromCache($userId, $targetType, $index, $pageSize);

        if ($focusList == null) {
            $focusListInDb = $userFollowModel->getFocusListFromDb($userId, $targetType, $offset, $pageSize);

            if (!empty($focusListInDb)) {
                /** @var UserFollowDTO $item */
                foreach ($focusListInDb as $item) {
                    $userFollowModel->saveFollowCache($userId, $item->target_id, $item->follow_timestamp, $item->target_type);
                    array_push($focusList, $item->target_id);
                }
            }
        }
        $result->setSuccessWithResult($focusList);
        return $result;
    }


    public function getFansList(FollowParams $followParams): ?Result
    {
        // TODO: Implement getMyFansList() method.
    }

    /**
     * 是否关注
     * @param FollowParams $followParams
     * @return Result|null
     */
    public function getFollowStatus(FollowParams $followParams): ?Result
    {

        $result = new Result();
        $userId = $followParams->user_id;
        $targetId = $followParams->target_id;
        $targetType = $followParams->target_type;
        $userFollowModel = UserFollowModel::getInstance();
        $isFollow = $userFollowModel->isFollowFromCache($userId, $targetId, $targetType);
        $result->setSuccessWithResult($isFollow);
        return $result;

    }

    /**
     * 查询粉丝数
     * @param FollowParams $followParams
     * @return Result|null
     */
    public function getFansNum(FollowParams $followParams): ?Result
    {
        $result = new Result();
        $targetId = $followParams->target_id;
        $targetType = $followParams->target_type;
        $userFollowModel = UserFollowModel::getInstance();
        $fansCount = $userFollowModel->getFansCount($targetId, $targetType);
        $result->setSuccessWithResult($fansCount);
        return $result;
    }

    /**
     * 查询我关注了多少人
     * @param FollowParams $followParams
     * @return Result|null
     */
    public function getFocusNum(FollowParams $followParams): ?Result
    {
        $result = new Result();
        $userId = $followParams->user_id;
        $userFollowModel = UserFollowModel::getInstance();
        $followCount = $userFollowModel->getFocusCount($userId, $followParams->target_type);
        $result->setSuccessWithResult($followCount);
        return $result;
    }

    public function getFollowDetail(FollowParams $followParams): ?Result
    {
        $result = new Result();
        $feedDetailResp = new FollowDetailResp();

        $userId = $followParams->user_id;
        $targetId = $followParams->target_id;
        $targetType = $followParams->target_type;
        $userFollowModel = UserFollowModel::getInstance();
        $isFollow = $userFollowModel->isFollowFromCache($userId, $targetId, $targetType);
        $feedDetailResp->follow_status = (int)$isFollow;
        $fansCount = $userFollowModel->getFansCount($targetId, $targetType);
        $feedDetailResp->fans_count = $fansCount;
        $result->setSuccessWithResult($feedDetailResp);
        return $result;
    }


    public function getRank(FollowParams $followParams): ?Result
    {
        $result = new Result();

        $targetId = $followParams->target_id;
        $targetType = $followParams->target_type;
        $userFollowModel = UserFollowModel::getInstance();
        $rank = $userFollowModel->getRank($targetId, $targetType);

        if ($rank != -1) {
            $rank = $rank + 1;
        }
        $result->setSuccessWithResult($rank);
        return $result;
    }

    public function getLeaderboard(GetFollowLeaderboardParams $getFollowLeaderboardParams): ?Result
    {
        $result = new Result();
        $index = $getFollowLeaderboardParams->index;
        $pageSize = $getFollowLeaderboardParams->page_size;
        $start = $index * $pageSize;
        $end = $start + $pageSize - 1;

        $targetType = $getFollowLeaderboardParams->target_type;
        $userFollowModel = UserFollowModel::getInstance();
        $leaderboard = $userFollowModel->getLeaderboard($targetType, $start, $end);
        $result->setSuccessWithResult($leaderboard);
        return $result;
    }

    /**
     * 明星需要更新人气值
     * @param FollowParams $followParams
     * @param string $recordType
     */
    private function updateMaintainLeaderboard(FollowParams $followParams, string $recordType)
    {


        if ($followParams->target_type != UserStarDTO::STAR_BIZ_TYPE) {
            return;
        }
        $starInfoRet = StarServiceImpl::getInstance()->getStarInfo($followParams->target_id);
        if (Result::isSuccess($starInfoRet) == false) {
            return;
        }
        /** @var UserStarDTO $starDTO */
        $starDTO = $starInfoRet->data;
        $createLeaderboardRecordParams = new CreateLeaderboardRecordParams();
        $createLeaderboardRecordParams->star_id = $followParams->target_id;
        $createLeaderboardRecordParams->user_id = $followParams->user_id;
        $createLeaderboardRecordParams->record_type = $recordType;
        if ($createLeaderboardRecordParams->record_type == MaintainLeaderboardRecordDTO::RECORD_TYPE_FOLLOW) {
            $createLeaderboardRecordParams->value = 1;
        } else {
            $createLeaderboardRecordParams->value = -1;
        }

        if ($starDTO->ip_type == UserStarDTO::IP_TYPE_IDOL) {
            $createLeaderboardRecordParams->rank_type = MaintainUserStarExtraDTO::RANK_TYPE_IDOL_POPULARITY;
        }
        $maintainLeaderboardServiceImpl = MaintainLeaderboardServiceImpl::getInstance();
        $maintainLeaderboardServiceImpl->createRecord($createLeaderboardRecordParams);
    }


}