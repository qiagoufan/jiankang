<?php

namespace service\user\impl;

use facade\request\user\LoginByPhoneParams;
use facade\request\user\RegisterParams;
use facade\request\user\UpdateUserInfoParams;
use facade\request\user\UpdateUserPushTokenParams;
use facade\response\Result;
use facade\response\user\JwtTokenResp;
use facade\service\user\UserExtraService;
use facade\service\user\UserPushService;
use facade\service\user\UserService;
use Firebase\JWT\JWT;
use lib\IpHelper;
use model\dto\UserDTO;
use model\dto\UserExtraDTO;
use model\dto\UserTokenDTO;
use model\user\dto\UserPushTokenDTO;
use model\user\UserPushTokenModel;
use model\UserExtraModel;
use model\UserModel;
use model\UserTokenModel;
use service\user\info\JwtPayloadInfo;

class UserPushServiceImpl implements UserPushService
{
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function updateUserPushToken(UpdateUserPushTokenParams $userPushTokenParams): Result
    {
        $result = new Result();
        $userPushModel = UserPushTokenModel::getInstance();
        $userPushTokenDTO = new    UserPushTokenDTO();
        \FUR_Core::copyProperties($userPushTokenParams, $userPushTokenDTO);
        $userPushTokenDTO->created_timestamp = time();
        $userPushTokenDTO->updated_timestamp = time();

        $ret = $userPushModel->updateUserPushToken($userPushTokenDTO);
        $result->setSuccessWithResult(true);
        return $result;
    }
}