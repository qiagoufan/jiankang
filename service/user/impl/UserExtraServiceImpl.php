<?php

namespace service\user\impl;

use facade\request\user\LoginByPhoneParams;
use facade\request\user\RegisterParams;
use facade\request\user\UpdateUserInfoParams;
use facade\response\Result;
use facade\response\user\JwtTokenResp;
use facade\service\user\UserExtraService;
use facade\service\user\UserService;
use Firebase\JWT\JWT;
use lib\IpHelper;
use model\dto\UserDTO;
use model\dto\UserExtraDTO;
use model\dto\UserTokenDTO;
use model\UserExtraModel;
use model\UserModel;
use model\UserTokenModel;
use service\user\info\JwtPayloadInfo;

class UserExtraServiceImpl implements UserExtraService
{
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function updateUserExtraCount(int $userId, string $field, int $number): bool
    {
        $userExtraModel = UserExtraModel::getInstance();
        $userExtraModel->updateUserExtraCount($userId, $field, $number);
        return true;
    }

    public function getUserExtra(int $userId, string $field)
    {
        $userExtraModel = UserExtraModel::getInstance();
        return $userExtraModel->getUserExtra($userId, $field);
    }

    public function setUserExtra(int $userId, string $field, string $value): bool
    {
        $userExtraModel = UserExtraModel::getInstance();
        $userExtraModel->updateUserExtra($userId, $field, $value);
        return true;
    }


}