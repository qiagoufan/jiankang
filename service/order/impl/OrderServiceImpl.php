<?php

namespace service\order\impl;


use facade\response\Result;
use facade\service\order\OrderService;
use model\order\dto\OrderInfoDTO;
use model\order\dto\OrderProductDTO;
use model\order\dto\OrderTransactionDTO;
use model\order\OrderModel;
use model\order\OrderProductModel;
use model\order\OrderTransactionModel;
use model\product\ProductItemModel;
use model\product\ProductSkuModel;
use model\ProductReviewModel;


class OrderServiceImpl implements OrderService
{

    public static $_instance;


    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    /**
     * @param OrderInfoDTO $orderInfoDTO
     * @return Result|null
     * 创建订单信息
     */
    public function createOrderInfo(OrderInfoDTO $orderInfoDTO): ?Result
    {
        $result = new Result();
        $orderModel = new OrderModel();
        $id = $orderModel->insertRecord($orderInfoDTO);
        $result->setSuccessWithResult($id);
        return $result;

    }


    public function createOrderProductInfo(OrderProductDTO $orderProductsDTO): ?Result
    {
        $result = new Result();

        $id = OrderProductModel::getInstance()->insertRecord($orderProductsDTO);
        $result->setSuccessWithResult($id);
        return $result;
    }

    /**
     * @param int $userId
     * @param int $orderType
     * @return Result
     */
    public function getOrderList(int $userId, int $orderType): Result
    {
        $result = new Result();
        $orderListDTO = OrderModel::getInstance()->queryOrderInfoList($userId, $orderType);
        $result->setSuccessWithResult($orderListDTO);
        return $result;

    }

    public function updateProductStock(int $itemId, int $skuId, int $saleCount): ?Result
    {
        $result = new Result();
        $rows = ProductSkuModel::getInstance()->updateSkuStock($skuId, $saleCount);
        if ($rows == 0) {
            $result->setError(\Config_Error::ERR_ORDER_STOCK_IS_NOT_ENOUGH);
            return $result;
        }

        $rows = ProductItemModel::getInstance()->updateItemStock($itemId, $saleCount);
        if ($rows == 0) {
            $result->setError(\Config_Error::ERR_ORDER_STOCK_IS_NOT_ENOUGH);
            return $result;
        }
        $result->setSuccessWithResult(true);
        return $result;
    }

    /**
     * @param $orderId
     * @return Result|null根据id查询order信息
     */

    public function getOrderInfo($orderId): ?Result
    {
        $result = new Result();
        $orderProductsModel = OrderModel::getInstance();
        $orderInfo = $orderProductsModel->queryOrderInfoById($orderId);
        $result->setSuccessWithResult($orderInfo);
        return $result;
    }

    public function getOrderInfoBySn($orderSn): ?Result
    {
        $result = new Result();
        $orderProductsModel = OrderModel::getInstance();
        $orderInfo = $orderProductsModel->getOrderByOrderSn($orderSn);
        $result->setSuccessWithResult($orderInfo);
        return $result;
    }

    /**
     * @param int $orderId
     * @return Result|null根据订单Id获取商品详情
     */
    public function queryOrderProductList(int $orderId): ?Result
    {
        $result = new Result();
        $orderProductsModel = OrderProductModel::getInstance();
        $orderProductsInfo = $orderProductsModel->queryOrderProductList($orderId);
        $result->setSuccessWithResult($orderProductsInfo);
        return $result;
    }

    /**
     * @param int $userId
     * @param int $orderType
     * @return Result
     */
    public function getUserOrderList(int $userId, int $orderType): Result
    {
        $result = new Result();
        $orderListModel = OrderModel::getInstance();
        $orderListDTO = $orderListModel->queryOrderInfoList($userId, $orderType);
        $result->setSuccessWithResult($orderListDTO);
        return $result;

    }


    /**
     * @param OrderInfoDTO $orderInfoDTO
     * @return Result|null支付更新订单状态
     */

    public function updateOrder(OrderInfoDTO $orderInfoDTO): ?Result
    {
        $result = new Result();
        $orderModel = OrderModel::getInstance();
        $id = $orderModel->updateOrderInfoAfterPay($orderInfoDTO);
        $result->setSuccessWithResult($id);
        return $result;
    }


    /**
     * @param OrderInfoDTO $orderInfoDTO
     * @return Result 更新订单记录
     */
    public function updateOrderTransaction(OrderInfoDTO $orderInfoDTO): ?Result
    {
        $result = new Result();
        $orderTransactionDTO = new OrderTransactionDTO();
        \FUR_Core::copyProperties($orderInfoDTO, $orderTransactionDTO);
        $orderTransactionDTO->created_timestamp = time();
        $orderTransactionDTO->updated_timestamp = time();
        $orderTransactionModel = new OrderTransactionModel();
        $queryOrderTransactionRet = $orderTransactionModel->queryRecord($orderInfoDTO->order_sn);
        if ($queryOrderTransactionRet == null) {
            $id = $orderTransactionModel->insertRecord($orderTransactionDTO);
            $result->setSuccessWithResult($id);
        }
        return $result;
    }

    /**
     * @param OrderInfoDTO $orderInfoDTO
     * @return Result|null 取消订单,加库存
     */
    public function cancelOrder(int $orderId): ?Result
    {
        $result = new Result();
        $orderModel = OrderModel::getInstance();
        $bool = $orderModel->cancelOrder($orderId);
        $result->setSuccessWithResult($bool);
        return $result;
    }


    /**
     * @param OrderInfoDTO $orderInfoDTO
     * @return Result|null删除订单
     */

    public function deleteOrder(int $orderSn, int $userId): ?Result
    {
        $result = new Result();
        $orderModel = OrderModel::getInstance();

        $orderInfo = $orderModel->getOrderByOrderSn($orderSn);
        if ($orderInfo->user_id != $userId) {
            $result->setError(\Config_Error::ERR_DELETE_ORDER_FAIL);
            return $result;
        }
        $orderModel->deleteOrder($orderInfo->id);
        $result->setSuccessWithResult(true);
        return $result;

    }

    /**
     * @param OrderInfoDTO $orderInfoDTO
     * @return Result|null确认收货
     */

    public function confirmReceipt(int $orderId, int $userId): ?Result
    {
        $result = new Result();
        $orderModel = OrderModel::getInstance();
        $orderModel->confirmReceiveOrder($orderId, $userId);
        $result->setSuccessWithResult(true);
        return $result;


    }


    /**
     * @param int $orderSn
     * @return Result|null根据order_sn获取订单
     */
    public function getOrderInfoByOrderSn(string $orderSn): ?Result
    {
        $result = new Result();
        $orderInfoModel = OrderModel::getInstance();
        $getOrderInfoByOrderSnRet = $orderInfoModel->getOrderByOrderSn($orderSn);
        $result->setSuccessWithResult($getOrderInfoByOrderSnRet);
        return $result;
    }

    public function queryProductListByOrderId($orderId): ?Result
    {
        $result = new Result();

        $list = OrderProductModel::getInstance()->queryOrderProductList($orderId);
        $result->setSuccessWithResult($list);
        return $result;
    }

    /**
     * @param int $userId
     * @param int $skuId
     * @return Result获取商品评价
     */
    public function getUserReviewList(int $orderId): ?Result
    {
        $result = new Result();
        $productReviewModel = ProductReviewModel::getInstance();
        $productReviewModelRet = $productReviewModel->getUserReviewList($orderId);
        $result->setSuccessWithResult($productReviewModelRet);
        return $result;
    }

    /**
     * @param int $userId
     * @return Result|null
     * 获取订单提醒数量
     */

    public
    function getOrderRemindNum(int $userId): ?Result
    {
        $result = new Result();
        $orderModel = OrderModel::getInstance();
        $orderRemindNum = $orderModel->queryOrderRemindNum($userId);

        if ($orderRemindNum == null) {
            return null;
        }
        $result->setSuccessWithResult($orderRemindNum);
        return $result;

    }

    /**
     * @return Result|null
     * 获取未支付的订单
     */
    public
    function getOverTimeUnPaidOrders(): ?Result
    {
        $result = new Result();
        $orderModel = OrderModel::getInstance();
        $unPaidOrders = $orderModel->queryUnpaidOrders();
        $result->setSuccessWithResult($unPaidOrders);
        return $result;
    }


    /**
     * 更新订单状态为交易关闭
     */
    public
    function closeOrder(int $orderId): ?Result
    {
        $result = new Result();
        $orderModel = OrderModel::getInstance();
        $unPaidOrders = $orderModel->updateOrderClosed($orderId);
        if ($unPaidOrders == null) {
            return null;
        }
        $result->setSuccessWithResult($unPaidOrders);
        return $result;
    }


    public function getUserProcessingOrderCount(int $userId): ?Result
    {
        $result = new Result();
        $orderModel = OrderModel::getInstance();
        $count = $orderModel->getUserProcessingOrderCount($userId);
        $result->setSuccessWithResult($count);
        return $result;
    }


    /**
     * @param int $userId
     * @return Result|null
     * 获取用户已付款订单信息
     */
    public function getUserPaidOrderInfo(int $userId): ?Result
    {
        $result = new Result();
        $orderModel = OrderModel::getInstance();
        $orderInfoDTO = $orderModel->getOrderPaidByUserId($userId);
        $result->setSuccessWithResult($orderInfoDTO);
        return $result;
    }


    public function getTransactionByOrderSn(int $orderSn): ?Result
    {
        $result = new Result();
        $orderTransactionModel = OrderTransactionModel::getInstance();
        $orderTransactionDTO = $orderTransactionModel->queryRecord($orderSn);
        $result->setSuccessWithResult($orderTransactionDTO);
        return $result;
    }

    public function queryUserOrderCount(int $userId): ?Result
    {
        $result = new Result();
        $orderTransactionDTO = OrderModel::getInstance()->queryUserOrderCount($userId);
        $result->setSuccessWithResult($orderTransactionDTO);
        return $result;
    }

}