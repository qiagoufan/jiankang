<?php


namespace service\qiniu\impl;


use Exception;
use facade\request\aliyun\oss\UploadFileParams;
use facade\response\Result;
use FUR_Config;
use lib\aliyun\uploader\FileVodBase64Saver;
use Qiniu\Auth;
use Qiniu\Storage\UploadManager;

class QiniuOssServiceImpl
{
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function uploadBase64Image(?string $imageBase64Str, ?string $fileName = ''): ?Result
    {
        $result = new Result();
        $filePath = $this->saveBase64Media($imageBase64Str);
        if ($fileName == '') {
            $finfo = finfo_open(FILEINFO_EXTENSION);
            $fileExtensionS = finfo_file($finfo, $filePath);
            $fileExtension = explode("/", $fileExtensionS)[0];

            \FUR_Log::info($fileExtensionS);
            if ($filePath == null) {
                $result->setError(\Config_Error::ERR_ALIYUN_UPLOAD_BASE64_FILE_FAIL);
                return $result;
            }
            $fileName = md5(uniqid()) . ".$fileExtension";
        }


        $uploadOssFileParams = new UploadFileParams();

        $uploadOssFileParams->file_path = $filePath;
        $uploadOssFileParams->object_name = 'image/' . $fileName;
        return $this->uploadOssFile($uploadOssFileParams);

    }


    public function uploadOssFile(UploadFileParams $uploadOssFileParams): ?Result
    {
        $result = new Result();
        $accessKey = FUR_Config::get_qiniu_config('accessKey');
        $secretKey = FUR_Config::get_qiniu_config('secretKey');
        $bucket = FUR_Config::get_qiniu_config('bucket');
        $oss_domain = FUR_Config::get_qiniu_config('oss_domain');


        $auth = new Auth($accessKey, $secretKey);
        $token = $auth->uploadToken($bucket);
        $uploadMgr = new UploadManager();
        try {
            list($ret, $err) = $uploadMgr->putFile($token, $uploadOssFileParams->object_name, $uploadOssFileParams->file_path);
            $result->setSuccessWithResult($oss_domain . DIRECTORY_SEPARATOR . $ret['key']);
        } catch (Exception $e) {
            \FUR_Log::warn("qiniuoss fail", $e->getTraceAsString());
            \FUR_Log::warn("qiniuoss fail", $e->getMessage());
        }
        return $result;
    }

    private function saveBase64Media(string $base64Str)
    {
        // 解码base64,并保存文件到本地临时目录
        $saver = new FileVodBase64Saver();
        $localFilePath = $saver->saveFile($base64Str);
        if ($localFilePath === false) {
            return null;
        }

        return $localFilePath;
    }


}