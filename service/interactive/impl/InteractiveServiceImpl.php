<?php

namespace service\interactive\impl;

use facade\request\interactive\CommentContent;
use facade\request\interactive\CreateCommentParams;
use facade\request\interactive\CreateLikeParams;
use facade\request\interactive\CreateReplyParams;
use facade\request\interactive\QueryInteractiveCommentListParams;
use facade\request\interactive\QueryInteractiveReplyListParams;
use facade\request\interactive\ReplyContent;
use facade\request\interactive\ShareParams;
use facade\request\user\QueryUserLikeListParams;
use facade\response\Result;
use facade\service\interactive\InteractiveService;
use model\interactive\dto\InteractiveCommentDTO;
use model\interactive\dto\InteractiveLikeDTO;
use model\interactive\dto\InteractiveOverviewDTO;
use model\interactive\dto\InteractiveReplyDTO;
use model\interactive\InteractiveCommentModel;
use model\interactive\InteractiveLikeModel;
use model\interactive\InteractiveOverviewModel;
use model\interactive\InteractiveReplyModel;
use service\user\impl\UserServiceImpl;


class InteractiveServiceImpl implements InteractiveService
{


    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    /**
     * @param int $userId
     * @param CreateCommentParams $createCommentParams
     * @return Result|null 新增评论
     */
    public function createComment(int $userId, CreateCommentParams $createCommentParams): ?Result
    {

        $result = new Result();

        $interactiveCommentDTO = new InteractiveCommentDTO();
        $interactiveCommentDTO->created_timestamp = time();
        $interactiveCommentDTO->updated_timestamp = time();
        $interactiveCommentDTO->biz_type = $createCommentParams->biz_type;
        $interactiveCommentDTO->biz_id = $createCommentParams->biz_id;
        $commentContent = new CommentContent();
        $commentContent->text = $createCommentParams->content_text;
        $interactiveCommentDTO->comment_user_id = $userId;
        $interactiveCommentDTO->comment_content = json_encode($commentContent);
        $interactiveCommentDTO->comment_status = \Config_Const::FEED_STATUS_NORMAL;
        $interactiveCommentModel = InteractiveCommentModel::getInstance();
        $id = $interactiveCommentModel->insertRecord($interactiveCommentDTO);


        $interactiveOverviewRet = $this->queryInteractiveOverview($createCommentParams->biz_type, $createCommentParams->biz_id);
        if (!Result::isSuccess($interactiveOverviewRet)) {
            $this->insertOverviewRecord($createCommentParams->biz_type, $createCommentParams->biz_id);
            $interactiveOverviewRet = $this->queryInteractiveOverview($createCommentParams->biz_type, $createCommentParams->biz_id);
        }


        /** @var InteractiveOverviewDTO $interactiveOverviewDTO */
        $interactiveOverviewDTO = $interactiveOverviewRet->data;
        $interactiveOverviewDTO->comment_count = $interactiveOverviewDTO->comment_count + 1;
        InteractiveOverviewModel::getInstance()->updateRecord($interactiveOverviewDTO);


        $result->setSuccessWithResult($id);
        return $result;
    }

    /**
     * @param int $userId
     * @param int $commentId
     * @param int $isAdmin
     * @return Result|null
     */
    public function delComment($userId, $commentId, $bizId, $isAdmin = false): ?Result
    {
        $result = new Result();
        $interactiveCommentModel = InteractiveCommentModel::getInstance();
        $bool = $interactiveCommentModel->deleteRecord($userId, $commentId, $isAdmin);
        $result->setSuccessWithResult($bool);
        return $result;

    }


    private function insertOverviewRecord($bizType, $bizId): ?Result
    {
        $result = new Result();
        $interactiveOverviewModel = InteractiveOverviewModel::getInstance();
        $interactiveOverviewDTO = new InteractiveOverviewDTO();
        $interactiveOverviewDTO->comment_count = 0;
        $interactiveOverviewDTO->like_count = 0;
        $interactiveOverviewDTO->share_count = 0;
        $interactiveOverviewDTO->reply_count = 0;
        $interactiveOverviewDTO->biz_id = $bizId;
        $interactiveOverviewDTO->biz_type = $bizType;
        $interactiveOverviewDTO->updated_timestamp = time();
        $interactiveOverviewDTO->created_timestamp = time();
        $id = $interactiveOverviewModel->insertRecord($interactiveOverviewDTO);
        $result->setSuccessWithResult($id);
        return $result;
    }


    /**
     * @param int $commentId
     * @return Result|null
     */
    public function queryCommentById($commentId): ?Result
    {
        $result = new Result();
        $interactiveCommentModel = InteractiveCommentModel::getInstance();
        $id = $interactiveCommentModel->queryCommentById($commentId);
        $result->setSuccessWithResult($id);
        return $result;

    }

    /**
     * @param int $commentId
     * @return Result|null
     * 删除评论下面的回复
     */
    public function deleteReplyByComment(int $bizId, string $bizType, int $commentId): ?Result
    {
        $result = new Result();
        $interactiveReplyModel = InteractiveReplyModel::getInstance();
        $id = $interactiveReplyModel->deleteReplyRecordByCommentId($commentId);
        $result->setSuccessWithResult($id);
        return $result;
    }

    /**
     * @param int $userId
     * @param CreateReplyParams $createReplyParams
     * @return Result|null创建回复
     */
    public function createReply(int $userId, CreateReplyParams $createReplyParams): ?Result
    {
        $result = new Result();
        if ($userId == 0) {
            $result->setError(\Config_Error::ERR_USER_NOT_EXIST);
            return $result;
        }
        $replyContent = new ReplyContent();
        $replyContent->text = $createReplyParams->reply_content;
        $interactiveReplyDTO = new InteractiveReplyDTO();
        \FUR_Core::copyProperties($createReplyParams, $interactiveReplyDTO);
        $interactiveReplyDTO->reply_user_id = $userId;
        $interactiveReplyDTO->created_timestamp = time();
        $interactiveReplyModel = InteractiveReplyModel::getInstance();
        if ($createReplyParams->reply_to_id) {
            $interactiveReplyRecordDTO = $interactiveReplyModel->queryRecordById($createReplyParams->reply_to_id);
            $interactiveReplyDTO->reply_to_user_id = $interactiveReplyRecordDTO->reply_user_id;
        }
        $interactiveReplyDTO->reply_content = json_encode($replyContent);
        $interactiveReplyDTO->updated_timestamp = time();
        $interactiveReplyDTO->reply_status = \Config_Const::FEED_STATUS_NORMAL;
        $interactiveReplyModel = InteractiveReplyModel::getInstance();
        $id = $interactiveReplyModel->insertRecord($interactiveReplyDTO);

        //更新overview
        $interactiveOverviewModel = InteractiveOverviewModel::getInstance();
        $interactiveOverviewDTO = $interactiveOverviewModel->queryRecord($createReplyParams->biz_type, $createReplyParams->biz_id);
        $interactiveOverviewDTO->reply_count = $interactiveOverviewDTO->reply_count + 1;
        $interactiveOverviewModel->updateRecord($interactiveOverviewDTO);


        $result->setSuccessWithResult($id);
        return $result;
    }


    /**
     * @param QueryInteractiveCommentListParams $queryInteractiveCommentListParams
     * @return Result|null查询评论列表
     */
    public function queryInteractiveCommentList(QueryInteractiveCommentListParams $queryInteractiveCommentListParams): ?Result
    {
        $result = new Result();
        if ($queryInteractiveCommentListParams->biz_id == 0) {
            $result->setError(\Config_Error::ERR_INTERACTIVE_BIZ_ID_IS_NULL);
            return $result;
        }
        if ($queryInteractiveCommentListParams->biz_type == null) {
            $result->setError(\Config_Error::ERR_INTERACTIVE_BIZ_TYPE_IS_NULL);
            return $result;
        }
        $commentList = InteractiveCommentModel::getInstance()->queryCommentListByBizId($queryInteractiveCommentListParams);

        $result->setSuccessWithResult($commentList);
        return $result;


    }


    public function queryCommentCountByBizId(string $bizType, int $bizId): ?Result
    {
        $result = new Result();
        $interactiveCommentModel = InteractiveCommentModel::getInstance();
        $countRet = $interactiveCommentModel->queryCommentCountByBizId($bizType, $bizId);
        $result->setSuccessWithResult($countRet);
        return $result;
    }

    /**
     * @param $userId
     * @param $replyId
     * @param int $isAdmin
     * @return Result|null删除回复
     */
    public function delReply($userId, $replyId, $isAdmin = -1): ?Result
    {
        $result = new Result();
        if ($isAdmin == -1) {
            $isAdmin = 0;
            $queryRet = UserServiceImpl::getInstance()->queryWhiteUserById($userId);
            if (Result::isSuccess($queryRet)) {
                $isAdmin = 1;
            }
        }
        $interactiveReplyModel = InteractiveReplyModel::getInstance();
        $bool = $interactiveReplyModel->deleteRecord($replyId, $userId, $isAdmin);
        $result->setSuccessWithResult($bool);
        return $result;
    }


    /**
     * @param QueryInteractiveReplyListParams $queryInteractiveReplyListParams
     * @return Result|null
     * 获取回复列表
     */
    public function queryInteractiveReplyList(QueryInteractiveReplyListParams $queryInteractiveReplyListParams): ?Result
    {
        $result = new Result();
        if (!$queryInteractiveReplyListParams->comment_id) {
            $result->setError(\Config_Error::ERR_INTERACTIVE_COMMENT_ID_IS_NULL);
            return $result;
        }

        $interactiveReplyModel = InteractiveReplyModel::getInstance();
        $interactiveReplyArray = $interactiveReplyModel->queryReplyListByCommentId($queryInteractiveReplyListParams);
        $result->setSuccessWithResult($interactiveReplyArray);
        return $result;
    }


    /**
     * @param int $commentId
     * @return Result|null 根据id查询评论详情
     */
    public function queryCommentDetailById(int $commentId): ?Result
    {
        $result = new Result();
        if (!$commentId) {
            $result->setError(\Config_Error::ERR_INTERACTIVE_COMMENT_ID_IS_NULL);
            return $result;
        }
        $interactiveCommentModel = InteractiveCommentModel::getInstance();
        $interactiveCommentDTO = $interactiveCommentModel->queryCommentById($commentId);
        if ($interactiveCommentDTO == null) {
            $result->setError(\Config_Error::ERR_INTERACTIVE_COMMENT_INFO_IS_NOT_EXIST);
            return $result;
        }
        $result->setSuccessWithResult($interactiveCommentDTO);
        return $result;
    }

    /**
     * @param int $bizType
     * @param int $bizId
     * @return Result|null查询评论统计信息
     */
    public function queryInteractiveOverview(string $bizType, int $bizId): ?Result
    {
        $result = new Result();
        $interactiveOverviewModel = InteractiveOverviewModel::getInstance();
        /** @var InteractiveOverviewDTO $interactiveOverviewDTO */
        $interactiveOverviewDTO = $interactiveOverviewModel->queryRecord($bizType, $bizId);
        $result->setSuccessWithResult($interactiveOverviewDTO);
        return $result;

    }


    /**
     * @param int $commentId
     * @return Result|null
     * 根据评论查询回复数
     */
    public function queryReplyCountByComment(int $commentId, int $bizId, string $bizType): ?Result
    {
        $result = new Result();
        $interactiveReplyModel = InteractiveReplyModel::getInstance();
        $countRet = $interactiveReplyModel->queryReplyCount($commentId, $bizId, $bizType);
        $result->setSuccessWithResult($countRet);
        return $result;
    }


    /**
     * @param int $bizId
     * @param string $bizType
     * @param int $commentId
     * @return Result|null
     * 统计互动评论人数信息
     */
    public function queryCommentTotalCount(int $bizId, string $bizType): ?Result
    {
        $result = new Result();
        $interactiveCommentModel = InteractiveCommentModel::getInstance();
        $commentPeopleCount = $interactiveCommentModel->queryCommentPeopleCount($bizId, $bizType);
        $result->setSuccessWithResult($commentPeopleCount);
        return $result;

    }

    /**
     * @param int $replyId
     * @return Result|null
     * 根据id查询回复
     */
    public function queryReplyById(int $replyId): ?Result
    {
        $result = new Result();
        $interactiveReplyModel = InteractiveReplyModel::getInstance();
        $queryReplyRet = $interactiveReplyModel->queryRecordById($replyId);
        $result->setSuccessWithResult($queryReplyRet);
        return $result;
    }


    public function updateOverviewCount(int $bizId, string $bizType, string $numType, int $num): ?Result
    {
        $result = new Result();
        $interactiveOverviewModel = InteractiveOverviewModel::getInstance();
        /** @var InteractiveOverviewDTO $interactiveOverviewRet */
        $interactiveOverviewRet = $interactiveOverviewModel->queryRecord($bizType, $bizId);
        if ($interactiveOverviewRet == null) {
            $result->setSuccessWithResult(true);
            return $result;
        }

        if ($numType == 'reply') {
            $interactiveOverviewRet->reply_count = $interactiveOverviewRet->reply_count + $num;
            $interactiveOverviewModel->updateRecord($interactiveOverviewRet);
        } elseif ($numType == 'comment') {
            $interactiveOverviewRet->comment_count = $interactiveOverviewRet->comment_count + $num;
            $interactiveOverviewModel->updateRecord($interactiveOverviewRet);
        }
        $result->setSuccessWithResult(true);
        return $result;
    }


    /**
     * @param int $userId
     * @return Result|null
     * 获取用户id评论信息
     */
    public function getCommentInfoByUserId(int $userId): ?Result
    {
        $result = new Result();
        $interactiveCommentModel = InteractiveCommentModel::getInstance();
        $commentPeopleCount = $interactiveCommentModel->queryCommentCountByUserId($userId);
        $result->setSuccessWithResult($commentPeopleCount);
        return $result;
    }


    /**
     * @param int $userId
     * @return Result|null
     * 获取用户回复信息
     */
    public function getReplyInfoByUserId(int $userId): ?Result
    {
        $result = new Result();
        $interactiveReplyModel = InteractiveReplyModel::getInstance();
        $replyPeopleCount = $interactiveReplyModel->queryReplyCountByUserId($userId);
        $result->setSuccessWithResult($replyPeopleCount);
        return $result;

    }


    /**
     * @param CreateLikeParams $createLikeParams
     * @return Result|null
     * 新增点赞
     */
    public function addLike(CreateLikeParams $createLikeParams): ?Result
    {
        $result = new Result();
        $interactiveLikeModel = InteractiveLikeModel::getInstance();

        /**检查记录*/
        $recordRet = $this->hasLikeRecord($createLikeParams->user_id, $createLikeParams->biz_id, $createLikeParams->biz_type);
        if (Result::isSuccess($recordRet) && ($recordRet->data != null)) {
            $result->setSuccessWithResult(true);
            return $result;

        } else {
            $interactiveLikeDTO = new InteractiveLikeDTO();
            \FUR_Core::copyProperties($createLikeParams, $interactiveLikeDTO);

            $interactiveLikeDTO->like_biz_id = $createLikeParams->biz_id;
            $interactiveLikeDTO->like_biz_type = $createLikeParams->biz_type;
            $interactiveLikeDTO->created_timestamp = time();
            $interactiveLikeDTO->updated_timestamp = time();
            $interactiveLikeDTO->like_status = \Config_Const::FEED_STATUS_NORMAL;
            $insertId = $interactiveLikeModel->insertRecord($interactiveLikeDTO);

            /**
             * 消息通知
             */
//            if ($insertId != null) {
//
//                $messageServiceImpl = MessageServiceImpl::getInstance();
//                $messageInteractiveDTO = new MessageInteractiveDTO();
//                $messageInteractiveDTO->biz_id = $insertId;
//                $feedServiceImpl = FeedServiceImpl::getInstance();
//                $feedInfoRet = $feedServiceImpl->getFeedDetail($createLikeParams->biz_id);
//
//                /**给自己点赞不发送消息*/
//                if ($feedInfoRet->data->author_id != $createLikeParams->user_id) {
//                    if (Result::isSuccess($feedInfoRet)) {
//                        $imageRet = $feedServiceImpl->getFeedContentImage($feedInfoRet->data);
//                        if (Result::isSuccess($imageRet)) {
//                            $messageInteractiveDTO->image = $imageRet->data;
//                        }
//                        $messageInteractiveDTO->receive_user_id = $feedInfoRet->data->author_id;
//                        $messageInteractiveDTO->interactive_type = MessageInteractiveDTO::INTERACTIVE_TYPE_LIKE;
//                        $messageInteractiveDTO->created_timestamp = time();
//                        $messageInteractiveDTO->biz_type = FeedInfoDTO::FEED_BIZ_TYPE;
//                        $messageInteractiveDTO->send_user_id = $createLikeParams->user_id;
//                        $messageInteractiveDTO->read_status = MessageInteractiveDTO::READ_STATUS_FALSE;
//                        $messageServiceImpl->insertInteractiveMessage($messageInteractiveDTO);
//                        MessageInteractiveModel::getInstance()->updateInteractiveMessageCount($messageInteractiveDTO->receive_user_id);
//                    }
//                }
//            }

        }


        $interactiveOverviewRet = $this->queryInteractiveOverview($createLikeParams->biz_type, $createLikeParams->biz_id);

        if (!Result::isSuccess($interactiveOverviewRet)) {
            $this->insertOverviewRecord($createLikeParams->biz_type, $createLikeParams->biz_id);
            $interactiveOverviewRet = $this->queryInteractiveOverview($createLikeParams->biz_type, $createLikeParams->biz_id);
        }
        /** @var InteractiveOverviewDTO $interactiveOverviewDTO */
        $interactiveOverviewDTO = $interactiveOverviewRet->data;
        $interactiveOverviewDTO->like_count = $interactiveOverviewDTO->like_count + 1;
        InteractiveOverviewModel::getInstance()->updateRecord($interactiveOverviewDTO);

        $result->setSuccessWithResult(true);
        return $result;
    }


    /**
     * @return Result|null
     * 取消点赞
     */
    public function cancelLike(int $userId, ?int $bizId, ?string $bizType): ?Result
    {
        $result = new Result();
        $interactiveLikeModel = InteractiveLikeModel::getInstance();

        /**
         * 校验状态
         */
        $recordRet = $this->hasLikeRecord($userId, $bizId, $bizType);
        if (Result::isSuccess($recordRet)) {
            $interactiveLikeModel->deleteRecord($userId, $bizId, $bizType);
            $interactiveOverviewRet = $this->queryInteractiveOverview($bizType, $bizId);

            if (Result::isSuccess($interactiveOverviewRet)) {

                /** @var InteractiveOverviewDTO $interactiveOverviewDTO */
                $interactiveOverviewDTO = $interactiveOverviewRet->data;
                $interactiveOverviewDTO->like_count = $interactiveOverviewDTO->like_count - 1;
                InteractiveOverviewModel::getInstance()->updateRecord($interactiveOverviewDTO);
            }

            $result->setSuccessWithResult(true);
            return $result;

        }

        $result->setSuccessWithResult(false);
        return $result;


    }

    public function addShare(ShareParams $shareParams): ?Result
    {
        $result = new Result();
        $interactiveOverviewRet = $this->queryInteractiveOverview($shareParams->biz_type, $shareParams->biz_id);

        if (!Result::isSuccess($interactiveOverviewRet)) {
            $this->insertOverviewRecord($shareParams->biz_type, $shareParams->biz_id);
            $interactiveOverviewRet = $this->queryInteractiveOverview($shareParams->biz_type, $shareParams->biz_id);
        }
        /** @var InteractiveOverviewDTO $interactiveOverviewDTO */
        $interactiveOverviewDTO = $interactiveOverviewRet->data;
        $interactiveOverviewDTO->share_count = $interactiveOverviewDTO->share_count + 1;
        InteractiveOverviewModel::getInstance()->updateRecord($interactiveOverviewDTO);

        $result->setSuccessWithResult(true);
        return $result;
    }

    /**
     * @param int $userId
     * @param int $bizId
     * @return Result|null
     * 检查点赞记录
     */
    public function hasLikeRecord(int $userId, int $bizId, string $bizType): ?Result
    {
        $result = new Result();
        $interactiveLikeModel = InteractiveLikeModel::getInstance();
        $queryRet = $interactiveLikeModel->queryRecord($userId, $bizId, $bizType);
        $result->setSuccessWithResult($queryRet);

        return $result;

    }

    /**
     * @return Result|null
     * 是否点赞
     */
    public function isLike(int $userId, int $bizId, string $bizType): ?Result
    {
        $result = new Result();
        $isLike = false;
        if ($userId == 0) {
            $result->setSuccessWithResult($isLike);
            return $result;
        }
        $isLikeRecord = InteractiveLikeModel::getInstance()->queryRecord($userId, $bizId, $bizType);
        if ($isLikeRecord) {
            $isLike = true;
        }
        $result->setSuccessWithResult($isLike);
        return $result;
    }


    /**
     * @param int $bizId
     * @param string $bizType
     * @return Result|null
     */
    public function getLikeCount(int $bizId, string $bizType): ?Result
    {
        $result = new Result();
        $likeCount = 0;
        $interactiveOverviewDTO = InteractiveOverviewModel::getInstance()->queryRecord($bizType, $bizId);
        if ($interactiveOverviewDTO != null) {
            $likeCount = $interactiveOverviewDTO->like_count;
        }
        $result->setSuccessWithResult(intval($likeCount));
        return $result;

    }


    /**
     * @param QueryUserLikeListParams $queryUserLikeListParams
     * @return Result
     * 获取点赞列表
     */
    public function queryUserLikeList(QueryUserLikeListParams $queryUserLikeListParams): Result
    {
        $result = new Result();
        $index = $queryUserLikeListParams->index;
        $pageSize = $queryUserLikeListParams->page_size;
        $userId = $queryUserLikeListParams->user_id;
        $bizType = $queryUserLikeListParams->biz_type;

        $offset = $index * $pageSize;

        $interactiveLikeModel = InteractiveLikeModel::getInstance();
        $userLikeList = $interactiveLikeModel->queryUserLikeList($userId, $bizType, $offset, $pageSize);

        $result->setSuccessWithResult($userLikeList);
        return $result;
    }
}