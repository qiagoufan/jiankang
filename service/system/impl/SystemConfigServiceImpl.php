<?php


namespace service\system\impl;


use facade\response\Result;
use facade\service\system\SystemConfigService;
use lib\TimeHelper;
use model\system\dto\SystemConfigDTO;
use model\system\SystemConfigModel;

class SystemConfigServiceImpl implements SystemConfigService
{

    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function getConfigList(): Result
    {
        $result = new Result();
        $data = SystemConfigModel::getInstance()->queryConfigList();
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function updateConfig(string $key, string $value): Result
    {
        $result = new Result();
        $configDTO = new SystemConfigDTO();
        $configDTO->config_key = $key;
        $configDTO->config_value = $value;
        $configDTO->updated_timestamp = time();
        $data = SystemConfigModel::getInstance()->updateConfig($configDTO);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function getConfigValue(string $key): Result
    {
        $result = new Result();
        $data = SystemConfigModel::getInstance()->queryConfigDetail($key);
        $value = null;
        if ($data) {
            $value = $data->config_value;
        }
        $result->setSuccessWithResult($value);
        return $result;
    }

    public function setConfig(string $key, string $value): Result
    {
        $result = new Result();
        $data = SystemConfigModel::getInstance()->queryConfigDetail($key);
        if ($data == null) {
            $configDTO = new SystemConfigDTO();
            $configDTO->config_key = $key;
            $configDTO->config_value = $value;
            $configDTO->created_timestamp = TimeHelper::getTimeMs();
            SystemConfigModel::getInstance()->insertConfig($configDTO);
        } else {
            $configDTO = new SystemConfigDTO();
            $configDTO->config_key = $key;
            $configDTO->config_value = $value;
            $configDTO->updated_timestamp = TimeHelper::getTimeMs();
            SystemConfigModel::getInstance()->updateConfig($configDTO);

        }
        $result->setSuccessWithResult(true);
        return $result;
    }
}