<?php

namespace service\product\impl;

use facade\request\product\GetProductItemListParams;
use facade\request\product\GetProductDetailParams;
use facade\request\product\SelectProductSpecificParams;
use facade\response\Result;
use facade\service\product\ProductExtraInfoService;
use facade\service\product\ProductItemService;
use facade\service\product\ProductService;
use model\dto\ProductGalleryDTO;
use model\dto\ProductSkuDTO;
use model\product\dto\ProductExtraInfoDTO;
use model\product\ProductExtraInfoModel;
use model\product\ProductSkuModel;
use model\ProductSkuPicModel;
use model\ProductSkuPriceModel;
use model\ProductGalleryModel;
use model\product\ProductItemModel;

class ProductExtraInfoServiceImpl implements ProductExtraInfoService
{

    public static $_instance;


    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function getProductExtraInfo(int $itemId): ?Result
    {
        $result = new Result();
        $itemExtraInfo = ProductExtraInfoModel::getInstance()->getExtraInfo($itemId);
        $result->setSuccessWithResult($itemExtraInfo);
        return $result;
    }

    public function insertProductExtraInfo(ProductExtraInfoDTO $productExtraInfoDTO): ?Result
    {
        $result = new Result();
        $id = ProductExtraInfoModel::getInstance()->insertExtraInfo($productExtraInfoDTO);
        $result->setSuccessWithResult($id);
        return $result;
    }

    public function updateProductExtraInfo(ProductExtraInfoDTO $productExtraInfoDTO): ?Result
    {
        $result = new Result();
        $rows = ProductExtraInfoModel::getInstance()->updateExtraInfo($productExtraInfoDTO);
        $result->setSuccessWithResult($rows);
        return $result;
    }
}