<?php

namespace service\product\impl;

use facade\request\product\GetProductDetailParams;
use facade\request\product\GetProductItemListParams;
use facade\response\Result;
use facade\service\product\ProductItemService;
use model\product\ProductItemModel;

class ProductItemServiceImpl implements ProductItemService
{

    public static $_instance;


    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function getProductItemDetail(GetProductDetailParams $getProductDetailParams): ?Result
    {
        $result = new Result();
        if ($getProductDetailParams->item_id == 0) {
            $result->setError(\Config_Error::ERR_PRODUCT_ID_IS_NULL);
            return $result;
        }
        $productItemDTO = ProductItemModel::getInstance()->getProductItem($getProductDetailParams->item_id);
        if ($productItemDTO == null) {
            $result->setError(\Config_Error::ERR_PRODUCT_ITEM_IS_NOT_EXIST);
            return $result;
        }

        $result->setSuccessWithResult($productItemDTO);
        return $result;
    }


    public function getProductList(GetProductItemListParams $getProductItemListParams): ?Result
    {
        $result = new Result();

        $productItemList = ProductItemModel::getInstance()->queryPublishProductItemList($getProductItemListParams);
        $result->setSuccessWithResult($productItemList);
        return $result;
    }

    public function queryProductByIdList(?array $idList): ?Result
    {
        $result = new Result();
        $productItemList = [];
        if ($idList == null) {
            $result->setSuccessWithResult($productItemList);
            return $result;
        }

        $productItemList = ProductItemModel::getInstance()->queryProductByIdList($idList);
        $result->setSuccessWithResult($productItemList);
        return $result;
    }

}