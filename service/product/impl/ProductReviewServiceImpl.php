<?php

namespace service\product\impl;

use business\product\ProductDetailConversionUtil;
use facade\request\admin\SearchProductReviewListParams;
use facade\request\product\GetProductReviewListParams;
use facade\request\product\GetRelatedReviewParams;
use facade\request\product\GetProductReviewParams;
use facade\request\product\GetProductDetailParams;
use facade\request\product\SendProductReviewParams;
use facade\response\Result;
use facade\service\product\ProductReviewService;
use model\dto\OrderInfoDTO;
use model\dto\ProductReviewDTO;
use model\dto\ProductSkuDTO;
use model\ProductReviewModel;
use model\OrderModel;
use service\aliyun\AliyunBase;
use facade\request\admin\AuditProductReviewParams;

class ProductReviewServiceImpl implements ProductReviewService
{

    const DEFAULT_PAGE_SIZE = 10;


    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function sendReview(SendProductReviewParams $sendProductReviewParams): ?Result
    {
        $result = new Result();
        if ($sendProductReviewParams->sku_id == 0) {
            $result->setError(\Config_Error::ERR_REVIEW_SKU_NOT_EXIST);
            return $result;
        }


        $productSkuDetailParams = new GetProductDetailParams();


        if ($sendProductReviewParams->user_id == 0) {
            $result->setError(\Config_Error::ERR_REVIEW_USER_ID_FAIL);
            return $result;
        }
        if ($sendProductReviewParams->content == '') {
            $result->setError(\Config_Error::ERR_REVIEW_CONTENT_IS_NULL);
            return $result;
        }

        if (mb_strlen($sendProductReviewParams->content) > 150) {
            $result->setError(\Config_Error::ERR_REVIEW_CONTENT_TOO_LONG);
            return $result;
        }

        $orderInfoDTO = OrderModel::getInstance()->queryOrderInfoById($sendProductReviewParams->order_id);

        $productInfo = OrderModel::getInstance()->queryOrderProductByOrderId($sendProductReviewParams->order_id);

        $productSkuDetailParams->sku_id = $productInfo->sku_id;
        if ($orderInfoDTO == null) {
            $result->setError(\Config_Error::ERR_REVIEW_CONTENT_ORDER_IS_NULL);
            return $result;
        }

        if ($orderInfoDTO->user_id != $sendProductReviewParams->user_id) {
            $result->setError(\Config_Error::ERR_REVIEW_CONTENT_ORDER_NO_RIGHT);
            return $result;
        }

        if ($orderInfoDTO->pay_status != OrderInfoDTO::PAY_STATUS_FINISHED) {
            $result->setError(\Config_Error::ERR_REVIEW_CONTENT_ORDER_NOT_RIGHT_STATUS);
            return $result;
        }

        $productSkuDetailRet = ProductServiceImpl::getInstance()->getProductDetail($productSkuDetailParams);

        if (!Result::isSuccess($productSkuDetailRet)) {
            $result->setError(\Config_Error::ERR_REVIEW_SKU_NOT_EXIST);
            return $result;
        }


        /** @var ProductSkuDTO $productSkuDTO */
        $productSkuDTO = $productSkuDetailRet->data;


        $productReviewDTO = new ProductReviewDTO();

        \FUR_Core::copyProperties($sendProductReviewParams, $productReviewDTO);
        $productReviewDTO->customer_id = $sendProductReviewParams->user_id;
        $productReviewDTO->item_id = $productSkuDTO->item_id;
        $productReviewDTO->sku_id = $productSkuDTO->id;
        $productReviewDTO->audit_status = ProductReviewDTO::AUDIT_STATUS_CHECKED;
        if (SERVER_TYPE == 'development') {
            $productReviewDTO->audit_status = ProductReviewDTO::AUDIT_STATUS_CHECKED;
        }
        $productReviewDTO->image_list = json_encode(AliyunBase::replaceCdnUrl($productReviewDTO->image_list));
        $productReviewDTO->audit_time = 0;
        $productReviewDTO->order_id = $sendProductReviewParams->order_id;
        $productReviewDTO->created_timestamp = time();
        $productReviewDTO->specification_detail = ProductDetailConversionUtil::conversionSpecification($productSkuDTO);

        $productReviewModel = ProductReviewModel::getInstance();
        $reviewId = $productReviewModel->insertRecord($productReviewDTO);
        $productReviewModel->updateReviewCount($productSkuDTO->item_id);
        //更新订单状态
        OrderModel::getInstance()->updateOrderStatusAfterComment($sendProductReviewParams->order_id);

        $result->setSuccessWithResult($reviewId);
        return $result;
    }

    public function getReviewInfo(int $reviewId): ?Result
    {
        $result = new  Result();
        $productReviewDTO = ProductReviewModel::getInstance()->queryProductReviewInfo($reviewId);
        if ($productReviewDTO == null) {
            $result->setData(null);
            return $result;
        }
        $productReviewDTO->image_list = json_decode($productReviewDTO->image_list, true);

        $result->setSuccessWithResult($productReviewDTO);
        return $result;
    }


    public function getProductReviewList(?GetProductReviewListParams $getProductReviewListParams): ?Result
    {
        $result = new Result();
        $index = $getProductReviewListParams->index;
        $reviewId = $getProductReviewListParams->review_id;

        $productReviewModel = ProductReviewModel::getInstance();

        $reviewList = [];
        if ($index == 0 && $reviewId != 0) {
            $productReviewDTO = $productReviewModel->queryProductReviewInfo($reviewId);
            if (!$productReviewDTO == null) {
                $productReviewDTO->image_list = json_decode($productReviewDTO->image_list, true);
                array_push($reviewList, $productReviewDTO);
            }
        }


        $skuId = $getProductReviewListParams->sku_id;
        $itemId = $getProductReviewListParams->item_id;
        $pageSize = $getProductReviewListParams->page_size == 0 ? self::DEFAULT_PAGE_SIZE : $getProductReviewListParams->page_size;
        $offset = $getProductReviewListParams->index * $pageSize;
        $reviewRecordList = [];
        if ($itemId != 0) {
            $reviewRecordList = $productReviewModel->queryProductReviewListByItemId($itemId, $offset, $pageSize);
        } elseif ($skuId != 0) {
            $reviewRecordList = $productReviewModel->queryProductReviewListBySkuId($skuId, $offset, $pageSize);
        }
        if (!empty($reviewRecordList)) {
            /**
             * 如果有指定商品,那需要先过滤掉
             * @var  int $k
             * @var  ProductReviewDTO $productReviewDTO
             */

            foreach ($reviewRecordList as $k => $productReviewDTO) {
                if ($productReviewDTO->id == $reviewId) {
                    unset($reviewRecordList[$k]);
                    continue;
                }

                $productReviewDTO->image_list = json_decode($productReviewDTO->image_list, true);
            }
            $reviewList = array_merge($reviewList, $reviewRecordList);
        }
        $result->setSuccessWithResult($reviewList);
        return $result;
    }

    public function getReviewCount(int $itemId): ?Result
    {
        $result = new Result();

        $count = ProductReviewModel::getInstance()->getReviewCount($itemId);
        $result->setSuccessWithResult($count);
        return $result;
    }

    public function getRelatedReview(GetRelatedReviewParams $getRelatedReviewParams): ?Result
    {
        $result = new Result();
        $productReviewModel = ProductReviewModel::getInstance();
        $skuId = $getRelatedReviewParams->sku_id;
        $itemId = $getRelatedReviewParams->item_id;
        $pageSize = 1;
        $offset = 0;
        $reviewRecordList = [];
        if ($skuId != 0) {
            $reviewRecordList = $productReviewModel->queryProductReviewListBySkuId($skuId, $offset, $pageSize);
        } elseif ($itemId != 0) {
            $reviewRecordList = $productReviewModel->queryProductReviewListByItemId($itemId, $offset, $pageSize);
        }
        if (!empty($reviewRecordList)) {
            $result->setSuccessWithResult($reviewRecordList[0]);
        }
        return $result;
    }


    /**
     * @param SearchProductReviewListParams $searchUserListParams
     * @return Result|null
     * 获取所有商品评价列表
     */
    public function getProductAdminReviewList(SearchProductReviewListParams $searchReviewListParams): ?Result
    {
        $result = new Result();
        $productReviewModel = ProductReviewModel::getInstance();
        $array = $productReviewModel->getProductReviewList($searchReviewListParams);
        $result->setSuccessWithResult($array);
        return $result;
    }


    /**
     * @return Result|null
     * 获取商品评价列表总数
     */
    public function queryProductReviewListCount(): ?Result
    {
        $result = new Result();
        $productReviewModel = ProductReviewModel::getInstance();
        $countRet = $productReviewModel->queryProductReviewCount();
        $result->setSuccessWithResult($countRet);
        return $result;
    }


    /**
     * @param AuditProductReviewParams $auditProductReviewParams
     * @return Result|null
     * 审核商品评价
     */
    public function auditProductReview(AuditProductReviewParams $auditProductReviewParams): ?Result
    {
        $result = new Result();
        $productReviewModel = ProductReviewModel::getInstance();
        $bool = $productReviewModel->updateProductReview($auditProductReviewParams);
        $result->setSuccessWithResult($bool);
        return $result;

    }


}