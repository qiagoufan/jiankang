<?php

namespace service\product\impl;

use facade\request\product\GetProductItemListParams;
use facade\request\product\GetProductDetailParams;
use facade\request\product\SelectProductSpecificParams;
use facade\response\Result;
use facade\service\product\ProductService;
use model\product\dto\ProductItemDTO;
use model\product\dto\ProductSkuDTO;
use model\product\ProductSkuModel;
use model\ProductSkuPicModel;
use model\ProductSkuPriceModel;
use model\ProductGalleryModel;
use model\product\ProductItemModel;

class ProductServiceImpl implements ProductService
{

    public static $_instance;


    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    /**
     * @param GetProductDetailParams $getProductDetailParams
     * @return Result|null根据商品id获取详情
     */
    public function getProductDetail(GetProductDetailParams $getProductDetailParams): ?Result
    {
        $result = new Result();
        if ($getProductDetailParams->sku_id == 0 && $getProductDetailParams->item_id == 0) {
            $result->setError(\Config_Error::ERR_PRODUCT_ID_IS_NULL);
            return $result;
        }
        if ($getProductDetailParams->sku_id == 0) {
            $itemInfo = ProductItemModel::getInstance()->getProductItem($getProductDetailParams->item_id);
            if ($itemInfo == null) {
                $result->setError(\Config_Error::ERR_PRODUCT_ITEM_IS_NOT_EXIST);
                return $result;
            }
            $getProductDetailParams->sku_id = $itemInfo->default_sku_id;
        }

        $productSkuDTO = $this->getProductSkuDetailById($getProductDetailParams->sku_id);
        if ($productSkuDTO == null) {
            $result->setError(\Config_Error::ERR_PRODUCT_SKU_NOT_EXIST);
            return $result;
        }
        $result->setSuccessWithResult($productSkuDTO);
        return $result;
    }

    public function queryProductSkuList(int $itemId): ?Result
    {
        $result = new Result();
        $productSkuModel = ProductSkuModel::getInstance();
        $data = $productSkuModel->queryProductSkuListByItemId($itemId);
        $result->setSuccessWithResult($data);
        return $result;
    }

    private function getProductSkuDetailById($skuId)
    {
        $productSkuModel = ProductSkuModel::getInstance();
        return $productSkuModel->querySkuDetailFromDB($skuId);
    }


    /**
     * @param SelectProductSpecificParams $selectProductSpecificParams
     * @return Result|null商品规格选择器
     */
    public function selectProductSpecific(SelectProductSpecificParams $selectProductSpecificParams): ?Result
    {
        $result = new Result();
        $productSkuModel = ProductSkuModel::getInstance();
        $productSkuDTO = $productSkuModel->queryProductRest($selectProductSpecificParams->item_id, $selectProductSpecificParams->indexes);
        if (!$productSkuDTO || $productSkuDTO->stock == null) {
            $result->setError(\Config_Error::ERR_PRODUCT_SPECIFIC_NOT_IN_DB);
            return $result;
        }

        $productSkuDTO = $this->getProductSkuDetailById($productSkuDTO->id);
        $result->setSuccessWithResult($productSkuDTO);
        return $result;
    }


    /**
     * @param int $skuId
     * @return Result|null获取商品主图信息
     */
    public function getProductGalleryPic(int $skuId): ?Result
    {
        $result = new Result();
        $productGalleryModel = ProductGalleryModel::getInstance();
        $productSkuPicDTO = $productGalleryModel->queryProductGalleryPic($skuId);
        if (!$productSkuPicDTO) {
            $result->setError(\Config_Error::ERR_PRODUCT_SPECIFIC_NOT_EXIST);
            return $result;
        }
        $result->setSuccessWithResult($productSkuPicDTO);
        return $result;

    }

    /**
     * @param int $skuId
     * @return Result|null
     */
    public function getProductSkuDetail(int $skuId): ?Result
    {
        $result = new Result();
        $data = ProductSkuModel::getInstance()->querySkuDetailFromDB($skuId);
        $result->setSuccessWithResult($data);
        return $result;

    }

    /**
     * @param ProductSkuDTO $productSkuDTO
     * @return Result|null 获取商品价格
     * @deprecated
     */
    public function getProductSkuPrice(ProductSkuDTO $productSkuDTO): ?Result
    {
        $result = new Result();

        return $result;

    }


    public function getProductPriceBySkuId(int $skuId): ?Result
    {
        $result = new Result();
        $productSkuPriceModel = ProductSkuPriceModel::getInstance();
        $productSkuPriceDTO = $productSkuPriceModel->queryProductSkuPrice($skuId);
        if (!$productSkuPriceDTO) {
            $result->setError(\Config_Error::ERR_PRODUCT_PRICE_IS_NOT_EXIST);
            return $result;
        }
        $result->setSuccessWithResult($productSkuPriceDTO);
        return $result;
    }

    /**
     * @param ProductSkuDTO $productSkuDTO
     * @return Result|null获取商品价格区间
     */
    public function getProductSkuPriceRange(ProductSkuDTO $productSkuDTO): ?Result
    {
        $result = new Result();
        $productSkuPriceModel = ProductSkuPriceModel::getInstance();
        $productSkuPriceDTO = $productSkuPriceModel->queryProductItemPriceRange($productSkuDTO->item_id);
        if (!$productSkuPriceDTO) {
            $result->setError(\Config_Error::ERR_PRODUCT_PRICE_IS_NOT_EXIST);
            return $result;
        }

        $result->setSuccessWithResult($productSkuPriceDTO);
        return $result;
    }

    /**
     * @param int|null $itemId
     * @return Result|null获取商品规格
     */
    public function getProductItem(?int $itemId): ?Result
    {
        $result = new Result();
        $productItemModel = ProductItemModel::getInstance();
        $productItemDTO = $productItemModel->getProductItem($itemId);
        if ($productItemDTO == null) {
            $result->setError(\Config_Error::ERR_PRODUCT_SPECIFICATIONS_IS_NOT_EXIST);
            return $result;
        }
        $result->setSuccessWithResult($productItemDTO);
        return $result;

    }

    /**
     * @param int $skuId
     * @return Result|null清除商品缓存
     */
    public function clearProductDetailCache(int $skuId): ?Result
    {
        $result = new Result();
        $productSkuModel = ProductSkuModel::getInstance();
        $bool = $productSkuModel->delSkuInfoDTOCache($skuId);
        $result->setSuccessWithResult($bool);
        return $result;
    }


    public function getDistinctProductSkuList(int $offset, int $pageSize): ?Result
    {
        $result = new Result();
        $productSkuModel = ProductSkuModel::getInstance();
        $skuDTOList = $productSkuModel->getDistinctProductSkuList($offset, $pageSize);
        $result->setSuccessWithResult($skuDTOList);
        return $result;

    }


    public function getDistinctProductItemList(int $offset, int $pageSize): ?Result
    {
        $result = new Result();
        $itemDTOList = ProductItemModel::getInstance()->getIndexDefaultProductItemList($offset, $pageSize);
        $result->setSuccessWithResult($itemDTOList);
        return $result;

    }

    /**
     * @param int $skuId
     * @param int $stock
     * @return Result|null更新库存
     */
    public function updateProductItemStockSale(ProductItemDTO $productItemDTO): ?Result
    {
        $result = new Result();
        $productItemModel = ProductItemModel::getInstance();
        $productSkuRet = $productItemModel->updateItemStockSale($productItemDTO);
        $result->setSuccessWithResult($productSkuRet);
        return $result;
    }


    public function getSkuByStatus(int $publish_status): ?Result
    {
        $result = new Result();
        $productSkuModel = ProductSkuModel::getInstance();
        $onlineRet = $productSkuModel->getSkuByStatus($publish_status);
        $result->setSuccessWithResult($onlineRet);
        return $result;
    }

    public function getIndexPageProductItemList(int $index): ?Result
    {
        $result = new Result();
        $mod = 7 + date("w");

        $itemDTOList = ProductItemModel::getInstance()->getIndexPageProductItemList($index, $mod);
        $result->setSuccessWithResult($itemDTOList);
        return $result;
    }

    public function getProductItemList(GetProductItemListParams $getProductItemListParams): ?Result
    {
        $result = new Result();
        $itemDTOList = ProductItemModel::getInstance()->queryPublishProductItemList($getProductItemListParams);
        $result->setSuccessWithResult($itemDTOList);
        return $result;
    }
}