<?php

namespace service\media\impl;

use facade\request\media\MediaImageParams;
use facade\response\Result;
use facade\response\system\media\GetImageInfoResp;
use facade\service\media\ImageService;
use model\dto\media\ImageInfoDTO;
use model\media\MediaImageModel;
use service\aliyun\impl\VodServiceImpl;

class ImageServiceImpl implements ImageService
{
    const DEFAULT_IMAGE_RESIZE_PATH = '?x-oss-process=image/resize,m_lfit,h_1000,w_1000/format,jpg';

    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    /**
     * @inheritDoc
     */
    public function createImage(MediaImageParams $mediaImageParams): ?Result
    {
        $result = new Result();
        if ($mediaImageParams == null) {
            $result->setSuccessWithResult(\Config_Error::ERR_MEDIA_CREATE_IMAGE_PARAMS_IS_NULL);
            return $result;
        }
        if ($mediaImageParams->image_id == null) {
            $result->setSuccessWithResult(\Config_Error::ERR_MEDIA_CREATE_IMAGE_PARAMS_IS_NOT_COMPLETE);
            return $result;
        }

        $imageModel = MediaImageModel::getInstance();
        $imageInfoDTO = $imageModel->queryImageByImageId($mediaImageParams->image_id);
        if ($imageInfoDTO != null) {
            $result->setError(\Config_Error::ERR_MEDIA_CREATE_IMAGE_HAS_EXISTS);
            return $result;
        }
        $imageInfoRet = VodServiceImpl::getInstance()->getImageInfo($mediaImageParams->image_id);
        if (!Result::isSuccess($imageInfoRet)) {
            $result->setSuccessWithResult(\Config_Error::ERR_MEDIA_CREATE_IMAGE_FILE_NOT_EXISTS);
            return $result;
        }

        /** @var GetImageInfoResp $imageInfoResp */
        $imageInfoResp = $imageInfoRet->data;

        $mediaImageDTO = new ImageInfoDTO();

        \FUR_Core::copyProperties($imageInfoResp, $mediaImageDTO);

        $mediaImageDTO->created_timestamp = time();
        $mediaImageDTO->user_id = $mediaImageParams->user_id;
        $mediaImageDTO->check_status = ImageInfoDTO::CHECK_STATUS_NEW;
        $mediaImageDTO->original_url = $imageInfoResp->image_url;
        $mediaImageDTO->image_url = $imageInfoResp->image_url . self::DEFAULT_IMAGE_RESIZE_PATH;
        $id = $imageModel->insertImageInfo($mediaImageDTO);
        $result->setSuccessWithResult($id);
        return $result;
    }

    /**
     * @inheritDoc
     */
    public function getImageInfoById(?int $id): ?Result
    {
        $result = new Result();
        $imageModel = MediaImageModel::getInstance();
        $imageInfoDTO = $imageModel->queryImageById($id);
        if ($imageInfoDTO == null) {
            $result->setError(\Config_Error::ERR_MEDIA_CREATE_IMAGE_FILE_NOT_IN_DB);
            return $result;
        }
        $result->setSuccessWithResult($imageInfoDTO);
        return $result;
    }

    public function getImageInfo(MediaImageParams $mediaImageParams): ?Result
    {
        $result = new Result();
        $imageModel = MediaImageModel::getInstance();
        $imageId = $mediaImageParams->image_id;
        $imageInfoDTO = $imageModel->queryImageByImageId($imageId);
        if ($imageInfoDTO == null) {
            $this->createImage($mediaImageParams);
            $imageInfoDTO = $imageModel->queryImageByImageId($imageId);
        }
        if ($imageInfoDTO == null) {
            $result->setError(\Config_Error::ERR_MEDIA_QUERY_IMAGE_FILE_NOT_FOUND);
            return $result;
        }
        $result->setSuccessWithResult($imageInfoDTO);
        return $result;
    }
}