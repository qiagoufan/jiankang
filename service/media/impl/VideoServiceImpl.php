<?php

namespace service\media\impl;


use facade\request\media\MediaVideoParams;
use facade\response\Result;
use facade\response\system\media\GetPlayInfoResp;
use facade\response\system\media\GetVideoInfoResp;
use facade\service\media\VideoService;
use model\dto\media\VideoInfoDTO;
use model\media\MediaVideoModel;
use service\aliyun\impl\VodServiceImpl;


class VideoServiceImpl implements VideoService
{
    /** @var int 最大转码时间 */
    const MAX_TRANSCODE_TIME = 18000;

    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    /**
     * @inheritDoc
     */
    public function createVideo(MediaVideoParams $mediaVideoParams): ?Result
    {
        $result = new Result();
        if ($mediaVideoParams == null) {
            $result->setError(\Config_Error::ERR_MEDIA_CREATE_VIDEO_PARAMS_IS_NULL);
            return $result;
        }
        if ($mediaVideoParams->video_id == null) {
            $result->setError(\Config_Error::ERR_MEDIA_CREATE_VIDEO_PARAMS_IS_NOT_COMPLETE);
            return $result;
        }


        $videoInfoDTO = new VideoInfoDTO();
        $videoInfoDTO->video_id = $mediaVideoParams->video_id;
        $videoInfoDTO->created_timestamp = time();
        $videoInfoDTO->user_id = $mediaVideoParams->user_id;
        $this->refreshVideoInfoDTO($videoInfoDTO);
        $videoModel = MediaVideoModel::getInstance();
        $id = $videoModel->insertVideoInfo($videoInfoDTO);
        $result->setSuccessWithResult($id);
        return $result;
    }

    /**
     * @inheritDoc
     */
    public function getVideoInfo(MediaVideoParams $mediaVideoParams): ?Result
    {
        $result = new Result();
        $videoModel = MediaVideoModel::getInstance();
        $videoId = $mediaVideoParams->video_id;
        $videoInfoDTO = $videoModel->queryVideoByVideoId($videoId);
        if ($videoInfoDTO == null) {
            $this->createVideo($mediaVideoParams);
            $videoInfoDTO = $videoModel->queryVideoByVideoId($videoId);
        }
        if ($videoInfoDTO == null) {
            $result->setError(\Config_Error::ERR_MEDIA_QUERY_VIDEO_FILE_NOT_FOUND);
            return $result;
        }


        $result->setSuccessWithResult($videoInfoDTO);
        return $result;
    }

    public function updateVideoCheckStatus(string $videoId, int $checkStatus): ?Result
    {
        // TODO: Implement updateVideoCheckStatus() method.
    }

    public function updateVideoTranscodeStatus(string $videoId, int $transcodeStatus): ?Result
    {
        // TODO: Implement updateVideoTranscodeStatus() method.
    }

    public function updateVideoInfo(string $videoId, int $checkStatus): ?Result
    {
        // TODO: Implement updateVideoInfo() method.
    }

    public function getVideoInfoById(int $id): ?Result
    {
        $now = time();
        $result = new Result();
        $videoModel = MediaVideoModel::getInstance();
        $videoInfoDTO = $videoModel->queryVideoById($id);

        if ($videoInfoDTO == null) {
            $result->setError(\Config_Error::ERR_MEDIA_QUERY_VIDEO_NOT_EXIST);
            return $result;
        }

        /** 如果数据没取到,并且还在可接受的转码时间内再查询一次 */
        if ($videoInfoDTO->video_width == 0 || $videoInfoDTO->cover_url == null) {
            $this->refreshVideoInfoDTO($videoInfoDTO);
            $videoInfoDTO->updated_timestamp = $now;
            $videoModel->updateVideo($videoInfoDTO);
        }

        $result->setSuccessWithResult($videoInfoDTO);
        return $result;
    }

    private function refreshVideoInfoDTO(VideoInfoDTO &$videoInfoDTO)
    {
        $videoId = $videoInfoDTO->video_id;
        $vodServiceImpl = VodServiceImpl::getInstance();
        $videoInfoRet = $vodServiceImpl->getVideoInfo($videoId);
        if (!Result::isSuccess($videoInfoRet)) {
            return;
        }
        /**
         * 获取短视频播放信息
         */
        /** @var GetVideoInfoResp $videoInfo */
        $videoInfo = $videoInfoRet->data;
        $playInfoRet = VodServiceImpl::getInstance()->getPlayInfo($videoId);
        if (!Result::isSuccess($playInfoRet)) {
            return;
        }

        /** @var GetPlayInfoResp $playInfoResp */
        $playInfoResp = $playInfoRet->data;

        \FUR_Core::copyProperties($playInfoResp, $videoInfoDTO);
        $videoInfoDTO->cover_url = $videoInfo->cover_url;
        $videoInfoDTO->check_status = VideoInfoDTO::CHECK_STATUS_NEW;
        $videoInfoDTO->original_play_url = $playInfoResp->od_play_url;
        $videoInfoDTO->original_file_size = $playInfoResp->od_video_size;
        $videoInfoDTO->transcode_status = VideoInfoDTO::TRANSCODE_STATUS_NEW;

        if ($playInfoResp->sd_play_url != null) {
            $videoInfoDTO->play_url = $playInfoResp->sd_play_url;
        } else {
            $videoInfoDTO->play_url = $playInfoResp->od_play_url;
        }
        if ($playInfoResp->sd_video_size != 0) {
            $videoInfoDTO->play_file_size = $playInfoResp->sd_video_size;
        } else {
            $videoInfoDTO->play_file_size = $playInfoResp->od_video_size;
        }
        $videoInfoDTO->video_width = $playInfoResp->od_video_width;
        $videoInfoDTO->video_height = $playInfoResp->od_video_height;
    }
}