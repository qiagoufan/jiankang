<?php

namespace service\media\impl;

use facade\request\aliyun\oss\UploadFileParams;
use facade\response\Result;

class LocalStorageServiceImpl
{

    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    /**
     * @inheritDoc
     */
    public function localStorage(UploadFileParams $uploadFileParams): ?Result
    {
        $result = new Result();

        $path = APP_PATH . $uploadFileParams->object_name;

        $data = move_uploaded_file($uploadFileParams->file_path, $path);
        if ($data) {
            $fileUrl = $_SERVER['REQUEST_SCHEME'] . "://" . $_SERVER['SERVER_NAME'] . DIRECTORY_SEPARATOR . $uploadFileParams->object_name;
            $result->setSuccessWithResult($fileUrl);
        }
        return $result;
    }


}