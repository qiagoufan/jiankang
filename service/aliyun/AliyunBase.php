<?php

namespace service\aliyun;

use AlibabaCloud\Client\AlibabaCloud;

class AliyunBase
{
    public $accessKeyId;
    public $accessKeySecret;
    public $regionId;


    public function __construct()
    {
        $accessKeyId = \FUR_Config::get_aliyun_config('accessKeyId');
        $accessKeySecret = \FUR_Config::get_aliyun_config('accessKeySecret');
        $regionId = \FUR_Config::get_aliyun_config('regionId');
        AlibabaCloud::accessKeyClient($accessKeyId, $accessKeySecret)->regionId($regionId)->asDefaultClient();
    }

    public static function replaceCdnUrl($data, bool $isVod = false)
    {
        if ($data == null) {
            return null;
        }
        if (is_array($data) && !empty($data)) {
            $cdnUrlArray = [];
            foreach ($data as $url) {
                $cdnUrl = self::replaceAliyunCdnUrl($url, $isVod);
                array_push($cdnUrlArray, $cdnUrl);
            }
            return $cdnUrlArray;

        } elseif (is_object($data)) {
            $objectVars = get_object_vars($data);

            if (empty($objectVars)) {
                return $data;
            }
            foreach ($objectVars as $k => $v) {
                if (strpos($v, 'oss')) {
                    $data->$k = self::replaceAliyunCdnUrl($v);
                }
            }
            return $data;

        } else {
            return self::replaceAliyunCdnUrl($data, $isVod);
        }

        return null;
    }

    private static function replaceAliyunCdnUrl(string $url, bool $isVod = false): ?string
    {
        $url = trim($url);
        if ($isVod) {
            $cdnDomain = \FUR_Config::get_aliyun_config('vodCdnDomain');
        } else {
            $cdnDomain = \FUR_Config::get_aliyun_config('cdnDomain');
        }
        $urlInfo = parse_url(trim($url));
        $cdnUrl = 'https://' . $cdnDomain . $urlInfo['path'];
        if (isset($urlInfo['query'])) {
            $cdnUrl .= $urlInfo['query'];
        }
        return $cdnUrl;
    }

    /**
     * 带image的对象
     * @param $imageObjectArray
     * @param bool $isVod
     * @return false|string|null
     */
    public static function replaceImageObjectArrayCdnUrl($imageObjectArray, bool $isVod = false)
    {
        if ($imageObjectArray == null || empty($imageObjectArray)) {
            return $imageObjectArray;
        }

        foreach ($imageObjectArray as $imageObject) {
            $imageCdnUrl = self::replaceAliyunCdnUrl($imageObject->image, $isVod);
            $imageObject->image = $imageCdnUrl;
        }
        return $imageObjectArray;
    }

}