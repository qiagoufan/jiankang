<?php


namespace service\aliyun\impl;


use AlibabaCloud\Client\AlibabaCloud;
use AlibabaCloud\Client\Exception\ClientException;
use AlibabaCloud\Client\Exception\ServerException;
use AlibabaCloud\Client\Profile\DefaultProfile;
use AlibabaCloud\Green\Green;
use AlibabaCloud\Vod\Vod;
use facade\request\aliyun\media\CreateUploadImageParams;
use facade\request\aliyun\media\CreateUploadVideoParams;
use facade\request\aliyun\media\RefreshUploadVideoParams;
use facade\response\Result;
use facade\response\system\media\CreateUploadImageResp;
use facade\response\system\media\CreateUploadVideoResp;
use facade\response\system\media\RefreshUploadVideoResp;
use facade\service\aliyun\AcsService;
use facade\service\aliyun\MediaService;
use service\aliyun\AliyunBase;

class AcsServiceImpl implements AcsService
{

    public function __construct()
    {
        $accessKeyId = \FUR_Config::get_aliyun_config('accessKeyId');
        $accessKeySecret = \FUR_Config::get_aliyun_config('accessKeySecret');
        AlibabaCloud::accessKeyClient($accessKeyId, $accessKeySecret)->regionId('cn-beijing')->asDefaultClient();
    }

    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function checkContent(string $content): ?Result
    {
        $result = new Result();
        $blockStatus = true;
        $data = [];
        $data['scenes'] = 'antispam';

        $task = array('dataId' => uniqid(),
            'content' => $content
        );
        $data['tasks'] = $task;
        try {
            $response = AlibabaCloud::roa()->product('Green')
                // ->scheme('https') // https | http
                ->version('2018-05-09')
                ->pathPattern('/green/text/scan')
                ->method('POST')
                ->options([
                    'query' => [

                    ],
                ])
                ->body(json_encode($data))
                ->request();

            $data = $response->get('data');
            if ($data[0]['results'][0]['suggestion'] != 'block') {
                $blockStatus = false;
            } elseif ($data[0]['results'][0]['rate'] < '70') {
                $blockStatus = false;
            }
        } catch (\Exception $e) {
            \FUR_Log::info('blockStatus', $e);
        }
        $result->setSuccessWithResult($blockStatus);
        return $result;
    }

    public function checkImage(array $imageUrlList): ?Result
    {
        $result = new Result();
        $blockStatus = false;
        if ($imageUrlList == null || empty($imageUrlList)) {
            $result->setSuccessWithResult($blockStatus);
            return $result;

        }


        $data = [];
        $data['scenes'] = ['porn', 'terrorism', 'ad', 'live', 'qrcode'];
        $data['tasks'] = [];
        foreach ($imageUrlList as $imageUrl) {
            $task = array('dataId' => uniqid(),
                'url' => $imageUrl
            );
            array_push($data['tasks'], $task);
        }

        try {
            $response = AlibabaCloud::roa()->product('Green')
                // ->scheme('https') // https | http
                ->version('2018-05-09')
                ->pathPattern('/green/image/scan')
                ->method('POST')
                ->options([
                    'query' => [

                    ],
                ])
                ->body(json_encode($data))
                ->request();

            $data = $response->get('data');
            if (!empty($data)) {
                $result->setSuccessWithResult($blockStatus);
                return $result;
            }
            /**
             * 针对图片,只要一张图命中任何一个规则都返回false
             */
            foreach ($data as $taskRet) {
                $checkResultList = $taskRet['results'];
                if (empty($checkResultList)) {
                    continue;
                }
                foreach ($checkResultList as $checkResult) {
                    if ($checkResult['suggestion'] == 'block') {
                        $blockStatus = true;
                        $result->setSuccessWithResult($blockStatus);
                        return $result;
                    }
                }

            }
        } catch (\Exception $e) {
            \FUR_Log::info('blockStatus', $e);
        }
        $result->setSuccessWithResult($blockStatus);
        return $result;
    }

    public function checkVideo(): ?Result
    {
        // TODO: Implement checkVideo() method.
    }
}