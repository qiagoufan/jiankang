<?php


namespace service\aliyun\impl;


use AlibabaCloud\Client\AlibabaCloud;
use AlibabaCloud\Client\Exception\ClientException;
use AlibabaCloud\Client\Exception\ServerException;
use AlibabaCloud\Vod\Vod;
use facade\request\aliyun\media\CreateUploadImageParams;
use facade\request\aliyun\media\CreateUploadVideoParams;
use facade\request\aliyun\media\RefreshUploadVideoParams;
use facade\response\Result;
use facade\response\system\media\CreateUploadImageResp;
use facade\response\system\media\CreateUploadVideoResp;
use facade\response\system\media\RefreshUploadVideoResp;
use facade\service\aliyun\MediaService;

class MediaServiceImpl implements MediaService
{

    const  ALIYUN_CLIENT_NAME = 'AliyunVodClient';
    const IMAGE_EXT_LIST = ['png', 'jpg', 'jpeg', 'gif'];
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function __construct()
    {
        $regionId = 'cn-beijing';
        $accessKeyId = \FUR_Config::get_aliyun_config('accessKeyId');
        $accessKeySecret = \FUR_Config::get_aliyun_config('accessKeySecret');
        try {
            AlibabaCloud::accessKeyClient($accessKeyId, $accessKeySecret)
                ->regionId($regionId)
                ->connectTimeout(1)
                ->timeout(3)
                ->name(self::ALIYUN_CLIENT_NAME);
        } catch (ClientException $e) {
        }
    }

    public function createUploadVideo(CreateUploadVideoParams $createUploadVideoParams): ?Result
    {
        $result = new Result();
        $fileSize = $createUploadVideoParams->file_size;
        $fileName = $createUploadVideoParams->file_name;
        $title = $createUploadVideoParams->title;
        try {
            $response = Vod::v20170321()->createUploadVideo()->client(self::ALIYUN_CLIENT_NAME)
                ->withFileSize($fileSize)
                ->withFileName($fileName)
                ->withTitle($title)
                ->request();
        } catch (\Exception $e) {
            $result->setError(\Config_Error::ERR_ALIYUN_CREATE_UPLOAD_VIDEO_FAIL);
            return $result;
        }

        if ($response->isSuccess() == false || $response->getReasonPhrase() != 'OK') {
            $result->setError(\Config_Error::ERR_ALIYUN_CREATE_UPLOAD_VIDEO_FAIL);
            return $result;

        }
        /**@see https://help.aliyun.com/document_detail/55407.html */
        $data = $response->get();
        $createUploadVideoResp = new CreateUploadVideoResp();
        $createUploadVideoResp->videoId = $data['VideoId'];
        $createUploadVideoResp->requestId = $data['RequestId'];
        $createUploadVideoResp->uploadAddress = $data['UploadAddress'];
        $createUploadVideoResp->uploadAuth = $data['UploadAuth'];
        $result->setSuccessWithResult($createUploadVideoResp);
        return $result;

    }

    public function refreshUploadVideo(RefreshUploadVideoParams $refreshUploadVideoParams): ?Result
    {
        $result = new Result();
        $videoId = $refreshUploadVideoParams->video_id;
        $result = new Result();
        try {
            $response = Vod::v20170321()->refreshUploadVideo()->client(self::ALIYUN_CLIENT_NAME)
                ->withVideoId($videoId)
                ->request();
        } catch (\Exception $e) {
            $result->setError(\Config_Error::ERR_ALIYUN_REFRESH_UPLOAD_VIDEO_FAIL);
            return $result;
        }
        if ($response->isSuccess() == false || $response->getReasonPhrase() != 'OK') {
            $result->setError(\Config_Error::ERR_ALIYUN_REFRESH_UPLOAD_VIDEO_FAIL);
            return $result;

        }
        /**@see https://help.aliyun.com/document_detail/55408.html */
        $data = $response->get();
        $refreshUploadVideoResp = new RefreshUploadVideoResp();
        $refreshUploadVideoResp->videoId = $data['VideoId'];
        $refreshUploadVideoResp->requestId = $data['RequestId'];
        $refreshUploadVideoResp->uploadAddress = $data['UploadAddress'];
        $refreshUploadVideoResp->uploadAuth = $data['UploadAuth'];
        $result->setSuccessWithResult($refreshUploadVideoResp);
        return $result;

    }

    public function createUploadImage(CreateUploadImageParams $createUploadImageParams): ?Result
    {
        $result = new Result();
        $imageType = $createUploadImageParams->image_type;
        $imageExt = strtolower($createUploadImageParams->image_ext);
        if (!in_array($imageExt, self::IMAGE_EXT_LIST)) {
            $result->setError(\Config_Error::ERR_ALIYUN_CREATE_UPLOAD_IMAGE_EXT_FAIL);
            return $result;
        }

        try {
            $response = Vod::v20170321()->createUploadImage()->client(self::ALIYUN_CLIENT_NAME)
                ->withImageType($imageType)
                ->withImageExt($imageExt)
                ->request();
        } catch (\Exception $e) {
            $result->setError(\Config_Error::ERR_ALIYUN_CREATE_UPLOAD_IMAGE_FAIL);
            return $result;
        }

        if ($response->isSuccess() == false || $response->getReasonPhrase() != 'OK') {
            $result->setError(\Config_Error::ERR_ALIYUN_CREATE_UPLOAD_IMAGE_FAIL);
            return $result;

        }
        /**@see https://help.aliyun.com/document_detail/55619.html */
        $data = $response->get();
        $createUploadImageResp = new CreateUploadImageResp();
        $createUploadImageResp->imageId = $data['ImageId'];
        $createUploadImageResp->requestId = $data['RequestId'];
        $createUploadImageResp->uploadAddress = $data['UploadAddress'];
        $createUploadImageResp->uploadAuth = $data['UploadAuth'];
        $createUploadImageResp->imageURL = $data['ImageURL'];
        $createUploadImageResp->fileURL = $data['FileURL'];
        $result->setSuccessWithResult($createUploadImageResp);
        return $result;
    }
}