<?php


namespace service\aliyun\impl;


use AlibabaCloud\Sts\Sts;
use Exception;
use facade\request\aliyun\oss\UploadFileParams;
use facade\response\Result;
use facade\response\system\OSSResp;
use facade\service\aliyun\OssService;
use FUR_Config;
use lib\aliyun\uploader\FileVodBase64Saver;
use lib\aliyun\uploader\AliyunVodDownloader;
use lib\aliyun\uploader\AliyunVodError;
use lib\aliyun\uploader\UploadImageRequest;
use OSS\Core\OssException;
use OSS\OssClient;
use service\aliyun\AliyunBase;

class AliyunOssServiceImpl implements OssService
{
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function ossEnable()
    {

        $endpoint = FUR_Config::get_aliyun_config('endpoint');
    }

    public function uploadOssFile(UploadFileParams $uploadOssFileParams): ?Result
    {
        $result = new Result();
        $endpoint = FUR_Config::get_aliyun_config('endpoint');
        $bucket = FUR_Config::get_aliyun_config('bucket');


        $accessKeyId = \FUR_Config::get_aliyun_config('accessKeyId');
        $accessKeySecret = \FUR_Config::get_aliyun_config('accessKeySecret');
        try {
            $ossClient = new OssClient($accessKeyId, $accessKeySecret, $endpoint);
            $uploadResult = $ossClient->uploadFile($bucket, $uploadOssFileParams->object_name, $uploadOssFileParams->file_path);
            $ossUrl = $uploadResult['oss-request-url'];
            $result->setSuccessWithResult($ossUrl);
        } catch (OssException $e) {
            $result->setError(\Config_Error::ERR_ALIYUN_UPLOAD_OSS_FILE_FAIL);
        }
        return $result;
    }


    public function uploadWebImage(?string $fileUrl): ?Result
    {

        $uploadImageRequest = new UploadImageRequest($fileUrl);
        // 下载文件
        $uploadImageRequest = $this->downloadWebMedia($uploadImageRequest);

        $uploadOssFileParams = new UploadFileParams();
        $uploadOssFileParams->file_path = $uploadImageRequest->getFilePath();
        $uploadOssFileParams->object_name = 'webImage/' . $uploadImageRequest->getTitle();
        $uploadRet = $this->uploadOssFile($uploadOssFileParams);
        $uploadRet->data = AliyunBase::replaceCdnUrl($uploadRet->data);
        return $uploadRet;
    }

    public function uploadBase64Image(?string $imageBase64Str): ?Result
    {
        $result = new Result();
        $filePath = $this->saveBase64Media($imageBase64Str);

        $finfo = finfo_open(FILEINFO_EXTENSION);
        $fileExtensionS = finfo_file($finfo, $filePath);
        $fileExtension = explode("/", $fileExtensionS)[0];

        if ($filePath == null) {
            $result->setError(\Config_Error::ERR_ALIYUN_UPLOAD_BASE64_FILE_FAIL);
            return $result;
        }
        $uploadOssFileParams = new UploadFileParams();

        $uploadOssFileParams->file_path = $filePath;
        $uploadOssFileParams->object_name = 'image/' . md5(uniqid()) . ".$fileExtension";
        $uploadRet = $this->uploadOssFile($uploadOssFileParams);
        $uploadRet->data = AliyunBase::replaceCdnUrl($uploadRet->data);
        return $uploadRet;
    }

    private function downloadWebMedia(UploadImageRequest $request)
    {
        // 下载视频文件到本地临时目录
        $downloader = new AliyunVodDownloader();
        $localFileName = sprintf("%s.%s", md5($request->getFileName()), $request->getMediaExt());
        $webFilePath = $request->getFilePath();;
        $localFilePath = $downloader->downloadFile($webFilePath, $localFileName);
        if ($localFilePath === false) {
            throw new Exception("file download fail: " . $webFilePath, AliyunVodError::VOD_ERR_FILE_DOWNLOAD);
        }

        // 重新设置上传请求对象
        $request->setFilePath($localFilePath);

        return $request;
    }

    private function saveBase64Media(string $base64Str)
    {
        // 解码base64,并保存文件到本地临时目录
        $saver = new FileVodBase64Saver();
        $localFilePath = $saver->saveFile($base64Str);
        if ($localFilePath === false) {
            return null;
        }

        return $localFilePath;
    }

    public function uploadFullOssFile(UploadFileParams $uploadOssFileParams): ?Result
    {
        $result = new Result();
        $endpoint = FUR_Config::get_aliyun_config('endpoint');
        $bucket = FUR_Config::get_aliyun_config('bucket');


        $accessKeyId = \FUR_Config::get_aliyun_config('accessKeyId');
        $accessKeySecret = \FUR_Config::get_aliyun_config('accessKeySecret');

        $ossClient = new OssClient($accessKeyId, $accessKeySecret, $endpoint);
        try {
            $uploadResult = $ossClient->uploadFile($bucket, $uploadOssFileParams->object_name, $uploadOssFileParams->file_path);
            $ossUrl = $uploadResult;
            $result->setSuccessWithResult($ossUrl);
        } catch (OssException $e) {
            $result->setError(\Config_Error::ERR_ALIYUN_UPLOAD_OSS_FILE_FAIL);
        }
        return $result;
    }


    public function getOssResp(): ?Result
    {
        $endpoint = FUR_Config::get_aliyun_config('endpoint');
        $bucket = FUR_Config::get_aliyun_config('bucket');
        $ossRoleArn = FUR_Config::get_aliyun_config('ossRoleArn');


        $result = new  Result();

        $stsResult = Sts::v20150401()->assumeRole()
            ->withRoleArn($ossRoleArn)
            ->withRoleSessionName('ipxmall-oss')
            ->withPolicy($this->getOssRight())
            ->connectTimeout(2)->connectTimeout(2)->request();

        if ($stsResult->isSuccess() == false) {
            $result->setError([$stsResult->getStatusCode(), $stsResult->getReasonPhrase()]);
            return $result;
        }
        $data_array = $stsResult->get();
        $ossResp = new OSSResp();
        $ossResp->accessKeyId = $data_array['Credentials']['AccessKeyId'];
        $ossResp->accessKeySecret = $data_array['Credentials']['AccessKeySecret'];
        $ossResp->expiration = $data_array['Credentials']['Expiration'];
        $ossResp->securityToken = $data_array['Credentials']['SecurityToken'];
        $ossResp->bucket = $bucket;
        $ossResp->endpoint = $endpoint;

        $result->setSuccessWithResult($ossResp);
        return $result;
    }

    private function getOssRight(): ?string
    {
        return '{"Statement":[{"Action":["oss:*"],"Effect":"Allow","Resource":"*"}],"Version":"1"}';
    }


}