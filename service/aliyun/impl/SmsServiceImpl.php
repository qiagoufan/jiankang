<?php


namespace service\aliyun\impl;

use AlibabaCloud\Client\AlibabaCloud;
use facade\request\aliyun\SendSmsParams;
use facade\response\Result;
use facade\service\aliyun\SmsService;
use service\aliyun\AliyunBase;

class SmsServiceImpl extends AliyunBase implements SmsService
{
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function sendSms(SendSmsParams $sendSmsParams): ?Result
    {
        $result = new Result();
        $smsSignName = \FUR_Config::get_aliyun_config('smsSignName');
        try {
            $smsResult = AlibabaCloud::rpc()
                ->product('Dysmsapi')
                // ->scheme('https') // https | http
                ->version('2017-05-25')
                ->action('SendSms')
                ->method('POST')
                ->host('dysmsapi.aliyuncs.com')
                ->options([
                    'query' => [
                        'PhoneNumbers' => $sendSmsParams->phone,
                        'SignName' => $smsSignName,
                        'TemplateCode' => $sendSmsParams->templateCode,
                        'Action' => 'SendSms',
                        'OutId' => $sendSmsParams->outId,
                        'TemplateParam' => $sendSmsParams->templateParam
                    ],
                ])
                ->request();
            if ($smsResult->isSuccess()) {
                $result->setError(\Config_Error::ERR_ALIYUN_SEND_SMS_FAIL);
                return $result;
            }
            $result->setSuccessWithResult(true);
        } catch (\Exception $e) {
            $result->setError(\Config_Error::ERR_ALIYUN_SEND_SMS_FAIL);
            return $result;
        }
        return $result;
    }
}