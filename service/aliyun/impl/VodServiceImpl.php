<?php


namespace service\aliyun\impl;


use AlibabaCloud\Client\AlibabaCloud;
use AlibabaCloud\Client\Exception\ClientException;
use AlibabaCloud\Vod\Vod;
use facade\request\aliyun\media\CreateUploadImageParams;
use facade\request\aliyun\media\CreateUploadVideoParams;
use facade\request\aliyun\media\RefreshUploadVideoParams;
use facade\response\Result;
use facade\response\system\media\CreateUploadImageResp;
use facade\response\system\media\CreateUploadVideoResp;
use facade\response\system\media\GetImageInfoResp;
use facade\response\system\media\GetPlayInfoResp;
use facade\response\system\media\GetVideoInfoResp;
use facade\response\system\media\RefreshUploadVideoResp;
use facade\service\aliyun\VodService;

class VodServiceImpl implements VodService
{

    const DEFAULT_SCENE = 'feed';
    const  ALIYUN_CLIENT_NAME = 'AliyunVodClient';
    const IMAGE_EXT_LIST = ['png', 'jpg', 'jpeg', 'gif'];


    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function __construct()
    {
        $regionId = 'cn-beijing';
        $accessKeyId = \FUR_Config::get_aliyun_config('accessKeyId');
        $accessKeySecret = \FUR_Config::get_aliyun_config('accessKeySecret');
        try {
            AlibabaCloud::accessKeyClient($accessKeyId, $accessKeySecret)
                ->regionId($regionId)
                ->connectTimeout(1)
                ->timeout(3)
                ->name(self::ALIYUN_CLIENT_NAME);
        } catch (ClientException $e) {
        }
    }

    public function createUploadVideo(CreateUploadVideoParams $createUploadVideoParams): ?Result
    {
        $result = new Result();
        $fileSize = $createUploadVideoParams->file_size;
        $fileName = $createUploadVideoParams->file_name;
        $title = $createUploadVideoParams->title;
        $workflowId = $this->getWorkFlowId($createUploadVideoParams->media_scene);
        $coverUrl = $createUploadVideoParams->cover_url;
        try {
            $createUploadVideoRequest = Vod::v20170321()->createUploadVideo()->client(self::ALIYUN_CLIENT_NAME)
                ->withFileSize($fileSize)
                ->withFileName($fileName);
            if ($title != null) {
                $createUploadVideoRequest->withTitle($title);
            }
            if ($workflowId != null) {
                $createUploadVideoRequest->withWorkflowId($workflowId);
            }
            if ($coverUrl != null) {
                $createUploadVideoRequest->withCoverURL($coverUrl);
            }

            $response = $createUploadVideoRequest->request();
        } catch (\Exception $e) {
            \FUR_Log::warn('createUploadVideo exception', $e->getMessage());
            $result->setError(\Config_Error::ERR_ALIYUN_CREATE_UPLOAD_VIDEO_FAIL);
            return $result;
        }

        \FUR_Log::debug('createUploadVideo get', $response->get());
        \FUR_Log::debug('createUploadVideo isSuccess', $response->isSuccess());
        \FUR_Log::debug('createUploadVideo getReasonPhrase', $response->getReasonPhrase());
        if ($response->isSuccess() == false || $response->getReasonPhrase() != 'OK') {
            $result->setError(\Config_Error::ERR_ALIYUN_CREATE_UPLOAD_VIDEO_FAIL);
            return $result;

        }
        /**@see https://help.aliyun.com/document_detail/55407.html */
        $data = $response->get();
        $createUploadVideoResp = new CreateUploadVideoResp();
        $createUploadVideoResp->videoId = $data['VideoId'];
        $createUploadVideoResp->requestId = $data['RequestId'];
        $createUploadVideoResp->uploadAddress = $data['UploadAddress'];
        $createUploadVideoResp->uploadAuth = $data['UploadAuth'];
        $result->setSuccessWithResult($createUploadVideoResp);
        return $result;

    }

    public function refreshUploadVideo(RefreshUploadVideoParams $refreshUploadVideoParams): ?Result
    {
        $result = new Result();
        $videoId = $refreshUploadVideoParams->video_id;
        $result = new Result();
        try {
            $response = Vod::v20170321()->refreshUploadVideo()->client(self::ALIYUN_CLIENT_NAME)
                ->withVideoId($videoId)
                ->request();
        } catch (\Exception $e) {
            $result->setError(\Config_Error::ERR_ALIYUN_REFRESH_UPLOAD_VIDEO_FAIL);
            return $result;
        }
        if ($response->isSuccess() == false || $response->getReasonPhrase() != 'OK') {
            $result->setError(\Config_Error::ERR_ALIYUN_REFRESH_UPLOAD_VIDEO_FAIL);
            return $result;

        }
        /**@see https://help.aliyun.com/document_detail/55408.html */
        $data = $response->get();
        $refreshUploadVideoResp = new RefreshUploadVideoResp();
        $refreshUploadVideoResp->videoId = $data['VideoId'];
        $refreshUploadVideoResp->requestId = $data['RequestId'];
        $refreshUploadVideoResp->uploadAddress = $data['UploadAddress'];
        $refreshUploadVideoResp->uploadAuth = $data['UploadAuth'];
        $result->setSuccessWithResult($refreshUploadVideoResp);
        return $result;

    }

    public function createUploadImage(CreateUploadImageParams $createUploadImageParams): ?Result
    {
        $result = new Result();
        $imageType = $createUploadImageParams->image_type;
        $imageExt = strtolower($createUploadImageParams->image_ext);
        if (!in_array($imageExt, self::IMAGE_EXT_LIST)) {
            $result->setError(\Config_Error::ERR_ALIYUN_CREATE_UPLOAD_IMAGE_EXT_FAIL);
            return $result;
        }

        try {
            $response = Vod::v20170321()->createUploadImage()->client(self::ALIYUN_CLIENT_NAME)
                ->withImageType($imageType)
                ->withImageExt($imageExt)
                ->request();
        } catch (\Exception $e) {
            $result->setError(\Config_Error::ERR_ALIYUN_CREATE_UPLOAD_IMAGE_SERVICE_FAIL);
            return $result;
        }

        if ($response->isSuccess() == false || $response->getReasonPhrase() != 'OK') {
            $result->setError(\Config_Error::ERR_ALIYUN_CREATE_UPLOAD_IMAGE_SERVICE_FAIL);
            return $result;

        }
        /**@see https://help.aliyun.com/document_detail/55619.html */
        $data = $response->get();
        $createUploadImageResp = new CreateUploadImageResp();
        $createUploadImageResp->imageId = $data['ImageId'];
        $createUploadImageResp->requestId = $data['RequestId'];
        $createUploadImageResp->uploadAddress = $data['UploadAddress'];
        $createUploadImageResp->uploadAuth = $data['UploadAuth'];
        $createUploadImageResp->imageURL = $data['ImageURL'];
        $createUploadImageResp->fileURL = $data['FileURL'];
        $result->setSuccessWithResult($createUploadImageResp);
        return $result;
    }

    private function getWorkFlowId(?string $scene): ?string
    {
        $workFlowId = '';
        if ($scene == null) {
            $scene = self::DEFAULT_SCENE;
        }

        if ($scene == self::DEFAULT_SCENE) {
            $workFlowId = \FUR_Config::get_aliyun_config('feedWorkflowId');
        }

        return $workFlowId;
    }

    public function getVideoInfo(?string $videoId): ?Result
    {
        $result = new Result();
        if ($videoId == null) {
            $result->setError(\Config_Error::ERR_ALIYUN_VIDEO_ID_IS_NULL);
            return $result;
        }
        try {
            $response = Vod::v20170321()->getVideoInfo()->client(self::ALIYUN_CLIENT_NAME)
                ->withVideoId($videoId)
                ->request();

            /** @see https://help.aliyun.com/document_detail/89742.html */
            if ($response->isSuccess() == false || $response->getReasonPhrase() != 'OK') {
                $result->setError(\Config_Error::ERR_ALIYUN_GET_VIDEO_INFO_FAIL);
                return $result;
            }
            $data = $response->get();
            $getVideoInfoResp = new GetVideoInfoResp();
            $getVideoInfoResp->video_id = $data['Video']['VideoId'];
            $getVideoInfoResp->video_size = $data['Video']['Size'];

            if (isset($data['Video']['Snapshots']) && isset($data['Video']['Snapshots']['Snapshot']) && !empty($data['Video']['Snapshots']['Snapshot'])) {
                $getVideoInfoResp->cover_url = $data['Video']['Snapshots']['Snapshot'][0];
            }
            $result->setSuccessWithResult($getVideoInfoResp);
        } catch (\Exception $e) {
            $result->setError(\Config_Error::ERR_ALIYUN_GET_VIDEO_INFO_FAIL);
            return $result;
        }

        return $result;
    }

    public function getImageInfo(?string $imageId): ?Result
    {
        $result = new Result();
        if ($imageId == null) {
            $result->setError(\Config_Error::ERR_ALIYUN_IMAGE_ID_IS_NULL);
            return $result;
        }

        try {
            $response = Vod::v20170321()->getImageInfo()->client(self::ALIYUN_CLIENT_NAME)
                ->withImageId($imageId)
                ->request();

            /** @see https://help.aliyun.com/document_detail/89742.html */
            if ($response->isSuccess() == false || $response->getReasonPhrase() != 'OK') {
                $result->setError(\Config_Error::ERR_ALIYUN_GET_IMAGE_INFO_FAIL);
                return $result;
            }
            $data = $response->get();

            \FUR_Log::debug("imageinfo:", json_encode($data));
            $getImageInfoResp = new GetImageInfoResp();
            $getImageInfoResp->image_id = $imageId;
            $getImageInfoResp->file_size = $data['ImageInfo']['Mezzanine']['FileSize'];
            if (isset($data['ImageInfo']['Mezzanine']['Height'])) {
                $getImageInfoResp->image_height = $data['ImageInfo']['Mezzanine']['Height'];
            }
            if (isset($data['ImageInfo']['Mezzanine']['Width'])) {
                $getImageInfoResp->image_width = $data['ImageInfo']['Mezzanine']['Width'];
            }
            $getImageInfoResp->image_url = $data['ImageInfo']['URL'];
            $getImageInfoResp->file_url = $this->getBaseUrl($data['ImageInfo']['Mezzanine']['FileURL']);
            $getImageInfoResp->status = $data['ImageInfo']['Status'];

            \FUR_Log::debug("getImageInfoResp:", json_encode($getImageInfoResp));
            $result->setSuccessWithResult($getImageInfoResp);
        } catch (\Exception $e) {
            $result->setError(\Config_Error::ERR_ALIYUN_GET_IMAGE_INFO_FAIL);
            return $result;
        }

        return $result;

    }

    public function getPlayInfo(?string $videoId): ?Result
    {
        $result = new Result();
        if ($videoId == null) {
            $result->setError(\Config_Error::ERR_ALIYUN_VIDEO_ID_IS_NULL);
            return $result;
        }
        try {
            $response = Vod::v20170321()->getPlayInfo()->client(self::ALIYUN_CLIENT_NAME)
                ->withVideoId($videoId)
                ->request();
        } catch (\Exception $e) {
            \FUR_Log::warn('getPlayInfo fail', $e->getMessage());
            $result->setError(\Config_Error::ERR_ALIYUN_GET_PLAY_INFO_FAIL);
            return $result;
        }
        /** @see https://help.aliyun.com/document_detail/89742.html */
        if ($response->isSuccess() == false || $response->getReasonPhrase() != 'OK') {
            $result->setError(\Config_Error::ERR_ALIYUN_GET_PLAY_INFO_FAIL);
            return $result;
        }

        $data = $response->get();
        $getPlayInfoResp = new GetPlayInfoResp();
        \FUR_Log::debug('getPlayInfo:', json_encode($response->get()));
        try {
            if (isset($data['Video']['CoverURL'])) {
                $getPlayInfoResp->cover_url = $data['Video']['CoverURL'];
            }
            if (isset($data['VideoBase']['Title'])) {
                $getPlayInfoResp->title = $data['VideoBase']['Title'];
            }
            if (isset($data['VideoBase']['Duration'])) {
                $getPlayInfoResp->duration = $data['VideoBase']['Duration'];
            }
            $getPlayInfoResp->video_id = $data['VideoBase']['VideoId'];
            if (isset($data['PlayInfoList']['PlayInfo']) && !empty($data['PlayInfoList']['PlayInfo'])) {
                foreach ($data['PlayInfoList']['PlayInfo'] as $item) {
                    if ($item['Definition'] == 'SD') {
                        $getPlayInfoResp->sd_play_url = $this->getBaseUrl($item['PlayURL']);
                        $getPlayInfoResp->sd_video_size = $item['Size'];
                    }
                    if ($item['Definition'] == 'OD') {
                        $getPlayInfoResp->od_play_url = $this->getBaseUrl($item['PlayURL']);
                        $getPlayInfoResp->od_video_height = $item['Height'];
                        $getPlayInfoResp->od_video_width = $item['Width'];
                        $getPlayInfoResp->od_video_size = $item['Size'];
                    }
                }
            }
        } catch (\Exception $e) {
            \FUR_Log::warn($e->getMessage());
            \FUR_Log::warn($e->getLine());

        }

        $result->setSuccessWithResult($getPlayInfoResp);
        return $result;
    }

    private function getBaseUrl(?string $url): ?string
    {
        if ($url == null) {
            return null;
        }
        $urlInfo = parse_url($url);
        if ($urlInfo == null) {
            return null;
        }
        $baseUrl = '';
        if (!empty($urlInfo['scheme'])) {
            $baseUrl .= $urlInfo['scheme'] . "://";
        }
        if (!empty($urlInfo['host'])) {
            $baseUrl .= $urlInfo['host'];
        }
        if (!empty($urlInfo['path'])) {
            $baseUrl .= $urlInfo['path'];
        }

        return $baseUrl;
    }



}