<?php


namespace service\aliyun\impl;

use AlibabaCloud\Client\AlibabaCloud;
use facade\response\Result;
use facade\service\aliyun\OneClickLoginService;
use service\aliyun\AliyunBase;

class OneClickLoginServiceImpl extends AliyunBase implements OneClickLoginService
{

    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function getPhone(string $accessToken): ?Result
    {
        $result = new Result();

        $phone = "";
        try {
            $phoneResult = AlibabaCloud::rpc()
                ->product('Dypnsapi')
                ->scheme('https')
                ->version('2017-05-25')
                ->action('GetMobile')
                ->options([
                    'query' => [
                        'AccessToken' => $accessToken
                    ],
                ])
                ->method('POST')
                ->host('dypnsapi.aliyuncs.com')->request();
            $data_array = $phoneResult->get();
            if (!isset($data_array['GetMobileResultDTO'])) {
                $result->setError(\Config_Error::ERR_ALIYUN_ONE_CLICK_LOGIN_FAIL);
                return $result;
            }
            $phone = $data_array['GetMobileResultDTO']['Mobile'];
        } catch (Exception $e) {
            $result->setError(\Config_Error::ERR_ALIYUN_ONE_CLICK_LOGIN_FAIL);
            return $result;
        }


        $result->setSuccessWithResult($phone);
        return $result;

    }
}