<?php

namespace service\pick\impl;


use business\order\OrderUtil;
use facade\request\admin\SearchProductReviewListParams;
use facade\response\pick\idol\IdolDongtaiListResp;
use facade\response\pick\idol\IdolFeedDetailResp;
use facade\response\pick\idol\IdolFeedListResp;
use facade\response\pick\idol\IdolRankListResp;
use facade\response\pick\idol\IdolStarInfoResp;
use facade\response\pick\idol\IdolWeiboDongtaiDetailResp;
use facade\response\Result;
use facade\service\order\OrderService;
use facade\request\order\CreateOrderParams;
use facade\service\pick\IdolPickService;
use model\dto\OrderInfoDTO;
use model\dto\OrderTransactionDTO;
use model\dto\ProductItemDTO;
use model\dto\ProductSkuDTO;
use model\OrderModel;
use model\OrderProductModel;
use model\OrderTransactionModel;
use model\product\ProductItemModel;
use model\ProductReviewModel;
use service\product\impl\ProductServiceImpl;
use  \facade\request\product\GetProductDetailParams;
use model\product\ProductSkuModel;


class IdolPickServiceImpl implements IdolPickService
{


    public static $_instance;


    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function getIdolRank(): ?Result
    {
        $result = new Result();
        $queryData = [
            'action' => 'get_star_ranklist',
            'starid' => -1,
            'count' => 100,
            'getLastChampion' => 1,
            'page' => 1,
            'type' => 'month',
            'rank_type' => 3
        ];
        $url = 'https://data.idol001.com/api_mobile_star_rank.php?' . http_build_query($queryData) . $this->buildgeneral_query_string();


        try {
            $data = \FUR_Curl::get($url, [], 5000);
            $jsonDecodeObj = json_decode($data);
            $idolRankListResp = new IdolRankListResp();
            \FUR_Core::copyProperties($jsonDecodeObj, $idolRankListResp);
            $result->setSuccessWithResult($idolRankListResp);
        } catch (\Exception $e) {
        }
        return $result;
    }

    public function getIdolStarInfo(int $idolStarId): ?Result
    {
        $result = new Result();
        $queryData = [
            'action' => 'get_star_homepage',
            'starid' => $idolStarId
        ];
        $url = 'https://data.idol001.com/api_moblie_idol_v2.php?' . http_build_query($queryData) . $this->buildgeneral_query_string();

        try {
            $data = \FUR_Curl::get($url, [], 5000);
            $jsonDecodeObj = json_decode($data);
            $idolStarInfoResp = new IdolStarInfoResp();
            $idolStarInfoResp->name = $jsonDecodeObj->starinfo->name;
            $idolStarInfoResp->en_name = $jsonDecodeObj->starinfo->en_name;
            $idolStarInfoResp->sid = $jsonDecodeObj->starinfo->sid;
            $idolStarInfoResp->logo_img = $jsonDecodeObj->starinfo->logo_img;
            $result->setSuccessWithResult($idolStarInfoResp);
        } catch (\Exception $e) {

        }
        return $result;
    }

    public function searchIdolStar(string $idolStarName): ?Result
    {
        $result = new Result();
        $queryData = [
            'platform' => 'all',
            'word' => $idolStarName
        ];
        $url = 'https://search.idol001.com/star_search_v2.php?' . http_build_query($queryData) . $this->buildgeneral_query_string();

        try {
            $data = \FUR_Curl::get($url, [], 5000);
            $jsonDecodeObj = json_decode($data);
            $idolStarInfoResp = new IdolStarInfoResp();
            $starList = $jsonDecodeObj->idolstar->list;
            $idolStarInfoResp->name = $starList[0]->name;
            $idolStarInfoResp->en_name = $starList[0]->en_name;
            $idolStarInfoResp->sid = $starList[0]->sid;
            $idolStarInfoResp->logo_img = $starList[0]->logo_img;
            $result->setSuccessWithResult($idolStarInfoResp);
        } catch (\Exception $e) {
        }
        return $result;
    }

    public function getIdolStarDongtaiList(int $idolStarId): ?Result
    {
        $result = new Result();
        $queryData = [
            'action' => 'get_star_dongtai_list',
            'starid' => $idolStarId,
        ];
        $url = 'https://data.idol001.com/api_moblie_idol_star_dongtai.php?' . http_build_query($queryData) . $this->buildgeneral_query_string();

        try {
            $data = \FUR_Curl::get($url, [], 5000);
            $jsonDecodeObj = json_decode($data);
            $idolDongtaiListResp = new IdolDongtaiListResp();
            $idolDongtaiListResp->allcount = $jsonDecodeObj->allcount;
            if (isset($jsonDecodeObj->list) == false || $jsonDecodeObj->list == null) {
                return $result;
            }
            $idolDongtaiListResp->list = [];
            foreach ($jsonDecodeObj->list as $decodeDongtaiObj) {
                $idolWeiboDongtai = new   IdolWeiboDongtaiDetailResp();
                $idolWeiboDongtai->type = $decodeDongtaiObj->type;
                $idolWeiboDongtai->weibo_id = $decodeDongtaiObj->weibo_id;
                $idolWeiboDongtai->idol_dongtai_id = $decodeDongtaiObj->_id;
                $idolWeiboDongtai->idol_star_id = $decodeDongtaiObj->star_id;

                $weiboDetail = null;
                if ($idolWeiboDongtai->type == 'weibo_new') {
                    $weiboDetail = $decodeDongtaiObj->data_weibo_new;
                }
                if ($weiboDetail == null) {
                    continue;
                }
                $idolWeiboDongtai->text = $weiboDetail->text;
                $idolWeiboDongtai->image_list = [];
                if (isset($weiboDetail->page_info)) {

                    if (isset($weiboDetail->page_info->page_pic)) {
                        array_push($idolWeiboDongtai->image_list, $weiboDetail->page_info->page_pic);
                    }
                    if (isset($weiboDetail->page_info->page_url)) {
                        $idolWeiboDongtai->weibo_url = $weiboDetail->page_info->page_url;
                    }
                }
                if (isset($weiboDetail->pics) && $weiboDetail->pics) {
                    foreach ($weiboDetail->pics as $picDetail) {
                        if (isset($picDetail->large)) {
                            array_push($idolWeiboDongtai->image_list, $picDetail->large->url);
                            continue;
                        }
                        array_push($idolWeiboDongtai->image_list, $picDetail->url);
                    }
                }
                array_push($idolDongtaiListResp->list, $idolWeiboDongtai);
            }
        } catch (\Exception $e) {
        }
        $result->setSuccessWithResult($idolDongtaiListResp);
        return $result;
    }

    public function getIdolStarFeedList(int $idolStarId): ?Result
    {
        $result = new Result();
        $queryData = [
            'action' => 'get_home_page_recommend_list',
            'starid' => $idolStarId,
        ];
        $url = 'https://data.idol001.com/api_moblie_idol_v2.php?' . http_build_query($queryData) . $this->buildgeneral_query_string();

        try {
            $data = \FUR_Curl::get($url, [], 5000);
            $jsonDecodeObj = json_decode($data);
            $idolFeedListResp = new IdolFeedListResp();
            $idolFeedListResp->allcount = $jsonDecodeObj->allcount;
            if (isset($jsonDecodeObj->feedlist) == false || $jsonDecodeObj->feedlist == null) {
                return $result;
            }

            $idolFeedListResp->list = [];
            foreach ($jsonDecodeObj->feedlist->list as $decodeFeedDetail) {
                $idolFeedDetail = new IdolFeedDetailResp();
                $idolFeedDetail->idol_obj_id = $decodeFeedDetail->objid;
                $idolFeedDetail->timestamp = $decodeFeedDetail->public_time / 1000;
                $dataObj = null;
                if ($decodeFeedDetail->type == 1) {
                    $dataObj = $decodeFeedDetail->data_news;
                } elseif ($decodeFeedDetail->type == 3) {
//                    $dataObj = $decodeFeedDetail->data_video;
                }

                if ($dataObj == null) {
                    continue;
                }
                if (isset($dataObj->title)) {
                    $idolFeedDetail->title = $dataObj->title;
                }
                $dataObj->text = preg_replace('/^\[.*\]/', '', $dataObj->text);

                $idolFeedDetail->author_name = $dataObj->author_name;
                $idolFeedDetail->text = $dataObj->text;
                $idolFeedDetail->images = [];
                if (isset($dataObj->images) && $dataObj->images != null) {
                    foreach ($dataObj->images as $image) {
                        $imagePathInfo = pathinfo($image->img_url->origin_pic);
                        if (strlen($imagePathInfo['filename']) < 32) {
                            continue;
                        }

                        array_push($idolFeedDetail->images, $image->img_url->origin_pic);
                    }
                }
                array_push($idolFeedListResp->list, $idolFeedDetail);
            }
        } catch (\Exception $e) {
        }
        $result->setSuccessWithResult($idolFeedListResp);
        return $result;
    }


    private function buildgeneral_query_string()
    {
        return '&version=227&channelId=S007&app_platform=android&unique_id=' . strtoupper(md5(uniqid()));
    }
}