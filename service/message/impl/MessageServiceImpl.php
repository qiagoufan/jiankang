<?php

namespace service\message\impl;

use model\dto\MessageInteractiveDTO;
use model\MessageInteractiveModel;
use model\UserMessageModel;
use facade\response\Result;
use facade\service\message\MessageService;
use facade\request\PageParams;

class MessageServiceImpl implements MessageService
{
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function getAllMessageList(PageParams $pageParams, int $userId): Result
    {
        $result = new Result();
        $userMessageModel = UserMessageModel::getInstance();
        $offset = $pageParams->index * $pageParams->page_size;
        $pageSize = $pageParams->page_size;
        $userMessageList = $userMessageModel->queryMessageList($userId, $offset, $pageSize);
        $result->setSuccessWithResult($userMessageList);
        return $result;

    }


    public function getUserUnreadMessage(?int $userId): ?Result
    {

        $result = new Result();
        $userMessageModel = UserMessageModel::getInstance();
        $getUserUnreadMessage = $userMessageModel->getUserUnreadMessage($userId);
        $result->setSuccessWithResult($getUserUnreadMessage);
        return $result;

    }


    public function getUserInteractiveMessageList(int $interactiveMessageType, int $receiveUserId, int $index, int $pageSize): ?Result
    {
        $result = new Result();
        $messageInteractiveModel = MessageInteractiveModel::getInstance();
        $messageList = $messageInteractiveModel->queryInteractiveMessageList($interactiveMessageType, $receiveUserId, $index, $pageSize);
        $result->setSuccessWithResult($messageList);
        return $result;

    }


    public function insertInteractiveMessage(MessageInteractiveDTO $messageInteractiveDTO): ?Result
    {
        $result = new Result();
        $messageInteractiveModel = MessageInteractiveModel::getInstance();
        $insertRet = $messageInteractiveModel->insertRecord($messageInteractiveDTO);
        $result->setSuccessWithResult($insertRet);
        return $result;
    }


    public function updateUserUnreadInteractiveMessage(int $userId): ?Result
    {
        $result = new Result();
        $messageInteractiveModel = MessageInteractiveModel::getInstance();
        $messageInteractiveModel->updateUserUnreadMessage($userId);
        $result->setSuccessWithResult(true);
        return $result;
    }


    public function getUserInteractiveMessageCount(int $userId): ?Result
    {
        $result = new Result();
        $messageInteractiveModel = MessageInteractiveModel::getInstance();
        $rowCount = $messageInteractiveModel->getInteractiveMessageCount($userId);

        if ($rowCount == null) {
            $rowCount = 0;
        }

        $result->setSuccessWithResult($rowCount);
        return $result;
    }

}