<?php


namespace service\message\impl;

use facade\request\message\NotifySwitchParams;
use facade\response\Result;
use facade\service\message\NotifyService;
use model\dto\NotifySwitchDTO;
use model\NotifySwitchModel;

class NotifyServiceImpl implements NotifyService
{
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function getNotifyConfig(?string $deviceId): ?Result
    {
        $result = new Result();
        $notifySwitchModel = NotifySwitchModel::getInstance();
        $notifySwitchDTO = $notifySwitchModel->queryNotifySwitchDetail($deviceId);
        if ($notifySwitchDTO == null) {
            $notifySwitchDTO = new NotifySwitchDTO();
        }
        $result->setSuccessWithResult($notifySwitchDTO);
        return $result;
    }

    public function updateNotifySwitch(?NotifySwitchParams $notifySwitchParams): ?Result
    {
        $result = new Result();
        $notifySwitchModel = NotifySwitchModel::getInstance();

        $notifySwitchDTO = new NotifySwitchDTO();
        \FUR_Core::copyProperties($notifySwitchParams, $notifySwitchDTO);
        $notifySwitchDTO->create_timestamp = time();
        $notifySwitchDTO->update_timestamp = time();
        $row = $notifySwitchModel->updateRecord($notifySwitchDTO);
        if ($row == 0) {
            $notifySwitchModel->insertRecord($notifySwitchDTO);
            $row = 1;
        }
        $result->setSuccessWithResult($row);
        return $result;
    }
}