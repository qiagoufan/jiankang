<?php


namespace service\jk\impl;


use facade\request\jk\SelfCheck\SelfCheckAnswerParams;
use facade\request\jk\SelfCheck\SubmitSelfCheckAnswerParams;
use facade\request\PageParams;
use facade\response\Result;
use model\jk\dto\JkSelfCheckCategoryDTO;
use model\jk\dto\JkSelfCheckQuestionDTO;
use model\jk\dto\JkSelfCheckResultDTO;
use model\jk\SelfCheckModel;
use service\jk\data\CheckQuestion;

class JkSelfCheckServiceImpl
{


    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function getJkSelfCheckCategory($id): Result
    {
        $result = new Result();
        $data = SelfCheckModel::getInstance()->getJkSelfCheckCategory($id);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function insertJkSelfCheckCategory(JkSelfCheckCategoryDTO $jkSelfCheckCategoryDTO): Result
    {
        $result = new Result();
        $jkSelfCheckCategoryDTO->created_timestamp = time();
        $data = SelfCheckModel::getInstance()->insertJkSelfCheckCategory($jkSelfCheckCategoryDTO);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function deleteJkSelfCheckCategory($id): Result
    {
        $result = new Result();
        $data = SelfCheckModel::getInstance()->deleteJkSelfCheckCategory($id);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function deleteJkSelfCheckQuestionById($id): Result
    {
        $result = new Result();
        $data = SelfCheckModel::getInstance()->deleteJkSelfCheckQuestionById($id);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function deleteJkSelfCheckResultById($id): Result
    {
        $result = new Result();
        $data = SelfCheckModel::getInstance()->deleteJkSelfCheckResultById($id);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function updateJkSelfCheckCategory(JkSelfCheckCategoryDTO $jkSelfCheckCategoryDTO): Result
    {
        $result = new Result();
        $jkSelfCheckCategoryDTO->updated_timestamp = time();
        $data = SelfCheckModel::getInstance()->updateJkSelfCheckCategory($jkSelfCheckCategoryDTO);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function getJkSelfCheckCategoryList(): Result
    {
        $result = new Result();
        $data = SelfCheckModel::getInstance()->getJkSelfCheckCategoryList();
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function queryJkSelfCheckCategoryList(PageParams $params): Result
    {
        $result = new Result();
        $data = SelfCheckModel::getInstance()->queryJkSelfCheckCategoryList($params);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function queryJkSelfCheckCategoryCount(): Result
    {
        $result = new Result();
        $data = SelfCheckModel::getInstance()->queryJkSelfCheckCategoryCount();
        $result->setSuccessWithResult($data);
        return $result;
    }


    public function insertJkSelfCheckQuestionDTO(JkSelfCheckQuestionDTO $jkSelfCheckQuestionDTO): Result
    {
        $result = new Result();
        $jkSelfCheckQuestionDTO->created_timestamp = time();
        $data = SelfCheckModel::getInstance()->insertJkSelfCheckQuestionDTO($jkSelfCheckQuestionDTO);
        $result->setSuccessWithResult($data);
        return $result;
    }


    public function insertJkSelfCheckResultDTO(JkSelfCheckResultDTO $resultDTO): Result
    {
        $result = new Result();
        $resultDTO->created_timestamp = time();
        $data = SelfCheckModel::getInstance()->insertJkSelfCheckResultDTO($resultDTO);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function getJkSelfCheckQuestionList($categoryId): Result
    {
        $result = new Result();
        $data = SelfCheckModel::getInstance()->getJkSelfCheckQuestionList($categoryId);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function getJkSelfCheckResultList($categoryId): Result
    {
        $result = new Result();
        $data = SelfCheckModel::getInstance()->getJkSelfCheckResultList($categoryId);
        $result->setSuccessWithResult($data);
        return $result;
    }


    public function submitSelfCheckQuestion(SubmitSelfCheckAnswerParams $params)
    {
        $result = new Result();
        $categoryId = $params->category_id;
        $answerList = $params->answer_list;

        /** @var JkSelfCheckCategoryDTO $selfCheckInfo */
        $selfCheckInfo = SelfCheckModel::getInstance()->getJkSelfCheckCategory($categoryId);
        $calculation_type = $selfCheckInfo->calculation_type;


        $score = 0;
        if ($calculation_type == \Config_Const::CALCULATION_TYPE_MUL) {
            $score = 1;
        }
        /** @var SelfCheckAnswerParams $answer */
        foreach ($answerList as $answer) {
            if ($answer->option_list == null) {
                continue;
            }
            if ($calculation_type == \Config_Const::CALCULATION_TYPE_PLUS) {
                /** @var CheckQuestion $option */
                foreach ($answer->option_list as $option) {
                    $score = $score + $option->score;
                }
            }
            if ($calculation_type == \Config_Const::CALCULATION_TYPE_MUL) {
                /** @var CheckQuestion $option */
                foreach ($answer->option_list as $option) {
                    $score = $score * $option->score;
                }
            }

        }
        $data = SelfCheckModel::getInstance()->queryResultByScore($categoryId, $score);
        $result->setSuccessWithResult($data);
        return $result;
    }
}