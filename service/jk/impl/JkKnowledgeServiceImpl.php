<?php


namespace service\jk\impl;


use facade\request\jk\knowledge\QueryKnowledgeListParams;
use facade\response\Result;
use model\jk\KnowledgeModel;
use model\jk\dto\JkKnowledgeCategoryDTO;
use model\jk\dto\JkKnowledgeDTO;

class JkKnowledgeServiceImpl
{


    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function getJkKnowledgeCategory($id): Result
    {
        $result = new Result();
        $data = KnowledgeModel::getInstance()->getJkKnowledgeCategory($id);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function insertJkKnowledgeCategory(JkKnowledgeCategoryDTO $jkKnowledgeCategoryDTO): Result
    {
        $result = new Result();
        $jkKnowledgeCategoryDTO->created_timestamp = time();
        $data = KnowledgeModel::getInstance()->insertJkKnowledgeCategory($jkKnowledgeCategoryDTO);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function deleteJkKnowledgeCategory($id): Result
    {
        $result = new Result();
        $data = KnowledgeModel::getInstance()->deleteJkKnowledgeCategory($id);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function updateJkKnowledgeCategory(JkKnowledgeCategoryDTO $jkKnowledgeCategoryDTO): Result
    {
        $result = new Result();
        $jkKnowledgeCategoryDTO->updated_timestamp = time();
        $data = KnowledgeModel::getInstance()->updateJkKnowledgeCategory($jkKnowledgeCategoryDTO);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function getJkKnowledgeCategoryList(): Result
    {
        $result = new Result();
        $data = KnowledgeModel::getInstance()->getJkKnowledgeCategoryList();
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function getJkKnowledge($id): Result
    {
        $result = new Result();
        $data = KnowledgeModel::getInstance()->getJkKnowledge($id);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function insertJkKnowledge(JkKnowledgeDTO $jkKnowledgeDTO): Result
    {
        $result = new Result();
        $jkKnowledgeDTO->created_timestamp = time();
        $data = KnowledgeModel::getInstance()->insertJkKnowledge($jkKnowledgeDTO);
        $result->setSuccessWithResult($data);
        return $result;

    }

    public function deleteJkKnowledge($id): Result
    {
        $result = new Result();
        $data = KnowledgeModel::getInstance()->deleteJkKnowledge($id);
        $result->setSuccessWithResult($data);
        return $result;

    }

    public function updateJkKnowledge(JkKnowledgeDTO $jkKnowledgeDTO): Result
    {
        $result = new Result();
        $jkKnowledgeDTO->updated_timestamp = time();
        $data = KnowledgeModel::getInstance()->updateJkKnowledge($jkKnowledgeDTO);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function getJkKnowledgeList(QueryKnowledgeListParams $queryKnowledgeListParams): Result
    {
        $result = new Result();
        $data = KnowledgeModel::getInstance()->getJkKnowledgeList($queryKnowledgeListParams);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function getManageJkKnowledgeList(QueryKnowledgeListParams $queryKnowledgeListParams): Result
    {
        $result = new Result();
        $data = KnowledgeModel::getInstance()->getManageJkKnowledgeList($queryKnowledgeListParams);
        $result->setSuccessWithResult($data);
        return $result;
    }
    public function getJkKnowledgeCount(QueryKnowledgeListParams $queryKnowledgeListParams): Result
    {
        $result = new Result();
        $data = KnowledgeModel::getInstance()->getJkKnowledgeCount($queryKnowledgeListParams);
        $result->setSuccessWithResult($data);
        return $result;
    }
}