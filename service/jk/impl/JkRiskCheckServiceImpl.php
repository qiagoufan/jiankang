<?php


namespace service\jk\impl;


use facade\request\jk\RiskCheck\RiskCheckAnswerParams;
use facade\request\jk\RiskCheck\SubmitRiskCheckAnswerParams;
use facade\request\PageParams;
use facade\response\Result;
use model\jk\dto\JkRiskCheckCategoryDTO;
use model\jk\dto\JkRiskCheckOptionDTO;
use model\jk\dto\JkRiskCheckQuestionDTO;
use model\jk\dto\JkRiskCheckResultDTO;
use model\jk\RiskCheckModel;
use service\jk\data\CheckQuestion;

class JkRiskCheckServiceImpl
{


    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function getJkRiskCheckCategory($id): Result
    {
        $result = new Result();
        $data = RiskCheckModel::getInstance()->getJkRiskCheckCategory($id);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function insertJkRiskCheckCategory(JkRiskCheckCategoryDTO $jkRiskCheckCategoryDTO): Result
    {
        $result = new Result();
        $jkRiskCheckCategoryDTO->created_timestamp = time();
        $data = RiskCheckModel::getInstance()->insertJkRiskCheckCategory($jkRiskCheckCategoryDTO);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function deleteJkRiskCheckCategory($id): Result
    {
        $result = new Result();
        $data = RiskCheckModel::getInstance()->deleteJkRiskCheckCategory($id);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function deleteJkRiskCheckQuestionById($id): Result
    {
        $result = new Result();
        $data = RiskCheckModel::getInstance()->deleteJkRiskCheckQuestionById($id);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function deleteJkRiskCheckResultById($id): Result
    {
        $result = new Result();
        $data = RiskCheckModel::getInstance()->deleteJkRiskCheckResultById($id);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function updateJkRiskCheckCategory(JkRiskCheckCategoryDTO $jkRiskCheckCategoryDTO): Result
    {
        $result = new Result();
        $jkRiskCheckCategoryDTO->updated_timestamp = time();
        $data = RiskCheckModel::getInstance()->updateJkRiskCheckCategory($jkRiskCheckCategoryDTO);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function getJkRiskCheckCategoryList(): Result
    {
        $result = new Result();
        $data = RiskCheckModel::getInstance()->getJkRiskCheckCategoryList();
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function getJkRiskCheckCategoryList1(): Result
    {
        $result = new Result();
        $data = RiskCheckModel::getInstance()->getJkRiskCheckCategoryList1();
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function queryJkRiskCheckCategoryList(PageParams $params): Result
    {
        $result = new Result();
        $data = RiskCheckModel::getInstance()->queryJkRiskCheckCategoryList($params);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function queryJkRiskCheckLevel2CategoryList(PageParams $params): Result
    {
        $result = new Result();
        $data = RiskCheckModel::getInstance()->queryJkRiskCheckLevel2CategoryList($params);
        $result->setSuccessWithResult($data);
        return $result;
    }


    public function queryJkRiskCheckCategoryCount(): Result
    {
        $result = new Result();
        $data = RiskCheckModel::getInstance()->queryJkRiskCheckCategoryCount();
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function queryJkRiskCheckLevel2CategoryCount(): Result
    {
        $result = new Result();
        $data = RiskCheckModel::getInstance()->queryJkRiskCheckLevel2CategoryCount();
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function queryJkRiskCheckCategoryCountByParentId($parentId): Result
    {
        $result = new Result();
        $data = RiskCheckModel::getInstance()->queryJkRiskCheckCategoryCountByParentId($parentId);
        $result->setSuccessWithResult($data);
        return $result;
    }


    public function insertJkRiskCheckQuestionDTO(JkRiskCheckQuestionDTO $jkRiskCheckQuestionDTO): Result
    {
        $result = new Result();
        $jkRiskCheckQuestionDTO->created_timestamp = time();
        $data = RiskCheckModel::getInstance()->insertJkRiskCheckQuestionDTO($jkRiskCheckQuestionDTO);
        $result->setSuccessWithResult($data);
        return $result;
    }


    public function insertJkRiskCheckResultDTO(JkRiskCheckResultDTO $resultDTO): Result
    {
        $result = new Result();
        $resultDTO->created_timestamp = time();
        $data = RiskCheckModel::getInstance()->insertJkRiskCheckResultDTO($resultDTO);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function getJkRiskCheckQuestionList($categoryId): Result
    {
        $result = new Result();
        $data = RiskCheckModel::getInstance()->getJkRiskCheckQuestionList($categoryId);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function queryJkRiskCheckQuestionList(PageParams $params): Result
    {
        $result = new Result();
        $data = RiskCheckModel::getInstance()->queryJkRiskCheckQuestionList($params);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function queryJkRiskCheckQuestionCount(PageParams $params): Result
    {
        $result = new Result();
        $data = RiskCheckModel::getInstance()->queryJkRiskCheckQuestionCount($params);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function getJkRiskCheckResultList($categoryId): Result
    {
        $result = new Result();
        $data = RiskCheckModel::getInstance()->getJkRiskCheckResultList($categoryId);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function insertJkRiskCheckOptionDTO(?JkRiskCheckOptionDTO $optionDTO): ?Result
    {
        $result = new Result();
        $optionDTO->created_timestamp = time();
        $data = RiskCheckModel::getInstance()->insertJkRiskCheckOptionDTO($optionDTO);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function queryOptionList(?int $questionId): ?Result
    {
        $result = new Result();
        $data = RiskCheckModel::getInstance()->queryOptionList($questionId);
        $result->setSuccessWithResult($data);
        return $result;
    }


    public function cleanOptionList(?int $questionId): ?Result
    {
        $result = new Result();
        $data = RiskCheckModel::getInstance()->cleanOptionList($questionId);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function queryOption(?int $optionId): ?Result
    {
        $result = new Result();
        $data = RiskCheckModel::getInstance()->queryOption($optionId);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function submitRiskCheckQuestion(SubmitRiskCheckAnswerParams $params)
    {
        $result = new Result();
        $categoryId = $params->category_id;
        $answerList = $params->answer_list;
        $optionIdList = $params->option_id_list;
        /** @var JkRiskCheckCategoryDTO $riskCheckInfo */
        $riskCheckInfo = RiskCheckModel::getInstance()->getJkRiskCheckCategory($categoryId);
        $calculation_type = $riskCheckInfo->calculation_type;

        $score = 0;
        if ($calculation_type == \Config_Const::CALCULATION_TYPE_MUL) {
            $score = 1;
        }
        $optionDTOList = [];
        if ($optionIdList == null || empty($optionIdList)) {
            /** @var RiskCheckAnswerParams $answer */
            foreach ($answerList as $answer) {
                if ($answer->option_list == null) {
                    continue;
                }
                /** @var CheckQuestion $option */
                if ($calculation_type == \Config_Const::CALCULATION_TYPE_PLUS) {
                    foreach ($answer->option_list as $option) {
                        $score = $score + $option->score;
                    }
                }
                if ($calculation_type == \Config_Const::CALCULATION_TYPE_MUL) {
                    foreach ($answer->option_list as $option) {
                        $score = $score * $option->score;
                    }
                }


            }
        } else {
            foreach ($optionIdList as $optionId) {
                $optionDTO = RiskCheckModel::getInstance()->queryOption($optionId);
                if ($calculation_type == \Config_Const::CALCULATION_TYPE_PLUS) {
                    $score = $score + $optionDTO->score;
                }
                if ($calculation_type == \Config_Const::CALCULATION_TYPE_MUL) {
                    $score = $score * $optionDTO->score;
                }
                array_push($optionDTOList, $optionDTO);
            }
        }


        $data = RiskCheckModel::getInstance()->queryResultByScore($categoryId, $score);
        if ($data == null) {
            $data = new JkRiskCheckResultDTO();
            $data->id = 0;
            $data->disease = '没有相关结果';
            $data->suggest = '哎哟~，还没有收录相关建议';
        }
        $data->option_list = $optionDTOList;
        $result->setSuccessWithResult($data);
        return $result;
    }
}