<?php


namespace service\jk\impl;


use facade\request\jk\bodyCheck\manage\EditSymptomParams;
use facade\request\jk\bodyCheck\manage\QuerySymptomParams;
use facade\request\jk\bodyCheck\QueryPossibleDiseaseListParams;
use facade\request\jk\bodyCheck\QuerySymptomListParams;
use facade\request\PageParams;
use facade\response\Result;
use model\jk\dto\JkBodyAgeSymptomDTO;
use model\jk\dto\JkBodyOptionDTO;
use model\jk\dto\JkBodyPossibleDiseaseDTO;
use model\jk\dto\JkBodyQuestionDTO;
use model\jk\dto\JkBodySymptomDTO;
use model\jk\dto\JkBodySymptomListDTO;
use model\jk\JkBodyDiseaseModel;
use model\jk\JkBodyModel;
use model\jk\JkBodyPossibleDiseaseModel;
use model\jk\JkBodySymptomModel;
use model\jk\JkBodySymptomQuestionModel;

class JkBodyServiceImpl
{


    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function queryBodyList(): Result
    {
        $result = new Result();
        $data = JkBodyModel::getInstance()->getBodyList();
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function querySymptomList(QuerySymptomListParams $params): Result
    {
        $result = new Result();
        $symptomList = JkBodySymptomModel::getInstance()->querySymptomList($params);
        $result->setSuccessWithResult($symptomList);
        return $result;
    }

    public function querySymptomDetail(?int $symptomId): Result
    {
        $result = new Result();
        $symptomList = JkBodySymptomModel::getInstance()->querySymptomDetail($symptomId);
        $result->setSuccessWithResult($symptomList);
        return $result;
    }

    public function queryManageSymptomList(QuerySymptomParams $params): Result
    {

        $result = new Result();
        $symptomList = JkBodySymptomModel::getInstance()->queryManageSymptomList($params);
        $result->setSuccessWithResult($symptomList);
        return $result;
    }

    public function querySymptomAgeList(int $symptomId): Result
    {

        $result = new Result();
        $data = JkBodySymptomModel::getInstance()->querySymptomAgeList($symptomId);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function queryManageSymptomCount(QuerySymptomParams $params): Result
    {

        $result = new Result();
        $symptomList = JkBodySymptomModel::getInstance()->queryManageSymptomCount($params);
        $result->setSuccessWithResult($symptomList);
        return $result;
    }

    public function queryQuestionList(int $symptomId): Result
    {

        $result = new Result();
        $data = JkBodySymptomQuestionModel::getInstance()->queryQuestionList($symptomId);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function queryOptionList(int $questionId): Result
    {
        $result = new Result();
        $data = JkBodySymptomQuestionModel::getInstance()->queryOptionList($questionId);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function queryPossibleDiseaseList(QueryPossibleDiseaseListParams $params): Result
    {
        $result = new Result();
        $optionIdList = $params->option_id_list;
        $totalScore = 0;

        /** @var JkBodySymptomDTO $bodySymptomDTO */
        $bodySymptomDTO = JkBodySymptomModel::getInstance()->querySymptomDetail($params->symptom_id);
        $calculation_type = $bodySymptomDTO->calculation_type;
        if ($calculation_type == \Config_Const::CALCULATION_TYPE_MUL) {
            $totalScore = 1;
        }
        if ($optionIdList != null) {
            foreach ($optionIdList as $optionId) {
                $optionInfo = JkBodySymptomQuestionModel::getInstance()->queryOptionInfo(intval($optionId));
                if ($calculation_type == \Config_Const::CALCULATION_TYPE_PLUS) {
                    $totalScore = $totalScore + $optionInfo->score;
                }
                if ($calculation_type == \Config_Const::CALCULATION_TYPE_MUL) {
                    $totalScore = $totalScore * $optionInfo->score;
                }

            }
        }
        $data = [];
        if ($totalScore != 0) {
            $data = JkBodyDiseaseModel::getInstance()->queryPossibleDiseaseListWithScore($params, $totalScore);
        }

        if (empty($data)) {
            $data = JkBodyDiseaseModel::getInstance()->queryPossibleDiseaseList($params);
        }
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function queryPossibleDiseaseListBySymptomId($symptomId): Result
    {
        $result = new Result();
        $data = JkBodyDiseaseModel::getInstance()->queryPossibleDiseaseListBySymptomId($symptomId);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function getDiseaseDetail(int $diseaseId): Result
    {
        $result = new Result();
        $data = JkBodyDiseaseModel::getInstance()->getDiseaseDetail($diseaseId);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function createSymptom(EditSymptomParams $editSymptomParams): ?Result
    {
        $result = new Result();

        $symptomDTO = new JkBodySymptomDTO();
        $symptomDTO->created_timestamp = time();
        $symptomDTO->have_question = 0;
        $symptomDTO->human_body_id = $editSymptomParams->human_body_id;
        $symptomDTO->symptom = $editSymptomParams->symptom;
        $symptomDTO->symptom_image = $editSymptomParams->symptom_image;
        $symptomDTO->calculation_type = $editSymptomParams->calculation_type;
        $symptom_id = JkBodySymptomModel::getInstance()->insertJkBodySymptom($symptomDTO);

        $ageList = $editSymptomParams->age;
        $sexList = $editSymptomParams->sex;
        foreach ($ageList as $age) {
            $bodyAgeSymptomDTO = new JkBodyAgeSymptomDTO();
            $bodyAgeSymptomDTO->created_timestamp = time();
            $bodyAgeSymptomDTO->age = $age;
            $bodyAgeSymptomDTO->symptom_id = $symptom_id;
            foreach ($sexList as $sex) {
                $bodyAgeSymptomDTO->sex = $sex;
                JkBodySymptomModel::getInstance()->insertHumanSymptomAge($bodyAgeSymptomDTO);
            }
        }

        $result->setSuccessWithResult($symptom_id);
        return $result;

    }

    public function updateSymptom(EditSymptomParams $editSymptomParams): ?Result
    {
        $result = new Result();

        JkBodySymptomModel::getInstance()->cleanSymptomAgeBySymptomId($editSymptomParams->symptom_id);
        $symptomDTO = JkBodySymptomModel::getInstance()->querySymptomDetail($editSymptomParams->symptom_id);
        $symptomDTO->updated_timestamp = time();
        $symptomDTO->human_body_id = $editSymptomParams->human_body_id;
        $symptomDTO->symptom = $editSymptomParams->symptom;
        $symptomDTO->calculation_type = $editSymptomParams->calculation_type;
        if ($editSymptomParams->symptom_image) {
            $symptomDTO->symptom_image = $editSymptomParams->symptom_image;
        }
        $rows = JkBodySymptomModel::getInstance()->updateJkBodySymptom($symptomDTO);

        $ageList = $editSymptomParams->age;
        $sexList = $editSymptomParams->sex;
        foreach ($ageList as $age) {
            $bodyAgeSymptomDTO = new JkBodyAgeSymptomDTO();
            $bodyAgeSymptomDTO->created_timestamp = time();
            $bodyAgeSymptomDTO->age = $age;
            $bodyAgeSymptomDTO->symptom_id = $symptomDTO->id;
            foreach ($sexList as $sex) {
                $bodyAgeSymptomDTO->sex = $sex;
                JkBodySymptomModel::getInstance()->insertHumanSymptomAge($bodyAgeSymptomDTO);
            }
        }

        $result->setSuccessWithResult($rows);
        return $result;

    }

    public function deleteSymptom(?int $symptomId): ?Result
    {
        $result = new Result();
        $data = JkBodySymptomModel::getInstance()->deleteSymptom($symptomId);
        $result->setSuccessWithResult($data);
        return $result;

    }

    public function insertJkBodyQuestion(JkBodyQuestionDTO $jkBodyQuestionDTO): ?Result
    {
        $result = new Result();
        $jkBodyQuestionDTO->created_timestamp = time();
        $data = JkBodySymptomQuestionModel::getInstance()->insertJkBodyQuestion($jkBodyQuestionDTO);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function cleanJkBodyQuestion(?int $symptomId): ?Result
    {
        $result = new Result();
        $data = JkBodySymptomQuestionModel::getInstance()->cleanJkBodyQuestion($symptomId);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function insertJkBodyOption(JkBodyOptionDTO $jkBodyOptionDTO): ?Result
    {
        $result = new Result();
        $jkBodyOptionDTO->created_timestamp = time();
        $data = JkBodySymptomQuestionModel::getInstance()->insertJkBodyOption($jkBodyOptionDTO);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function cleanPossibleDisease(int $symptomId): ?Result
    {
        $result = new Result();
        $data = JkBodyPossibleDiseaseModel::getInstance()->cleanPossibleDisease($symptomId);
        $result->setSuccessWithResult($data);
        return $result;

    }

    public function insertPossibleDisease(JkBodyPossibleDiseaseDTO $jkBodyPossibleDiseaseDTO): ?Result
    {
        $result = new Result();
        $jkBodyPossibleDiseaseDTO->created_timestamp = time();
        $data = JkBodyPossibleDiseaseModel::getInstance()->insertJkPossibleDisease($jkBodyPossibleDiseaseDTO);
        $result->setSuccessWithResult($data);
        return $result;

    }
}