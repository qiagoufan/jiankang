<?php


namespace service\jk\impl;


use facade\request\jk\knowledge\QueryKnowledgeListParams;
use facade\response\Result;
use model\jk\DiseaseModel;
use model\jk\dto\JkHospitalClassDTO;
use model\jk\HospitalClassModel;

class JkHospitalClassServiceImpl
{


    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function getHospitalClass($id): Result
    {
        $result = new Result();
        $data = HospitalClassModel::getInstance()->getHospitalClass($id);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function insertHospitalClass(JkHospitalClassDTO $hospitalClassDTO): Result
    {
        $result = new Result();
        $data = HospitalClassModel::getInstance()->insertHospitalClass($hospitalClassDTO);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function deleteHospitalClass($id): Result
    {
        $result = new Result();
        $data = HospitalClassModel::getInstance()->deleteHospitalClass($id);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function updateHospitalClass(JkHospitalClassDTO $hospitalClassDTO): Result
    {
        $result = new Result();
        $data = HospitalClassModel::getInstance()->updateHospitalClass($hospitalClassDTO);
        DiseaseModel::getInstance()->updateDiseaseClassName($hospitalClassDTO->id, $hospitalClassDTO->class_name);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function getHospitalClassList(): Result
    {
        $result = new Result();
        $data = HospitalClassModel::getInstance()->getHospitalClassList();
        $result->setSuccessWithResult($data);
        return $result;
    }


}