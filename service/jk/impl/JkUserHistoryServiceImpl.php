<?php


namespace service\jk\impl;


use facade\request\jk\knowledge\QueryKnowledgeListParams;
use facade\request\PageParams;
use facade\response\Result;
use model\jk\dto\JkUserCheckHistoryDTO;
use model\jk\KnowledgeModel;
use model\jk\dto\JkKnowledgeCategoryDTO;
use model\jk\dto\JkKnowledgeDTO;
use model\jk\UserCheckHistoryModel;

class JkUserHistoryServiceImpl
{


    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function saveUserHistory(JkUserCheckHistoryDTO $jkUserCheckHistoryDTO): Result
    {
        $result = new Result();
        $jkUserCheckHistoryDTO->created_timestamp = time();
        $data = UserCheckHistoryModel::getInstance()->insertRecord($jkUserCheckHistoryDTO);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function getUserHistory(PageParams $params): Result
    {
        $result = new Result();
        $data = UserCheckHistoryModel::getInstance()->getUserHistoryList($params);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function getDetail(int $id): Result
    {
        $result = new Result();
        $data = UserCheckHistoryModel::getInstance()->getDetail($id);
        $result->setSuccessWithResult($data);
        return $result;
    }
    public function getUserHistoryCount($userId): Result
    {
        $result = new Result();
        $data = UserCheckHistoryModel::getInstance()->getUserHistoryCount($userId);
        $result->setSuccessWithResult($data);
        return $result;
    }

}