<?php


namespace service\jk\impl;


use facade\request\jk\bodyCheck\manage\EditDiseaseParams;
use facade\request\jk\bodyCheck\manage\QueryDiseaseParams;
use facade\request\PageParams;
use facade\response\Result;
use model\jk\dto\JkBodyDiseaseDetailDTO;
use model\jk\DiseaseModel;
use model\jk\dto\JkHospitalClassDTO;
use model\jk\HospitalClassModel;

class JkDiseaseServiceImpl
{


    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function getDisease($id): Result
    {
        $result = new Result();
        $data = DiseaseModel::getInstance()->getDisease($id);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function insertDisease(EditDiseaseParams $params): Result
    {
        $result = new Result();
        $diseaseDTO = new JkBodyDiseaseDetailDTO();
        \FUR_Core::copyProperties($params, $diseaseDTO);
        $diseaseDTO->created_timestamp = time();

        $hospitalClassInfoRet = JkHospitalClassServiceImpl::getInstance()->getHospitalClass($params->class_id);
        /** @var JkHospitalClassDTO $hospitalClassInfo */
        $hospitalClassInfo = $hospitalClassInfoRet->data;
        $diseaseDTO->class_name = $hospitalClassInfo->class_name;
        $data = DiseaseModel::getInstance()->insertDisease($diseaseDTO);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function deleteDisease($id): Result
    {
        $result = new Result();
        $data = DiseaseModel::getInstance()->deleteDisease($id);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function updateDisease(EditDiseaseParams $params): Result
    {
        $result = new Result();
        $diseaseDTO = DiseaseModel::getInstance()->getDisease($params->disease_id);
        \FUR_Core::copyProperties($params, $diseaseDTO);
        $diseaseDTO->updated_timestamp = time();

        $hospitalClassInfoRet = JkHospitalClassServiceImpl::getInstance()->getHospitalClass($params->class_id);
        /** @var JkHospitalClassDTO $hospitalClassInfo */
        $hospitalClassInfo = $hospitalClassInfoRet->data;
        $diseaseDTO->class_name = $hospitalClassInfo->class_name;
        $data = DiseaseModel::getInstance()->updateDisease($diseaseDTO);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function getDiseaseList(QueryDiseaseParams $params): Result
    {
        $result = new Result();
        $data = DiseaseModel::getInstance()->getDiseaseList($params);
        $result->setSuccessWithResult($data);
        return $result;
    }
    public function getDiseaseCountByClassId($classId): Result
    {
        $result = new Result();
        $data = DiseaseModel::getInstance()->getDiseaseCountByClassId($classId);
        $result->setSuccessWithResult($data);
        return $result;
    }


    public function getDiseaseCount(QueryDiseaseParams $params): Result
    {
        $result = new Result();
        $data = DiseaseModel::getInstance()->getDiseaseCount($params);
        $result->setSuccessWithResult($data);
        return $result;
    }
}