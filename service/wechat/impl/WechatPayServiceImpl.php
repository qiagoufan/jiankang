<?php

namespace service\wechat\impl;

use facade\service\wechat\WechatService;

header('Content-type:text/html; Charset=utf-8');

class WechatPayServiceImpl implements WechatService
{
    protected $mchid;
    protected $appid;
    protected $appSecret;
    protected $h5appId;
    protected $miniAppId;
    protected $miniAppSecret;
    protected $apiKey;
    protected $appKey;
    protected $totalFee;
    protected $outTradeNo;
    protected $orderName;
    protected $notifyUrl;
    protected $returnUrl = 'http://starmall.ipxmall.com/order/order-result';
    protected $wapUrl = 'https://www.jfshare.com/';
    protected $wapName = 'H5支付';
    public static $_instance;


    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function __construct()
    {
        $this->mchid = \FUR_Config::get_wechat_pay_config('mchid');
        $this->appid = \FUR_Config::get_wechat_pay_config('appid');
        $this->apiKey = \FUR_Config::get_wechat_pay_config('apiKey');
        //小程序
        $this->miniAppId = \FUR_Config::get_wechat_pay_config('miniAppId');
        $this->miniAppSecret = \FUR_Config::get_wechat_pay_config('miniAppSecret');
        //微信公众号
        $this->h5appId = \FUR_Config::get_wechat_pay_config('h5appId');
        $this->appKey = \FUR_Config::get_wechat_pay_config('appKey');
        $this->appSecret = \FUR_Config::get_wechat_pay_config('apiKey');


        $this->notifyUrl = \FUR_Config::get_wechat_pay_config('notify_url');
        $this->wapName = \FUR_Config::get_wechat_pay_config('wap_name');
        $this->returnUrl = \FUR_Config::get_wechat_pay_config('return_url');
        $this->wapUrl = \FUR_Config::get_wechat_pay_config('wap_url');


    }


    public function setTotalFee($totalFee)
    {
        $this->totalFee = $totalFee;
    }

    public function setOutTradeNo($outTradeNo)
    {
        $this->outTradeNo = $outTradeNo;
    }

    public function setOrderName($orderName)
    {
        $this->orderName = $orderName;
    }


    /**
     * 发起h5订单
     * @return array
     */
    public function createH5JsBizPackage(int $totalFee, int $outTradeNo, string $orderName)
    {
        $config = array(
            'mch_id' => $this->mchid,
            'appid' => $this->h5appId,
            'key' => $this->apiKey,
        );
        $scene_info = array(
            'h5_info' => array(
                'type' => 'Wap',
                'wap_url' => $this->wapUrl,
                'wap_name' => $this->wapName,
            )
        );
        $unified = array(
            'appid' => $config['appid'],
            'attach' => 'pay',             //商家数据包，原样返回，如果填写中文，请注意转换为utf-8
            'body' => $orderName,
            'mch_id' => $config['mch_id'],
            'nonce_str' => self::createNonceStr(),
            'notify_url' => $this->notifyUrl,
            'out_trade_no' => $outTradeNo,
            'spbill_create_ip' => $_SERVER['REMOTE_ADDR'],
            'total_fee' => intval($totalFee),       //单位 转为分
            'trade_type' => 'MWEB',
            'scene_info' => json_encode($scene_info)
        );
        $unified['sign'] = self::getSign($unified, $config['key']);
        $responseXml = self::curlPost('https://api.mch.weixin.qq.com/pay/unifiedorder', self::arrayToXml($unified));
        $unifiedOrder = simplexml_load_string($responseXml, 'SimpleXMLElement', LIBXML_NOCDATA);
        if ($unifiedOrder->return_code != 'SUCCESS') {
            die($unifiedOrder->return_msg);
        }
        if ($unifiedOrder->mweb_url) {
            return $unifiedOrder->mweb_url . '&redirect_url=' . urlencode($this->returnUrl);
        }
        exit('error');
    }


    /**
     * 统一下单
     * @param string $openid 调用【网页授权获取用户信息】接口获取到用户在该公众号下的Openid
     * @param float $totalFee 收款总费用 单位元
     * @param string $outTradeNo 唯一的订单号
     * @param string $orderName 订单名称
     * @param string $notifyUrl 支付结果通知url 不要有问号
     * @param string $timestamp 支付时间
     * @return string
     * 微信公众号支付
     */
    public function createJsAPIBizPackage(int $totalFee, int $outTradeNo, string $orderName, int $timestamp, string $code)
    {
        //获取用户openid
        $openId = $this->GetOpenid($code);
        \FUR_Log::info(__FILE__ . __LINE__ . "openid:$openId-------outTradeNo:$outTradeNo--------orderName:$orderName---timestamp:$timestamp-----code:$code");
        //获取openid
        if (!$openId) exit('获取openid失败');
        $config = array(
            'mch_id' => $this->mchid,
            'appid' => $this->h5appId,
            'key' => $this->appSecret,
        );
        \FUR_Log::info(__FILE__ . __LINE__ . "configArr:" . json_encode($config));
        $unified = array(
            'appid' => $config['appid'],
            'attach' => 'pay',             //商家数据包，原样返回，如果填写中文，请注意转换为utf-8
            'body' => $orderName,
            'mch_id' => $config['mch_id'],
            'nonce_str' => self::createNonceStr(),
            'notify_url' => $this->notifyUrl,
            'openid' => $openId,         //rade_type=JSAPI，此参数必传
            'out_trade_no' => $outTradeNo,
            'spbill_create_ip' => '127.0.0.1',
            'total_fee' => $totalFee,       //单位 转为分
            'trade_type' => 'JSAPI',
        );
        $unified['sign'] = self::getSign($unified, $config['key']);
        \FUR_Log::info(__FILE__ . __LINE__ . "------unified------", json_encode($unified));
        $responseXml = self::curlPost('https://api.mch.weixin.qq.com/pay/unifiedorder', self::arrayToXml($unified));
        //禁止引用外部xml实体
        libxml_disable_entity_loader(true);
        $unifiedOrder = simplexml_load_string($responseXml, 'SimpleXMLElement', LIBXML_NOCDATA);
        \FUR_Log::info(__FILE__ . __LINE__ . "--------responseXml:-------", json_encode($responseXml));
        \FUR_Log::info(__FILE__ . __LINE__ . "--------unifiedOrder:--------", json_encode($unifiedOrder));
        if ($unifiedOrder === false) {
            die('parse xml error');
        }
        if ($unifiedOrder->return_code != 'SUCCESS') {
            die($unifiedOrder->return_msg);
        }
        if ($unifiedOrder->result_code != 'SUCCESS') {
            die($unifiedOrder->err_code);
        }
        $arr = array(
            "appId" => $config['appid'],
            "timeStamp" => "$timestamp",        //这里是字符串的时间戳，不是int，所以需加引号
            "nonceStr" => self::createNonceStr(),
            "package" => "prepay_id=" . $unifiedOrder->prepay_id,
            "signType" => 'MD5',
        );
        $arr['paySign'] = self::getSign($arr, $config['key']);
        \FUR_Log::info(__FILE__ . __LINE__ . "-------返回arr:-------", json_encode($arr));
        return $arr;
    }


    public function createMiniBizPackage($totalFee, $outTradeNo, $orderName, $timestamp, $openId = null, $code = null)
    {


        $this->mchid = \FUR_Config::get_wechat_pay_config('mchid');
        //获取用户openid
        if ($openId == null) {
            $openId = $this->GetMiniOpenid($code);
        }
        //获取openid
        if (!$openId) exit('获取openid失败');
        $config = array(
            'mch_id' => \FUR_Config::get_wechat_pay_config('mch_id'),
            'app_id' => \FUR_Config::get_wechat_pay_config('miniAppId'),
            'mch_key' => \FUR_Config::get_wechat_pay_config('mch_key'),
            'notify_url' => \FUR_Config::get_wechat_pay_config('notify_url')
        );
        \FUR_Log::info(__FILE__ . __LINE__ . "configArr:" . json_encode($config));
        $unified = array(
            'appid' => $config['app_id'],
            'attach' => 'pay',             //商家数据包，原样返回，如果填写中文，请注意转换为utf-8
            'body' => $orderName,
            'mch_id' => $config['mch_id'],
            'nonce_str' => self::createNonceStr(),
            'notify_url' => $config['notify_url'],
            'openid' => $openId,         //rade_type=JSAPI，此参数必传
            'out_trade_no' => $outTradeNo,
            'spbill_create_ip' => \FUR_Helper::get_ip(),
            'total_fee' => $totalFee,       //单位 转为分
            'trade_type' => 'JSAPI',
            'sign_type' => 'MD5'
        );
        $unified['sign'] = self::getSign($unified, $config['mch_key']);
        \FUR_Log::info(__FILE__ . __LINE__ . "------unified------", json_encode($unified));
        $xmlData = self::arrayToXml($unified);
        $responseXml = self::curlPost('https://api.mch.weixin.qq.com/pay/unifiedorder', $xmlData);
        //禁止引用外部xml实体
        libxml_disable_entity_loader(true);
        $unifiedOrder = simplexml_load_string($responseXml, 'SimpleXMLElement', LIBXML_NOCDATA);
        \FUR_Log::info(__FILE__ . __LINE__ . "--------responseXml:-------", json_encode($responseXml));
        if ($unifiedOrder === false) {
            die('parse xml error');
        }
        if ($unifiedOrder->return_code != 'SUCCESS') {
            die($unifiedOrder->return_msg);
        }
        if ($unifiedOrder->result_code != 'SUCCESS') {
            die($unifiedOrder->err_code);
        }
        $arr = array(
            "appId" => $config['app_id'],
            "timeStamp" => "$timestamp",        //这里是字符串的时间戳，不是int，所以需加引号
            "nonceStr" => self::createNonceStr(),
            "package" => "prepay_id=" . $unifiedOrder->prepay_id,
            "signType" => 'MD5',
        );
        $arr['paySign'] = self::getSign($arr, $config['mch_key']);
        \FUR_Log::info(__FILE__ . __LINE__ . "-------返回arr:-------", json_encode($arr));
        return $arr;
    }


    /**
     * 通过跳转获取用户的openid，跳转流程如下：
     * 1、设置自己需要调回的url及其其他参数，跳转到微信服务器https://open.weixin.qq.com/connect/oauth2/authorize
     * 2、微信服务处理完成之后会跳转回用户redirect_uri地址，此时会带上一些参数，如：code
     * @return 用户的openid
     */
    public function GetOpenid($code)
    {
        //通过code获得openid
        if ($code == null) {
            //触发微信返回code码
            $scheme = 'http://';
            $uri = $_SERVER['PHP_SELF'] . $_SERVER['QUERY_STRING'];
            if ($_SERVER['REQUEST_URI']) $uri = $_SERVER['REQUEST_URI'];
            $baseUrl = urlencode($scheme . $_SERVER['HTTP_HOST'] . $uri);
            $url = $this->__CreateOauthUrlForCode($baseUrl);
            Header("Location: $url");
            exit();
        } else {
            //获取code码，以获取openid
            \FUR_Log::info(__FILE__ . __LINE__ . "-----------已经传入code");
            $openid = $this->GetOpenidFromMp($code);
            return $openid;
        }
    }


    /**
     * 通过跳转获取用户的openid，跳转流程如下：
     * 1、设置自己需要调回的url及其其他参数，跳转到微信服务器https://open.weixin.qq.com/connect/oauth2/authorize
     * 2、微信服务处理完成之后会跳转回用户redirect_uri地址，此时会带上一些参数，如：code
     * @return 用户的openid
     */
    public function GetMiniOpenid($code)
    {
        //通过code获得openid
        if ($code == null) {
            //触发微信返回code码
            $scheme = 'http://';
            $uri = $_SERVER['PHP_SELF'] . $_SERVER['QUERY_STRING'];
            if ($_SERVER['REQUEST_URI']) $uri = $_SERVER['REQUEST_URI'];
            $baseUrl = urlencode($scheme . $_SERVER['HTTP_HOST'] . $uri);
            $url = $this->__CreateMiniOauthUrlForCode($baseUrl);
            Header("Location: $url");
            exit();
        } else {
            //获取code码，以获取openid
            \FUR_Log::info(__FILE__ . __LINE__ . "-----------已经传入code");
            return $this->GetMiniOpenidFromMp($code);
        }
    }


    /**
     * 构造获取code的url连接
     * @param string $redirectUrl 微信服务器回跳的url，需要url编码
     * @return 返回构造好的url
     */
    private function __CreateOauthUrlForCode($redirectUrl)
    {
        $urlObj["appid"] = $this->h5appId;
        $urlObj["redirect_uri"] = "$redirectUrl";
        $urlObj["response_type"] = "code";
        $urlObj["scope"] = "snsapi_base";
        $urlObj["state"] = "STATE" . "#wechat_redirect";
        $bizString = $this->ToUrlParams($urlObj);
        return "https://open.weixin.qq.com/connect/oauth2/authorize?" . $bizString;
    }

    /**
     * 构造获取code的url连接
     * @param string $redirectUrl 微信服务器回跳的url，需要url编码
     * @return 返回构造好的url
     */
    private function __CreateMiniOauthUrlForCode($redirectUrl)
    {
        //小程序
        $urlObj["appid"] = $this->miniAppId;
        $urlObj["redirect_uri"] = "$redirectUrl";
        $urlObj["response_type"] = "code";
        $urlObj["scope"] = "snsapi_base";
        $urlObj["state"] = "STATE" . "#wechat_redirect";
        $bizString = $this->ToUrlParams($urlObj);
        return "https://open.weixin.qq.com/connect/oauth2/authorize?" . $bizString;
    }


    /**
     * 拼接签名字符串
     * @param array $urlObj
     * @return 返回已经拼接好的字符串
     */
    private function ToUrlParams($urlObj)
    {
        $buff = "";
        foreach ($urlObj as $k => $v) {
            if ($k != "sign") $buff .= $k . "=" . $v . "&";
        }
        $buff = trim($buff, "&");
        return $buff;
    }

    /**
     * 通过code从工作平台获取openid机器access_token
     * @param string $code 微信跳转回来带上的code
     * @return openid
     */
    public function GetOpenidFromMp($code)
    {
        $url = $this->__CreateOauthUrlForOpenid($code);
        $res = self::curlGet($url);
        //取出openid
        $data = json_decode($res, true);
        $this->data = $data;
        \FUR_Log::info(__LINE__ . "data:", json_encode($data));
        $openid = $data['openid'];
        return $openid;
    }


    /**
     * 通过code从工作平台获取openid机器access_token
     * @param string $code 微信跳转回来带上的code
     * @return openid
     */
    public function GetMiniOpenidFromMp($code)
    {
        $url = $this->getWechatMiniOpenid($code);
        $res = self::curlGet($url);
        //取出openid
        $data = json_decode($res, true);
        $this->data = $data;
        \FUR_Log::info(__LINE__ . "data:", json_encode($data));
        $openid = $data['openid'];
        return $openid;
    }


    /**
     * 构造获取open和access_toke的url地址
     * @param string $code，微信跳转带回的code
     * @return 请求的url
     */
    private function __CreateOauthUrlForOpenid($code)
    {
        $urlObj["appid"] = $this->h5appId;
        $urlObj["secret"] = $this->appKey;

        $urlObj["code"] = $code;
        $urlObj["grant_type"] = "authorization_code";
        $bizString = $this->ToUrlParams($urlObj);
        \FUR_Log::info(__LINE__ . "__CreateOauthUrlForOpenid:", json_encode($urlObj));
        return "https://api.weixin.qq.com/sns/oauth2/access_token?" . $bizString;
    }


    private function getWechatMiniOpenid($code)
    {

        $urlObj["appid"] = $this->miniAppId;
        $urlObj["secret"] = $this->miniAppSecret;

        $urlObj["js_code"] = $code;
        $urlObj["grant_type"] = "authorization_code";
        $bizString = http_build_query($urlObj);
        \FUR_Log::info(__LINE__ . "__CreateOauthUrlForOpenid:", json_encode($urlObj));
        return "https://api.weixin.qq.com/sns/jscode2session?" . $bizString;
    }


    /**
     * 发起订单
     * @param float $totalFee 收款总费用 单位分
     * @param string $outTradeNo 唯一的订单号
     * @param string $orderName 订单名称
     * @param string $notifyUrl 支付结果通知url 不要有问号
     * @param string $timestamp 订单发起时间
     * @return array
     */
    public function createJsBizPackage(int $totalFee, int $outTradeNo, string $orderName, int $timestamp)
    {
        $config = array(
            'mch_id' => $this->mchid,
            'appid' => $this->appid,
            'key' => $this->apiKey,
        );
        $unified = array(
            'appid' => $config['appid'],
            'attach' => 'pay',             //商家数据包，原样返回，如果填写中文，请注意转换为utf-8
            'body' => $orderName,
            'mch_id' => $config['mch_id'],
            'nonce_str' => self::createNonceStr(),
            'notify_url' => $this->notifyUrl,
            'out_trade_no' => $outTradeNo,
            'spbill_create_ip' => '127.0.0.1',
            'total_fee' => $totalFee,       //单位 转为分
            'trade_type' => 'NATIVE',
        );
        $unified['sign'] = self::getSign($unified, $config['key']);
        $responseXml = self::curlPost('https://api.mch.weixin.qq.com/pay/unifiedorder', self::arrayToXml($unified));
        //禁止引用外部xml实体
        libxml_disable_entity_loader(true);
        $unifiedOrder = simplexml_load_string($responseXml, 'SimpleXMLElement', LIBXML_NOCDATA);
        if ($unifiedOrder === false) {
            die('parse xml error');
        }
        if ($unifiedOrder->return_code != 'SUCCESS') {
            die($unifiedOrder->return_msg);
        }
        if ($unifiedOrder->result_code != 'SUCCESS') {
            die($unifiedOrder->err_code);
        }
        $codeUrl = (array)($unifiedOrder->code_url);
        if (!$codeUrl[0]) exit('get code_url error');
        $prepay_id = json_decode(json_encode($unifiedOrder->prepay_id), TRUE);
        $arr = array(
            "appid" => $config['appid'],
            "timestamp" => $timestamp,
            "noncestr" => self::createNonceStr(),
            'partnerid' => $config['mch_id'],
            'prepayid' => $prepay_id[0],
            'package' => 'Sign=WXPay',
        );
        $arr['sign'] = self::getSign($arr, $config['key']);
        return $arr;
    }


    /**
     * @param $outTradeNo
     * @return mixed
     * 订单查询
     */
    public function orderquery($outTradeNo)
    {
        $config = array(
            'mch_id' => $this->mchid,
            'appid' => $this->appid,
            'key' => $this->apiKey,
        );
        $unified = array(
            'appid' => $config['appid'],
            'mch_id' => $config['mch_id'],
            'out_trade_no' => $outTradeNo,
            'nonce_str' => self::createNonceStr(),
        );
        $unified['sign'] = self::getSign($unified, $config['key']);
        $responseXml = self::curlPost('https://api.mch.weixin.qq.com/pay/orderquery', self::arrayToXml($unified));
        $queryResult = simplexml_load_string($responseXml, 'SimpleXMLElement', LIBXML_NOCDATA);

        \FUR_Log::info("wechat_orderquery_result", json_encode($queryResult));
        if ($queryResult === false) {
            die('parse xml error');
        }
        if ($queryResult->return_code != 'SUCCESS') {
            die($queryResult->return_msg);
        }
        $trade_state = $queryResult->trade_state;
        $data['code'] = $trade_state == 'SUCCESS' ? 0 : 1;
        $data['data'] = $trade_state;
        $data['msg'] = $this->getTradeSTate($trade_state);
        $data['time'] = date('Y-m-d H:i:s');
        return $data;
    }

    public function getTradeSTate($str)
    {
        switch ($str) {
            case 'SUCCESS';
                return '支付成功';
            case 'REFUND';
                return '转入退款';
            case 'NOTPAY';
                return '未支付';
            case 'CLOSED';
                return '已关闭';
            case 'REVOKED';
                return '已撤销（刷卡支付）';
            case 'USERPAYING';
                return '用户支付中';
            case 'PAYERROR';
                return '支付失败';
        }
    }


    /**
     * curl get
     *
     * @param string $url
     * @param array $options
     * @return mixed
     */
    public static function curlGet($url = '', $options = array())
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        if (!empty($options)) {
            curl_setopt_array($ch, $options);
        }
        //https请求 不验证证书和host
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }

    public static function curlPost($url = '', $postData = '', $options = array())
    {
        if (is_array($postData)) {
            $postData = http_build_query($postData);
        }
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30); //设置cURL允许执行的最长秒数
        if (!empty($options)) {
            curl_setopt_array($ch, $options);
        }
        //https请求 不验证证书和host
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }

    public static function createNonceStr($length = 16)
    {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $str = '';
        for ($i = 0; $i < $length; $i++) {
            $str .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
        }
        return $str;
    }

    public static function arrayToXml($arr)
    {
        $xml = "<xml>";
        foreach ($arr as $key => $val) {
            if (is_numeric($val)) {
                $xml .= "<" . $key . ">" . $val . "</" . $key . ">";
            } else
                $xml .= "<" . $key . "><![CDATA[" . $val . "]]></" . $key . ">";
        }
        $xml .= "</xml>";
        return $xml;
    }

    /**
     * 获取签名
     */
    public static function getSign($params, $key)
    {
        ksort($params, SORT_STRING);
        $unSignParaString = self::formatQueryParaMap($params, false);
        $signStr = strtoupper(md5($unSignParaString . "&key=" . $key));
        return $signStr;
    }

    protected static function formatQueryParaMap($paraMap, $urlEncode = false)
    {
        $buff = "";
        ksort($paraMap);
        foreach ($paraMap as $k => $v) {
            if (null != $v && "null" != $v) {
                if ($urlEncode) {
                    $v = urlencode($v);
                }
                $buff .= $k . "=" . $v . "&";
            }
        }
        $reqPar = '';
        if (strlen($buff) > 0) {
            $reqPar = substr($buff, 0, strlen($buff) - 1);
        }
        return $reqPar;
    }

}
