<?php

namespace service\organization\impl;


use facade\request\organization\EditOrganizationCompanyParams;
use facade\request\organization\EditOrganizationStoreParams;
use facade\request\organization\QueryOrganizationCompanyParams;
use facade\request\organization\QueryOrganizationStoreParams;
use facade\response\Result;
use facade\service\organization\OrganizationCompanyAccountService;
use facade\service\organization\OrganizationService;
use lib\TimeHelper;
use model\organization\dto\OrganizationCompanyDTO;
use model\organization\dto\OrganizationStoreDTO;
use model\organization\OrganizationCompanyModel;
use model\organization\OrganizationStoreModel;


class OrganizationCompanyAccountServiceImpl implements OrganizationCompanyAccountService
{


    public static $_instance;


    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    private function encryptPassword($password)
    {
        return md5(\Config_Const::COMPANY_ACCOUNT_SALT . $password);
    }

    public function LoginByAccountName(?string $accountName, ?string $password): ?Result
    {
        $result = new Result();
        $organizationCompanyDTO = OrganizationCompanyModel::getInstance()->queryCompanyByAccountName($accountName);
        if ($organizationCompanyDTO == null) {
            $result->setError(\Config_Error::ERR_COMPANY_ACCOUNT_NOT_EXIST);
            return $result;
        }
        if ($organizationCompanyDTO->company_status != \Config_Const::COMPANY_STATUS_VALID) {
            $result->setError(\Config_Error::ERR_COMPANY_ACCOUNT_FROZEN);
            return $result;
        }
        $encryptPassword = $this->encryptPassword($password);
        if ($encryptPassword != $organizationCompanyDTO->account_password) {
            $result->setError(\Config_Error::ERR_COMPANY_PASSWORD_IS_ERROR);
            return $result;
        }

        $result->setSuccessWithResult($organizationCompanyDTO->id);
        return $result;
    }

    public function queryCompanyByAccountName(?string $accountName): ?Result
    {
        $result = new Result();
        $organizationCompanyDTO = OrganizationCompanyModel::getInstance()->queryCompanyByAccountName($accountName);
        $result->setSuccessWithResult($organizationCompanyDTO);
        return $result;
    }
}