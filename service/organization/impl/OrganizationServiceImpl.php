<?php

namespace service\organization\impl;


use facade\request\organization\EditOrganizationCompanyParams;
use facade\request\organization\EditOrganizationStoreParams;
use facade\request\organization\QueryOrganizationCompanyParams;
use facade\request\organization\QueryOrganizationStoreParams;
use facade\response\Result;
use facade\service\organization\OrganizationService;
use lib\TimeHelper;
use model\organization\dto\OrganizationCompanyDTO;
use model\organization\dto\OrganizationStoreDTO;
use model\organization\dto\OrganizationStoreManagerDTO;
use model\organization\OrganizationCompanyModel;
use model\organization\OrganizationStoreModel;


class OrganizationServiceImpl implements OrganizationService
{


    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function createCompany(EditOrganizationCompanyParams $params): ?Result
    {
        $result = new Result();
        $organizationCompanyDTO = new OrganizationCompanyDTO();
        \FUR_Core::copyProperties($params, $organizationCompanyDTO);
        $accountRecord = OrganizationCompanyModel::getInstance()->queryCompanyByAccountName($params->account_name);
        if ($accountRecord != null) {
            $result->setError(\Config_Error::ERR_COMPANY_ACCOUNT_HAS_EXIST);
            return $result;
        }
        $organizationCompanyDTO->company_status = \Config_Const::COMPANY_STATUS_VALID;
        $organizationCompanyDTO->created_timestamp = TimeHelper::getTimeMs();
        $organizationCompanyDTO->account_password = $this->encryptPassword($params->account_password);
        $id = OrganizationCompanyModel::getInstance()->insertCompany($organizationCompanyDTO);
        $result->setSuccessWithResult($id);
        return $result;
    }

    public function getCompanyInfo(int $companyId): ?Result
    {
        $result = new Result();
        $organizationCompanyDTO = OrganizationCompanyModel::getInstance()->queryCompanyById($companyId);
        if ($organizationCompanyDTO == null) {
            $result->setError(\Config_Error::ERR_COMPANY_NOT_FOUND);
            return $result;
        }
        $result->setSuccessWithResult($organizationCompanyDTO);
        return $result;
    }

    public function getCompanyList(QueryOrganizationCompanyParams $params): ?Result
    {
        $result = new Result();

        $organizationCompanyList = OrganizationCompanyModel::getInstance()->queryCompanyList($params);
        $result->setSuccessWithResult($organizationCompanyList);
        return $result;
    }

    public function getCompanyCount(QueryOrganizationCompanyParams $params): ?Result
    {
        $result = new Result();
        $organizationCompanyCount = OrganizationCompanyModel::getInstance()->queryCompanyCount($params);
        $result->setSuccessWithResult($organizationCompanyCount);
        return $result;
    }

    public function updateCompanyInfo(EditOrganizationCompanyParams $params): ?Result
    {
        $result = new Result();

        if ($params->account_name) {
            $accountRecord = OrganizationCompanyModel::getInstance()->queryCompanyByAccountName($params->account_name);
            if ($accountRecord != null && $accountRecord->id != $params->id) {
                $result->setError(\Config_Error::ERR_COMPANY_ACCOUNT_HAS_EXIST);
                return $result;
            }
        }


        $organizationCompanyDTO = new OrganizationCompanyDTO();
        \FUR_Core::copyProperties($params, $organizationCompanyDTO);
        $organizationCompanyDTO->updated_timestamp = TimeHelper::getTimeMs();

        if ($organizationCompanyDTO->account_password) {
            $organizationCompanyDTO->account_password = $this->encryptPassword($organizationCompanyDTO->account_password);
        }
        $updateCount = OrganizationCompanyModel::getInstance()->updateCompany($organizationCompanyDTO);
        $result->setSuccessWithResult($updateCount);
        return $result;
    }

    public function deleteCompany(int $companyId): ?Result
    {
        $result = new Result();
        $updateCount = OrganizationCompanyModel::getInstance()->updateCompanyStatus($companyId, \Config_Const::COMPANY_STATUS_DELETED);
        $result->setSuccessWithResult($updateCount);
        return $result;
    }


    public function reactivateCompany(int $companyId): ?Result
    {
        $result = new Result();
        $updateCount = OrganizationCompanyModel::getInstance()->updateCompanyStatus($companyId, \Config_Const::COMPANY_STATUS_VALID);
        $result->setSuccessWithResult($updateCount);
        return $result;
    }

    public function createCompanyStore(EditOrganizationStoreParams $params): ?Result
    {
        $result = new Result();
        $organizationStoreDTO = new OrganizationStoreDTO();
        \FUR_Core::copyProperties($params, $organizationStoreDTO);
        $organizationStoreDTO->store_status = \Config_Const::STORE_STATUS_VALID;
        $organizationStoreDTO->created_timestamp = TimeHelper::getTimeMs();
        $id = OrganizationStoreModel::getInstance()->insertCompanyStore($organizationStoreDTO);
        $result->setSuccessWithResult($id);
        return $result;
    }

    public function getStoreInfo(int $storeId): ?Result
    {
        $result = new Result();
        $organizationStoreDTO = OrganizationStoreModel::getInstance()->queryCompanyStoreById($storeId);
        if ($organizationStoreDTO == null) {
            $result->setError(\Config_Error::ERR_STORE_NOT_FOUND);
            return $result;
        }
        $result->setSuccessWithResult($organizationStoreDTO);
        return $result;
    }

    public function queryStoreList(QueryOrganizationStoreParams $queryParams): ?Result
    {
        $result = new Result();
        $organizationStoreList = OrganizationStoreModel::getInstance()->queryCompanyStoreList($queryParams);
        $result->setSuccessWithResult($organizationStoreList);
        return $result;
    }

    public function queryStoreCount(QueryOrganizationStoreParams $queryParams): ?Result
    {
        $result = new Result();
        $organizationStoreCount = OrganizationStoreModel::getInstance()->queryCompanyStoreCount($queryParams);
        $result->setSuccessWithResult($organizationStoreCount);
        return $result;
    }

    public function updateStoreInfo(EditOrganizationStoreParams $params): ?Result
    {
        $result = new Result();
        $organizationStoreDTO = new OrganizationStoreDTO();
        \FUR_Core::copyProperties($params, $organizationStoreDTO);
        $organizationStoreDTO->updated_timestamp = TimeHelper::getTimeMs();
        $updateCount = OrganizationStoreModel::getInstance()->updateCompanyStore($organizationStoreDTO);
        $result->setSuccessWithResult($updateCount);
        return $result;
    }

    public function deleteStore(int $storeId): ?Result
    {
        $result = new Result();
        $deleteCount = OrganizationStoreModel::getInstance()->deleteCompanyStore($storeId);
        $result->setSuccessWithResult($deleteCount);
        return $result;
    }

    private function encryptPassword($password)
    {
        return md5(\Config_Const::COMPANY_ACCOUNT_SALT . $password);
    }

    public function updateCompanyStoreManager(?int $storeId, ?array $managerInfoList): ?Result
    {
        $result = new Result();
        if ($storeId == null || $managerInfoList == null) {
            return $result;
        }
        OrganizationStoreModel::getInstance()->cleanStoreManager($storeId);
        foreach ($managerInfoList as $managerInfo) {
            $storeManager = new OrganizationStoreManagerDTO();
            $storeManager->created_timestamp = TimeHelper::getTimeMs();
            $storeManager->store_id = $storeId;
            $storeManager->store_manager = $managerInfo->store_manager;
            $storeManager->contact_info = $managerInfo->contact_info;
            OrganizationStoreModel::getInstance()->insertStoreManagerInfo($storeManager);
        }
        $result->setSuccessWithResult(true);
        return $result;
    }


    public function getStoreManagerList(?int $storeId): ?Result
    {
        $result = new Result();
        if ($storeId == null) {
            return $result;
        }
        $managerList = OrganizationStoreModel::getInstance()->getStoreManagerList($storeId);
        $result->setSuccessWithResult($managerList);
        return $result;
    }


    public function queryStoreInfoByName(int $companyId, string $storeNum, string $storeName): ?Result
    {
        $result = new Result();
        $data = OrganizationStoreModel::getInstance()->queryCompanyStoreByName($companyId, $storeNum, $storeName);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function queryStoreManagerByPhone(string $phoneNum): ?Result
    {
        $result = new Result();
        $data = OrganizationStoreModel::getInstance()->queryLatestStoreManagerByPhoneNum($phoneNum);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function queryStoreManagerListByPhoneNum(string $phoneNum): ?Result
    {
        $result = new Result();
        $data = OrganizationStoreModel::getInstance()->queryStoreManagerListByPhoneNum($phoneNum);
        $result->setSuccessWithResult($data);
        return $result;
    }
}