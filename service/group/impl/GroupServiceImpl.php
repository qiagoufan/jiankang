<?php


namespace service\group\impl;

use facade\request\feed\CreateFeedParams;
use facade\request\feed\GetFeedListParams;
use facade\request\feed\group\ApplyManagerParams;
use facade\request\maintain\CreateLeaderboardRecordParams;
use facade\response\Result;
use facade\service\feed\FeedService;
use facade\service\group\GroupService;
use model\dto\FeedReceiveDTO;
use model\dto\FeedSendDTO;
use model\dto\MaintainLeaderboardRecordDTO;
use model\dto\MaintainUserStarExtraDTO;
use model\dto\UserDTO;
use model\dto\UserExtraDTO;
use model\dto\UserStarDTO;
use model\FeedModel;
use model\group\dto\GroupManagerApplyFormDTO;
use model\group\dto\GroupManagerDTO;
use model\group\dto\GroupManageViolationRecordDTO;
use model\group\GroupModel;
use model\ProductSkuModel;
use model\UserFollowModel;
use model\dto\FeedInfoDTO;
use service\maintain\impl\MaintainLeaderboardServiceImpl;
use service\user\impl\FollowServiceImpl;
use service\user\impl\StarServiceImpl;
use service\user\impl\UserExtraServiceImpl;


class GroupServiceImpl implements GroupService
{

    public static $_instance;

    const CACHE_TIME = 60;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function applyManager(ApplyManagerParams $applyManagerParams): ?Result
    {
        $result = new Result();
        $applyFormDTO = new GroupManagerApplyFormDTO();
        \FUR_Core::copyProperties($applyManagerParams, $applyFormDTO);
        $applyFormDTO->created_timestamp = time();
        $applyFormDTO->form_status = GroupManagerApplyFormDTO::FORM_STATUS_NEW;
        $id = GroupModel::getInstance()->createApplyForm($applyFormDTO);
        $result->setSuccessWithResult($id);
        return $result;

    }


    public function queryUserManagerType(int $userId, ?int $starId = 0): ?Result
    {
        $result = new Result();
        $managerType = GroupManagerDTO::MANAGER_TYPE_USER;
        $groupManagerDTO = GroupModel::getInstance()->queryUserManagerInfo($starId, $userId);
        if ($groupManagerDTO != null) {
            $managerType = $groupManagerDTO->manager_type;
        }
        $result->setSuccessWithResult($managerType);
        return $result;
    }

    public function queryUserApplyHistory(int $userId): ?Result
    {
        $result = new Result();
        $applyFormList = GroupModel::getInstance()->queryUserApplyHistoryList($userId);
        $result->setSuccessWithResult($applyFormList);
        return $result;
    }

    public function queryManagerList(int $starId): ?Result
    {
        $result = new Result();
        $groupManagerList = GroupModel::getInstance()->queryAllManagerList($starId);
        $result->setSuccessWithResult($groupManagerList);
        return $result;
    }

    public function queryGroupOwner(int $starId): ?Result
    {
        $result = new Result();
        $owner = GroupModel::getInstance()->queryGroupOwner($starId);
        $result->setSuccessWithResult($owner);
        return $result;
    }

    public function queryManagerApplyFormList(int $starId): ?Result
    {
        $result = new Result();
        $groupManagerList = GroupModel::getInstance()->queryManagerApplyFormList($starId);
        $result->setSuccessWithResult($groupManagerList);
        return $result;
    }

    public function queryOwnerApplyFormList(?int $offset = 0, ?int $pageSize = 20): ?Result
    {
        $result = new Result();
        $groupManagerList = GroupModel::getInstance()->queryOwnerApplyFormList($offset, $pageSize);
        $result->setSuccessWithResult($groupManagerList);
        return $result;
    }


    public function queryOwnerApplyFormCount(): ?Result
    {
        $result = new Result();
        $count = GroupModel::getInstance()->queryOwnerApplyFormCount();
        $result->setSuccessWithResult($count);
        return $result;
    }

    public function queryApplyForm(int $applyFormId): ?Result
    {
        $result = new Result();
        $applyFormDTO = GroupModel::getInstance()->queryApplyFormDetail($applyFormId);

        $result->setSuccessWithResult($applyFormDTO);
        return $result;
    }


    public function updateApplyFormStatus(int $applyFormId, int $applyFormStatus): ?Result
    {
        $result = new Result();
        $updateNum = GroupModel::getInstance()->updateApplyFormStatus($applyFormId, $applyFormStatus);
        $result->setSuccessWithResult($updateNum);
        return $result;
    }

    public function queryManagerNum(int $starId, int $managerType): ?Result
    {
        $result = new Result();
        $applyFormDTO = GroupModel::getInstance()->queryManagerNum($starId, $managerType);
        $result->setSuccessWithResult($applyFormDTO);
        return $result;
    }

    public function createGroupManager(int $userId, int $starId, int $managerType): ?Result
    {
        $result = new Result();
        $groupManagerDTO = new GroupManagerDTO();
        $groupManagerDTO->manager_user_id = $userId;
        $groupManagerDTO->manager_type = $managerType;
        $groupManagerDTO->star_id = $starId;
        $groupManagerDTO->created_timestamp = time();

        $id = GroupModel::getInstance()->createGroupManager($groupManagerDTO);
        $result->setSuccessWithResult($id);
        return $result;
    }

    public function createViolationRecord(GroupManageViolationRecordDTO $groupManageViolationRecordDTO): ?Result
    {
        $result = new Result();
        $id = GroupModel::getInstance()->createViolationRecord($groupManageViolationRecordDTO);
        $result->setSuccessWithResult($id);
        return $result;
    }

    public function deleteGroupManager(int $userId, int $starId): ?Result
    {
        $result = new Result();
        $ret = GroupModel::getInstance()->deleteGroupManager($userId, $starId);
        $result->setSuccessWithResult($ret);
        return $result;
    }

    public function cleanUserGroupManager(int $userId): ?Result
    {
        $result = new Result();
        $ret = GroupModel::getInstance()->cleanUserGroupManager($userId);
        $result->setSuccessWithResult($ret);
        return $result;
    }
}