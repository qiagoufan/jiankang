<?php

namespace service\maintain\impl;

use facade\request\maintain\CreateLeaderboardRecordParams;
use facade\request\maintain\UpdateStarContributionParams;
use facade\response\Result;
use facade\service\maintain\MaintainLeaderboardService;
use model\dto\MaintainLeaderboardRecordDTO;
use model\dto\MaintainUserStarExtraDTO;
use model\maintain\MaintainLeaderboardRecordModel;
use model\maintain\MaintainUserStarExtraModel;

class MaintainLeaderboardServiceImpl implements MaintainLeaderboardService
{


    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    /**
     * @param CreateLeaderboardRecordParams $createLeaderboardRecordParams
     * @return Result
     * 插入打榜记录
     */
    public function createRecord(CreateLeaderboardRecordParams $createLeaderboardRecordParams): Result
    {
        $result = new Result();

        /**
         * 插入用户打榜记录表
         */
        $maintainLeaderboardRecordDTO = new MaintainLeaderboardRecordDTO();
        \FUR_Core::copyProperties($createLeaderboardRecordParams, $maintainLeaderboardRecordDTO);
        $maintainLeaderboardRecordDTO->created_timestamp = time();

        $leaderboardRecordModel = MaintainLeaderboardRecordModel::getInstance();
        $leaderboardRecordModel->insertRecord($maintainLeaderboardRecordDTO);
        $rankType = null;


        /**
         * 插入用户与ip的互动信息统计表，先检查是否有记录
         */

        $userStarExtraModel = MaintainUserStarExtraModel::getInstance();
        $userStarRecord = $userStarExtraModel->queryUserRecord($createLeaderboardRecordParams->user_id, $createLeaderboardRecordParams->star_id);

        if ($userStarRecord == null) {
            $maintainUserStarExtraDTO = new MaintainUserStarExtraDTO();
            \FUR_Core::copyProperties($createLeaderboardRecordParams, $maintainUserStarExtraDTO);
            $maintainUserStarExtraDTO->leaderboard_total_value += $createLeaderboardRecordParams->value;
            $maintainUserStarExtraDTO->created_timestamp = time();
            $userStarExtraModel->insertRecord($maintainUserStarExtraDTO);
        } else {
            $maintainUserStarExtraDTO = new MaintainUserStarExtraDTO();
            \FUR_Core::copyProperties($createLeaderboardRecordParams, $maintainUserStarExtraDTO);
            $maintainUserStarExtraDTO->leaderboard_total_value = $userStarRecord->leaderboard_total_value + $createLeaderboardRecordParams->value;
            $maintainUserStarExtraDTO->updated_timestamp = time();
            $userStarExtraModel->updateRecord($maintainUserStarExtraDTO);
        }
        //更新人气排行榜
        $score = $leaderboardRecordModel->getStarScore($createLeaderboardRecordParams->star_id, MaintainUserStarExtraDTO::RANK_TYPE_IDOL_POPULARITY);
        $score = intval($score) + $createLeaderboardRecordParams->value;
        //如果结果为负数考虑一个兼容性
        if ($score < 0) {
            $result->setSuccessWithResult(true);
            return $result;
        }
        if ($createLeaderboardRecordParams->rank_type != null) {
            $leaderboardRecordModel->updateStarRank($createLeaderboardRecordParams->star_id, $score, $createLeaderboardRecordParams->rank_type);
        }


        //更新贡献值排行
        $updateStarContributionParams = new UpdateStarContributionParams();
        $updateStarContributionParams->user_id = $createLeaderboardRecordParams->user_id;
        $updateStarContributionParams->star_id = $createLeaderboardRecordParams->star_id;
        $updateStarContributionParams->value = $createLeaderboardRecordParams->value;
        $this->updateStarContribution($updateStarContributionParams);


        $result->setSuccessWithResult(true);
        return $result;

    }


    /**
     * @param int $userId
     * @param int $starId
     * @return Result|null
     * 查询统计记录
     */
    public function queryRecord(int $userId, int $starId): ?Result
    {
        $result = new Result();
        $feedInformationDTO = MaintainUserStarExtraModel::getInstance()->queryUserRecord($userId, $starId);
        $result->setSuccessWithResult($feedInformationDTO);
        return $result;
    }


    public function getUserStarPopularityValueByType(int $starId, int $userId, array $recordTypeList): Result
    {
        $result = new Result();
        $value = MaintainLeaderboardRecordModel::getInstance()->queryTotalValueByType($userId, $starId, $recordTypeList);
        $result->setSuccessWithResult($value);
        return $result;
    }

    public function getStarPopularityRank(int $starId): Result
    {
        $result = new Result();
        $rank = MaintainLeaderboardRecordModel::getInstance()->getStarRank($starId, MaintainUserStarExtraDTO::RANK_TYPE_IDOL_POPULARITY);
        $result->setSuccessWithResult($rank);
        return $result;

    }

    public function getStarPopularityValue(int $starId): Result
    {
        $result = new Result();
        $rank = MaintainLeaderboardRecordModel::getInstance()->getStarScore($starId, MaintainUserStarExtraDTO::RANK_TYPE_IDOL_POPULARITY);
        $result->setSuccessWithResult(intval($rank));
        return $result;

    }

    public function getStarPopularityLeaderboard(int $index, int $pageSize): Result
    {
        $result = new Result();
        $start = $index * $pageSize;
        $end = $start + $pageSize - 1;
        $leaderboard = MaintainLeaderboardRecordModel::getInstance()->getStarLeaderboard(MaintainUserStarExtraDTO::RANK_TYPE_IDOL_POPULARITY, $start, $end);
        $result->setSuccessWithResult($leaderboard);
        return $result;
    }


    /**
     * 获取明星贡献榜
     * @param int $index
     * @param int $pageSize
     * @return Result
     */
    public function getStarContributionLeaderboard(int $starId, int $index, int $pageSize): Result
    {
        $result = new Result();
        $start = $index * $pageSize;
        $end = $start + $pageSize - 1;
        $leaderboard = MaintainLeaderboardRecordModel::getInstance()->getStarContributionUserLeaderboard($starId, $start, $end);
        $result->setSuccessWithResult($leaderboard);
        return $result;
    }


    public function updateStarContribution(UpdateStarContributionParams $updateStarContributionParams): Result
    {

        $result = new Result();
        $userId = $updateStarContributionParams->user_id;
        $starId = $updateStarContributionParams->star_id;
        $value = $updateStarContributionParams->value;

        $maintainLeaderboardRecordModel = MaintainLeaderboardRecordModel::getInstance();
        //更新明星的贡献值总数
        $maintainLeaderboardRecordModel->updateStarContribution($starId, $value);

        //更新明星粉丝的贡献值排行榜
        $score = $maintainLeaderboardRecordModel->getStarContributionUserScore($starId, $userId);
        $score = intval($score) + $value;
        MaintainLeaderboardRecordModel::getInstance()->updateStarContributionUserLeaderboard($starId, $userId, $score);

        $result->setSuccessWithResult(true);
        return $result;
    }

    public function getStarContributionTotalValue(int $starId): Result
    {
        $result = new Result();
        $maintainLeaderboardRecordModel = MaintainLeaderboardRecordModel::getInstance();
        $score = $maintainLeaderboardRecordModel->getStarContributionScore($starId);
        $result->setSuccessWithResult(intval($score));
        return $result;
    }

    public function getStarContributionUserScore(int $starId, int $userId): Result
    {
        $result = new Result();
        $maintainLeaderboardRecordModel = MaintainLeaderboardRecordModel::getInstance();
        $score = $maintainLeaderboardRecordModel->getStarContributionUserScore($starId, $userId);
        $result->setSuccessWithResult(intval($score));
        return $result;
    }

    public function getStarContributionUserRank(int $starId, int $userId): Result
    {
        $result = new Result();
        $maintainLeaderboardRecordModel = MaintainLeaderboardRecordModel::getInstance();
        $rank = $maintainLeaderboardRecordModel->getStarContributionUserRank($starId, $userId);
        $result->setSuccessWithResult($rank);
        return $result;
    }
}