<?php

namespace service\maintain\impl;


use facade\request\maintain\GetMaintainTaskRankParams;
use facade\service\maintain\MaintainUserStarExtraService;
use facade\response\Result;
use mode\dto\MaintainStarTaskDTO;
use model\maintain\MaintainUserStarExtraModel;

class MaintainUserStarExtraServiceImpl implements MaintainUserStarExtraService
{


    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    /**
     * @param int $userId
     * @param int $starId
     * @return Result|null
     * 查询统计记录
     */
    public function queryRecord(int $userId, int $starId): ?Result
    {
        $result = new Result();
        $feedInformationDTO = MaintainUserStarExtraModel::getInstance()->queryUserRecord($userId, $starId);
        $result->setSuccessWithResult($feedInformationDTO);
        return $result;
    }


}