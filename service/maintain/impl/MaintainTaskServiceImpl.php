<?php

namespace service\maintain\impl;


use facade\request\admin\GetStarListParams;
use facade\request\maintain\CreateMaintainStarTaskParams;
use facade\request\maintain\CreateMaintainUserTaskRecordParams;
use facade\request\maintain\UpdateStarContributionParams;
use facade\service\maintain\MaintainTaskService;
use facade\response\Result;
use model\dto\MaintainTaskCreateDTO;
use model\dto\MaintainTaskDTO;
use model\maintain\MaintainTaskModel;
use facade\request\maintain\CreateMaintainTaskParams;
use mode\dto\MaintainStarTaskDTO;
use model\maintain\MaintainStarTaskModel;
use model\dto\MaintainTaskRecordDTO;
use model\maintain\MaintainTaskRecordModel;
use model\dto\MaintainUserStarExtraDTO;
use model\maintain\MaintainUserStarExtraModel;
use facade\request\maintain\GetMaintainTaskRankParams;
use facade\request\maintain\GetUserTaskHistoryForStarParams;
use facade\request\maintain\GetStarMaintainTaskListParams;

class MaintainTaskServiceImpl implements MaintainTaskService
{


    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    /**
     * @param CreateMaintainTaskParams $createMaintainTaskParams
     * @return Result|null
     * 创建任务
     */
    public function insertTask(CreateMaintainTaskParams $createMaintainTaskParams): ?Result
    {
        $result = new Result();
        $maintainTaskDTO = new MaintainTaskDTO();
        \FUR_Core::copyProperties($createMaintainTaskParams, $maintainTaskDTO);
        $maintainTaskDTO->created_timestamp = time();
        $maintainTaskDTO->status = 1;
        $insertId = MaintainTaskModel::getInstance()->insertRecord($maintainTaskDTO);
        $result->setSuccessWithResult($insertId);
        return $result;
    }


    /**
     * @param CreateMaintainStarTaskParams $createMaintainStarTaskParams
     * @return Result|null
     * 创建明星关联任务
     */
    public function insertStarTask(CreateMaintainStarTaskParams $createMaintainStarTaskParams): ?Result
    {
        $result = new Result();
        $maintainStarTaskDTO = new \model\dto\MaintainStarTaskDTO();
        $MaintainTaskDTO = MaintainTaskModel::getInstance()->queryRecord($createMaintainStarTaskParams->task_id);
        \FUR_Core::copyProperties($createMaintainStarTaskParams, $maintainStarTaskDTO);

        $maintainStarTaskDTO->created_timestamp = time();
        $maintainStarTaskDTO->task_status = $MaintainTaskDTO->status;

        $MaintainStarTaskModel = MaintainStarTaskModel::getInstance();
        $countRet = $MaintainStarTaskModel->queryStarExistTask($createMaintainStarTaskParams->star_id, $createMaintainStarTaskParams->task_id);
        if ($countRet != null) {
            return $result;
        }
        $insertId = $MaintainStarTaskModel->insertRecord($maintainStarTaskDTO);
        $result->setSuccessWithResult($insertId);
        return $result;
    }


    /**
     * @param CreateMaintainUserTaskRecordParams $createUserTaskRecordParams
     * @return Result|null
     * 插入打卡记录
     */
    public function insertUserTaskRecord(CreateMaintainUserTaskRecordParams $createUserTaskRecordParams): ?Result
    {
        $result = new Result();
        /**
         * 查询用户打卡记录表
         */
        $starId = $createUserTaskRecordParams->star_id;
        $userId = $createUserTaskRecordParams->user_id;
        $queryRet = $this->queryUserTaskRecord($userId, $starId, $createUserTaskRecordParams->task_id);


        /**
         * 已经有符合条件的打卡记录
         */
        if (Result::isSuccess($queryRet) && $queryRet->data == true) {

            $result->setSuccessWithResult(false);
        } else {


            $maintainTaskRecordDTO = new MaintainTaskRecordDTO();
            \FUR_Core::copyProperties($createUserTaskRecordParams, $maintainTaskRecordDTO);
            $maintainTaskRecordDTO->created_timestamp = time();
            $insertId = MaintainTaskRecordModel::getInstance()->insertRecord($maintainTaskRecordDTO);

            /**
             * 插入用户与ip的互动信息统计表，先检查是否有记录
             */

            $UserStarExtraServiceImpl = MaintainUserStarExtraServiceImpl::getInstance();
            $queryRet = $UserStarExtraServiceImpl::queryRecord($userId, $starId);
            if (!Result::isSuccess($queryRet)) {
                /**
                 * 插入统计记录
                 */
                $MaintainUserStarExtraDTO = new MaintainUserStarExtraDTO();
                \FUR_Core::copyProperties($createUserTaskRecordParams, $MaintainUserStarExtraDTO);
                $MaintainUserStarExtraDTO->task_total_value += $createUserTaskRecordParams->value;
                $MaintainUserStarExtraDTO->created_timestamp = time();
                MaintainUserStarExtraModel::getInstance()->insertRecord($MaintainUserStarExtraDTO);


            } else {
                /**
                 * 更新统计记录
                 */
                $MaintainUserStarExtraDTO = new MaintainUserStarExtraDTO();
                \FUR_Core::copyProperties($createUserTaskRecordParams, $MaintainUserStarExtraDTO);
                $MaintainUserStarExtraDTO->task_total_value = $queryRet->data->task_total_value + $createUserTaskRecordParams->value;
                $MaintainUserStarExtraDTO->updated_timestamp = time();
                unset($MaintainUserStarExtraDTO->leaderboard_total_value);
                MaintainUserStarExtraModel::getInstance()->updateRecord($MaintainUserStarExtraDTO);


            }

            /** 更改星能量榜单排名 */
            $maintainUserStarExtraModel = MaintainTaskRecordModel::getInstance();
            $targetType = GetMaintainTaskRankParams::RANK_TYPE_TASK_TOTAL;
            $maintainUserStarExtraModel->updateTaskRankScore($starId, $targetType, $createUserTaskRecordParams->value);

            /**
             * 更新打卡记录缓存
             */
            $MaintainTaskCreateDTO = MaintainTaskCreateDTO::buildDTO($userId, $insertId, time());
            MaintainTaskRecordModel::getInstance()->updateUserCreateTaskRecord($MaintainTaskCreateDTO);


            /**更新贡献值排行*/
            $updateStarContributionParams = new UpdateStarContributionParams();
            $updateStarContributionParams->user_id = $createUserTaskRecordParams->user_id;
            $updateStarContributionParams->star_id = $createUserTaskRecordParams->star_id;
            $updateStarContributionParams->value = $createUserTaskRecordParams->value;
            $maintainLeaderboardServiceImpl = MaintainLeaderboardServiceImpl::getInstance();
            $maintainLeaderboardServiceImpl->updateStarContribution($updateStarContributionParams);

            $result->setSuccessWithResult($insertId);
        }

        return $result;
    }


    /**
     * @return Result|null
     * 查询用户打卡记录
     */
    public function queryUserTaskRecord(int $userId, int $starId, int $taskId): ?Result
    {

        /**
         *查询打卡记录
         */
        $result = new Result();
        $maintainTaskDTO = MaintainTaskRecordModel::getInstance()->queryRecord($userId, $starId, $taskId);

        if ($maintainTaskDTO != null && $maintainTaskDTO->created_timestamp) {

            $hasRecord = date('Y-m-d', $maintainTaskDTO->created_timestamp) == date("Y-m-d");
            if ($hasRecord) {
                $result->setSuccessWithResult(true);
            } else {
                $result->setSuccessWithResult(false);
            }
        }

        return $result;
    }


    /**
     * @return Result|null
     * 根据starid查询相关任务列表
     */
    public function queryTaskListByStarId(int $starId): ?Result
    {
        $result = new Result();
        $starRecordList = MaintainStarTaskModel::getInstance()->queryTaskListByStarId($starId);
        $result->setSuccessWithResult($starRecordList);
        return $result;
    }


    /**
     * @param int $taskId
     * @return Result|null
     * 查询任务信息
     * //todo:可以做个缓存
     */
    public function queryTaskInfo(int $taskId): ?Result
    {
        $result = new Result();
        $maintainTaskDTO = MaintainTaskModel::getInstance()->queryRecord($taskId);
        $result->setSuccessWithResult($maintainTaskDTO);
        return $result;
    }


    /**
     * @param int $taskId
     * @param int $starId
     * @return Result|null
     * 查询明星任务下的星能量总值
     */

    public function queryStarTotalValueByTaskId(int $taskId, int $starId): ?Result
    {
        $result = new Result();
        $maintainTaskDTO = MaintainTaskRecordModel::getInstance()->queryStarValueByTaskId($taskId, $starId);
        $result->setSuccessWithResult($maintainTaskDTO);
        return $result;

    }

    /**
     * @param int $taskId
     * @param int $starId
     * @return Result|null
     * 查询明星任务下的用户贡献的星能量
     */

    public function queryUserValueByTaskId(int $taskId, int $starId, ?int $userId): ?Result
    {
        $result = new Result();
        $maintainTaskDTO = MaintainTaskRecordModel::getInstance()->queryUserValueByTaskId($taskId, $starId, $userId);
        $result->setSuccessWithResult($maintainTaskDTO);
        return $result;

    }


    /**
     * @param int $taskId
     * @param int $starId
     * @param int|null $userId
     * @return Result|null
     * 查询明星任务下的用户打卡人数
     */

    public function queryUserListByStarId(int $starId): ?Result
    {
        $result = new Result();
        $userArray = MaintainTaskRecordModel::getInstance()->queryUserListByStarId($starId);
        $result->setSuccessWithResult($userArray);
        return $result;

    }


    /**
     * @param GetMaintainTaskRankParams $getMaintainRankParams
     * @return Result|null
     * 获取公益榜单排名
     */
    public function getMaintainTaskRankList(GetMaintainTaskRankParams $getMaintainRankParams): ?Result
    {
        $result = new Result();
        $index = $getMaintainRankParams->index;
        $pageSize = $getMaintainRankParams->page_size;
        $start = $index * $pageSize;
        $end = $start + $pageSize - 1;


        $rankType = $getMaintainRankParams->rank_type;
        $maintainUserStarExtraModel = MaintainTaskRecordModel::getInstance();
        $maintainRank = $maintainUserStarExtraModel->getMaintainTaskRankList($rankType, $start, $end);
        $result->setSuccessWithResult($maintainRank);
        return $result;

    }


    /**
     * @param int $starId
     * @return Result
     * 获取星能量值排名
     */
    public function getMaintainTaskRank(int $starId, string $rankType): Result
    {
        $result = new Result();
        $rank = MaintainTaskRecordModel::getInstance()->getMaintainTaskRank($starId, $rankType);
        $result->setSuccessWithResult($rank);
        return $result;

    }


    /**
     * @param int $starId
     * @param string $rankType
     * @return Result
     * 获取上榜明星数
     */
    public function getMaintainTaskRankCount(string $rankType): Result
    {
        $result = new Result();
        $count = MaintainTaskRecordModel::getInstance()->getMaintainTaskRankCount($rankType);
        $result->setSuccessWithResult($count);
        return $result;
    }

    /**
     * @param int $starId
     * @return Result
     * 获取星能量排行榜的值
     */
    public function getMaintainTaskValue(int $starId, string $rankType): Result
    {
        $result = new Result();
        $rank = MaintainTaskRecordModel::getInstance()->getMaintainTaskValue($starId, $rankType);
        $result->setSuccessWithResult(intval($rank));
        return $result;

    }


    /**
     * @param int $userId
     * @return Result|null
     * 用户星能量总值
     */
    public function getUserTaskTotalValue(int $userId): ?Result
    {
        $result = new Result();
        $maintainTaskRecordModel = MaintainTaskRecordModel::getInstance();
        $count = intval($maintainTaskRecordModel->getUserTaskTotalValue($userId));
        $result->setSuccessWithResult($count);
        return $result;
    }


    /**
     * @param GetUserTaskHistoryForStarParams $getUserTaskHistoryForStarParams
     * @return Result|null
     * 查询用户在明星下的打卡信息
     */
    public function getUserTaskHistoryForStar(GetUserTaskHistoryForStarParams $getUserTaskHistoryForStarParams): ?Result
    {
        $result = new Result();
        $maintainTaskRecordModel = MaintainTaskRecordModel::getInstance();
        $array = $maintainTaskRecordModel->queryUserTaskHistoryForStar($getUserTaskHistoryForStarParams->user_id, $getUserTaskHistoryForStarParams->star_id);
        $result->setSuccessWithResult($array);
        return $result;
    }


    /**
     * @param GetUserTaskHistoryForStarParams $getUserTaskHistoryForStarParams
     * @return Result|null
     * 查询贡献总值
     */
    public function queryTotalValueForStar(GetUserTaskHistoryForStarParams $getUserTaskHistoryForStarParams): ?Result
    {
        $result = new Result();
        $maintainTaskRecordModel = MaintainTaskRecordModel::getInstance();
        $countRet = $maintainTaskRecordModel->queryTotalValueForStar($getUserTaskHistoryForStarParams->user_id, $getUserTaskHistoryForStarParams->star_id);
        $result->setSuccessWithResult($countRet);
        return $result;
    }


    /**
     * @param GetUserTaskHistoryForStarParams $getUserTaskHistoryForStarParams
     * @return Result|null
     * 查询任务完成结果
     */
    public function queryTaskFinishStatus(int $starId, int $userId, int $taskId): ?Result
    {
        $result = new Result();
        $maintainTaskRecordModel = MaintainTaskRecordModel::getInstance();
        $finishRet = $maintainTaskRecordModel->queryStarTaskFinishStatus($starId, $userId, $taskId);
        $result->setSuccessWithResult($finishRet);
        return $result;
    }


    /**
     * @param int $index
     * @param int $pageSize
     * @return Result|null
     * 获取所有任务列表
     */
    public function queryAllTaskList(int $index, int $pageSize): ?Result
    {
        $result = new Result();
        $maintainTaskModel = MaintainTaskModel::getInstance();
        $offset = $index * $pageSize;
        $finishRet = $maintainTaskModel->queryAllTaskList($offset, $pageSize);
        $result->setSuccessWithResult($finishRet);
        return $result;
    }


    /**
     * @return Result|null
     * 获取有任务的明星id
     */
    public function queryDistinctStarId(GetStarMaintainTaskListParams $GetStarMaintainTaskListParams): ?Result
    {
        $result = new Result();
        $maintainStarTaskModel = MaintainStarTaskModel::getInstance();

        $offset = $GetStarMaintainTaskListParams->index * $GetStarMaintainTaskListParams->page_size;
        $pageSize = $GetStarMaintainTaskListParams->page_size;
        $starIdArray = $maintainStarTaskModel->queryDistinctStarId($offset, $pageSize);
        $result->setSuccessWithResult($starIdArray);
        return $result;

    }

}