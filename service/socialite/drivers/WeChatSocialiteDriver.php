<?php


namespace service\socialite\drivers;

use facade\response\Result;
use FUR_Curl;
use FUR_Log;
use Zhiyi\Plus\Http\Controllers\APIs\V2\Controller;

class WeChatSocialiteDriver extends SocialiteDriverAbstract
{

    const GET_ACCESS_TOKEN_URL = 'https://api.weixin.qq.com/sns/oauth2/access_token';

    protected function getBaseURI(): string
    {
        return 'https://api.weixin.qq.com';
    }


    public function getOpenId(string $accessToken): ?Result
    {

        $result = new Result();
        $oauthResult = json_decode(
            $this->getHttpClient()->get('/sns/userinfo', [
                'headers' => ['Accept' => 'application/json'],
                'query' => [
                    'access_token' => $accessToken,
                    'openid' => md5($accessToken),
                ],
            ])
                ->getBody()
                ->getContents(), true
        );
        if (isset($oauthResult['errcode'])) {
            $result->setError([$oauthResult['errcode'], $oauthResult['errmsg']]);
            return $result;

        }
        $result->setSuccessWithResult($oauthResult['openid']);
        return $result;
    }


    public function getOpenIdByCode(string $code, ?string $platform = null): ?Result
    {

        $result = new Result();
        if ($platform == 'wxMin') {
            $params['appid'] = \FUR_Config::get_socialite_config('wechat_mini')['appid'];
            $params['secret'] = \FUR_Config::get_socialite_config('wechat_mini')['appkey'];
        } elseif ($platform == 'h5') {
            $params['appid'] = \FUR_Config::get_socialite_config('wechat_h5')['appid'];
            $params['secret'] = \FUR_Config::get_socialite_config('wechat_h5')['appkey'];
        } else {
            $params['appid'] = \FUR_Config::get_socialite_config('wechat')['appid'];
            $params['secret'] = \FUR_Config::get_socialite_config('wechat')['appkey'];
        }
        $params['code'] = $code;
        $params['grant_type'] = 'authorization_code';
        $urlParams = http_build_query($params);

        $url = self::GET_ACCESS_TOKEN_URL . "?" . $urlParams;

        $data = FUR_Curl::get($url);
        FUR_Log::info('getOpenIdByCode.data:', $data);
        $dataJsonObj = json_decode($data);
        if (isset($dataJsonObj->openid) == false) {
            $result->setError(\Config_Error::ERR_SOCIALITE_GET_WECHAT_TOKEN_FAIL);
        }
        $ret = [];
        $ret['open_id'] = $dataJsonObj->openid;
        $ret['access_token'] = $dataJsonObj->access_token;
        $result->setSuccessWithResult($ret);
        return $result;
    }
}
