<?php


namespace service\socialite\drivers;

use facade\response\Result;
use GuzzleHttp\Client as GuzzleHttpClient;

abstract class SocialiteDriverAbstract
{

    protected $http;

    protected function getHttpClient(bool $httpErrors = false): GuzzleHttpClient
    {
        if ($this->http instanceof GuzzleHttpClient) {
            return $this->http;
        }

        $baseURI = '';
        if (method_exists($this, 'getBaseURI')) {
            $baseURI = $this->getBaseURI();
        }

        return $this->http = new GuzzleHttpClient([
            'base_uri' => $baseURI,
            'http_errors' => $httpErrors,
        ]);
    }

    abstract public function getOpenId(string $accessToken): ?Result;
}
