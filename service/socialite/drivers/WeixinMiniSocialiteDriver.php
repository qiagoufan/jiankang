<?php


namespace service\socialite\drivers;

use facade\response\Result;
use FUR_Curl;
use Zhiyi\Plus\Http\Controllers\APIs\V2\Controller;

class WeixinMiniSocialiteDriver
{
    const JSCODE2SESSION_URL = 'https://api.weixin.qq.com/sns/jscode2session';

    public static function code2Session(string $code): ?Result
    {

        $result = new Result();
        $urlObj["appid"] = \FUR_Config::get_wx_mini_config('mini_app_id');
        $urlObj["secret"] = \FUR_Config::get_wx_mini_config('mini_app_secret');
        $urlObj["js_code"] = $code;
        $urlObj["grant_type"] = "authorization_code";
        $bizString = http_build_query($urlObj);
        $url = self::JSCODE2SESSION_URL . "?" . $bizString;
        $data = FUR_Curl::get($url);
        \FUR_Log::debug('weixinMini:data', json_encode($data));
        if ($data == null) {
            $result->setError(\Config_Error::ERR_USER_WXMINI_API_ERROR);
            return $result;
        }

        $jsonDecodeData = json_decode($data);
        if (isset($jsonDecodeData->errcode) && $jsonDecodeData->errcode != 0) {
            $result->setError([$jsonDecodeData->errcode, $jsonDecodeData->errmsg]);
            return $result;
        }
        $result->setSuccessWithResult($jsonDecodeData);
        return $result;
    }


}
