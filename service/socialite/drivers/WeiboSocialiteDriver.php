<?php


namespace service\socialite\drivers;

use facade\response\Result;
use Zhiyi\Plus\Http\Controllers\APIs\V2\Controller;

class WeiboSocialiteDriver extends SocialiteDriverAbstract
{

    protected function getBaseURI(): string
    {
        return 'https://api.weibo.com/';
    }


    public function getOpenId(string $accessToken): ?Result
    {
        $result = new Result();
        $oauthResult = json_decode(
            $this->getHttpClient()->post('/oauth2/get_token_info', [
                'query' => ['access_token' => $accessToken],
                'headers' => ['Accept' => 'application/json'],
            ])
                ->getBody()
                ->getContents(), true
        );
        if (isset($oauthResult['error_code'])) {
            $result->setError([$oauthResult['error_code'], $oauthResult['error']]);
            return $result;

        }
        $result->setSuccessWithResult($oauthResult['uid']);
        return $result;
    }
}
