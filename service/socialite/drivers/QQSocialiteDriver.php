<?php


namespace service\socialite\drivers;


use facade\response\Result;

class QQSocialiteDriver extends SocialiteDriverAbstract
{

    protected function getBaseURI(): string
    {
        return 'https://graph.qq.com';
    }

    public function getOpenId(string $accessToken): ?Result
    {

        $result = new Result();

        $oauthResultStr = $this->removeCallback(
            $this->getHttpClient()->get('/oauth2.0/me', [
                'query' => [
                    'access_token' => $accessToken
                ],
            ])
                ->getBody()
                ->getContents()
        );
        $oauthResult = json_decode($oauthResultStr, true);
        if (isset($oauthResult['error'])) {
            $result->setError([$oauthResult['error'], $oauthResult['error_description']]);
            return $result;

        }
        $result->setSuccessWithResult($oauthResult['openid']);

        return $result;
    }


    protected function removeCallback(string $response): string
    {
        if (strpos($response, 'callback') !== false) {
            $lpos = strpos($response, '(');
            $rpos = strrpos($response, ')');
            $response = substr($response, $lpos + 1, $rpos - $lpos - 1);
        }

        return $response;
    }
}
