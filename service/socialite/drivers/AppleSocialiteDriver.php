<?php


namespace service\socialite\drivers;


use facade\response\Result;
use Firebase\JWT\JWT;

class AppleSocialiteDriver extends SocialiteDriverAbstract
{

    protected function getBaseURI(): string
    {
        return 'https://appleid.apple.com';
    }

    public function getOpenId(string $accessToken): ?Result
    {
        $result = new Result();

        $openId = null;
        try {
            $data = explode('.', $accessToken);
            $payload = json_decode(base64_decode($data[1]));
            $openId = $payload->sub;
        } catch (\Exception $e) {
            $result->setError(\Config_Error::ERR_USER_APPLE_SOCIALITE_LOGIN_FAIL);
            return $result;
        }
//        try {
//        $publicKey = $this->getPublicKey();
//            $decoded = JWT::decode($accessToken, $publicKey, array('RS256'));
//            $openId = $decoded->sub;
//        } catch (\Exception $e) {
//            $result->setError(\Config_Error::ERR_USER_APPLE_SOCIALITE_LOGIN_FAIL);
//            return $result;
//        }
        if ($openId == null) {
            return $result;
        }
        $result->setSuccessWithResult($openId);
        return $result;
    }

    private function getPublicKey()
    {

        $keyFile = 'file://' . \FUR_Config::get_apple_config('apple_public_key_path');
        return openssl_pkey_get_public($keyFile);

    }


}
