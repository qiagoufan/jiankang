<?php


namespace service\socialite\impl;


use facade\request\socialite\SocialiteParams;
use facade\response\Result;
use facade\service\socialite\SocialiteService;
use model\UserSocialiteModel;
use service\socialite\drivers\AppleSocialiteDriver;
use service\socialite\drivers\QQSocialiteDriver;
use service\socialite\drivers\SocialiteDriverAbstract;
use service\socialite\drivers\WeChatSocialiteDriver;
use service\socialite\drivers\WeiboSocialiteDriver;
use service\socialite\drivers\WeixinMiniSocialiteDriver;

class SocialiteServiceImpl implements SocialiteService
{

    const PROVIDER_QQ = 'qq';
    const PROVIDER_WEIBO = 'weibo';
    const PROVIDER_WECHAT = 'wechat';
    const PROVIDER_APPLE = 'apple';
    const PROVIDER_WEIXIN_MINI = 'wxMini';
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function getOpenId(SocialiteParams $socialiteParams): ?Result
    {
        $result = new Result();
        if ($socialiteParams->access_token == null && $socialiteParams->code == null) {
            $result->setError(\Config_Error::ERR_SOCIALITE_TOKEN_FAIL);
            return $result;
        }

        $openid = "";
        if ($socialiteParams->provider == self::PROVIDER_WEIXIN_MINI) {
            $openid = $this->getWeixinMiniOpenId($socialiteParams->code);
            if ($openid == null) {
                $result->setError(\Config_Error::ERR_SOCIALITE_GET_WECHAT_MINI_OPENID_FAIL);
                return $result;
            }
        } else {
            $providerDriver = $this->getDriver($socialiteParams->provider);
            if ($providerDriver == null) {
                $result->setError(\Config_Error::ERR_SOCIALITE_PROVIDER_NOT_EXIST);
                return $result;
            }
            $getOpenIdRet = $providerDriver->getOpenId($socialiteParams->access_token);
            if (!Result::isSuccess($getOpenIdRet)) {
                $result->setError(\Config_Error::ERR_SOCIALITE_TOKEN_CHECK_FAIL);
                return $result;
            }
            $openid = $getOpenIdRet->data;
        }


        $result->setSuccessWithResult($openid);
        return $result;
    }


    private function getDriver(?string $provider): ?SocialiteDriverAbstract
    {
        $provider = strtolower($provider);
        $providerDriver = null;
        if ($provider == self::PROVIDER_QQ) {
            $providerDriver = new QQSocialiteDriver();
        } else if ($provider == self::PROVIDER_WEIBO) {
            $providerDriver = new WeiboSocialiteDriver();
        } else if ($provider == self::PROVIDER_WECHAT) {
            $providerDriver = new WeChatSocialiteDriver();
        } else if ($provider == self::PROVIDER_APPLE) {
            $providerDriver = new AppleSocialiteDriver();
        }
        return $providerDriver;
    }

    private function getWeixinMiniOpenId(?string $code): ?string
    {
        $weixinMiniSessionInfoRet = WeixinMiniSocialiteDriver::code2Session($code);

        \FUR_Log::debug('getWeixinMiniOpenId:openId :', json_encode($weixinMiniSessionInfoRet));
        if (!Result::isSuccess($weixinMiniSessionInfoRet)) {
            return null;
        }
        $sessionData = $weixinMiniSessionInfoRet->data;
        return $sessionData->openid;
    }

    public function getWxMiniSessionKey(string $openid): ?Result
    {
        $result = new Result();
        $sessionKey = UserSocialiteModel::getInstance()->getWeixinMiniSessionKeyCache($openid);
        $result->setSuccessWithResult($sessionKey);
        return $result;
    }


    public function queryUserOpenid(string $provider, int $userId): ?Result
    {
        $result = new Result();
        $openid = null;
        $userSocialiteDTO = UserSocialiteModel::getInstance()->queryUserSocialiteInfo($provider, $userId);
        if ($userSocialiteDTO != null) {
            $openid = $userSocialiteDTO->open_id;
        }
        $result->setSuccessWithResult($openid);
        return $result;
    }

    public function getWxMiniSessionData(string $code): ?Result
    {
        $result = new Result();
        $weixinMiniSessionInfoRet = WeixinMiniSocialiteDriver::code2Session($code);

        \FUR_Log::debug('getWeixinMiniOpenId:openId :', json_encode($weixinMiniSessionInfoRet));
        if (!Result::isSuccess($weixinMiniSessionInfoRet)) {
            return $weixinMiniSessionInfoRet;
        }
        $sessionData = json_encode($weixinMiniSessionInfoRet->data);
        $result->setSuccessWithResult($sessionData);
        return $result;
    }
}