<?php

namespace service\activity\impl;

use facade\request\activity\hbxj\FillDeliveryAddressParams;
use facade\response\activity\hbxj\HbxjAwardResp;
use facade\response\activity\hbxj\HbxjReceiveFragmentResp;
use facade\response\base\PageInfoResp;
use facade\service\activity\HbxjService;
use facade\response\Result;
use model\activity\dto\XhActivityHuobaoxiaojiuCodeDTO;
use model\activity\dto\XhActivityHuobaoxiaojiuFragmentDTO;
use model\activity\dto\XhActivityHuobaoxiaojiuUserAwardDTO;
use model\activity\XhActivityHuobaoxiaojiuModel;
use service\logistics\impl\LogisticsServiceImpl;

class HbxjServiceImpl implements HbxjService
{
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    /**
     * 领取碎片
     * @param string $code
     * @param int $userId
     * @return Result|null
     */
    public function receiveFragment(string $code, int $userId): ?Result
    {
        $result = new Result();

        $receiveFragmentResp = new  HbxjReceiveFragmentResp();
        $hbxjActivityModel = XhActivityHuobaoxiaojiuModel::getInstance();
        $codeDetail = $hbxjActivityModel->getCodeDetail($code);


        //码是否存在
        $codeType = 0;
        if ($codeDetail == null) {
            $result->setError(\Config_Error::ERR_HBXJ_CODE_IS_NOT_EXIST);
            return $result;
        }

        //如果是中奖码,并且没使用过的话,就变更类型
        if ($codeDetail->award_id != XhActivityHuobaoxiaojiuCodeDTO::CODE_TYPE_GENERAL && $codeDetail->code_status != XhActivityHuobaoxiaojiuCodeDTO::CODE_STATUS_USED) {
            $codeType = $codeDetail->award_id;
        }


        //普通碎片
        if ($codeType == 0) {
            $fragmentCountList = $hbxjActivityModel->getUserFragmentCountList($userId);
            //计算中奖码,给用户发一个普通碎片
            $fragmentCountMap = $this->conversionFragmentCountMap($fragmentCountList);
            $fragmentNum = $this->calculateGeneralFragmentNum($fragmentCountMap);
            $receiveFragmentResp->fragment_num = $fragmentNum;

            //给用户发一个中奖碎片
            $this->insertUserFragment($userId, $fragmentNum, XhActivityHuobaoxiaojiuCodeDTO::CODE_TYPE_GENERAL, null);
        } else {
            //更新中奖码状态
            $codeDetail->code_status = XhActivityHuobaoxiaojiuCodeDTO::CODE_STATUS_USED;
            $codeDetail->user_id = $userId;
            $codeDetail->use_timestamp = time();
            $hbxjActivityModel->updateCodeToBeUsed($codeDetail);
            $fragmentNum = XhActivityHuobaoxiaojiuFragmentDTO::FRAGMENT_NUM_AWARD;
            $awardType = $codeDetail->award_id;

            //一个用户只能中一次一等奖
            if ($awardType == 1) {
                //查询用户中间碎片数
                $topAwardCount = $hbxjActivityModel->queryUserAwardFragmentCount($userId, $awardType);
                if ($topAwardCount > 0) {
                    $fragmentNum = rand(1, 5);
                    $awardType = XhActivityHuobaoxiaojiuCodeDTO::CODE_TYPE_GENERAL;
                }
            }

            //如果中奖码过期了,给一个随机码
            if ($codeDetail->code_status == XhActivityHuobaoxiaojiuCodeDTO::CODE_STATUS_EXPIRE) {
                $fragmentNum = rand(1, 5);
                $awardType = XhActivityHuobaoxiaojiuCodeDTO::CODE_TYPE_GENERAL;
            }
            $receiveFragmentResp->fragment_num = $fragmentNum;
            //给用户发一个中奖碎片
            $this->insertUserFragment($userId, $fragmentNum, $awardType, $code);
        }
        //查询数据,判断用户领取完成以后是否中奖,如果没有中间直接返回
        $fragmentCountList = $hbxjActivityModel->getUserFragmentCountList($userId);


        $fragmentCountMap = $this->conversionFragmentCountMap($fragmentCountList);

        $checkAwardRet = $this->checkAward($fragmentCountMap);
        if ($checkAwardRet == false) {
            $result->setSuccessWithResult($receiveFragmentResp);
            return $result;
        }
        //生成奖励
        $userAwardId = $this->generateAward($userId);
        if ($userAwardId == 0) {
            $result->setSuccessWithResult($receiveFragmentResp);
            return $result;
        }

        $userAwardDetail = $hbxjActivityModel->getUserAwardDetail($userAwardId);

        $hbxjAwardResp = $this->conversionAwardResp($userAwardDetail);
        $receiveFragmentResp->award_info = $hbxjAwardResp;
        $result->setSuccessWithResult($receiveFragmentResp);
        return $result;

    }


    private function conversionFragmentCountMap($fragmentCountList)
    {
        if ($fragmentCountList == null) {
            $fragmentCountList = [];
        }

        $fragmentCountMap = [];
        //转换一下,看一下各种类型的碎片总数有多少
        foreach ($fragmentCountList as $fragmentCount) {
            $fragmentCountMap[$fragmentCount->fragment_num] = $fragmentCount->fragment_count;
        }
        for ($i = 1; $i < 7; $i++) {
            if (isset($fragmentCountMap[$i]) == false) {
                $fragmentCountMap[$i] = 0;
            }
        }
        return $fragmentCountMap;
    }

    /**
     * 计算应得的碎片号
     * @param array $fragmentCountMap
     * @return array
     */
    private function calculateGeneralFragmentNum(array $fragmentCountMap): int
    {
        $notReceiveFragmentNum = 0;

        //如果用户有从未获取过的碎片,就优先获得未获取的,否则随机取
        for ($i = 1; $i < 6; $i++) {
            if (isset($fragmentCountMap[$i]) == false || $fragmentCountMap[$i] == 0) {
                $notReceiveFragmentNum = $i;
                break;
            }
        }

        $fragmentNum = 1;
        if ($notReceiveFragmentNum == 0) {
            $fragmentNum = rand(1, 5);
        } else {
            $fragmentNum = $notReceiveFragmentNum;
        }

        return $fragmentNum;
    }

    private function insertUserFragment(int $userId, int $fragmentNum, int $awardType, ?string $code = null)
    {
        $fragmentDTO = new XhActivityHuobaoxiaojiuFragmentDTO();
        $fragmentDTO->user_id = $userId;
        $fragmentDTO->fragment_code = $code;
        $fragmentDTO->award_id = $awardType;
        $fragmentDTO->fragment_status = 0;
        $fragmentDTO->created_timestamp = time();
        $fragmentDTO->fragment_num = $fragmentNum;
        //给用户发一个中奖碎片
        XhActivityHuobaoxiaojiuModel::getInstance()->insertFragment($fragmentDTO);
    }

    //检查所有碎片是不是都集齐了
    private function checkAward(array $fragmentCountMap): bool
    {
        foreach ($fragmentCountMap as $fragmentCount) {

            if ($fragmentCount == 0) {
                return false;
            }
        }

        return true;
    }

    //生成奖励
    private function generateAward(int $userId): ?int
    {
        $awardId = 0;
        $awardType = 0;
        $activityModel = XhActivityHuobaoxiaojiuModel::getInstance();

        $fragmentIdList = [];
        //遍历获取应该兑换的碎片id
        for ($i = 1; $i < 7; $i++) {
            $fragmentNum = $i;
            $fragmentInfo = $activityModel->getUserFragmentByType($userId, $fragmentNum);
            if ($fragmentInfo == null) {
                return $awardId;
            }
            if ($i == XhActivityHuobaoxiaojiuFragmentDTO::FRAGMENT_NUM_AWARD) {
                $awardType = $fragmentInfo->award_id;
            }
            array_push($fragmentIdList, $fragmentInfo->id);
        }
        $activityModel->updateFragmentToBeUsed($fragmentIdList);
        //写入奖品

        $userAwardDTO = new XhActivityHuobaoxiaojiuUserAwardDTO();
        $userAwardDTO->user_id = $userId;
        $userAwardDTO->award_id = $awardType;
        $userAwardDTO->fragment_id_list = implode(',', $fragmentIdList);
        $userAwardDTO->created_timestamp = time();
        $userAwardDTO->receive_status = 0;
        $userAwardDTO->withdraw_order_num = 'hbxj' . date('YmdHis') . rand(10000000, 100000000 - 1);
        $awardId = $activityModel->insertUserAward($userAwardDTO);
        return $awardId;
    }


    public function lockUser(int $userId): ?Result
    {
        $result = new Result();
        $lockRet = XhActivityHuobaoxiaojiuModel::getInstance()->lockUser($userId);
        $result->setSuccessWithResult($lockRet);

        return $result;
    }

    public function unlockUser(int $userId): ?Result
    {
        $result = new Result();
        $unlockRet = XhActivityHuobaoxiaojiuModel::getInstance()->unlockUser($userId);
        $result->setSuccessWithResult($unlockRet);

        return $result;
    }


    private function conversionAwardResp(?XhActivityHuobaoxiaojiuUserAwardDTO $userAwardDTO): ?HbxjAwardResp
    {
        if ($userAwardDTO == null) {
            return null;
        }

        $hbxjAwardResp = new HbxjAwardResp();

        $hbxjAwardResp->user_award_id = $userAwardDTO->id;
        $hbxjAwardResp->receive_status = $userAwardDTO->receive_status;
        $hbxjAwardResp->get_time = date('m-d H:i', $userAwardDTO->created_timestamp);
        $awardDetail = XhActivityHuobaoxiaojiuModel::getInstance()->getAwardInfo($userAwardDTO->award_id);
        $hbxjAwardResp->award_type = $awardDetail->award_type;
        $hbxjAwardResp->award_title = $awardDetail->award_title;
        $hbxjAwardResp->award_image = $awardDetail->award_img;
        if ($userAwardDTO->express_num != null) {
            $logisticsDetailRet = LogisticsServiceImpl::getInstance()->getLogisticsDetail($userAwardDTO->express_company, $userAwardDTO->express_num);
            if (Result::isSuccess($logisticsDetailRet)) {
                $hbxjAwardResp->express_info = $logisticsDetailRet->data;
            }
        }

        return $hbxjAwardResp;

    }

    public function getUserAwardList(int $userId): ?Result
    {
        $result = new Result();
        $userAwardList = XhActivityHuobaoxiaojiuModel::getInstance()->getUserAwardList($userId);
        $awardRespList = [];
        if ($userAwardList != null && !empty($userAwardList)) {
            foreach ($userAwardList as $userAwardDTO) {
                $hbxjAwardResp = $this->conversionAwardResp($userAwardDTO);
                if ($hbxjAwardResp == null) {
                    continue;
                }
                array_push($awardRespList, $hbxjAwardResp);
            }
        }
        $result->setSuccessWithResult($awardRespList);
        return $result;
    }

    public function fillDeliveryAddress(int $userId, FillDeliveryAddressParams $params): ?Result
    {
        $result = new Result();
        if ($params->address == null || $params->province == null || $params->city == null || $params->area == null) {
            $result->setError(\Config_Error::ERR_HBXJ_USER_ADDRESS_CAN_NOT_NULL);
            return $result;
        }
        if ($params->phone_num == null) {
            $result->setError(\Config_Error::ERR_HBXJ_USER_PHONE_NUM_CAN_NOT_NULL);
            return $result;
        }

        $userAwardDetail = XhActivityHuobaoxiaojiuModel::getInstance()->getUserAwardDetail($params->user_award_id);

        if ($userAwardDetail == null) {
            $result->setError(\Config_Error::ERR_HBXJ_USER_AWARD_NOT_EXIST);
            return $result;
        }
        if ($userAwardDetail->user_id != $userId) {
            $result->setError(\Config_Error::ERR_HBXJ_USER_AWARD_UNAUTHORIZED);
            return $result;
        }
        if ($userAwardDetail->receive_status != 0) {
            $result->setError(\Config_Error::ERR_HBXJ_USER_AWARD_HAS_RECEIVED);
            return $result;
        }

        //3是现金提现,懒得查了,直接写死
        if ($userAwardDetail->award_id == 3) {
            $result->setError(\Config_Error::ERR_HBXJ_ERROR_WITHDRAW_RECEIVE_TYPE_ERROR);
            return $result;
        }
        \FUR_Core::copyProperties($params, $userAwardDetail);
        $userAwardDetail->user_id = $userId;
        $userAwardDetail->receive_status = 1;
        $userAwardDetail->updated_timestamp = time();
        XhActivityHuobaoxiaojiuModel::getInstance()->updateUserAward($userAwardDetail);
        $result->setSuccessWithResult(true);
        return $result;
    }

    public function getUserFragmentCount(int $userId): ?Result
    {
        $result = new Result();
        $fragmentCountList = XhActivityHuobaoxiaojiuModel::getInstance()->getUserFragmentCountList($userId);
        //计算中奖码,给用户发一个普通碎片
        $fragmentCountMap = $this->conversionFragmentCountMap($fragmentCountList);
        $result->setSuccessWithResult($fragmentCountMap);
        return $result;
    }


    public function deliveryAward(int $userAwardId, string $expressCompany, string $expressNum): ?Result
    {
        $result = new Result();
        $hbxjModel = XhActivityHuobaoxiaojiuModel::getInstance();
        $userAwardDetail = $hbxjModel->getUserAwardDetail($userAwardId);
        if ($userAwardDetail == null) {
            $result->setError(\Config_Error::ERR_HBXJ_USER_AWARD_NOT_EXIST);
            return $result;
        }
        if ($userAwardDetail->receive_status == 0) {
            $result->setError(\Config_Error::ERR_HBXJ_NOT_RECEIVE);
            return $result;
        }
        if ($expressCompany == null || $expressNum == null) {
            $result->setError(\Config_Error::ERR_HBXJ_EXPRESS_INFO_ERROR);
            return $result;
        }
        $userAwardDetail->express_company = $expressCompany;
        $userAwardDetail->express_num = $expressNum;
        $userAwardDetail->delivery_timestamp = time();
        $hbxjModel->updateUserAward($userAwardDetail);
        $result->setSuccessWithResult(true);
        return $result;

    }

    public function getAwardList(int $index): ?Result
    {
        $result = new Result();
        $pageSize = 10;
        $start = $index * $pageSize;
        $hbxjModel = XhActivityHuobaoxiaojiuModel::getInstance();
        $data = [];
        $data['award_list'] = [];
        $award_list = $hbxjModel->getAwardList($start, $pageSize);
        foreach ($award_list as $userAward) {
            $awardInfo = $hbxjModel->getAwardInfo($userAward->award_id);
            $userAward->award_name = $awardInfo->award_title;
            array_push($data['award_list'], $userAward);
        }
        $count = $hbxjModel->getAwardCount();
        $data['page_info'] = PageInfoResp::buildPageInfoResp($index, $pageSize, count($data['award_list']), $count);
        $result->setSuccessWithResult($data);
        return $result;
    }


    public function getUserAwardDetail(int $userAwardId): ?Result
    {
        $result = new Result();
        $userAwardDetail = XhActivityHuobaoxiaojiuModel::getInstance()->getUserAwardDetail($userAwardId);
        $result->setSuccessWithResult($userAwardDetail);
        return $result;

    }


    public function updateUserWithdraw(int $userId, int $userAwardId, string $orderNum, string $paymentNum): ?Result
    {
        $result = new Result();
        $userAwardDetail = XhActivityHuobaoxiaojiuModel::getInstance()->getUserAwardDetail($userAwardId);
        $userAwardDetail->receive_status = 1;
        $userAwardDetail->withdraw_order_num = $orderNum;
        $userAwardDetail->withdraw_payment_num = $paymentNum;
        $userAwardDetail->withdraw_timestamp = time();
        XhActivityHuobaoxiaojiuModel::getInstance()->updateUserAward($userAwardDetail);
        $result->setSuccessWithResult(true);
        return $result;
    }

    public function getAwardTemplateList(): ?Result
    {
        $result = new Result();
        $awardList = XhActivityHuobaoxiaojiuModel::getInstance()->getAwardInfoList();

        $data = [];
        foreach ($awardList as $awardDetail) {
            $template = new \stdClass();
            $template->award_id = (int)$awardDetail->id;
            $template->award_type = (int)$awardDetail->award_type;
            $template->award_title = $awardDetail->award_title;
            $template->award_image = $awardDetail->award_img;
            array_push($data, $template);
        }
        $result->setSuccessWithResult($data);
        return $result;

    }
}