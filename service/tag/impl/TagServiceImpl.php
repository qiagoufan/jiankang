<?php


namespace service\tag\impl;

use facade\request\tag\CreateTagEntityParams;
use facade\request\tag\CreateTagParams;
use facade\request\tag\DeleteTagParams;
use facade\request\tag\QueryFullSiteTagEntityListParams;
use facade\request\tag\QueryTagEntityListParams;
use facade\request\tag\TagEntityParams;
use facade\request\tag\TagParams;
use facade\response\Result;
use facade\service\tag\TagService;
use model\tag\dto\TagEntityDTO;
use model\tag\dto\TagInfoDTO;
use model\tag\TagModel;

class TagServiceImpl implements TagService
{

    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function createTagIfNotExists(CreateTagParams $tagParams): Result
    {
        $result = new Result();
        if ($tagParams == null) {
            $result->setError(\Config_Error::ERR_TAG_INFO_IS_NULL);
            return $result;
        }
        if ($tagParams->tag_biz_id == null || $tagParams->tag_biz_type == null) {
            $result->setError(\Config_Error::ERR_TAG_BIZ_IS_NULL);
            return $result;
        }
        $tagModel = TagModel::getInstance();
        /**
         * 校验标签是否被使用过了
         */
        $tagRecord = $tagModel->queryTagInfoByBiz($tagParams->tag_biz_id, $tagParams->tag_biz_type);
        if ($tagRecord != null) {
            $tagModel->setTagInfoIdCache($tagRecord);
            $result->setSuccessWithResult($tagRecord->id);
            return $result;
        }

        $tagInfoDTO = new TagInfoDTO();
        \FUR_Core::copyProperties($tagParams, $tagInfoDTO);
        $tagInfoDTO->created_timestamp = time();
        $tagInfoDTO->tag_status = 1;
        $id = $tagModel->insertTagInfo($tagInfoDTO);

        $tagInfoDTO->id = $id;
        $tagModel->setTagInfoIdCache($tagInfoDTO);
        $result->setSuccessWithResult($id);
        return $result;

    }


    public function createTagEntity(CreateTagEntityParams $createTagEntityParams): Result
    {
        $result = new Result();
        $tagId = $createTagEntityParams->tag_id;
        $tagBizType = $createTagEntityParams->tag_biz_type;
        $tagBizId = $createTagEntityParams->tag_biz_id;
        $entityBizType = $createTagEntityParams->entity_biz_type;
        $entityBizId = $createTagEntityParams->entity_biz_id;

        /**
         *校验参数
         */
        if ($createTagEntityParams == null || $entityBizType == null || $entityBizId == null) {
            $result->setError(\Config_Error::ERR_CREATE_TAG_ENTITY_PARAMS_IS_NULL);
            return $result;
        }
        if ($tagId == 0 && ($tagBizType == null || $tagBizId == null)) {
            $result->setError(\Config_Error::ERR_CREATE_TAG_ENTITY_TAG_IS_NULL);
            return $result;
        }


        $tagModel = TagModel::getInstance();
        /**
         * 如果没有直接给tagId就根据信息查询tagid
         */
        if ($tagId == 0) {
            $tagInfoDTO = $tagModel->queryTagInfoByBiz($tagBizId, $tagBizType);
            if ($tagInfoDTO != null) {
                $tagId = $tagInfoDTO->id;
                $tagModel->setTagInfoIdCache($tagInfoDTO);
            }
        } else {
            $tagInfoDTO = $tagModel->queryTagInfoById($tagId);
            if ($tagInfoDTO == null) {
                $result->setError(\Config_Error::ERR_CREATE_TAG_ENTITY_TAG_IS_NOT_EXIST);
                return $result;
            }
        }
        /**
         * 如果标不存在就创建一个标
         */
        if ($tagId == 0) {
            $createTagParams = new CreateTagParams();
            $createTagParams->tag_biz_id = $tagBizId;
            $createTagParams->tag_biz_type = $tagBizType;
            $createTagRet = $this->createTagIfNotExists($createTagParams);
            if (Result::isSuccess($createTagRet)) {
                $tagId = $createTagRet->data;
            }
        }
        /**
         *如果创建失败返回错误
         */
        if ($tagId == 0) {
            $result->setError(\Config_Error::ERR_CREATE_TAG_ENTITY_TAG_ID_IS_NULL);
            return $result;
        }


        /**
         * 查看标是否已经打过了
         */
        $hasTag = $tagModel->hasTagEntity($tagId, $entityBizId, $entityBizType);
        if ($hasTag) {
            $result->setError(\Config_Error::ERR_CREATE_TAG_ENTITY_TAG_HAS_EXIST);
            return $result;
        }
        $tagEntityDTO = new TagEntityDTO();
        \FUR_Core::copyProperties($createTagEntityParams, $tagEntityDTO);
        $tagEntityDTO->tag_id = $tagId;
        $tagEntityDTO->created_timestamp = time();
        /**
         * 插入db
         */
        $id = $tagModel->insertTagEntity($tagEntityDTO);
        $tagEntityDTO->id = $id;
        /**
         * 插入缓存
         */
        $tagModel->addTagEntityCache($tagEntityDTO);
        /**
         * 全量缓存
         */
        $tagModel->addFullSiteTagEntityCache($tagEntityDTO);


        /**
         * 插入
         */
        $tagModel->updateTagEntityCount($tagId);

        $result->setSuccessWithResult($id);
        return $result;
    }

    public function deleteTag(DeleteTagParams $deleteTagParams): Result
    {
        // TODO: Implement deleteTag() method.
    }

    public function deleteTagEntity(TagEntityParams $tagEntityParams): Result
    {
        $result = new Result();
        $tagId = $tagEntityParams->tag_id;
        $entityBizId = $tagEntityParams->entity_biz_id;
        $entityBizType = $tagEntityParams->entity_biz_type;

        if ($tagEntityParams == null || $entityBizType == null || $entityBizId == null) {
            $result->setError(\Config_Error::ERR_DELETE_TAG_ENTITY_PARAMS_IS_NULL);
            return $result;
        }
        $tagModel = TagModel::getInstance();
        if ($tagId == 0) {
            $tagId = $tagModel->queryTagIdByBiz($tagEntityParams->tag_biz_id, $tagEntityParams->tag_biz_type);
        }

        $tagModel->deleteTagEntity($tagId, $entityBizId, $entityBizType);

        //删除缓存
        $tagEntityDTO = new TagEntityDTO();
        $tagEntityDTO->tag_id = $tagId;
        $tagEntityDTO->entity_biz_id = $entityBizId;
        $tagEntityDTO->entity_biz_type = $entityBizType;
        $tagModel->removeTagEntityCache($tagEntityDTO);

        /**
         * 全量缓存
         */
        $tagModel->removeFullSiteTagEntityCache($tagEntityDTO);

        //减少计数
        $tagModel->updateTagEntityCount($tagId, -1);
        $result->setSuccessWithResult(true);
        return $result;
    }

    public function queryTagId(TagParams $tagParams): Result
    {
        $result = new Result();
        if ($tagParams == null || $tagParams->tag_biz_type == null || $tagParams->tag_biz_id == null) {
            $result->setError(\Config_Error::ERR_QUERY_TAG_PARAMS_IS_NULL);
            return $result;
        }

        $tagModel = TagModel::getInstance();

        /**
         * 从缓存里取
         */
        $tagId = $tagModel->getTagIdCache($tagParams->tag_biz_type, $tagParams->tag_biz_id);
        if ($tagId != 0) {
            $result->setSuccessWithResult($tagId);
            return $result;
        }
        /**
         * 如果缓存没有命中则从db查询,不论是tagid否存在构造一个数据,避免缓存被击穿
         */
        $tagId = $tagModel->queryTagIdByBiz($tagParams->tag_biz_id, $tagParams->tag_biz_type);
        $tagInfoDTO = new TagInfoDTO();
        $tagInfoDTO->id = $tagId;
        $tagInfoDTO->tag_biz_type = $tagParams->tag_biz_type;
        $tagInfoDTO->tag_biz_id = $tagParams->tag_biz_id;
        $tagModel->setTagInfoIdCache($tagInfoDTO);

        $result->setSuccessWithResult($tagId);
        return $result;
    }

    public function queryTagEntityList(QueryTagEntityListParams $queryTagEntityListParams): Result
    {
        $result = new Result();
        $tagIdList = [];
        if ($queryTagEntityListParams == null) {
            $result->setError(\Config_Error::ERR_QUERY_TAG_ENTITY_PARAMS_IS_NULL);
            return $result;
        }

        $tagModel = TagModel::getInstance();

        /**
         * 如果传过来的tag_id为空,那就通过业务类型和id去查询tagid;
         */
        $tagId = $queryTagEntityListParams->tag_id;
        if ($tagId == 0) {
            $tagId = $tagModel->queryTagIdByBiz($queryTagEntityListParams->tag_biz_id, $queryTagEntityListParams->tag_biz_type);
        }
        $entityBizType = $queryTagEntityListParams->entity_biz_type;
        $pageSize = $queryTagEntityListParams->page_size;
        $offset = ($queryTagEntityListParams->index * $pageSize);

        $tagEntityBizIdList = $tagModel->queryTagEntityListFromCache($tagId, $entityBizType, $offset, $pageSize);
        if ($tagEntityBizIdList != null) {
            $tagIdList = $tagEntityBizIdList;
            $result->setSuccessWithResult($tagIdList);
            return $result;
        }

        $tagEntityDTOList = $tagModel->queryTagEntityList($tagId, $entityBizType, $offset, $pageSize);
        if ($tagEntityDTOList != null) {
            /** @var TagEntityDTO $tagEntity */
            foreach ($tagEntityDTOList as $tagEntity) {
                array_push($tagIdList, $tagEntity->entity_biz_id);
            }
        }
        $result->setSuccessWithResult($tagIdList);
        return $result;
    }

    public function hasTagEntity(TagEntityParams $tagEntityParams): Result
    {
        $result = new Result();
        $tagModel = TagModel::getInstance();
        $tagId = $tagEntityParams->tag_id;
        $entityBizId = $tagEntityParams->entity_biz_id;
        $entityBizType = $tagEntityParams->entity_biz_type;


        $hasTag = $tagModel->hasTagEntity($tagId, $entityBizId, $entityBizType);
        $result->setSuccessWithResult($hasTag);
        return $result;
    }

    public function getTagEntityCount(TagEntityParams $tagEntityParams): Result
    {
        $result = new Result();
        $tagModel = TagModel::getInstance();
        /**
         * 如果传过来的tag_id为空,那就通过业务类型和id去查询tagid;
         */
        $tagId = $tagEntityParams->tag_id;
        if ($tagId == 0) {
            $tagId = $tagModel->queryTagIdByBiz($tagEntityParams->tag_biz_id, $tagEntityParams->tag_biz_type);
        }
        if ($tagId == 0) {
            $result->setSuccessWithResult(0);
            return $result;
        }
        $count = $tagModel->getTagEntityCount($tagId, $tagEntityParams->entity_biz_type);
        $result->setSuccessWithResult($count);
        return $result;
    }

    /**
     * 把一个tag下面所有的标记都删除
     * @param TagEntityParams $tagEntityParams
     * @return Result
     */
    public function cleanTagEntity(TagEntityParams $tagEntityParams): Result
    {
        $result = new Result();
        $tagModel = TagModel::getInstance();
        /**
         * 如果传过来的tag_id为空,那就通过业务类型和id去查询tagid;
         */
        $tagId = $tagEntityParams->tag_id;
        if ($tagId == 0) {
            $tagId = $tagModel->queryTagIdByBiz($tagEntityParams->tag_biz_id, $tagEntityParams->tag_biz_type);
        }
        if ($tagId == 0) {
            $result->setSuccessWithResult(0);
            return $result;
        }
        $tagEntityDTO = new TagEntityDTO();
        $tagEntityDTO->tag_id = $tagId;
        $tagEntityDTO->entity_biz_type = $tagEntityParams->entity_biz_type;
        $tagModel->cleanTagEntityCache($tagEntityDTO);
        $tagModel->cleanTagEntity($tagEntityDTO);
        $result->setSuccessWithResult(true);
        return $result;
    }


    public function queryFullSiteTagEntityDTOList(QueryFullSiteTagEntityListParams $queryFullSiteTagEntityListParams): Result
    {
        $result = new Result();
        $tagEntityDTOList = [];
        if ($queryFullSiteTagEntityListParams == null) {
            $result->setError(\Config_Error::ERR_QUERY_TAG_ENTITY_PARAMS_IS_NULL);
            return $result;
        }

        $tagModel = TagModel::getInstance();

        /**
         * 如果传过来的tag_id为空,那就通过业务类型和id去查询tagid;
         */
        $tagId = $queryFullSiteTagEntityListParams->tag_id;
        if ($tagId == 0) {
            $tagId = $tagModel->queryTagIdByBiz($queryFullSiteTagEntityListParams->tag_biz_id, $queryFullSiteTagEntityListParams->tag_biz_type);
        }

        $pageSize = $queryFullSiteTagEntityListParams->page_size;
        $offset = ($queryFullSiteTagEntityListParams->index * $pageSize);

        $tagEntityDTOList = $tagModel->queryFullSiteTagEntityListFromCache($tagId, $offset, $pageSize);
        if ($tagEntityDTOList != null) {
            $result->setSuccessWithResult($tagEntityDTOList);
            return $result;
        }

        $tagEntityDTOList = $tagModel->queryTagEntityList($tagId, null, $offset, $pageSize);

        $result->setSuccessWithResult($tagEntityDTOList);
        return $result;
    }
}