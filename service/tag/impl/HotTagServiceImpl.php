<?php


namespace service\tag\impl;

use facade\request\tag\CreateHotTagParams;
use facade\request\tag\QueryHotTagListParams;
use facade\response\Result;
use facade\service\tag\HotTagService;
use model\dto\tag\HotTagDTO;
use model\tag\HotTagModel;

class HotTagServiceImpl implements HotTagService
{

    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function createTagIfNotExists(CreateHotTagParams $tagParams): Result
    {
        $result = new Result();
        $hotTagModel = HotTagModel::getInstance();
        /**
         * 校验是否已经设置热门标签
         */
        $tagRecord = $hotTagModel->queryHotTagInfoByBiz($tagParams->biz_tag_type, $tagParams->biz_tag_id);
        if ($tagRecord != null) {
            $result->setSuccessWithResult($tagRecord->id);
            return $result;
        }

        $hotTagDTO = new HotTagDTO();
        \FUR_Core::copyProperties($tagParams, $hotTagDTO);
        $hotTagDTO->created_timestamp = time();
        $id = $hotTagModel->insertHotTag($hotTagDTO);


        $result->setSuccessWithResult($id);
        return $result;

    }


    public function deleteHotTag(int $hotTagId): ?Result
    {

        $result = new Result();
        $tagModel = HotTagModel::getInstance();
        /**
         * 如果传过来的tag_id为空,那就通过业务类型和id去查询tagid;
         */

        $hotTagDTO = new HotTagDTO();
        $hotTagDTO->id = $hotTagId;
        $tagModel->deleteHotTag($hotTagDTO);
        $result->setSuccessWithResult(true);
        return $result;

    }


    public function getHotTagList(QueryHotTagListParams $queryHotTagListParams): ?Result
    {
        $result = new Result();
        $tagModel = HotTagModel::getInstance();


        $pageSize = intval($queryHotTagListParams->page_size);
        $offset = ($queryHotTagListParams->index * $pageSize);


        $hotTagList = $tagModel->queryHotTagList($offset, $pageSize);

        $result->setSuccessWithResult($hotTagList);
        return $result;

    }


}