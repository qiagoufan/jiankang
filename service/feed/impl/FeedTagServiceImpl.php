<?php


namespace service\feed\impl;

use business\feed\FeedTagConversionUtil;
use facade\request\feed\GetHashTagListParams;
use facade\request\product\GetProductDetailParams;
use facade\request\tag\QueryHotTagListParams;
use facade\request\user\FollowParams;
use facade\response\base\PageInfoResp;
use facade\response\feed\FeedHashTagCategoryResp;
use facade\response\feed\FeedHashTagListResp;
use facade\response\Result;
use facade\service\feed\FeedTagService;
use model\dto\ProductItemDTO;
use model\dto\UserStarDTO;
use service\product\impl\ProductServiceImpl;
use service\tag\impl\HotTagServiceImpl;
use service\user\impl\FollowServiceImpl;
use service\user\impl\StarServiceImpl;


class FeedTagServiceImpl implements FeedTagService
{

    const SCENE_FEED_GENERATOR = 'feed_generator';
    const SCENE_INDEX_SEARCH = 'index_search';

    public static $_instance;

    const DEFAULT_PAGE_SIZE = 10;


    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function getHashTagCategoryList(string $scene): ?Result
    {
        $result = new Result();
        $feedCategoryArray = [];

        if ($scene == self::SCENE_FEED_GENERATOR) {
            $feedTagFocusCategory = new FeedHashTagCategoryResp();
            $feedTagFocusCategory->hashtag_category_type = 'focus';
            $feedTagFocusCategory->hashtag_category_name = '我关注的';
            array_push($feedCategoryArray, $feedTagFocusCategory);
        }

        $feedTagStarCategory = new FeedHashTagCategoryResp();
        $feedTagStarCategory->hashtag_category_type = 'star';
        $feedTagStarCategory->hashtag_category_name = '明星';
        array_push($feedCategoryArray, $feedTagStarCategory);

        $feedTagBrandCategory = new FeedHashTagCategoryResp();
        $feedTagBrandCategory->hashtag_category_type = 'brand';
        $feedTagBrandCategory->hashtag_category_name = '品牌';
        array_push($feedCategoryArray, $feedTagBrandCategory);

        $feedTagProductCategory = new FeedHashTagCategoryResp();
        $feedTagProductCategory->hashtag_category_type = 'sku';
        $feedTagProductCategory->hashtag_category_name = '商品';
        array_push($feedCategoryArray, $feedTagProductCategory);


        $result->setSuccessWithResult($feedCategoryArray);
        return $result;
    }

    public function getHashTagList(GetHashTagListParams $getFeedTagListParams): ?Result
    {
        $result = new Result();
        $hashTagList = [];
        $hashTagListResp = new FeedHashTagListResp();
        /**
         * 数据校验
         */
        if ($getFeedTagListParams->hash_tag_type == null) {
            $getFeedTagListParams->hash_tag_type = 'star';
        }

        if ($getFeedTagListParams->hash_tag_type == 'focus') {
            $userId = $getFeedTagListParams->user_id;
            if ($userId == 0) {
                $result->setSuccessWithResult($hashTagListResp);
                return $result;
            }

            /**获取关注列表*/
            $followService = FollowServiceImpl::getInstance();
            $followParams = new FollowParams();
            $followParams->target_type = UserStarDTO::STAR_BIZ_TYPE;
            $followParams->user_id = $userId;
            $followParams->index = $getFeedTagListParams->index;
            $focusStarListRet = $followService->getUserFocusList($followParams);

            if (Result::isSuccess($focusStarListRet) && ($focusStarListRet->data != null)) {
                $starIdList = $focusStarListRet->data;
                $starService = StarServiceImpl::getInstance();
                foreach ($starIdList as $starId) {
                    $userStarDTO = $starService->getStarInfo($starId)->data;
                    $focusFeedTagResp = FeedTagConversionUtil::buildStarFeedTagInfo($userStarDTO);
                    if ($focusFeedTagResp == null) {
                        continue;
                    }
                    array_push($hashTagList, $focusFeedTagResp);
                }
            }
        } elseif ($getFeedTagListParams->hash_tag_type == 'star') {
            $starServiceImpl = StarServiceImpl::getInstance();
            $starListRet = $starServiceImpl->getStarList($getFeedTagListParams->index, self::DEFAULT_PAGE_SIZE, null, UserStarDTO::IP_TYPE_IDOL);
            foreach ($starListRet->data as $userStarDTO) {
                $starFeedTagResp = FeedTagConversionUtil::buildStarFeedTagInfo($userStarDTO);
                if ($starFeedTagResp == null) {
                    continue;
                }
                array_push($hashTagList, $starFeedTagResp);
            }

        } elseif ($getFeedTagListParams->hash_tag_type == 'brand') {

            $starServiceImpl = StarServiceImpl::getInstance();
            $starListRet = $starServiceImpl->getStarList($getFeedTagListParams->index, self::DEFAULT_PAGE_SIZE, null, UserStarDTO::IP_TYPE_FAMOUS);
            foreach ($starListRet->data as $userStarDTO) {
                $starFeedTagResp = FeedTagConversionUtil::buildStarFeedTagInfo($userStarDTO);
                if ($starFeedTagResp == null) {
                    continue;
                }
                array_push($hashTagList, $starFeedTagResp);
            }

        } elseif ($getFeedTagListParams->hash_tag_type == 'sku') {
            /** 拿出所有的商品列表 */
            $offset = $getFeedTagListParams->index * self::DEFAULT_PAGE_SIZE;
            $productItemListRet = ProductServiceImpl::getInstance()->getDistinctProductItemList($offset, self::DEFAULT_PAGE_SIZE);
            if (Result::isSuccess($productItemListRet)) {
                $itemList = $productItemListRet->data;
                /** @var ProductItemDTO $productItemDTO */
                foreach ($itemList as $productItemDTO) {
                    $productFeedTagResp = FeedTagConversionUtil::buildProductFeedTagInfoBySkuId($productItemDTO->default_sku_id);
                    if ($productFeedTagResp == null) {
                        continue;
                    }
                    array_push($hashTagList, $productFeedTagResp);
                }
            }
        } elseif ($getFeedTagListParams->hash_tag_type == 'hot') {
            $hashTagList = $this->getHotTagList($getFeedTagListParams);
        }

        $index = $getFeedTagListParams->index;
        $page_info = PageInfoResp::buildPageInfoRespBaseLine($index, self::DEFAULT_PAGE_SIZE, count($hashTagList));
        $hashTagListResp->page_info = $page_info;
        $hashTagListResp->hashtag_list = $hashTagList;

        $result->setSuccessWithResult($hashTagListResp);
        return $result;
    }

    /**
     * @return array|null
     * 获取热门标签列表
     */
    private function getHotTagList(GetHashTagListParams $getFeedTagListParams): ?array
    {
        $hashTagList = [];
        $queryHotTagListParams = new QueryHotTagListParams();
        \FUR_Core::copyProperties($getFeedTagListParams, $queryHotTagListParams);

        $hotTagServiceImpl = HotTagServiceImpl::getInstance();
        $hotTagListRet = $hotTagServiceImpl->getHotTagList($queryHotTagListParams);
        if (!Result::isSuccess($hotTagListRet)) {
            return $hashTagList;
        }

        $hotTagListData = $hotTagListRet->data;
        foreach ($hotTagListData as $tag) {
            if ($tag->biz_tag_type == 'star') {
                $starServiceImpl = StarServiceImpl::getInstance();
                $userStarRet = $starServiceImpl->getStarInfo($tag->biz_tag_id);
                if (!Result::isSuccess($userStarRet)) {
                    continue;
                }
                $userStarDTO = $userStarRet->data;
                $starFeedTagResp = FeedTagConversionUtil::buildStarFeedTagInfo($userStarDTO);
                if ($starFeedTagResp == null) {
                    continue;
                }
                array_push($hashTagList, $starFeedTagResp);

            } elseif ($tag->biz_tag_type == 'sku') {

                $productSkuDetailParams = new GetProductDetailParams();
                $productSkuDetailParams->sku_id = $tag->biz_tag_id;
                $productSkuServiceImpl = ProductServiceImpl::getInstance();
                $productSkuDetailResult = $productSkuServiceImpl->getProductDetail($productSkuDetailParams);
                if (!Result::isSuccess($productSkuDetailResult)) {
                    continue;
                }
                $productSkuDTO = $productSkuDetailResult->data;
                $ProductFeedTagResp = FeedTagConversionUtil::buildProductFeedTagInfo($productSkuDTO);
                array_push($hashTagList, $ProductFeedTagResp);
            }
        }


        return $hashTagList;

    }
}