<?php


namespace service\feed\impl;

use business\feed\FeedTagConversionUtil;
use business\feed\PickContentUtil;
use facade\response\feed\content\AnalysisResultResp;
use facade\response\feed\FeedTagResp;
use facade\response\Result;
use facade\service\feed\FeedContentConversionService;
use lib\PregHelper;
use lib\AppConstant;
use model\feed\dto\FeedOutsiteLinkDTO;
use model\feed\FeedOutsiteLinkModel;

class FeedContentConversionServiceImpl implements FeedContentConversionService
{

    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    const SITE_TYPE_FEED = 'feed';
    const SITE_TYPE_PRODUCT = 'product';

    public function conversionContent(string $content): ?Result
    {
        $result = new Result();
        $link = PregHelper::extractHttpLink($content);
        if ($link == null) {
            $result->setSuccessWithResult(true);
            return $result;
        }
        $siteType = $this->getLinkSiteType($link);
        $analysisResultResp = new AnalysisResultResp();
        if ($siteType == self::SITE_TYPE_FEED) {
            $analysisResultResp = $this->getFeedAnalysisResult($link);
        } else {
            $analysisResultResp = $this->getProductAnalysisResult($link);
        }
        if ($analysisResultResp->content == null) {
            $analysisResultResp->content = $content;
        }
        $result->setSuccessWithResult($analysisResultResp);
        return $result;
    }


    private function getLinkSiteType($link)
    {
        if (strpos($link, 'tb.cn') ||
            strpos($link, 'jd.com') ||
            strpos($link, 'taobao.com') ||
            strpos($link, 'tmall.com')
        ) {
            return self::SITE_TYPE_PRODUCT;
        } elseif (strpos($link, 'weibo.cn') ||
            strpos($link, 'weibo.com')) {
            return self::SITE_TYPE_FEED;
        } else {
            return self::SITE_TYPE_FEED;
        }

    }


    private function getFeedAnalysisResult($link)
    {
        $analysisResultResp = new AnalysisResultResp();
        $analysisResultResp->content_type = self::SITE_TYPE_FEED;

        $feedOutsiteLinkDTO = new FeedOutsiteLinkDTO();
        $feedOutsiteLinkDTO->link_address = $link;
        $feedOutsiteLinkDTO->created_timestamp = time();

        if (strpos($link, 'weibo.cn') ||
            strpos($link, 'weibo.com')) {
            PickContentUtil::pickWeiboContent($link, $analysisResultResp, $feedOutsiteLinkDTO);
        } else {
            PickContentUtil::pickNormalContent($link, $analysisResultResp, $feedOutsiteLinkDTO);
        }

        $analysisResultResp->hash_tag_list = $this->buildhashTagList($feedOutsiteLinkDTO);
        return $analysisResultResp;
    }

    private function getProductAnalysisResult($link)
    {
        $analysisResultResp = new AnalysisResultResp();
        $analysisResultResp->content_type = self::SITE_TYPE_PRODUCT;

        $feedOutsiteLinkDTO = new FeedOutsiteLinkDTO();
        $feedOutsiteLinkDTO->link_address = $link;
        $feedOutsiteLinkDTO->link_type = FeedOutsiteLinkDTO::LINK_TYPE_PRODUCT;
        $feedOutsiteLinkDTO->created_timestamp = time();
        PickContentUtil::pickProductContent($link, $analysisResultResp, $feedOutsiteLinkDTO);


        $analysisResultResp->hash_tag_list = $this->buildhashTagList($feedOutsiteLinkDTO);
        return $analysisResultResp;
    }

    private function buildhashTagList(FeedOutsiteLinkDTO $feedOutsiteLinkDTO): ?array
    {
        $hashtagList = [];
        if ($feedOutsiteLinkDTO->link_title == null) {
            return $hashtagList;
        }

        $hashtagId = 0;
        $feedOutsiteLinkDTORet = FeedOutsiteServiceImpl::getInstance()->getOutsiteInfoByLink($feedOutsiteLinkDTO->link_address);
        if (Result::isSuccess($feedOutsiteLinkDTORet)) {
            $hashtagId = $feedOutsiteLinkDTORet->data->id;
        } else {
            $insertRet = FeedOutsiteServiceImpl::getInstance()->insert($feedOutsiteLinkDTO);
            if (Result::isSuccess($insertRet)) {
                $hashtagId = $insertRet->data;
            }

        }
        $hashtag = FeedTagConversionUtil::buildOutsiteLinkTag($hashtagId, $feedOutsiteLinkDTO->link_address);
        array_push($hashtagList, $hashtag);
        return $hashtagList;
    }

}