<?php


namespace service\feed\impl;

use facade\request\admin\GetFeedInformationListParams;
use facade\request\feed\CreateFeedParams;
use facade\request\feed\GetFeedListParams;
use facade\request\feed\news\CreateFeedInformationParams;
use facade\request\PageParams;
use facade\response\Result;
use facade\service\feed\FeedInformationService;
use facade\service\feed\FeedService;
use model\dto\FeedReceiveDTO;
use model\dto\FeedSendDTO;
use model\dto\UserDTO;
use model\dto\UserExtraDTO;
use model\feed\dto\FeedInformationDTO;
use model\feed\FeedInformationModel;
use model\FeedModel;
use model\UserFollowModel;
use model\dto\FeedInfoDTO;
use service\user\impl\FollowServiceImpl;
use service\user\impl\UserExtraServiceImpl;


class FeedInformationServiceImpl implements FeedInformationService
{

    public static $_instance;

    const CACHE_TIME = 60;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function createFeedInformation(CreateFeedInformationParams $createFeedInformationParams): ?Result
    {
        $result = new Result();
        $feedInformationDTO = new FeedInformationDTO();
        \FUR_Core::copyProperties($createFeedInformationParams, $feedInformationDTO);
        $feedInformationDTO->created_timestamp = time();
        $feedInformationDTO->status = FeedInformationDTO::STATUS_NORMAL;
        $insertId = FeedInformationModel::getInstance()->insertRecord($feedInformationDTO);
        $result->setSuccessWithResult($insertId);
        return $result;
    }

    public function getFeedInformationDetail(int $id): ?Result
    {
        $result = new Result();
        $feedInformationDTO = FeedInformationModel::getInstance()->getDetail($id);
        $result->setSuccessWithResult($feedInformationDTO);
        return $result;
    }


    public function getFeedInformationList(PageParams $pageParams): ?Result
    {
        $result = new Result();
        $offset = $pageParams->index * $pageParams->page_size;
        $pageSize = $pageParams->page_size;
        $feedInformationList = FeedInformationModel::getInstance()->getInformationList($offset, $pageSize);
        $result->setSuccessWithResult($feedInformationList);
        return $result;

    }


    public function queryFeedInformationListCount(): ?Result
    {
        $result = new Result();
        $feedInformationListCount = FeedInformationModel::getInstance()->queryInformationListCount();
        $result->setSuccessWithResult($feedInformationListCount);
        return $result;
    }


    public function delInformationById(int $informationId): ?Result
    {
        $result = new Result();
        $delRet = FeedInformationModel::getInstance()->deleteRecord($informationId);
        $result->setSuccessWithResult($delRet);
        return $result;
    }


    public function updateFeedInformationDetail(FeedInformationDTO $feedInformationDTO): ?Result
    {
        $result = new Result();
        $updateRet = FeedInformationModel::getInstance()->updateInformationById($feedInformationDTO);
        $result->setSuccessWithResult($updateRet);
        return $result;
    }
}