<?php


namespace service\feed\impl;

use facade\request\feed\heat\TriggerEventParams;
use facade\response\Result;
use facade\service\feed\FeedHeatService;
use model\dto\FeedInfoDTO;
use model\dto\UserDTO;
use model\feed\dto\FeedHeatDTO;
use model\feed\FeedHeatModel;
use service\user\impl\UserServiceImpl;

class FeedHeatServiceImpl implements FeedHeatService
{

    const ACTION_TYPE_INCR = 'incr';
    const ACTION_TYPE_DECR = 'decr';

    /**
     * 发布
     */
    const EVENT_PUBLISH = 'publish';

    /**
     * 曝光
     */
    const EVENT_EXPOSURE = 'exposure';
    /**
     * 精选
     */
    const EVENT_FEATURE = 'feature';
    /**
     * 点赞
     */
    const EVENT_LIKE = 'like';
    /**
     * 评论
     */
    const EVENT_COMMENT = 'comment';
    /**
     * 收藏
     */
    const EVENT_COLLECT = 'collect';
    /**
     * 收藏
     */
    const EVENT_SHARE = 'share';

    /**
     * 点击
     */
    const EVENT_CLICK = 'click';

    /**
     * 定时任务
     */
    const EVENT_SCHEDULE = 'schedule';

    /**
     * 热度对应表
     */
    const EVENT_POINT_MAP = [
//        'publish' => 100,
        'exposure' => 1,
//        'feature' => 600,
//        'like' => 3,
//        'comment' => 10,
//        'collect' => 10,
//        'share' => 30,
        'click' => 2
    ];

    const ADMINISTRATOR_PUBLISH_POINT = 100;


    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    /**
     * feed触发事件
     * @param TriggerEventParams $triggerEventParams
     * @return Result|null
     */
    public function triggerEvent(TriggerEventParams $triggerEventParams): ?Result
    {
        $result = new Result();
        $point = 0;
        if ($triggerEventParams->event_type == self::EVENT_PUBLISH) {
            $point = $this->getPublishEventPoint($triggerEventParams->feed_id);
        } elseif ($triggerEventParams->event_type == self::EVENT_SCHEDULE) {
            $point = $triggerEventParams->point;
        } else {
            if (isset(self::EVENT_POINT_MAP[$triggerEventParams->event_type])) {
                $point = self::EVENT_POINT_MAP[$triggerEventParams->event_type];
            }
            if ($triggerEventParams->action_type == self::ACTION_TYPE_DECR) {
                $point = 0 - $point;
            }
        }
        if ($point == 0) {
            $result->setSuccessWithResult(0);
            return $result;
        }

        $feedHeatDTO = new FeedHeatDTO();
        $feedHeatDTO->feed_id = $triggerEventParams->feed_id;
        $feedHeatDTO->point = $point;
        FeedHeatModel::getInstance()->incrHeatPoint($feedHeatDTO);

        FeedHeatModel::getInstance()->incrPointCache($feedHeatDTO);


        $result->setSuccessWithResult($point);
        return $result;

    }

    /**
     * 获取热度值
     * @param int $feedId
     * @return Result|null
     */
    public function getHeatValue(int $feedId): ?Result
    {
        $result = new Result();

        $point = FeedHeatModel::getInstance()->getPointCache($feedId);
        if ($point !== null) {
            $result->setSuccessWithResult($point);
            return $result;
        }
        $point = (int)FeedHeatModel::getInstance()->getPoint($feedId);
        if ($point < 0) {
            $point = 0;
        }
        $result->setSuccessWithResult($point);
        return $result;
    }


    /**
     * 计算发布内容积分
     * @param int $feedId
     * @return int
     */
    private function getPublishEventPoint(int $feedId): int
    {
        $feedInfoRet = FeedServiceImpl::getInstance()->getFeedDetail($feedId);
        if (!Result::isSuccess($feedInfoRet)) {
            return 0;
        }
        $administrator_publish_point = rand(100, 999);

        /** @var FeedInfoDTO $feedInfoDTO */
        $feedInfoDTO = $feedInfoRet->data;
        $authorId = $feedInfoDTO->author_id;
        if ($authorId < 1000000) {
            return $administrator_publish_point;
        }
        $authorInfoRet = UserServiceImpl::getInstance()->getValidUserInfo($authorId);
        if (!Result::isSuccess($authorInfoRet)) {
            return 0;
        }
//        if ($authorInfoRet->data->user_type & UserDTO::USER_TYPE_ADMIN) {
//            return $administrator_publish_point;
//        }

        return 0;
    }


}