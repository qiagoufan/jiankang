<?php


namespace service\feed\impl;

use facade\request\feed\heat\TriggerEventParams;
use facade\response\Result;
use facade\service\feed\FeedHeatService;
use facade\service\feed\FeedOutsiteService;
use model\dto\FeedInfoDTO;
use model\dto\UserDTO;
use model\feed\dto\FeedHeatDTO;
use model\feed\dto\FeedOutsiteLinkDTO;
use model\feed\FeedHeatModel;
use model\feed\FeedOutsiteLinkModel;
use service\user\impl\UserServiceImpl;

class FeedOutsiteServiceImpl implements FeedOutsiteService
{

    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function insert(FeedOutsiteLinkDTO $feedOutsiteLinkDTO): ?Result
    {
        $result = new Result();
        $id = FeedOutsiteLinkModel::getInstance()->insert($feedOutsiteLinkDTO);
        $result->setSuccessWithResult($id);
        return $result;
    }

    public function getOutsiteInfoById(int $linkId): ?Result
    {
        $result = new Result();
        $feedOutsiteLinkDTO = FeedOutsiteLinkModel::getInstance()->getRecord($linkId);
        $result->setSuccessWithResult($feedOutsiteLinkDTO);
        return $result;
    }

    public function getOutsiteInfoByLink(string $link): ?Result
    {
        $result = new Result();
        $feedOutsiteLinkDTO = FeedOutsiteLinkModel::getInstance()->queryRecordByLink($link);
        $result->setSuccessWithResult($feedOutsiteLinkDTO);
        return $result;
    }

    public function updateRecord(FeedOutsiteLinkDTO $feedOutsiteLinkDTO): ?Result
    {
        $result = new Result();
        $feedOutsiteLinkDTO = FeedOutsiteLinkModel::getInstance()->updateRecord($feedOutsiteLinkDTO);
        $result->setSuccessWithResult($feedOutsiteLinkDTO);
        return $result;
    }
}