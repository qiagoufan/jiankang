<?php


namespace service\feed\impl;

use facade\request\feed\CreateFeedCrawlParams;
use facade\request\feed\CreateFeedParams;
use facade\request\feed\GetFeedListParams;
use facade\request\feed\QueryManageFeedListParams;
use facade\request\maintain\CreateLeaderboardRecordParams;
use facade\request\PageParams;
use facade\request\tag\TagEntityParams;
use facade\response\Result;
use facade\service\feed\FeedService;
use lib\AppConstant;
use model\feed\dto\FeedCategoryDTO;
use model\feed\dto\FeedInfoDTO;
use model\feed\dto\FeedSendDTO;
use model\feed\FeedModel;
use model\feed\FeedCategoryModel;
use service\maintain\impl\MaintainLeaderboardServiceImpl;
use service\tag\impl\TagServiceImpl;
use service\user\impl\FollowServiceImpl;
use service\user\impl\StarServiceImpl;
use service\user\impl\UserExtraServiceImpl;

class FeedServiceImpl implements FeedService
{

    public static $_instance;

    const CACHE_TIME = 60;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function createFeed(CreateFeedParams $createFeedParams): ?Result
    {
        $result = new Result();
        $feedInfoDTO = new FeedInfoDTO();
        \FUR_Core::copyProperties($createFeedParams, $feedInfoDTO);
        $feedModel = FeedModel::getInstance();
        $feedInfoDTO->created_timestamp = time();
        $feedInfoDTO->published_timestamp = time();
        $feedInfoDTO->status = \Config_Const::FEED_STATUS_WAIT_CHECK;
        $feedInfoDTO->detail_url = $createFeedParams->detail_url;
        $id = $feedModel->insertRecord($feedInfoDTO);

//        $feedSendDTO = $this->buildDTO($createFeedParams->author_id, $id, $feedInfoDTO->published_timestamp);
//        /**
//         * 用户发送历史
//         */
//        $feedModel->insertUserSendRecord($feedSendDTO);
        /**
         * 用户发送历史的缓存(zset)
         */
//        $feedModel->updateUserSendHistory($feedSendDTO);

        $result->setSuccessWithResult($id);

        return $result;

    }

    public function getFeedCategory($id): Result
    {
        $result = new Result();
        $data = FeedCategoryModel::getInstance()->getFeedCategory($id);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function insertFeedCategory(FeedCategoryDTO $FeedCategoryDTO): Result
    {
        $result = new Result();
        $data = FeedCategoryModel::getInstance()->insertFeedCategory($FeedCategoryDTO);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function deleteFeedCategory($id): Result
    {
        $result = new Result();
        $data = FeedCategoryModel::getInstance()->deleteFeedCategory($id);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function updateFeedCategory(FeedCategoryDTO $FeedCategoryDTO): Result
    {
        $result = new Result();
        $data = FeedCategoryModel::getInstance()->updateFeedCategory($FeedCategoryDTO);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function getFeedCategoryList(): Result
    {
        $result = new Result();
        $data = FeedCategoryModel::getInstance()->getFeedCategoryList();
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function getFeedDetail(int $feedId): ?Result
    {
        $result = new Result();
        $feedModel = FeedModel::getInstance();
        $feedInfoDTO = $feedModel->queryFeedDetail($feedId);
        if (!$feedInfoDTO) {
            $result->setError(\Config_Error::ERR_FEED_NOT_EXIST);
            return $result;
        }


        $result->setSuccessWithResult($feedInfoDTO);
        return $result;

    }


    public function getFeedList(GetFeedListParams $getFeedListParams): ?Result
    {
        $result = new Result();
        $feedModel = FeedModel::getInstance();
        $indexFeedDTOList = $feedModel->queryFeedList($getFeedListParams);
        $result->setSuccessWithResult($indexFeedDTOList);
        return $result;

    }

    public function queryManageFeedList(QueryManageFeedListParams $params): ?Result
    {
        $result = new Result();
        $feedModel = FeedModel::getInstance();
        $indexFeedDTOList = $feedModel->queryManageFeedList($params);
        $result->setSuccessWithResult($indexFeedDTOList);
        return $result;

    }

    public function queryManageFeedCount(QueryManageFeedListParams $params): ?Result
    {
        $result = new Result();
        $feedModel = FeedModel::getInstance();
        $indexFeedDTOList = $feedModel->queryManageFeedCount($params);
        $result->setSuccessWithResult($indexFeedDTOList);
        return $result;

    }

    public function updateUserReceiveFeedList(int $userId): ?Result
    {
        $result = new Result();
        $now = time();

        /**
         * 获取用户上次访问时间,来做一个结束点
         */
        $userExtraServiceImpl = UserExtraServiceImpl::getInstance();
        $userLatestVisitFeedTime = $userExtraServiceImpl->getUserExtra($userId, UserExtraDTO::FIELD_USER_FEED_LATEST_VISIT_TIME);

        /**
         * 获取用户的关注列表
         */
        $followService = FollowServiceImpl::getInstance();
        $userFocusListRet = $followService->getUserFocusList($userId);
        /**
         * 默认关注用户id为0的系统账号
         */
        $focusUserIdList = [0];
        if ($userFocusListRet != null && $userFocusListRet->errorCode == 0) {
            $focusUserIdList = array_merge($focusUserIdList, $userFocusListRet->data);
        }

        $feedModel = FeedModel::getInstance();

        /**
         * 循环获取关注的用户feed最新列表,然后写入到自己的收件箱
         */
        foreach ($focusUserIdList as $focusUserId) {
            $focusUserFeedList = $feedModel->getUserSendHistory($focusUserId, $now, $userLatestVisitFeedTime);

            $receiveFeedList = [];
            if (!empty($focusUserFeedList)) {
                foreach ($focusUserFeedList as $feedId => $timestamp) {
                    $feedReceiveDTO = FeedReceiveDTO::buildDTO($userId, $feedId, $timestamp);
                    array_push($receiveFeedList, $feedReceiveDTO);
                }
                $feedModel->updateUserReceiveList($receiveFeedList, $userId);
            }
        }
        $userExtraServiceImpl->setUserExtra($userId, UserExtraDTO::FIELD_USER_FEED_LATEST_VISIT_TIME, $now);
        $result->setSuccessWithResult(true);
        return $result;
    }


    public function getUserReceiveFeedList(int $userId, int $index, int $pageSize): ?Result
    {
        $result = new Result();
        $feedModel = FeedModel::getInstance();
        $start = $index * $pageSize;
        /**@var  array $receive_list * */

        /**
         * 获取用户feed详情
         */
        $feedInfoList = [];
        $receiveFeedIdList = $feedModel->getUserReceiveList($userId, $start, $pageSize);
        if (empty($receiveFeedIdList)) {
            $result->setSuccessWithResult($feedInfoList);
            return $result;
        }

        /**
         * 迭代查询feed详情
         */
        foreach ($receiveFeedIdList as $feedId) {
            $feedDTO = $feedModel->queryFeedDetail($feedId);
            if (empty($feedDTO)) {
                continue;
            }
            array_push($feedInfoList, $feedDTO);
        }
        $result->setSuccessWithResult($feedInfoList);
        return $result;
    }

    public function getUserPostFeedList(int $userId, int $index, int $pageSize): ?Result
    {
        $result = new Result();
        $feedModel = FeedModel::getInstance();
        $offset = $index * $pageSize;
        $feedInfoList = $feedModel->queryFeedListByUserId($userId, $offset, $pageSize);
        $result->setSuccessWithResult($feedInfoList);
        return $result;
    }


    /**
     * @param int $visitUserId
     * @param int $userId
     * @return Result|null
     * 获取个人主页动态总数
     */
    public function getUserPostFeedListCount(int $visitUserId, int $userId): ?Result
    {
        $result = new Result();
        $feedModel = FeedModel::getInstance();
        $countRet = $feedModel->queryUserPostFeedListCount($visitUserId, $userId);
        $result->setSuccessWithResult($countRet);
        return $result;
    }

    public function getFeedDetailRespCache(int $feedId): ?Result
    {
        $result = new Result();
        $feedModel = FeedModel::getInstance();
        $data = $feedModel->getFeedInfoRespCache($feedId, 'feed_detail');
        $result->setSuccessWithResult($data);
        return $result;
    }

    /**
     * 设置feed 缓存
     * @param int $feedId
     * @param string $field
     * @param string $value
     * @return Result|null
     */
    public function setFeedDetailRespCache(int $feedId, string $field, string $value): ?Result
    {
        $result = new Result();
        $feedModel = FeedModel::getInstance();
        $ret = $feedModel->setFeedInfoRespCache($feedId, $field, $value);

        $result->setSuccessWithResult($ret);
        return $result;
    }

    public function getUserPostCount(int $userId): ?Result
    {
        $result = new Result();
        $feedModel = FeedModel::getInstance();
        $count = intval($feedModel->getUserPostCount($userId));
        $result->setSuccessWithResult($count);
        return $result;
    }

    public function deleteFeed(int $feedId, int $userId = 0): ?Result
    {
        $result = new Result();
        $feedModel = FeedModel::getInstance();
        $deleteRet = $feedModel->deleteFeed($feedId);
//        $feedModel->deleteUserSendHistory($feedId);
//        if ($userId != 0) {
//            $feedModel->removeUserSendHistory($userId, $feedId);
//        }
        $result->setSuccessWithResult($deleteRet);
        return $result;
    }


    public function updateIdolMaintainLeaderboardScore(int $feedId, array $starIdList, string $recordType): ?Result
    {
        $result = new  Result();
        $result->setSuccessWithResult(true);


        /** @var FeedInfoDTO $feedInfo */
        $feedModel = FeedModel::getInstance();
        $feedInfoDTO = $feedModel->queryFeedDetail($feedId);
        if ($feedInfoDTO->template_type == FeedInfoDTO::TEMPLATE_TYPE_STANDARD_SINGLE_CONTENT_13) {
            return $result;
        }
        if (empty($starIdList)) {
            return $result;
        }

        $maintainLeaderboardService = MaintainLeaderboardServiceImpl::getInstance();
        foreach ($starIdList as $starId) {
            $starServiceImpl = StarServiceImpl::getInstance();
            $starInfoRet = $starServiceImpl->getStarInfo($starId);
            if (!Result::isSuccess($starInfoRet)) {
                continue;
            }
            /**
             * 给ip加人气值2点
             */
            $createLeaderboardRecordParams = new CreateLeaderboardRecordParams();
            $createLeaderboardRecordParams->star_id = $starId;
            $createLeaderboardRecordParams->user_id = $feedInfoDTO->author_id;
            $createLeaderboardRecordParams->record_type = $recordType;
            if ($recordType == MaintainLeaderboardRecordDTO::RECORD_TYPE_POST_FEED) {
                $createLeaderboardRecordParams->value = 2;
            } else {
                $createLeaderboardRecordParams->value = -2;
            }
            if ($starInfoRet->data->ip_type == UserStarDTO::IP_TYPE_IDOL) {
                $createLeaderboardRecordParams->rank_type = MaintainUserStarExtraDTO::RANK_TYPE_IDOL_POPULARITY;
            }

            $maintainLeaderboardService->createRecord($createLeaderboardRecordParams);
        }

        $result->setSuccessWithResult(true);
        return $result;
    }


    /**
     * @param array $starIdList
     * @return Result|null
     * 减去品牌讨论热度
     */
    public function reduceDiscussionHeat(array $starIdList): ?Result
    {
        $result = new  Result();
        $result->setSuccessWithResult(true);

        if (empty($starIdList)) {
            return $result;
        }

        foreach ($starIdList as $starId) {
            $starServiceImpl = StarServiceImpl::getInstance();
            $starInfoRet = $starServiceImpl->getStarInfo($starId);
            if (!Result::isSuccess($starInfoRet)) {
                continue;
            }
            /**
             * 减去讨论热度
             */
            if ($starInfoRet->data->ip_type == UserStarDTO::IP_TYPE_FAMOUS) {
                $starServiceImpl->reduceDiscussionHeat($starId, UserStarDTO::STAR_BIZ_TYPE);
            }
        }

        $result->setSuccessWithResult(true);
        return $result;
    }


    public function getIndexFeedList(GetFeedListParams $getFeedListParams): ?Result
    {
        $result = new Result();
        $indexFeedDTOList = FeedModel::getInstance()->queryFeedList($getFeedListParams);

        $result->setSuccessWithResult($indexFeedDTOList);
        return $result;
    }


    public function queryUserFeedList(PageParams $params): ?Result
    {
        $result = new Result();
        $indexFeedDTOList = FeedModel::getInstance()->queryUserFeedList($params);

        $result->setSuccessWithResult($indexFeedDTOList);
        return $result;
    }

    public function getIndexFeedIdList(GetFeedListParams $getFeedListParams): ?Result
    {
        $result = new Result();
        $feedIdList = [];
        $feedModel = FeedModel::getInstance();
        $featuredFeedList = $feedModel->getIndexFeedIdList($getFeedListParams);

        if ($featuredFeedList != null) {
            foreach ($featuredFeedList as $featuredFeed) {
                array_push($feedIdList, $featuredFeed->feed_id);
            }
        }
        $result->setSuccessWithResult($feedIdList);
        return $result;
    }


    public function deleteStarFeedTag(int $feedId, array $starIdList): ?Result
    {
        $result = new  Result();

        if (empty($starIdList)) {
            return $result;
        }

        $tagService = TagServiceImpl::getInstance();
        foreach ($starIdList as $starId) {
            $tagEntityParams = new TagEntityParams();
            $tagEntityParams->tag_id = $starId;
            $tagEntityParams->tag_biz_type = AppConstant::BIZ_TYPE_STAR;
            $tagEntityParams->entity_biz_type = AppConstant::BIZ_TYPE_FEED;
            $tagEntityParams->entity_biz_id = $feedId;
            $tagService->deleteTagEntity($tagEntityParams);

        }

        $result->setSuccessWithResult(true);
        return $result;
    }

    public function updateFeedDTO(FeedInfoDTO $feedInfoDTO): ?Result
    {
        $result = new Result();
        $feedInfoDTO->updated_timestamp = time();
        $data = FeedModel::getInstance()->updateFeedInfo($feedInfoDTO);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function getFeatureFeedIdListByTemplateType(int $index, int $templateType, int $pageSize): ?Result
    {
        $result = new Result();
        $feedIdList = [];
        $feedModel = FeedModel::getInstance();
        $featuredFeedList = $feedModel->getFeatureFeedIdListByTemplateType($index, $templateType, $pageSize);

        if ($featuredFeedList != null) {
            foreach ($featuredFeedList as $featuredFeed) {
                array_push($feedIdList, $featuredFeed->feed_id);
            }
        }

        $result->setSuccessWithResult($feedIdList);
        return $result;
    }


    public function getFeedContentImage(?FeedInfoDTO $feedInfoDTO): ?Result
    {
        $result = new Result();
        $feedImage = null;
        $feedInfoRespServiceImpl = FeedInfoRespServiceImpl::getInstance();
        $feedInfoResp = $feedInfoRespServiceImpl->conversionFeedInfoResp($feedInfoDTO);
        if ($feedInfoResp->feed_detail != null) {
            $feedContentList = $feedInfoResp->feed_detail->feed_content_list;
            foreach ($feedContentList as $feedContent) {
                if ($feedContent->cover != null) {
                    $feedImage = $feedContent->cover;
                }

                if ($feedContent->image != null) {
                    $feedImage = $feedContent->image;
                }
                break;
            }
        }
        $result->setSuccessWithResult($feedImage);
        return $result;
    }

    public static function buildDTO(int $user_id, int $feed_id, int $published_timestamp)
    {
        $feedSendDTO = new FeedSendDTO();
        $feedSendDTO->published_timestamp = $published_timestamp;
        $feedSendDTO->user_id = $user_id;
        $feedSendDTO->feed_id = $feed_id;
        return $feedSendDTO;

    }


}