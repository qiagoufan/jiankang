<?php

namespace service\feed\impl;


use business\feed\FeedInformationUtil;
use business\interactive\InteractiveConversionUtil;
use business\user\UserInfoConversionUtil;
use facade\request\feed\ConversionFeedParams;
use facade\request\feed\creation\FeedContent;
use facade\request\feed\heat\TriggerEventParams;
use facade\request\interactive\QueryInteractiveCommentListParams;
use facade\request\product\GetProductDetailParams;
use facade\request\tag\QueryTagEntityListParams;
use facade\response\feed\detail\comment\CommentResp;
use facade\response\feed\detail\decoration\FeedHashtag;
use facade\response\feed\detail\decoration\FeedHomePage;
use facade\response\feed\detail\element\FeedImageElementResp;
use facade\response\feed\detail\element\fragment\HotAreaResp;
use facade\response\feed\detail\FeedAuthorInfoResp;
use facade\response\feed\detail\FeedBaseInfoResp;
use facade\response\feed\detail\FeedDecorationResp;
use facade\response\feed\detail\FeedDetailResp;
use facade\response\feed\detail\FeedInteractiveInfoResp;
use facade\response\feed\detail\FeedTheatreChallengeResp;
use facade\response\feed\FeedInfoResp;
use facade\response\Result;
use facade\service\feed\FeedInfoRespService;
use lib\AppConstant;
use lib\ClientRouteHelper;
use lib\TimeHelper;
use model\feed\dto\FeedInfoDTO;
use model\interactive\dto\InteractiveCommentDTO;
use model\interactive\dto\InteractiveOverviewDTO;
use model\theatre\dto\TheatreChallengeDTO;
use model\user\dto\UserDTO;
use service\interactive\impl\InteractiveServiceImpl;
use service\product\impl\ProductServiceImpl;
use service\tag\impl\TagServiceImpl;
use service\theatre\impl\TheatreChallengeServiceImpl;
use service\user\impl\StarServiceImpl;
use service\user\impl\UserServiceImpl;

class FeedInfoRespServiceImpl implements FeedInfoRespService
{

    const SCENE_INDEX = 'index';
    const SCENE_DETAIL = 'detail';
    const SCENE_MY_STAR_MATERIALS = 'my_star_materials';
    const SCENE_STAR_CARD = 'star_card';
    const SCENE_SUPER_PRODUCT_CARD = 'super_product_card';
    const SCENE_MY_COLLECT = 'my_collect';
    const SCENE_USER_HOME_PAGE = 'homepage';
    const SCENE_SEARCH = 'search';
    const SCENE_THEATRE = 'theatre';
    const SCENE_PREVIEW = 'preview';


    const STAR_CARD_TYPE_POPULARITY = 'popularity_rank';
    const STAR_CARD_TYPE_STAR_ENERGY = 'star_energy_rank';

    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function conversionFeedInfoResp(?FeedInfoDTO &$feedInfoDTO, ?ConversionFeedParams &$conversionFeedParams = null): ?FeedInfoResp
    {
        if ($feedInfoDTO == null) {
            return null;
        }
        if ($conversionFeedParams == null) {
            $conversionFeedParams = new ConversionFeedParams();
        }
        $feedInfoResp = new FeedInfoResp();
        /**
         *如果不是虚拟feed,需要获取作者信息
         */
        $feedAuthorInfoResp = null;

        if ($feedInfoDTO->virtual != 1) {
            $feedAuthorInfoResp = $this->getFeedAuthorInfo($feedInfoDTO->author_id, $conversionFeedParams);
            $feedInfoResp->author_info = $feedAuthorInfoResp;
        }

        /**
         * 转换feed基础信息
         */

        $feedBaseInfoResp = $this->conversionFeedBaseInfo($feedInfoDTO, $conversionFeedParams);
        if ($feedBaseInfoResp == null) {
            return null;
        }
        $feedInfoResp->feed_base_info = $feedBaseInfoResp;


        /**
         * 转换feed 详情
         */
        $feedDetailResp = $this->conversionFeedDetail($feedInfoDTO, $conversionFeedParams);
        if ($feedDetailResp == null) {
            return null;
        }
        $feedInfoResp->feed_detail = $feedDetailResp;

        /** 预览feed不需要装饰信息和插入的广告 */
        $feedInfoResp->interactive_info = $this->getFeedInteractiveInfo($feedInfoDTO, $conversionFeedParams);
        $feedInfoResp->decoration_info = $this->getFeedDecorationInfo($feedInfoDTO, $conversionFeedParams);

        return $feedInfoResp;
    }


    private function getFeedAuthorInfo(int $userId, ConversionFeedParams $conversionFeedParams): ?FeedAuthorInfoResp
    {
        $userService = UserServiceImpl::getInstance();
        $userInfoResult = $userService->getValidUserInfo($userId);
        if (!Result::isSuccess($userInfoResult)) {
            return null;
        }
        /** @var UserDTO $userDTO */
        $userDTO = $userInfoResult->data;
        $feedAuthorInfoResp = new FeedAuthorInfoResp();
        $feedAuthorInfoResp->user_id = $userDTO->id;
        $feedAuthorInfoResp->real_name = $userDTO->real_name;
        $feedAuthorInfoResp->nick_name = $userDTO->nick_name;
        $feedAuthorInfoResp->avatar = $userDTO->avatar;
        return $feedAuthorInfoResp;
    }


    private function conversionFeedBaseInfo(FeedInfoDTO &$feedInfoDTO, ConversionFeedParams $conversionFeedParams = null): ?FeedBaseInfoResp
    {
        $feedBaseInfoResp = new FeedBaseInfoResp();
        $feedBaseInfoResp->feed_id = $feedInfoDTO->id;
        $feedBaseInfoResp->feed_type = $feedInfoDTO->feed_type;
        $feedBaseInfoResp->feed_status = $feedInfoDTO->status;

        if ($feedInfoDTO->status == -1) {
            $feedBaseInfoResp->feed_status_name = '审核中';
        } elseif ($feedInfoDTO->status == -2) {
            $feedBaseInfoResp->feed_status_name = '已拒绝';
        } elseif ($feedInfoDTO->status == 1) {
            $feedBaseInfoResp->feed_status_name = '已审核';
        } elseif ($feedInfoDTO->status == 0) {
            $feedBaseInfoResp->feed_status_name = '已审核';
        }
        $feedBaseInfoResp->publish_timestamp = $feedInfoDTO->published_timestamp;
        $feedBaseInfoResp->detail_url = $feedInfoDTO->detail_url;
        $feedBaseInfoResp->feed_category_id = $feedInfoDTO->feed_category_id;
        $feedBaseInfoResp->publish_time_str = TimeHelper::getPersonalTimeString($feedBaseInfoResp->publish_timestamp);
        $feedBaseInfoResp->template_type = $this->calculationTemplateType($feedInfoDTO->template_type, $feedInfoDTO->feed_type, $conversionFeedParams);

        $feedCategoryInfoRet = FeedServiceImpl::getInstance()->getFeedCategory($feedInfoDTO->feed_category_id);
        if (Result::isSuccess($feedCategoryInfoRet)) {
            $feedBaseInfoResp->feed_category_name = $feedCategoryInfoRet->data->category_name;
        }
        /**
         * 收藏状态
         */
//        $collectParams = new UserCollectParams();
//        $collectParams->user_id = $conversionFeedParams->user_id;
//        $collectParams->collect_biz_id = $feedInfoDTO->id;
//        $collectParams->collect_biz_type = \Config_Const::BIZ_TYPE_FEED;
//        $collectRet = CollectServiceImpl::getInstance()->isCollect($collectParams);
//        if (Result::isSuccess($collectRet)) {
//            $feedBaseInfoResp->collect_status = intval($collectRet->data);
//        }
        return $feedBaseInfoResp;
    }

    /**
     * 根据场景,feed类型,template类型计算端上展示的模板
     * @param int|null $templateType
     * @param int|null $feedType
     * @param ConversionFeedParams|null $conversionFeedParams
     * @return int
     */
    private function calculationTemplateType(?int $templateType, ?int $feedType, ?ConversionFeedParams &$conversionFeedParams = null): int
    {

        if ($conversionFeedParams == null) {
            return $templateType;
        }


        return $templateType;

    }


    private function conversionFeedDetail(FeedInfoDTO &$feedInfoDTO, ?ConversionFeedParams $conversionFeedParams = null): ?FeedDetailResp
    {
        $feedDetailResp = new FeedDetailResp();


        /** @var FeedContent $feedContent */
        $feedContent = json_decode($feedInfoDTO->content);

        if ($feedContent == null) {
            return null;
        }


        $feedDetailResp->feed_title = $feedInfoDTO->feed_title;
        $feedDetailResp->image_list = $feedContent->image_list;
        $feedDetailResp->video_link = $feedContent->video_link;
        if (isset($feedContent->content_desc)) {
            $feedDetailResp->feed_content = $feedContent->content_desc;
        }


        return $feedDetailResp;
    }

    private function getFeedDecorationInfo(FeedInfoDTO &$feedInfoDTO, ConversionFeedParams $conversionFeedParams): ?FeedDecorationResp
    {
        $feedDecorationResp = new FeedDecorationResp();
        return $feedDecorationResp;
    }

    private function getFeedInteractiveInfo(FeedInfoDTO &$feedInfoDTO, ConversionFeedParams &$conversionFeedParams): ?FeedInteractiveInfoResp
    {
        if ($feedInfoDTO == null || $feedInfoDTO->id == 0) {
            return null;
        }
        $interactiveService = InteractiveServiceImpl::getInstance();

        $userId = $conversionFeedParams->user_id;
        $interactiveInfoResp = new FeedInteractiveInfoResp();


        $interactiveOverviewRet = $interactiveService->queryInteractiveOverview(\Config_Const::BIZ_TYPE_FEED, $feedInfoDTO->id);
        if (Result::isSuccess($interactiveOverviewRet) == false) {
            return $interactiveInfoResp;
        }
        /** @var InteractiveOverviewDTO $interactiveOverviewDTO */
        $interactiveOverviewDTO = $interactiveOverviewRet->data;
        /**点赞数*/

        $interactiveInfoResp->like_count = $interactiveOverviewDTO->like_count;
        $interactiveInfoResp->comment_count = $interactiveOverviewDTO->comment_count;
        $interactiveInfoResp->share_count = $interactiveOverviewDTO->share_count;


        /**点赞状态*/
        $likeStatusRet = $interactiveService->isLike($userId, $feedInfoDTO->id, \Config_Const::BIZ_TYPE_FEED);
        if (Result::isSuccess($likeStatusRet)) {
            $interactiveInfoResp->like_status = $likeStatusRet->data;
        }


        $interactiveInfoResp->interactive_link = '';

        $queryInteractiveCommentListParams = new QueryInteractiveCommentListParams();
        $queryInteractiveCommentListParams->biz_type = \Config_Const::BIZ_TYPE_FEED;
        $queryInteractiveCommentListParams->biz_id = $feedInfoDTO->id;
        $queryInteractiveCommentListParams->index = 0;
        $queryInteractiveCommentListParams->page_size = 100;
        $commentListRet = $interactiveService->queryInteractiveCommentList($queryInteractiveCommentListParams);

        if (Result::isSuccess($commentListRet) == false) {
            return $interactiveInfoResp;
        }

        $commentDTOList = $commentListRet->data;
        if (empty($commentDTOList)) {
            return $interactiveInfoResp;
        }

        $userService = UserServiceImpl::getInstance();

        $interactiveInfoResp->comment_list = [];
        /** @var InteractiveCommentDTO $item */

        foreach ($commentDTOList as $item) {
            $commentResp = new  CommentResp();
            $commentResp->comment_id = $item->id;
            $commentResp->comment_content = InteractiveConversionUtil::parseContent($item->comment_content);
            $commentResp->comment_user_id = $item->comment_user_id;
            $userInfoRet = $userService->getValidUserInfo($item->comment_user_id);
            if (Result::isSuccess($userInfoRet) == false) {
                continue;
            }

            $commentResp->comment_timestamp = $item->created_timestamp;
            $commentResp->comment_time_str = TimeHelper::getPersonalTimeString($item->created_timestamp);
            $commentResp->comment_user_avatar = $userInfoRet->data->avatar;
            $commentResp->comment_user_name = $userInfoRet->data->nick_name;
            array_push($interactiveInfoResp->comment_list, $commentResp);
        }


        return $interactiveInfoResp;
    }


    private function updatePosterFeedImageHotArea(FeedImageElementResp &$feedImageElementResp)
    {
        $feedImageElementResp->hot_area_list = [];

        $imageHotArea = new HotAreaResp();
        $imageHotArea->start_coords = ["x" => 0.1, "y" => 0.1];
        $imageHotArea->end_coords = ["x" => 0.9, "y" => 0.4];
        $imageHotArea->link = FeedInformationUtil::buildInformationUrl(1);
        array_push($feedImageElementResp->hot_area_list, $imageHotArea);


        $imageHotArea1 = new HotAreaResp();
        $imageHotArea1->start_coords = ["x" => 0.1, "y" => 0.5];
        $imageHotArea1->end_coords = ["x" => 0.9, "y" => 0.9];
        $imageHotArea1->link = FeedInformationUtil::buildInformationUrl(3);
        array_push($feedImageElementResp->hot_area_list, $imageHotArea1);
    }


    private function triggerFeedExposureEvent(int $feedId, ?ConversionFeedParams &$conversionFeedParams)
    {

        $event = null;
        if ($conversionFeedParams->scene == self::SCENE_INDEX ||
            $conversionFeedParams->scene == self::SCENE_STAR_CARD ||
            $conversionFeedParams->scene == self::SCENE_SUPER_PRODUCT_CARD ||
            $conversionFeedParams->scene == self::SCENE_SEARCH) {

            $event = FeedHeatServiceImpl::EVENT_EXPOSURE;
        } elseif ($conversionFeedParams->scene == self::SCENE_DETAIL) {
            $event = FeedHeatServiceImpl::EVENT_CLICK;
        }

        if ($event == null) {
            return;
        }


        $triggerEventParams = new  TriggerEventParams();
        $triggerEventParams->feed_id = $feedId;
        $triggerEventParams->event_type = $event;
        FeedHeatServiceImpl::getInstance()->triggerEvent($triggerEventParams);

    }

    private function buildFeedHashtagList(int $feedId, FeedDecorationResp &$feedDecorationResp)
    {


        $this->buildTheatreInfo($feedId, $feedDecorationResp);
        $feedDecorationResp->hashtag_list = [];
        $this->buildIpHashTag($feedId, $feedDecorationResp);
        $this->buildOutsiteHashTag($feedId, $feedDecorationResp);
        $this->buildProductHashTag($feedId, $feedDecorationResp);

    }

    private function buildTheatreInfo(int $feedId, FeedDecorationResp &$feedDecorationResp)
    {
        $feedDecorationResp->theatre_hashtag_list = [];
        //挑战活动
        $queryTagEntityListParams = new QueryTagEntityListParams();
        $queryTagEntityListParams->tag_biz_id = $feedId;
        $queryTagEntityListParams->tag_biz_type = TagEntityDTO::BIZ_TYPE_FEED;
        $queryTagEntityListParams->entity_biz_type = AppConstant::BIZ_TYPE_CHALLENGE;
        $queryTagEntityListParams->page_size = 1;
        $tagEntityListRet = TagServiceImpl::getInstance()->queryTagEntityList($queryTagEntityListParams);

//        if (!Result::isSuccess($tagEntityListRet)) {
//            return;
//        }
//        $starIdList = $tagEntityListRet->data;
//        if (empty($starIdList)) {
//            return;
//        }
//        $challengeIdList = $tagEntityListRet->data;
//        $challengeId = $challengeIdList[0];
        $challengeId = 1;

        $theatreInfoRet = TheatreChallengeServiceImpl::getInstance()->getChallengeInfo($challengeId);
        if (!Result::isSuccess($theatreInfoRet)) {
            return;
        }
        /** @var TheatreChallengeDTO $theatreChallengeDTO */
        $theatreChallengeDTO = $theatreInfoRet->data;

        $challengeLink = ClientRouteHelper::buildTheatreChallengeLink($challengeId);
        $feedHashTag = new FeedHashtag();
        $feedHashTag->hashtag_name = $theatreChallengeDTO->challenge_name;
        $feedHashTag->hashtag_link = $challengeLink;
        $feedHashTag->hashtag_img = \FUR_Config::get_static_config('theatre')['challenge_hashtag_icon'];
        array_push($feedDecorationResp->theatre_hashtag_list, $feedHashTag);


        $theatreChallengeInfo = new FeedTheatreChallengeResp();
        $theatreChallengeInfo->join_count = $theatreChallengeDTO->join_num;
        $theatreChallengeInfo->challenge_button_name = '立即参与';
        $theatreChallengeInfo->challenge_link = $challengeLink;
        $theatreChallengeInfo->user_list = [];

        //参与挑战活动的用户
        $queryTagEntityListParams = new QueryTagEntityListParams();
        $queryTagEntityListParams->tag_biz_id = $challengeId;
        $queryTagEntityListParams->tag_biz_type = AppConstant::BIZ_TYPE_CHALLENGE;
        $queryTagEntityListParams->entity_biz_type = AppConstant::BIZ_TYPE_USER;
        $queryTagEntityListParams->page_size = 5;
        $tagEntityListRet = TagServiceImpl::getInstance()->queryTagEntityList($queryTagEntityListParams);
        if (Result::isSuccess($tagEntityListRet)) {
            $userIdList = $tagEntityListRet->data;
            foreach ($userIdList as $userId) {
                $userInfoResp = UserInfoConversionUtil::getInstance()->getSimpleUserInfoResp($userId);
                if ($userInfoResp == null) {
                    continue;
                }
                array_push($theatreChallengeInfo->user_list, $userInfoResp);
            }
        }
    }


    private function buildIpHashTag(int $feedId, FeedDecorationResp &$feedDecorationResp)
    {
        //ip的hashtag
        $queryTagEntityListParams = new QueryTagEntityListParams();
        $queryTagEntityListParams->tag_biz_id = $feedId;
        $queryTagEntityListParams->tag_biz_type = TagEntityDTO::BIZ_TYPE_FEED;
        $queryTagEntityListParams->entity_biz_type = TagEntityDTO::BIZ_TYPE_STAR;
        $queryTagEntityListParams->page_size = 3;
        $tagEntityListRet = TagServiceImpl::getInstance()->queryTagEntityList($queryTagEntityListParams);
        if (!Result::isSuccess($tagEntityListRet)) {
            return;
        }
        $starIdList = $tagEntityListRet->data;
        if (empty($starIdList)) {
            return;
        }
        foreach ($starIdList as $starId) {
            $starInfoRet = StarServiceImpl::getInstance()->getStarInfo($starId);
            if (!Result::isSuccess($starInfoRet)) {
                continue;
            }
            $feedHashTag = new FeedHashtag();
            $feedHashTag->hashtag_name = "#" . $starInfoRet->data->star_name . "#";
            $feedHashTag->hashtag_link = ClientRouteHelper::buildStarLink($starInfoRet->data->id, $starInfoRet->data->ip_type);
            $feedHashTag->hashtag_img = \FUR_Config::get_static_config('feed')['icon_hash_tag_ip_image'];
            array_push($feedDecorationResp->hashtag_list, $feedHashTag);

            $feedHomepage = new  FeedHomePage();
            $feedHomepage->link = $feedHashTag->hashtag_link;
            $feedHomepage->content = '来自' . $feedHashTag->hashtag_name;
            $feedDecorationResp->homepage = $feedHomepage;
        }
    }


    private function buildOutsiteHashTag(int $feedId, FeedDecorationResp &$feedDecorationResp)
    {
        //外链的hashtag
        $queryTagEntityListParams = new QueryTagEntityListParams();
        $queryTagEntityListParams->tag_biz_id = $feedId;
        $queryTagEntityListParams->tag_biz_type = TagEntityDTO::BIZ_TYPE_FEED;
        $queryTagEntityListParams->entity_biz_type = AppConstant::BIZ_TYPE_OUTSITE;
        $queryTagEntityListParams->page_size = 3;
        $tagEntityListRet = TagServiceImpl::getInstance()->queryTagEntityList($queryTagEntityListParams);
        if (!Result::isSuccess($tagEntityListRet)) {
            return;
        }
        $outsiteLinkIdList = $tagEntityListRet->data;
        if (empty($outsiteLinkIdList)) {
            return;
        }
        foreach ($outsiteLinkIdList as $linkId) {
            $outsiteLinkRet = FeedOutsiteServiceImpl::getInstance()->getOutsiteInfoById($linkId);
            if (!Result::isSuccess($outsiteLinkRet)) {
                continue;
            }
            $feedHashTag = new FeedHashtag();
            $feedHashTag->hashtag_name = "#站外链接#";
            $feedHashTag->hashtag_link = $outsiteLinkRet->data->link_address;
            $feedHashTag->hashtag_img = \FUR_Config::get_static_config('feed')['icon_hash_tag_link_image'];
            array_push($feedDecorationResp->hashtag_list, $feedHashTag);
        }

    }


    private function buildProductHashTag(int $feedId, FeedDecorationResp &$feedDecorationResp)
    {
        //商品的hashtag
        $queryTagEntityListParams = new QueryTagEntityListParams();
        $queryTagEntityListParams->tag_biz_id = $feedId;
        $queryTagEntityListParams->tag_biz_type = TagEntityDTO::BIZ_TYPE_FEED;
        $queryTagEntityListParams->entity_biz_type = TagEntityDTO::BIZ_TYPE_PRODUCT_SKU;
        $queryTagEntityListParams->page_size = 3;
        $tagEntityListRet = TagServiceImpl::getInstance()->queryTagEntityList($queryTagEntityListParams);
        if (!Result::isSuccess($tagEntityListRet)) {
            return;
        }
        $skuIdList = $tagEntityListRet->data;
        if (empty($skuIdList)) {
            return;
        }
        foreach ($skuIdList as $skuId) {
            $productSkuDetailParams = new GetProductDetailParams ();
            $productSkuDetailParams->sku_id = $skuId;
            $skuInfoRet = ProductServiceImpl::getInstance()->getProductDetail($productSkuDetailParams);
            if (!Result::isSuccess($skuInfoRet)) {
                continue;
            }
            $feedHashTag = new FeedHashtag();
            $feedHashTag->hashtag_name = "#" . $skuInfoRet->data->product_name . "#";
            $feedHashTag->hashtag_link = ClientRouteHelper::buildPdpLink($skuInfoRet->data->id);
            $feedHashTag->hashtag_img = \FUR_Config::get_static_config('feed')['icon_hash_tag_image'];
            array_push($feedDecorationResp->hashtag_list, $feedHashTag);

            $feedHomepage = new  FeedHomePage();
            $feedHomepage->link = $feedHashTag->hashtag_link;
            $feedHomepage->content = '来自' . $feedHashTag->hashtag_name;
            $feedDecorationResp->homepage = $feedHomepage;
        }
    }
}