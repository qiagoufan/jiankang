<?php

namespace service\theatre\impl;

use facade\request\theatre\calendar\CreateCalendarParams;
use facade\response\Result;
use facade\service\theatre\TheatreCalendarService;
use model\theatre\dto\TheatreCalendarDTO;
use model\theatre\TheatreCalendarModel;

class TheatreCalendarServiceImpl implements TheatreCalendarService
{

    public const PAGE_SIZE = 20;
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function queryEventListByYearMonth($yearMonth): ?Result
    {
        $result = new Result();
        $startDate = $yearMonth . "00";
        $endDate = $yearMonth . "99";
        $eventList = TheatreCalendarModel::getInstance()->queryCalendarListByDate($startDate, $endDate);
        $result->setSuccessWithResult($eventList);
        return $result;
    }

    public function queryEventCount(int $date): ?Result
    {
        // TODO: Implement queryEventCount() method.
    }

    public function insertEvent(CreateCalendarParams $createCalendarParams): ?Result
    {
        $result = new Result();

        $theatreCalendarDTO = new TheatreCalendarDTO();
        \FUR_Core::copyProperties($createCalendarParams, $theatreCalendarDTO);
        $theatreCalendarDTO->created_timestamp = time();
        if ($theatreCalendarDTO->event_date == null) {
            $theatreCalendarDTO->event_date = date('Ymd');
        }
        if ($theatreCalendarDTO->event_timestamp == null) {
            $theatreCalendarDTO->event_timestamp = time();
        }
        $calendarId = TheatreCalendarModel::getInstance()->insertRecord($theatreCalendarDTO);
        $result->setSuccessWithResult($calendarId);
        return $result;
    }
}