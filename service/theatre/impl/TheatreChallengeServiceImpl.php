<?php

namespace service\theatre\impl;

use facade\response\Result;
use facade\service\theatre\TheatreChallengeService;
use model\dto\FeedInfoDTO;
use model\theatre\dto\TheatreChallengeFeedDTO;
use model\theatre\TheatreChallengeModel;

class TheatreChallengeServiceImpl implements TheatreChallengeService
{

    public const PAGE_SIZE = 10;
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function queryValidChallengeList($timestamp): ?Result
    {
        $result = new Result();
        $challengeList = TheatreChallengeModel::getInstance()->queryValidChallengeList($timestamp);
        $result->setSuccessWithResult($challengeList);
        return $result;
    }

    public function getChallengeInfo(int $challengeId): ?Result
    {
        $result = new Result();
        $theatreChallengeDTO = TheatreChallengeModel::getInstance()->queryChallengeInfo($challengeId);
        $result->setSuccessWithResult($theatreChallengeDTO);
        return $result;
    }

    public function queryChallengePrizeList(int $challengeId): ?Result
    {
        $result = new Result();
        $prizeList = TheatreChallengeModel::getInstance()->queryChallengePrizeList($challengeId);
        $result->setSuccessWithResult($prizeList);
        return $result;
    }

    public function queryChallengeTopFeedList(int $challengeId): ?Result
    {
        $result = new Result();
        $feedList = TheatreChallengeModel::getInstance()->queryChallengeTopFeedList($challengeId);
        $result->setSuccessWithResult($feedList);
        return $result;
    }

    public function queryChallengeFeedList(int $challengeId, int $index, int $sortType = 1): ?Result
    {
        $result = new Result();
        $pageSize = self::PAGE_SIZE;
        $feedList = TheatreChallengeModel::getInstance()->queryChallengeFeedList($challengeId, $index, $pageSize, $sortType);
        $result->setSuccessWithResult($feedList);
        return $result;
    }

    public function queryChallengeAuthorFeedList(int $challengeId): ?Result
    {
        $result = new Result();
        $pageSize = self::PAGE_SIZE;
        $feedList = TheatreChallengeModel::getInstance()->queryChallengeAuthorFeedList($challengeId, $pageSize);
        $result->setSuccessWithResult($feedList);
        return $result;
    }

    public function updateChallengeFeedLikeCount(int $feedId, int $likeNum): ?Result
    {
        $result = new Result();
        $challengeFeedInfo = TheatreChallengeModel::getInstance()->queryChallengeFeedInfo($feedId);
        if ($challengeFeedInfo == null) {
            $result->setSuccessWithResult(false);
            return $result;
        }
        TheatreChallengeModel::getInstance()->updateChallengeFeedLikeCount($feedId, $challengeFeedInfo->like_count + $likeNum);
        $result->setSuccessWithResult(true);
        return $result;
    }

    public function addChallengeFeed(int $challengeId, FeedInfoDTO $feedInfoDTO): ?Result
    {
        $result = new Result();
        $challengeInfo = TheatreChallengeModel::getInstance()->queryChallengeInfo($challengeId);
        if ($challengeInfo == null) {
            $result->setError(\Config_Error::ERR_CHALLENGE_IS_NOT_EXIST);
            return $result;
        }
        $now = time();
        if ($now < $challengeInfo->challenge_start_timestamp) {
            $result->setError(\Config_Error::ERR_CHALLENGE_IS_NOT_START);
            return $result;
        }
        if ($now > $challengeInfo->challenge_end_timestamp) {
            $result->setError(\Config_Error::ERR_CHALLENGE_IS_FINISH);
            return $result;
        }
        if ($feedInfoDTO->template_type != FeedInfoDTO::TEMPLATE_TYPE_STANDARD_VIDEO_3) {
            $result->setError(\Config_Error::ERR_CHALLENGE_FEED_ONLY_VIDEO);
            return $result;
        }


        $challengeFeedDTO = new TheatreChallengeFeedDTO();
        $challengeFeedDTO->like_count = 0;
        $challengeFeedDTO->created_timestamp = time();
        $challengeFeedDTO->feed_id = $feedInfoDTO->id;
        $challengeFeedDTO->author_id = $feedInfoDTO->author_id;
        $challengeFeedDTO->is_top = 0;
        $challengeFeedDTO->challenge_id = $challengeId;

        TheatreChallengeModel::getInstance()->insertChallengeFeed($challengeFeedDTO);
        $result->setSuccessWithResult(true);
        return $result;
    }
}