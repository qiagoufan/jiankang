<?php

namespace service\theatre\impl;

use facade\request\theatre\CreateTheatreDailyShowParams;
use facade\response\Result;
use facade\service\theatre\TheatreService;
use model\theatre\dto\TheatreDailyShowDTO;
use model\theatre\TheatreModel;

class TheatreServiceImpl implements TheatreService
{

    public const PAGE_SIZE = 20;
    public static $_instance;

    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }

    public function getShowInfoById(int $id): ?Result
    {
        $result = new Result();
        /** @var theatreDailyShowDTO $showInfoDTO */
        $showInfoDTO = TheatreModel::getInstance()->queryShowInfoById($id);
        $result->setSuccessWithResult($showInfoDTO);
        return $result;
    }

    public function getShowListByDate(string $date): ?Result
    {
        $result = new Result();
        $showList = TheatreModel::getInstance()->queryValidShowListByDate($date);
        $result->setSuccessWithResult($showList);
        return $result;
    }

    public function createTheatreDailyShow(CreateTheatreDailyShowParams $createTheatreDailyShowParams): ?Result
    {
        $result = new Result();
        $theatreDailyShowDTO = new TheatreDailyShowDTO();
        \FUR_Core::copyProperties($createTheatreDailyShowParams, $theatreDailyShowDTO);

        $theatreDailyShowDTO->show_status = TheatreDailyShowDTO::STATUS_NORMAL;
        $theatreDailyShowDTO->created_timestamp = time();

        $id = TheatreModel::getInstance()->insertRecord($theatreDailyShowDTO);
        $result->setSuccessWithResult($id);
        return $result;
    }

    public function getShowListByIndex(int $index, int $pageSize): ?Result
    {
        $result = new Result();
        $start = $index * self::PAGE_SIZE;

        $list = TheatreModel::getInstance()->queryShowList($start, $pageSize);
        $result->setSuccessWithResult($list);
        return $result;
    }

    public function getShowCount(): ?Result
    {
        $result = new Result();
        $count = TheatreModel::getInstance()->queryShowCount();
        $result->setSuccessWithResult($count);
        return $result;
    }

    public function deleteShow(int $id): ?Result
    {
        $result = new Result();
        $count = TheatreModel::getInstance()->setShowDelete($id);
        $result->setSuccessWithResult($count);
        return $result;
    }
}