<?php

namespace service\invoice\impl;


use facade\request\invoice\ApplyForInvoiceParams;
use facade\request\invoice\QueryInvoiceListParams;
use facade\request\repair\QueryRepairOrderListParams;
use facade\response\Result;
use facade\service\invoice\InvoiceService;
use lib\TimeHelper;
use model\invoice\dto\InvoiceInfoDTO;
use model\invoice\dto\InvoiceOrderListDTO;
use model\invoice\InvoiceModel;
use model\repair\dto\RepairOrderInfoDTO;
use model\repair\RepairOrderModel;
use service\repair\impl\RepairOrderServiceImpl;


class InvoiceServiceImpl implements InvoiceService
{

    public static $_instance;


    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function applyForInvoice(ApplyForInvoiceParams $applyForInvoiceParams): ?Result
    {
        $result = new Result();
        $requestRepairOrderIdList = $applyForInvoiceParams->repair_order_id_list;

        $queryRepairOrderListParams = new  QueryRepairOrderListParams ();
        $queryRepairOrderListParams->company_id = $applyForInvoiceParams->company_id;
        $queryRepairOrderListParams->invoicing_status = \Config_Const::INVOICING_STATUS_NONE;
        $queryRepairOrderListParams->page_size = 100;
        $queryRepairOrderListParams->order_status_list = [\Config_Const::REPAIR_ORDER_CONFIRM_PRICE, \Config_Const::REPAIR_ORDER_COMPLETE];
        if ($requestRepairOrderIdList != null) {
            $queryRepairOrderListParams->repair_order_id_list = $requestRepairOrderIdList;
        }
        $repairOrderListRet = RepairOrderServiceImpl::getInstance()->queryRepairOrderList($queryRepairOrderListParams);

        if (Result::isSuccess($repairOrderListRet) == false) {
            return $repairOrderListRet;
        }
        $repairOrderIdList = [];
        $repairOrderInfoList = [];
        $totalAmount = 0;
        /** @var RepairOrderInfoDTO $repairOrderInfo */
        foreach ($repairOrderListRet->data as $repairOrderInfo) {
            $totalAmount = $totalAmount + $repairOrderInfo->customer_price;
            array_push($repairOrderIdList, $repairOrderInfo->id);
            array_push($repairOrderInfoList, $repairOrderInfo);
        }
        if (!empty($requestRepairOrderIdList)) {
            if (count($requestRepairOrderIdList) != count($repairOrderIdList)) {
                $result->setError(\Config_Error::ERR_REPAIR_INVOICE_APPLY_ERROR);
                return $result;
            }
        }
        if (empty($repairOrderIdList)) {
            $result->setError(\Config_Error::ERR_REPAIR_INVOICE_APPLY_NO_ORDER);
            return $result;
        }
        $invoiceInfoDTO = new InvoiceInfoDTO();
        $invoiceInfoDTO->company_id = $applyForInvoiceParams->company_id;
        $invoiceInfoDTO->total_amount = $totalAmount;
        $invoiceInfoDTO->total_count = count($repairOrderIdList);
        $invoiceInfoDTO->invoicing_status = \Config_Const::INVOICING_STATUS_APPLY;
        $invoiceInfoDTO->created_timestamp = TimeHelper::getTimeMs();
        $invoiceId = InvoiceModel::getInstance()->createInvoiceInfo($invoiceInfoDTO);
        //插入记录
        foreach ($repairOrderIdList as $repairOrderId) {
            $invoiceOrderDTO = new InvoiceOrderListDTO();
            $invoiceOrderDTO->created_timestamp = TimeHelper::getTimeMs();
            $invoiceOrderDTO->repair_order_id = $repairOrderId;
            $invoiceOrderDTO->invoice_id = $invoiceId;
            InvoiceModel::getInstance()->createInvoiceOrder($invoiceOrderDTO);
        }
        //更新订单开票状态
        /** @var RepairOrderInfoDTO $repairOrderInfo */
        foreach ($repairOrderInfoList as $repairOrderInfo) {
            $repairOrderInfo->invoicing_status = \Config_Const::INVOICING_STATUS_APPLY;
            $repairOrderInfo->updated_timestamp = TimeHelper::getTimeMs();
            RepairOrderServiceImpl::getInstance()->updateRepairOrder($repairOrderInfo);
        }
        $result->setSuccessWithResult($invoiceId);
        return $result;

    }

    public function queryInvoiceList(QueryInvoiceListParams $queryInvoiceListParams): ?Result
    {

        $result = new Result();
        $invoiceList = InvoiceModel::getInstance()->queryInvoiceList($queryInvoiceListParams);
        $result->setSuccessWithResult($invoiceList);
        return $result;
    }

    public function queryInvoiceInfo(int $invoiceId): ?Result
    {

        $result = new Result();
        $invoiceInfo = InvoiceModel::getInstance()->queryInvoiceInfo($invoiceId);
        $result->setSuccessWithResult($invoiceInfo);
        return $result;
    }

    public function finishInvoice(int $invoiceId): ?Result
    {
        $result = new Result();
        $invoiceInfo = InvoiceModel::getInstance()->queryInvoiceInfo($invoiceId);
        $invoiceInfo->invoicing_status = \Config_Const::INVOICING_STATUS_DONE;
        $rows = InvoiceModel::getInstance()->updateInvoiceInfo($invoiceInfo);


        $invoiceOrderList = InvoiceModel::getInstance()->queryInvoiceOrderList($invoiceId);
        /** @var InvoiceOrderListDTO $invoiceOrder */
        foreach ($invoiceOrderList as $invoiceOrder) {
            $repairOrderDTO = new  RepairOrderInfoDTO();
            $repairOrderDTO->id = $invoiceOrder->repair_order_id;
            $repairOrderDTO->invoicing_status = \Config_Const::INVOICING_STATUS_DONE;
            $repairOrderDTO->updated_timestamp = TimeHelper::getTimeMs();
            RepairOrderModel::getInstance()->updateRepairInfo($repairOrderDTO);
        }
        $result->setSuccessWithResult($rows);
        return $result;
    }

    public function queryInvoiceOrderList(int $invoiceId): ?Result
    {
        $result = new Result();
        $data = InvoiceModel::getInstance()->queryInvoiceOrderList($invoiceId);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function queryInvoiceCount(QueryInvoiceListParams $queryInvoiceListParams): ?Result
    {
        $result = new Result();
        $count = InvoiceModel::getInstance()->queryInvoiceCount($queryInvoiceListParams);
        $result->setSuccessWithResult($count);
        return $result;
    }
}