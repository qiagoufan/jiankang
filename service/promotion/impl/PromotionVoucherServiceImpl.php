<?php

namespace service\promotion\impl;


use facade\request\PageParams;
use facade\request\promotion\EditVoucherParams;
use facade\response\Result;
use facade\service\promotion\PromotionVoucherService;
use FUR_Core;
use model\promotion\dto\PromotionVoucherEntityDTO;
use model\promotion\dto\PromotionVoucherDTO;
use model\promotion\VoucherEntityModel;
use model\promotion\VoucherModel;
use service\promotion\data\VoucherDetail;


class PromotionVoucherServiceImpl implements PromotionVoucherService
{

    public static $_instance;


    public static function getInstance()
    {
        if (!self::$_instance) {
            self::$_instance = new self ();
        }
        return self::$_instance;
    }


    public function useVoucher(string $voucherSn, int $orderId): ?Result
    {
        $result = new Result();

        $voucherEntityDTO = VoucherEntityModel::getInstance()->queryVoucherEntityBySn($voucherSn);
        if ($voucherEntityDTO == null) {
            $result->setError(\Config_Error::ERR_VOUCHER_SN_NOT_FOUND);
            return $result;
        }
        if ($voucherEntityDTO->use_status != 0) {
            $result->setError(\Config_Error::ERR_VOUCHER_SN_CANT_USE);
            return $result;
        }
        $voucherEntityDTO->use_status = 1;
        $voucherEntityDTO->order_id = $orderId;
        $voucherEntityDTO->use_timestamp = time();
        $voucherEntityDTO->updated_timestamp = $voucherEntityDTO->use_timestamp;
        VoucherEntityModel::getInstance()->updateVoucherEntity($voucherEntityDTO);


        $rows = VoucherModel::getInstance()->updateVoucherUsedCount($voucherEntityDTO->voucher_id, 1);
        $result->setSuccessWithResult(true);
        return $result;
    }


    public function returnVoucher(string $voucherSn, int $orderId): ?Result
    {
        $result = new Result();
        $voucherEntityDTO = VoucherEntityModel::getInstance()->queryVoucherEntityBySn($voucherSn);
        if ($voucherEntityDTO == null) {
            return $result;
        }
        if ($voucherEntityDTO->use_status != 1) {
            return $result;
        }
        $voucherEntityDTO->use_status = 0;
        $voucherEntityDTO->order_id = $orderId;
        $voucherEntityDTO->use_timestamp = 0;
        $voucherEntityDTO->updated_timestamp = time();
        VoucherEntityModel::getInstance()->updateVoucherEntity($voucherEntityDTO);

        VoucherModel::getInstance()->updateVoucherUsedCount($voucherEntityDTO->voucher_id, -1);
        $result->setSuccessWithResult(true);
        return $result;
    }


    public function getVoucherInfo(int $voucherId): ?Result
    {
        $result = new Result();
        $voucherInfoDTO = VoucherModel::getInstance()->getVoucherInfo($voucherId);
        $result->setSuccessWithResult($voucherInfoDTO);
        return $result;
    }

    public function queryEnableVoucherList(): ?Result
    {
        $result = new Result();
        $data = VoucherModel::getInstance()->queryEnableVoucherList();
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function getVoucherEntityBySn(string $voucherSn): ?Result
    {
        $result = new Result();
        $voucherEntity = VoucherEntityModel::getInstance()->queryVoucherEntityBySn($voucherSn);
        $result->setSuccessWithResult($voucherEntity);
        return $result;
    }


    public function queryUserVoucherList(?int $userId = 0): ?Result
    {
        $result = new Result();
        $voucherEntityList = VoucherEntityModel::getInstance()->queryVoucherListByUserId($userId);
        $result->setSuccessWithResult($voucherEntityList);
        return $result;
    }

    public function distributeVoucher(int $userId, int $voucherId): ?Result
    {
        $result = new Result();
        $now = time();
        $voucherEntityRecord = VoucherEntityModel::getInstance()->queryUserVoucherEntityByVoucherId($userId, $voucherId);
        if ($voucherEntityRecord && $voucherEntityRecord->expire_timestamp > $now) {
            $result->setError(\Config_Error::ERR_VOUCHER_IS_RECEIVED);
            return $result;
        }
        $voucherInfo = VoucherModel::getInstance()->getVoucherInfo($voucherId);
        if ($voucherInfo == null) {
            $result->setError(\Config_Error::ERR_VOUCHER_SN_NOT_FOUND);
            return $result;
        }
        $now = time();
        if ($voucherInfo->end_timestamp != 0 && $voucherInfo->end_timestamp < $now) {
            $result->setError(\Config_Error::ERR_VOUCHER_IS_EXPIRE);
            return $result;
        }
        if ($voucherInfo->voucher_status != \Config_Const::VOUCHER_STATUS_USEFUL) {
            $result->setError(\Config_Error::ERR_VOUCHER_CANT_USE);
            return $result;
        }
        $rows = VoucherModel::getInstance()->updateVoucherCount($voucherId, 1);

        if ($rows == 0) {
            $result->setError(\Config_Error::ERR_VOUCHER_IS_NOT_ENOUGH);
            return $result;
        }
        VoucherModel::getInstance()->updateVoucherDispatchCount($voucherId, 1);
        $voucherEntityStartTimestamp = $now;
        $voucherEntityExpireTimestamp = $voucherInfo->end_timestamp;
        if ($voucherInfo->start_timestamp > $now) {
            $voucherEntityStartTimestamp = $voucherInfo->start_timestamp;
        }
        //设置voucher过期时间
        if ($voucherInfo->expire_timelong != 0) {
            $voucherEntityExpireTimestamp = $voucherEntityStartTimestamp + $voucherInfo->expire_timelong;
        }
        //设置voucher过期时间比设定的要长,以voucher为准
        if ($voucherInfo->end_timestamp != 0 && $voucherEntityExpireTimestamp > $voucherInfo->end_timestamp) {
            $voucherEntityExpireTimestamp = $voucherInfo->end_timestamp;
        }

        $voucherEntity = new PromotionVoucherEntityDTO();
        $voucherEntity->user_id = $userId;
        $voucherEntity->voucher_id = $voucherId;
        $voucherEntity->created_timestamp = $now;
        $voucherEntity->use_status = 0;
        $voucherEntity->voucher_sn = md5(uniqid());
        $voucherEntity->start_timestamp = $voucherEntityStartTimestamp;
        $voucherEntity->received_timestamp = $now;
        $voucherEntity->expire_timestamp = $voucherEntityExpireTimestamp;
        $id = VoucherEntityModel::getInstance()->insertVoucherEntity($voucherEntity);
        $result->setSuccessWithResult($id);
        return $result;
    }

    public function queryUserVoucherEntityByVoucherId(int $userId, int $voucherId): ?Result
    {
        $result = new Result();
        $voucherEntityList = VoucherEntityModel::getInstance()->queryUserVoucherEntityByVoucherId($userId, $voucherId);
        $result->setSuccessWithResult($voucherEntityList);
        return $result;
    }


    public function queryVoucherList(PageParams $params): ?Result
    {
        $result = new Result();
        $data = VoucherModel::getInstance()->queryVoucherList($params);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function queryVoucherCount(PageParams $params): ?Result
    {
        $result = new Result();
        $data = VoucherModel::getInstance()->queryVoucherCount($params);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function createVoucher(EditVoucherParams $params): ?Result
    {
        $result = new Result();
        $voucherDTO = new PromotionVoucherDTO();
        FUR_Core::copyProperties($params, $voucherDTO);
        $voucherDTO->created_timestamp = time();
        $voucherDTO->voucher_type = \Config_Const::VOUCHER_TYPE_DISCOUNT;
        $voucherDTO->voucher_status = \Config_Const::VOUCHER_STATUS_USEFUL;
        $voucherDTO->voucher_used_count = 0;
        $voucherDTO->voucher_distribute_count = 0;
        $voucherDetail = new VoucherDetail();
        $voucherDetail->amount = $params->amount * 100;
        $voucherDetail->price_limit = $params->price_limit * 100;
        $voucherDTO->voucher_detail = json_encode($voucherDetail);

        $data = VoucherModel::getInstance()->createdVoucher($voucherDTO);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function createsShareVoucher($voucherId): ?Result
    {
        $result = new Result();
        $voucherDTO = new PromotionVoucherDTO();
        $voucherDTO->id = $voucherId;
        $voucherDTO->created_timestamp = time();
        $voucherDTO->start_timestamp = 0;
        $voucherDTO->voucher_count = 10000000;
        $voucherDTO->expire_timelong = 365 * 86400;
        $voucherDTO->voucher_name = '新人分享奖励券';
        $voucherDTO->voucher_type = \Config_Const::VOUCHER_TYPE_DISCOUNT;
        $voucherDTO->voucher_status = \Config_Const::VOUCHER_STATUS_USEFUL;
        $voucherDTO->voucher_used_count = 0;
        $voucherDetail = new VoucherDetail();
        $voucherDetail->amount = 1 * 100;
        $voucherDetail->price_limit = 0 * 100;
        $voucherDTO->voucher_detail = json_encode($voucherDetail);

        $data = VoucherModel::getInstance()->createdVoucher($voucherDTO);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function updateVoucher(EditVoucherParams $params): ?Result
    {
        $result = new Result();
        $voucherDTO = VoucherModel::getInstance()->getVoucherInfo($params->voucher_id);
        FUR_Core::copyProperties($params, $voucherDTO);
        $voucherDTO->updated_timestamp = time();

        $voucherDetail = new VoucherDetail();
        $voucherDetail->amount = $params->amount * 100;
        $voucherDetail->price_limit = $params->price_limit * 100;
        $voucherDTO->voucher_detail = json_encode($voucherDetail);

        $data = VoucherModel::getInstance()->updateVoucher($voucherDTO);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function deleteVoucher(int $voucherId): ?Result
    {
        $result = new Result();
        $data = VoucherModel::getInstance()->deleteVoucher($voucherId);
        $result->setSuccessWithResult($data);
        return $result;
    }

    public function updateVoucherStatus($voucherId, $voucherStatus): ?Result
    {
        $result = new Result();
        $data = VoucherModel::getInstance()->updateVoucherStatus($voucherId, $voucherStatus);
        $result->setSuccessWithResult($data);
        return $result;
    }
}