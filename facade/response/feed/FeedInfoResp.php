<?php


namespace facade\response\feed;

use facade\response\feed\detail\FeedAuthorInfoResp;
use facade\response\feed\detail\FeedBaseInfoResp;
use facade\response\feed\detail\FeedDecorationResp;
use facade\response\feed\detail\FeedInteractiveInfoResp;

class FeedInfoResp
{
    public $feed_detail = null;
    public $feed_base_info = null;
    public $author_info = null;
    public $interactive_info = null;
    public $decoration_info = null;

}