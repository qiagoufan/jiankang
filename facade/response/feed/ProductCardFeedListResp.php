<?php


namespace facade\response\feed;

use facade\response\base\PageInfoResp;
use facade\response\feed\detail\FeedAuthorInfoResp;
use facade\response\feed\detail\FeedBaseInfoResp;
use facade\response\feed\detail\FeedDetailResp;
use facade\response\feed\detail\FeedInteractiveInfoResp;
use facade\response\product\ProductSuperDetailResp;
use facade\response\user\StarInfoResp;

class ProductCardFeedListResp
{
    public ?ProductSuperDetailResp $product_info_resp = null;
    public array $forum_news_list = [];
    public ?ForumOverviewResp $forum_overview = null;
    public array $forum_feed_list = [];
    public array $product_tag_info = [];
    public ?PageInfoResp $page_info;

}