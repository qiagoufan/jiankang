<?php


namespace facade\response\feed\info;

use facade\response\feed\detail\element\FeedProductElementResp;

class FeedInformationResp
{
    public $id;
    public $content;
    public $main_image;
    public $title;
    public $sub_title;

    public $is_original;
    public $outsite_link;
    public $outsite_author;
    public $outsite_source;
    public $created_timestamp;
    public $created_time_str;
    public $updated_timestamp;

    public $author_id;
    public $status;
    public $author_name;

    public ?FeedProductElementResp $bind_product = null;

}