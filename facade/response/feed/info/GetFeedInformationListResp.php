<?php


namespace facade\response\feed\info;


use facade\response\base\PageInfoResp;

class GetFeedInformationListResp
{

    public array $info_list = [];
    public PageInfoResp $page_info;

}