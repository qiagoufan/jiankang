<?php


namespace facade\response\feed;


class FeedFlowerDetailResp
{
    public ?array $image_list = null;
    public ?string $description = null;
    public ?string $title = null;
    public ?array $content_tag_list = null;
}