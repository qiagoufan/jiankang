<?php


namespace facade\response\feed\group;


use facade\response\base\PageInfoResp;

class GroupManagerApplyFormListResp
{

    public ?array $apply_form_list;
    public ?PageInfoResp $page_info;
}