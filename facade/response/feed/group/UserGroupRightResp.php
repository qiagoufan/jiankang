<?php


namespace facade\response\feed\group;


class UserGroupRightResp
{
    /**
     * @var int|null 是否可以删除圈子的feed
     */
    public ?int $right_enable_delete_feed = 0;
    /**
     * @var int|null 是否可以移除管理员
     */
    public ?int $right_enable_remove_manager = 0;
    /**
     * @var int|null 是否可以审核管理员申请
     */
    public ?int $right_enable_approve_manager = 0;

}