<?php


namespace facade\response\feed\group;


use facade\response\user\group\GroupUserInfoResp;

class FeedGroupInfoResp
{
    public ?string $group_name;
    public ?string $star_name;
    public ?GroupUserInfoResp $group_owner = null;
    public ?array $group_manager_list = [];
    public ?array $apply_form_list = [];
    public ?array $top_fans_list= [];
}