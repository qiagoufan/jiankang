<?php


namespace facade\response\feed\group;


use facade\response\user\group\GroupUserInfoResp;

class GroupManagerApplyFormResp
{

    public $id = 0;
    public ?GroupUserInfoResp $group_user_info = null;
    public $apply_desc;
    public $contact;
    public $form_status;
    public $star_name;
    public $created_timestamp;
    public $star_info = null;
}