<?php


namespace facade\response\feed\group;


use facade\response\user\group\GroupUserInfoResp;

class ApplyForm4UserRecordResp
{

    public $id = 0;
    public $created_time;
    public $updated_time;
    public $apply_desc;
    public $contact;
    public $group_name;
    public $form_status;
    public $form_type;
}