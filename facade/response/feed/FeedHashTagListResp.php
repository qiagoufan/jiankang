<?php

namespace facade\response\feed;
class FeedHashTagListResp
{

    public $page_info;
    public array $hashtag_list = [];

}