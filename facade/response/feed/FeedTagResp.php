<?php

namespace facade\response\feed;
class FeedTagResp
{
    public $hashtag_id;
    public $hashtag_name;
    public $hashtag_avatar;
    public $hashtag_interactive;
    public $hashtag_img;
    public $hashtag_link;
    public $hashtag_type;
}