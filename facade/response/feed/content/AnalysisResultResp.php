<?php


namespace facade\response\feed\content;


class AnalysisResultResp
{

    public string $content;
    public string $content_type;
    public ?array $images;
    public ?array $hash_tag_list = null;
}