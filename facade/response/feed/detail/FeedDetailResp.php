<?php


namespace facade\response\feed\detail;


class FeedDetailResp
{
    public $image_list = null;
    public $video_link = null;
    public $feed_content = null;
    public $feed_title = null;
}