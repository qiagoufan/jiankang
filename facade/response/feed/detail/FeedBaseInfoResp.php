<?php


namespace facade\response\feed\detail;


class FeedBaseInfoResp
{
    public $feed_id;
    public $collect_status = 0;
    public $feed_status = 0;
    public $feed_status_name = null;
    public $feed_type;
    public $template_type;
    public $detail_url = null;
    public $publish_time_str;
    public $publish_timestamp;
    public $feed_category_id;
    public $feed_category_name;

}