<?php

namespace facade\response\feed\detail\ad;

class FeedAdProductResp
{
    public string $product_image = '';
    public string $product_price = '';
    public string $product_name = '';
    public string $product_link = '';
    public string $product_hashtag_icon = '';
    public string $product_hashtag_name = '';

}