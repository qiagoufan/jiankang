<?php


namespace facade\response\feed\detail;


class FeedInteractiveInfoResp
{
    public $comment_list=[];
    public $interactive_link = '';
    public $heat = 0;
    public $like_status = 0;
    public $like_count = 0;
    public $share_count = 0;
    public $comment_count = 0;
}