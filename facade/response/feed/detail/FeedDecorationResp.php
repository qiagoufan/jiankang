<?php


namespace facade\response\feed\detail;


use facade\response\feed\detail\decoration\FeedHeader;
use facade\response\feed\detail\decoration\FeedHomePage;

/**
 * feed装饰
 * Class FeedDecorationResp
 * @package facade\response\feed\detail
 */
class FeedDecorationResp
{
    public $homepage = null;
    public $hashtag_list = null;
    public $feed_header = null;
}