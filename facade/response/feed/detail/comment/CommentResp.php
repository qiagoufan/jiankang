<?php

namespace facade\response\feed\detail\comment;

class CommentResp
{
    public $comment_id;
    public $comment_user_id;
    public $comment_user_name;
    public $comment_user_avatar;
    public $comment_content;
    public $comment_image_list;
    public $comment_time_str;
    public $comment_timestamp;
}