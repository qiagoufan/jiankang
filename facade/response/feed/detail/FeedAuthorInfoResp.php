<?php


namespace facade\response\feed\detail;


class FeedAuthorInfoResp
{
    public  $user_id;
    public  $user_type = 1;
    public  $real_name = null;
    public  $nick_name = null;
    public  $avatar = null;
    public  $user_medal_list = [];

}