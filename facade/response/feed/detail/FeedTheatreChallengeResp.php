<?php


namespace facade\response\feed\detail;


class FeedTheatreChallengeResp
{
    public ?array $user_list = null;
    public int $join_count = 0;
    public ?string $description = null;
    public ?string $challenge_link = null;
    public ?string $challenge_button_name = null;
}