<?php

namespace facade\response\feed\detail\tag;

class FeedContentTag
{
    public string $icon = '';
    public int $width = 136;
    public int $height = 84;

}