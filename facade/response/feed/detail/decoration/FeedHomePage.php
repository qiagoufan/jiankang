<?php

namespace facade\response\feed\detail\decoration;

class FeedHomePage
{
    public string $content = '';
    public string $link = '';
}