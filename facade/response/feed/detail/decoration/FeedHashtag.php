<?php

namespace facade\response\feed\detail\decoration;

class FeedHashtag
{
    public string $hashtag_name = '';
    public string $hashtag_img = '';
    public string $hashtag_link = '';

}