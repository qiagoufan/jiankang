<?php

namespace facade\response\feed\detail\decoration;

class FeedHeader
{
    const  HEADER_TYPE_STAR_ENERGY = 'star_energy_rank';
    const  HEADER_TYPE_POPULARITY = 'popularity_rank';


    public ?string $header_icon = '';
    public ?string $header_name = '';
    public ?string $header_link_url = '';
    public ?string $header_type = '';
    public ?string $header_link_name = '';

}