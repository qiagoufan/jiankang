<?php


namespace facade\response\feed\detail;


use facade\response\feed\detail\ad\FeedAdProductResp;

class FeedAdvertisingResp
{
    const AD_TYPE_PRODUCT = 1;


    public ?int $ad_type = 1;
    public ?FeedAdProductResp $ad_product = null;
}