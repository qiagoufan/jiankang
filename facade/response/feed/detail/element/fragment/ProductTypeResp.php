<?php


namespace facade\response\feed\detail\element\fragment;


class ProductTypeResp
{
    public $product_type_name;
    public $product_type_icon;
}