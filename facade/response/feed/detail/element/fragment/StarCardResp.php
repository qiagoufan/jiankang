<?php


namespace facade\response\feed\detail\element\fragment;


class StarCardResp
{
    public $user_id;
    public $star_name;
    public $background;
    public $follow_status;
    public $subtitle_image;
    public $subtitle_content;
    public $font_color;
    public $star_link;
}