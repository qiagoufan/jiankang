<?php


namespace facade\response\feed\detail\element\fragment;


class InformationResp
{
    public $content;
    public $link;
    public $author_name;
    public $publish_time_str;
    public $icon;
}