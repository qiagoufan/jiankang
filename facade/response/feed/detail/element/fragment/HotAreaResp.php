<?php


namespace facade\response\feed\detail\element\fragment;


class HotAreaResp
{
    //左上角坐标[x=>12.11,y=>42.11]
    public $start_coords = null;
    public $end_coords = null;
    public ?string $link = null;
}