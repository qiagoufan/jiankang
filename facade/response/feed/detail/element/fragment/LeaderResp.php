<?php


namespace facade\response\feed\detail\element\fragment;


class LeaderResp
{
    public $user_id;
    public $nick_name;
    public $avatar;
    public $score_str;
}