<?php


namespace facade\response\feed\detail\element;


class FeedBaseElementResp
{

    const ELEMENT_TYPE_PRODUCT = 'product';
    const ELEMENT_TYPE_VIDEO = 'video';
    const ELEMENT_TYPE_REVIEW = 'review';
    const ELEMENT_TYPE_SUBFEED = 'subfeed';
    const ELEMENT_TYPE_INFORMATION = 'information';
    const ELEMENT_TYPE_LEADERBOARD = 'leaderboard';
    const ELEMENT_TYPE_STAR = 'star';
    const ELEMENT_TYPE_IMAGE = 'image';


    public int $element_id = 0;
    public ?string $element_type = null;
    public ?int $index = 0;
    public ?string $icon = null;
    public ?string $link = null;
}