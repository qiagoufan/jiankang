<?php


namespace facade\response\feed\detail\element;


class FeedVideoElementResp extends FeedBaseElementResp
{

    public ?int $video_width = 0;
    public ?int $video_height = 0;
    public ?int $video_duration = 0;
    public ?string $video_link = null;
    public ?string $video_content = null;
    public ?string $cover = null;
}