<?php


namespace facade\response\feed\detail\element;


use facade\response\feed\detail\element\fragment\LeaderResp;

class FeedLeaderboardElementResp extends FeedBaseElementResp
{
    /**
     * @var array|LeaderResp
     */
    public ?array $leader_list;
}