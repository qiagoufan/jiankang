<?php


namespace facade\response\feed\detail\element;


use facade\response\feed\detail\element\fragment\StarCardResp;

class FeedStarElementResp extends FeedBaseElementResp
{
    /**
     * @var array|StarCardResp
     */
    public $user_id;
    public $star_id;
    public $star_name;
    public $star_avatar;
    public $background;
    public $follow_status;
    public $subtitle_image;
    public $subtitle_content;
    public $font_color;
    public $ip_type;
    public $rank;
}