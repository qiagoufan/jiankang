<?php


namespace facade\response\feed\detail\element;


use facade\response\feed\detail\decoration\FeedHashtag;

class FeedSubfeedElementResp extends FeedBaseElementResp
{
    public $feed_id;
    public $user_id;
    public $nick_name;
    public $avatar;
    public $description;
    public $image_list;
    public ?FeedHashtag $hashtag = null;
    public ?array $hashtag_list = null;
    public $content;
    public $highlight_content;

}