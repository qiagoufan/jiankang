<?php


namespace facade\response\feed\detail\element;


class FeedReviewElementResp extends FeedBaseElementResp
{
    public $user_id;
    public $nick_name;
    public $avatar;
    public $content;
    public $image_list;
}