<?php


namespace facade\response\feed\detail\element;

class FeedImageElementResp extends FeedBaseElementResp
{
    public ?string $image = null;
    public ?string $original_image = null;
    public ?string $content = null;
    public int $image_width = 0;
    public int $image_height = 0;
    public ?array $hot_area_list = null;
}