<?php


namespace facade\response\feed\detail\element;


use facade\response\feed\detail\element\fragment\ProductTypeResp;

class FeedProductElementResp extends FeedBaseElementResp
{

    public $product_name;
    public $subtitle;
    public $format_price;
    public $main_image;
    public $background;
    public $sku_id;
    public $item_id;
    public $is_super;
    public $collect_status = 0;

    public ProductTypeResp $product_type;
}