<?php


namespace facade\response\feed\detail\element;


use facade\response\feed\detail\element\fragment\InformationResp;

class FeedInformationElementResp extends FeedBaseElementResp
{
    /**
     * @var array|InformationResp
     */
    public ?array $information_list;
}