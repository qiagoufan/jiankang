<?php


namespace facade\response\feed;


class StarRelatedActivityResp
{
    public ?string $cover = null;
    public ?string $link = null;
}