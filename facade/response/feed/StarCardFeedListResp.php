<?php


namespace facade\response\feed;

use facade\response\base\PageInfoResp;
use facade\response\feed\detail\FeedAuthorInfoResp;
use facade\response\feed\detail\FeedBaseInfoResp;
use facade\response\feed\detail\FeedDetailResp;
use facade\response\feed\detail\FeedInteractiveInfoResp;
use facade\response\maintain\MaintainTaskListResp;
use facade\response\user\group\StarGroupManagementResp;
use facade\response\user\StarInfoResp;
use facade\response\feed\StarRelatedActivityResp;

class StarCardFeedListResp
{
    public ?StarInfoResp $star_info_resp = null;
    public ?StarGroupManagementResp $star_group_management = null;
    public ?array $related_feed_list = [];
    public ?array $forum_feed_list = [];
    public ?StarRelatedActivityResp $star_related_activity;
    public ?ForumOverviewResp $forum_overview = null;
    public ?MaintainTaskListResp $star_task_list = null;
    public ?array $star_tag_info = [];
    public ?PageInfoResp $page_info;

}