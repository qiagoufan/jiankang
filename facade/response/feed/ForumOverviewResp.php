<?php


namespace facade\response\feed;


class ForumOverviewResp
{
    public ?int $total_user_num = 0;
    public ?array $total_user_list;
}