<?php


namespace facade\response\user;

class JwtTokenResp
{
    public $access_token;
    public $token_type;
    public $refresh_token;
    public $phone_num;
    public $user_id;


    public static function buildToken(string $accessToken, ?string $refreshToken = null, int $user_id): JwtTokenResp
    {
        $jwtToken = new JwtTokenResp();
        $jwtToken->token_type = "Bearer";
        $jwtToken->access_token = $accessToken;
        $jwtToken->refresh_token = $refreshToken;
        $jwtToken->user_id = $user_id;
        return $jwtToken;
    }


    public static function buildTokenV2(string $accessToken, string $phoneNum): JwtTokenResp
    {
        $jwtToken = new JwtTokenResp();
        $jwtToken->token_type = "Bearer";
        $jwtToken->access_token = $accessToken;
        $jwtToken->phone_num = $phoneNum;
        return $jwtToken;
    }
}