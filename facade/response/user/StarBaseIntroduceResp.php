<?php


namespace facade\response\user;

class StarBaseIntroduceResp
{
    public $ch_name;
    public $en_name;
    public $country;
    public $constellation;
    public $blood_type;
    public $tall;
    public $weight;
    public $born_place;
    public $born_date;


}