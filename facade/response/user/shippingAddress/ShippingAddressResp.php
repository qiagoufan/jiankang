<?php


namespace facade\response\user\shippingAddress;


class ShippingAddressResp
{
    public $id;
    public $user_id;
    public $address;
    public $receiver;
    public $province;
    public $city;
    public $area;
    public $is_default;
    public $phone;
    public $created_timestamp;
    public $updated_timestamp;
}