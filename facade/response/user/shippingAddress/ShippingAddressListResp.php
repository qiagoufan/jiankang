<?php


namespace facade\response\user\shippingAddress;


class ShippingAddressListResp
{
    public $shipping_address_list;

    public $page_info;

}