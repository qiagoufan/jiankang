<?php


namespace facade\response\user;

class UserInfoResp
{
    public $id;
    public $user_type;
    public $sex;
    public $nick_name;
    public $real_name;
    public $avatar;
    public $background;
    public $birthday;
    public $status;
    public $created_timestamp;
    public $platform;
    public $user_extra;
    //身高
    public $height;
    //体重
    public $weight;
    //婚姻情况
    public $marital_status;
    //职业
    public $profession;
    //身份证号
    public $id_num;
    //文化程度
    public $education;
    //医保类型
    public $insurance_type;
    //名字同步方式
    public $syn_status;

    public $user_remark;


    //管理端的字段
    public $phone;
    public $order_count;
    public $check_count;
    public $invite_count;

}