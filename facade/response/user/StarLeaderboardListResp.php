<?php


namespace facade\response\user;

use facade\response\base\PageInfoResp;

class StarLeaderboardListResp
{
    public ?array $star_list = null;

}