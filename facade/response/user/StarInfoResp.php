<?php


namespace facade\response\user;

class StarInfoResp
{
    public $star_id;
    public $star_name;
    public $star_avatar;
    public $star_background;
    public $star_description;
    public $star_summary;
    public $star_base_introduce = [];
    public int $follow_status = 0;
    public int $fans_count = 0;
    public int $leaderboard_value = 0;
    public string $leaderboard_rank;
    public int $task_value = 0;
    public $task_rank;
    public string $summary;
    public string $leader_summary;
    public string $task_summary;
    public ?int $ip_type = 1;
    public ?int $rank;
    public ?int $task_status = 0;
    public ?int $user_contribution = 0;
    public ?string $fans_leader_summary;
    public ?string $brand_leader_board_rank;
    public ?int $brand_hot_value = 0;

}