<?php


namespace facade\response\user;

use facade\response\base\PageInfoResp;

class UserHostHomeInfoResp
{

    public array $home_feed_info_list = [];
    public ?UserInfoResp $visit_user_info = null;
    public ?PageInfoResp $page_info;
    public int $block_status = 0;


}