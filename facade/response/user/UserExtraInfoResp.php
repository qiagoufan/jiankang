<?php


namespace facade\response\user;

class UserExtraInfoResp
{
    public int $follow_num = 0;
    public int $post_num = 0;
    public int $task_total_value = 0;
}