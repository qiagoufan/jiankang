<?php


namespace facade\response\user\invite;


class InviteRecordResp
{
    public $real_name;
    public $nick_name;
    public $avatar;
    public $display_invite_time;
    public $invite_timestamp;

}