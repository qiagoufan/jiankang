<?php


namespace facade\response\user\group;

use facade\response\feed\group\UserGroupRightResp;
use facade\response\user\UserInfoResp;

class StarGroupManagementResp
{
    public ?UserInfoResp $group_owner;
    public ?array $group_manager_list;
    public int $enable_apply_group_owner = 0;
    public int $enable_apply_group_manager = 0;
    public ?UserGroupRightResp $user_group_right = null;
    public int $is_owner = 0;
    public int $is_manager = 0;

}