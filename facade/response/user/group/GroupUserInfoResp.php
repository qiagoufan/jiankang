<?php


namespace facade\response\user\group;

/**
 * 圈子里的用户信息
 * @package facade\response\feed\group
 */
class GroupUserInfoResp
{
    public ?string $nick_name = null;
    public int $user_id;
    public ?string $avatar = null;
    public ?string $summary;
    public ?string $score;
    public ?string $rank;
    /**
     * @var array|null 用户勋章列表
     */
    public ?array $user_medal_list;

}