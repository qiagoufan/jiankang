<?php


namespace facade\response\user\achievement;


class UserMedalResp
{
    public ?string $medal_name = null;
    public ?string $medal_icon = null;

}