<?php


namespace facade\response\user;

use facade\response\base\PageInfoResp;

class StarListResp
{
    public ?array $star_list = null;
    public ?string  $list_name = null;
    public ?PageInfoResp $page_info;

}