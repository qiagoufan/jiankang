<?php


namespace facade\response\user;

class UserAdminInfoResp
{
    public $id;
    public $created_timestamp;
    public $user_info = [];

}