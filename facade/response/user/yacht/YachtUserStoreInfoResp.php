<?php


namespace facade\response\user\yacht;


class YachtUserStoreInfoResp
{
    public $store_id;
    public $store_province;
    public $store_city;
    public $store_area;
    public $store_name;
    public $store_address;

}