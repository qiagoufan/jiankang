<?php


namespace facade\response\user\yacht;

class YachtEngineerInfoResp
{
    public ?int $id = null;
    public ?int  $engineer_type = null;
    public ?int $sex = null;
    //真实姓名
    public ?string $real_name = null;
    //头像
    public ?string $avatar = null;
    //用户状态1:正常,0:已注销
    public ?int $status = 1;
    //地址
    public ?string $location = null;
    //注册时间
    public ?int $created_timestamp = null;
    //更新时间
    public ?int  $updated_timestamp = null;
    //省
    public ?string $province = null;
    //市
    public ?string $city = null;
    //区
    public ?string $area = null;
    //手机号
    public ?string $phone_num = null;
}