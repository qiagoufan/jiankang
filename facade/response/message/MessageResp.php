<?php

namespace facade\response\message;

use facade\response\base\RemindResp;

class MessageResp
{
    public $id;
    public $sender_user_id;
    public $receiver_user_id;
    public $image;
    public $title;
    public $content;
    public $message_status;
    public $message_type;
    public $link;
    public $created_timestamp;
    public $updated_timestamp;
    public $publish_timestamp;
    public $publish_timestamp_str;
}