<?php

namespace facade\response\message;


class MessageInteractiveListResp
{
    public $page_info;
    public $interactive_message_list = [];
    public $interactive_message_like_count = 0;
    public $like_people_count;

}