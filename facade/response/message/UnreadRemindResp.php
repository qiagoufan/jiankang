<?php

namespace facade\response\message;


use facade\response\base\RemindResp;

class UnreadRemindResp extends RemindResp
{
}