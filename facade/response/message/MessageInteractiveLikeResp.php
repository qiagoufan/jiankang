<?php

namespace facade\response\message;


class MessageInteractiveLikeResp
{

    public $like_user_id;
    public $like_user_name;
    public $like_user_avatar;
    public $feed_content;
    public $feed_image;
    public $created_timestamp;

}