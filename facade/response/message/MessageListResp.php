<?php

namespace facade\response\message;


use facade\response\base\RemindResp;

class MessageListResp
{
    public $page_info;
    public $message_list;

}