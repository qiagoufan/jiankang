<?php

namespace facade\response\sign;


class SignTaskUserResp
{

    public $sign_type_id;

    public $sign_task_id;
    //打卡任务名称
    public $sign_in_task_name;

    public $sign_in_task_bg;
    //打卡数值
    public $task_value;
    //打卡数值单位
    public $task_value_unit;

    public $user_score = 0;

    public $user_finish_status = 0;

    public $sign_in_date;

}