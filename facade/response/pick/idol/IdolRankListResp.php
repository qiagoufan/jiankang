<?php


namespace facade\response\pick\idol;

class IdolRankListResp
{
    public $beginDate;
    public $endDate;
    public array $list;

}