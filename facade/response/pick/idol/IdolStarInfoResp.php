<?php


namespace facade\response\pick\idol;

class IdolStarInfoResp
{
    public $logo_img;
    public $name;
    public $sid;
    public $en_name;

}