<?php


namespace facade\response\pick\idol;

class IdolFeedDetailResp
{
    public $idol_obj_id;
    public $title;
    public $type;
    public ?array $images = null;
    public $text;
    public $author_name;
    public $timestamp;

}