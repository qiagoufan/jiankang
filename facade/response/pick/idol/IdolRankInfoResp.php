<?php


namespace facade\response\pick\idol;

class IdolRankInfoResp
{
    public $rank;
    public $score;
    public IdolStarInfoResp $starinfo;
    public int $rise;

}