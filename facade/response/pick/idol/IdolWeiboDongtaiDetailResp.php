<?php


namespace facade\response\pick\idol;

class IdolWeiboDongtaiDetailResp
{
    public $idol_dongtai_id;
    public $weibo_id;
    public $idol_star_id;
    public ?string $type;
    public ?string $text;
    public ?array $image_list = null;
    public $weibo_url;

}