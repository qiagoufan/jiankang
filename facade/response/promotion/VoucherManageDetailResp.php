<?php


namespace facade\response\promotion;


class VoucherManageDetailResp
{
    public $display_voucher_discount;
    public $display_voucher_discount_qualification;
    public $voucher_name;
    public $start_timestamp;
    public $end_timestamp;
    public $expire_timelong;
    public $day_long;
    public $voucher_qualification;
    public $voucher_status;
    //优惠券数量
    public $voucher_count;
    //已领取的数量
    public $voucher_distribute_count;
    //已使用的数量
    public $voucher_used_count;
    public $id;
    public $voucher_detail = null;
}