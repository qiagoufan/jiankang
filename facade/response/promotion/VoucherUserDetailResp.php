<?php


namespace facade\response\promotion;


class VoucherUserDetailResp
{
    public $display_voucher_discount;
    public $display_voucher_discount_qualification;
    public $voucher_name;
    public $display_time_range;
    public $voucher_qualification;
    public $unavailable_reason;
    public $enable_status;
    public $conform_status;
    public $use_status;
    public $voucher_sn;
    public $voucher_id;
    public $available_status = 0;
    public $voucher_detail = null;
}