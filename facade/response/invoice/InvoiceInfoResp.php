<?php

namespace facade\response\invoice;

use facade\response\repair\order\RepairCustomerInfoResp;
use facade\response\repair\order\RepairEngineerResp;
use facade\response\repair\order\RepairExtraInfoResp;
use facade\response\repair\order\RepairOrderBaseResp;
use facade\response\repair\order\RepairOrderDetailResp;

class InvoiceInfoResp
{
    public ?int $invoice_id = null;

    public ?string $company_name = null;

    public ?string $total_amount = null;

    public ?int $total_count = null;

    public ?int $invoicing_status = null;

    public ?int $created_timestamp = null;

    public ?array $order_list = null;


}