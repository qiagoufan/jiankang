<?php

namespace facade\response\invoice\order;

use facade\response\repair\order\RepairCustomerInfoResp;
use facade\response\repair\order\RepairEngineerResp;
use facade\response\repair\order\RepairExtraInfoResp;
use facade\response\repair\order\RepairOrderBaseResp;
use facade\response\repair\order\RepairOrderDetailResp;

class InvoiceOrderResp
{
    public ?int $repair_order_id = null;

    public ?string $customer_price = null;

    public ?int $store_id = null;

    public ?string $store_name = null;

    public ?string $store_num = null;

    public ?string $address = null;

    public ?string $store_manager_phone = null;

    public ?string $store_manager_name = null;

    public ?int $created_timestamp = null;

    public ?int $appointment_timestamp = null;


}