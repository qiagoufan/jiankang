<?php


namespace facade\response\organization;


class OrganizationCompanyDetailResp
{
    public $id;
    //
    public $company_name;
    //
    public $company_license;

    public $company_license_pic;
    //
    public $service_start_timestamp;
    //
    public $service_end_timestamp;
    //
    public $contact_person;
    //
    public $contac_info;
    //
    public $emergency_contact_person;
    //
    public $emergency_contact_info;
    //
    public $company_status;

    public $account_name;
}