<?php


namespace facade\response\organization\store;


class StoreManagerDetailResp
{
    public $id;
    public $store_id;
    public $store_manager;
    public $contact_info;
}