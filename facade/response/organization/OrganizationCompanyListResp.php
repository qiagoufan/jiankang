<?php


namespace facade\response\organization;


use facade\response\base\PageInfoResp;

class OrganizationCompanyListResp
{
    public ?array $list = null;
    public ?PageInfoResp $page_info = null;
}