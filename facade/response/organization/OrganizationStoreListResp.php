<?php


namespace facade\response\organization;


use facade\response\base\PageInfoResp;

class OrganizationStoreListResp
{
    public ?array $list = null;
    public ?PageInfoResp $page_info = null;
}