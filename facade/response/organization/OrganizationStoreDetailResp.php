<?php


namespace facade\response\organization;


class OrganizationStoreDetailResp
{
    public $id;
    //
    public ?int $company_id = null;
    //
    public ?string $store_name = null;
    //
    public $store_status;
    //
    public $store_address;
    //
    public $store_province;
    //
    public $store_city;
    //
    public $store_area;
    //
    public $store_street;
    //
    public $store_num;

    public ?array $store_manager_list = null;
    //
    public $store_level;
}