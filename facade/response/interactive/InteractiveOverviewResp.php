<?php

namespace facade\response\interactive;

use facade\response\base\PageInfoResp;


class InteractiveOverviewResp
{
    public ?int $comment_count;
    public ?int $reply_count;

}