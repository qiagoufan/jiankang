<?php

namespace facade\response\interactive;

use facade\response\base\PageInfoResp;


class InteractiveCommentListResp
{
    public array $comment_base_list = [];
    public ?PageInfoResp $page_info;

}