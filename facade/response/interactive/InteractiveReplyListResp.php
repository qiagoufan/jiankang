<?php

namespace facade\response\interactive;

use facade\response\base\PageInfoResp;


class InteractiveReplyListResp
{
    public array $reply_base_list = [];
    public ?PageInfoResp $page_info;

}