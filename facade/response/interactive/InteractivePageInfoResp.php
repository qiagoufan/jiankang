<?php

namespace facade\response\interactive;

use facade\response\base\PageInfoResp;


class InteractiveCommentBaseInfoResp
{
    public ?string $comment_content;
    public ?int  $comment_id;
    public ?int $user_id;
    public ?string $user_name;
    public ?string $user_avatar;

}