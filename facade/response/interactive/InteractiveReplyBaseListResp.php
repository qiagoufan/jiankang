<?php

namespace facade\response\interactive;


class InteractiveReplyBaseListResp
{

    const REPLY_TYPE_TO_COMMENT = 1;
    const REPLY_TYPE_REPLY = 2;
    public $reply_content;
    public ?int  $reply_id;
    public ?int $user_id;
    public ?string $user_name;
    public ?string $user_avatar;
    public ?int $reply_status;
    public ?int $created_timestamp;
    public ?int $reply_to_user_id = null;
    public ?int $reply_to_id = null;
    public ?string $reply_to_user_name = null;
    public ?int $reply_type;
    public ?string $publish_time_str;
    public $reply_to_user_content;
    public ?int $reply_to_parent_status;

}