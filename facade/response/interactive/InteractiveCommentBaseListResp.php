<?php

namespace facade\response\interactive;


class InteractiveCommentBaseListResp
{
    public object $comment_content;
    public ?int  $comment_id;
    public ?int $user_id;
    public ?string $user_name;
    public ?string $user_avatar;
    public ?InteractiveReplyListResp $reply_list;
    public ?int $comment_status;
    public ?int $created_timestamp;
    public ?string $publish_time_str;

}