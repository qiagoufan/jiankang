<?php

namespace facade\response\shoppingCart;


class ShoppingCartProductResp
{
    public $item_id = 0;
    public $sku_id = 0;
    public $product_name = null;
    public $product_price = null;
    public $format_product_price = null;
    public $count = 0;
    public $main_image = null;
}