<?php

namespace facade\response\shoppingCart;


class ShoppingCartResp
{
    public $shopping_cart_product_list = [];
    public $total_price = 0;
    public $format_total_price = '';
}