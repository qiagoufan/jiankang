<?php


namespace facade\response\maintain;

use facade\response\base\PageInfoResp;
use facade\response\maintain\contribution\StarContributionInfoResp;
use facade\response\maintain\contribution\UserContributionResp;

/**
 * 贡献值榜单
 * Class MaintainrContributionLeaderboardResp
 * @package facade\response\maintain
 */
class MaintainrContributionLeaderboardResp
{

    public StarContributionInfoResp $starInfo;
    public array $user_list;
    public ?string $list_name = null;
    public PageInfoResp $page_info;
    public UserContributionResp $user_contribution;

}