<?php


namespace facade\response\maintain;

class MaintainStarTaskResp
{

    public string $star_name;
    public array $task_list = [];
    public string $star_avatar;
    public int $star_id;
}