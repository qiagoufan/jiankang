<?php


namespace facade\response\maintain;

class MaintainTaskHistoryResp
{

    public string $task_name;
    public int $task_value;
    public string $star_name;
    public int $task_time;
    public int $task_id;
}