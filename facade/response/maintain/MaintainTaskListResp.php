<?php


namespace facade\response\maintain;


class MaintainTaskListResp
{

    public ?int $task_user_num = 0;
    public ?array $task_user_list = [];
    public ?array $task_list = [];
    public $task_audio_list = null;
    public $activity_text_info;


}


