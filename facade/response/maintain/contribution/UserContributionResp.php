<?php


namespace facade\response\maintain\contribution;

class UserContributionResp
{
    public ?string $nick_name = null;
    public int $user_id;
    public ?string $avatar = null;
    public ?int $score = null;
    public ?string $rank;
}