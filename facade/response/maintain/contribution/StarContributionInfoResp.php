<?php


namespace facade\response\maintain\contribution;

class StarContributionInfoResp
{
    public string $leaderboard_name;
    public int $sum_score;
    public ?string $background;
}