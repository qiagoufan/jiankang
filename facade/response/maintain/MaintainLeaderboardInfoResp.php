<?php


namespace facade\response\maintain;


class MaintainLeaderboardInfoResp
{

    public string $leaderboard_title;
    public string $leaderboard_summary;
    public string $leaderboard_icon;
    public ?array $task_list;

}