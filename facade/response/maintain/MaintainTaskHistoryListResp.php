<?php


namespace facade\response\maintain;

use facade\response\base\PageInfoResp;

class MaintainTaskHistoryListResp
{

    public ?array $task_user_star_info = [];
    public ?array $task_list;
    public ?PageInfoResp $page_info;
}