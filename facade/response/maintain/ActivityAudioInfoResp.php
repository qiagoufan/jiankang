<?php


namespace facade\response\maintain;


class ActivityAudioInfoResp
{

    public ?int $video_width;
    public ?int  $video_height;
    public ?int $video_duration;
    public ?string $video_link;
    public ?string $video_content;
    public ?string $cover;
    public ?string $type;


}

