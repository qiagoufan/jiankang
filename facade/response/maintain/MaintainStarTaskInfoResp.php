<?php


namespace facade\response\maintain;


class MaintainStarTaskInfoResp
{
    public ?int $task_id;
    public ?int $task_status = 0;
    public ?string $task_name = null;
    public ?int $task_value = 1;//任务值
    public $star_total_value = 0;//明星收到的所有贡献总值
    public $star_value_summary;
    public $user_value = null;//用户已经贡献
}