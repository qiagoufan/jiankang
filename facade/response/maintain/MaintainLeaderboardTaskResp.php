<?php


namespace facade\response\maintain;


class MaintainLeaderboardTaskResp
{

    public string $title;
    public string $summary;
    public string $button_name;
    public int $button_status = 0;
    public ?string $button_link = null;

}