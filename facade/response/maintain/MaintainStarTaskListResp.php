<?php


namespace facade\response\maintain;

use facade\response\base\PageInfoResp;

class MaintainStarTaskListResp
{

    public PageInfoResp $page_info;
    public array $maintain_star_task_list = [];

}