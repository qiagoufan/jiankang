<?php


namespace facade\response;


class Result
{
    public $errorCode;
    public $errorMessage;
    public $data = null;
    public $success = false;

    public function setSuccessWithResult($data = null)
    {
        $this->success = true;
        $this->errorCode = 0;
        $this->errorMessage = 'success';
        $this->data = $data;
    }


    /**
     * result校验
     * @param Result $result
     * @param bool $dataVerified
     * @return bool
     */
    public static function isSuccess(Result $result, bool $dataVerified = true): bool
    {
        if ($result == null || $result->success == false || $result->errorCode != 0) {
            return false;
        }

        if ($dataVerified && $result->data === null) {
            return false;
        }
        return true;
    }

    public function setError(array $errorInfo)
    {
        $this->errorCode = $errorInfo[0];
        $this->errorMessage = $errorInfo[1];
    }

    public function setData($data)
    {
        $this->data = $data;
    }

}