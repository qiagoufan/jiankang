<?php


namespace facade\response\search;


class SearchKeywordResp
{
    /**
     * @var array|null 推荐词列表
     */
    public ?array $recommend_keyword_list;
    /**
     * @var array|null 搜索分词列表
     */
    public ?array $word_list;

}