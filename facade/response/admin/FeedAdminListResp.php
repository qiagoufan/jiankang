<?php

namespace facade\response\admin;
class FeedAdminListResp
{
    public $feed_info_list;
    public $page_info;
}