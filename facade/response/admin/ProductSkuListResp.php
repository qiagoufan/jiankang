<?php

namespace facade\response\admin;


class ProductSkuListResp
{

    public ?array $product_sku_list = [];
    public ?array $specifications = [];
    public ?array $related_ip = [];
    public ?int $shipping_price = null;


}