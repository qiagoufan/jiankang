<?php

namespace facade\response\admin;

class FeedAdminDetailResp
{
    public $feed_author_info;
    public $feed_base_info;
    public $feed_interaction_info;
    public $feed_bind_info;
}