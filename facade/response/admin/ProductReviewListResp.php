<?php

namespace facade\response\admin;


class ProductReviewListResp
{

    public $product_review_base_info;
    public $order_product_info;
    public $order_base_info;

}