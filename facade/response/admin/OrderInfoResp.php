<?php

namespace facade\response\admin;


use facade\response\base\PageInfoResp;

class OrderInfoResp
{


    public $order_product_info;
    public $order_user_info;
    public $order_base_info;

}