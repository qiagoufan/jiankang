<?php

namespace facade\response\admin;


class ProductItemResp
{

    public ?array $product_sku_info = [];
    public $id;
    public $name;
    public $created_timestamp;
    public $publish_status;
    public $low_price;
    public $high_price;
    public $sale;
    public $stock;
    public $default_sku_id;
    public $detail_image;
    public $gallery_image;
    public ?string $background_pic;
    public ?string $main_pic;
    public ?string $description;
    public ?string $summary;
    public ?int $is_super;

}