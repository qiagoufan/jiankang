<?php

namespace facade\response\admin;


use facade\response\base\PageInfoResp;

class ProductAdminItemListResp
{


    public array $product_item_list = [];
    public PageInfoResp $pageInfo;


}