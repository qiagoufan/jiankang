<?php

namespace facade\response\admin;


class ProductCategoryListResp
{

    public $value;
    public $label;
    public array $children = [];
}