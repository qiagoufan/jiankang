<?php

namespace facade\response\admin\user;


class UserAdminInteractiveInfoResp
{
    public ?int $focus_ip_num;
    public ?int $comment_num;
    public ?int $collect_num;
    public ?int $share_num;

}