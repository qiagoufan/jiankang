<?php

namespace facade\response\admin\user;


class UserAdminTransactionResp
{

    public ?int $latest_order_time;//最近购买时间
    public ?int $total_consumption;//总消费金额
    public ?int $total_order_number;//总订单数
    public ?int $total_refund_amount;//总退款金额
    public ?int $total_refund_count;//总退款单数


}