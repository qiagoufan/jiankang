<?php

namespace facade\response\admin\user;


class UserAdminBaseInfoResp
{

    public ?string $nick_name = null;
    public ?int $created_timestamp = null;
    public ?int $first_order_time;
    public ?string $phone = null;
    public ?string $avatar = null;
    public ?string $default_shipping_address;
    public ?int $latest_login_timestamp;
    public ?int $sex;
    public ?int $id;

}