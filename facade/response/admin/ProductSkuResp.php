<?php

namespace facade\response\admin;


class ProductSkuResp
{
    public $id;
    public $current_price = 0;
    public $former_price = 0;
    public $stock = 0;
    public $indexes;
    public $detail;


}