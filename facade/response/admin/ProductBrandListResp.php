<?php

namespace facade\response\admin;


use facade\response\base\PageInfoResp;

class ProductBrandListResp
{
    public array $product_brand_list = [];
}