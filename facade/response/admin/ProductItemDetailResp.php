<?php

namespace facade\response\admin;


class ProductItemDetailResp
{

    public $specifications;
    public $name;
    public $brand_id;
    public $sale;
    public $seller_id;
    public $created_timestamp;
    public $updated_timestamp;
    public $publish_status;
    public $stock;
    public $shipping_price;
    public $default_sku_id;
    public $deleted_timestamp;
    public $review_count;
    public $categories;
    public $main_pic;
    public $detail_image = [];
    public $background_pic;
    public $gallery_image = [];
    public $is_super;
    public $summary;
    public $description;
    public $related_ip = [];


}