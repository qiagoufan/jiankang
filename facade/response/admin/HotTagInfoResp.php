<?php

namespace facade\response\admin;


use facade\response\base\PageInfoResp;

class HotTagInfoResp
{


    public $id;
    public $hot_tag_id;
    public $hot_tag_type;
    public $hot_tag_name;

}