<?php


namespace facade\response\product;


class ProductSpecificResp
{
    public string $index;
    public array $specification;
}