<?php

namespace facade\response\product;

class ProductExtraInfoResp
{
    public string $explanation = '';
    public int $collect_status = 0;

}