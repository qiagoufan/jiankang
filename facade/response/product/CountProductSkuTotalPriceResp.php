<?php

namespace facade\response\product;

class CountProductSkuTotalPriceResp
{
    public $total_price;
    public $pay_price;
    public $shipping_price;

}