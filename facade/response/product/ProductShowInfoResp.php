<?php

namespace facade\response\product;


class ProductShowInfoResp
{
    public ?string $feed_id;
    public ?string $product_show_image;
}