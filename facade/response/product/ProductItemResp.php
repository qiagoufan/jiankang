<?php

namespace facade\response\product;


class ProductItemResp
{

    public ?string $product_name;
    public ?int $stock;
    public ?int $sale;
    public ?string $sale_price;
    public ?string $original_price;
    public ?int $published_timestamp;
    public ?int $item_id;
    public ?int $sku_id;
    public ?string $main_image;


}