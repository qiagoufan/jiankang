<?php

namespace facade\response\product;


class ProductItemForSkuResp
{

    const PURCHASE_STATUS_NORMAL = 1;
    const PURCHASE_STATUS_OFFLINE = -1;
    const PURCHASE_STATUS_SELL_OUT = 0;

    public ?int $item_id;
    public ?array $product_specification_list;
    public ?int $purchase_status;
    public ?int $default_sku_id;

}