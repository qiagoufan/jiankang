<?php

namespace facade\response\product;


class ProductBaseInfoResp
{

    public ?string $title;
    public ?int $stock;
    public ?int $sku_id;
    public ?int $item_id;
    public ?int $seller_id;
    public ?int $sale;
    public ?ProductSellerInfoResp $seller = null;
    public ?string $current_price = null;

}