<?php

namespace facade\response\product;


class ProductSkuDetailResp
{


    /**
     * 规格
     * @var ProductSpecificResp|null
     */
    public ?ProductSpecificResp $product_specification = null;

    /**
     * 商品画廊
     * @var ProductGalleyResp|null
     */
    public ?ProductGalleyResp $product_gallery;

    /**
     * 商品基础信息
     * @var ProductBaseInfoResp|null
     */
    public ?ProductBaseInfoResp $product_base_info;

    /**
     * 商品附加信息
     * @var ProductExtraInfoResp|null
     */
    public ?ProductExtraInfoResp $product_extra_info;

    /**
     * 商品详情
     * @var ProductDetailInfoResp|null
     */
    public ?ProductDetailInfoResp $product_detail_info;

    /**
     * 商品评价
     * @var ProductReviewListResp|null
     */
    public ?ProductReviewListResp $product_review_info;

    /**
     * item信息?
     * @var ProductItemForSkuResp|null
     */
    public ?ProductItemForSkuResp $product_item;

    /**
     * 装饰信息
     * @var ProductDecorationResp|null
     */
    public ?ProductDecorationResp $product_decoration_info;

    /**
     * 品牌信息
     * @var ProductSkuBrandInfoResp|null
     */
    public ?ProductSkuBrandInfoResp $product_brand_info;


    /**
     * 商品标签
     * @var array|null
     */
    public ?array $product_tag_info = null;

}