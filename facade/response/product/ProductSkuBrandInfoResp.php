<?php

namespace facade\response\product;


class ProductSkuBrandInfoResp
{
    public ?string $brand_link;
    public ?string $brand_name;
}