<?php

namespace facade\response\product\category;


class CategoryResp
{
    //主键id
    public $id;
    //一级分类id
    public $cate_level1_id;
    //一级分类名称
    public $cate_level1_name;
    //二级分类id
    public $cate_level2_id;
    //二级分类名称
    public $cate_level2_name;
    //三级分类id
//    public $cate_level3_id;
//    //三级分类名称
//    public $cate_level3_name;
    //分类名称
    public $category_name;
    //父分类id
    public $parent_id;
    //分类层级
    public $category_level;
    //封面
    public $category_cover;


    public $category_list = null;
}