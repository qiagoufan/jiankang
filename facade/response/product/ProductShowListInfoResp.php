<?php

namespace facade\response\product;


use facade\response\base\PageInfoResp;

class ProductShowListInfoResp
{
    public ?array $feed_info_list;
    public ?PageInfoResp $page_info;
}