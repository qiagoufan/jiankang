<?php

namespace facade\response\product\jk\detail;


class JkProductSkuResp
{

    public $sku_name;
    public $sku_id;
    public $stock;
    public $sale;
    public $display_regular_price;
    public $regular_price;
    public $current_price;
    public $display_current_price;
    public $shipping_price;
    public $display_shipping_price;


}