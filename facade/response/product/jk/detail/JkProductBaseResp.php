<?php

namespace facade\response\product\jk\detail;


class JkProductBaseResp
{

    public $item_id;
    public $default_sku_id;
    public $product_name;
    public $publish_status;
    public $summary;
    public $stock;
    public $sale;
    public $current_price;
    public $category_id;
    public $display_current_price;
    public $regular_price;
    public $display_regular_price;
    public $shipping_price;
    public $display_shipping_price;
    public $published_timestamp;
    public $created_timestamp;
    public $main_image;
    public $seq;


}