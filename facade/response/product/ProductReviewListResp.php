<?php

namespace facade\response\product;


use facade\response\base\PageInfoResp;

class ProductReviewListResp
{

    public ?array $product_review_list;
    public ?int $total;
    public ?PageInfoResp $page_info;


}