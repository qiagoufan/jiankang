<?php


namespace facade\response\product;

/**
 * 超级商品信息
 * Class ProductSuperDetailResp
 * @package facade\response\product
 */
class ProductSuperDetailResp
{
    public $sku_id;
    public $title;
    public $sku_name;
    public $sku_main_pic;
    public $star_avatar;
    public $star_name;
    public $star_id;
    public $sku_background;
    public $sku_price;
    public $purchase_info;
    public $is_super;
    public int $collect_status = 0;
    public int $collect_count = 0;
    public int $comment_count = 0;
}