<?php

namespace facade\response\product;


class ProductReviewResp
{

    public $review_id;
    public $item_id;
    public $sku_id;
    public $link;
    public $user_id;
    public $nick_name;
    public $user_avatar;
    public $publish_timestamp;
    public $publish_time_str;
    public $content;
    public $image_list;
    public $specification_detail;


}