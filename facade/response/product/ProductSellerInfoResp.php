<?php

namespace facade\response\product;


class ProductSellerInfoResp
{

    public string $name;
    public string $avatar;

}