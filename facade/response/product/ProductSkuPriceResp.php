<?php

namespace facade\response\product;


class ProductSkuPriceResp
{

    public $former_price;
    public $former_price_formatted;
    public $current_price;
    public $current_price_formatted;
    public $price_tag;
    public $low_price;
    public $low_price_formatted;
    public $high_price;
    public $high_price_formatted;
    public $tag_list;
    public $shipping_price;
    public $shipping_price_formatted;
    public $total_price;
    public $total_price_formatted;

}