<?php


namespace facade\response\system;

class OTAResp
{
    public $method;
    public $upgrade_hints;
    public $version_name;
    public $download_link;
    public $file_size;


    const METHOD_NONE = 0;
    const METHOD_NORMAL = 1;
    const METHOD_FORCE = 2;
}