<?php


namespace facade\response\system;

class OSSResp
{
    public $securityToken;
    public $accessKeyId;
    public $accessKeySecret;
    public $expiration;
    public $bucket;
    public $endpoint;

}