<?php


namespace facade\response\system\media;

class CreateUploadImageResp
{

    public $uploadAddress;
    public $requestId;
    public $uploadAuth;
    public $imageURL;
    public $fileURL;
    public $imageId;

}