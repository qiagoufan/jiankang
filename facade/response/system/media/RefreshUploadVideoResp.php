<?php


namespace facade\response\system\media;

class RefreshUploadVideoResp
{
    public $uploadAddress;
    public $videoId;
    public $requestId;
    public $uploadAuth;

}