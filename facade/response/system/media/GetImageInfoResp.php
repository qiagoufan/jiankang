<?php


namespace facade\response\system\media;


class GetImageInfoResp
{
    public $image_width;
    public $image_height;
    public $file_size;
    public $image_id;
    public $image_url;
    public $file_url;
    public $status;
}