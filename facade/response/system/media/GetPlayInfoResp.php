<?php


namespace facade\response\system\media;


class GetPlayInfoResp
{
    public $cover_url;
    public $video_id;
    public $duration;
    /**
     *  原画播放地址
     *
     */
    public $od_play_url;
    public $od_video_width;
    public $od_video_height;
    public $od_video_size;
    /**
     *  高清播放地址
     */
    public $sd_play_url;
    public $sd_video_size;

    public $title;
}