<?php


namespace facade\response\system\media;

class CreateUploadVideoResp
{
    public $uploadAddress;
    public $videoId;
    public $requestId;
    public $uploadAuth;

}