<?php


namespace facade\response\system\media;


class GetVideoInfoResp
{
    public $cover_url;
    public $video_id;
    public $duration;
    public $video_size;
}