<?php

namespace facade\response\juhe;


class JuheCalendarResp
{
    public $animals_year;
    public $avoid;
    public $lunar;
    public $lunar_year;
    public $suit;
    public $holiday;
}