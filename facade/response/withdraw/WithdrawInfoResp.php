<?php


namespace facade\response\withdraw;

class WithdrawInfoResp
{
    public $withdraw_id;
    public $engineer_id;
    public $engineer_name;
    public $engineer_phone_num;
    public $withdraw_total_amount;
    public $repair_order_id_list;
    public $creatd_timestamp;

    public $cash_out_status;
    //提现卡号
    public $withdraw_card_num;
    //提现卡姓名
    public $withdraw_card_real_name;
    //提现银行
    public $withdraw_card_bank;


    public ?array  $repair_order_list = [];


}