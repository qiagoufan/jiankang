<?php


namespace facade\response\withdraw\order;

class WithdrawRepairOrderInfoResp
{
    public ?int $repair_order_id = null;

    public ?string $engineer_reward = null;

    public ?int $store_id = null;

    public ?string $store_name = null;

    public ?string $store_num = null;

    public ?string $address = null;

    public ?string $store_manager_phone = null;

    public ?string $store_manager_name = null;

    public ?int $created_timestamp = null;

    public ?int $appointment_timestamp = null;


}