<?php

namespace facade\response\jiankang\bodyCheck;

class BodyQuestionResp
{
    public $question_id = null;
    public $title = null;
    public $seq = 0;
    public $option_list = [];
}