<?php

namespace facade\response\jiankang\knowledge;


class JkKnowledgeResp
{
    //主键
    public $id;
    //封面
    public $knowledge_cover;
    //标题
    public $knowledge_title;
    //详情
    public $knowledge_content;
    //
    public $category_id;
    //
    public $created_timestamp;

    public $display_created_time;

}