<?php

namespace facade\response\jiankang\riskCheck;

class RiskCheckQuestionResp
{
    //主键
    public $id;
    //标题
    public $question_title;
    //选项列表
    public $question_option_list;
    //排序
    public $question_sort_num;

    public $category_id;
}