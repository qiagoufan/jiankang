<?php


namespace facade\response\order\product;


class OrderProductSpecificResp
{
    public  $index = null;
    public  $specification = null;
}