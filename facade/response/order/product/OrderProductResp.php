<?php


namespace facade\response\order\product;


class OrderProductResp
{
    public $item_id = 0;
    public $sku_id = 0;
    public $in_stock = 0;
    public $stock = 0;
    public $count = 0;
    public $total_price = 0;
    public $sku_price = 0;
    public $pay_price = 0;
    public $publish_status = 0;
    public $display_sku_price = null;
    public $display_pay_price = null;
    public $display_total_price = null;
    public $product_name = null;
    public $main_image = null;
    public $sku_name = null;
}