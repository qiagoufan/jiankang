<?php


namespace facade\response\order;


class QueryPayDetailInfoResp
{

    public $total_amount;
    public $trade_status;
    public $trade_no;
    public $out_trade_no;
    public $buyer_user_id;
    public $send_pay_date;

}