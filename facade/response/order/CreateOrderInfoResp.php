<?php


namespace facade\response\order;


class CreateOrderInfoResp
{

    public $user_id = null;
    public $order_sn = null;
    public $total_price = null;
    public $payment_trade_no = null;
    public $pay_price = 0;
    public $pay_status;
    public $pay_timestamp = null;
    public $pay_type;
    public $receiver = null;
    public $address = null;
    public $phone = null;
    public $delivery_timestamp = null;
    public $order_status;
    public $created_timestamp = null;
    public $updated_timestamp = null;
    public $review_status = null;
    public $platform;
    public $delete_status;
    public $province = null;
    public $city = null;
    public $area = null;
    public $order_close_type = null;
    public $express_company = null;
    public $express_no = null;
}



