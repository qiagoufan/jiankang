<?php


namespace facade\response\order;


class OrderDetailResp
{
    public $order_id = null;
    public $order_sn = null;
    public $order_product_list = null;
    public $total_price = 0;
    public $display_total_price = null;
    public $pay_price = 0;
    public $display_pay_price = null;
    public $shipping_price = 0;
    public $display_shipping_price = null;
    public $discount_price = 0;
    public $display_discount_price = null;
    public $order_status = 0;
    public $pay_status = 0;
    public $shipping_info = null;
    public $order_tips = null;
    public $created_timestamp = 0;
    public $display_created_time = null;
    public $pay_timestamp = 0;
    public $transaction_info = null;
    public $user_info = null;

}