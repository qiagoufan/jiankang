<?php


namespace facade\response\order;

use facade\response\base\PageInfoResp;

class GetOrderListResp
{

    public  $page_info;
    public  $order_detail_info;

}