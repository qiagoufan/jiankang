<?php


namespace facade\response\order;

class OrderRemindResp
{
    public $wait_pay;
    public $wait_ship;
    public $wait_receive;
    public $wait_review;
    public $shopping_cart_count;
}