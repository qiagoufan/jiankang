<?php


namespace facade\response\order;

class CalculatePriceResp
{
    public $order_product_list = null;
    public $product_total_price = 0;
    public $discount_price = 0;
    public $shipping_price = 0;
    public $pay_price = 0;
    public $product_count = 0;
    public $display_product_total_price = null;
    public $display_discount_price = null;
    public $display_shipping_price = null;
    public $display_pay_price = null;
    public $voucher_sn = null;
    public $voucher_sn_list = null;
    public $super_value = 0;
}