<?php


namespace facade\response\order;

class QueryOrderPayInfoResp
{
    /** @var int 0未支付，1已支付 */
    public $result = 0;
    public $order_info;
}