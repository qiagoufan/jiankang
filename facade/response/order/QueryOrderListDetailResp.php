<?php


namespace facade\response\order;


class QueryOrderListDetailResp
{
    public $product_main_pic;
    public $product_name;
    public $product_specifications;
    public $order_sn;
    public $product_order_status;
    public $sku_id;
    public $order_id;
    public $shipping_word;
    public $shipping_info;
    public $product_comment_id = null;
    public $item_id;
    public $total_price;
    public $display_total_price;
    public $created_timestamp;
    public $count;
    public $transaction_info;
    public $seller_name;

}