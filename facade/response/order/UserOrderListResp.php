<?php


namespace facade\response\order;


class UserOrderListResp
{
    public $order_list = null;
    public $page_info = null;
}