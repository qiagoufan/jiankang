<?php

namespace facade\response\order;


class OrderDisplayInfoResp
{
    public $id = null;
    public $user_id = null;
    public $order_sn = null;
    public $total_price = null;
    public $payment_trade_no = null;
    public $pay_price = null;
    public $pay_status = null;
    public $pay_timestamp = null;
    public $pay_type = null;
    public $receiver = null;
    public $address = null;
    public $phone = null;
    public $delivery_timestamp = null;
    public $order_status = null;
    public $created_timestamp = null;
    public $updated_timestamp = null;
    public $review_status = null;
    public $platform = null;
    public $delete_status = null;
    public $province = null;
    public $city = null;
    public $area = null;
    public $order_close_type = null;
    public $express_company = null;
    public $express_no = null;
    public $shipping_price = null;
    public $discount_price = null;
    public $voucher_sn = null;
    public $power_value = null;

}