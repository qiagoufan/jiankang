<?php


namespace facade\response\order;

use facade\response\base\PageInfoResp;

class OrderTransactionResp
{

    public $pay_type;
    public $created_timestamp;
    public $pay_timestamp;
}