<?php

namespace facade\response\base;

class PageInfoResp
{
    public $page_size = 10;
    public $index = 0;
    public $has_more = false;
    public $count = 0;

    public static function buildPageInfoResp(int $index = 0, int $pageSize = 0, int $currentSize = 0, int $count = 0): PageInfoResp
    {

        $pageInfoResp = new self();
        $pageInfoResp->page_size = $pageSize;
        $pageInfoResp->index = $index + 1;
        if ($index == 0) {
            $pageInfoResp->has_more = ($count > $pageSize);
        } else {
            $pageInfoResp->has_more = $count > ($pageInfoResp->index * $pageSize);
        }
        $pageInfoResp->count = $count;
        return $pageInfoResp;
    }

    public static function buildPageInfoRespBaseLine(int $index, int $pageSize, int $currentSize, ?int $baseLineSize = 0): PageInfoResp
    {
        $pageInfoResp = new self();
        $pageInfoResp->index = $index + 1;
        $pageInfoResp->page_size = $pageSize;
        if ($baseLineSize == 0) {
            $baseLineSize = $pageSize;
        }
        $pageInfoResp->has_more = $currentSize >= $baseLineSize;
        $pageInfoResp->count = $currentSize;

        return $pageInfoResp;
    }
}