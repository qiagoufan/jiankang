<?php

namespace facade\response\base;

class DataListResp
{
    public $page_info = null;
    public $list = [];
}