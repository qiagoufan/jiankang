<?php

namespace facade\response\theatre;

use facade\response\base\PageInfoResp;

class TheatreIndexListResp
{
    public ?array $today_show_list = null;
    public ?array $history_show_list = null;
    public ?ShowDetailResp $upcoming_show = null;
    public ?PageInfoResp $page_info = null;
}