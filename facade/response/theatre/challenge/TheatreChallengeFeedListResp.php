<?php

namespace facade\response\theatre\challenge;

use facade\response\base\PageInfoResp;

class TheatreChallengeFeedListResp
{
    public ?array $challenge_feed_list = null;
    public ?PageInfoResp $page_info;
}