<?php

namespace facade\response\theatre\challenge;

class TheatreChallengeListResp
{
    public ?array $challenge_list = null;
}