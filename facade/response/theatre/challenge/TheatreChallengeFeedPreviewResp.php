<?php

namespace facade\response\theatre\challenge;

class TheatreChallengeFeedPreviewResp
{
    public int $feed_id = 0;
    public ?string $top_icon = null;
    public ?string $preview_image = null;
    public ?int $like_count = null;
    public int $rank = 0;
    public ?string $feed_link = null;
}