<?php

namespace facade\response\theatre\challenge;

class TheatreChallengePrizeResp
{
    public ?string $prize_name = null;
    public ?string $prize_image = null;
    public ?string $prize_price = null;
    public ?string $prize_link = null;
}