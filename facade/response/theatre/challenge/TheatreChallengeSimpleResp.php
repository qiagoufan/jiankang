<?php

namespace facade\response\theatre\challenge;

class TheatreChallengeSimpleResp
{
    public ?string $challenge_image = null;
    public ?int $challenge_id = 0;
    public ?string $challenge_name = null;
    public ?string $challenge_icon = null;
}