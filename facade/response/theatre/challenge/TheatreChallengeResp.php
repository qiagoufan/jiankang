<?php

namespace facade\response\theatre\challenge;

class TheatreChallengeResp
{
    public ?string $challenge_image = null;
    public ?int $challenge_id = 0;
    public ?string $challenge_name = null;
    public ?string $challenge_group_icon = null;
    public ?string $challenge_group_link = null;
    public ?string $challenge_status_str = null;
    public int $challenge_status = 0;
    public ?string $challenge_time_range = null;
    public ?array $prize_list = null;
    public ?string $challenge_desc = null;
    public ?int $join_num = null;
    public ?array $user_list = null;
}