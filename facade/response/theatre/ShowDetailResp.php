<?php

namespace facade\response\theatre;

use facade\response\theatre\detail\DailyPictorialShowResp;
use facade\response\theatre\detail\ShowBaseInfoResp;
use facade\response\theatre\detail\NewArrivalProductShowResp;
use facade\response\theatre\detail\SuperProductShowResp;
use facade\response\theatre\detail\ActivityNoticeShowResp;
use facade\response\theatre\detail\UpcomingTipsShowResp;

class ShowDetailResp
{
    public ?ShowBaseInfoResp $show_base_info = null;
    public ?DailyPictorialShowResp $daily_pictorial = null;
    public ?SuperProductShowResp $super_product = null;
    public ?ActivityNoticeShowResp $activity_notice = null;
    public ?NewArrivalProductShowResp $new_arrival_product = null;
    public ?UpcomingTipsShowResp $upcoming_tips = null;
}