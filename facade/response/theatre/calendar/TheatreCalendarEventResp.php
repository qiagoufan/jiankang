<?php


namespace facade\response\theatre\calendar;


class TheatreCalendarEventResp
{
    public string $event_image;
    public string $event_title;
    public string $event_desc;
    public string $event_link;
}