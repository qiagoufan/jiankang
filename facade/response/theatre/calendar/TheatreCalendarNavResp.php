<?php

namespace facade\response\theatre\calendar;

class TheatreCalendarNavResp
{
    public const TYPE_YEAR = 1;
    public const TYPE_MONTH = 2;

    public ?string  $display = null;
    public ?int  $type = null;
    public ?string  $value = null;
    public ?string  $bg_str = null;
}