<?php


namespace facade\response\theatre\calendar;



class TheatreCalendarNavListResp
{
    public ?array  $calendar_nav_list = null;
}