<?php

namespace facade\response\theatre\calendar;

class TheatreCalendarDateNavResp
{
    public ?string  $day = null;
    public ?array  $list = null;
}