<?php


namespace facade\response\theatre\calendar;


class TheatreCalendarEventListResp
{
    public ?array  $calendar_event_list = null;
}