<?php

namespace facade\response\theatre\detail;

class NewArrivalProductShowResp
{
    public string $top_img;
    public int $sku_id = 0;
    public int $type = 0;
    public int $is_super = 0;
    public string $sku_img = '';
    public string $sku_name = '';
    public string $subtitle = '';
    public string $price = '';
    public string $usp = '';
    public string $highlight_content = '';
    public string $content = '';
}