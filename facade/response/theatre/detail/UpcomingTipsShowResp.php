<?php

namespace facade\response\theatre\detail;

class UpcomingTipsShowResp
{

    public ?string $top_img = null;
    public int $type;
    public string $background;
    public string $content;
    public ?string $link = null;
    public string $year;
    public string $month;
    public string $date;
    public string $holiday;
}