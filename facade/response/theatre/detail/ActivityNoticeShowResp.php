<?php

namespace facade\response\theatre\detail;

class ActivityNoticeShowResp
{

    public ?string $top_img = null;
    public ?int $type = 0;
    public ?string $background = null;
    public ?string $link = null;
    public ?string $link_content = null;
}