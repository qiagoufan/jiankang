<?php

namespace facade\response\theatre\detail;

class DailyPictorialShowResp
{
    public string $top_img;
    public int $type;
    public string $background;
    public string $date;
    public string $holiday;
    public string $year;
    public string $month;
    public ?string $content = null;
}