<?php

namespace facade\response\theatre\detail;

use facade\response\theatre\detail\feed\SimpleFeedResp;

class SuperProductShowResp extends NewArrivalProductShowResp
{
    public ?SimpleFeedResp $simple_feed = null;
    public ?string  $background = null;
}