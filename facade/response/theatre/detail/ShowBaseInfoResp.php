<?php

namespace facade\response\theatre\detail;

class ShowBaseInfoResp
{
    public int $show_id;
    public int $show_type;
    public ?string $date;
}