<?php


namespace facade\response\theatre\detail\feed;


class AuthorResp
{
    public int $user_id;
    public int $user_type = 1;
    public string $nick_name;
    public ?string $avatar;

}