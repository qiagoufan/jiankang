<?php


namespace facade\response\theatre\detail\feed;

class CommentResp
{
    public $comment_id;
    public $comment_user_id;
    public $comment_user_name;
    public $comment_user_avatar;
    public $comment_content;
    public $comment_image_list;
}