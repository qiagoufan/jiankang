<?php

namespace facade\response\theatre\detail\feed;

class SimpleFeedResp
{
    public $feed_id;
    public $content;
    public ?AuthorResp $author;
    public ?string $main_image;
    public $link;
    public ?array $comment_list;
}