<?php

namespace facade\response\theatre;

use facade\response\base\PageInfoResp;

class TheatreIndexFeedListResp
{
    public ?array $feed_list = null;
    public ?PageInfoResp $page_info = null;
}