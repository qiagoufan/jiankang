<?php

namespace facade\response\repair;

use facade\response\repair\order\RepairCustomerInfoResp;
use facade\response\repair\order\RepairEngineerResp;
use facade\response\repair\order\RepairExtraInfoResp;
use facade\response\repair\order\RepairOrderBaseResp;
use facade\response\repair\order\RepairOrderDetailResp;

class RepairOrderInfoResp
{
    public ?RepairOrderBaseResp $base_info = null;

    public ?RepairEngineerResp $engineer_info = null;

    public ?RepairCustomerInfoResp $customer_info = null;


    public ?RepairOrderDetailResp $order_detail = null;

    public ?RepairExtraInfoResp $extra_info = null;

}