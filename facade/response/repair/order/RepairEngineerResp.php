<?php

namespace facade\response\repair\order;

class RepairEngineerResp
{
    //工程师id
    public ?int $engineer_id = null;

    //工程师姓名
    public $engineer_name;

    //工程师联系方式
    public $contact_info;

}