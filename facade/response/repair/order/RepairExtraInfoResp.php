<?php

namespace facade\response\repair\order;

class RepairExtraInfoResp
{

    //维修说明
    public ?string $repair_instructions = null;
    //维修票据
    public ?array $repair_bills = null;
    //维修照片
    public ?array $repair_photos = null;


}