<?php

namespace facade\response\repair\order;

class RepairCustomerInfoResp
{
    //店长手机号
    public $store_manager_phone;
    //店长姓名
    public $store_manager_name;
    //公司id
    public ?int  $company_id = null;

    public ?string $company_name = null;
    //店铺id


    public ?int $store_id = null;

    //店铺编号
    public ?string $store_num = null;
    //店铺地址
    public ?string $store_address = null;

    public ?string $store_name = null;
    public ?string $store_province = null;
    public ?string $store_city = null;
    public ?string $store_area = null;

}