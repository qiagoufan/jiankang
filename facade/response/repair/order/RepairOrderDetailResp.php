<?php

namespace facade\response\repair\order;

class RepairOrderDetailResp
{

    //预约时间
    public ?int $appointment_timestamp = null;

    //客户备注
    public $customer_remark;
    //图片
    public $images;

    //联系人
    public $customer_name;
    //联系电话
    public $customer_phone_num;
    //维修地址
    public $customer_address;

}