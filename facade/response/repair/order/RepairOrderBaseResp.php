<?php

namespace facade\response\repair\order;

class RepairOrderBaseResp
{
    public ?int $id = null;
    //订单版本
    public ?int  $order_version = 0;
    //订单状态
    public ?int  $order_status = 0;
    //客户支付状态
    public ?int $pay_status = null;
    //客户开票状态
    public ?int $invoicing_status = null;
    //订单分类id
    public ?int $order_category_id = null;
    //订单分类名称
    public $order_category_name;
    //工程师提现状态
    public ?int $cash_out_status = null;

    //维修价格
    public ?string $customer_price = null;
    //工程师收入
    public ?string $engineer_reward = null;

    public ?int $created_timestamp = null;
    //更新时间
    public ?int $updated_timestamp = null;

}