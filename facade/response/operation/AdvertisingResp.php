<?php


namespace facade\response\operation;

class AdvertisingResp
{
    public $id;
    public $title;
    public $link;
    public $link_type;
    public $image;
    public $type;
    public $space_id;
    public $sort;

}