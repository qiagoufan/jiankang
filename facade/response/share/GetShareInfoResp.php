<?php


namespace facade\response\share;


class GetShareInfoResp
{
    public ?string $share_pic;
    public ?string $share_desc;
    public ?string $share_link;
    public ?string $share_title;

}