<?php

namespace facade\response\activity\hbxj;

class HbxjAwardResp
{
    public $award_title;
    public $award_image;
    public $award_type;
    public $user_award_id;
    public $receive_status;
    public $get_time;
    public $express_info;
}