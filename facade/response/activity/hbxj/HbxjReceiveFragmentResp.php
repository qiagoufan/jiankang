<?php

namespace facade\response\activity\hbxj;

class HbxjReceiveFragmentResp
{
    /**
     * @var int  领取的碎片号
     */
    public int $fragment_num;
    public ?HbxjAwardResp $award_info = null;
}