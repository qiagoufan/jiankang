<?php

namespace facade\response\logistics;


class LogisticsDetailResp
{
    public $record_list = null;
    public $state = 0;
    public $express_no;
    public $express_company_name;
    public $receiver;
    public $phone;
    public $province;
    public $city;
    public $area;
    public $address;
}