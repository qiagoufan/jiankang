<?php

namespace facade\response\logistics;


class LogisticsRecordResp
{
    public $time;
    public $ftime;
    public $context;
}