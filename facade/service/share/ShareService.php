<?php

namespace facade\service\share;

use facade\request\share\GetShareInfoParams;
use facade\response\Result;

interface ShareService
{

    public function getShareInfoToThird(GetShareInfoParams $getShareInfoParams): ?Result;

}