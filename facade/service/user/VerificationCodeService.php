<?php

namespace facade\service\user;


use facade\request\user\VerificationCodeParams;
use facade\response\Result;

interface VerificationCodeService
{

    public function sendCode(VerificationCodeParams &$verificationCodeParams): Result;

    public function verifyCode(VerificationCodeParams &$verificationCodeParams): Result;

    public function deleteCode(string $phone, string $code): Result;

}