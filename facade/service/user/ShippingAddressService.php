<?php


namespace facade\service\user;


use facade\request\user\ShippingAddressParams;
use facade\response\Result;

interface ShippingAddressService
{
    public function getUserShippingAddressList(int $userId): ?Result;

    public function getShippingAddressDetail(int $userId, int $addressId): ?Result;

    public function createShippingAddress(ShippingAddressParams $shippingAddressParams): ?Result;

    public function updateShippingAddress(ShippingAddressParams $shippingAddressParams): ?Result;

    public function deleteShippingAddress(int $userId, int $addressId): ?Result;
}