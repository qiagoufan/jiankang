<?php


namespace facade\service\user;

use facade\request\engineer\QueryEngineerListParams;
use facade\response\Result;
use model\user\dto\UserEngineerDTO;


interface UserEngineerService
{
    public function queryEngineerInfoByPhone(?string $phoneNum): Result;

    public function getEngineerInfo(?int $engineerId): Result;

    public function updateEngineerInfo(UserEngineerDTO $userEngineerDTO): Result;

    public function queryEngineerList(QueryEngineerListParams $params ): Result;

    public function queryEngineerCount(QueryEngineerListParams $params ): Result;

    public function registerEngineer(?string $phoneNum): Result;

    public function searchEngineer(?string $keyword): Result;
}