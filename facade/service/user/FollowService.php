<?php


namespace facade\service\user;

use facade\request\user\FollowParams;
use facade\request\user\GetFollowLeaderboardParams;
use facade\response\Result;

interface FollowService
{

    public function follow(FollowParams $followParams): ?Result;

    public function unFollow(FollowParams $followParams): ?Result;

    public function getUserFocusList(FollowParams $followParams): ?Result;

    public function getFansList(FollowParams $followParams): ?Result;

    public function getFollowDetail(FollowParams $followParams): ?Result;

    public function getFollowStatus(FollowParams $followParams): ?Result;

    public function getFocusNum(FollowParams $followParams): ?Result;

    public function getFansNum(FollowParams $followParams): ?Result;

    public function getRank(FollowParams $followParams): ?Result;

    public function getLeaderboard(GetFollowLeaderboardParams $getFollowLeaderboardParams): ?Result;


}