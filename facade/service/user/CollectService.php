<?php


namespace facade\service\user;


use facade\request\user\UserCollectParams;
use facade\request\user\QueryUserCollectListParams;
use facade\response\Result;

interface CollectService
{

    public function saveUserCollect(UserCollectParams $collectParams): Result;

    public function deleteUserCollect(UserCollectParams $collectParams): Result;

    public function queryUserCollectList(QueryUserCollectListParams $queryUserCollectListParams): Result;

    public function isCollect(UserCollectParams $collectParams): Result;

    public function getCollectDetail(UserCollectParams $collectParams): Result;

}