<?php


namespace facade\service\user;

use facade\request\user\FeedbackParams;
use facade\request\user\LoginByPhoneParams;
use facade\request\user\RegisterParams;
use facade\request\user\UpdateUserInfoParams;
use facade\response\Result;


interface FeedbackService
{
    public function sendFeedback(FeedbackParams $feedbackParams): ?Result;
}