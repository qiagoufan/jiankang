<?php


namespace facade\service\user;

use facade\request\user\LoginByPhoneParams;
use facade\request\user\RegisterParams;
use facade\request\user\UpdateUserInfoParams;
use facade\response\Result;


interface UserExtraService
{
    public function updateUserExtraCount(int $userId, string $field, int $number): bool;

    public function setUserExtra(int $userId, string $field, string $value): bool;

    public function getUserExtra(int $userId, string $field);
}