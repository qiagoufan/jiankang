<?php


namespace facade\service\user;


use facade\response\Result;
use facade\request\user\CreateStarParams;
use facade\request\user\UpdateStarParams;

interface StarService
{

    public function saveStarInfo(CreateStarParams $createStarParams): Result;

    public function updateStarInfo(UpdateStarParams $updateStarParams): Result;

    public function getStarInfo(int $starId): Result;

    public function getStarInfoByName(string $starName): Result;

    public function getStarRank(int $starId): Result;

    public function searchStar(string $keyword, int $index = 0, int $pageSize = 10): Result;

    public function getAllStarList(int $index, int $pageSize, ?string $starName = null): ?Result;

    public function queryStarTotalCount(?string $keyword = null): ?Result;

    public function getLeaderBoardValue(int $userId, int $starId): ?Result;

}