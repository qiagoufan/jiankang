<?php


namespace facade\service\user;

use facade\request\user\LoginByPhoneParams;
use facade\request\user\LoginBySocialiteParams;
use facade\request\user\RegisterParams;
use facade\request\user\UpdateUserInfoParams;
use facade\response\Result;
use model\dto\UserDTO;


interface UserServiceBak
{
    public function loginByPhone(LoginByPhoneParams $loginByPhoneParams): Result;

    public function loginBySocialite(LoginBySocialiteParams $loginBySocialiteParams): ?Result;

    public function registerBySocialite(LoginBySocialiteParams $loginBySocialiteParams): ?Result;

    public function register(RegisterParams $registerParams): ?Result;

    public function queryUserIdByPhone(string $phone): Result;

    public function getValidUserInfo(int $userId): ?Result;

    public function bindPhone(int $userId, string $phoneNum): ?Result;


    public function updateUserInfo(UpdateUserInfoParams $updateUserInfoParams): ?Result;

    public function refreshToken(string $refreshToken, string $deviceId): ?Result;

    public function queryUserIdBySocialite(string $openId, string $provider): ?Result;

    public function logout(int $userId): ?Result;

    public function bindUserSocialite(int $userId, string $openId, string $provider): Result;

    public function querySocialiteBindStatus(int $userId, string $provider): Result;

    public function registerMajiaUser(RegisterParams $registerParams, int $userId): Result;

    public function getMaxMajiaUserId(): Result;

    public function cancelUser(int $userId): Result;

    public function checkUpdateNickName(UserDTO &$userDTO): Result;

    public function queryUserIdByNickName(string $nickName): Result;

    public function formatValidSocialiteNickName(string $nickName): Result;

    public function getMajiaUserId(): Result;

    public function blockUser(int $userId, int $blockUserId): Result;

    public function unblockUser(int $userId, int $blockUserId): Result;

    public function isBlock(int $userId, int $blockUserId): Result;

    public function getBlockUserIdList(int $userId): Result;

    public function bindPhoneCheck(int $userId): Result;

    public function queryBlackUserById(int $userId): ?Result;

    public function queryWhiteUserById(int $userId): ?Result;

    public function queryBlackUserList(int $index, int $pageSize): ?Result;

    public function getRegisterUserCount(): ?Result;

}