<?php


namespace facade\service\user;

use facade\request\user\LoginByPhoneParams;
use facade\request\user\RegisterParams;
use facade\request\user\UpdateUserInfoParams;
use facade\request\user\UpdateUserPushTokenParams;
use facade\response\Result;


interface UserPushService
{
    public function updateUserPushToken(UpdateUserPushTokenParams $userPushTokenParams): Result;

}