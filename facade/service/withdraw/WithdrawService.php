<?php


namespace facade\service\withdraw;

use facade\request\withdraw\QueryWithdrawListParams;
use model\withdraw\dto\WithdrawRecordDTO;


interface WithdrawService
{
    public function createWithDrawRecord(WithdrawRecordDTO $withdrawRecordDTO);

    public function updateWithDrawRecord(WithdrawRecordDTO $withdrawRecordDTO);

    public function queryRecordById(int $withdrawId);

    public function queryEngineerWithdrawRecordList(int $engineerId, int $index, int $pageSize);

    public function queryWithdrawRecordList(QueryWithdrawListParams $params);

    public function queryWithdrawRecordCount(QueryWithdrawListParams $params);

}