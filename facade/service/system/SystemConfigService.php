<?php


namespace facade\service\system;


use facade\request\tag\CreateTagCategoryParams;
use facade\request\tag\CreateTagEntityParams;
use facade\request\tag\CreateTagParams;
use facade\request\tag\DeleteTagEntityParams;
use facade\request\tag\DeleteTagParams;
use facade\request\tag\QueryFullSiteTagEntityListParams;
use facade\request\tag\QueryTagEntityListParams;
use facade\request\tag\TagEntityParams;
use facade\request\tag\TagParams;
use facade\response\Result;

interface SystemConfigService
{

    public function getConfigList(): Result;

    public function updateConfig(string $key, string $value): Result;

    public function setConfig(string $key, string $value): Result;


    public function getConfigValue(string $key): Result;

}