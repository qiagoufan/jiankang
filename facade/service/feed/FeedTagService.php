<?php


namespace facade\service\feed;

use facade\request\feed\CreateFeedParams;
use facade\request\feed\GetFeedListParams;
use facade\request\feed\GetHashTagListParams;
use facade\request\feed\heat\TriggerEventParams;
use facade\request\feed\news\CreateFeedInformationParams;
use facade\response\Result;

interface FeedTagService
{


    public function getHashTagCategoryList(string $scene): ?Result;

    public function getHashTagList(GetHashTagListParams $getFeedTagListParams): ?Result;

}