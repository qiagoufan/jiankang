<?php


namespace facade\service\feed;

use facade\request\feed\CreateFeedParams;
use facade\request\feed\GetFeedListParams;
use facade\response\Result;

interface FeedService
{

    public function createFeed(CreateFeedParams $createFeedParams): ?Result;

    public function getFeedDetail(int $feedId): ?Result;

    public function getFeedDetailRespCache(int $feedId): ?Result;

    public function setFeedDetailRespCache(int $feedId, string $field, string $value): ?Result;

    public function getFeedList(GetFeedListParams $getFeedListParams): ?Result;

    public function getIndexFeedList(GetFeedListParams $getFeedListParams): ?Result;

    public function getIndexFeedIdList(GetFeedListParams $getFeedListParams): ?Result;

    public function getUserPostFeedList(int $userId, int $index, int $pageSize): ?Result;

    public function updateUserReceiveFeedList(int $userId): ?Result;

    public function getUserPostCount(int $userId): ?Result;

    public function getUserReceiveFeedList(int $userId, int $index, int $pageSize): ?Result;

    public function deleteFeed(int $feedId, int $userId = 0): ?Result;

    public function updateIdolMaintainLeaderboardScore(int $feedId, array $starIdList, string $recordType): ?Result;

    public function deleteStarFeedTag(int $feedId, array $starIdList): ?Result;


    public function getFeatureFeedIdListByTemplateType(int $index, int $templateType, int $pageSize): ?Result;
}