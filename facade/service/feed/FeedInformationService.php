<?php


namespace facade\service\feed;

use facade\request\feed\news\CreateFeedInformationParams;
use facade\request\PageParams;
use facade\response\Result;

interface FeedInformationService
{

    public function createFeedInformation(CreateFeedInformationParams $createFeedInformationParams): ?Result;

    public function getFeedInformationDetail(int $id): ?Result;

    public function getFeedInformationList(PageParams $pageParams): ?Result;

}