<?php


namespace facade\service\feed;

use facade\request\feed\CreateFeedParams;
use facade\request\feed\GetFeedListParams;
use facade\request\feed\heat\TriggerEventParams;
use facade\request\feed\news\CreateFeedInformationParams;
use facade\response\Result;
use model\feed\dto\FeedOutsiteLinkDTO;

interface FeedOutsiteService
{

    public function insert(FeedOutsiteLinkDTO $feedOutsiteLinkDTO): ?Result;

    public function getOutsiteInfoById(int $linkId): ?Result;

    public function getOutsiteInfoByLink(string $link): ?Result;

    public function updateRecord(FeedOutsiteLinkDTO $feedOutsiteLinkDTO): ?Result;

}