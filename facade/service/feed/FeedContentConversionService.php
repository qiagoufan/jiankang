<?php


namespace facade\service\feed;

use facade\request\feed\CreateFeedParams;
use facade\request\feed\GetFeedListParams;
use facade\response\Result;

interface FeedContentConversionService
{

    public function conversionContent(string $content): ?Result;

}