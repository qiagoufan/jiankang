<?php

namespace facade\service\feed;

use facade\request\feed\ConversionFeedParams;
use facade\response\feed\FeedInfoResp;
use model\feed\dto\FeedInfoDTO;

interface FeedInfoRespService
{
    public function conversionFeedInfoResp(FeedInfoDTO &$feedInfoDTO, ?ConversionFeedParams &$conversionFeedParams = null): ?FeedInfoResp;

}