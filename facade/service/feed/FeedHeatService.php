<?php


namespace facade\service\feed;

use facade\request\feed\CreateFeedParams;
use facade\request\feed\GetFeedListParams;
use facade\request\feed\heat\TriggerEventParams;
use facade\request\feed\news\CreateFeedInformationParams;
use facade\response\Result;

interface FeedHeatService
{


    /**
     * feed触发事件
     * @param TriggerEventParams $triggerEventParams
     * @return Result|null
     */
    public function triggerEvent(TriggerEventParams $triggerEventParams): ?Result;



    /**
     * 获取热度值
     * @param int $feedId
     * @return Result|null
     */
    public function getHeatValue(int $feedId): ?Result;

}