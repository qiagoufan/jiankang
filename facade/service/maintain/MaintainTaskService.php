<?php

namespace facade\service\maintain;

use facade\request\maintain\CreateMaintainStarTaskParams;
use facade\request\maintain\CreateMaintainTaskParams;
use facade\request\maintain\CreateMaintainUserTaskRecordParams;
use facade\request\maintain\GetUserTaskHistoryForStarParams;
use facade\response\Result;

interface MaintainTaskService
{

    public function insertTask(CreateMaintainTaskParams $createMaintainTaskParams): ?Result;


    public function insertStarTask(CreateMaintainStarTaskParams $createMaintainStarTaskParams): ?Result;

    public function insertUserTaskRecord(CreateMaintainUserTaskRecordParams $createUserTaskRecordParams): ?Result;

    public function queryUserTaskRecord(int $userId, int $starId, int $taskId): ?Result;

    public function queryTaskListByStarId(int $starId): ?Result;

    public function queryTaskInfo(int $taskId): ?Result;


    public function queryStarTotalValueByTaskId(int $taskId, int $starId): ?Result;


    public function queryUserValueByTaskId(int $taskId, int $starId, ?int $userId): ?Result;

    public function queryUserListByStarId(int $starId): ?Result;


    public function getMaintainTaskRank(int $starId, string $rankType): Result;

    public function getMaintainTaskValue(int $starId, string $rankType): Result;

    public function getUserTaskTotalValue(int $userId): ?Result;

    public function getUserTaskHistoryForStar(GetUserTaskHistoryForStarParams $getUserTaskHistoryForStarParams): ?Result;

    public function queryTotalValueForStar(GetUserTaskHistoryForStarParams $getUserTaskHistoryForStarParams): ?Result;


}

