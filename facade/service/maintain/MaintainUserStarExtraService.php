<?php

namespace facade\service\maintain;


use facade\response\Result;

interface MaintainUserStarExtraService
{
    public function queryRecord(int $userId, int $starId): ?Result;
}

