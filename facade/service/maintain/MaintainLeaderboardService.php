<?php

namespace facade\service\maintain;

use facade\request\maintain\CreateLeaderboardRecordParams;
use facade\request\maintain\UpdateStarContributionParams;
use facade\response\Result;


interface MaintainLeaderboardService
{

    public function createRecord(CreateLeaderboardRecordParams $createLeaderboardRecordParams): Result;

    public function getStarPopularityRank(int $starId): Result;

    public function getStarPopularityValue(int $starId): Result;

    public function getUserStarPopularityValueByType(int $starId, int $userId, array $recordTypeList): Result;

    public function getStarPopularityLeaderboard(int $index, int $pageSize): Result;

    public function queryRecord(int $userId, int $starId): ?Result;

    public function updateStarContribution(UpdateStarContributionParams $updateStarContributionParams): Result;

    public function getStarContributionLeaderboard(int $starId, int $index, int $pageSize): Result;

    public function getStarContributionTotalValue(int $starId): Result;

    public function getStarContributionUserRank(int $starId, int $userId): Result;

    public function getStarContributionUserScore(int $starId, int $userId): Result;


}

