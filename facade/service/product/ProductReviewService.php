<?php

namespace facade\service\product;

use facade\request\admin\SearchProductReviewListParams;
use facade\request\product\GetProductReviewListParams;
use facade\request\product\GetRelatedReviewParams;
use facade\request\product\SendProductReviewParams;
use facade\response\Result;

interface ProductReviewService
{

    public function sendReview(SendProductReviewParams $sendProductReviewParams): ?Result;

    public function getReviewInfo(int $reviewId): ?Result;

    public function getProductReviewList(?GetProductReviewListParams $getProductReviewListParams): ?Result;

    public function getReviewCount(int $itemId): ?Result;

    public function getRelatedReview(GetRelatedReviewParams $getRelatedReviewParams): ?Result;

    public function getProductAdminReviewList(SearchProductReviewListParams $searchUserListParams): ?Result;

    public function queryProductReviewListCount(): ?Result;

}