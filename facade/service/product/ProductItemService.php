<?php

namespace facade\service\product;


use facade\request\product\GetProductDetailParams;
use facade\request\product\GetProductItemListParams;
use facade\request\product\SelectProductSpecificParams;
use facade\response\Result;
use model\dto\ProductSkuDTO;

interface ProductItemService
{

    public function getProductItemDetail(GetProductDetailParams $productSkuDetailParams): ?Result;

    public function getProductList(GetProductItemListParams $getProductItemListParams): ?Result;

}