<?php

namespace facade\service\product;


use facade\response\Result;
use model\dto\ProductSkuDTO;
use model\product\dto\ProductExtraInfoDTO;

interface ProductExtraInfoService
{

    public function getProductExtraInfo(int $itemId): ?Result;

    public function insertProductExtraInfo(ProductExtraInfoDTO $productExtraInfoDTO): ?Result;

}