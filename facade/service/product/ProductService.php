<?php

namespace facade\service\product;


use facade\request\product\GetProductItemListParams;
use facade\request\product\GetProductDetailParams;
use facade\request\product\SelectProductSpecificParams;
use facade\response\Result;
use model\product\dto\ProductSkuDTO;

interface ProductService
{

    public function getProductItemList(GetProductItemListParams $getProductItemListParams): ?Result;

    public function getProductDetail(GetProductDetailParams $getProductDetailParams): ?Result;

    public function getProductItem(?int $itemId): ?Result;

    public function getProductGalleryPic(int $skuId): ?Result;

    public function getProductSkuDetail(int $skuId): ?Result;

    public function selectProductSpecific(SelectProductSpecificParams $selectProductSpecificParams): ?Result;

    public function getProductSkuPrice(ProductSkuDTO $productSkuDTO): ?Result;

    public function getProductPriceBySkuId(int $skuId): ?Result;

    public function getProductSkuPriceRange(ProductSkuDTO $productSkuDTO): ?Result;

    public function clearProductDetailCache(int $skuId): ?Result;

    public function getDistinctProductSkuList(int $offset, int $pageSize): ?Result;

    public function getIndexPageProductItemList(int $index): ?Result;

}