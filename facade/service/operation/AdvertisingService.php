<?php


namespace facade\service\operation;

use facade\response\Result;
use model\operation\dto\OperationAdvertisingDTO;

interface AdvertisingService
{
    public function getAdvertisingListBySpace(int $spaceId): ?Result;

    public function getAdvertisingDetail(int $advertising_id): ?Result;

    public function createAdvertising(OperationAdvertisingDTO $advertisingDTO): ?Result;

    public function updateAdvertising(OperationAdvertisingDTO $advertisingDTO): ?Result;

    public function deleteAdvertising(int $advertising_id): ?Result;


}