<?php

namespace facade\service\sign;


use facade\request\sign\EditSignTypeResp;
use facade\response\Result;


interface SignService
{

    public function createSignType(EditSignTypeResp $editSignParams): ?Result;

    public function updateSignType(EditSignTypeResp $editSignParams): ?Result;

    public function getSignTypeDetail(int $signTypeId): ?Result;

    public function deleteSignType(int $signTypeId): ?Result;

    public function createSignTask(EditSignTypeResp $editSignParams): ?Result;

    public function updateSignTask(EditSignTypeResp $editSignParams): ?Result;

    public function getSignTaskDetail(int $signTaskId): ?Result;

    public function deleteSignTask(int $signTaskId): ?Result;

    public function getSignTaskList(int $signTypeId): ?Result;

    public function insertSignTaskRecord(int $signTaskId, int $userId, int $taskValue): ?Result;

    public function queryUserSignTaskRecord(int $signTaskId, int $userId, int $date): ?Result;

    public function queryUserSignHistoryByMonth(int $signTaskId, int $userId, int $yearMonth): ?Result;
}