<?php


namespace facade\service\invoice;

use facade\request\invoice\ApplyForInvoiceParams;
use facade\request\invoice\QueryInvoiceListParams;
use facade\response\Result;

interface InvoiceService
{
    //申请开票
    public function applyForInvoice(ApplyForInvoiceParams $applyForInvoiceParams): ?Result;

    //查看开票列表
    public function queryInvoiceList(QueryInvoiceListParams $queryInvoiceListParams): ?Result;


    //查看开票列表
    public function queryInvoiceCount(QueryInvoiceListParams $queryInvoiceListParams): ?Result;

    //查看开票详情
    public function queryInvoiceInfo(int $invoiceId): ?Result;

    //查看开票订单列表
    public function queryInvoiceOrderList(int $invoiceId): ?Result;

    //完成开票
    public function finishInvoice(int $invoiceId): ?Result;


}