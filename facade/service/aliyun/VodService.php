<?php

namespace facade\service\aliyun;

use facade\request\aliyun\media\CreateUploadImageParams;
use facade\request\aliyun\media\CreateUploadVideoParams;
use facade\request\aliyun\media\RefreshUploadVideoParams;
use facade\response\Result;

interface VodService
{
    public function createUploadVideo(CreateUploadVideoParams $createUploadVideoParams): ?Result;

    public function refreshUploadVideo(RefreshUploadVideoParams $refreshUploadVideoParams): ?Result;

    public function createUploadImage(CreateUploadImageParams $createUploadImageParams): ?Result;

    public function getVideoInfo(?string $videoId): ?Result;

    public function getPlayInfo(?string $videoId): ?Result;

    public function getImageInfo(?string $imageId): ?Result;



}