<?php

namespace facade\service\aliyun;

use facade\request\aliyun\media\CreateUploadImageParams;
use facade\request\aliyun\media\CreateUploadVideoParams;
use facade\request\aliyun\media\RefreshUploadVideoParams;
use facade\response\Result;

interface AcsService
{
    public function checkContent(string $content): ?Result;

    public function checkImage(array $imageUrlList): ?Result;

    public function checkVideo(): ?Result;


}