<?php

namespace facade\service\aliyun;

use facade\request\aliyun\oss\UploadFileParams;
use facade\response\Result;

interface OssService
{
    public function uploadOssFile(UploadFileParams $uploadOssFileParams): ?Result;

    public function getOssResp(): ?Result;

    public function uploadWebImage(?string $fileUrl): ?Result;

    public function uploadBase64Image(?string $imageBase64Str): ?Result;
}