<?php

namespace facade\service\aliyun;

use facade\request\aliyun\media\CreateUploadImageParams;
use facade\request\aliyun\media\CreateUploadVideoParams;
use facade\request\aliyun\media\RefreshUploadVideoParams;
use facade\response\Result;

interface MediaService
{
    public function createUploadVideo(CreateUploadVideoParams $createUploadVideoParams): ?Result;
    public function refreshUploadVideo(RefreshUploadVideoParams $refreshUploadVideoParams): ?Result;
    public function createUploadImage(CreateUploadImageParams $createUploadImageParams): ?Result;
}