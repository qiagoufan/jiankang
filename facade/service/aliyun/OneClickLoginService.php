<?php

namespace facade\service\aliyun;

use facade\response\Result;

interface OneClickLoginService
{

    public function getPhone(string $accessToken): ?Result;
}