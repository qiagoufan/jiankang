<?php

namespace facade\service\aliyun;

use facade\request\aliyun\SendSmsParams;
use facade\response\Result;

interface SmsService
{
    public function sendSms(SendSmsParams $sendSmsParams): ?Result;
}