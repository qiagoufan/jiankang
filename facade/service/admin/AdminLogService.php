<?php

namespace facade\service\admin;

use model\dto\AdminLogDTO;

interface AdminLogService
{

    public function insertAdminLog(AdminLogDTO $adminLogDTO);
}