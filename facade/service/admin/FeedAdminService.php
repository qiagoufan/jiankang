<?php

namespace facade\service\admin;

use facade\response\Result;
use facade\request\admin\GetAllFeedListParams;

interface FeedAdminService
{

    public function getAllFeedList(GetAllFeedListParams $getAllFeedListParams): ?Result;

    public function queryFeedListCount(GetAllFeedListParams $getAllFeedListParams): ?Result;
}