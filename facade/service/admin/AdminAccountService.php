<?php

namespace facade\service\admin;

use facade\response\Result;

interface AdminAccountService
{

    public function queryAdminByAccount(string $account): ?Result;

    public function queryAdminByPhone(string $phone): ?Result;


    public function adminAccountLogin(string $account, string $password): Result;
}