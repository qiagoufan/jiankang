<?php

namespace facade\service\admin;

use facade\request\admin\QueryAllOrderListParams;
use facade\response\Result;

interface OrderAdminService
{
    public function getOrderList(QueryAllOrderListParams $queryOrderListParams): ?Result;

    public function deliveryOrder(int $orderId, string $expressCompany, string $expressOrderId): ?Result;

    public function getOrderProductByOrderId(int $orderId): ?Result;

    public function queryOrderListCount(QueryAllOrderListParams $queryOrderListParams): ?Result;


}