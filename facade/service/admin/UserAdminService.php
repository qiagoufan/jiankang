<?php


namespace facade\service\admin;

use facade\request\admin\SearchUserListParams;
use facade\response\Result;

interface UserAdminService
{

    public function getUserList(SearchUserListParams $searchUserListParams): ?Result;

    public function getUserTotalCount(): ?Result;
}