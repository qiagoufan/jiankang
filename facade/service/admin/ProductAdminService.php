<?php

namespace facade\service\admin;

use facade\request\admin\EditProductItemParams;
use facade\request\product\GetProductItemListParams;
use facade\response\Result;

interface ProductAdminService
{

    public function createProductItem(EditProductItemParams $editProductItemParams): ?Result;

    public function getProductItemList(GetProductItemListParams $getProductItemListParams): ?Result;

    public function queryItemCount(GetProductItemListParams $productItemListTypeParams): ?Result;

    public function updateProductInfo(EditProductItemParams $editProductItemParams): ?Result;

    public function updateProductItemPublishStatus(int $itemId, int $status): ?Result;

    public function updateProductStock(int $itemId, ?int $stock = null, ?int $sale = null): ?Result;

    public function deleteProduct(int $itemId): ?Result;
}