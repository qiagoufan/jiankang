<?php

namespace facade\service\alipay;

use facade\response\Result;
use facade\request\order\CreateOrderPayInfoParams;

interface AlipayService
{
    public function createAliPayOrder(int $pay_type, string $product_name, int $total_price, int $order_no): ?Result;

    public function queryAliPayResult($order_sn, $payment_trade_no): ?Result;
}