<?php


namespace facade\service\logistics;


use facade\response\Result;
use model\dto\OrderInfoDTO;

interface LogisticsService
{
    public function getOrderLogisticsDetail(OrderInfoDTO $orderInfoDTO): ?Result;

    public function getLogisticsStatusStr(string $expressCompany, string $expressNo): ?Result;

    public function getLogisticsDetail(string $expressCompany, string $expressNo): ?Result;


}