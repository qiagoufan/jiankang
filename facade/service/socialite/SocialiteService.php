<?php

namespace facade\service\socialite;

use facade\request\socialite\SocialiteParams;
use facade\response\Result;

interface SocialiteService
{
    public function getOpenId(SocialiteParams $socialiteParams): ?Result;

    public function getWxMiniSessionKey(string $openid): ?Result;

    public function getWxMiniSessionData(string $code): ?Result;

    public function queryUserOpenid(string $provider, int $userId): ?Result;


}