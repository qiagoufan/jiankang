<?php


namespace facade\service\pick;

use facade\response\Result;

interface IdolPickService
{

    public function getIdolRank(): ?Result;

    public function getIdolStarInfo(int $idolStarId): ?Result;

    public function searchIdolStar(string $idolStarName): ?Result;

    public function getIdolStarDongtaiList(int $idolStarId): ?Result;

    public function getIdolStarFeedList(int $idolStarId): ?Result;

}