<?php


namespace facade\service\tag;


use facade\request\tag\CreateTagCategoryParams;
use facade\request\tag\CreateTagEntityParams;
use facade\request\tag\CreateTagParams;
use facade\request\tag\DeleteTagEntityParams;
use facade\request\tag\DeleteTagParams;
use facade\request\tag\QueryFullSiteTagEntityListParams;
use facade\request\tag\QueryTagEntityListParams;
use facade\request\tag\TagEntityParams;
use facade\request\tag\TagParams;
use facade\response\Result;

interface TagService
{

    public function createTagIfNotExists(CreateTagParams $tagParams): Result;

    public function createTagEntity(CreateTagEntityParams $createTagEntityParams): Result;

    public function deleteTag(DeleteTagParams $deleteTagParams): Result;

    public function deleteTagEntity(TagEntityParams $tagEntityParams): Result;

    public function queryTagId(TagParams $tagParams): Result;

    public function queryTagEntityList(QueryTagEntityListParams $queryTagEntityListParams): Result;

    public function queryFullSiteTagEntityDTOList(QueryFullSiteTagEntityListParams $queryFullSiteTagEntityListParams): Result;

    public function hasTagEntity(TagEntityParams $tagEntityParams): Result;

    public function getTagEntityCount(TagEntityParams $tagEntityParams): Result;

    public function cleanTagEntity(TagEntityParams $tagEntityParams): Result;

}