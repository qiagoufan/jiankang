<?php


namespace facade\service\tag;


use facade\request\tag\CreateHotTagParams;
use facade\request\tag\QueryHotTagListParams;
use facade\response\Result;

interface HotTagService
{

    public function createTagIfNotExists(CreateHotTagParams $tagParams): Result;

    public function deleteHotTag(int $hotTagId): ?Result;

    public function getHotTagList(QueryHotTagListParams $queryHotTagListParams): ?Result;

}