<?php


namespace facade\service\message;
use facade\request\PageParams;
use facade\response\Result;

interface MessageService
{
    public function getAllMessageList(PageParams $pageParams, int $userId): ?Result;


    public function getUserUnreadMessage(?int $userId): ?Result;
}