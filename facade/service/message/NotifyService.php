<?php


namespace facade\service\message;

use facade\request\message\NotifySwitchParams;
use facade\response\Result;

interface NotifyService
{


    public function getNotifyConfig(?string $deviceId): ?Result;

    public function updateNotifySwitch(?NotifySwitchParams $notifySwitchParams): ?Result;
}