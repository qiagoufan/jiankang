<?php


namespace facade\service\brand;

use facade\response\Result;

interface ProductBrandService
{
    public function getBrandInfo(int $brandId): Result;


}