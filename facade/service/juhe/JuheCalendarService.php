<?php

namespace facade\service\juhe;

use facade\request\aliyun\media\CreateUploadImageParams;
use facade\request\aliyun\media\CreateUploadVideoParams;
use facade\request\aliyun\media\RefreshUploadVideoParams;
use facade\response\Result;

interface JuheCalendarService
{
    public function getDateInfo(string $date): ?Result;

}