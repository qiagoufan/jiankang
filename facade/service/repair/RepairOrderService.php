<?php

namespace facade\service\repair;

use facade\request\repair\CreateRepairOrderParams;
use facade\request\repair\QueryRepairOrderListParams;
use facade\response\Result;
use model\repair\dto\RepairOrderExtraInfoDTO;
use model\repair\dto\RepairOrderInfoDTO;


interface RepairOrderService
{

    public function createRepairOrder(CreateRepairOrderParams $createRepairOrderParams): ?Result;

    public function queryRepairOrderList(QueryRepairOrderListParams $params): ?Result;

    public function queryRepairOrderCount(QueryRepairOrderListParams $params): ?Result;

    public function distributeRepairOrder(int $repairOrderId, int $engineerId): ?Result;

    public function getRepairOrder(int $repairOrderId): ?Result;

    public function updateRepairOrder(RepairOrderInfoDTO $repairOrderInfoDTO): ?Result;

    public function getRepairOrderExtraInfo(int $repairOrderId): ?Result;

    public function queryWithdrawAbleAmount(int $engineerId): ?Result;


    public function updateRepairOrderExtraInfo(RepairOrderExtraInfoDTO $repairOrderExtraInfoDTO): ?Result;

}