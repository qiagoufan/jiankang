<?php

namespace facade\service\promotion;


use facade\response\Result;

interface PromotionVoucherService
{

    public function distributeVoucher(int $userId, int $voucherId): ?Result;

    public function useVoucher(string $voucherSn, int $orderId): ?Result;

    public function getVoucherInfo(int $voucherId): ?Result;

    public function queryUserVoucherEntityByVoucherId(int $userId, int $voucherId): ?Result;

    public function getVoucherEntityBySn(string $voucherSn): ?Result;

    public function queryUserVoucherList(?int $userId = 0): ?Result;

}