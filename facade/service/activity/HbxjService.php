<?php

namespace facade\service\activity;

use facade\request\activity\hbxj\FillDeliveryAddressParams;
use facade\response\Result;

interface HbxjService
{

    /**
     * 领取碎片
     * @param string $code
     * @param int $userId
     * @return Result|null
     */
    public function receiveFragment(string $code, int $userId): ?Result;

    public function getUserAwardList(int $userId): ?Result;

    public function fillDeliveryAddress(int $userId, FillDeliveryAddressParams $params): ?Result;

    public function getUserFragmentCount(int $userId): ?Result;

    public function lockUser(int $userId): ?Result;

    public function unlockUser(int $userId): ?Result;

    public function deliveryAward(int $userAwardId, string $expressCompany, string $expressNum): ?Result;

    public function getAwardList(int $index): ?Result;

    public function updateUserWithdraw(int $userId, int $userAwardId, string $orderNum, string $paymentNum): ?Result;

    public function getAwardTemplateList(): ?Result;

    public function getUserAwardDetail(int $userAwardId): ?Result;
}