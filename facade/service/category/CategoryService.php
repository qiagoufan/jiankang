<?php


namespace facade\service\category;

use facade\request\category\EditCategoryParams;
use facade\response\Result;

interface CategoryService
{
    public function createCategory(EditCategoryParams $editCategoryParams): ?Result;

    public function queryCategoryList(): ?Result;

    public function queryCategoryById($cateId): ?Result;

    public function queryCategoryByNameAndLevel(string $categoryName, int $level): ?Result;


}