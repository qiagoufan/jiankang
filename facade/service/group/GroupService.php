<?php

namespace facade\service\group;

use facade\request\feed\group\ApplyManagerParams;
use facade\response\Result;
use model\group\dto\GroupManageViolationRecordDTO;

interface GroupService
{
    public function applyManager(ApplyManagerParams $applyManagerParams): ?Result;

    public function queryUserManagerType(int $userId, ?int $starId = 0): ?Result;

    public function queryUserApplyHistory(int $userId): ?Result;

    public function queryManagerApplyFormList(int $starId): ?Result;

    public function queryManagerList(int $starId): ?Result;

    public function queryApplyForm(int $applyFormId): ?Result;

    public function updateApplyFormStatus(int $applyFormId, int $applyFormStatus): ?Result;

    public function queryManagerNum(int $starId, int $managerType): ?Result;

    public function createGroupManager(int $userId, int $starId, int $managerType): ?Result;

    public function deleteGroupManager(int $userId, int $starId): ?Result;


    public function cleanUserGroupManager(int $userId): ?Result;

    public function createViolationRecord(GroupManageViolationRecordDTO $groupManageViolationRecordDTO): ?Result;


}