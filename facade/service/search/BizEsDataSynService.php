<?php


namespace facade\service\search;

use facade\request\feed\search\EsDataSynParams;
use facade\response\Result;

/**
 * 业务ES数据同步服务
 * Interface BizEsDataSynchronizeService
 * @package facade\service\search
 */
interface BizEsDataSynService
{
    public function setBizRecord(EsDataSynParams $dataSynchronizeParams): ?Result;

    public function delBizRecord(EsDataSynParams $dataSynchronizeParams): ?Result;


}