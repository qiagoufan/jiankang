<?php


namespace facade\service\search;

use facade\request\feed\search\DeleteEsDocumentParams;
use facade\request\feed\search\WriteEsDocumentParams;
use facade\request\feed\SearchParams;
use facade\response\Result;

interface SearchService
{
    public function getRecommendWordList(SearchParams $searchParams): ?Result;

    public function getSearchDTOList(SearchParams $searchParams): ?Result;

    public function getStarAdminDTOlist(SearchParams $searchParams): ?Result;

    public function getProductSkuAdminList(SearchParams $searchParams): ?Result;

    public function createEsDoc(WriteEsDocumentParams $writeEsDocumentParams);

    public function updateEsDoc(WriteEsDocumentParams $createEsDocumentParams);

    public function deleteEsDoc(DeleteEsDocumentParams $deleteEsDocumentParams);


}