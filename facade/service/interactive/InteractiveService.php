<?php

namespace facade\service\interactive;


use facade\request\interactive\CreateCommentParams;
use facade\request\interactive\CreateReplyParams;
use facade\request\interactive\QueryInteractiveCommentListParams;
use facade\response\Result;

interface InteractiveService
{

    public function createComment(int $userId, CreateCommentParams $SendCommentParams): ?Result;

    public function delComment($userId, $commentId, $bizId, $isAdmin = -1): ?Result;

    public function createReply(int $userId, CreateReplyParams $createReplyParams): ?Result;

    public function queryInteractiveCommentList(QueryInteractiveCommentListParams $queryInteractiveCommentListParams): ?Result;

    public function delReply($userId, $replyId, $isAdmin = -1): ?Result;


    public function queryCommentDetailById(int $commentId): ?Result;

    public function queryReplyCountByComment(int $commentId, int $bizId, string $bizType): ?Result;

    public function deleteReplyByComment(int $bizId, string $bizType, int $commentId): ?Result;

    public function queryInteractiveOverview(string $bizType, int $bizId): ?Result;

    public function queryCommentTotalCount(int $bizId, string $bizType): ?Result;

    public function updateOverviewCount(int $bizId, string $bizType, string $numType, int $num): ?Result;

    public function getCommentInfoByUserId(int $userId): ?Result;

    public function getReplyInfoByUserId(int $userId): ?Result;

}