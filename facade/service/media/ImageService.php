<?php


namespace facade\service\media;

use facade\request\media\MediaImageParams;
use facade\response\Result;

interface ImageService
{

    /**
     * 创建短视频
     * @param MediaImageParams $mediaImageParams
     * @return Result|null
     */
    public function createImage(MediaImageParams $mediaImageParams): ?Result;


    /**
     * 根据id
     * @param int|null $id
     * @return Result|null
     */
    public function getImageInfoById(?int $id): ?Result;

    public function getImageInfo(MediaImageParams $mediaImageParams): ?Result;


}