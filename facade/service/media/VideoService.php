<?php


namespace facade\service\media;

use facade\request\media\MediaVideoParams;
use facade\response\Result;

interface VideoService
{

    /**
     * 创建短视频记录
     * @param MediaVideoParams $createVideoParams
     * @return Result|null
     */
    public function createVideo(MediaVideoParams $createVideoParams): ?Result;

    public function getVideoInfo(MediaVideoParams $mediaVideoParams): ?Result;

    public function getVideoInfoById(int $id): ?Result;

    public function updateVideoInfo(string $videoId, int $checkStatus): ?Result;


}