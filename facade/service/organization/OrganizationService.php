<?php


namespace facade\service\organization;

use facade\request\organization\EditOrganizationCompanyParams;
use facade\request\organization\EditOrganizationStoreParams;
use facade\request\organization\QueryOrganizationCompanyParams;
use facade\request\organization\QueryOrganizationStoreParams;
use facade\response\Result;

interface OrganizationService
{
    public function createCompany(EditOrganizationCompanyParams $params): ?Result;

    public function getCompanyInfo(int $companyId): ?Result;

    public function getCompanyList(QueryOrganizationCompanyParams $params): ?Result;

    public function getCompanyCount(QueryOrganizationCompanyParams $params): ?Result;

    public function updateCompanyInfo(EditOrganizationCompanyParams $params): ?Result;

    public function deleteCompany(int $companyId): ?Result;

    public function reactivateCompany(int $companyId): ?Result;

    public function createCompanyStore(EditOrganizationStoreParams $params): ?Result;

    public function updateCompanyStoreManager(?int $storeId, ?array $managerInfoList): ?Result;

    public function getStoreInfo(int $storeId): ?Result;

    public function queryStoreInfoByName(int $companyId, string $storeNum, string $storeName): ?Result;

    public function queryStoreList(QueryOrganizationStoreParams $queryParams): ?Result;

    public function queryStoreCount(QueryOrganizationStoreParams $queryParams): ?Result;

    public function updateStoreInfo(EditOrganizationStoreParams $params): ?Result;

    public function deleteStore(int $storeId): ?Result;

    public function queryStoreManagerByPhone(string $phoneNum): ?Result;

    public function queryStoreManagerListByPhoneNum(string $phoneNum): ?Result;
}