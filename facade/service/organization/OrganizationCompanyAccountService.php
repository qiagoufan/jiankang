<?php


namespace facade\service\organization;

use facade\request\organization\EditOrganizationCompanyParams;
use facade\request\organization\EditOrganizationStoreParams;
use facade\request\organization\QueryOrganizationCompanyParams;
use facade\request\organization\QueryOrganizationStoreParams;
use facade\response\Result;

interface OrganizationCompanyAccountService
{
    public function LoginByAccountName(?string $accountName, ?string $password): ?Result;

    public function queryCompanyByAccountName(?string $accountName): ?Result;
}