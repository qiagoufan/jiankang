<?php


namespace facade\service\wechat;

use facade\response\Result;


interface WechatService
{
    public function createJsBizPackage(int $totalFee, int $outTradeNo, string $orderName, int $timestamp);

    public function createJsAPIBizPackage(int $totalFee, int $outTradeNo, string $orderName, int $timestamp, string $code);

    public function createMiniBizPackage(int $totalFee, int $outTradeNo, string $orderName, int $timestamp, string $code);


}