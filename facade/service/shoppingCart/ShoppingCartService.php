<?php

namespace facade\service\shoppingCart;


use facade\request\shoppingCart\UpdateCartParams;
use facade\request\shoppingCart\RemoveFromCartParams;
use facade\response\Result;


interface ShoppingCartService
{

    public function updateCart(UpdateCartParams $addToCartParams): ?Result;

    public function removeFromCart(int $userId, array $itemIdList): ?Result;

    public function getUserCart(int $userId): ?Result;
}