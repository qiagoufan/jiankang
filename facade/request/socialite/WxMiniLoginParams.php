<?php


namespace facade\request\socialite;


class WxMiniLoginParams extends SocialiteParams
{
    public $encryptedData;
    public $iv;
}