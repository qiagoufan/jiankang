<?php


namespace facade\request\socialite;

use facade\request\RequestParams;

class SocialiteParams extends RequestParams
{
    public $access_token;
    public $open_id;
    public $code;
    public $provider='wxMini';
    public $invite_code;


    public $name;
    public $avatar;
    public $sex = 1;
}