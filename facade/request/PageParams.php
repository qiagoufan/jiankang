<?php


namespace facade\request;

class PageParams extends RequestParams
{
    public $index = 0;
    public $page_size = 20;

}