<?php


namespace facade\request;

class RequestParams
{
    public $user_id = 0;
    public $device_id;
    public $platform;
    public $app_version;
    public $api_version;

}