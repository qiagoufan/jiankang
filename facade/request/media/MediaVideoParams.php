<?php


namespace facade\request\media;

use facade\request\RequestParams;

class MediaVideoParams extends RequestParams
{
    public ?string $video_id = null;
}