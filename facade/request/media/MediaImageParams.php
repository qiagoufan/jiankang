<?php


namespace facade\request\media;

use facade\request\RequestParams;

class MediaImageParams extends RequestParams
{
    public $image_id;
}