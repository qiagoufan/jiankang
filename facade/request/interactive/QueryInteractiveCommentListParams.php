<?php


namespace facade\request\interactive;


use facade\request\RequestParams;

class QueryInteractiveCommentListParams extends RequestParams
{
    public  $biz_id = 0;
    public  $biz_type = 'feed';
    public  $index = 0;
    public  $page_size = 10;

}