<?php


namespace facade\request\interactive;


use facade\request\RequestParams;

class QueryInteractiveReplyListParams extends RequestParams
{
    public  $comment_id = 0;
    public  $index = 0;
    public  $page_size = 10;
}