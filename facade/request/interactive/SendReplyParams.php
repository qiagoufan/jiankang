<?php


namespace facade\request\interactive;

use facade\request\RequestParams;

class SendReplyParams extends RequestParams
{
    public  $reply_user_id;
    public  $comment_id;
    public  $biz_type = 'feed';
    public  $biz_id;
    public  $reply_to_user_id = null;
    public  $reply_to_id = null;
    public  $reply_content;

}