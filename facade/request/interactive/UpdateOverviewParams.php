<?php


namespace facade\request\interactive;

use facade\request\RequestParams;

class UpdateOverviewParams extends RequestParams
{
    public $biz_type = 'feed';
    public $biz_id;
    public $comment_count;
    public $reply_count;
    public $created_timestamp;
    public $updated_timestamp;

}