<?php


namespace facade\request\interactive;

use facade\request\RequestParams;

class CreateCommentParams extends RequestParams
{
    public $biz_type = 'feed';
    public $biz_id = null;
    public $content_text = null;


}