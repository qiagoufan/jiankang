<?php


namespace facade\request\interactive;

use facade\request\RequestParams;

class CreateReplyParams extends RequestParams
{
    public $biz_type = 'feed';
    public $biz_id;
    public $comment_id = 0;
    public $reply_user_id = null;
    public $reply_to_user_id = null;
    public $reply_to_id = null;
    public $reply_content;


}