<?php


namespace facade\request\theatre;


use facade\request\RequestParams;

class GetTheatreIndexFeedListParams extends RequestParams
{
    public int $index = 0;


}