<?php
namespace facade\request\theatre;

use facade\request\RequestParams;

class GetShowListParams extends RequestParams
{
    public int $index = 0;

}