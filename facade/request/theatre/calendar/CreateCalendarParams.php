<?php


namespace facade\request\theatre\calendar;


use facade\request\RequestParams;

class CreateCalendarParams extends RequestParams
{

    public $id;
    public $event_timestamp;
    public $event_date;
    public $event_biz_type;
    public $event_biz_id;
    public $event_extra_info;
    public $sort_num;

}