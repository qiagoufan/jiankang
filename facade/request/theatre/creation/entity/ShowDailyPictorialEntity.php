<?php


namespace facade\request\theatre\creation\entity;


class ShowDailyPictorialEntity extends ShowBaseEntity
{
    public ?string $background;
}