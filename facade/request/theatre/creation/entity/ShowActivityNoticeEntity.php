<?php


namespace facade\request\theatre\creation\entity;


class ShowActivityNoticeEntity extends ShowBaseEntity
{
    public ?string $background;
}