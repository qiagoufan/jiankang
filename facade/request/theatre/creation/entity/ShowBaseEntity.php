<?php

namespace facade\request\theatre\creation\entity;

class ShowBaseEntity
{

    const ENTITY_TYPE_PRODUCT = 'product';
    const ENTITY_TYPE_SUPER_PRODUCT = 'super_product';
    const ENTITY_TYPE_DAILY_PICTORIAL = 'daily_pictorial';
    const ENTITY_TYPE_ACTIVITY_NOTICE = 'activity_notice';

    public ?int $entity_id;
    public ?array $entity_id_list;
    public ?string $entity_type;
    public int $index;
    public ?string $entity_link = null;
    public ?string $entity_extra_info;
}