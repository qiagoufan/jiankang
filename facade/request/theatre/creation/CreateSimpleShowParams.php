<?php


namespace facade\request\theatre\creation;


use facade\request\RequestParams;

class CreateSimpleShowParams extends RequestParams
{
    public $id;
    public $show_type;
    public $sku_id;
    public $link;
    public $background;
    public $holiday;
    public $show_date;
    public $content_desc;

}