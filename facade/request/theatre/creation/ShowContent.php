<?php

namespace facade\request\theatre\creation;

use facade\request\theatre\creation\entity\ShowBaseEntity;

class ShowContent
{
    public ?string $content_desc = null;
    public ?string $holiday = null;
    public ?int $show_type = 0;
    public ?string $show_extra_info;
    public ?ShowBaseEntity $show_entity;
}