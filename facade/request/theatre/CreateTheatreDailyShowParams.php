<?php


namespace facade\request\theatre;


use facade\request\RequestParams;

class CreateTheatreDailyShowParams extends RequestParams
{
    public int $show_type = 0;
    public int $sort = 0;
    public string $show_content;
    public string $show_date;

}