<?php

namespace facade\request\repair;


class  CreateRepairOrderParams
{

    public ?int $store_id = null;

    public ?string $unique_id = null;

    public ?int $login_type = null;
    //预约时间
    public ?int $appointment_timestamp = 0;
    //订单分类
    public ?int $order_category_id = null;
    //客户备注
    public ?string $customer_remark = null;
    //联系人
    public ?string $customer_name = null;
    //联系电话
    public ?string $customer_phone_num = null;
    //维修地址
    public ?string $customer_address = null;
    //图片
    public $images;


}