<?php

namespace facade\request\repair;


class  SetRepairOrderPriceParams
{

    public ?string $engineer_reward = null;
    public ?string $customer_price = null;
    public ?int $repair_order_id = 0;


}