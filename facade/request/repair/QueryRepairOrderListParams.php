<?php

namespace facade\request\repair;


class  QueryRepairOrderListParams
{

    public ?int $index = 0;
    public ?int $page_size = 10;

    public ?int $engineer_id = null;
    public ?int $repair_order_id = null;
    public ?array $repair_order_id_list = null;
    public ?int $order_category_id = null;
    public ?int $cash_out_status = null;
    public ?int $order_status = null;
    public ?int $invoicing_status = null;
    public $order_status_list = null;
    public ?int $company_id = null;
    public ?int $store_id = null;
    public ?array $store_id_list = null;
    public ?int $created_timestamp = null;
    public ?int $appointment_timestamp = null;


    //用于搜索条件
    public ?string $engineer_phone = null;
    public ?string $engineer_real_name = null;
    public ?string $store_province = null;
    public ?string $store_city = null;
    public ?string $store_area = null;
    public ?string $store_num = null;
    public ?string $store_name = null;
}