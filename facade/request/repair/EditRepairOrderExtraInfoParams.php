<?php

namespace facade\request\repair;


class  EditRepairOrderExtraInfoParams
{

    public ?int $repair_order_id = null;
    //维修说明
    public ?string $repair_instructions = null;
    //维修照片
    public $repair_photos = null;
    //维修票据
    public $repair_bills = null;
}