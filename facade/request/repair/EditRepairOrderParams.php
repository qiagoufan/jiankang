<?php

namespace facade\request\repair;


class  EditRepairOrderParams
{

    public ?int $repair_order_id = null;
    //预约时间
    public ?int $appointment_timestamp = null;
    //订单分类
    public ?int $order_category_id = null;
    //客户备注
    public ?string $customer_remark = null;
    //联系人
    public ?string $customer_name = null;
    //联系电话
    public ?string $customer_phone_num = null;
    //维修地址
    public ?string $customer_address = null;

    public ?int $order_status = null;

    public ?int $invoicing_status = null;

}