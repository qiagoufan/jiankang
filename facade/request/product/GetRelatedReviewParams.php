<?php


namespace facade\request\product;


use facade\request\RequestParams;

class GetRelatedReviewParams extends RequestParams
{
    public $sku_id;
    public $item_id;

}