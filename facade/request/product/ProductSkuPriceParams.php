<?php


namespace facade\request\product;
use facade\request\RequestParams;

class ProductSkuPriceParams extends RequestParams
{
    public ?int $skuId;

}