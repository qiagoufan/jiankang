<?php


namespace facade\request\product;


use facade\request\RequestParams;

class GetProductDetailParams extends RequestParams
{

    public $sku_id = 0;
    public $item_id = 0;


}