<?php


namespace facade\request\product;

use facade\request\RequestParams;


class SelectProductSpecificParams extends RequestParams
{
    public ?int $item_id = null;
    public ?string $indexes = null;
}