<?php


namespace facade\request\product;

use facade\request\RequestParams;


class CountProductSkuTotalPriceParams extends RequestParams
{
    public ?int $sku_id = null;
    public ?int $count = null;
}