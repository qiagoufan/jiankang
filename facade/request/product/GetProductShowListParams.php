<?php


namespace facade\request\product;


use facade\request\RequestParams;

class GetProductShowListParams extends RequestParams
{

    public ?int $sku_id = null;
    public ?int $index = 0;
    public ?int $page_size = 10;


}