<?php


namespace facade\request\product;


use facade\request\RequestParams;

class SendProductReviewParams extends RequestParams
{
    public string $content;
    public $score = 5;
    public ?int $sku_id = 0;
    public $order_id;
    public $image_list;
    public $video_list;
}