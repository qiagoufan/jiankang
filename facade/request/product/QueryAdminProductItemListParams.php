<?php


namespace facade\request\product;


use facade\request\PageParams;

class QueryAdminProductItemListParams extends PageParams
{
    public $index = 0;
    public $publish_status = 0;

    public $product_category_id = null;
    public $keyword = null;
    public $sort_detail = null;

}