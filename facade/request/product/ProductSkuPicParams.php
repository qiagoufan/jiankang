<?php


namespace facade\request\product;


use facade\request\RequestParams;

class ProductSkuPicParams extends RequestParams
{
    public ?int $skuId;


}