<?php


namespace facade\request\product;


use facade\request\PageParams;

class GetProductItemListParams extends PageParams
{
    public $index = 0;
    public $product_type = 0;

    public $product_category_id = null;
    public $keyword = null;
    public $sort_detail = null;

}