<?php


namespace facade\request\product;


use facade\request\RequestParams;

class GetProductReviewListParams extends RequestParams
{
    public $sku_id;
    public $item_id;
    public $review_id;
    public $index = 0;
    public $page_size;

}