<?php

namespace facade\request\organization;

use facade\request\RequestParams;

class EditOrganizationCompanyParams extends RequestParams
{

    //
    public $id;
    //
    public $company_name;
    //
    public $company_license;

    public $company_license_pic;
    //
    public $service_start_timestamp;
    //
    public $service_end_timestamp;
    //
    public $contact_person;
    //
    public $contact_info;
    //
    public $emergency_contact_person;
    //
    public $emergency_contact_info;

    public $account_name;

    public $account_password;

}