<?php

namespace facade\request\organization;

use facade\request\RequestParams;

class QueryOrganizationStoreParams extends RequestParams
{
    public $company_id = 0;
    public $index = 0;
    public $page_size;
    public $store_name;
    public $store_num;
    public $store_province;
    public $store_city;
    public $store_address;
    public $store_level;
}