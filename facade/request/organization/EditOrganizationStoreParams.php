<?php

namespace facade\request\organization;

use facade\request\RequestParams;

class EditOrganizationStoreParams extends RequestParams
{
    //
    public $id;
    //
    public $company_id;
    //
    public $store_name;
    //
    public $store_address;
    //
    public $store_province;
    //
    public $store_city;
    //
    public $store_area;
    //
    public $store_street;
    //
    public $store_num;

    public $store_manager_list;
    //
    public $store_level;
}