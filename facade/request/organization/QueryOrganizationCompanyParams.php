<?php

namespace facade\request\organization;

use facade\request\RequestParams;

class QueryOrganizationCompanyParams extends RequestParams
{
    public $company_status = 1;
    public $company_license;
    public $company_name;
    public $service_start_timestamp;
    public $service_end_timestamp;
    public $index = 0;
    public $pageSize = 20;
}