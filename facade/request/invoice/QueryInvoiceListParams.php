<?php

namespace facade\request\invoice;


class QueryInvoiceListParams
{

    public ?int $index = 0;
    public ?int $page_size = 10;
    public ?int $company_id = null;
    public ?int $invoicing_status = null;
}