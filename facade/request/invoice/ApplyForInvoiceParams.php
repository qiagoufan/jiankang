<?php

namespace facade\request\invoice;


class ApplyForInvoiceParams
{

    public ?int $company_id = null;

    public ?array $repair_order_id_list = null;
}