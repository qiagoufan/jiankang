<?php

namespace facade\request\order;

use facade\request\RequestParams;


class CalculateOrderPriceParams extends RequestParams
{
    /**
     * 商品列表
     * @var array|null
     */
    public $product_list = null;
    /**
     * 优惠码
     * @var string|null
     */
    public $voucher_sn = null;

    public $voucher_sn_list = [];

    public $need_chose_voucher = 1;

}