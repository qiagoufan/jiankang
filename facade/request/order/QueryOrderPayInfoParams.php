<?php

namespace facade\request\order;

use facade\request\RequestParams;


class QueryOrderPayInfoParams extends RequestParams
{

    public $order_sn;
    public $payment_trade_no;
    public $pay_type;
}