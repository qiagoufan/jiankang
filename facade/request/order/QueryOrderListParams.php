<?php

namespace facade\request\order;

use facade\request\RequestParams;


class QueryOrderListParams extends RequestParams
{


//status:
//1.待付款2.待支付3.已支付4.待发货 5.待收货6.交易完成7.交易关闭8.待评价9.

    /** @var int 待付款 */
    const  ORDER_STATUS_TO_PAY = 10;
    /** @var int 已支付 */
    const ORDER_STATUS_HAS_PAY = 20;
    /** @var int 待发货 */
    const ORDER_STATUS_WAIT_SHIPPING = 30;
    /** @var int 已发货 */
    const ORDER_STATUS_SHIPPED = 40;
    /** @var int 完成 */
    const ORDER_STATUS_FINISHED = 50;
    /** @var int 交易关闭 */
    const ORDER_STATUS_CLOSED = 60;
    /** @var int 待评价 */
    const ORDER_STATUS_WAIT_COMMENT = 70;
    /** @var int 已评价 */
    const ORDER_STATUS_HAS_COMMENT = 80;


//type:
//1.待付款2.待发货3.待收货4.待评价 5.全部订单

    /** @var int 待付款 */
    const  ORDER_TYPE_TO_PAY = 1;
    /** @var int 待发货 */
    const  ORDER_TYPE_TO_SEND = 2;
    /** @var int 待收货 */
    const  ORDER_TYPE_TO_RECEIVE = 3;
    /** @var int 待评价 */
    const  ORDER_TYPE_FINISH = 4;
    /** @var int 全部订单 */
    const  ORDER_TYPE_ALL = 5;

    public $order_type = 5;

    public $index = 0;
    public $page_size = 10;

}