<?php


namespace facade\request\order;


use facade\request\RequestParams;

class CreateOrderPayInfoParams extends RequestParams
{
    public $order_sn;
    public $pay_type;
    public $code;


}