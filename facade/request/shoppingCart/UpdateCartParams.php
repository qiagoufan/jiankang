<?php

namespace facade\request\shoppingCart;

use facade\request\RequestParams;


class UpdateCartParams extends RequestParams
{

    public $item_id = 0;
    public $sku_id = 0;
    public $count = 1;

}