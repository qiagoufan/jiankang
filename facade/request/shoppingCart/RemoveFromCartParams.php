<?php

namespace facade\request\shoppingCart;

use facade\request\RequestParams;


class RemoveFromCartParams extends RequestParams
{

    public ?array $item_id_list = [];

}