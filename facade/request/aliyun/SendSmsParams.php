<?php


namespace facade\request\aliyun;


class SendSmsParams
{

    const TEMPLATE_CODE_LOGIN = 'SMS_187540703';

    public $phone;
    public $outId;
    public $templateCode;
    public $templateParam;
}