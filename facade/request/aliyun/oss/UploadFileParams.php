<?php


namespace facade\request\aliyun\oss;


class UploadFileParams
{
    public $file_path;
    public $object_name;
    public $is_cdn;
}