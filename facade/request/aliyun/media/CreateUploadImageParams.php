<?php


namespace facade\request\aliyun\media;


class CreateUploadImageParams
{
    public $image_type = 'default';
    public $image_ext;
}