<?php


namespace facade\request\aliyun\media;


class CreateUploadVideoParams
{
    public $title = '';
    public $file_name;
    public $file_size;
    public $cover_url;
    public $media_scene;
}