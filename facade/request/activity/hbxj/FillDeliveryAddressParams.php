<?php

namespace facade\request\activity\hbxj;

use facade\request\RequestParams;

class FillDeliveryAddressParams extends RequestParams
{
    public $user_award_id;
    public $address;
    public $phone_num;
    public $province;
    public $city;
    public $area;
    public $receiver;
}