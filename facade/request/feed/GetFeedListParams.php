<?php


namespace facade\request\feed;


use facade\request\PageParams;

class GetFeedListParams extends PageParams
{


    public $keyword = null;
    public $feed_category_id = 0;
    public $scene = 'index';
    public $feed_status;
    public $is_publish;
}