<?php


namespace facade\request\feed\search;


class WriteEsDocumentParams
{
    public $index;
    public $type;
    public $id;
    public $body;
}