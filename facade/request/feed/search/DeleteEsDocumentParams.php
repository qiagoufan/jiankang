<?php


namespace facade\request\feed\search;


class DeleteEsDocumentParams
{
    public $index;
    public $type;
    public $id;
}