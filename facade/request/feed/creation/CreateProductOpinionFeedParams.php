<?php


namespace facade\request\feed\creation;


use facade\request\RequestParams;

class CreateProductOpinionFeedParams extends RequestParams
{
    public int $sku_id = 0;
    public ?array $image_list = null;
    public $content;
    public ?string $video_id = null;
}