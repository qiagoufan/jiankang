<?php


namespace facade\request\feed\creation;


use facade\request\RequestParams;

class CreateAdminMaterialFeedParams extends RequestParams
{

    public ?array $image_list = null;
    public $content;
    public ?string $video_id = null;
    public $tag_list;
    public ?bool $is_fake = true;
}