<?php


namespace facade\request\feed\creation;


use facade\request\RequestParams;

class CreateJkFeedParams extends RequestParams
{
    public $image_list = null;
    public $video_link = null;
    public $feed_content = '';
    public $feed_title = '';
    public $feed_category_id;

}