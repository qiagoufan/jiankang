<?php


namespace facade\request\feed\creation;

class FeedContent
{

    public $content_desc;
    public $content_title;
    public $content_type = null;
    public $feed_extra_info;
    public $image_list;
    public $video_id;
    public $video_link;
    public $content_tag_list = null;
}