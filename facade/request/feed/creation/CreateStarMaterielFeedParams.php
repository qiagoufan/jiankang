<?php


namespace facade\request\feed\creation;


use facade\request\RequestParams;

class CreateStarMaterielFeedParams extends RequestParams
{
    public int $star_id = 0;
    public ?array $image_list = null;
    public $content;
    public ?string $video_id = null;
}