<?php


namespace facade\request\feed\creation\entity;
class FeedBaseEntity
{

    const ENTITY_TYPE_VIDEO = 'video';
    const ENTITY_TYPE_PRODUCT = 'product';
    const ENTITY_TYPE_REVIEW = 'review';
    const ENTITY_TYPE_INFORMATION = 'information';
    const ENTITY_TYPE_LEADERBOARD = 'leaderboard';
    const ENTITY_TYPE_STAR = 'star';
    const ENTITY_TYPE_IMAGE = 'image';
    const ENTITY_TYPE_SUBFEED = 'subfeed';

    public ?int $entity_id;
    public ?array $entity_id_list = null;
    public string $entity_type;
    public int $index = 0;
    public ?string $entity_link = null;
    public ?string $entity_extra_info;
    public ?string $icon = null;
}