<?php


namespace facade\request\feed\creation\entity;
class FeedImageEntity extends FeedBaseEntity
{
    public ?string $image = null;
    public ?string $original_image = null;
    public ?int $width = 0;
    public ?int $height = 0;
    public ?string $content = null;
}