<?php


namespace facade\request\feed\creation\entity;


class FeedVideoEntity extends FeedBaseEntity
{
    public ?int $video_width = 0;
    public ?int $video_height = 0;
    public ?int $video_duration = 0;
    public ?string $cover_url = '';
}