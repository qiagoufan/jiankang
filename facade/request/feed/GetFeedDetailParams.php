<?php


namespace facade\request\feed;


use facade\request\RequestParams;

class GetFeedDetailParams extends RequestParams
{
    public $feed_id = 0;
    public $scene = 'detail';
}