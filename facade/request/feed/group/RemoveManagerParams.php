<?php

namespace facade\request\feed\group;

use facade\request\RequestParams;

class RemoveManagerParams extends RequestParams
{
    public ?int $mananger_user_id = null;
    public ?int $star_id = null;
    public ?int $reason_id = null;
    public ?string $additional_desc = null;

}
