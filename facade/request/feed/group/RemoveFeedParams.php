<?php

namespace facade\request\feed\group;

use facade\request\RequestParams;

class RemoveFeedParams extends RequestParams
{
    public ?string $feed_id = null;
    public ?int $star_id = null;
    public ?int $reason_id = 1;
    public ?string $additional_desc = null;

}
