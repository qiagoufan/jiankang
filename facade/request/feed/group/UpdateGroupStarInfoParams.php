<?php

namespace facade\request\feed\group;

use facade\request\RequestParams;

class UpdateGroupStarInfoParams extends RequestParams
{
    public ?string $avatar = null;
    public ?string $base_introduce = null;
    public ?string $description = null;
    public ?int $star_id = null;

}
