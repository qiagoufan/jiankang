<?php

namespace facade\request\feed\group;

use facade\request\RequestParams;

class ApplyManagerParams extends RequestParams
{
    public ?string $contact = null;
    public ?int $star_id = null;
    public ?string $star_name = null;
    public ?string $apply_desc = null;
    public ?int $form_type = 1;

}
