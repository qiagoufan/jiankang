<?php


namespace facade\request\feed;


use facade\request\RequestParams;

class ConversionFeedParams extends RequestParams
{
    public $scene = null;
    public $star_rank_type = null;
    public $star_id = null;

}