<?php


namespace facade\request\feed;


use facade\request\RequestParams;

class GetHashTagListParams extends RequestParams
{

    public $hash_tag_type;
    public $index = 0;

}