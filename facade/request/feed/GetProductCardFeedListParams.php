<?php


namespace facade\request\feed;


class GetProductCardFeedListParams
{
    public $sku_id = 0;
    public $index = 0;
    public $feed_id = 0;

}