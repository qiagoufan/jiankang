<?php


namespace facade\request\feed;


use facade\request\RequestParams;

class CreateFeedParams extends RequestParams
{
    public $id;
    public $author_id;
    public $feed_title;
    public $cover_url;
    public $feed_type;
    public $content;
    public $template_type;
    public $detail_url;
    public $feed_category_id;

}