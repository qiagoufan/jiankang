<?php


namespace facade\request\feed;


use facade\request\RequestParams;

class CreateFeedCrawlParams extends RequestParams
{

    public $id;
    public $author_id;
    public $author_name;
    public $feed_type;
    public $content;
    public $template_type;
    public $published_timestamp;
    public $created_timestamp;


}