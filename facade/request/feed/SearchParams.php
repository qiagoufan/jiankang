<?php


namespace facade\request\feed;


use facade\request\RequestParams;

class SearchParams extends RequestParams
{
    public int $index = 0;
    public int $page_size = 10;
    public $scene;
    public $keyword;

}