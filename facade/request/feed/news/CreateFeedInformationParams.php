<?php


namespace facade\request\feed\news;


use facade\request\RequestParams;

class CreateFeedInformationParams
{
    public $title;
    public $content;
    public $outsite_link;
    public $main_image;
    public $star_id;
    public $sku_id;
    public $is_original;
    public $author_id;

}