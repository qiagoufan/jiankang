<?php


namespace facade\request\feed\news;


use facade\request\RequestParams;

class GetFeedInformationDetailParams extends RequestParams
{
    public $info_id;

}