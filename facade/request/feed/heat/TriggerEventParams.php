<?php


namespace facade\request\feed\heat;


use facade\request\RequestParams;
use service\feed\impl\FeedHeatServiceImpl;

class TriggerEventParams extends RequestParams
{
    public int $feed_id = 0;
    public string $event_type = '';
    public int $point = 0;
    public string $action_type = FeedHeatServiceImpl::ACTION_TYPE_INCR;

}