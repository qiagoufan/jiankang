<?php


namespace facade\request\feed;


use facade\request\RequestParams;

class GetStarCardFeedListParams extends RequestParams
{
    public $star_id = 0;
    public $index = 0;

}