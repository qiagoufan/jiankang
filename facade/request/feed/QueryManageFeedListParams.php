<?php


namespace facade\request\feed;


use facade\request\PageParams;

class QueryManageFeedListParams extends PageParams
{


    public $keyword = null;
    public $scene = 'index';
    public $feed_status;
}