<?php

namespace facade\request\sign;

use facade\request\RequestParams;


class EditSignTypeParams extends RequestParams
{

    public $id;
    public $sign_type_name;
    public $seq;
    public $sign_type_bg;

}