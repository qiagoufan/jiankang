<?php

namespace facade\request\sign;

use facade\request\RequestParams;


class SignUserTaskParams extends RequestParams
{

    public $sign_task_id;

    public $user_score;

}