<?php

namespace facade\request\sign;

use facade\request\RequestParams;


class EditSignTipsParams extends RequestParams
{

    public $sign_tips_id;
    //打卡区id
    public $sign_in_type_id;
    //内容
    public $content;
    //日期
    public $show_date;
    //图片
    public $image;

}