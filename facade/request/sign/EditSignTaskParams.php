<?php

namespace facade\request\sign;

use facade\request\RequestParams;


class EditSignTaskParams extends RequestParams
{

    public $sign_task_id;
    //打卡id
    public $sign_in_type_id;
    //打卡id
    public $sign_in_task_bg;
    //打卡任务名称
    public $sign_in_task_name;
    //打卡任务说明
    public $sign_in_task_detail;
    //打卡数值
    public $task_value;
    //打卡数值单位
    public $task_value_unit;

}