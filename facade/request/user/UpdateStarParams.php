<?php


namespace facade\request\user;

use facade\request\RequestParams;

class UpdateStarParams extends RequestParams
{
    public $sex;
    public $star_name;
    public $star_id;
    public $official_background;
    public $official_avatar;
    public $official_description;
    public $base_introduce;
    public $ip_type;
    public $owner_user_id = null;
}