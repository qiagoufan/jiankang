<?php


namespace facade\request\user;

use facade\request\RequestParams;

class VerificationCodeParams extends RequestParams
{
    public $phone;
    public $login_type = \Config_Const::LOGIN_TYPE_ENGINEER;
    public $code;
    public $action;

}