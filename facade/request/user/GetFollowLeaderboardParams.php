<?php


namespace facade\request\user;

use facade\request\RequestParams;

class GetFollowLeaderboardParams extends RequestParams
{
    public int $index = 0;
    public int $page_size = 0;
    public ?string $target_type = '';
}