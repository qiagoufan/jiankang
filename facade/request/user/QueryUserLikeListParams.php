<?php


namespace facade\request\user;

use facade\request\RequestParams;

class QueryUserLikeListParams extends RequestParams
{
    public int $index = 0;
    public int $page_size = 10;
    public string $biz_type = 'feed';
}