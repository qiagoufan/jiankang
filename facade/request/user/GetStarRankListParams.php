<?php

namespace facade\request\user;


class GetStarRankListParams
{
    const RANK_TYPE_STAR = 1;
    const RANK_TYPE_BRAND = 2;

    public ?int $index = 0;
    public ?int $type = 1;


}