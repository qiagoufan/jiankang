<?php


namespace facade\request\user;

use facade\request\RequestParams;

class FeedbackParams extends RequestParams
{
    public $contact;
    public $feedback_content;
    public $feedback_type;
    public $feedback_imgs;
}