<?php


namespace facade\request\user;


use facade\request\RequestParams;

class OneClickLoginParams extends RequestParams
{
    public $access_token;
}