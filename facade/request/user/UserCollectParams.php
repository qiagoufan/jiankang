<?php


namespace facade\request\user;

use facade\request\RequestParams;

class UserCollectParams extends RequestParams
{

    public ?int $collect_biz_id = 0;
    public ?string $collect_biz_type = 'feed';
}