<?php


namespace facade\request\user;

use facade\request\PageParams;

class QueryUserListParams extends PageParams
{
    public $keyword = null;
}