<?php


namespace facade\request\user;


class VerificationSocialiteCodeParams extends VerificationCodeParams
{

    public $access_token;
    public $provider;
    public $nick_name;
    public $avatar;
    public $sex = 1;
}