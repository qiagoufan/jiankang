<?php


namespace facade\request\user;

use facade\request\RequestParams;

class LoginByPhoneParams extends RequestParams
{
    public $phone;
    public $ip;
    public $source;
}