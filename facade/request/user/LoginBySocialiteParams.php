<?php


namespace facade\request\user;

use facade\request\RequestParams;

class LoginBySocialiteParams extends RequestParams
{
    public $open_id;
    public $provider;


    public $nick_name;
    public $avatar;
    public $sex = 1;
}