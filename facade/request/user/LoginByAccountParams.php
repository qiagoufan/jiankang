<?php


namespace facade\request\user;

use facade\request\RequestParams;

class LoginByAccountParams extends RequestParams
{
    public $login_account;
    public $password;
    public $source;
}