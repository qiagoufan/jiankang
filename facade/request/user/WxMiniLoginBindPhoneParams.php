<?php


namespace facade\request\user;


use facade\request\RequestParams;

class WxMiniLoginBindPhoneParams extends RequestParams
{
    public $encryptedData;
    public $openid;
    public $iv;

}