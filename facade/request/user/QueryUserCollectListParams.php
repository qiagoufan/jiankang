<?php


namespace facade\request\user;

use facade\request\RequestParams;

class QueryUserCollectListParams extends RequestParams
{
    public int $index = 0;
    public int $page_size = 10;
}