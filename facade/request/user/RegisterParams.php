<?php


namespace facade\request\user;

use facade\request\RequestParams;
use facade\request\socialite\SocialiteParams;
use model\user\dto\UserDTO;
use model\user\dto\UserLoginInfoDTO;

class RegisterParams extends RequestParams
{
    public $user_type;
    public $sex;
    public $real_name;
    public $nick_name;
    public $avatar;
    public $background;
    public $phone;
    public $phone_verified_timestamp;
    public $birthday;
    public $socialite_open_id;
    public $socialite_provider;
    public $status;
    public $location;
    public $created_timestamp;
    public $platform;
    public $device_id;
    public $login_type;
    public $login_account;
    public $login_certificate;
    public $syn_status = 1;

    public static function buildUserDTOByPhone(RequestParams $requestParams, string $phone): RegisterParams
    {
        $registerParams = new self();
        $registerParams->platform = $requestParams->platform;
        $registerParams->created_timestamp = time();
        $registerParams->nick_name = "用户" . uniqid();
        $registerParams->user_type = UserDTO::USER_TYPE_NORMAL;
        $registerParams->status = UserDTO::USER_STATUS_VALID;
        $registerParams->device_id = $requestParams->device_id;
        $registerParams->login_type = \Config_Const::USER_LOGIN_TYPE_PHONE;
        $registerParams->login_account = $phone;
        return $registerParams;
    }

    public static function buildParamsBySocialite(SocialiteParams $requestParams, string $openId, string $provider): RegisterParams
    {
        $registerParams = new self();
        $registerParams->platform = $requestParams->platform;
        $registerParams->socialite_open_id = $openId;
        $registerParams->socialite_provider = $provider;
        $registerParams->created_timestamp = time();
        $registerParams->nick_name = $requestParams->name;
        if ($registerParams->nick_name == null) {
            $registerParams->nick_name = "用户" . uniqid();
            $registerParams->syn_status = 0;
        }
        $registerParams->avatar = $requestParams->avatar;
        if ($registerParams->avatar == null) {
            $registerParams->avatar = 'https://s3.ax1x.com/2021/01/23/s7U5MF.png';
        }
        $registerParams->sex = (int)$requestParams->sex;
        $registerParams->user_type = 1;
        $registerParams->status = 1;
        $registerParams->device_id = $requestParams->device_id;
        $registerParams->login_type = $provider;
        $registerParams->login_account = $requestParams->open_id;
        return $registerParams;
    }


    public static function buildUserDTO(RegisterParams $params): UserDTO
    {
        $userDTO = new UserDTO();
        \FUR_Core::copyProperties($params, $userDTO);

        $userDTO->status = 1;
        $userDTO->user_type = 1;
        $userDTO->created_timestamp = time();
        return $userDTO;
    }

    public static function buildUserLoginDTO(RegisterParams $params): UserLoginInfoDTO
    {
        $userLoginDTO = new UserLoginInfoDTO();

        $userLoginDTO->user_id = $params->user_id;
        $userLoginDTO->login_account = $params->login_account;
        $userLoginDTO->login_type = $params->login_type;
        $userLoginDTO->created_timestamp = time();
        return $userLoginDTO;
    }


}