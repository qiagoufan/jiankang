<?php


namespace facade\request\user;

use facade\request\RequestParams;

class GetUserHomePageListParams extends RequestParams
{

    const POST_FEED = 'post';
    const LIKE_FEED = 'like';

    public ?int $visit_user_id = 0;
    public ?int $index = 0;
    public ?string $visit_type = 'post';
}