<?php


namespace facade\request\user;

use facade\request\RequestParams;

class CreateStarParams extends RequestParams
{
    public $sex;
    public $star_name;
    public $user_id;
    public $official_background;
    public $official_avatar;
    public $official_description;
    public $base_introduce;
    public $ip_type;
}