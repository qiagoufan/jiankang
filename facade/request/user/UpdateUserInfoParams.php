<?php


namespace facade\request\user;

use facade\request\RequestParams;

class UpdateUserInfoParams extends RequestParams
{
    public $sex;
    public $real_name;
    public $name;
    public $avatar;
    public $birthday;
    public $height;
    //身份证号
    public $id_num;
    //体重
    public $weight;
    //婚姻情况
    public $marital_status;
    //职业
    public $profession;
    //文化程度
    public $education;
    //医保类型
    public $insurance_type;

    public $status;
    public $user_remark;

    public $syn_status;
}