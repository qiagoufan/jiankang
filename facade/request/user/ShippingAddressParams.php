<?php


namespace facade\request\user;


class ShippingAddressParams
{
    public $shipping_id;
    public $user_id;
    public $address;
    public $receiver;
    public $province;
    public $city;
    public $area;
    public $is_default;
    public $phone;

}