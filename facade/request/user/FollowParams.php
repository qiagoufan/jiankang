<?php


namespace facade\request\user;

use facade\request\RequestParams;

class FollowParams extends RequestParams
{
    public int $target_id;
    public string $target_type = '';
    public int $index = 0;
}