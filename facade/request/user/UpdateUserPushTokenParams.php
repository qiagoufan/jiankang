<?php


namespace facade\request\user;

use facade\request\RequestParams;

class UpdateUserPushTokenParams extends RequestParams
{
    public $push_token;

}