<?php


namespace facade\request\admin;


use facade\request\RequestParams;

class GetIndexBannerListParams extends RequestParams
{
    public $page_size = 10;
    public $index = 0;

}