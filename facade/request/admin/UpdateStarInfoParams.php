<?php

namespace facade\request\admin;


class UpdateStarInfoParams
{

    public ?string $star_name;
    public ?string $star_desc;


}