<?php


namespace facade\request\admin;


class OperateProductItemParams
{

    const OPERATE_OFFLINE = 0;
    const OPERATE_ONLINE = 1;

    public $item_array;
    public ?int $operate_type = null;


}