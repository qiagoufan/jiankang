<?php

namespace facade\request\admin;


class GetStarListParams
{

    public ?int $index = 0;
    public ?int $page_size = 10;
    public ?string $star_name = null;


}