<?php


namespace facade\request\admin;


use facade\request\RequestParams;

class GetFeedInformationDetailParams extends RequestParams
{
    public $content;
    public $id;
    public $main_image;
    public $title;
    public $author_id;
    public $is_original;
    public $outsite_link;
    public $outsite_author;
    public $outsite_source;


}