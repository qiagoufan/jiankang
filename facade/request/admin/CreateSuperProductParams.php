<?php

namespace facade\request\admin;


class CreateSuperProductParams
{

    public ?int $item_id = 0;
    public ?string $background_pic = null;
    public ?int $is_super = 0;
    public ?array $ip_list = [];


}