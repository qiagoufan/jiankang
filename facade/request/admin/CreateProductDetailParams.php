<?php

namespace facade\request\admin;


class CreateProductDetailParams
{

    const AUDIT_STATUS_FINISHED = 1;
    const AUDIT_STATUS_UNFINISHED = 0;
    const publish_status_ONLINE = 1;
    const publish_status_OFFLINE = 0;


    public ?int $item_id = 0;
    public ?int $seller_id = 1;
    public $specifications;
    public array $sku_list = [];
    public ?int $shipping_price;
    public ?array $ip_list = [];

    /**
     * @var int|null 预留
     */
//    public ?int $width;
//    public ?int $height;
//    public ?int $weight;
//    public ?int $length;
//    public ?int $expire_timestamp;
//    public ?int $product_code;
}