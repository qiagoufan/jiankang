<?php


namespace facade\request\admin\banner;

use facade\request\RequestParams;

class UpdateBannerParams extends RequestParams
{

    public $id;
    public $title;
    public $link;
    public $link_type;
    public $image;
    public $sort;

}