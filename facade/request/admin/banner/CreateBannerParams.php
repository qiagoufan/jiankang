<?php


namespace facade\request\admin\banner;

use facade\request\RequestParams;

class CreateBannerParams extends RequestParams
{
    public $title;
    public $link;
    public $link_type;
    public $image;
    public $sort;
    public $space_id;
}