<?php

namespace facade\request\admin;


class SearchUserListParams
{

    public ?string $nick_name = null;
    public ?int $phone = null;
    public ?int $start_time = null;
    public ?int $end_time = null;
    public ?int $index = 0;
    public ?int $pageSize = 10;
    public ?int $search_user_id = null;


}