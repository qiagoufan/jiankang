<?php


namespace facade\request\admin;


use facade\request\RequestParams;

class GetFeedInformationListParams extends RequestParams
{
    public $page_size = 10;
    public $index = 0;

}