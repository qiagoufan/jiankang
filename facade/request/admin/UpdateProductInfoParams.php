<?php

namespace facade\request\admin;

class UpdateProductInfoParams
{

    const AUDIT_STATUS_FINISHED = 1;
    const AUDIT_STATUS_UNFINISHED = 0;
    const publish_status_ONLINE = 1;
    const publish_status_OFFLINE = 0;
    public ?int $item_id;

    public ?string $product_name = null;
    public ?int $is_super = null;
    public ?array $star_id = [];
    public ?int $bar_code = null;
    public ?int $brand_id = null;
    public ?int $cate_level_1 = null;
    public ?int $cate_level_2 = null;
    public ?int $cate_level_3 = null;
    public ?int $stock = null;
    public ?string $description = null;
    public ?string $summary = null;


    public ?string $main_pic = null;
    public ?array $gallery_image = null;
    public ?array $detail_image = null;
    public ?string $background_pic = null;
    public ?array $specifications = null;
    public ?int $shipping_price = null;

}