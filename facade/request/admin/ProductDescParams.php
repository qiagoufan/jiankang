<?php


namespace facade\request\admin;


class ProductDescParams
{

    public ?string $explanation = "本产品由深圳市博源电子商务有限公司销售，并提供开票及售后服务，产品以实际为准。";
    public ?string $service = "客服电话：4008002383";
    public ?string $title;

}