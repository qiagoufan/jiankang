<?php

namespace facade\request\admin;


class SaveStarParams
{

    public ?string $star_name = null;
    public ?string $star_desc = null;
    public ?string $background = null;
    public ?string $base_introduce = null;
    public ?string $avatar = null;
    public ?int $ip_type = 1;
    public $owner_user_id = null;


}