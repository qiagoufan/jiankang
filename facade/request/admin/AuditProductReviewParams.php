<?php

namespace facade\request\admin;


class AuditProductReviewParams
{

    public ?int $product_review_id;
    public ?int $audit_status;


}