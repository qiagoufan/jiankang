<?php

namespace facade\request\admin;


use facade\request\RequestParams;

class EditProductItemParams extends RequestParams
{


    public $item_id = null;
    public $product_name;
    public $category_id;
    public $product_type;
    public $main_image;
    public $stock;
    public $current_price;
    public $regular_price;
    public $gallery_image;
    //详情
    public $product_desc;
    //简介
    public $summary;
    //规格
    public $specifications;

    public $seq;


}