<?php


namespace facade\request\admin;


class GetProductItemListParams
{


    public $publish_type;
    public ?int $index = 0;
    public ?int $page_size = 10;
    public ?int $low_price;
    public ?int $high_price;
    public ?string $name = null;
    public ?int $is_virtual = null;


}