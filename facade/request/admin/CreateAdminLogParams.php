<?php

namespace facade\request\admin;


use facade\request\RequestParams;

class CreateAdminLogParams extends RequestParams
{
    public $operate_text;
    public $operate_type;
    public $result;

}