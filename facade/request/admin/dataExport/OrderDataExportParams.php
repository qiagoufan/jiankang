<?php

namespace facade\request\admin\dataExport;


use facade\request\RequestParams;

class OrderDataExportParams extends RequestParams
{

    public $start_time;
    public $end_time;

}