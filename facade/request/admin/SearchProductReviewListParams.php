<?php

namespace facade\request\admin;


class SearchProductReviewListParams
{

    public ?string $product_name;
    public ?int $order_sn;
    public ?string $buyer_name;
    public ?int $start_time = null;
    public ?int $end_time = null;
    public ?int $index = 0;
    public ?int $page_size = 10;


}