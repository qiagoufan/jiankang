<?php

namespace facade\request\admin;


class AuditFeedParams
{
    const AUDIT_FEED_PASS = 1;
    const AUDIT_FEED_UNPASS = -2;

    public $feed_id = null;
    public $audit_status = null;


}