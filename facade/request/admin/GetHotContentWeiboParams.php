<?php

namespace facade\request\admin;


class GetHotContentWeiboParams
{

    public $count = 100;
    public $page = 1;
    public $since_id = 0;
    public $max_id = 0;
    public $base_app = 0;
    public $feature = 0;


}