<?php

namespace facade\request\admin;


class QueryAllOrderListParams
{

    public $index = 0;
    public $page_size = 10;
    public $query_time_type = null;
    public $start_timestamp = null;
    public $end_timestamp = null;
    public $pay_type = null;
    public $order_status_type = null;
    public $order_sn = null;


}