<?php

namespace facade\request\admin;


class GetAllFeedListParams
{

    const FEED_STATUS_PASS = 1;
    const FEED_STATUS_UNPASS = -2;
    const FEED_STATUS_ALL = 2;
    consT FEED_STATUS_FEATRUED = 3;


    public ?int $index = 0;
    public ?int $page_size = 10;

    public $start_time = null;
    public $end_time = null;
    public ?int $feed_status = null;
    public $feed_resource_type = null;

}