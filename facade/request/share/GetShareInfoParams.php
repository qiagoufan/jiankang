<?php


namespace facade\request\share;

use facade\request\RequestParams;

class GetShareInfoParams extends RequestParams
{

    const SHARE_TYPE_PRODUCT = 1;
    const SHARE_TYPE_STAR_FEED = 2;
    const SHARE_TYPE_MATERIALS_FEED = 3;
    const SHARE_TYPE_SUPER_PRODUCT = 4;

    public ?int $share_type = null;
    public ?int $biz_id = null;
}