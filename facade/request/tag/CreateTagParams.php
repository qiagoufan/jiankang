<?php


namespace facade\request\tag;


use facade\request\RequestParams;

class CreateTagParams extends RequestParams
{
    public $tag_name;
    public $tag_desc;
    public $tag_biz_type;
    public $tag_biz_id;
}