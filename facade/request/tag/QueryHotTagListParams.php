<?php


namespace facade\request\tag;


use facade\request\RequestParams;

class QueryHotTagListParams extends RequestParams
{

    public ?int $index = 0;
    public ?int $page_size = 10;

}