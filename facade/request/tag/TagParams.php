<?php


namespace facade\request\tag;


use facade\request\RequestParams;

class TagParams extends RequestParams
{
    public $tag_biz_id;
    public $tag_biz_type;
}