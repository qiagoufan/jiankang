<?php


namespace facade\request\tag;


use facade\request\RequestParams;

class QueryFullSiteTagEntityListParams extends RequestParams
{
    public int $tag_id = 0;
    public ?int $index = 0;
    public ?int $page_size = 10;
    public $tag_biz_id;
    public $tag_biz_type;
}