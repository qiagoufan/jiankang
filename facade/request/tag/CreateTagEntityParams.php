<?php


namespace facade\request\tag;


use facade\request\RequestParams;

class CreateTagEntityParams extends RequestParams
{
    /**
     * tag_id 或者 tag_biz_type+tag_biz_id
     */
    public $tag_id;
    public $tag_biz_type;
    public $tag_biz_id;
    public $entity_biz_type;
    public $entity_biz_id;
}