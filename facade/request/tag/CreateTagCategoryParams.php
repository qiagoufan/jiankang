<?php


namespace facade\request\tag;


use facade\request\RequestParams;

class CreateTagCategoryParams extends RequestParams
{
    public $category_name;
    public $category_desc;
    public $category_biz_type;
}