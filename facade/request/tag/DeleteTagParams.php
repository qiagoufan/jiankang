<?php


namespace facade\request\tag;


use facade\request\RequestParams;

class DeleteTagParams extends RequestParams
{
    public int $tag_id;
}