<?php


namespace facade\request\tag;


use facade\request\RequestParams;

class DeleteTagEntityParams extends RequestParams
{
    public ?int $tag_id = 0;
    public ?int $tag_biz_id = 0;
    public ?string $tag_biz_type = null;
    public ?int $entity_biz_id = 0;
    public ?string $entity_biz_type = null;
}