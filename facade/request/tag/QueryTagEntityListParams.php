<?php


namespace facade\request\tag;


use facade\request\RequestParams;

class QueryTagEntityListParams extends RequestParams
{
    public $tag_id = 0;
    public $index = 0;
    public $page_size = 10;
    public $entity_biz_type;
    public $tag_biz_id;
    public $tag_biz_type;
}