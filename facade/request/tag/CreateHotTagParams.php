<?php


namespace facade\request\tag;


use facade\request\RequestParams;

class CreateHotTagParams extends RequestParams
{
    public $biz_tag_type;
    public $biz_tag_id;
}