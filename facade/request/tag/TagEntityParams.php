<?php


namespace facade\request\tag;


use facade\request\RequestParams;

class TagEntityParams extends RequestParams
{
    public $tag_id;
    public $entity_biz_type;
    public $entity_biz_id;
    public $tag_biz_id;
    public $tag_biz_type;
}