<?php


namespace facade\request\promotion;


class EditVoucherParams
{
    public $voucher_name = null;
    public $voucher_id = null;
    public $voucher_count = null;
    public $amount = null;
    public $start_timestamp = null;
    public $end_timestamp = null;
    public $expire_timelong = null;
    public $price_limit = null;
    public $voucher_status = null;

}