<?php


namespace facade\request\promotion;


use facade\request\RequestParams;

class QueryVoucherListForProductParams extends RequestParams
{
    public $product_list = null;

}