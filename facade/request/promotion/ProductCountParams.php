<?php


namespace facade\request\promotion;


use facade\request\RequestParams;

class ProductCountParams
{
    public $sku_id = null;
    public $count = null;

}