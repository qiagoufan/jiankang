<?php

namespace facade\request\jk\riskCheck;

use facade\request\RequestParams;

class  RiskCheckAnswerParams extends RequestParams
{
    public $question_id = 0;
    public $option_list;
}