<?php

namespace facade\request\jk\riskCheck;

use facade\request\RequestParams;

class  EditCheckQuestionParams extends RequestParams
{
    public $category_id;
    public $question_list;
}