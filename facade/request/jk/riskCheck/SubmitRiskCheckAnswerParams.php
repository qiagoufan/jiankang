<?php

namespace facade\request\jk\riskCheck;

use facade\request\RequestParams;

class  SubmitRiskCheckAnswerParams extends RequestParams
{
    public $category_id = 0;
    public $answer_list;
    public $option_id_list;
}