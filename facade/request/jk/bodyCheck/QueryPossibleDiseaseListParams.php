<?php

namespace facade\request\jk\bodyCheck;


use facade\request\PageParams;

class  QueryPossibleDiseaseListParams extends PageParams
{
    public $symptom_id;
    public $sex;
    public $age;
    public $option_id_list;
}