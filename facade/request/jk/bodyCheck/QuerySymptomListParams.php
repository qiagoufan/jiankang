<?php

namespace facade\request\jk\bodyCheck;


use facade\request\PageParams;

class  QuerySymptomListParams extends PageParams
{
    public $body_id;
    public $sex;
    public $age;
}