<?php

namespace facade\request\jk\bodyCheck\manage;


use facade\request\RequestParams;

class  EditSymptomParams extends RequestParams
{
    public $human_body_id;
    public $symptom_id;
    public $sex;
    public $age;
    public $symptom;
    public $symptom_image;
    public $calculation_type;
}