<?php

namespace facade\request\jk\bodyCheck\manage;


use facade\request\PageParams;

class  QuerySymptomParams extends PageParams
{

    public $keyword;
    public $human_body_id;
}