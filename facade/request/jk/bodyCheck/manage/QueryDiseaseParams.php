<?php

namespace facade\request\jk\bodyCheck\manage;


use facade\request\PageParams;

class  QueryDiseaseParams extends PageParams
{

    public $disease_name;
    public $class_name;
}