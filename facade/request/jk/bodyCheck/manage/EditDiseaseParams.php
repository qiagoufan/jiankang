<?php

namespace facade\request\jk\bodyCheck\manage;


use facade\request\RequestParams;

class  EditDiseaseParams extends RequestParams
{
    public $disease_id;

    public $name;
    //
    public $letter;
    //
    public $first_letter;
    //
    public $is_hurry;
    //
    public $class_id;
    //
    public $class_name;
    //
    public $introduction;
    //
    public $checking;
    //
    public $concurrent;
    //
    public $diagnosis;
    //
    public $prevention;
    //
    public $reason_description;
    //
    public $symptom_description;
    //
    public $treatment;
}