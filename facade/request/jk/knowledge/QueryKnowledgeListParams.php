<?php

namespace facade\request\jk\knowledge;


use facade\request\PageParams;

class  QueryKnowledgeListParams extends PageParams
{
    public $category_id = 0;
    public $is_publish = 0;
}