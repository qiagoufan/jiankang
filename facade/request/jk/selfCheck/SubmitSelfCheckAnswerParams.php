<?php

namespace facade\request\jk\selfCheck;

use facade\request\RequestParams;

class  SubmitSelfCheckAnswerParams extends RequestParams
{
    public $category_id = 0;
    public $answer_list;
}