<?php

namespace facade\request\jk\selfCheck;

use facade\request\RequestParams;

class  SelfCheckAnswerParams extends RequestParams
{
    public $question_id = 0;
    public $option_list;
}