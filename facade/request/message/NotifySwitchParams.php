<?php


namespace facade\request\message;

use facade\request\RequestParams;

class NotifySwitchParams extends RequestParams
{
    public $push_switch;
}