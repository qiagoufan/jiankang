<?php


namespace facade\request\message;

use facade\request\PageParams;
use facade\request\RequestParams;

class GetInteractiveMessageListParams extends RequestParams
{

    const MESSAGE_LIKE_TYPE = 1;
    const MESSAGE_COMMENT_TYPE = 2;
    const MESSAGE_REPLY_TYPE = 3;

    public ?int $type;
    public ?int $index = 0;
    public ?int $page_size = 10;


}