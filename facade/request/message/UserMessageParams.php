<?php


namespace facade\request\message;

use facade\request\RequestParams;

class UserMessageParams extends RequestParams
{
    public $index;
    public $page_size;
}