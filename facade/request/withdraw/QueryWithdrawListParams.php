<?php


namespace facade\request\withdraw;

use facade\request\RequestParams;

class QueryWithdrawListParams extends RequestParams
{
    public $index = 0;
    public $page_size = 10;
    public $cash_out_status;
    public $engineer_phone_num;
    public $engineer_id;
}