<?php


namespace facade\request\maintain;

use facade\request\RequestParams;

class UpdateStarContributionParams extends RequestParams
{
    public $star_id;
    public $value;

}