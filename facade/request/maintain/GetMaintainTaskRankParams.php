<?php

namespace facade\request\maintain;

use facade\request\RequestParams;

class GetMaintainTaskRankParams extends RequestParams
{

    const RANK_TYPE_TASK_TOTAL = 'total';
    const RANK_TYPE_TASK_SOAR = 'soar';

    public int $index = 0;
    public int $page_size = 10;
    public ?string $rank_type = 'total';
}