<?php


namespace facade\request\maintain;

use facade\request\RequestParams;

class CreateContributionLeaderboardParams extends RequestParams
{

    public $star_id;
    public $index;

}