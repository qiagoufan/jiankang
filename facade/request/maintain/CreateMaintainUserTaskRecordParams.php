<?php

namespace facade\request\maintain;

use facade\request\RequestParams;

class CreateMaintainUserTaskRecordParams extends RequestParams
{
    public $user_id;
    public $star_id;
    public $task_id;
    public $value;

}