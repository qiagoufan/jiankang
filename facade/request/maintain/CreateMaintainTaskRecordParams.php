<?php


namespace facade\request\maintain;

use facade\request\RequestParams;

class CreateMaintainTaskRecordParams extends RequestParams
{
    public $task_id;
    public $star_id;


}