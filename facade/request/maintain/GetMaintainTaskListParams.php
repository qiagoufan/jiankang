<?php


namespace facade\request\maintain;

use facade\request\RequestParams;

class GetMaintainTaskListParams extends RequestParams
{
    public $index = 0;
    public $page_size = 10;


}