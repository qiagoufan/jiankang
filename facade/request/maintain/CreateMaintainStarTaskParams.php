<?php


namespace facade\request\maintain;

use facade\request\RequestParams;

class CreateMaintainStarTaskParams extends RequestParams
{
    public $task_id;
    public $star_id;


}