<?php


namespace facade\request\maintain;

use facade\request\RequestParams;

class CreateLeaderboardRecordParams extends RequestParams
{


    public $user_id;
    public $star_id;
    public $value;
    public $rank_type;
    public $record_type;

}