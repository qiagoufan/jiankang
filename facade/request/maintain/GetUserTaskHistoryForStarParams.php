<?php

namespace facade\request\maintain;

use facade\request\RequestParams;

class GetUserTaskHistoryForStarParams extends RequestParams
{


    public int $index = 0;
    public int $page_size = 10;
    public ?int $star_id = 0;
    public ?int $visit_user_id = 0;
}