<?php

namespace tools;
/**
 * 对官方提供的代码进行了修改
 */
class CurlUtil
{
    const DEFAULT_TIMEOUT = 10000;

    public static function get($url, $header = array(), $timeout = 0, &$errInfo = "")
    {


        $ssl = stripos($url, 'https://') === 0 ? true : false;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPGET, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_NOSIGNAL, 1);
        curl_setopt($ch, CURLOPT_DNS_CACHE_TIMEOUT, 0);

        if ($timeout > 0) {
            curl_setopt($ch, CURLOPT_TIMEOUT_MS, $timeout);
        } else {
            curl_setopt($ch, CURLOPT_TIMEOUT_MS, self::DEFAULT_TIMEOUT);
        }
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 3);
        if ($ssl) {
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE); // https请求 不验证证书和hosts
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        }

        if (!empty ($header)) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        }

        $result = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $errCode = curl_errno($ch);
        $errMsg = curl_error($ch);

        $content_type = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);

        curl_close($ch);

        if ($errCode && $httpCode != 200) {
            $req_uri = $url;
            $pos = strpos($url, "?");
            if ($pos > 0)
                $req_uri = substr($url, 0, $pos);
            $err_info = array(
                'rely' => $req_uri . ":" . $errCode,
                'url' => $url,
                'errno' => $errCode,
                'errmsg' => $errMsg,
                'httpCode' => $httpCode
            );
            return false;
        }
//        if (!stripos($content_type, 'utf-8')) {
//            $charset_explode = explode('charset=', $content_type);
//            $charset = $charset_explode[1];
//            $result = $content = mb_convert_encoding($result, 'UTF-8', $charset);
//        }


        return $result;
    }

    public static function post($url, $post_data, $header = array(), $timeout = 2)
    {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        curl_setopt($ch, CURLOPT_NOSIGNAL, 1);

        curl_setopt($ch, CURLOPT_TIMEOUT_MS, self::DEFAULT_TIMEOUT);


        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_data);
        curl_setopt($ch, CURLOPT_DNS_CACHE_TIMEOUT, 0);

        if (!empty ($header)) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        }

        $result = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $errCode = curl_errno($ch);
        $errMsg = curl_error($ch);

        curl_close($ch);


        if ($httpCode != 200) {
            if ($result == "") {
                $req_uri = $url;
                $pos = strpos($url, "?");
                if ($pos > 0)
                    $req_uri = substr($url, 0, $pos);
                $err_info = array(
                    'rely' => $req_uri . ":" . $errCode,
                    'url' => $url,
                    'errno' => $errCode,
                    'errmsg' => $errMsg,
                    'httpCode' => $httpCode
                );

                return false;
            }
        }

        return $result;
    }
}
