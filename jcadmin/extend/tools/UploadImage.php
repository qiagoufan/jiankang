<?php

namespace tools;
/**
 * 对官方提供的代码进行了修改
 */
class UploadImage
{

    public static function uploadFile($fileName)
    {

        if (isset($_FILES[$fileName]) && $_FILES[$fileName]['name'] != null) {
            $fileBas64 = base64_encode(file_get_contents($_FILES[$fileName]['tmp_name']));
            $fileExtension = pathinfo($_FILES[$fileName]['name'], PATHINFO_EXTENSION);
            $fileName = md5(uniqid()) . "." . $fileExtension;
            return self::uploadBase64Image($fileBas64, $fileName);
        }
        return null;
    }

    public static function uploadFileByTmpName($tmpName)
    {
        if ($tmpName != null) {
            $fileBas64 = base64_encode(file_get_contents($tmpName));
            return self::uploadBase64Image($fileBas64);
        }
        return null;
    }

    public static function uploadBase64Image($base64_data, $fileName = '')
    {
        $url = config("app.server_domain") . '/system/media/uploadBase64Image';
        $data['file_base_64'] = $base64_data;
        $data['file_name'] = $fileName;
        $response = CurlUtil::post($url, $data);
        if ($response == null) {
            return $response;
        }
        $responseArray = json_decode($response, true);
        return $responseArray['data'];
    }

}
