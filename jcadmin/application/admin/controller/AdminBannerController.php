<?php
/**
 * 后台用户控制器
 * @author yupoxiong<i@yufuping.com>
 */

namespace app\admin\controller;

use think\Request;
use tools\UploadImage;

class AdminBannerController extends Controller
{

    //列表
    public function index(Request $request)
    {

        $responseData = $this->getServerData('/admin/advertisingManage/getAdvertisingList');
        //关键词，排序等赋值
        $this->assign([
            'data' => $responseData['data'],
        ]);
        return $this->fetch();
    }

    //添加
    public function add(Request $request)
    {
        if ($request->isPost()) {
            $imageUrl = UploadImage::uploadFile('upload_image');
            $params = $this->request->param();
            $params['image'] = $imageUrl;
            $responseData = $this->getServerData('/admin/advertisingManage/createAdvertising', $params, true);
            return json($responseData);
        }
        return $this->fetch();
    }

    //修改
    public function edit(Request $request)
    {

        if ($request->isPost()) {
            $params = $this->request->param();
            $params['image'] = UploadImage::uploadFile('upload_image');
            $responseData = $this->getServerData('/admin/advertisingManage/updateAdvertising', $params, true);
            return json($responseData);
        }
        $responseData = $this->getServerData('/admin/advertisingManage/getAdvertisingDetail');
        $this->assign([
            'data' => $responseData['data'],
        ]);
        return $this->fetch('add');

    }


    //删除
    public function del($id)
    {
        $responseData = $this->getServerData('/admin/advertisingManage/deleteAdvertising', null, true);
        return json($responseData);
    }

}
