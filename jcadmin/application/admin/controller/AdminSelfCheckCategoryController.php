<?php
/**
 * 后台用户控制器
 * @author yupoxiong<i@yufuping.com>
 */

namespace app\admin\controller;

use think\Request;
use tools\UploadImage;

class AdminSelfCheckCategoryController extends Controller
{

    //列表
    public function index(Request $request)
    {

        $responseData = $this->getServerData('/admin/selfCheckManage/getSelfCheckCategoryList');
        //关键词，排序等赋值
        $this->assign([
            'data' => $responseData['data'],
        ]);
        return $this->fetch();
    }

    //添加
    public function add(Request $request)
    {
        if ($request->isPost()) {
            $params = $request->param();
            $categoryCover = UploadImage::uploadFile('category_cover');
            $params['category_cover'] = $categoryCover;
            $responseData = $this->getServerData('/admin/selfCheckManage/createSelfCheckCategory', $params, true);
            return json($responseData);
        }
        return $this->fetch();
    }

    //修改
    public function edit(Request $request)
    {

        if ($request->isPost()) {
            $params = $request->param();
            $categoryCover = UploadImage::uploadFile('category_cover');
            if ($categoryCover) {
                $params['category_cover'] = $categoryCover;
            }
            $responseData = $this->getServerData('/admin/selfCheckManage/updateSelfCheckCategory', $params, true);
            return json($responseData);
        }
        $responseData = $this->getServerData('/admin/selfCheckManage/getSelfCheckCategoryDetail');
        $this->assign([
            'data' => $responseData['data'],
        ]);
        return $this->fetch('add');

    }


    //删除
    public function del($id)
    {
        $responseData = $this->getServerData('/admin/selfCheckManage/deleteSelfCheckCategory', null, true);
        return json($responseData);
    }

    //修改
    public function question(Request $request)
    {

        if ($request->isPost()) {
            $params = $request->param();
            $questionCount = $request->param('question_count');
            $questionList = [];

            for ($i = 1; $i <= $questionCount; $i++) {
                $questionInfo = new \stdClass();
                $questionInfo->question_title = $request->param('question' . $i);
                if ($questionInfo->question_title == null) {
                    continue;
                }

                $questionInfo->question_option = [];
                $questionInfo->sort_num = $i;
                $options = $request->param('option' . $i);
                $scores = $request->param('score' . $i);
                for ($j = 0; $j < count($options); $j++) {
                    $optionInfo = new \stdClass();
                    $optionInfo->symptom = $options[$j];
                    $optionInfo->score = $scores[$j];
                    array_push($questionInfo->question_option, $optionInfo);
                }
                array_push($questionList, $questionInfo);
            }
            $params['question_list'] = json_encode($questionList);
            $responseData = $this->getServerData('/admin/selfCheckManage/updateSelfCheckQuestion', $params, true);
            return json($responseData);
        }
        $categoryData = $this->getServerData('/admin/selfCheckManage/getSelfCheckCategoryList');
        $responseData = $this->getServerData('/admin/selfCheckManage/getSelfCheckDetail');
        $this->assign([
                'category_list' => $categoryData['data'],
                'data' => $responseData['data'],
                'category_id' => $request->param('id')
            ]
        );

        return $this->fetch();

    }

    public function check_result(Request $request)
    {
        if ($request->isPost()) {
            $params = $request->param();
            $scoreCount = $request->param('score_count');
            $scoreList = [];

            for ($i = 1; $i <= $scoreCount; $i++) {
                $scoreInfo = new \stdClass();
                $scoreInfo->disease = $request->param('disease' . $i);
                if ($scoreInfo->disease == null) {
                    continue;
                }

                $scoreInfo->start_score = $request->param('start_score' . $i);
                $scoreInfo->end_score = $request->param('end_score' . $i);
                $scoreInfo->suggest = $request->param('suggest' . $i);
                $productInfoList = $request->param('bind_product_' . $i);
                $scoreInfo->product_list = [];
                foreach ($productInfoList as $productInfo){
                    if ($productInfo != null) {
                        preg_match('#\((.*?)\)#', $productInfo, $match);
                        $productId = $match[1];
                        array_push($scoreInfo->product_list, $productId);
                    }
                }

                array_push($scoreList, $scoreInfo);
            }
            $params['result_list'] = json_encode($scoreList);
            $responseData = $this->getServerData('/admin/selfCheckManage/updateSelfCheckResult', $params, true);
            return json($responseData);
        }
        $responseData = $this->getServerData('/admin/selfCheckManage/getSelfCheckResult');
        $this->assign([
            'data' => $responseData['data']
        ]);
        return $this->fetch();

    }

}
