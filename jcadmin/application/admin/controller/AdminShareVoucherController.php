<?php
/**
 * 后台用户控制器
 * @author yupoxiong<i@yufuping.com>
 */

namespace app\admin\controller;

use think\Request;
use tools\UploadImage;

class AdminShareVoucherController extends Controller
{

    public function index(Request $request)
    {
        $voucher_id = -1;
        if ($request->isPost()) {
            $params = $request->param();
            $params['start_timestamp'] = 0;
            $params['end_timestamp'] = 2242656000;
            $params['expire_timelong'] = 365 * 86400;
            $params['voucher_id'] = $voucher_id;
            $responseData = $this->getServerData('/admin/voucherManage/updateVoucher', $params, true);
            $responseData['url'] = '';
            return json($responseData);
        }
        $params = ['id' => $voucher_id];
        $responseData = $this->getServerData('/admin/voucherManage/queryVoucherDetail', $params);
        $this->assign([
            'data' => $responseData['data'],
        ]);

        return $this->fetch('edit');
    }


}
