<?php
/**
 * 后台用户控制器
 * @author yupoxiong<i@yufuping.com>
 */

namespace app\admin\controller;

use think\Request;
use tools\UploadImage;

class AdminSecurityWordController extends Controller
{

    //列表
    public function index(Request $request)
    {

        $responseData = $this->getServerData('/admin/securityWordManage/getSecurityWordList');
        $this->assign([
            'data' => $responseData['data'],
        ]);
        return $this->fetch();
    }

    //添加
    public function add(Request $request)
    {
        if ($request->isPost()) {
            $params = $this->request->param();
            $responseData = $this->getServerData('/admin/securityWordManage/createSecurityWord', $params, true);
            return json($responseData);
        }
        return $this->fetch();
    }

    //修改
    public function edit(Request $request)
    {

        if ($request->isPost()) {
            $params = $this->request->param();
            $responseData = $this->getServerData('/admin/securityWordManage/updateSecurityWord', $params, true);
            return json($responseData);
        }
        $responseData = $this->getServerData('/admin/securityWordManage/getSecurityWordDetail');
        $this->assign([
            'data' => $responseData['data'],
        ]);
        return $this->fetch('add');

    }


    //删除
    public function del($id)
    {
        $responseData = $this->getServerData('/admin/securityWordManage/deleteSecurityWord', null, true);
        return json($responseData);
    }

}
