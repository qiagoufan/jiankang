<?php
/**
 * 后台用户控制器
 * @author yupoxiong<i@yufuping.com>
 */

namespace app\admin\controller;

use think\Request;
use tools\UploadImage;

class AdminRiskCheckCategoryController extends Controller
{

    //列表
    public function index(Request $request)
    {

        $responseData = $this->getServerData('/admin/riskCheckManage/getRiskCheckCategoryList');
        //关键词，排序等赋值
        $this->assign([
            'data' => $responseData['data'],
        ]);
        return $this->fetch();
    }

    //添加
    public function add(Request $request)
    {
        if ($request->isPost()) {
            $params = $request->param();
            $categoryCover = UploadImage::uploadFile('category_cover');
            $params['category_cover'] = $categoryCover;
            $responseData = $this->getServerData('/admin/riskCheckManage/createRiskCheckCategory', $params, true);
            return json($responseData);
        }

        $category_list = $this->getServerData('/admin/riskCheckManage/getRiskCheckCategoryList');
        $this->assign([
            'category_list' => $category_list['data'],
        ]);
        return $this->fetch();
    }

    //修改
    public function edit(Request $request)
    {

        if ($request->isPost()) {
            $params = $request->param();
            $categoryCover = UploadImage::uploadFile('category_cover');
            if ($categoryCover) {
                $params['category_cover'] = $categoryCover;
            }
            $responseData = $this->getServerData('/admin/riskCheckManage/updateRiskCheckCategory', $params, true);
            return json($responseData);
        }
        $responseData = $this->getServerData('/admin/riskCheckManage/getRiskCheckCategoryDetail');
        $category_list = $this->getServerData('/admin/riskCheckManage/getRiskCheckCategoryList');
        $this->assign([
            'data' => $responseData['data'], 'category_list' => $category_list['data']
        ]);
        return $this->fetch('add');

    }


    //删除
    public function del($id)
    {
        $responseData = $this->getServerData('/admin/riskCheckManage/deleteRiskCheckCategory', null, true);
        return json($responseData);
    }

}
