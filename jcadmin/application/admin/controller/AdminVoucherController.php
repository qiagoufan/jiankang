<?php
/**
 * 后台用户控制器
 * @author yupoxiong<i@yufuping.com>
 */

namespace app\admin\controller;

use think\Request;
use tools\UploadImage;

class AdminVoucherController extends Controller
{

    //列表
    public function index(Request $request)
    {

        $responseData = $this->getServerData('/admin/voucherManage/queryVoucherList');
        //关键词，排序等赋值
        $this->assign([
            'data' => $responseData['data'],
        ]);
        return $this->fetch();
    }

    //添加
    public function add(Request $request)
    {
        if ($request->isPost()) {
            $params = $request->param();
            list($startDate, $endDate) = explode(" - ", $request->param('date_range'));
            $params['start_timestamp'] = strtotime($startDate);
            $params['end_timestamp'] = strtotime($endDate) + 86400 - 1;
            $params['expire_timelong'] = $request->param('day_long') * 86400;
            $responseData = $this->getServerData('/admin/voucherManage/createVoucher', $params, true);
            return json($responseData);
        }
        return $this->fetch();
    }

    //修改
    public function edit(Request $request)
    {

        if ($request->isPost()) {
            $params = $request->param();
            list($startDate, $endDate) = explode(" - ", $request->param('date_range'));
            $params['start_timestamp'] = strtotime($startDate);
            $params['end_timestamp'] = strtotime($endDate) + 86400 - 1;
            $params['expire_timelong'] = $request->param('day_long') * 86400;
            $params['voucher_id'] = $request->param('id');
            $responseData = $this->getServerData('/admin/voucherManage/updateVoucher', $params, true);
            return json($responseData);
        }
        $responseData = $this->getServerData('/admin/voucherManage/queryVoucherDetail');

        $responseData['data']['date_range'] = date('Y-m-d', $responseData['data']['start_timestamp']) . " - " . date('Y-m-d', $responseData['data']['end_timestamp']);
        $this->assign([
            'data' => $responseData['data'],
        ]);
        return $this->fetch('add');

    }


    //删除
    public function del()
    {
        $responseData = $this->getServerData('/admin/voucherManage/deleteVoucher', null, true);
        return json($responseData);
    }

    public function publish(Request $request)
    {
        $params = ['id' => $request->param('id'), 'voucher_status' => 1];

        $responseData = $this->getServerData('/admin/voucherManage/updateVoucherStatus', $params, true);
        return json($responseData);
    }

    public function cancel_publish(Request $request)
    {
        $params = ['id' => $request->param('id'), 'voucher_status' => -1];
        $responseData = $this->getServerData('/admin/voucherManage/updateVoucherStatus', $params, true);
        return json($responseData);
    }
}
