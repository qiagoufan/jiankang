<?php
/**
 * 后台用户控制器
 * @author yupoxiong<i@yufuping.com>
 */

namespace app\admin\controller;

use think\Request;
use tools\UploadImage;

class AdminOrderController extends Controller
{

    //列表
    public function index(Request $request)
    {
        $params = $request->param();
        $dataRange = $request->param('date_range');
        if ($dataRange) {
            list($startDate, $endDate) = explode(" - ", $dataRange);
            $params['start_timestamp'] = strtotime($startDate);
            $params['end_timestamp'] = strtotime($endDate) + 86400 - 1;
        }

        $responseData = $this->getServerData('/admin/orderManage/queryOrderList', $params);
        //关键词，排序等赋值
        $this->assign([
            'data' => $responseData['data'],
            'date_range' => $request->param('date_range'),
            'order_status_type' => $request->param('order_status_type'),
            'order_sn' => $request->param('order_sn'),
            'query_time_type' => $request->param('query_time_type')
        ]);
        return $this->fetch();
    }

    //添加
    public function detail(Request $request)
    {
        $responseData = $this->getServerData('/admin/orderManage/queryOrderDetail');
        //关键词，排序等赋值
        $this->assign([
            'data' => $responseData['data'],
        ]);
        return $this->fetch();
    }

    //修改
    public function deliver(Request $request)
    {

        if ($request->isPost()) {
            $responseData = $this->getServerData('/admin/orderManage/deliverOrder', null, true);
            return json($responseData);
        }
        $responseData = $this->getServerData('/admin/orderManage/queryOrderDetail');
        $express_company = $this->getServerData('/admin/orderManage/getExpressList');
        //关键词，排序等赋值
        $this->assign([
            'data' => $responseData['data'],
            'express_company_list' => $express_company['data'],
        ]);
        return $this->fetch();

    }


}
