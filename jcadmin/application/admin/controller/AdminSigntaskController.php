<?php
/**
 * 后台用户控制器
 * @author yupoxiong<i@yufuping.com>
 */

namespace app\admin\controller;

use app\common\model\Attachment;
use think\Request;
use app\admin\model\AdminRole;
use app\admin\model\AdminUser;
use app\admin\validate\AdminUserValidate;
use tools\UploadImage;

class AdminSigntaskController extends Controller
{

    //列表
    public function index()
    {
        $responseData = $this->getServerData('/admin/jkSignInManage/querySignTaskList');
        //关键词，排序等赋值
        $this->assign([
            'data' => $responseData['data'],
            'sign_in_type_id' => $this->request->param('sign_type_id')
        ]);

        return $this->fetch();
    }

    //添加
    public function add(Request $request)
    {
        if ($request->isPost()) {
            $params = $request->param();
            $params['sign_in_task_bg'] = UploadImage::uploadFile('sign_in_task_bg');

            $responseData = $this->getServerData('/admin/jkSignInManage/createSignTask', $params, true);
            return json($responseData);
        }
        return $this->fetch();
    }

    //修改
    public function edit(Request $request)
    {

        if ($request->isPost()) {
            $params = $request->param();
            $params['sign_in_task_bg'] = UploadImage::uploadFile('sign_in_task_bg');
            $responseData = $this->getServerData('/admin/jkSignInManage/updateSignTask', $params, true);
            return json($responseData);
        }
        $responseData = $this->getServerData('/admin/jkSignInManage/querySignTask');
        $this->assign([
            'data' => $responseData['data'],
        ]);

        return $this->fetch('add');

    }


    //删除
    public function del(Request $request)
    {
        $param = ['sign_task_id' => $request->param('id')];
        $responseData = $this->getServerData('/admin/jkSignInManage/deleteSignTask', $param, true);
        return json($responseData);
    }


}
