<?php
/**
 * 后台用户控制器
 * @author yupoxiong<i@yufuping.com>
 */

namespace app\admin\controller;

use think\Request;
use tools\UploadImage;

class AdminFeedController extends Controller
{

    //列表
    public function index(Request $request)
    {

        $responseData = $this->getServerData('/admin/feedManage/queryManageFeedList');
        //关键词，排序等赋值
        $this->assign([
            'data' => $responseData['data'], 'keyword' => $request->param('keyword'),
            'feed_status' => $request->param('feed_status')
        ]);
        return $this->fetch();
    }


    //删除
    public function del($id)
    {
        $responseData = $this->getServerData('/admin/feedManage/deleteFeed', null, true);
        return json($responseData);
    }

    public function check(Request $request)
    {
        $feedId = $request->param('id');

        $param = ['id' => $feedId, 'check_status' => $request->param('check_status')];
        $responseData = $this->getServerData('/admin/feedManage/updateFeedStatus', $param, true);
        return json($responseData);
    }

    public function batch_check(Request $request)
    {
        if ($request->isPost()) {
            $idList = $request->param('id');
            foreach ($idList as $feedId) {
                $param = ['id' => $feedId, 'check_status' => $request->param('check_status')];
                $responseData = $this->getServerData('/admin/feedManage/updateFeedStatus', $param, true);
            }
            return json($responseData);
        }
        $responseData = $this->getServerData('/admin/feedManage/updateFeedStatus', null, true);
        return json($responseData);
    }

    //详情
    public function detail(Request $request)
    {


        $responseData = $this->getServerData('/admin/feedManage/getFeedDetail');
        $this->assign(
            [
                'data' => $responseData['data']
            ]
        );
        return $this->fetch();

    }
}
