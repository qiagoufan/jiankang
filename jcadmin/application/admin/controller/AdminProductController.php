<?php
/**
 * 后台操作日志控制器
 * @author yupoxiong<i@yufuping.com>
 */

namespace app\admin\controller;

use think\Request;
use tools\UploadImage;

class AdminProductController extends Controller
{

    public function index(Request $request)
    {
        $responseData = $this->getServerData('/admin/productManage/getProductItemList');
        //关键词，排序等赋值
        $this->assign([
            'data' => $responseData['data'],
            'keyword' => $request->param('keyword'),
            'publish_status' => $request->param('publish_status'),
            'layer_product' => $request->param('layer_product'),
        ]);
        return $request->param('layer_product') ? $this->fetch('admin_product/index_layer') : $this->fetch();
    }

    //添加
    public function add(Request $request)
    {
        if ($request->isPost()) {
            $params = $request->param();
            $params['main_image'] = UploadImage::uploadFile('main_image');
            $uploadTmpFileList = $_FILES['gallery_image'];
            $params['gallery_image'] = [];
            if ($uploadTmpFileList != null) {
                foreach ($uploadTmpFileList['tmp_name'] as $tmpName) {
                    $image = UploadImage::uploadFileByTmpName($tmpName);
                    if ($image == null) {
                        continue;
                    }
                    array_push($params['gallery_image'], $image);
                }
            }
            $responseData = $this->getServerData('/admin/productManage/createProductItem', $params, true);
            return json($responseData);
        }
        $responseData = $this->getServerData('/admin/categoryManage/getProductCategoryList');
        $this->assign([
            'category_list' => $responseData['data'],
        ]);
        return $this->fetch();
    }

    //编辑
    public function edit(Request $request)
    {

        if ($request->isPost()) {
            $params = $request->param();
            $params['main_image'] = UploadImage::uploadFile('main_image');
            $uploadTmpFileList = $_FILES['gallery_image'];
            $params['gallery_image'] = [];
            if ($uploadTmpFileList != null) {
                foreach ($uploadTmpFileList['tmp_name'] as $tmpName) {
                    $image = UploadImage::uploadFileByTmpName($tmpName);
                    if ($image == null) {
                        continue;
                    }
                    array_push($params['gallery_image'], $image);
                }
            }
            $responseData = $this->getServerData('/admin/productManage/updateProductItem', $params, true);
            return json($responseData);
        }
        $responseData = $this->getServerData('/admin/productManage/getProductItemDetail');
        $categoryData = $this->getServerData('/admin/categoryManage/getProductCategoryList');
        $responseData['data']['gallery_image'] = null;
        if (isset($responseData['data']['product_detail']['galley_image_list'])) {
            $responseData['data']['gallery_image'] = '"' . implode('","', $responseData['data']['product_detail']['galley_image_list']) . '"';
        }
        $this->assign([
            'data' => $responseData['data'],
            'category_list' => $categoryData['data']
        ]);
        return $this->fetch('add');

    }


    //删除
    public function del(Request $request)
    {
        $param = ['item_id' => $request->param('id')];
        $responseData = $this->getServerData('/admin/productManage/deleteProduct', $param, true);
        return json($responseData);
    }

    //上架
    public function batch_publish(Request $request)
    {
        $itemIdList = $request->param('id');
        foreach ($itemIdList as $itemId) {
            $param = ['item_id' => $itemId, 'publish_status' => 1];
            $responseData = $this->getServerData('/admin/productManage/updateProductPublishStatus', $param, true);
        }
        return json($responseData);

    }

    public function publish(Request $request)
    {

        $param = ['item_id' => $request->param('item_id'), 'publish_status' => 1];
        $responseData = $this->getServerData('/admin/productManage/updateProductPublishStatus', $param, true);
        return json($responseData);
    }

    public function cancel_publish(Request $request)
    {

        $param = ['item_id' => $request->param('item_id'), 'publish_status' => -1];
        $responseData = $this->getServerData('/admin/productManage/updateProductPublishStatus', $param, true);
        return json($responseData);
    }

    public function batch_cancel_publish(Request $request)
    {
        $itemIdList = $request->param('id');
        foreach ($itemIdList as $itemId) {
            $param = ['item_id' => $itemId, 'publish_status' => -1];
            $responseData = $this->getServerData('/admin/productManage/updateProductPublishStatus', $param, true);
        }
        return json($responseData);
    }
}
