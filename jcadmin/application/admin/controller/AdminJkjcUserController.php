<?php
/**
 * 后台用户控制器
 * @author yupoxiong<i@yufuping.com>
 */

namespace app\admin\controller;

use think\Request;

class AdminJkjcUserController extends Controller
{

    //列表
    public function index(Request $request)
    {
        $responseData = $this->getServerData('/admin/userManage/queryUserList');
        //关键词，排序等赋值
        $this->assign([
            'data' => $responseData['data'],
            'keyword' => $request->param('keyword')
        ]);
        return $this->fetch();
    }

    //删除
    public function del(Request $request)
    {
        $responseData = $this->getServerData('/admin/userManage/deleteUser', null, true);
        return json($responseData);
    }


    //删除
    public function edit(Request $request)
    {
        if ($request->isPost()) {
            $params = $this->request->param();
            $responseData = $this->getServerData('/admin/userManage/updateUser', $params, true);
            return json($responseData);
        }
        $responseData = $this->getServerData('/admin/userManage/queryUserDetail');
        //关键词，排序等赋值
        $this->assign([
            'data' => $responseData['data'],
        ]);

        return $this->fetch();
    }
}
