<?php
/**
 * 后台用户控制器
 * @author yupoxiong<i@yufuping.com>
 */

namespace app\admin\controller;

use think\Request;

class AdminHospitalClassController extends Controller
{

    //列表
    public function index(Request $request)
    {

        $responseData = $this->getServerData('/admin/hospitalClassManage/getHospitalClassList');
        $this->assign([
            'data' => $responseData['data'],
        ]);
        return $this->fetch();
    }

    //添加
    public function add(Request $request)
    {
        if ($request->isPost()) {
            $responseData = $this->getServerData('/admin/hospitalClassManage/createHospitalClass', null, true);
            return json($responseData);
        }
        return $this->fetch();
    }

    //修改
    public function edit(Request $request)
    {

        if ($request->isPost()) {
            $responseData = $this->getServerData('/admin/hospitalClassManage/updateHospitalClass', null, true);
            return json($responseData);
        }
        $responseData = $this->getServerData('/admin/hospitalClassManage/getHospitalClassDetail');
        $this->assign([
            'data' => $responseData['data'],
        ]);
        return $this->fetch('add');

    }


    //删除
    public function del($id)
    {
        $responseData = $this->getServerData('/admin/hospitalClassManage/deleteHospitalClass', null, true);
        return json($responseData);
    }

}
