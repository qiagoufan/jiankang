<?php
/**
 * 后台用户控制器
 * @author yupoxiong<i@yufuping.com>
 */

namespace app\admin\controller;

use think\Request;
use tools\UploadImage;

class AdminKnowledgeController extends Controller
{

    //列表
    public function index(Request $request)
    {

        $responseData = $this->getServerData('/admin/knowledgeManage/getKnowledgeList');
        //关键词，排序等赋值
        $this->assign([
            'data' => $responseData['data'],
        ]);
        return $this->fetch();
    }

    //添加
    public function add(Request $request)
    {
        if ($request->isPost()) {
            $params = $request->param();
            $params['knowledge_cover'] = UploadImage::uploadFile('knowledge_cover');
            $responseData = $this->getServerData('/admin/knowledgeManage/createKnowledge', $params, true);
            return json($responseData);
        }
        $responseData = $this->getServerData('/admin/knowledgeManage/getKnowledgeCategoryList');
        $this->assign([
            'category_list' => $responseData['data'],
        ]);
        return $this->fetch();
    }

    //修改
    public function edit(Request $request)
    {

        if ($request->isPost()) {
            $params = $request->param();
            $params['knowledge_cover'] = UploadImage::uploadFile('knowledge_cover');
            $responseData = $this->getServerData('/admin/knowledgeManage/updateKnowledge', $params, true);
            return json($responseData);
        }

        $detail = $this->getServerData('/admin/knowledgeManage/getKnowledgeDetail');
        $detailInfo = $detail['data'];
        $detailInfo['knowledge_content'] = htmlspecialchars_decode($detailInfo['knowledge_content']);
        $categorylist = $this->getServerData('/admin/knowledgeManage/getKnowledgeCategoryList');
        $this->assign([
            'category_list' => $categorylist['data'],
            'data' => $detailInfo
        ]);
        return $this->fetch('add');

    }


    //删除
    public function del($id)
    {
        $responseData = $this->getServerData('/admin/knowledgeManage/deleteKnowledge', null, true);
        return json($responseData);
    }

    //推荐
    public function publish(Request $request)
    {

        $param = ['id' => $request->param('id'), 'publish_status' => 1];
        $responseData = $this->getServerData('/admin/knowledgeManage/updateKnowledgePublishStatus', $param, true);
        return json($responseData);
    }

    //取消推荐
    public function cancel_publish(Request $request)
    {

        $param = ['id' => $request->param('id'), 'publish_status' => 0];
        $responseData = $this->getServerData('/admin/knowledgeManage/updateKnowledgePublishStatus', $param, true);
        return json($responseData);
    }
}
