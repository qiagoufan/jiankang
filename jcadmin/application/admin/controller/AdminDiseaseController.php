<?php

namespace app\admin\controller;

use think\Request;

class AdminDiseaseController extends Controller
{

    //列表
    public function index(Request $request)
    {
        $responseData = $this->getServerData('/admin/diseaseManage/getDiseaseList');

        $this->assign([
            'data' => $responseData['data'],
            'disease_name' => $request->param('disease_name'),
            'class_name' => $request->param('class_name')
        ]);
        return $this->fetch();
    }

    //添加
    public function add(Request $request)
    {
        if ($request->isPost()) {
            $responseData = $this->getServerData('/admin/diseaseManage/createDisease', null, true);
            return json($responseData);
        }

        $classListData = $this->getServerData('/admin/hospitalClassManage/getHospitalClassList');
        $this->assign([
            'class_list' => $classListData['data'],
        ]);
        return $this->fetch();
    }

    //修改
    public function edit(Request $request)
    {

        if ($request->isPost()) {
            $responseData = $this->getServerData('/admin/diseaseManage/updateDisease', null, true);
            return json($responseData);
        }
        $responseData = $this->getServerData('/admin/diseaseManage/getDiseaseDetail');
        $classListData = $this->getServerData('/admin/hospitalClassManage/getHospitalClassList');
        $this->assign([
            'class_list' => $classListData['data'],
            'data' => $responseData['data']
        ]);
        return $this->fetch('add');

    }


    //删除
    public function del($id)
    {
        $responseData = $this->getServerData('/admin/diseaseManage/deleteDisease', null, true);
        return json($responseData);
    }

    //相关商品列表
    public function product(Request $request)
    {
        if ($request->isPost()) {
            $params = $request->param();
            $productInfoList = $request->param('product_info');
            $params['product_id'] = [];
            foreach ($productInfoList as $productInfo) {
                if ($productInfo != null) {
                    preg_match('#\((.*?)\)#', $productInfo, $match);
                    $productId = $match[1];
                    array_push($params['product_id'], $productId);
                }
            }
            $responseData = $this->getServerData('/admin/diseaseManage/updateDiseaseRelatedProductList', $params, true);
            return json($responseData);
        }

        $responseData = $this->getServerData('/admin/diseaseManage/getDiseaseRelatedProductList', null, true);
        $this->assign([
            'data' => $responseData['data']
        ]);
        return $this->fetch();
    }


}
