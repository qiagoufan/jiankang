<?php
/**
 * 后台用户控制器
 * @author yupoxiong<i@yufuping.com>
 */

namespace app\admin\controller;

use think\Request;

class AdminFeedCategoryController extends Controller
{

    //列表
    public function index(Request $request)
    {

        $responseData = $this->getServerData('/admin/feedManage/getFeedCategoryList');
        //关键词，排序等赋值
        $this->assign([
            'data' => $responseData['data'],
        ]);
        return $this->fetch();
    }

    //添加
    public function add(Request $request)
    {
        if ($request->isPost()) {
            $params = $this->request->param();
            $responseData = $this->getServerData('/admin/feedManage/createFeedCategory', $params, true);
            return json($responseData);
        }
        return $this->fetch();
    }

    //修改
    public function edit(Request $request)
    {

        if ($request->isPost()) {
            $params = $this->request->param();
            $responseData = $this->getServerData('/admin/feedManage/updateFeedCategory', $params, true);
            return json($responseData);
        }
        $responseData = $this->getServerData('/admin/feedManage/getFeedCategoryDetail');
        $this->assign([
            'data' => $responseData['data'],
        ]);
        return $this->fetch('add');

    }


    //删除
    public function del($id)
    {
        $responseData = $this->getServerData('/admin/feedManage/deleteFeedCategory', null, true);
        return json($responseData);
    }

}
