<?php
/**
 * 后台操作日志控制器
 * @author yupoxiong<i@yufuping.com>
 */

namespace app\admin\controller;

use think\Request;
use tools\UploadImage;

class AdminProductCategoryController extends Controller
{

    public function index()
    {
        $responseData = $this->getServerData('/admin/categoryManage/getProductCategoryList');
        //关键词，排序等赋值
        $this->assign([
            'data' => $responseData['data'],
        ]);
        return $this->fetch();
    }

    //添加
    public function add(Request $request)
    {
        if ($request->isPost()) {
            $params = $this->request->param();
            $params['category_cover'] = UploadImage::uploadFile('category_cover');
            $responseData = $this->getServerData('/admin/categoryManage/addProductCategory', $params, true);
            return json($responseData);
        }
        $responseData = $this->getServerData('/admin/categoryManage/getProductCategoryList');
        $this->assign([
            'category_list' => $responseData['data'],
        ]);
        return $this->fetch();
    }

    //编辑
    public function edit(Request $request)
    {

        if ($request->isPost()) {
            $params = $request->param();
            $params['category_cover'] = UploadImage::uploadFile('category_cover');
            $responseData = $this->getServerData('/admin/categoryManage/updateProductCategory', $params, true);
            return json($responseData);
        }
        $responseData = $this->getServerData('/admin/categoryManage/queryProductCategory');
        $categoryData = $this->getServerData('/admin/categoryManage/getProductCategoryList');
        $this->assign([
            'data' => $responseData['data'],
            'category_list' => $categoryData['data']
        ]);


        return $this->fetch('edit');

    }


    //删除
    public function del(Request $request)
    {
        $param = ['category_id' => $request->param('id')];
        $responseData = $this->getServerData('/admin/categoryManage/deleteProductCategory', $param, true);
        return json($responseData);
    }

}
