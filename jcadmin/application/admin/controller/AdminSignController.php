<?php
/**
 * 后台用户控制器
 * @author yupoxiong<i@yufuping.com>
 */

namespace app\admin\controller;

use think\Request;
use tools\UploadImage;

class AdminSignController extends Controller
{

    //列表
    public function index()
    {
        $responseData = $this->getServerData('/admin/jkSignInManage/getSignTypeList');
        //关键词，排序等赋值
        $this->assign([
            'data' => $responseData['data'],
        ]);
        return $this->fetch();
    }

    //添加
    public function add(Request $request)
    {
        if ($request->isPost()) {
            $imageUrl = UploadImage::uploadFile('sign_type_bg');
            $params = $this->request->param();
            $params['sign_type_bg'] = $imageUrl;
            $responseData = $this->getServerData('/admin/jkSignInManage/createSignType', $params, true);
            return json($responseData);
        }
        return $this->fetch();
    }

    //修改
    public function edit($id, Request $request)
    {

        if ($request->isPost()) {
            $params = $this->request->param();
            $params['sign_type_bg'] = UploadImage::uploadFile('sign_type_bg');
            $responseData = $this->getServerData('/admin/jkSignInManage/updateSignType', $params, true);
            return json($responseData);
        }
        $responseData = $this->getServerData('/admin/jkSignInManage/getSignTypeDetail');
        $this->assign([
            'data' => $responseData['data'],
        ]);


        return $this->fetch('add');

    }


    //删除
    public function del($id)
    {
        $responseData = $this->getServerData('/admin/jkSignInManage/deleteSignType', null, true);
        return json($responseData);
    }

    public function tips_index()
    {
        $responseData = $this->getServerData('/admin/jkSignInManage/querySignTipsList');
        //关键词，排序等赋值
        $this->assign([
            'data' => $responseData['data'],
            'sign_in_type_id' => $this->request->param('sign_type_id')
        ]);
        return $this->fetch();
    }

    public function tips_edit(Request $request)
    {
        if ($request->isPost()) {
            $params = $this->request->param();
            $params['image'] = UploadImage::uploadFile('image');
            $params['show_date'] = str_replace('-', '', $request->param('show_date'));
            $responseData = $this->getServerData('/admin/jkSignInManage/updateSignTips', $params, true);
            return json($responseData);
        }
        $responseData = $this->getServerData('/admin/jkSignInManage/querySignTips');
        $responseData['data']['show_date'] = date('Y-m-d', strtotime($responseData['data']['show_date']));
        $this->assign([
            'data' => $responseData['data'],
        ]);

        return $this->fetch('tips_add');
    }

    //添加
    public function tips_add(Request $request)
    {
        if ($request->isPost()) {
            $params = $request->param();
            $params['image'] = UploadImage::uploadFile('image');
            $params['show_date'] = str_replace('-', '', $request->param('show_date'));
            $responseData = $this->getServerData('/admin/jkSignInManage/createSignTips', $params, true);
            return json($responseData);
        }
        return $this->fetch();
    }

    //删除
    public function tips_del(Request $request)
    {
        $param = ['sign_tips_id' => $request->param('id')];
        $responseData = $this->getServerData('/admin/jkSignInManage/deleteSignTips', $param, true);
        return json($responseData);
    }
}
