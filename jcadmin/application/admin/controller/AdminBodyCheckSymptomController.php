<?php
/**
 * 后台用户控制器
 * @author yupoxiong<i@yufuping.com>
 */

namespace app\admin\controller;

use think\Request;
use tools\UploadImage;

class AdminBodyCheckSymptomController extends Controller
{

    //列表
    public function index(Request $request)
    {

        $responseData = $this->getServerData('/admin/bodyCheckManage/querySymptomList');
        $bodyListData = $this->getServerData('/admin/bodyCheckManage/getBodyList');

        //关键词，排序等赋值
        $this->assign([
            'data' => $responseData['data'],
            'body_list' => $bodyListData['data'],
            'keyword' => $request->param('keyword'),
            'human_body_id' => $request->param('human_body_id')
        ]);
        return $this->fetch();
    }

    //添加
    public function add(Request $request)
    {
        if ($request->isPost()) {
            $params = $request->param();
            $image = UploadImage::uploadFile('symptom_image');
            $params['symptom_image'] = $image;
            $responseData = $this->getServerData('/admin/bodyCheckManage/createSymptom', $params, true);
            return json($responseData);
        }
        $bodyListData = $this->getServerData('/admin/bodyCheckManage/getBodyList');
        $this->assign([
            'body_list' => $bodyListData['data'],
        ]);
        return $this->fetch();
    }

    //修改
    public function edit(Request $request)
    {

        if ($request->isPost()) {
            $params = $request->param();
            $params['symptom_id'] = $request->param('id');
            $image = UploadImage::uploadFile('symptom_image');
            if ($image) {
                $params['symptom_image'] = $image;
            }
            $responseData = $this->getServerData('/admin/bodyCheckManage/updateSymptom', $params, true);
            return json($responseData);
        }
        $bodyListData = $this->getServerData('/admin/bodyCheckManage/getBodyList');
        $params = $request->param();
        $params['symptom_id'] = $request->param('id');
        $responseData = $this->getServerData('/admin/bodyCheckManage/querySymptomDetail', $params, true);
        $this->assign([
            'body_list' => $bodyListData['data'],
            'data' => $responseData['data']
        ]);
        return $this->fetch('add');

    }


    //删除
    public function del(Request $request)
    {
        $params = ['symptom_id' => $request->param('id')];
        $responseData = $this->getServerData('/admin/bodyCheckManage/deleteSymptom', $params, true);
        return json($responseData);
    }

    public function question(Request $request)
    {

        if ($request->isPost()) {
            $params = $request->param();
            $params['symptom_id'] = $request->param('id');
            $questionCount = $request->param('question_count');
            $questionList = [];

            for ($i = 1; $i <= $questionCount; $i++) {
                $questionInfo = new \stdClass();
                $questionInfo->question_title = $request->param('question' . $i);
                if ($questionInfo->question_title == null) {
                    continue;
                }
                $questionInfo->question_option = [];
                $questionInfo->sort_num = $i;
                $options = $request->param('option' . $i);
                $scores = $request->param('score' . $i);
                for ($j = 0; $j < count($options); $j++) {
                    $optionInfo = new \stdClass();
                    $optionInfo->content = $options[$j];
                    $optionInfo->score = $scores[$j];
                    array_push($questionInfo->question_option, $optionInfo);
                }
                array_push($questionList, $questionInfo);
            }
            $params['question_list'] = json_encode($questionList);
            $responseData = $this->getServerData('/admin/bodyCheckManage/updateBodyCheckQuestion', $params, true);
            return json($responseData);
        }
        $params = $request->param();
        $params['symptom_id'] = $request->param('id');
        $responseData = $this->getServerData('/admin/bodyCheckManage/querySymptomDetail', $params, true);
        $questionListData = $this->getServerData('/admin/bodyCheckManage/querySymptomQuestionOptionList', $params, true);
        $this->assign([
            'data' => $responseData['data'],
            'question_list' => $questionListData['data']
        ]);
        return $this->fetch();
    }

    //修改
    public function check_result(Request $request)
    {
        if ($request->isPost()) {
            $params = $request->param();
            $scoreCount = 10;
            $scoreList = [];

            for ($i = 1; $i <= $scoreCount; $i++) {
                $scoreInfo = new \stdClass();
                $scoreInfo->disease_id = $request->param('disease_id' . $i);
                if ($scoreInfo->disease_id == null) {
                    continue;
                }
                $scoreInfo->start_score = $request->param('start_score' . $i);
                $scoreInfo->end_score = $request->param('end_score' . $i);
                array_push($scoreList, $scoreInfo);
            }
            $params['result_list'] = json_encode($scoreList);
            $params['symptom_id'] = $request->param('id');
            $responseData = $this->getServerData('/admin/bodyCheckManage/updatePossibleDisease', $params, true);
            return json($responseData);
        }
        $diseaseListParams = ['page_size' => 2000];
        $diseaseList = $this->getServerData('/admin/diseaseManage/getDiseaseList', $diseaseListParams);
        $params = $request->param();
        $params['symptom_id'] = $request->param('id');
        $responseData = $this->getServerData('/admin/bodyCheckManage/querySymptomDetail', $params, true);
        $possibleDiseaseData = $this->getServerData('/admin/bodyCheckManage/queryPossibleDiseaseList', $params, true);
        $possibleDiseaseList = $this->buildPossibleDiseaseList($possibleDiseaseData);
        $this->assign([
            'data' => $responseData['data'],
            'disease_list' => $diseaseList['data'],
            'possible_disease_list' => $possibleDiseaseList
        ]);

        return $this->fetch();

    }

    private function buildPossibleDiseaseList($possibleDiseaseData)
    {
        if ($possibleDiseaseData['data'] == null || $possibleDiseaseData['data']['list'] == null) {
            return [];
        }
        $diseaseList = [];
        $diseaseIdList = [];
        foreach ($possibleDiseaseData['data']['list'] as $item) {
            if (in_array($item['disease_id'], $diseaseIdList)) {
                continue;
            }
            $diseaseInfo = [];
            $diseaseInfo['disease_id'] = $item['disease_id'];
            $diseaseInfo['start_score'] = $item['start_score'];
            $diseaseInfo['end_score'] = $item['end_score'];
            array_push($diseaseList, $diseaseInfo);
            array_push($diseaseIdList, $item['disease_id']);

        }
        return $diseaseList;
    }

}
