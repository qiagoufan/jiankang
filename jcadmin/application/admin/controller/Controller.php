<?php
/**
 * 后台基础控制器
 * @author yupoxiong<i@yufuping.com>
 */

namespace app\admin\controller;

use app\admin\model\AdminMenu;
use app\admin\model\AdminUser;
use app\admin\traits\{AdminAuth, AdminTree, PhpOffice};
use facade\response\Result;
use think\Container;
use think\db\Query;
use think\exception\DbException;
use think\Paginator;
use tools\CurlUtil;

class Controller extends \think\Controller
{
    use AdminAuth, AdminTree, PhpOffice;


    /**
     * 当前url
     * @var string
     */
    protected $url;

    /**
     * 当前用户ID
     * @var int
     */
    protected $uid = 0;

    /**
     * 当前用户
     * @var AdminUser
     */
    protected $user;


    /**
     * 后台变量
     * @var array
     */
    protected $admin;

    /**
     * 无需验证权限的url
     * @var array
     */
    protected $authExcept = [];

    //初始化基础控制器
    protected function initialize(): void
    {
        $request = $this->request;
        //获取当前访问url
        $this->url = parse_name($request->module()) . '/' .
            parse_name($request->controller()) . '/' .
            parse_name($request->action());

        //验证权限
        if (!in_array($this->url, $this->authExcept, true)) {
            if (!$this->isLogin()) {
                error('未登录', 'auth/login');
            } else if ($this->user->id !== 1 && !$this->authCheck($this->user)) {
                error('无权限', $this->request->isGet() ? null : URL_CURRENT);
            }
        }

        if ((int)$request->param('check_auth') === 1) {
            success();
        }

        //记录日志
        $menu = AdminMenu::get(['url' => $this->url]);
        if ($menu) {
            $this->admin['title'] = $menu->name;
            if ($menu->log_method === $request->method()) {
                $this->createAdminLog($this->user, $menu);
            }
        }

        $this->admin['per_page'] = cookie('admin_per_page') ?? 10;
        $this->admin['per_page'] = $this->admin['per_page'] < 100 ? $this->admin['per_page'] : 100;

    }


    //重写fetch
    protected function fetch($template = '', $vars = [], $config = [])
    {
        $this->admin['pjax'] = $this->request->isPjax();
        $this->admin['user'] = $this->user;
        $this->setAdminInfo();
        if ('admin/auth/login' !== $this->url && !$this->admin['pjax']) {
            $this->admin['menu'] = $this->getLeftMenu($this->user);
        }

        $this->assign([
            'debug' => config('app.app_debug') ? 'true' : 'false',
            'cookie_prefix' => config('cookie.prefix') ?? '',
            'admin' => $this->admin,
        ]);

        return parent::fetch($template, $vars, $config);
    }

    //空方法
    public function _empty()
    {
        $this->admin['title'] = '404';
        return $this->fetch('template/404');
    }


    //设置前台显示的后台信息
    protected function setAdminInfo(): void
    {
        $admin_config = config('admin.base');

        $this->admin['name'] = $admin_config['name'] ?? '';
        $this->admin['author'] = $admin_config['author'] ?? '';
        $this->admin['version'] = $admin_config['version'] ?? '';
        $this->admin['short_name'] = $admin_config['short_name'] ?? '';
    }

    protected function getServerData($url_path, $param = null, $dataTranslation = false)
    {
        $url = config("app.server_domain") . $url_path;
        if ($param == null) {
            $param = $this->request->param();
        }
        if (isset($param['index']) && $param['index']) {
            $param['index'] = $param['index'] - 1;
        }
        $data = http_build_query($param);
        $response = CurlUtil::post($url, $data);
        if ($response == null) {
            return $response;
        }
        $responseArray = json_decode($response, true);
        if (isset($responseArray['data']['page_info'])) {
            $responseArray['data']['paginator'] = $this->getPaginator($responseArray['data']['page_info'], $param);
        }
        if ($dataTranslation) {
            $code = 1;
            $returnUrl = "url://back";
            $msg = '操作成功';
            if ($responseArray['errorCode'] != 0) {
                $code = 0;
                $returnUrl = 'url://current';
                $msg = $responseArray['errorMessage'];
            }
            $returnData = [
                'code' => $code,
                'data' => $responseArray['data'],
                'msg' => $msg,
                'url' => $returnUrl,
                'wait' => 0];
            return $returnData;
        }
        return $responseArray;
    }


    /**
     * 分页查询
     * @access public
     * @param int|array $listRows 每页数量 数组表示配置参数
     * @param int|bool $simple 是否简洁模式或者总记录数
     * @param array $config 配置参数
     *                            page:当前页,
     *                            path:url路径,
     *                            query:url额外参数,
     *                            fragment:url锚点,
     *                            var_page:分页变量,
     *                            list_rows:每页数量
     *                            type:分页类名
     * @return $this[]|\think\Paginator
     */
    private function getPaginator($page_info, $param)
    {
        $simple = false;
        $config = ['list_rows' => $page_info['page_size'], 'type' => '\tools\BearPage', 'var_page' => 'index', 'query' => $param];
        $paginate = Container::get('config')->pull('paginate');


        /** @var Paginator $class */
        $class = false !== strpos($config['type'], '\\') ? $config['type'] : '\\think\\paginator\\driver\\' . ucwords($config['type']);
        $page = $this->request->param('index');
        $page = $page < 1 ? 1 : $page;

        $config['path'] = isset($config['path']) ? $config['path'] : call_user_func([$class, 'getCurrentPath']);
        $listRows = $page_info['page_size'];
        $total = $page_info['count'];
        $data = $class::make(null, $listRows, $page, $total, $simple, $config);
        return $data->render();
    }
}
