<?php
/**
 * 后台用户控制器
 * @author yupoxiong<i@yufuping.com>
 */

namespace app\admin\controller;

use think\Request;
use tools\UploadImage;

class AdminRiskCheckController extends Controller
{

    //列表
    public function index()
    {

        $responseData = $this->getServerData('/admin/riskCheckManage/getRiskCheckList');
        $this->assign([
            'data' => $responseData['data'],
        ]);
        return $this->fetch('index');
    }

    //添加
    public function add(Request $request)
    {
        if ($request->isPost()) {
            $params = $request->param();
            $questionCount = $request->param('question_count');
            $questionList = [];

            for ($i = 1; $i <= $questionCount; $i++) {
                $questionInfo = new \stdClass();
                $questionInfo->question_title = $request->param('question' . $i);
                if ($questionInfo->question_title == null) {
                    continue;
                }
                $questionInfo->question_option = [];
                $questionInfo->sort_num = $i;
                $options = $request->param('option' . $i);
                $scores = $request->param('score' . $i);
                $option_desc = $request->param('option_desc' . $i);
                for ($j = 0; $j < count($options); $j++) {
                    $optionInfo = new \stdClass();
                    $optionInfo->symptom = $options[$j];
                    $optionInfo->score = $scores[$j];
                    $optionInfo->option_desc = $option_desc[$j];
                    $optionInfo->option_type = 'single';
                    array_push($questionInfo->question_option, $optionInfo);
                }
                array_push($questionList, $questionInfo);
            }
            $params['question_list'] = json_encode($questionList);
            $responseData = $this->getServerData('/admin/riskCheckManage/createRiskCheckQuestion', $params, true);


            return json($responseData);
        }

        $responseData = $this->getServerData('/admin/riskCheckManage/getRiskCheckCategoryList');
        $this->assign([
            'category_list' => $responseData['data'],
        ]);
        return $this->fetch('');
    }

    //修改
    public function edit(Request $request)
    {

        if ($request->isPost()) {
            $params = $request->param();
            $questionCount = $request->param('question_count');
            $questionList = [];

            for ($i = 1; $i <= $questionCount; $i++) {
                $questionInfo = new \stdClass();
                $questionInfo->question_title = $request->param('question' . $i);
                if ($questionInfo->question_title == null) {
                    continue;
                }

                $questionInfo->question_option = [];
                $questionInfo->sort_num = $i;
                $options = $request->param('option' . $i);
                $scores = $request->param('score' . $i);
                $option_desc = $request->param('option_desc' . $i);
                for ($j = 0; $j < count($options); $j++) {
                    $optionInfo = new \stdClass();
                    $optionInfo->symptom = $options[$j];
                    $optionInfo->score = $scores[$j];
                    $optionInfo->option_desc = $option_desc[$j];
                    $optionInfo->option_type = 'single';
                    array_push($questionInfo->question_option, $optionInfo);
                }
                array_push($questionList, $questionInfo);
            }
            $params['question_list'] = json_encode($questionList);
            $responseData = $this->getServerData('/admin/riskCheckManage/updateRiskCheckQuestion', $params, true);
            return json($responseData);
        }
        $categoryData = $this->getServerData('/admin/riskCheckManage/getRiskCheckCategoryList');
        $responseData = $this->getServerData('/admin/riskCheckManage/getRiskCheckDetail');
        $this->assign([
            'category_list' => $categoryData['data'],
            'data' => $responseData['data']
        ]);

        return $this->fetch();

    }


    //删除
    public function del($id)
    {
        $responseData = $this->getServerData('/admin/riskCheckManage/deleteRiskCheck', null, true);
        return json($responseData);
    }


    //修改
    public function check_result(Request $request)
    {
        if ($request->isPost()) {
            $params = $request->param();
            $scoreCount = $request->param('score_count');
            $scoreList = [];

            for ($i = 1; $i <= $scoreCount; $i++) {
                $scoreInfo = new \stdClass();
                $scoreInfo->disease = $request->param('disease' . $i);
                if ($scoreInfo->disease == null) {
                    continue;
                }

                $scoreInfo->start_score = $request->param('start_score' . $i);
                $scoreInfo->end_score = $request->param('end_score' . $i);
                $scoreInfo->suggest = $request->param('suggest' . $i);
                $productInfoList = $request->param('bind_product_' . $i);
                $scoreInfo->product_list = [];
                foreach ($productInfoList as $productInfo){
                    if ($productInfo != null) {
                        preg_match('#\((.*?)\)#', $productInfo, $match);
                        $productId = $match[1];
                        array_push($scoreInfo->product_list, $productId);
                    }
                }


                array_push($scoreList, $scoreInfo);
            }
            $params['result_list'] = json_encode($scoreList);
            $responseData = $this->getServerData('/admin/riskCheckManage/updateRiskCheckResult', $params, true);
            return json($responseData);
        }
        $responseData = $this->getServerData('/admin/riskCheckManage/getRiskCheckResult');
        $this->assign([
            'data' => $responseData['data']
        ]);


        return $this->fetch();

    }
}
