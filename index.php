<?php
define('APP_PATH', dirname(__FILE__) . DIRECTORY_SEPARATOR);
define('PUBLIC_PATH', APP_PATH . 'public' . DIRECTORY_SEPARATOR);
define('CORE_PATH', PUBLIC_PATH . 'core' . DIRECTORY_SEPARATOR);
define('TPL_PATH', APP_PATH . 'tpl' . DIRECTORY_SEPARATOR);
define('MODEL_PATH', APP_PATH . 'model' . DIRECTORY_SEPARATOR);
define('CONTROLLER_PATH', APP_PATH . 'controller' . DIRECTORY_SEPARATOR);
define('CONF_PATH', APP_PATH . 'config' . DIRECTORY_SEPARATOR);
define('API_PATH', APP_PATH . 'api' . DIRECTORY_SEPARATOR);
define('SERVER_TYPE', get_cfg_var('server_type'));

require CORE_PATH . 'core.php';
require 'vendor/autoload.php';
if (!isset($_SERVER['SHELL'])) {
    FUR_Core::init();
}
