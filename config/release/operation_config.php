<?php
return [
    'index_page' => [
        'index_star_list' => [
            [48, 53, 28, 1, 54, 3],
            [7, 8, 9, 10, 11]
        ],
        'index_super_product_sku_list' => [
            [3], [348]
        ],
        'index_normal_product_sku_list' => [
            [214, 218, 230],
            [222, 226],
            [234, 238]
        ],
        /**
         * 首页运营位feed
         */
        'index_operating_feed_list' => [5 => 442]
    ],
    'search' => [
        'recommend_keywords' => ['张碧晨', '吴莫愁', '艺术舍得']
    ]

]; 