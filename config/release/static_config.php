<?php


return array(
    'theatre' => [
        'daily_pictorial_top_img' => 'https://cdn.ipxmall.com/system/tag_calendar.png',
        'upcoming_background_img' => 'https://cdn.ipxmall.com/system/theatre_upcoming.png',
        'new_product_top_img' => 'https://cdn.ipxmall.com/system/new_product.png',
        'super_product_top_img' => 'https://cdn.ipxmall.com/system/super_product.png',
        'activity_notice_top_img' => 'https://cdn.ipxmall.com/system/upcoming_notice.png',
        'upcoming_notice_top_img' => 'https://ipxcdn.jfshare.com/ipxmall/e748f6921cda05eb801be3a439077e4c',
        'challenge_hashtag_icon' => 'https://ipxcdn.jfshare.com/ipxmall/f66e19745dea780a498af31e28dee8fa',
        'challenge_top_icon' => 'https://ipxcdn.jfshare.com/ipxmall/ea1ed1c0ecceb222ac2cfb61d788fc3b'
    ],
    'feed' => [
        'icon_tag_product' => 'https://cdn.ipxmall.com/system/tag_evaluate_20200526.png',
        'icon_tag_super_product' => 'https://cdn.ipxmall.com/system/tag_super_product_20200527.png',
        'checking_feed_status_img' => 'https://cdn.ipxmall.com/system/tag_reviewing.png',
        'icon_content_tag_original' => 'https://cdn.ipxmall.com/system/tag_original.png',
        'icon_content_tag_exclusive' => 'https://cdn.ipxmall.com/system/tag_exclusive.png',
        'icon_content_tag_top' => 'https://cdn.ipxmall.com/system/tag_top.png',
        'icon_tag_video' => 'https://cdn.ipxmall.com/system/tag_video3x_20200521.png',
        'icon_tag_review' => 'https://cdn.ipxmall.com/system/tag_evaluate_20200528.png',
        'icon_tag_teaser' => 'https://cdn.ipxmall.com/system/tag_activity3x_20200521.png',
        'icon_tag_information' => 'https://cdn.ipxmall.com/system/tag_news_20200528.png',
        'icon_tag_image' => 'https://cdn.ipxmall.com/system/tag_Photo.png',
        'icon_hash_tag_image' => 'https://cdn.ipxmall.com/ipxmall/541f61b903a087c38730156eb7773628',
        'icon_hash_tag_link_image' => 'https://ipxcdn.jfshare.com/ipxmall/8770f15e630390bb5f615676520a193c',
        'icon_hash_tag_ip_image' => 'https://ipxcdn.jfshare.com/ipxmall/0d44e2e25f03e06ad183adfaf2a96283',
        'header_icon_jingxuan' => 'https://ipxcdn.jfshare.com/ipxmall/d45aa84f66803463118f16a7a2ff772b',
        'header_icon_sp' => 'https://ipxcdn.jfshare.com/ipxmall/ee0c1afae5a521d1d9d3a3a701c25c1b',
        'header_icon_com' => 'https://ipxcdn.jfshare.com/ipxmall/e43643744e7baf6bf522187da5dabe69',
        'header_icon_leaderboard' => 'https://ipxcdn.jfshare.com/ipxmall/74872f8758de063371d151b8e6ae9b10',
        'header_icon_star_energy' => 'https://ipxcdn.jfshare.com/ipxmall/41b25f52484601564af4b1bff1ba0459',
        'header_icon_kjlm' => 'https://ipxcdn.jfshare.com/ipxmall/ea78344df3061f4014a4e0562eafa4bd',
        'icon_dujia' => 'https://ipxcdn.jfshare.com/ipxmall/7a354cc2953b36b13117eee3ac400891',
        'icon_jiugongge' => 'https://ipxcdn.jfshare.com/ipxmall/affe094b368457a84049b30f18623709',
    ],
    'video' => [
        'default_cover' => 'https://cdn.ipxmall.com/system/pic_gray_background.jpg',
        'transcoding_cover' => 'https://cdn.ipxmall.com/system/transcoding.jpg'
    ],
    'user' => [
        'group_owner_medal' => 'https://ipxcdn.jfshare.com/ipxmall/aecd938b4709c8520399c242936afde6',
        'group_manager_medal' => 'https://ipxcdn.jfshare.com/ipxmall/a6639ae0dc471f81a5332dc4beb8b998'
    ],
    'product' => [
        'type_icon_star_selection' => 'https://ipxcdn.jfshare.com/ipxmall/c27b44382a7f07403e17abb92f4e985f',
        'type_icon_ip' => 'https://ipxcdn.jfshare.com/ipxmall/0d44e2e25f03e06ad183adfaf2a96283',
        'type_icon_super_product' => 'https://ipxcdn.jfshare.com/ipxmall/3d7f3d04274eea8b50f7a0732a3c9f7e'
    ]
);