<?php

/**
 * Class Config_Error
 * 负数 表示系统服务的错误,不建议展示给用户错误详情
 * 正数 表示业务层级的错误,建议展示给用户errormessage
 */
class Config_Error
{

    const SUCC = [0, "success"];

    const ERR_INVALID_TOKEN = [401, "您的登录已失效,请重新登录"];
    const ERR_TOKEN_EXPIRED = [401, "您的登录已过期,请重新登录"];
    const ERR_TOKEN_REFRESH_FAIL = [402, "请重新登录"];

    /**
     * 阿里云相关
     */
    const ERR_ALIYUN_ONE_CLICK_LOGIN_FAIL = [501, "一键登录验证失败"];
    const ERR_ALIYUN_SEND_SMS_FAIL = [502, "发送短信失败"];
    const ERR_ALIYUN_CREATE_UPLOAD_VIDEO_FAIL = [503, "创建视频上传请求失败"];
    const ERR_ALIYUN_REFRESH_UPLOAD_VIDEO_FAIL = [504, "刷新视频上传请求失败"];
    const ERR_ALIYUN_CREATE_UPLOAD_IMAGE_FAIL = [505, "刷新视频上传请求失败"];
    const ERR_ALIYUN_CREATE_UPLOAD_IMAGE_EXT_FAIL = [506, "上传的图片类型还不支持"];
    const ERR_ALIYUN_IMAGE_ID_IS_NULL = [-507, "image_id不能为空"];
    const ERR_ALIYUN_GET_IMAGE_INFO_FAIL = [508, "获取图片信息失败"];
    const ERR_ALIYUN_VIDEO_ID_IS_NULL = [-509, "video_id不能为空"];
    const ERR_ALIYUN_GET_VIDEO_INFO_FAIL = [510, "获取短视频信息失败"];
    const ERR_ALIYUN_GET_PLAY_INFO_FAIL = [511, "获取短视频播放信息失败"];
    const ERR_ALIYUN_CREATE_UPLOAD_IMAGE_SERVICE_FAIL = [512, "获取图片上传服务失败"];
    const ERR_ALIYUN_UPLOAD_OSS_FILE_FAIL = [513, "上传文件失败"];
    const ERR_ALIYUN_UPLOAD_BASE64_FILE_FAIL = [514, "上传base64文件失败"];

    /**
     * 手机号相关
     */
    const ERR_INVALID_PHONE = [1001, "请输入正确的手机号"];

    /**
     * 验证码相关类型
     */
    const ERR_VERIFICATION_VALID_CODE = [1101, "验证码无效"];
    const ERR_VERIFICATION_INVALID_ACTION = [1102, "非法操作"];
    const ERR_VERIFICATION_FREQUENT_ACTION = [1103, "操作过于频繁,请稍后再试"];
    const ERR_DAILY_MAX_SEND_COUNT = [1104, "今天已经达到了最大次数,请明天再试"];
    const ERR_VERIFICATION_EXPIRE_CODE = [1105, "验证码已过期"];
    const ERR_VERIFICATION_ERROR_CODE = [1106, "验证码错误,请重新输入"];
    const ERR_MAX_VERIFICATION_COUNT = [1107, "验证码输入错误"];

    /**
     * 用户相关
     */

    const ERR_USER_NOT_EXIST = [1201, "用户不存在"];
    const ERR_USER_REGISTER_FAIL = [1202, "注册失败"];
    const ERR_USER_SOCIALITE_NOT_REGISTER = [1203, "社区用户还未注册"];
    const ERR_USER_HAS_REGISTER = [1204, "手机号已被注册"];
    const ERR_USER_SOCIALITE_REGISTER_FAIL = [1205, "社区用户注册失败"];
    const ERR_VISIT_USER_IS_NOT_EXIST = [1206, "访问用户不存在"];
    const ERR_USER_STAR_NOT_EXIST = [1207, "明星还不存在"];
    const ERR_USER_HOME_PAGE_CAN_NOT_ACCESS = [1208, "个人主页未登录不能访问"];
    const ERR_USER_HAS_BIND_SOCIALITE_ACCOUNT = [1209, "您已经绑定过了该账号,请使用其他账号登录"];
    const ERR_USER_SOCIALITE_HAS_BIND = [1210, "这个社交账号已经被绑定了,请切换其他账号登录"];
    const ERR_USER_APPLE_SOCIALITE_LOGIN_FAIL = [1211, "苹果账号登录失败"];
    const ERR_USER_UPDATE_NICK_NAME_TIME_NOT_UP = [1212, "24小时内只能更新一次昵称"];
    const ERR_USER_NICK_NAME_HAS_EXIST = [1213, "这个昵称已经有人使用了"];
    const ERR_USER_NICK_NAME_TOO_LONG = [1214, "昵称过长,重新输入"];
    const ERR_USER_CANCEL_FAIL_BY_ORDER_COUNT = [1215, "您的订单信息异常,暂不能注销,请稍后再试"];
    const ERR_USER_CANCEL_FAIL_BY_ORDER_PROCESSING_COUNT = [1216, "您还有未完成的订单,请在订单完成后操作"];
    const ERR_USER_BLOCK_MAX = [1217, "已经达到黑名单用户上限"];
    const ERR_USER_WXMINI_DECODE_PHONE_FAIL = [1218, "微信小程序获取手机号失败"];
    const ERR_USER_WXMINI_API_ERROR = [1219, "微信小程序获取信息失败"];
    const ERR_USER_PHONE_HAS_BIND = [1220, "手机号已被绑定"];
    const ERR_USER_IS_INVALID = [1221, "用户信息无效"];
    const ERR_USER_MUST_BIND_PHONE = [1222, "请先绑定手机号"];
    const ERR_USER_NAME_IS_BLANK = [1223, "昵称不能为空"];
    const ERR_VISIT_USER_LIST_TYPE_IS_NOT_EXIST = [1224, "访问列表类型不能为空"];
    const ERR_PASSWORD_ERROR = [1235, "密码错误"];

    /**
     * follow相关
     */

    const ERR_FOLLOW_PARAMS_INVALID = [1301, "关注人不能为空"];
    const ERR_FOLLOW_CANT_BE_SELF = [1302, "你不能关注自己"];
    const ERR_FOLLOW_GROUP_OWNER_CANT_UNFOLLOW = [1303, "圈主/管理员无法取消关注"];

    /**
     * feed相关
     */

    const ERR_FEED_NOT_EXIST = [1401, "内容不存在"];
    const ERR_DELETE_FEED_USER_NOT_LOGIN = [1402, "请先登录"];
    const ERR_DELETE_FEED_NOT_EXIST = [1403, "你要删除的内容不存在"];
    const ERR_DELETE_FEED_NO_RIGHT = [1404, "删除失败"];
    const ERR_COLLECT_BIZ_ID_NOT_EXIST = [1405, "收藏失败"];
    const ERR_INFO_NOT_EXIST = [1406, "资讯已删除"];
    const ERR_FEED_NOT_USEFUL = [1402, "找不到对应的内容"];
    const ERR_CREATE_FEED_IS_NOT_ALLOWED = [1403, "你因违规操作，暂时无法使用发布功能"];


    /*
     * product相关
     */

    const ERR_PRODUCT_SPECIFIC_NOT_EXIST = [1501, "商品不存在"];
    const ERR_PRODUCT_ID_IS_NULL = [1502, "请选择你要查看的商品"];
    const ERR_PRODUCT_GALLERY_IS_NOT_EXIST = [1503, "商品主图不存在"];
    const ERR_PRODUCT_DETAIL_PIC_IS_NOT_EXIST = [1504, "商品详情图片不存在"];
    const ERR_PRODUCT_PRICE_IS_NOT_EXIST = [1505, "商品价格信息不存在"];
    const ERR_PRODUCT_SPECIFICATIONS_IS_NOT_EXIST = [1506, "商品规格信息不存在"];
    const ERR_PRODUCT_SPECIFIC_NOT_IN_DB = [1507, "库存不足，请重新选择"];
    const ERR_PRODUCT_ITEM_PARAMS_NOT_FULL = [1508, "创建产品规格参数不完整"];
    const ERR_PRODUCT_ITEM_ID_NOT_EXISTS = [1509, "产品item_id不能为空"];
    const ERR_PRODUCT_ITEM_INFO_NOT_FULL = [1510, "产品item信息不完整"];
    const ERR_PRODUCT_BASE_INFO_NOT_FULL = [1511, "产品基础信息不完整"];
    const ERR_PRODUCT_SUPER_INFO_NOT_FULL = [1511, "创建超级商品信息不完整"];
    const ERR_PRODUCT_GALLEY_IMAGE_IS_NULL = [1511, "轮播图片不能为空"];
    const ERR_PRODUCT_ITEM_LIST_TYPE_IS_NULL = [1512, "产品item参数不完整"];
    const ERR_STAR_FEED_IS_NULL = [1513, "明星物料不存在"];
    const ERR_COUNT_TOTAL_PRICE_PARAMS_IS_NULL = [1514, "查询价格参数不能为空"];
    const ERR_PRODUCT_SUPER_PRODUCT_INFO_IS_NOT_FULL = [1515, "超级商品信息不完整"];
    const ERR_PRODUCT_SKU_NOT_EXIST = [1516, "sku信息不存在"];
    const ERR_CREATE_ORDER_SHIPPING_ID_IS_NULL = [1517, "请选择订单物流信息"];
    const ERR_CREATE_ORDER_PRODUCT_STOCK_NOT_ENOUGH = [1518, "订单中商品库存不足,请重新选择"];
    const ERR_PRODUCT_SKU_NOT_PUBLISH = [1519, "sku未上架"];
    const ERR_PRODUCT_ITEM_IS_NOT_EXIST = [1520, "商品不存在"];
    const ERR_PRODUCT_EDIT_CATEGORY_LEVEL1_ERROR = [1531, "一级商品类目不能修改为二级类目"];
    const ERR_PRODUCT_EDIT_CATEGORY_LEVEL2_ERROR = [1531, "二级商品类目不能修改为一级类目"];

    /**
     * 商品评价相关
     */

    const ERR_REVIEW_SKU_NOT_EXIST = [1601, "请选择需要评价的商品"];
    const ERR_REVIEW_USER_ID_FAIL = [-1602, "评价的用户不能为空"];
    const ERR_REVIEW_CONTENT_IS_NULL = [1603, "评价内容不能为空"];
    const ERR_REVIEW_CONTENT_TOO_LONG = [1604, "评价内容不能超过150个字"];
    const ERR_REVIEW_CONTENT_IS_BLOCK = [1605, "评价内容含有不良内容"];
    const ERR_REVIEW_CONTENT_ORDER_IS_NULL = [1606, "评价的内容不能为空"];
    const ERR_REVIEW_CONTENT_ORDER_NO_RIGHT = [1607, "你不能评价其他人的订单"];
    const ERR_REVIEW_CONTENT_ORDER_NOT_RIGHT_STATUS = [1608, "订单还未付款,不能评价"];

    /**
     *社区账号
     */
    const ERR_SOCIALITE_PROVIDER_NOT_EXIST = [1701, "平台不存在"];
    const ERR_SOCIALITE_TOKEN_FAIL = [1702, "token无效"];
    const ERR_SOCIALITE_TOKEN_CHECK_FAIL = [1703, "token校验失败"];
    const ERR_SOCIALITE_GET_WECHAT_TOKEN_FAIL = [1704, "获取微信信息失败"];
    const ERR_SOCIALITE_GET_WECHAT_MINI_OPENID_FAIL = [1705, "获取微信小程序openid失败"];


    /**
     * 订单相关
     */
    const ERR_SHIPPING_INFO_IS_NOT_EXIST = [1801, "收货地址不能为空"];
    const ERR_PRODUCT_INFO_IS_NOT_EXIST = [1802, "订单商品信息不能为空"];
    const ERR_ORDER_INFO_IS_NOT_EXIST = [1803, "订单信息不存在"];
    const ERR_PAY_INFO_IS_NOT_EXITS = [1804, "支付信息错误"];
    const ERR_CREATE_ORDER_FAILED = [1805, "创建订单失败"];
    const ERR_ORDER_TYPE_IS_INCORRECT = [1806, "订单类型输入错误"];
    const ERR_CREATE_ORDER_PAY_INFO_FAILED = [1807, "创建支付信息失败"];
    const ERR_ORDER_SKU_COUNT_IS_ZERO = [1808, "请选择你要购买的商品数量"];
    const ERR_ORDER_PRODUCT_IS_INVALID = [1809, "您购买的商品异常,请重新选择"];
    const ERR_ORDER_PRODUCT_PRICE_INFO_IS_NOT_EXIST = [1810, "您购买的商品价格信息不存在"];
    const ERR_ORDER_PARAMS_IS_NOT_FULL = [1811, "查询订单信息参数不完整"];
    const ERR_DELETE_ORDER_FAIL = [1812, "删除订单失败"];
    const ERR_CONFIRM_RECEIPT_ORDER_INFO_IS_NOT_FULL = [1813, "确认收货信息不完整"];
    const ERR_CANCEL_ORDER_PARAMS_IS_NOT_FULL = [1814, "取消订单参数不完整"];
    const ERR_CANCEL_ORDER_IS_FAILED = [1815, "取消订单失败"];
    const ERR_ORDER_QUERY_USER_INFO_IS_NULL = [1816, "查询订单用户信息为空"];
    const ERR_ORDER_ID_CAN_NOT_BE_NULL = [1817, "订单id不能为空"];
    const ERR_ORDER_STOCK_IS_NOT_ENOUGH = [1818, "抱歉,商品库存数量不足,请重新选择"];
    const ERR_ORIGINAL_ORDER_PLATFORM_IS_WXMIN = [1819, '抱歉，请前往小程序进行支付'];
    const ERR_ORIGINAL_ORDER_PLATFORM_IS_WEB_PAGE = [1820, '抱歉，请前往网页端支付'];
    const ERR_ORIGINAL_ORDER_PLATFORM_IS_JS_API = [1821, '抱歉，请前往微信浏览器支付'];
    const ERR_UPDATE_ORDER_STOCK_IS_FAILED = [1822, '库存更新失败'];
    const ERR_CONFIRM_RECEIPT_ORDER_IS_FAILED = [1823, '确认收货失败'];

    /**
     * 标签系统
     */
    const ERR_TAG_INFO_IS_NULL = [1901, "标签信息不能为空"];
    const ERR_TAG_BIZ_IS_NULL = [1902, "标签业务信息不能为空"];
    const ERR_TAG_BIZ_IS_EXIST = [1903, "该业务标签已经存在"];
    const ERR_TAG_NAME_IS_EXIST = [1904, "标签名已经存在"];
    const ERR_QUERY_TAG_PARAMS_IS_NULL = [1905, "查询的标签信息不能为空"];
    const ERR_QUERY_TAG_ENTITY_PARAMS_IS_NULL = [1906, "查询标签列表参数不能为空"];
    const ERR_CREATE_TAG_ENTITY_PARAMS_IS_NULL = [1907, "打标实体信息不能为空"];
    const ERR_CREATE_TAG_ENTITY_TAG_IS_NOT_EXIST = [1908, "该业务标签不存在"];
    const ERR_DELETE_TAG_ENTITY_PARAMS_IS_NULL = [1909, "删除标信息不能为空"];
    const ERR_CREATE_TAG_ENTITY_TAG_HAS_EXIST = [1910, "该标记已经存在了"];
    const ERR_CREATE_TAG_ENTITY_TAG_ID_IS_NULL = [1911, "实体打标失败"];
    const ERR_CREATE_TAG_ENTITY_TAG_IS_NULL = [1912, "打标主体不能为空"];
    const ERR_CREATE_HOT_TAG_INFO_IS_NOT_FULL = [1913, "热门标签信息不完整"];
    const ERR_DELETE_HOT_TAG_ID_IS_NULL = [1913, "删除热门标签ID为空"];

    /**
     *创建feed
     */
    const ERR_CREATE_FEED_PARAMS_IS_NULL = [2001, "参数不能为空"];
    const ERR_CREATE_FEED_CONTENT_IS_NULL = [2002, "请描述你的问题"];
    const ERR_CREATE_FEED_IMAGE_IS_NULL = [2003, "请选择需要上传的素材"];
    const ERR_CREATE_FEED_TITLE_IS_NULL = [2004, "请输入问题标题"];
    const ERR_CREATE_FEED_CONTENT_IS_BLOCK = [2005, "素材中含有不良信息,请重新创建"];
    const ERR_CREATE_FEED_TAG_IS_NULL = [2006, "请选择标签"];
    const ERR_CREATE_FEED_TAG_TYPE_IS_NULL = [2007, "请选择标签类型"];
    const ERR_CREATE_FEED_TAG_TYPE_IS_ERROR = [2008, "标签类型错误"];
    const ERR_CREATE_FEED_TITLE_IS_TOO_LONG = [2009, "帖子标题不能超过20个字"];
    /**
     *媒体管理
     */
    const ERR_MEDIA_CREATE_VIDEO_PARAMS_IS_NULL = [2101, "视频参数不能为空"];
    const ERR_MEDIA_CREATE_VIDEO_PARAMS_IS_NOT_COMPLETE = [2102, "视频参数不完整"];
    const ERR_MEDIA_CREATE_IMAGE_PARAMS_IS_NULL = [2103, "创建图片信息"];
    const ERR_MEDIA_CREATE_IMAGE_PARAMS_IS_NOT_COMPLETE = [2104, "图片参数不完整"];
    const ERR_MEDIA_CREATE_IMAGE_FILE_NOT_EXISTS = [2105, "图片文件不存在"];
    const ERR_MEDIA_CREATE_IMAGE_HAS_EXISTS = [2106, "图片已经保存了"];
    const ERR_MEDIA_QUERY_IMAGE_FILE_NOT_FOUND = [2107, "图片信息无法找到"];
    const ERR_MEDIA_QUERY_VIDEO_FILE_NOT_FOUND = [2108, "视频信息无法找到"];
    const ERR_MEDIA_QUERY_VIDEO_NOT_EXIST = [2109, "视频不存在"];


    /**
     * 讨论区相关
     */

    const ERR_INTERACTIVE_COMMENT_IS_INVALID = [2201, '评论业务类型参数不能为空'];
    const ERR_INTERACTIVE_REPLY_IS_INVALID = [2202, '回复业务类型参数不能为空'];
    const ERR_INTERACTIVE_COMMENT_ID_IS_NULL = [2203, '评论ID不能为空'];
    const ERR_INTERACTIVE_BIZ_ID_IS_NULL = [2204, '业务ID不能为空'];
    const ERR_INTERACTIVE_COMMENT_INFO_IS_NOT_EXIST = [2205, '评论信息不存在'];
    const ERR_INTERACTIVE_OVERVIEW_INFO_IS_NOT_EXIST = [2206, '评论统计信息不存在'];
    const ERR_INTERACTIVE_BIZ_TYPE_IS_NULL = [2207, '业务类型不能为空'];
    const ERR_COMMENT_COUNT_INFO_IS_NULL = [2208, '评论数量统计信息为空'];
    const ERR_DELETE_COMMENT_PARAMS_INFO_NO_RIGHT = [2209, "删除商品评价参数不完整"];
    const ERR_INTERACTIVE_COMMENT_IS_NULL = [2210, "评论不能为空"];
    const ERR_INTERACTIVE_COMMENT_IS_BLOCK = [2211, "评论中含有不良信息"];
    const ERR_INTERACTIVE_REPLY_IS_BLOCK = [2212, "回复中含有不良信息"];
    const ERR_INTERACTIVE_IS_BLOCK = [2213, "你已被对方拉黑"];
    const ERR_LIKE_BIZ_ID_NOT_EXIST = [2214, "点赞失败"];
    const ERR_BLACK_USER_SEND_COMMENT_IS_NOT_ALLOWED = [2215, "你因违规操作，暂时无法评论"];
    /**
     * 分享相关
     */
    const ERR_SHARE_PARAMS_IS_NULL = [2301, '分享参数不能为空'];
    const ERR_SHARE_TYPE_IS_INCORRECT = [2302, '分享类型错误'];

    /**
     * admin商品相关
     */
    const ERR_PRODUCT_ITEM_NAME_IS_NULL = [2401, '请填写商品名称'];
    const ERR_PRODUCT_ITEM_MAIN_IMAGE_IS_NULL = [2402, '请上传商品图片'];
    const ERR_PRODUCT_ITEM_PRICE_IS_NULL = [2403, '售价不能为空'];
    const ERR_PRODUCT_ITEM_SUMMARY_IS_NULL = [2404, '商品简介不能为空'];
    const ERR_PRODUCT_ITEM_DESCRIPTION_IS_NULL = [2405, '商品详情不能为空'];
    const ERR_PRODUCT_IS_OFFLINE = [2406, '购买的商品已下架'];
    const ERR_UPDATE_PRODUCT_ITEM_FAILED = [2407, '更新商品item失败'];
    const ERR_CREATE_PRODUCT_SKU_FAILED = [2408, '创建商品SKU失败'];
    const ERR_CREATE_PRODUCT_SKU_PRICE_FAILED = [2409, '创建商品SKU价格失败'];
    const ERR_CREATE_SUPER_PRODUCT_IS_FAILED = [2410, '创建超级商品请先创建sku'];
    const ERR_OPERATE_PRODUCT_TYPE__FAILED = [2411, '未创建sku不能上架'];
    /**
     * admin订单相关
     */
    const ERR_UPDATE_ITEM_ID_IS_NULL = [2501, '商品item_id为空'];
    const ERR_CANCEL_ORDER_INFO_IS_NOT_FULL = [2502, '取消订单参数不完整'];
    const ERR_DELIVER_ORDER_IS_NOT_FULL = [2503, '操作发货信息不完整'];
    const ERR_ORDER_EXPORT_DATA_TIME_IS_NOT_FULL = [2504, '订单导出数据查询时间不完整'];
    /**
     * admin管理登录相关
     */
    const ERR_ADMIN_USER_IS_NOT_EXIST = [2601, "管理用户不存在"];
    const ERR_ADMIN_USER_IS_INVALID = [2602, "管理员状态实效"];
    const ERR_ADMIN_ACCOUNT_PASSWORD_IS_ERROR = [2603, "账号或密码错误"];
    const ERR_ADMIN_ACCOUNT_NEED_LOGIN = [2604, "请先登录"];

    /**
     *剧场编辑内容
     */

    const ERR_CREATE_THEATRE_SHOW_PARAMS_IS_NULL = [2701, '参数不能为空'];


    /**
     *剧场编辑内容
     */
    const ERR_JUHE_API_FAIL = [2801, '聚合数据服务不可用'];

    /**
     * 物流相关
     */

    const ERR_LOGISTICS_ORDER_NOT_EXIST = [2901, '订单查询失败'];
    const ERR_LOGISTICS_KUAIDI100_QUERY_FAIL = [2902, '订单查询服务失败'];
    const ERR_LOGISTICS_KUAIDI100_DATA_FAIL = [2903, '订单查询数据无效'];


    const ERR_COMPANY_NAME_IS_NULL = [10001, "公司名称不能为空"];
    const ERR_COMPANY_LICENSE_IS_NULL = [10002, "营业执照号码不能为空"];
    const ERR_COMPANY_LICENSE_IMAGE_IS_NULL = [10003, "营业执照图片不能为空"];
    const ERR_COMPANY_NOT_FOUND = [10004, "公司没有找到"];
    const ERR_COMPANY_ID_IS_NULL = [10005, "请选择公司"];
    const ERR_STORE_ADDRESS_IS_NULL = [10006, "店面地址不能为空"];
    const ERR_STORE_PROVINCE_IS_NULL = [10007, "省份不能为空"];
    const ERR_STORE_CITY_IS_NULL = [10008, "店面地址不能为空"];
    const ERR_STORE_NUM_IS_NULL = [10009, "店铺编号不能为空"];
    const ERR_STORE_NAME_IS_NULL = [10010, "店铺名称不能为空"];
    const ERR_STORE_NOT_FOUND = [10011, "店铺没有找到"];
    const ERR_COMPANY_ACCOUNT_INFO_IS_NULL = [10012, "请设置企业用户登录信息"];
    const ERR_COMPANY_ACCOUNT_HAS_EXIST = [10013, "企业账号已经存在,请重新输入"];
    const ERR_COMPANY_ACCOUNT_NOT_EXIST = [10014, "企业账号不存在"];
    const ERR_COMPANY_ACCOUNT_FROZEN = [10015, "企业账号已冻结"];
    const ERR_COMPANY_PASSWORD_IS_ERROR = [10016, "企业账号密码有误"];
    const ERR_COMPANY_ACCOUNT_NEED_LOGIN = [10017, "请先登录"];
    const ERR_STORE_NO_RIGHT_DELETE = [10018, "你无权删除这个店铺"];
    const ERR_STORE_HAS_EXIST = [10019, "店铺已经存在了"];


    /**
     * 打卡相关
     */

    const ERR_CREATE_TASK_PARAMS_IS_NOT_FULL = [9001, "参与打卡任务参数不完整"];
    const ERR_CREATE_TASK_INFO_IS_NOT_EXIST = [9002, "打卡任务不存在"];
    const ERR_QUERY_TASK_HISTORY_STAR_ID_IS_NULL = [9003, '查询打卡历史明星id为空'];
    const ERR_GET_TASK_RECORD_USER_ID_IS_NULL = [9004, '查询打卡历史user_id为空'];
    const ERR_NEED_FOLLOW_IF_CREATE_TASK_RECORD = [9005, '需关注明星才能打卡'];
    const ERR_VISIT_RECORD_IS_NOT_ALLOWED = [9006, '没有权限查看他人打卡记录'];


    /**
     * 搜索feed相关
     */

    const ERR_SEARCH_FEED_TAG_IS_NULL = [10001, '搜索标签关键词为空'];
    const ERR_SEARCH_ES_BODY_IS_NULL = [10002, 'es数据解析失败'];


    /**
     * 购物车
     */
    const ERR_SHOPPING_CART_MAX_SIZE = [17001, '购物车已经装满了~'];
    const ERR_SHOPPING_CART_COUNT_ERROR = [17002, '变更数量不能为0'];
    const ERR_SHOPPING_CART_NULL_PRODUCT = [17003, '请选择你要加入购物车的商品'];
    const ERR_SHOPPING_CART_PRODUCT_MAX_COUNT = [17004, '单件商品最多购买99件'];
    const ERR_SHOPPING_CART_PRODUCT_STOCK_NOT_ENOUGH = [17005, '该商品库存不足'];
    /**
     * voucher
     */
    const ERR_VOUCHER_SN_NOT_FOUND = [16001, '优惠券无效'];
    const ERR_VOUCHER_SN_CANT_USE = [16002, '优惠券码无法使用'];
    const ERR_VOUCHER_CANT_USE = [16003, '优惠券无法正常使用'];
    const ERR_VOUCHER_IS_EXPIRE = [16004, '优惠券已过期'];
    const ERR_VOUCHER_PRODUCT_IS_NULL = [16005, "请选择你要购买的商品"];
    const ERR_VOUCHER_IS_NOT_ENOUGH = [16006, '优惠券已领完'];
    const ERR_VOUCHER_IS_RECEIVED= [16007, '你已经领过这个券了'];


    /**
     * 健康项目专用
     *
     */
    const ERR_JK_SIGN_TYPE_NO_NAME = [200001, '请输入打卡类型名称'];
    const ERR_JK_SIGN_TASK_NO_NAME = [200002, '请输入打卡任务名称'];
    const ERR_JK_SIGN_TASK_VALUE_FAIL = [200003, '请输入有效的打卡任务数值'];
    const ERR_JK_SIGN_TASK_TYPE_NOT_CHOOSE = [200004, '请选择打卡区'];
    const ERR_JK_SIGN_TASK_NOT_EXIST = [200005, '请选择打卡任务'];
    const ERR_JK_SIGN_TASK_NO_DETAIL = [200006, '请输入打卡任务说明'];
    const ERR_JK_SIGN_TASK_HAS_SIGN = [200007, '用户已经打过卡了'];
    const ERR_JK_SIGN_TASK_TIP_CONTENT_CANT_NULL = [200008, '请输入有效的文案和图片'];


    /**
     * 检测区
     */
    const ERR_USER_NO_RESULT = [200102, '没有权限'];
    const ERR_CHECK_NO_RESULT = [200101, '没有对应的结果'];

    const ERR_HAS_KNOWLEDGE = [200102, '请先删除该分类下的文章'];
    const ERR_CATEGORY_HAVE_CHILDREN = [200103, '请先删除该分类下的二级分类'];
    const ERR_CATEGORY_HAVE_PRODUCT = [200104, '请先删除该分类下的商品'];
    const ERR_CLASS_HAVE_DISEASE = [200105, '请先删除该科室内的疾病'];
    const ERR_RISK_CATEGORY_HAVE_CHILDREN= [200106, '请先删除该分类下的二级分类'];
    const ERR_SECURITY_WORD= [200107, '内容中含有敏感词'];
}