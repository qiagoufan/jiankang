<?php


class Config_Const
{
    const ADMIN_ACCOUNT_SALT = 'sbu3qnfoasnda@71208815';

    const COMPANY_ACCOUNT_SALT = 'GUQZJHnppkizo1#fgIBaIbw';


    const BIZ_TYPE_RISK_CHECK_RESULT = 'risk_check';
    const BIZ_TYPE_DISEASE = 'disease';
    const BIZ_TYPE_SELF_CHECK_RESULT = 'self_check';
    const BIZ_TYPE_PRODUCT= 'product';


    const BIZ_TYPE_FEED = 'feed';


    const FEED_STATUS_DELETED = 0;
    const FEED_STATUS_NORMAL = 1;
    const FEED_STATUS_WAIT_CHECK = -1;
    const FEED_STATUS_REJECTED = -2;

    const FEED_TYPE_NORMAL = 1;

    const FEED_TYPE_TEMPLATE_TYPE_1 = 1;

    const FEED_SCENE_INDEX = 'index';

    const ALI_PAY_TYPE = 1;
    const WECHAT_PAY_TYPE = 2;


    /**
     * 手机验证码
     */
    const VERIFICATION_CODE_INTERVAL_TIME_LONG = 60000;
    const VERIFICATION_CODE_DAILY_MAX_SEND_COUNT = 30;
    const VERIFICATION_CODE_EXPIRE_TIME_LONG = 600000;
    const VERIFICATION_CODE_MAX_VERIFICATION_COUNT = 6;
    /**
     *
     */
    //jwt只能用秒
    const JWT_TOKEN_EXPIRE_TIME = 15 * 86400;


    const USER_LOGIN_TYPE_PHONE = 'phone';
    const USER_LOGIN_TYPE_WX_MINI = 'wxMini';
    const USER_LOGIN_TYPE_ACCOUNT = 'account';


    /**
     *用户状态
     */
    const USER_STATUS_DELETED = 0;
    const USER_STATUS_VALID = 1;

    /**
     *用户类型
     */
    const USER_TYPE_NORMAL = 1;
    const USER_TYPE_ADMIN = 2;

    //满减
    const VOUCHER_TYPE_DISCOUNT = 1;
    //打折
    const VOUCHER_TYPE_OFF = 2;

    /**
     * voucher是否可用
     */

    const VOUCHER_ENABLE_STATUS_VALID = 1;
    //已经使用
    const VOUCHER_ENABLE_STATUS_USED = -1;
    //已过期
    const VOUCHER_ENABLE_STATUS_EXPIRED = -2;
    const VOUCHER_ENABLE_STATUS_INCOMPATIBLE = -10000;
    const VOUCHER_STATUS_USEFUL = 1;
    const VOUCHER_STATUS_DELETE = 0;

    /**
     *用户类型
     */
    const AD_TYPE_IMAGE = 'image';


    //order 相关
    /** @var int 待付款 */
    const  ORDER_STATUS_TO_PAY = 10;
    /** @var int 已支付 */
    const ORDER_STATUS_HAS_PAY = 20;
    /** @var int 待发货 */
    const ORDER_STATUS_WAIT_SEND = 30;
    /** @var int 待收货 */
    const ORDER_STATUS_WAIT_RECEIVE = 40;
    /** @var int 交易完成 */
    const ORDER_STATUS_FINISHED = 50;
    /** @var int 交易关闭 */
    const ORDER_STATUS_CLOSED = 60;
    /** @var int 待评价 */
    const ORDER_STATUS_WAIT_COMMENT = 70;
    /** @var int 已评价 */
    const ORDER_STATUS_HAS_COMMENT = 80;


    //订单关闭类型
    const ORDER_CLOSE_TYPE_BY_HAND = 1;
    const ORDER_CLOSE_TYPE_AUTO = 2;

    /**
     * 支付状态
     */
    const PAY_STATUS_NO_PAY = 0;//未支付
    const PAY_STATUS_PAYED = 1;//支付完成


    /**
     * 商品状态
     */


    const PUBLISHED_STATUS_ONLINE = 1;
    const PUBLISHED_STATUS_OFFLINE = -1;


    //自检
    const CHECK_TYPE_SELF = 'self_check';
    //风险检
    const CHECK_TYPE_RISK = 'risk_check';


    const CALCULATION_TYPE_PLUS=1;
    const CALCULATION_TYPE_MUL=2;


}