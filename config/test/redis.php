<?php
return array(
    'unlogin_message_redis' => array(
        array(
            'bid' => 0,
            'eid' => 99,
            'ip' => '127.0.0.1',
            'port' => 6379
        )
    ),
    'user' => array(
        array(
            'bid' => 0,
            'eid' => 99,
            'ip' => '39.98.210.75',
            'port' => 6379
        )
    ),
    'follow' => array(
        array(
            'bid' => 0,
            'eid' => 99,
            'ip' => '127.0.0.1',
            'port' => 6379
        )
    ),
    'feed' => array(
        array(
            'bid' => 0,
            'eid' => 99,
            'ip' => '127.0.0.1',
            'port' => 6379
        )
    ),
    'product' => array(
        array(
            'bid' => 0,
            'eid' => 99,
            'ip' => '127.0.0.1',
            'port' => 6379
        )
    ),
    'tag' => array(
        array(
            'bid' => 0,
            'eid' => 99,
            'ip' => '127.0.0.1',
            'port' => 6379
        )
    ),
    'maintain' => array(
        array(
            'bid' => 0,
            'eid' => 99,
            'ip' => '39.98.210.75',
            'port' => 6379
        )
    ),
    'interactive' => array(
        array(
            'bid' => 0,
            'eid' => 99,
            'ip' => '127.0.0.1',
            'port' => 6379
        )
    )
);
