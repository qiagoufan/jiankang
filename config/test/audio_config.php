<?php
return [
    /**明星音频列表*/
    'star_audio_list' => [
        '10' => ['video_link' => 'https://cdn.ipxmall.com/system/admin/f483e863645272744337000a50add40d.mp3', 'cover' => 'https://cdn.ipxmall.com/system/admin/1b94ce31f41f512d694ec30b42ccff3e.jpg'],
        '39' => ['video_link' => 'https://cdn.ipxmall.com/ipxmall/d8efad168c23da0038d1338614a78591', 'cover' => 'https://cdn.ipxmall.com/ipxmall/aea188591bd7f675d6511cf1289fcc59'],
        '18' => ['video_link' => 'https://cdn.ipxmall.com/ipxmall/cbd45b21bab1a0d4ca2e48ad34052fea', 'cover' => 'https://cdn.ipxmall.com/ipxmall/85450e0289e6d1f8e8a8fd70e8630b68'],
        '45' => ['video_link' => 'https://cdn.ipxmall.com/ipxmall/c7ced43d833aa34e720010010042a51b', 'cover' => 'https://cdn.ipxmall.com/ipxmall/19eaf8a0c3707c16e65754fd51421833'],
        '14' => ['video_link' => 'https://cdn.ipxmall.com/ipxmall/445e9b0a40372180ce25f00ee51d6421', 'cover' => 'https://cdn.ipxmall.com/ipxmall/77d0fd7ba083d6a34090ee30d06332a1'],
        '32' => ['video_link' => 'https://cdn.ipxmall.com/ipxmall/b9478e0001599745695860d6b7b0be81', 'cover' => 'https://cdn.ipxmall.com/ipxmall/ff48ddde03635ec88f7381457451b5c8'],
        '23' => ['video_link' => 'https://cdn.ipxmall.com/ipxmall/0910f991097b82cf14b7fcdda03e6464', 'cover' => 'https://cdn.ipxmall.com/ipxmall/3f6117741d72ae5cbba1b961ef30c656'],
        '46' => ['video_link' => 'https://cdn.ipxmall.com/ipxmall/45ba62956a2ed9bc884f61e5127b3340', 'cover' => 'https://cdn.ipxmall.com/ipxmall/6042f7cd8669757b9b84367ab0399c4b'],
        '7' => ['video_link' => 'https://cdn.ipxmall.com/ipxmall/fcf4fc449c6fda10428ed1dab4827f8d', 'cover' => 'https://cdn.ipxmall.com/ipxmall/1ad2b813b08b53e90abf03815c850d42'],
        '29' => ['video_link' => 'https://cdn.ipxmall.com/ipxmall/39cebe46ac40529667579aa1421aaf03', 'cover' => 'https://cdn.ipxmall.com/ipxmall/dd7bdc0384f8092c542c038383868ab9'],
        '22' => ['video_link' => 'https://cdn.ipxmall.com/ipxmall/4ff094ba51ffcfb413ccdb4a6425d81c', 'cover' => 'https://cdn.ipxmall.com/ipxmall/4122a022f14178912f4832ddeb1a4e3b'],

    ]


];