<?php
return array(
    'express_company_list' => [
        'zhongtong' => '中通快递',
        'yunda' => '韵达快递',
        'yuantong' => '圆通速递',
        'huitongkuaidi' => '百世快递',
        'shentong' => '申通快递',
        'youzhengguonei' => '邮政快递包裹',
        'shunfeng' => '顺丰速运',
        'jd' => '京东物流',
        'tiantian' => '天天快递',
        'ems' => 'EMS',
        'youzhengbk' => '邮政标准快递',
        'zhaijisong' => '宅急送',
        'debangwuliu' => '德邦',
        'youshuwuliu' => '优速快递',
        'jtexpress' => '极兔速递',
        'debangkuaidi' => '德邦快递',
        'yuantongkuaiyun' => '圆通快运',
        'zhongtongkuaiyun' => '中通快运',
        'yundakuaiyun' => '韵达快运',
        'baishiwuliu' => '百世快运',
        'annengwuliu' => '安能快运',
        'danniao' => '丹鸟',
        'suning' => '苏宁物流',
        'dhl' => 'DHL - 中国件',
        'shpost' => '同城快寄',
        'jinguangsudikuaijian' => '京广速递',
        'youzhengguoji' => '国际包裹',
        'lntjs' => '特急送',
        'meixi' => '美西快递',
        'rrs' => '日日顺物流'
    ]

);